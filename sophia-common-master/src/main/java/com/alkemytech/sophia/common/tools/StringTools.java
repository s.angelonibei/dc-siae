package com.alkemytech.sophia.common.tools;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class StringTools {

	public static String DURATION_FORMAT = "^((?=(?:\\D*\\d\\D*){6})(?:[01]\\d|2[0-3]):(?:[0-5]\\d):(?:[0-5]\\d))$";
	
	public static String[] split(String value, String regex) {
		return null == value ? null : value.split(regex);
	}

	public static String replace(String value, CharSequence target, CharSequence replacement) {
		return null == value ? null : value.replace(target, replacement);
	}

	public static String join(char delimiter, Object...args){
		String ret ="";
		if(args != null){
			for (Object current : args){
				if( current != null && StringUtils.isNotEmpty( (String)current ) ){
					if(StringUtils.isNotEmpty(ret)){
						ret = ret+ delimiter;
					}
					ret = ret + (String)current;
				}
			}
		}
		return ret;
	}
	
	public static boolean isNullOrEmpty(String value) {
		return null == value || "".equals(value);
	}

	public static boolean isNullOrZero(String value) {

		return null == value || "0".equals(value);
	}

	public static boolean equals(String left, String right, boolean bothNull) {
		if (null == left) {
			return null == right ? bothNull : false;
		} else {
			return left.equals(right);
		}
	}

	public static boolean equalsIgnoreCase(String left, String right, boolean bothNull) {
		if (null == left) {
			return null == right ? bothNull : false;
		} else {
			return left.equalsIgnoreCase(right);
		}
	}

	public static String[] parseCsv(String value, String regex, boolean trim) {
		if (StringTools.isNullOrEmpty(value)) {
			return null;
		}
		final String[] items = value.split(regex);
		if (trim) for (int i = 0; i < items.length; i ++) {
			items[i] = items[i].trim();
		}
		return items;
	}

	
	public static String asCsv(char separator, String...values) {
		final StringBuffer csv = new StringBuffer();
		if (values.length > 0) {
			csv.append(values[0]);
			for (int i = 1; i < values.length; i ++) {
				csv.append(separator).append(values[i]);
			}
		}
		return csv.toString();
	}

	public static String asCsv(char separator, Collection<String> values) {
		final StringBuffer csv = new StringBuffer();
		if (values.size() > 0) {
			Iterator<String> iterator = values.iterator();
			csv.append(iterator.next());
			while (iterator.hasNext()) {
				csv.append(separator).append(iterator.next());
			}
		}
		return csv.toString();
	}

	public static String toString(InputStream in, String charsetName) throws IOException {
		try (final ByteArrayOutputStream out = new ByteArrayOutputStream()) {
	        final byte[] bytes = new byte[1024];
	        for (int count; -1 != (count = in.read(bytes)); ) {
	        	out.write(bytes, 0, count);
	        }
	        return out.toString(charsetName);
		}
	}

	public static boolean checkFormat(String input, String regex){
		try{
			if(StringUtils.isNotEmpty(input) && StringUtils.isNotEmpty(regex)){
				if(input.matches(regex)){
					return true;
				}
			}
			else if(StringUtils.isEmpty(input)){
				return true;
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public static String buildBucketfromDate(String bucketFormat, String baseBucket, String fileName ){
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		return String.format(bucketFormat, cleanPath(baseBucket,false),System.getenv("ENVNAME"), cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DAY_OF_MONTH), fileName);
	}

	public static String buildBucketfromBroadcaster(String bucketFormat, String baseBucket, String broadcasterName, String fileName ){
		return  String.format(bucketFormat, cleanPath(baseBucket,false),System.getenv("ENVNAME"), broadcasterName, fileName);
	}

	private static String cleanPath( String path, boolean includeEndSlash){
		if( path.endsWith("/") && !includeEndSlash ){
			return path.substring(0, path.length()-1);
		}
		if( !path.endsWith("/") && includeEndSlash ){
			return path+"/";
		}
		return path;
	}

	public static String substring (String inputString, int start, int end){
		try{
			String ret = inputString.substring(start, end);
			return ret;
		}
		catch (Exception e ){
			return null;
		}
	}

	public static String parseTimeToSeconds(String format) {
		try {
			int seconds = 0;
			String[] split = format.split(":");
			switch (split.length){
				case 3:
					seconds += Double.parseDouble(split[0])*3600;
					seconds += Double.parseDouble(split[1])*60;
					seconds += Double.parseDouble(split[2]);
					break;
				case 2:
					seconds += Double.parseDouble(split[0])*60;
					seconds += Double.parseDouble(split[1]);
					break;
				default:
					seconds += Double.parseDouble(split[0]);
			}
			return Integer.toString(seconds);
		}
		catch (Exception e) {
			return null;
		}
	}

	public static String parseDurationFormat(String durationValue, String durationFormat, String indexes){
		try{
			List<String> parts = new ArrayList<>();
			String[] intervals = indexes.split(",");
			if(intervals != null && intervals.length > 0){
				Integer lastIndex = 0;
				for(String interval : intervals){
					parts.add(durationValue.substring(lastIndex, Integer.parseInt(interval) ) );
					lastIndex = Integer.parseInt(interval);
				}
				parts.add(durationValue.substring(lastIndex));
			}
			return String.format(durationFormat, parts.toArray(new String[parts.size()]));
		}
		catch (Exception e) {
			return null;
		}
	}

	public static String parseSecondsToTime(String value) {
		try{
			long seconds = Long.parseLong(value);
			Period period = new Period(seconds * 1000L);
			PeriodFormatter fmt = new PeriodFormatterBuilder()
					.printZeroAlways().minimumPrintedDigits(2).appendHours().appendSeparator(":")
					.printZeroAlways().minimumPrintedDigits(2).appendMinutes().appendSeparator(":")
					.printZeroAlways().minimumPrintedDigits(2).appendSeconds().toFormatter();
			return fmt.print(period);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String paddingLeadingZeroes(String input, int length){
		while (input.length() < length) {
			input = "0" + input;
		}
		return input;
	}

	public static boolean checkISWC(String iswc){
		final String regex = "\\.|-|_";
		final String regexFormat = "[a-zA-Z]{1}[0-9]{10}";
		if(!StringTools.isNullOrEmpty(iswc)) {
		    iswc = iswc.replaceAll(regex, "");
			if(iswc.matches(regexFormat))return true;
			else return false;
		}
		return false;
	}

	public static boolean checkISRC(String isrc){
		final String regex = "\\.|-|_";
		final String regexFormat = "[a-zA-Z]{2}[a-zA-Z0-9]{3}[0-9]{7}";

		if (!StringTools.isNullOrEmpty(isrc)){
			isrc = isrc.replaceAll(regex, "");
			if(isrc.matches(regexFormat))return true;
			else return false;
		}
		return false;
	}

	public static boolean checkLunh(String iswc){
		int sum = 0;
		boolean alternate = false;
		if(checkISWC(iswc)){
			final String regex = "\\.|-|_|[a-zA-Z]";
			int checkNumber = Integer.parseInt(String.valueOf(iswc.charAt(iswc.length()-1)));
			iswc = iswc.replaceAll(regex, "");
			iswc = iswc.substring(0, iswc.length() - 1);
			for (int i = iswc.length()-1; i>0; i--){
				int n = Integer.parseInt(iswc.substring(i, i + 1));
				if (alternate)
				{
					n *= 2;
					if (n > 9)
					{
						n = (n % 10) + 1;
					}
				}
				sum += n;
				alternate = !alternate;
			}
			return ((sum*9)%10) == checkNumber;
		}
		return false;
	}


	public static void main (String[] args){

		System.out.println(checkLunh("t-0000000000"));
//		String duration = "25:25:59";
//		boolean ret = duration.matches(DURATION_FORMAT);
//		System.out.println( ret );
//		String duration = "991515";
//		System.out.println( parseDurationFormat(duration, "%s:%s", "3," ) );

//		long seconds = Long.parseLong("150");
//		Period period = new Period(seconds * 1000L);
//		PeriodFormatter fmt = new PeriodFormatterBuilder()
//				.printZeroAlways()
//				.minimumPrintedDigits(2)
//				.appendHours()
//				.appendSeparator(":")
//				.printZeroAlways()
//				.minimumPrintedDigits(2)
//				.appendMinutes()
//				.appendSeparator(":")
//				.printZeroAlways()
//				.minimumPrintedDigits(2)
//				.appendSeconds()
//				.toFormatter();
//		String time = fmt.print(period);

	}
}
