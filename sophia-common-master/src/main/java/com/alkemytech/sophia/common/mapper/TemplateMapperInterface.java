package com.alkemytech.sophia.common.mapper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.apache.poi.ss.usermodel.Cell;
import org.beanio.BeanReader;
import org.beanio.BeanWriter;
import org.beanio.StreamFactory;

import com.alkemytech.sophia.common.s3.S3Service;

public interface TemplateMapperInterface {

    StreamFactory loadStreamFactory(String configFilePath);
    StreamFactory loadStreamFactory( String configFilePath, S3Service s3Service) throws IOException;
    BeanReader createBeanReader(String configPath, String configStream, ByteArrayOutputStream bos);
    BeanReader createBeanReader(String configPath, String configStream, ByteArrayOutputStream bos, S3Service s3Service, Charset charset) throws IOException;
    BeanWriter createBeanWriter(String configPath, String configStream, File fileToWrite);
    BeanWriter createBeanWriter(String configPath, String configStream, File fileToWrite, S3Service s3Service)throws IOException;
    List<Object> parseFile(String configPath, String configStream, ByteArrayOutputStream bos);
    List<Object> parseFile(String configPath, String configStream, ByteArrayOutputStream bos, S3Service s3Service, Charset charset) throws IOException;
    Map<String, Object> parseEmpyTemplate(String configPath, String configStream, ByteArrayOutputStream bos);
    Map<String, Object> parseEmpyTemplate(String configPath, String configStream, ByteArrayOutputStream bos, S3Service s3Service)throws IOException;
    void writeFile(Map<String, Object>header, List<Map<String,Object>> recordList, String configPath, String configStream, File fileToWrite) throws Exception;
    void writeFile(Map<String, Object>header, List<Map<String,Object>> recordList, String configPath, String configStream, File fileToWrite, S3Service s3Service) throws Exception;

    List<Object> parseXlsFile(String configFilePath, ByteArrayOutputStream bos) throws Exception;
    List<Object> parseXlsFile(String configFilePath, ByteArrayOutputStream bos, S3Service s3Service) throws Exception;

    XlsMapperConfiguration loadXlsConfiguration(String configFilePath) throws JAXBException;
    XlsMapperConfiguration loadXlsConfiguration(String configFilePath, S3Service s3Service) throws JAXBException;

    void parseCell(Cell cell, XlsMapperConfiguration configParser, Map<String, Object> parsedRow);
    void evaluateHeaderCell(Cell cell, XlsMapperConfiguration configParser) throws Exception;

}