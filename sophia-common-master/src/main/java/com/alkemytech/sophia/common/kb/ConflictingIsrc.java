package com.alkemytech.sophia.common.kb;

import com.google.gson.GsonBuilder;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class ConflictingIsrc {

	private SophiaIsrcOpera sophiaIsrcOpera;
	private LessonIsrc lessonIsrc;
	
	public SophiaIsrcOpera getSophiaIsrcOpera() {
		return sophiaIsrcOpera;
	}
	public ConflictingIsrc setSophiaIsrcOpera(SophiaIsrcOpera sophiaIsrcOpera) {
		this.sophiaIsrcOpera = sophiaIsrcOpera;
		return this;
	}

	public LessonIsrc getLessonIsrc() {
		return lessonIsrc;
	}
	public ConflictingIsrc setLessonIsrc(LessonIsrc lessonIsrc) {
		this.lessonIsrc = lessonIsrc;
		return this;
	}
	
	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}

}
