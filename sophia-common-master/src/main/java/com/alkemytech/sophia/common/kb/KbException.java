package com.alkemytech.sophia.common.kb;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
@SuppressWarnings("serial")
public class KbException extends Exception {

	public static enum Error {
		
		BAD_REQUEST		("01", "bad request"),
		INSERT_CA		("02", "insert into sophia_ca_opera"),
		UPDATE_CA		("03", "update sophia_ca_opera"),
		INSERT_ISWC		("04", "insert into sophia_iswc_opera"),
		INSERT_ISRC		("05", "insert into sophia_isrc_opera"),
		INSERT_IDSONG	("06", "insert into sophia_idsong_opera"),
		GENERIC			("99", "generic error");
		
		private final String code;
		private final String message;
		
		Error(String code, String message) {
			this.code = code;
			this.message = message;
		}
		
		public String getCode() {
			return code;
		}
		public String getMessage() {
			return message;
		}
		
	};
	
	final Error error;
	
	public KbException(Error error, Throwable cause) {
		super(String.format("[%s] %s", error.getCode(), error.getMessage()), cause);
		this.error = error;
	}

	public KbException(Error error) {
		super(String.format("[%s] %s", error.getCode(), error.getMessage()));
		this.error = error;
	}

	public KbException(Error error, String message) {
		super(String.format("[%s] %s", error.getCode(), message));
		this.error = error;
	}
	
	public Error getError() {
		return error;
	}

}
