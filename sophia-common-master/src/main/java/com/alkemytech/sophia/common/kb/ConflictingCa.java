package com.alkemytech.sophia.common.kb;

import com.google.gson.GsonBuilder;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class ConflictingCa {

	private SophiaCaOpera sophiaCaOpera;
	private LessonCa lessonCa;
	private String artistName;

	public SophiaCaOpera getSophiaCaOpera() {
		return sophiaCaOpera;
	}

	public ConflictingCa setSophiaCaOpera(SophiaCaOpera sophiaCaOpera) {
		this.sophiaCaOpera = sophiaCaOpera;
		return this;
	}

	public LessonCa getLessonCa() {
		return lessonCa;
	}

	public ConflictingCa setLessonCa(LessonCa lessonCa) {
		this.lessonCa = lessonCa;
		return this;
	}

	public String getArtistName() {
		return artistName;
	}

	public ConflictingCa setArtistName(String artistName) {
		this.artistName = artistName;
		return this;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}
	
}
