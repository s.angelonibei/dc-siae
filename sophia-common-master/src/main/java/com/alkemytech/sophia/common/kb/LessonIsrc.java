package com.alkemytech.sophia.common.kb;

import com.google.gson.GsonBuilder;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class LessonIsrc {

	private String isrc;
	private String codiceOpera;
	private String origin;
	private String idutilSource;
	private String iddsrSource;

	public String getIsrc() {
		return isrc;
	}
	public LessonIsrc setIsrc(String isrc) {
		this.isrc = isrc;
		return this;
	}
	public String getCodiceOpera() {
		return codiceOpera;
	}
	public LessonIsrc setCodiceOpera(String codiceOpera) {
		this.codiceOpera = codiceOpera;
		return this;
	}
	public String getOrigin() {
		return origin;
	}
	public LessonIsrc setOrigin(String origin) {
		this.origin = origin;
		return this;
	}
	public String getIdutilSource() {
		return idutilSource;
	}
	public LessonIsrc setIdutilSource(String idutilSource) {
		this.idutilSource = idutilSource;
		return this;
	}
	public String getIddsrSource() {
		return iddsrSource;
	}
	public LessonIsrc setIddsrSource(String iddsrSource) {
		this.iddsrSource = iddsrSource;
		return this;
	}
	
	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}

}
