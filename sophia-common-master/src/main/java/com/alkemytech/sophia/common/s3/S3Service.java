package com.alkemytech.sophia.common.s3;

import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.tools.StringTools;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.auth.PropertiesFileCredentialsProvider;
import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressEventType;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.services.s3.transfer.Download;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.amazonaws.services.s3.transfer.Upload;
import com.amazonaws.util.IOUtils;
import com.amazonaws.util.StringUtils;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Properties;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class S3Service {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Properties configuration;

	private TransferManager transferManager;
	private AmazonS3 s3Client;

	@Inject
	protected S3Service(@Named("configuration") Properties configuration) {
		super();
		this.configuration = configuration;
	}

	public void startup() {
		shutdown();
		final String proxyHost = configuration.getProperty("proxy.host");
		final String proxyPort = configuration.getProperty("proxy.port");
		final ClientConfiguration clientConfiguration = new ClientConfiguration();
		if (!StringUtils.isNullOrEmpty(proxyHost) && !StringUtils.isNullOrEmpty(proxyPort)) {
			clientConfiguration.setProxyHost(proxyHost);
			clientConfiguration.setProxyPort(Integer.parseInt(proxyPort));
		}
		final String region = configuration.getProperty("aws.region", "eu-west-1");
		logger.debug("startup: region {}", region);
		final String credentialsFilePath = configuration.getProperty("aws.credentials");
		logger.debug("startup: credentialsFilePath {}", credentialsFilePath);
		final AWSCredentialsProvider credentials = StringTools.isNullOrEmpty(credentialsFilePath)
				? new ClasspathPropertiesFileCredentialsProvider()
				: new PropertiesFileCredentialsProvider(credentialsFilePath);
		s3Client = AmazonS3ClientBuilder.standard().withCredentials(credentials)
				.withClientConfiguration(clientConfiguration).withRegion(region).build();
		transferManager = TransferManagerBuilder.standard().withS3Client(s3Client).build();
	}

	public void shutdown() {
		if (null != transferManager) {
			transferManager.shutdownNow(true);
		}
	}

	public boolean upload(String bucket, String key, final File file) {
		for (int retries = Integer
				.parseInt(configuration.getProperty("s3.retry.put_object", "1")); retries > 0; retries--) {
			try {
				logger.debug("upload: upload started {} {}", file.getName(), file.length());
				final PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, key.replace("//", "/"), file);
				putObjectRequest.setGeneralProgressListener(new ProgressListener() {
					private long accumulator = 0L;
					private long total = 0L;
					private final long chunk = Math.max(16L * 1024L, file.length() / 20L);

					@Override
					public void progressChanged(ProgressEvent event) {
						if (event.getEventType().equals(ProgressEventType.REQUEST_BYTE_TRANSFER_EVENT)) {
							accumulator += event.getBytes();
							total += event.getBytes();
							if (accumulator >= chunk) {
								accumulator -= chunk;
								logger.debug("upload: {}Kb {}", total / 1024, file.getName());
							}
						} else if (event.getEventType().equals(ProgressEventType.TRANSFER_COMPLETED_EVENT)) {
							logger.debug("upload: transfer completed {}", file.getName());
						}
					}
				});
				final Upload upload = transferManager.upload(putObjectRequest);
				upload.waitForCompletion();
				logger.debug("upload: upload finished {}", file.getName());
				return true;
			} catch (Exception e) {
				logger.error("upload", e);
			}
		}
		logger.debug("upload: upload failed {}", file.getName());
		return false;
	}

	public boolean upload(String bucket, String key, final InputStream inputStream, String fileName) {
		for (int retries = Integer.parseInt(configuration.getProperty("s3.retry.put_object", "1")); retries > 0; retries--) {
			try {
				logger.debug("upload: upload started {}", fileName);
				ObjectMetadata metadata = new ObjectMetadata();
				byte[] bytes = IOUtils.toByteArray(inputStream);
				metadata.setContentLength(bytes.length);
				final PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, key.replace("//", "/"), new ByteArrayInputStream(bytes), metadata);
				final Upload upload = transferManager.upload(putObjectRequest);
				upload.waitForCompletion();
				logger.debug("upload: upload finished {}", fileName);
				return true;
			} catch (Exception e) {
				logger.error("upload", e);
			}
		}
		logger.debug("upload: upload failed {}", fileName);
		return false;
	}

	public boolean upload(String bucket, String key, String mimeType, final byte[] bytes) {
		for (int retries = Integer
				.parseInt(configuration.getProperty("s3.retry.put_object", "1")); retries > 0; retries--) {
			try (final ByteArrayInputStream in = new ByteArrayInputStream(bytes)) {
				logger.debug("upload: upload started");
				final ObjectMetadata meta = new ObjectMetadata();
				meta.setContentLength(bytes.length);
				meta.setContentType(mimeType);
				final PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, key.replace("//", "/"), in, meta);
				putObjectRequest.setGeneralProgressListener(new ProgressListener() {
					private long accumulator = 0L;
					private long total = 0L;
					private final long chunk = Math.max(1024L, (long) bytes.length / 20L);

					@Override
					public void progressChanged(ProgressEvent event) {
						if (event.getEventType().equals(ProgressEventType.REQUEST_BYTE_TRANSFER_EVENT)) {
							accumulator += event.getBytes();
							total += event.getBytes();
							if (accumulator >= chunk) {
								accumulator -= chunk;
								logger.debug("upload: {}Kb {}", total / 1024, bytes.length);
							}
						} else if (event.getEventType().equals(ProgressEventType.TRANSFER_COMPLETED_EVENT)) {
							logger.debug("upload: transfer completed");
						}
					}
				});
				final Upload upload = transferManager.upload(putObjectRequest);
				upload.waitForCompletion();
				logger.debug("upload: upload finished");
				return true;
			} catch (Exception e) {
				logger.error("upload", e);
			}
		}
		logger.debug("upload: upload failed");
		return false;
	}

	public boolean delete(String bucket, String key) {
		for (int retries = Integer
				.parseInt(configuration.getProperty("s3.retry.delete_object", "1")); retries > 0; retries--) {
			try {
				s3Client.deleteObject(bucket, key);
				return true;
			} catch (Exception e) {
				logger.error("delete", e);
			}
		}
		return false;
	}

	public boolean download(String bucket, String key, File file) {
		try {
			Download xfer = transferManager.download(bucket, key, file);
			// loop with xfer.isDone()
			// or block with Transfer.waitForCompletion()
			xfer.waitForCompletion();
			return true;
		} catch (Exception e) {
			logger.error("download in file", e);
		}
		return false;
	}

	public boolean download(String bucket, String key, OutputStream out) {
		try {
			final S3Object object = s3Client.getObject(new GetObjectRequest(bucket, key));
			try (final InputStream in = object.getObjectContent()) {
				int count = 0;
				final byte[] bytes = new byte[1024];
				while (-1 != (count = in.read(bytes))) {
					out.write(bytes, 0, count);
				}
			}
			return true;
		} catch (Exception e) {
			logger.error("download", e);
			return false;
		}
	}

	public InputStream download(String bucket, String key) {
		return s3Client.getObject(new GetObjectRequest(bucket, key)).getObjectContent();
	}
	
	public List<S3ObjectSummary> listObjects(String bucketName, String prefix) {
		ObjectListing listing = s3Client.listObjects( bucketName, prefix );
		List<S3ObjectSummary> summaries = listing.getObjectSummaries();

		while (listing.isTruncated()) {
			listing = s3Client.listNextBatchOfObjects (listing);
			summaries.addAll (listing.getObjectSummaries());
		}
		return summaries;
	}

	public S3Object getObject(String bucket, String key) {
		for (int retries = Integer
				.parseInt(configuration.getProperty("s3.retry.get_object", "1")); retries > 0; retries--) {
			try {
				return s3Client.getObject(new GetObjectRequest(bucket, key));
			} catch (Exception e) {
				logger.error("getObject", e);
			}
		}
		return null;
	}

	public boolean doesObjectExist(String bucket, String key) {
		try {
			return s3Client.doesObjectExist(bucket, key);
		} catch (Exception e) {
			logger.error("doesObjectExist", e);
			return false;
		}
	}

	public boolean copyObject(String bucket, String fromKey, String toKey) {
		try {
			final CopyObjectRequest request = new CopyObjectRequest(bucket, fromKey, bucket, toKey);
			final CopyObjectResult result = s3Client.copyObject(request);
			return null != result;
		} catch (Exception e) {
			logger.error("copyObject", e);
			return false;
		}
	}

	public boolean deleteObject(String bucket, String key) {
		try {
			final DeleteObjectRequest request = new DeleteObjectRequest(bucket, key);
			s3Client.deleteObject(request);
			return true;
		} catch (Exception e) {
			logger.error("deleteObject", e);
			return false;
		}
	}

	public boolean lockFile(String bucket, String key){
		return lockFile(bucket, key, Constants.LOCK_SUFFIX);
	}

	public boolean lockFile(String bucket, String key,String lockSuffix){
		if( doesObjectExist(bucket, key) && !doesObjectExist(bucket, key + lockSuffix ) ){
			logger.debug("Locking file {} with suffix {}", key, lockSuffix );
			// create meta-data for your folder and set content-length to 0
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentLength(0);
			// create empty content
			InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
			// create a PutObjectRequest passing the folder name suffixed by /
			PutObjectRequest putObjectRequest = new PutObjectRequest(bucket,
					key + lockSuffix, emptyContent, metadata);
			// send request to S3 to create folder
			s3Client.putObject(putObjectRequest);
			logger.debug("File {} locked!", key);
			return true;
		}
		logger.debug("Unable to lock file {}", key);
		return false;
	}
}
