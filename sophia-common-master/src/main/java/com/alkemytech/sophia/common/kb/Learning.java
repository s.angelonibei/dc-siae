package com.alkemytech.sophia.common.kb;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.common.tools.StringTools;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class Learning {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private Connection connection;
	private int kbVersion;

	public Learning() {
		super();
		this.connection = null;
		this.kbVersion = 0;
	}
	
	public Learning setConnection(Connection connection) {
		this.connection = connection;
		return this;
	}

	public Learning setKbVersion(int kbVersion) {
		this.kbVersion = kbVersion;
		return this;
	}

	public static String getHashTitle(String title) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		return Hex.encodeHexString(MessageDigest.getInstance("MD5")
				.digest(title.getBytes("UTF-8")));
	}
	
	public static String getIdSong(String albumTitle, String proprietaryId, String title, String artists, String roles, String iswc, String isrc, String dsp) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		final String text = new StringBuilder()
				.append(null == albumTitle ? "" : albumTitle)
				.append(null == proprietaryId ? "" : proprietaryId) 
				.append(null == title ? "" : title)
				.append(null == artists ? "" : artists)
				.append(null == roles ? "" : roles)
				.append(null == iswc ? "" : iswc)
				.append(null == isrc ? "" : isrc)
				.append(null == dsp ? "" : dsp)
				.toString();
		return Hex.encodeHexString(MessageDigest.getInstance("MD5")
				.digest(text.getBytes("UTF-8")));
	}
	
	private String escape(String text) {
		return text.replace("'", "''")
				.replace("\\", "\\\\")
//				.replace("'", "\\'")
//				.replace("\"", "\\\"")
				.replace("\t", "\\t")
				.replace("\n", "\\n");
	}
	
	private Collection<String> split(String text, String regex, Collection<String> collection) {
		if (!StringTools.isNullOrEmpty(text)) {
			final String[] items = text.split(regex);
			for (String item : items) {
				collection.add(item);
			}
		}
		return collection;
	}

	private String join(Collection<String> collection, String separator) {
		final StringBuilder result = new StringBuilder();
		final Iterator<String> iterator = collection.iterator();
		if (iterator.hasNext()) {
			result.append(iterator.next());
			while (iterator.hasNext()) {
				result.append(separator).append(iterator.next());
			}
		}
		return result.toString();
	}
	
	private String repeat(String text, int count, String separator) {
		final StringBuilder result = new StringBuilder();
		if (count > 0) {
			result.append(text);
			for (int i = 1; i < count; i ++) {
				result.append(separator).append(text);
			}
		}
		return result.toString();
	}
	
	public List<ConflictingCa> learn(LessonCa lessonCa) throws KbException {
		logger.debug("learn: lessonCa {}", lessonCa);
		try (final Statement statement = connection.createStatement()) {
			
			// parse input
			final Set<String> lessonArtistsNames = (Set<String>)
					split(lessonCa.getArtistsNames(), "\\|", new HashSet<String>());
			logger.debug("learn: lessonArtistsNames {}", lessonArtistsNames);
			if (lessonArtistsNames.isEmpty()) {
				throw new KbException(KbException.Error.BAD_REQUEST);
			}

			// select from sophia_ca_opera
			final List<SophiaCaOpera> sophiaCaOperas = new ArrayList<SophiaCaOpera>();
			final String hashTitle = StringTools.isNullOrEmpty(lessonCa.getHashTitle()) ?
					getHashTitle(lessonCa.getTitle()) : lessonCa.getHashTitle();
			logger.debug("learn: hashTitle {}", hashTitle);
			StringBuilder sql = new StringBuilder()
				.append("select codice_opera")
				.append(", artists_names")
				.append(", artists_ipi")
				.append(", black_list_artists")
				.append(", an_origin_vector")
				.append(", ai_origin_vector")
				.append(", an_idutil_src_matrix")
				.append(", ai_idutil_src_matrix")
				.append(", an_iddsr_source_matrix")
				.append(", ai_iddsr_source_matrix")
				.append(", kb_version")
				.append(", valid")
				.append(" from sophia_ca_opera")
				.append(" where hash_title = '").append(hashTitle).append("'");
			logger.debug("learn: executing sql {}", sql);
			try (final ResultSet resultSet = statement.executeQuery(sql.toString())) {
				while (resultSet.next()) {
					int i = 1;
					final SophiaCaOpera sophiaCaOpera = new SophiaCaOpera();
					sophiaCaOpera.setCodiceOpera(resultSet.getString(i++));
					sophiaCaOpera.setTitle(lessonCa.getTitle());
					sophiaCaOpera.setHashTitle(hashTitle);
					sophiaCaOpera.setArtistsNames(resultSet.getString(i++));
					sophiaCaOpera.setArtistsIpi(resultSet.getString(i++));
					sophiaCaOpera.setBlackListArtists(resultSet.getString(i++));
					sophiaCaOpera.setAnOriginVector(resultSet.getString(i++));
					sophiaCaOpera.setAiOriginVector(resultSet.getString(i++));
					sophiaCaOpera.setAnIdutilSrcMatrix(resultSet.getString(i++));
					sophiaCaOpera.setAiIdutilSrcMatrix(resultSet.getString(i++));
					sophiaCaOpera.setAnIddsrSourceMatrix(resultSet.getString(i++));
					sophiaCaOpera.setAiIddsrSourceMatrix(resultSet.getString(i++));
					sophiaCaOpera.setKbVersion(resultSet.getInt(i++));
					sophiaCaOpera.setValid(resultSet.getInt(i++));
					sophiaCaOperas.add(sophiaCaOpera);
				}
			}
			logger.debug("learn: sophiaCaOperas {}", sophiaCaOperas);

			// search for same work code and conflicting artists with different work codes
			final List<ConflictingCa> conflictingCas = new ArrayList<ConflictingCa>();
			SophiaCaOpera sameWorkCode = null;
			for (final SophiaCaOpera sophiaCaOpera : sophiaCaOperas) {
				// build search index for artist names
				final Set<String> artistNamesSearchIndex = (Set<String>)
						split(sophiaCaOpera.getArtistsNames(), "\\|", new HashSet<String>());
				// a row with the same work code exists
				if (StringTools.equals(lessonCa.getCodiceOpera(), sophiaCaOpera.getCodiceOpera(), true)) {
					sameWorkCode = sophiaCaOpera;
					split(sophiaCaOpera.getBlackListArtists(), "\\|", artistNamesSearchIndex); // include black-listed artists in search index
				}
				// search conflicts within index
				final Iterator<String> iteratorLessonArtistsNames = lessonArtistsNames.iterator();
				while (iteratorLessonArtistsNames.hasNext()) {
					final String lessonArtistName = iteratorLessonArtistsNames.next();
					if (artistNamesSearchIndex.contains(lessonArtistName)) { // case sensitive match
						// remove already existing artist name
						iteratorLessonArtistsNames.remove();
						if (sophiaCaOpera != sameWorkCode) {
							// save conflict with different work code
							conflictingCas.add(new ConflictingCa()
									.setSophiaCaOpera(sophiaCaOpera)
									.setLessonCa(lessonCa)
									.setArtistName(lessonArtistName));
						}
					}
				}
			}
			logger.debug("learn: sameWorkCode {}", sameWorkCode);
			logger.debug("learn: lessonArtistsNames {}", lessonArtistsNames);
			
			// no remaining artists name(s)
			if (lessonArtistsNames.isEmpty()) {
				return conflictingCas.isEmpty() ? null : conflictingCas;
			}

			// pipe separated strings
			final String lessonAnOriginVector = repeat(lessonCa
					.getOrigin(), lessonArtistsNames.size(), "|");
			final String lessonAnIdutilSrcMatrix = repeat(lessonCa
					.getIdutilSource() + "|" + kbVersion, lessonArtistsNames.size(), "||");
			final String lessonAnIddsrSourceMatrix = repeat(lessonCa
					.getIddsrSource() + "|" + kbVersion, lessonArtistsNames.size(), "||");
			logger.debug("learn: lessonAnOriginVector {}", lessonAnOriginVector);
			logger.debug("learn: lessonAnIdutilSrcMatrix {}", lessonAnIdutilSrcMatrix);
			logger.debug("learn: lessonAnIddsrSourceMatrix {}", lessonAnIddsrSourceMatrix);

			// work code not found
			if (null == sameWorkCode) {
				// insert into sophia_ca_opera
				sql = new StringBuilder()
					.append("insert into sophia_ca_opera")
					.append("(codice_opera")
					.append(", title")
					.append(", hash_title")
					.append(", artists_names")
//					.append(", artists_ipi")
//					.append(", black_list_artists")
					.append(", an_origin_vector")
//					.append(", ai_origin_vector")
					.append(", an_idutil_src_matrix")
//					.append(", ai_idutil_src_matrix")
					.append(", an_iddsr_source_matrix")
//					.append(", ai_iddsr_source_matrix")
					.append(", kb_version")
					.append(", valid")
					.append(") values")
					.append("('").append(lessonCa.getCodiceOpera())
					.append("', '").append(escape(lessonCa.getTitle()))
					.append("', '").append(hashTitle)
					.append("', '").append(escape(join(lessonArtistsNames, "|")))
//					.append("', '")
//					.append("', '")
					.append("', '").append(lessonAnOriginVector)
//					.append("', '")
					.append("', '").append(lessonAnIdutilSrcMatrix)
//					.append("', '")
					.append("', '").append(lessonAnIddsrSourceMatrix)
//					.append("', '")
					.append("', ").append(kbVersion)
					.append(", 1)"); // valid
				logger.debug("learn: executing sql {}", sql);
				final int count = statement.executeUpdate(sql.toString());
				if (1 != count) {
					throw new KbException(KbException.Error.INSERT_CA);
				}
				return conflictingCas.isEmpty() ? null : conflictingCas;
			}

			// work code already exists
			// update sophia_ca_opera
			sql = new StringBuilder()
				.append("update sophia_ca_opera")
				.append(" set artists_names = '")
					.append(escape(sameWorkCode.getArtistsNames() + "|" + join(lessonArtistsNames, "|")))
//				.append("', artists_ipi = '")
				.append("', an_origin_vector = '")
					.append(sameWorkCode.getAnOriginVector() + "|" + lessonAnOriginVector)
//				.append("', ai_origin_vector = '")
				.append("', an_idutil_src_matrix = '")
					.append(sameWorkCode.getAnIdutilSrcMatrix() + "||" + lessonAnIdutilSrcMatrix)
//				.append("', ai_idutil_src_matrix = '")
				.append("', an_iddsr_source_matrix = '")
					.append(sameWorkCode.getAnIddsrSourceMatrix() + "||" + lessonAnIddsrSourceMatrix)
//				.append("', ai_iddsr_source_matrix = '")
				.append("', kb_version = ").append(kbVersion)
				.append(" where codice_opera = '").append(lessonCa.getCodiceOpera())
				.append("' and hash_title = '").append(hashTitle).append("'");
			logger.debug("learn: executing sql {}", sql);
			final int count = statement.executeUpdate(sql.toString());
			if (count < 1) {
				throw new KbException(KbException.Error.UPDATE_CA);
			}
			return conflictingCas.isEmpty() ? null : conflictingCas;
			
		} catch (Exception e) {
			throw new KbException(KbException.Error.GENERIC, e);
		}
	}

	public ConflictingIswc learn(LessonIswc lessonIswc) throws KbException {
		logger.debug("learn: lessonIswc {}", lessonIswc);
		try (final Statement statement = connection.createStatement()) {
			
			// select from sophia_iswc_opera
			final List<SophiaIswcOpera> sophiaIswcOperas = new ArrayList<SophiaIswcOpera>();
			StringBuilder sql = new StringBuilder()
				.append("select codice_opera")
				.append(", origin")
				.append(", idutil_source")
				.append(", iddsr_source")
				.append(", kb_version")
				.append(", valid")
				.append(" from sophia_iswc_opera")
				.append(" where iswc = '").append(lessonIswc.getIswc()).append("'");
			logger.debug("learn: executing sql {}", sql);
			try (final ResultSet resultSet = statement.executeQuery(sql.toString())) {
				while (resultSet.next()) {
					int i = 1;
					final SophiaIswcOpera sophiaIswcOpera = new SophiaIswcOpera();
					sophiaIswcOpera.setIswc(lessonIswc.getIswc());
					sophiaIswcOpera.setCodiceOpera(resultSet.getString(i++));
					sophiaIswcOpera.setOrigin(resultSet.getString(i++));
					sophiaIswcOpera.setIdutilSource(resultSet.getString(i++));
					sophiaIswcOpera.setIddsrSource(resultSet.getString(i++));
					sophiaIswcOpera.setKbVersion(resultSet.getInt(i++));
					sophiaIswcOpera.setValid(resultSet.getInt(i++));
					sophiaIswcOperas.add(sophiaIswcOpera);
				}
			}
			logger.debug("learn: sophiaIswcOperas {}", sophiaIswcOperas);
						
			// search for same work code and valid rows with different work codes
			ConflictingIswc conflictingIswc = null;
			int validRowsNumber = 0;
			for (SophiaIswcOpera sophiaIswcOpera : sophiaIswcOperas) {
				// a row with the same work code exists
				if (StringTools.equals(lessonIswc.getCodiceOpera(), sophiaIswcOpera.getCodiceOpera(), true)) {
					return null; // lesson already learned
				}
				// a valid row with different work code exists
				if (1 == sophiaIswcOpera.getValid()) {
					validRowsNumber ++;
					conflictingIswc = new ConflictingIswc()
						.setSophiaIswcOpera(sophiaIswcOpera)
						.setLessonIswc(lessonIswc);
				}
			}
			if (validRowsNumber > 1) {
				logger.error("learn: validRowsNumber {}", validRowsNumber);
			}
			
			// no conflicting iswc(s)
			if (null == conflictingIswc) {
				// insert into sophia_iswc_opera
				sql = new StringBuilder()
					.append("insert into sophia_iswc_opera")
					.append("(iswc")
					.append(", codice_opera")
					.append(", origin")
					.append(", idutil_source")
					.append(", iddsr_source")
					.append(", kb_version")
					.append(", valid")
					.append(") values")
					.append("('").append(lessonIswc.getIswc())
					.append("', '").append(lessonIswc.getCodiceOpera())
					.append("', '").append(lessonIswc.getOrigin())
					.append("', '").append(lessonIswc.getIdutilSource())
					.append("', '").append(lessonIswc.getIddsrSource())
					.append("', ").append(kbVersion)
					.append(", 1)"); // valid
				logger.debug("learn: executing sql {}", sql);
				final int count = statement.executeUpdate(sql.toString());
				if (1 != count) {
					throw new KbException(KbException.Error.INSERT_ISWC);
				}
				return null;
			}
			
			// insert into sophia_iswc_opera
			sql = new StringBuilder()
				.append("insert into sophia_iswc_opera")
				.append("(iswc")
				.append(", codice_opera")
				.append(", origin")
				.append(", idutil_source")
				.append(", iddsr_source")
				.append(", kb_version")
				.append(", valid")
				.append(") values")
				.append("('").append(lessonIswc.getIswc())
				.append("', '").append(lessonIswc.getCodiceOpera())
				.append("', '").append(lessonIswc.getOrigin())
				.append("', '").append(lessonIswc.getIdutilSource())
				.append("', '").append(lessonIswc.getIddsrSource())
				.append("', ").append(kbVersion)
				.append(", 0)"); // not valid
			logger.debug("learn: executing sql {}", sql);
			final int count = statement.executeUpdate(sql.toString());
			if (1 != count) {
				throw new KbException(KbException.Error.INSERT_ISWC);
			}
			
			// return conflicting iswc
			return conflictingIswc;

		} catch (Exception e) {
			throw new KbException(KbException.Error.GENERIC, e);
		}
	}

	public ConflictingIsrc learn(LessonIsrc lessonIsrc) throws KbException {
		logger.debug("learn: lessonIsrc {}", lessonIsrc);
		try (final Statement statement = connection.createStatement()) {
			
			// select from sophia_isrc_opera
			final List<SophiaIsrcOpera> sophiaIsrcOperas = new ArrayList<SophiaIsrcOpera>();
			StringBuilder sql = new StringBuilder()
				.append("select codice_opera")
				.append(", origin")
				.append(", idutil_source")
				.append(", iddsr_source")
				.append(", kb_version")
				.append(", valid")
				.append(" from sophia_isrc_opera")
				.append(" where isrc = '").append(lessonIsrc.getIsrc()).append("'");
			logger.debug("learn: executing sql {}", sql);
			try (final ResultSet resultSet = statement.executeQuery(sql.toString())) {
				while (resultSet.next()) {
					int i = 1;
					final SophiaIsrcOpera sophiaIsrcOpera = new SophiaIsrcOpera();
					sophiaIsrcOpera.setIsrc(lessonIsrc.getIsrc());
					sophiaIsrcOpera.setCodiceOpera(resultSet.getString(i++));
					sophiaIsrcOpera.setOrigin(resultSet.getString(i++));
					sophiaIsrcOpera.setIdutilSource(resultSet.getString(i++));
					sophiaIsrcOpera.setIddsrSource(resultSet.getString(i++));
					sophiaIsrcOpera.setKbVersion(resultSet.getInt(i++));
					sophiaIsrcOpera.setValid(resultSet.getInt(i++));
					sophiaIsrcOperas.add(sophiaIsrcOpera);
				}
			}
			logger.debug("learn: sophiaIsrcOperas {}", sophiaIsrcOperas);
						
			// search for same work code and valid rows with different work codes
			ConflictingIsrc conflictingIsrc = null;
			int validRowsNumber = 0;
			for (SophiaIsrcOpera sophiaIsrcOpera : sophiaIsrcOperas) {
				// a row with the same work code exists
				if (StringTools.equals(lessonIsrc.getCodiceOpera(), sophiaIsrcOpera.getCodiceOpera(), true)) {
					return null; // lesson already learned
				}
				// a valid row with different work code exists
				if (1 == sophiaIsrcOpera.getValid()) {
					validRowsNumber ++;
					conflictingIsrc = new ConflictingIsrc()
						.setSophiaIsrcOpera(sophiaIsrcOpera)
						.setLessonIsrc(lessonIsrc);
				}
			}
			if (validRowsNumber > 1) {
				logger.error("learn: validRowsNumber {}", validRowsNumber);
			}
			
			// no conflicting isrc(s)
			if (null == conflictingIsrc) {
				// insert into sophia_isrc_opera
				sql = new StringBuilder()
					.append("insert into sophia_isrc_opera")
					.append("(isrc")
					.append(", codice_opera")
					.append(", origin")
					.append(", idutil_source")
					.append(", iddsr_source")
					.append(", kb_version")
					.append(", valid")
					.append(") values")
					.append("('").append(lessonIsrc.getIsrc())
					.append("', '").append(lessonIsrc.getCodiceOpera())
					.append("', '").append(lessonIsrc.getOrigin())
					.append("', '").append(lessonIsrc.getIdutilSource())
					.append("', '").append(lessonIsrc.getIddsrSource())
					.append("', ").append(kbVersion)
					.append(", 1)"); // valid
				logger.debug("learn: executing sql {}", sql);
				final int count = statement.executeUpdate(sql.toString());
				if (1 != count) {
					throw new KbException(KbException.Error.INSERT_ISRC);
				}
				return null;
			}
			
			// insert into sophia_isrc_opera
			sql = new StringBuilder()
				.append("insert into sophia_isrc_opera")
				.append("(isrc")
				.append(", codice_opera")
				.append(", origin")
				.append(", idutil_source")
				.append(", iddsr_source")
				.append(", kb_version")
				.append(", valid")
				.append(") values")
				.append("('").append(lessonIsrc.getIsrc())
				.append("', '").append(lessonIsrc.getCodiceOpera())
				.append("', '").append(lessonIsrc.getOrigin())
				.append("', '").append(lessonIsrc.getIdutilSource())
				.append("', '").append(lessonIsrc.getIddsrSource())
				.append("', ").append(kbVersion)
				.append(", 0)"); // not valid
			logger.debug("learn: executing sql {}", sql);
			final int count = statement.executeUpdate(sql.toString());
			if (1 != count) {
				throw new KbException(KbException.Error.INSERT_ISRC);
			}
			
			// return conflicting isrc
			return conflictingIsrc;
	
		} catch (Exception e) {
			throw new KbException(KbException.Error.GENERIC, e);
		}
	}

	public ConflictingIdsong learn(LessonIdsong lessonIdsong) throws KbException {
		logger.debug("learn: lessonIdsong {}", lessonIdsong);
		try (final Statement statement = connection.createStatement()) {
			
			// select from sophia_idsong_opera
			final List<SophiaIdsongOpera> sophiaIdsongOperas = new ArrayList<SophiaIdsongOpera>();
			StringBuilder sql = new StringBuilder()
				.append("select codice_opera")
				.append(", origin")
				.append(", idutil_source")
				.append(", iddsr_source")
				.append(", iddsp_source")
				.append(", kb_version")
				.append(", valid")
				.append(" from sophia_idsong_opera")
				.append(" where idsong_sophia = '").append(lessonIdsong.getIdsongSophia()).append("'");
			logger.debug("learn: executing sql {}", sql);
			try (final ResultSet resultSet = statement.executeQuery(sql.toString())) {
				while (resultSet.next()) {
					int i = 1;
					final SophiaIdsongOpera sophiaIdsongOpera = new SophiaIdsongOpera();
					sophiaIdsongOpera.setIdsongSophia(lessonIdsong.getIdsongSophia());
					sophiaIdsongOpera.setCodiceOpera(resultSet.getString(i++));
					sophiaIdsongOpera.setOrigin(resultSet.getString(i++));
					sophiaIdsongOpera.setIdutilSource(resultSet.getString(i++));
					sophiaIdsongOpera.setIddsrSource(resultSet.getString(i++));
					sophiaIdsongOpera.setIddspSource(resultSet.getString(i++));
					sophiaIdsongOpera.setKbVersion(resultSet.getInt(i++));
					sophiaIdsongOpera.setValid(resultSet.getInt(i++));
					sophiaIdsongOperas.add(sophiaIdsongOpera);
				}
			}
			logger.debug("learn: sophiaIdsongOperas {}", sophiaIdsongOperas);
						
			// search for same work code and valid rows with different work codes
			ConflictingIdsong conflictingIdsong = null;
			int validRowsNumber = 0;
			for (SophiaIdsongOpera sophiaIdsongOpera : sophiaIdsongOperas) {
				// a row with the same work code exists
				if (StringTools.equals(lessonIdsong.getCodiceOpera(), sophiaIdsongOpera.getCodiceOpera(), true)) {
					return null; // lesson already learned
				}
				// a valid row with different work code exists
				if (1 == sophiaIdsongOpera.getValid()) {
					validRowsNumber ++;
					conflictingIdsong = new ConflictingIdsong()
						.setSophiaIdsongOpera(sophiaIdsongOpera)
						.setLessonIdsong(lessonIdsong);
				}
			}
			if (validRowsNumber > 1) {
				logger.error("learn: validRowsNumber {}", validRowsNumber);
			}
			
			// no conflicting idsong(s)
			if (null == conflictingIdsong) {
				// insert into sophia_idsong_opera
				sql = new StringBuilder()
					.append("insert into sophia_idsong_opera")
					.append("(idsong_sophia")
					.append(", codice_opera")
					.append(", origin")
					.append(", idutil_source")
					.append(", iddsr_source")
					.append(", iddsp_source")
					.append(", kb_version")
					.append(", valid")
					.append(") values")
					.append("('").append(lessonIdsong.getIdsongSophia())
					.append("', '").append(lessonIdsong.getCodiceOpera())
					.append("', '").append(lessonIdsong.getOrigin())
					.append("', '").append(lessonIdsong.getIdutilSource())
					.append("', '").append(lessonIdsong.getIddsrSource())
					.append("', '").append(lessonIdsong.getIddspSource())
					.append("', ").append(kbVersion)
					.append(", 1)"); // valid
				logger.debug("learn: executing sql {}", sql);
				final int count = statement.executeUpdate(sql.toString());
				if (1 != count) {
					throw new KbException(KbException.Error.INSERT_IDSONG);
				}
				return null;
			}
			
			// insert into sophia_idsong_opera
			sql = new StringBuilder()
				.append("insert into sophia_idsong_opera")
				.append("(idsong_sophia")
				.append(", codice_opera")
				.append(", origin")
				.append(", idutil_source")
				.append(", iddsr_source")
				.append(", iddsp_source")
				.append(", kb_version")
				.append(", valid")
				.append(") values")
				.append("('").append(lessonIdsong.getIdsongSophia())
				.append("', '").append(lessonIdsong.getCodiceOpera())
				.append("', '").append(lessonIdsong.getOrigin())
				.append("', '").append(lessonIdsong.getIdutilSource())
				.append("', '").append(lessonIdsong.getIddsrSource())
				.append("', '").append(lessonIdsong.getIddspSource())
				.append("', ").append(kbVersion)
				.append(", 0)"); // not valid
			logger.debug("learn: executing sql {}", sql);
			final int count = statement.executeUpdate(sql.toString());
			if (1 != count) {
				throw new KbException(KbException.Error.INSERT_IDSONG);
			}
			
			// return conflicting idsong
			return conflictingIdsong;
						
		} catch (Exception e) {
			throw new KbException(KbException.Error.GENERIC, e);
		}
	}

}
