package com.alkemytech.sophia.common.json;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.UUID;

import com.alkemytech.sophia.common.sqs.Iso8601Helper;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class GsonAdapter {

	protected String convert(Throwable e) {
		final StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		return sw.toString();
	}
	
	protected boolean getAsBoolean(JsonObject json, String key, boolean ifNull) {
		final JsonElement element = (null == json ? null : json.get(key));
		return null == element ? ifNull : element.getAsBoolean();
	}

	protected int getAsInt(JsonObject json, String key, int ifNull) {
		final JsonElement element = (null == json ? null : json.get(key));
		return null == element ? ifNull : element.getAsInt();
	}
	
	protected String getAsString(JsonObject json, String key) {
		return getAsString(json, key, null);
	}
	
	protected String getAsString(JsonObject json, String key, String ifNull) {
		final JsonElement element = (null == json ? null : json.get(key));
		final String value = (null == element ? null : element.getAsString());
		return null == value ? ifNull : value;
	}
	
	protected String getHostName() {
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			return "localhost";
		}
	}
	
	protected JsonObject formatErrorJson(String queue, String sender, JsonObject input, String code, String description, Exception e) {
		final JsonObject header = new JsonObject();
		header.addProperty("uuid", UUID.randomUUID().toString());
		header.addProperty("timestamp", Iso8601Helper.format(new Date()));
		header.addProperty("queue", queue);
		header.addProperty("sender", sender);
		header.addProperty("hostname", getHostName());
		final JsonObject error = new JsonObject();
		error.addProperty("code", code);
		error.addProperty("description", description);
		if (null != e) {
			error.addProperty("stackTrace", convert(e));
		}
		final JsonObject message = new JsonObject();
		message.add("header", header);
		message.add("input", input);
		message.add("error", error);
		return message;
	}

	protected JsonObject formatStartedJson(String queue, String sender, JsonObject input) {
		final JsonObject header = new JsonObject();
		header.addProperty("uuid", UUID.randomUUID().toString());
		header.addProperty("timestamp", Iso8601Helper.format(new Date()));
		header.addProperty("queue", queue);
		header.addProperty("sender", sender);
		header.addProperty("hostname", getHostName());
		final JsonObject message = new JsonObject();
		message.add("header", header);
		message.add("input", input);
		return message;
	}
	
	protected JsonObject formatCompletedJson(String queue, String sender, JsonObject input, JsonObject output) {
		final JsonObject header = new JsonObject();
		header.addProperty("uuid", UUID.randomUUID().toString());
		header.addProperty("timestamp", Iso8601Helper.format(new Date()));
		header.addProperty("queue", queue);
		header.addProperty("sender", sender);
		header.addProperty("hostname", getHostName());
		final JsonObject message = new JsonObject();
		message.add("header", header);
		message.add("input", input);
		message.add("output", output);
		return message;
	}
	
}
