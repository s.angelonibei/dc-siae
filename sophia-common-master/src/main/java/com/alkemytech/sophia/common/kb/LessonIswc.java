package com.alkemytech.sophia.common.kb;

import com.google.gson.GsonBuilder;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class LessonIswc {

	private String iswc;
	private String codiceOpera;
	private String origin;
	private String idutilSource;
	private String iddsrSource;

	public String getIswc() {
		return iswc;
	}
	public LessonIswc setIswc(String iswc) {
		this.iswc = iswc;
		return this;
	}
	public String getCodiceOpera() {
		return codiceOpera;
	}
	public LessonIswc setCodiceOpera(String codiceOpera) {
		this.codiceOpera = codiceOpera;
		return this;
	}
	public String getOrigin() {
		return origin;
	}
	public LessonIswc setOrigin(String origin) {
		this.origin = origin;
		return this;
	}
	public String getIdutilSource() {
		return idutilSource;
	}
	public LessonIswc setIdutilSource(String idutilSource) {
		this.idutilSource = idutilSource;
		return this;
	}
	public String getIddsrSource() {
		return iddsrSource;
	}
	public LessonIswc setIddsrSource(String iddsrSource) {
		this.iddsrSource = iddsrSource;
		return this;
	}
	
	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}

}
