package com.alkemytech.sophia.common.tools;

import java.io.File;
import java.net.URI;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.config.Configurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class Log4j2Tools {

	private static final Logger logger = LoggerFactory.getLogger(Log4j2Tools.class);

	public static void initialize(Properties properties) {

		final String log4jConfigFilePath = properties.getProperty("log4j.configuration");
		logger.info("initialize: log4jConfigFilePath [{}]", log4jConfigFilePath);
		if (!StringTools.isNullOrEmpty(log4jConfigFilePath)) {
			final File log4jConfigFile = new File(log4jConfigFilePath);
			logger.info("initialize: log4jConfigFile [{}] {}", log4jConfigFile, log4jConfigFile.length());
			try {
				final org.apache.logging.log4j.core.LoggerContext context = (org
						.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false);
				final URI contextConfigLocation = context.getConfigLocation();
				logger.info("initialize: contextConfigLocation {}", contextConfigLocation);
				final URI log4jConfigUri = log4jConfigFile.toURI();
				logger.info("initialize: log4jConfigUri [{}]", log4jConfigUri);
				if (log4jConfigUri.equals(contextConfigLocation)) {
					logger.info("initialize: log4j context already initialized");
					return;
				}
			} catch (Throwable e) {
				logger.error("initialize", e);
			}
			if (null == Configurator.initialize(null, log4jConfigFilePath)) {
				try {
					final org.apache.logging.log4j.core.LoggerContext context = (org
							.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false);
					context.setConfigLocation(log4jConfigFile.toURI());
				} catch (Throwable e) {
					logger.error("initialize", e);
				}
			}
		}
		
	}
	
}
