package com.alkemytech.sophia.common.sqs;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class SQSMessage {

	private String text;
	private String receiptHandle;
	
	public SQSMessage() {
		super();
	}
	
	public SQSMessage(String text, String receiptHandle) {
		super();
		this.text = text;
		this.receiptHandle = receiptHandle;
	}
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getReceiptHandle() {
		return receiptHandle;
	}
	public void setReceiptHandle(String receiptHandle) {
		this.receiptHandle = receiptHandle;
	}
	
	@Override
	public String toString() {
		return text;
	}
	
}
