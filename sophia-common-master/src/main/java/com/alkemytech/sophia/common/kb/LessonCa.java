package com.alkemytech.sophia.common.kb;

import com.google.gson.GsonBuilder;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class LessonCa {

	private String codiceOpera;
	private String title;
	private String hashTitle;
	private String artistsNames; // pipe separated list
	private String artistsIpi; // pipe separated list
	private String blackListArtists; // pipe separated list
	private String origin;
	private String idutilSource;
	private String iddsrSource;
	
	public String getCodiceOpera() {
		return codiceOpera;
	}
	public LessonCa setCodiceOpera(String codiceOpera) {
		this.codiceOpera = codiceOpera;
		return this;
	}
	public String getTitle() {
		return title;
	}
	public LessonCa setTitle(String title) {
		this.title = title;
		return this;
	}
	public String getHashTitle() {
		return hashTitle;
	}
	public LessonCa setHashTitle(String hashTitle) {
		this.hashTitle = hashTitle;
		return this;
	}
	public String getArtistsNames() {
		return artistsNames;
	}
	public LessonCa setArtistsNames(String artistsNames) {
		this.artistsNames = artistsNames;
		return this;
	}
	public String getArtistsIpi() {
		return artistsIpi;
	}
	public LessonCa setArtistsIpi(String artistsIpi) {
		this.artistsIpi = artistsIpi;
		return this;
	}
	public String getBlackListArtists() {
		return blackListArtists;
	}
	public LessonCa setBlackListArtists(String blackListArtists) {
		this.blackListArtists = blackListArtists;
		return this;
	}
	public String getOrigin() {
		return origin;
	}
	public LessonCa setOrigin(String origin) {
		this.origin = origin;
		return this;
	}
	public String getIdutilSource() {
		return idutilSource;
	}
	public LessonCa setIdutilSource(String idutilSource) {
		this.idutilSource = idutilSource;
		return this;
	}
	public String getIddsrSource() {
		return iddsrSource;
	}
	public LessonCa setIddsrSource(String iddsrSource) {
		this.iddsrSource = iddsrSource;
		return this;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}

}
