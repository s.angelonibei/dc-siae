package com.alkemytech.sophia.common.sqs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.common.tools.StringTools;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.auth.PropertiesFileCredentialsProvider;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.DeleteMessageResult;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.QueueDeletedRecentlyException;
import com.amazonaws.services.sqs.model.QueueDoesNotExistException;
import com.amazonaws.services.sqs.model.QueueNameExistsException;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import com.amazonaws.util.StringUtils;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class SQSService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Properties configuration;
	
	private AmazonSQS sqsClient;

	@Inject
	protected SQSService(@Named("configuration") Properties configuration) {
		super();
		this.configuration = configuration;
	}
	
	public SQSService startup() {
		shutdown();
		final String proxyHost = configuration.getProperty("proxy.host");
		final String proxyPort = configuration.getProperty("proxy.port");
	    final ClientConfiguration clientConfiguration = new ClientConfiguration();
	    if (!StringUtils.isNullOrEmpty(proxyHost) && !StringUtils.isNullOrEmpty(proxyPort)) {
	    	clientConfiguration.setProxyHost(proxyHost);
	    	clientConfiguration.setProxyPort(Integer.parseInt(proxyPort));
	    }
		final String region = configuration.getProperty("aws.region", "eu-west-1");
	    logger.debug("startup: region {}", region);
		final String endpoint = configuration.getProperty("sqs.endpoint", "https://sqs.eu-west-1.amazonaws.com");
	    logger.debug("startup: endpoint {}", endpoint);
	    final String credentialsFilePath = configuration.getProperty("aws.credentials");
	    logger.debug("startup: credentialsFilePath {}", credentialsFilePath);
	    final AWSCredentialsProvider credentials = StringTools.isNullOrEmpty(credentialsFilePath) ?
	    		new ClasspathPropertiesFileCredentialsProvider() :
	    			new PropertiesFileCredentialsProvider(credentialsFilePath);
		sqsClient = AmazonSQSClientBuilder.standard()
			.withCredentials(credentials)
			.withClientConfiguration(clientConfiguration)
			.withEndpointConfiguration(new EndpointConfiguration(endpoint, region))
			.build();
		return this;
	}
		
	public SQSService shutdown() {
		if (null != sqsClient) {
			sqsClient.shutdown();
		}
		return this;
	}

	public List<String> getQueueUrls() {
		return sqsClient.listQueues().getQueueUrls();
	}

	public String getUrl(String queueName) {
		
		// get existing queue url
		logger.debug("getUrl: opening queue {}", queueName);
		try {
			return sqsClient
					.getQueueUrl(new GetQueueUrlRequest(queueName))
					.getQueueUrl();
		} catch (QueueDoesNotExistException e) {
			logger.error("getUrl", e);
		}
		
		// error
		return null;
		
	}
	
	public String getOrCreateUrl(String queueName) {
		
		// get existing queue url
		logger.debug("getOrCreateUrl: opening queue {}", queueName);
		try {
			return sqsClient
					.getQueueUrl(new GetQueueUrlRequest(queueName))
					.getQueueUrl();
		} catch (QueueDoesNotExistException e) {
			logger.error("getOrCreateUrl", e);
		}
		
		// create new queue
		logger.debug("getOrCreateUrl: creating queue {}", queueName);
		try {
			final Map<String, String> attributes = new HashMap<String, String>();
			attributes.put("VisibilityTimeout", configuration.getProperty("sqs.visibility_timeout", "300")); // default to 5 minutes
			return sqsClient.createQueue(new CreateQueueRequest()
				.withQueueName(queueName)
				.withAttributes(attributes))
					.getQueueUrl();
		} catch (QueueNameExistsException | QueueDeletedRecentlyException  e) {
			logger.error("getOrCreateUrl", e);
		}
		
		// error
		return null;
		
	}
	
	public boolean sendMessage(String queueUrl, String text) {
		
        final SendMessageRequest request = new SendMessageRequest()
        	.withMessageBody(text)
        	.withQueueUrl(queueUrl);
		final SendMessageResult result = sqsClient.sendMessage(request);
		return null != result && null != result.getSdkHttpMetadata() && 
				200 == result.getSdkHttpMetadata().getHttpStatusCode();
		
	}

	public List<SQSMessage> receiveMessages(String queueUrl) {
		final Integer maxNumberOfMessages = Integer
				.valueOf(configuration.getProperty("sqs.max_number_of_messages", "1"));
		return receiveMessages(queueUrl, maxNumberOfMessages);
	}

	public List<SQSMessage> receiveMessages(String queueUrl, Integer maxNumberOfMessages) {
		
        final ReceiveMessageRequest request = new ReceiveMessageRequest()
        	.withQueueUrl(queueUrl)
        	.withMaxNumberOfMessages(maxNumberOfMessages);
        final List<SQSMessage> result = new ArrayList<SQSMessage>();
        final List<Message> messages = sqsClient.receiveMessage(request).getMessages();        
        if (null != messages) for (Message message : messages) {
        	result.add(new SQSMessage(message.getBody(), message.getReceiptHandle()));
        }
        return result;
        
	}
	
	public boolean deleteMessage(String queueUrl, String receiptHandle) {
		
		final DeleteMessageRequest request = new DeleteMessageRequest()
			.withQueueUrl(queueUrl)
			.withReceiptHandle(receiptHandle);
        final DeleteMessageResult result = sqsClient.deleteMessage(request);
		return null != result && null != result.getSdkHttpMetadata() && 
				200 == result.getSdkHttpMetadata().getHttpStatusCode();

	}
	
}
