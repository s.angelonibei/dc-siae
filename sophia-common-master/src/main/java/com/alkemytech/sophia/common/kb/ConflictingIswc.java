package com.alkemytech.sophia.common.kb;

import com.google.gson.GsonBuilder;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class ConflictingIswc {

	private SophiaIswcOpera sophiaIswcOpera;
	private LessonIswc lessonIswc;
	
	public SophiaIswcOpera getSophiaIswcOpera() {
		return sophiaIswcOpera;
	}
	public ConflictingIswc setSophiaIswcOpera(SophiaIswcOpera sophiaIswcOpera) {
		this.sophiaIswcOpera = sophiaIswcOpera;
		return this;
	}

	public LessonIswc getLessonIswc() {
		return lessonIswc;
	}
	public ConflictingIswc setLessonIswc(LessonIswc lessonIswc) {
		this.lessonIswc = lessonIswc;
		return this;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}

}
