package com.alkemytech.sophia.common.kb;

import com.google.gson.GsonBuilder;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class SophiaCaOpera {

	private String codiceOpera;
	private String title;
	private String hashTitle;
	private String artistsNames; // pipe separated list
	private String artistsIpi; // pipe separated list
	private String blackListArtists; // pipe separated list
	private String anOriginVector; // pipe separated list
	private String aiOriginVector; // pipe separated list
	private String anIdutilSrcMatrix; // double-pipe separated list
	private String aiIdutilSrcMatrix; // double-pipe separated list
	private String anIddsrSourceMatrix; // double-pipe separated list
	private String aiIddsrSourceMatrix; // double-pipe separated list
	private int kbVersion;
	private int valid;
	
	public String getCodiceOpera() {
		return codiceOpera;
	}
	public void setCodiceOpera(String codiceOpera) {
		this.codiceOpera = codiceOpera;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getHashTitle() {
		return hashTitle;
	}
	public void setHashTitle(String hashTitle) {
		this.hashTitle = hashTitle;
	}
	public String getArtistsNames() {
		return artistsNames;
	}
	public void setArtistsNames(String artistsNames) {
		this.artistsNames = artistsNames;
	}
	public String getArtistsIpi() {
		return artistsIpi;
	}
	public void setArtistsIpi(String artistsIpi) {
		this.artistsIpi = artistsIpi;
	}
	public String getBlackListArtists() {
		return blackListArtists;
	}
	public void setBlackListArtists(String blackListArtists) {
		this.blackListArtists = blackListArtists;
	}
	public String getAnOriginVector() {
		return anOriginVector;
	}
	public void setAnOriginVector(String anOriginVector) {
		this.anOriginVector = anOriginVector;
	}
	public String getAiOriginVector() {
		return aiOriginVector;
	}
	public void setAiOriginVector(String aiOriginVector) {
		this.aiOriginVector = aiOriginVector;
	}
	public String getAnIdutilSrcMatrix() {
		return anIdutilSrcMatrix;
	}
	public void setAnIdutilSrcMatrix(String anIdutilSrcMatrix) {
		this.anIdutilSrcMatrix = anIdutilSrcMatrix;
	}
	public String getAiIdutilSrcMatrix() {
		return aiIdutilSrcMatrix;
	}
	public void setAiIdutilSrcMatrix(String aiIdutilSrcMatrix) {
		this.aiIdutilSrcMatrix = aiIdutilSrcMatrix;
	}
	public String getAnIddsrSourceMatrix() {
		return anIddsrSourceMatrix;
	}
	public void setAnIddsrSourceMatrix(String anIddsrSourceMatrix) {
		this.anIddsrSourceMatrix = anIddsrSourceMatrix;
	}
	public String getAiIddsrSourceMatrix() {
		return aiIddsrSourceMatrix;
	}
	public void setAiIddsrSourceMatrix(String aiIddsrSourceMatrix) {
		this.aiIddsrSourceMatrix = aiIddsrSourceMatrix;
	}
	public int getKbVersion() {
		return kbVersion;
	}
	public void setKbVersion(int kbVersion) {
		this.kbVersion = kbVersion;
	}
	public int getValid() {
		return valid;
	}
	public void setValid(int valid) {
		this.valid = valid;
	}
	
	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}

}
