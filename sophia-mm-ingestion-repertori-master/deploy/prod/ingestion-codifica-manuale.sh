#!/bin/sh

HOME=/home/orchestrator/sophia/ingestion-kb-mm

JPACKAGE=com.alkemytech.sophia.mm.ingestion.repertori
JCLASS=IngestionCodificaManuale
JCONFIG=ingestion-codifica-manuale.properties

JSTDOUT=/dev/null
JOPTIONS="-Ddefault.home_folder=$HOME"
JVERSION=1.0.0


nohup java -cp "$HOME/sophia-mm-ingestion-repertori-$JVERSION.jar:$HOME/lib/*" $JOPTIONS $JPACKAGE.$JCLASS $HOME/$JCONFIG 2>&1 >> $JSTDOUT &
