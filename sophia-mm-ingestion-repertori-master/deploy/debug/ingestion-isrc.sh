#!/bin/sh

HOME=/home/orchestrator/sophia/ingestion-kb

JPACKAGE=com.alkemytech.sophia.mm.ingestion.repertori
JCLASS=IngestionIsrc
JCONFIG=ingestion-isrc.properties

JSTDOUT=/dev/null
JOPTIONS="-Ddefault.home_folder=$HOME"
JVERSION=1.0.0


nohup /opt/java8/bin/java -cp "$HOME/sophia-mm-ingestion-repertori-$JVERSION-jar-with-dependencies.jar:$HOME/lib/*" $JOPTIONS $JPACKAGE.$JCLASS $HOME/$JCONFIG 2>&1 >> $JSTDOUT &
