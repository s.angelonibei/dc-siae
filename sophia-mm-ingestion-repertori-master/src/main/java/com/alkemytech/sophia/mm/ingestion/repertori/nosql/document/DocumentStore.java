package com.alkemytech.sophia.mm.ingestion.repertori.nosql.document;

import com.alkemytech.sophia.mm.ingestion.repertori.nosql.model.Document;

import java.io.IOException;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface DocumentStore {
	
	public DocumentStore startup() throws IOException;
	public DocumentStore shutdown();
	public boolean isReadOnly();
	public Document getById(long id) throws IOException;
	public Document upsert(Document document) throws IOException;
	public DocumentStore select(Document.Selector selector) throws Exception;
	public DocumentStore truncate() throws IOException;
	public DocumentStore defrag() throws IOException;

}
