package com.alkemytech.sophia.mm.ingestion.repertori.orchestrator;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.mm.ingestion.repertori.jdbc.McmdbDAO;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.gson.JsonObject;
import com.google.inject.name.Named;

import javax.inject.Inject;
import java.util.Comparator;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IndexUpdater extends UpdateKbProcessHelper {

    public static final String PROCESS_NAME = "index_updater";

    @Inject
    public IndexUpdater(@Named("configuration") Properties configuration, S3 s3, SQS sqs, McmdbDAO mcmdbDAO) {
        super(s3, sqs, configuration, mcmdbDAO);
    }

    @Override
    public String getProcessName() {
        return PROCESS_NAME;
    }

    @Override
    public boolean updateNeeded() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isEnabled() {
        return Boolean.parseBoolean(configuration.getProperty(PROCESS_NAME + ".enabled"));
    }

    @Override
    public JsonObject getToProcessMessage(String launchId) {

        final String kbS3KeyRegex = configuration.getProperty("ingestion_orchestrator.kb_s3_key_regex"); //mm_kb/([0-9]{4}_[0-9]{2}_[0-9]{2})/[0-9]{4}_[0-9]{2}_[0-9]{2}_export_mm_kb_[0-9]{3}\.json\.gz
        final String kbS3BasePath = configuration.getProperty("ingestion_orchestrator.kb_s3_base_path"); //s3://siae-sophia-datalake/env/mm_kb/

        Pattern folderRegEx = Pattern.compile(kbS3KeyRegex);
        String lastExported = s3.listObjects(new S3.Url(kbS3BasePath)).stream()
                .map(S3ObjectSummary::getKey)
                .map(folderRegEx::matcher)
                .filter(Matcher::matches)
                .map(matcher -> matcher.group(1))
                .max(Comparator.naturalOrder())
                .orElseThrow(() -> new RuntimeException("kb export not found"));

        JsonObject body = super.getToProcessMessage(launchId);
        body.addProperty("force", "true");
        body.addProperty("dump", lastExported);

        return body;
    }
}
