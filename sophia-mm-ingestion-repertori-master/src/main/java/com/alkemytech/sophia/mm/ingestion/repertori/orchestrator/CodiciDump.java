package com.alkemytech.sophia.mm.ingestion.repertori.orchestrator;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.mm.ingestion.repertori.jdbc.McmdbDAO;
import com.google.gson.JsonObject;
import com.google.inject.name.Named;

import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Properties;

public class CodiciDump extends UpdateKbProcessHelper {

    public static final String PROCESS_NAME = "codici_dump";

    @Inject
    public CodiciDump(@Named("configuration") Properties configuration, S3 s3, SQS sqs, McmdbDAO mcmdbDAO) {
        super(s3, sqs, configuration, mcmdbDAO);
    }

    @Override
    public String getProcessName() {
        return PROCESS_NAME;
    }

    @Override
    public boolean updateNeeded() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isEnabled() {
        return Boolean.parseBoolean(configuration.getProperty(PROCESS_NAME + ".enabled"));
    }

    @Override
    public JsonObject getToProcessMessage(String launchId) {
        try (ByteArrayOutputStream kbMetadataBaos = new ByteArrayOutputStream()) {

            s3.download(new S3.Url(configuration.getProperty("ingestion_orchestrator.mm_kb.metadata.s3_path")), kbMetadataBaos);
            Properties kbMetadata = new Properties();
            kbMetadata.load(new ByteArrayInputStream(kbMetadataBaos.toByteArray()));

            JsonObject body = super.getToProcessMessage(launchId);
            body.addProperty("jsonUrl", kbMetadata.getProperty("codici_dump.latest"));
            return body;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }


}
