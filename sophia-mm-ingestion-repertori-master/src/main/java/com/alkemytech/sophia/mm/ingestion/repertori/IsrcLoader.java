package com.alkemytech.sophia.mm.ingestion.repertori;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.mm.ingestion.repertori.jdbc.DSLContextProvider;
import com.alkemytech.sophia.mm.ingestion.repertori.jdbc.McmdbDAO;
import com.alkemytech.sophia.mm.ingestion.repertori.jdbc.McmdbDataSource;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.jooq.DSLContext;
import org.jooq.InsertQuery;
import org.jooq.Record;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.*;
import java.net.ServerSocket;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;

public class IsrcLoader {

    private static final Logger logger = LoggerFactory.getLogger(IsrcLoader.class);

    private static class GuiceModuleExtension extends GuiceModule {

        public GuiceModuleExtension(String[] args, String resourceName) {
            super(args, resourceName);
        }

        @Override
        protected void configure() {
            super.configure();
            // amazon service(s)
            bind(S3.class)
                    .toInstance(new S3(configuration));

            // data source(s)
            bind(DataSource.class)
                    .to(McmdbDataSource.class)
                    .asEagerSingleton();

            bind(DSLContext.class)
                    .toProvider(DSLContextProvider.class)
                    .asEagerSingleton();

            // self
            bind(IsrcLoader.class)
                    .asEagerSingleton();
        }

    }

    public static void main(String[] args) {
        try {
            final IsrcLoader instance = Guice
                    .createInjector(new GuiceModuleExtension(args,
                            "/isrc-loader.properties"))
                    .getInstance(IsrcLoader.class)
                    .startup();
            try {
                instance.process(args);
            } finally {
                instance.shutdown();
            }
        } catch (Throwable e) {
            logger.error("main", e);
        } finally {
            System.exit(0);
        }
    }

    private final Properties configuration;
    private final S3 s3;
    private final DSLContext dslContext;

    @Inject
    protected IsrcLoader(@Named("configuration") Properties configuration,
                         S3 s3, DSLContextProvider dslContextProvider) {
        this.configuration = configuration;
        this.s3 = s3;
        this.dslContext = dslContextProvider.get();
    }

    public IsrcLoader startup() {
        s3.startup();
        return this;
    }

    public IsrcLoader shutdown() {
        s3.shutdown();
        return this;
    }

    private IsrcLoader process(String[] args) throws Exception {

        final int bindPort = Integer.parseInt(configuration
                .getProperty("isrc_loader.bind_port", configuration
                        .getProperty("default.bind_port", "0")));

        // bind lock tcp port
        try (final ServerSocket socket = new ServerSocket(bindPort)) {
            logger.info("socket bound to {}", socket.getLocalSocketAddress());

            process();

        }

        return this;
    }

    private void process() throws Exception {
        String workingDirectory = configuration.getProperty("isrc_loader.working_directory");
        String isrcRegex = configuration.getProperty("isrc_loader.csv.isrc_regex");
        String csvPathS3 = configuration.getProperty("isrc_loader.s3.csv_path");
        Pattern isrcPattern = Pattern.compile(isrcRegex);

        List<S3ObjectSummary> fileToLoad = s3.listObjects(new S3.Url(csvPathS3), true);
        for (S3ObjectSummary objectSummary : fileToLoad) {
            File tmp = File.createTempFile("__tmp", Long.toString(System.currentTimeMillis()), new File(workingDirectory));
            tmp.deleteOnExit();

            S3.Url s3url = new S3.Url(objectSummary.getBucketName(), objectSummary.getKey());
            try (InputStream fileStream = new FileInputStream(tmp);
                 Reader reader = new InputStreamReader(fileStream, StandardCharsets.UTF_8)) {

                logger.info("downloading {}", s3url);
                s3.download(s3url, tmp);

                CSVParser csvParser = new CSVParser(reader, CSVFormat.newFormat(';'));
                final InsertQuery<Record> insert = dslContext.insertQuery(table("MM_KB_INGESTION_ISRC"));

                for (CSVRecord csvRecord : csvParser) {
                    String isrc = csvRecord.get(0);
                    String uuid = csvRecord.get(1);

                    Matcher matcher = isrcPattern.matcher(isrc);
                    if (matcher.matches()) {
                        insert.addValue(field("ISRC"), matcher.group(1));
                        insert.addValue(field("UUID"), uuid);
                        insert.newRecord();
                    }

                }

                insert.onDuplicateKeyIgnore(true);
                int inserted = insert.execute();

                logger.info("inserted {} rows in MM_KB_INGESTION_ISRC", inserted);
            }

            logger.info("deleting {}", s3url);
            s3.delete(s3url);

        }


    }

}

