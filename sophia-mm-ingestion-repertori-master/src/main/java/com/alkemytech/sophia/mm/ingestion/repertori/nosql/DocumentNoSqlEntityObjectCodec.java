package com.alkemytech.sophia.mm.ingestion.repertori.nosql;

import com.alkemytech.sophia.commons.mmap.MemoryMapException;
import com.alkemytech.sophia.commons.mmap.ObjectCodec;

import java.nio.charset.Charset;

public class DocumentNoSqlEntityObjectCodec extends ObjectCodec<DocumentNoSqlEntity> {

    private final Charset charset;

    public DocumentNoSqlEntityObjectCodec(Charset charset) {
        super();
        this.charset = charset;
    }

    @Override
    public DocumentNoSqlEntity bytesToObject(byte[] bytes) throws MemoryMapException {
        beginDecoding(bytes);
        return new DocumentNoSqlEntity(getLong(), getString(charset));
    }

    @Override
    public byte[] objectToBytes(DocumentNoSqlEntity object) throws MemoryMapException {
        beginEncoding();
        putLong(object.id);
        putString(object.json, charset);
        return commitEncoding();
    }
}
