package com.alkemytech.sophia.mm.ingestion.repertori.nosql;

import com.alkemytech.sophia.commons.mmap.ObjectCodec;
import com.alkemytech.sophia.commons.nosql.ConcurrentNoSql;
import com.alkemytech.sophia.commons.nosql.NoSql;
import com.alkemytech.sophia.commons.nosql.NoSqlConfig;
import com.alkemytech.sophia.commons.trie.ConcurrentTrieEx;
import com.alkemytech.sophia.commons.util.FileUtils;
import com.alkemytech.sophia.commons.util.TextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class SupportNoSqlDb {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private static class ThreadLocalObjectCodec extends ThreadLocal<ObjectCodec<SupportNoSqlEntity>> {

        private final Charset charset;

        public ThreadLocalObjectCodec(Charset charset) {
            super();
            this.charset = charset;
        }

        @Override
        protected synchronized ObjectCodec<SupportNoSqlEntity> initialValue() {
            return new SupportNoSqlEntityObjectCodec(charset);
        }

    }

    private NoSql<SupportNoSqlEntity> database;

    public SupportNoSqlDb(Properties configuration, String propertyPrefix, String homeFolderPath) {
        super();
        final File homeFolder = new File(homeFolderPath);
        final boolean readOnly = "true".equalsIgnoreCase(configuration
                .getProperty(propertyPrefix + ".read_only", "true"));
        final int pageSize = TextUtils.parseIntSize(configuration
                .getProperty(propertyPrefix + ".page_size", "-1"));
        final int cacheSize = TextUtils.parseIntSize(configuration
                .getProperty(propertyPrefix + ".cache_size", "0"));
        final boolean createAlways = "true".equalsIgnoreCase(configuration
                .getProperty(propertyPrefix + ".create_always"));
        final String trieClassName = configuration
                .getProperty(propertyPrefix + ".trie_class_name",
                        ConcurrentTrieEx.class.getName());
        if (!readOnly && createAlways && homeFolder.exists()) {
            FileUtils.deleteFolder(homeFolder, true);
        }
        logger.debug("propertyPrefix {}", propertyPrefix);


        database = new ConcurrentNoSql<>(new NoSqlConfig<SupportNoSqlEntity>()
                .setHomeFolder(homeFolder)
                .setReadOnly(readOnly)
                .setPageSize(pageSize)
                .setCacheSize(cacheSize)
                .setCodec(new ThreadLocalObjectCodec(StandardCharsets.UTF_8))
                .setTrieClassName(trieClassName)
                .setSecondaryIndex(false));
    }

    public File getHomeFolder() {
        return database.getHomeFolder();
    }

    public void truncate() {
        database.truncate();
    }

    public void defrag() {
        database.defrag();
    }

    public SupportNoSqlEntity get(String pk) {
        return database.getByPrimaryKey(pk);
    }

    public boolean put(SupportNoSqlEntity bean) {
        database.upsert(bean);
        return true;
    }

    public void select(NoSql.Selector<SupportNoSqlEntity> selector) {
        database.select(selector);
    }

    public long count() {
        return database.count();
    }

    public boolean deleteByPrimaryKey(String id) {
        return database.deleteByPrimaryKey(id);
    }
}
