package com.alkemytech.sophia.mm.ingestion.repertori.nosql.document;

import com.alkemytech.sophia.commons.mmap.ObjectCodec;
import com.alkemytech.sophia.commons.nosql.ConcurrentNoSql;
import com.alkemytech.sophia.commons.nosql.NoSql;
import com.alkemytech.sophia.commons.nosql.NoSqlConfig;
import com.alkemytech.sophia.commons.nosql.NoSqlException;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.mm.ingestion.repertori.nosql.model.Document;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class NoSqlDocumentStore implements DocumentStore {

	private static class ThreadLocalObjectCodec extends ThreadLocal<ObjectCodec<Document>> {

		private final Charset charset;
		
	    public ThreadLocalObjectCodec(Charset charset) {
			super();
			this.charset = charset;
		}

		@Override
	    protected synchronized ObjectCodec<Document> initialValue() {
	        return new DocumentCodec(charset);
	    }
	    
	}

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final AtomicInteger startupCalls;
	private final NoSqlConfig<Document> config;
	
	private NoSql<Document> database;

	@Inject
	public NoSqlDocumentStore(@Named("configuration") Properties configuration,
			@Named("document_store") String propertyPrefix) {
		super();
		logger.debug("propertyPrefix: {}", propertyPrefix);
		this.startupCalls = new AtomicInteger(0);
		this.config = new NoSqlConfig<Document>()
			.setHomeFolder(new File(configuration
					.getProperty(propertyPrefix + ".home_folder")))
			.setReadOnly("true".equalsIgnoreCase(configuration
					.getProperty(propertyPrefix + ".read_only", "true")))
			.setPageSize(TextUtils.parseIntSize(configuration
					.getProperty(propertyPrefix + ".page_size", "-1")))
			.setCacheSize(TextUtils.parseIntSize(configuration
					.getProperty(propertyPrefix + ".cache_size", "0")))
			.setCodec(new ThreadLocalObjectCodec(Charset.forName("UTF-8")));
	}

	@Override
	public NoSqlDocumentStore startup() throws IOException {
		if (0 == startupCalls.getAndIncrement()) {
			database = new ConcurrentNoSql<>(config);
		}
		return this;
	}

	@Override
	public NoSqlDocumentStore shutdown() {
		if (0 == startupCalls.decrementAndGet()) {
			try {
				database.close();
			} catch (IOException e) {
				logger.error("close", e);
			}
		}
		return this;
	}

	@Override
	public boolean isReadOnly() {
		return database.isReadOnly();
	}
	
	@Override
	public Document getById(long id) {
		return database.getByPrimaryKey(Long.toString(id));
	}

	@Override
	public Document upsert(Document document) {
		return database.upsert(document);
	}
	
	@Override
	public NoSqlDocumentStore select(final Document.Selector selector) {
		database.select(new NoSql.Selector<Document>() {
			@Override
			public void select(Document document) {
				try {
					selector.select(document);
				} catch (Exception e) {
					throw new NoSqlException(e);
				}
			}
		});
		return this;
	}

	@Override
	public NoSqlDocumentStore truncate() {
		database.truncate();
		return this;
	}

	@Override
	public NoSqlDocumentStore defrag() {
		database.defrag();
		return this;
	}
	
}
