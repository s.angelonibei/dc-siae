package com.alkemytech.sophia.mm.ingestion.repertori.orchestrator;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.mm.ingestion.repertori.jdbc.McmdbDAO;
import com.google.gson.JsonObject;
import com.google.inject.name.Named;

import javax.inject.Inject;
import java.io.*;
import java.sql.SQLException;
import java.util.Date;
import java.util.Properties;

public class IngestionUcmrAda extends UpdateKbProcessHelper {

    public static final String PROCESS_NAME = "ingestion_ucmr_ada";

    @Inject
    public IngestionUcmrAda(@Named("configuration") Properties configuration, S3 s3, SQS sqs, McmdbDAO mcmdbDAO) {
        super(s3, sqs, configuration, mcmdbDAO);
    }

    @Override
    public String getProcessName() {
        return PROCESS_NAME;
    }

    @Override
    public boolean updateNeeded() {
        try (ByteArrayOutputStream metadataKbBaos = new ByteArrayOutputStream();
             ByteArrayOutputStream latestUcmrAdaBaos = new ByteArrayOutputStream()) {
            s3.download(new S3.Url(configuration.getProperty("ingestion_orchestrator.mm_kb.metadata.s3_path")), metadataKbBaos);
            Properties kbMetadata = new Properties();
            kbMetadata.load(new ByteArrayInputStream(metadataKbBaos.toByteArray()));

            s3.download(new S3.Url(configuration.getProperty("ingestion_orchestrator.ucmr_ada.metadata.s3_path")), latestUcmrAdaBaos);
            Properties ucmrAdaMetadata = new Properties();
            ucmrAdaMetadata.load(new ByteArrayInputStream(latestUcmrAdaBaos.toByteArray()));

            String ucmrAdaCsvLocation = ucmrAdaMetadata.getProperty("location");

            return !kbMetadata.getProperty("ucmr_ada.latest", "").equals(ucmrAdaCsvLocation);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean isEnabled() {
        return Boolean.parseBoolean(configuration.getProperty(PROCESS_NAME + ".enabled"));
    }

    @Override
    public JsonObject onCompletedMessage(JsonObject message) throws SQLException {
        JsonObject output = GsonUtils.getAsJsonObject(message, "output");
        String updatedKb = GsonUtils.getAsString(output, "updatedKb");
        String codici = GsonUtils.getAsString(output, "codici");
        String ucmrAdaDocumentation = GsonUtils.getAsString(output, "ucmr_ada_documentation");
        try (ByteArrayOutputStream metadataBaos = new ByteArrayOutputStream()) {
            String kbMetadatS3Path = configuration.getProperty("ingestion_orchestrator.mm_kb.metadata.s3_path");
            s3.download(new S3.Url(kbMetadatS3Path), metadataBaos);
            Properties kbMetadata = new Properties();
            kbMetadata.load(new ByteArrayInputStream(metadataBaos.toByteArray()));
            kbMetadata.put("kb.latest", updatedKb);
            kbMetadata.put("codici_dump.latest", codici);
            kbMetadata.put("ucmr_ada.latest", ucmrAdaDocumentation);


            File latestVersion = File.createTempFile("__tmp_", System.currentTimeMillis() + "_latest_version",
                    new File(configuration.getProperty("ingestion_orchestrator.working_directory")));
            latestVersion.deleteOnExit();
            FileOutputStream fos = new FileOutputStream(latestVersion);
            kbMetadata.store(fos, new Date().toString());

            s3.upload(new S3.Url(kbMetadatS3Path), latestVersion);

            return super.onCompletedMessage(message);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
