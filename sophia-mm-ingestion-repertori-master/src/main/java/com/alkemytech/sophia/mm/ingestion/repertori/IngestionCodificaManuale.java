package com.alkemytech.sophia.mm.ingestion.repertori;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.io.CompressionAwareFileOutputStream;
import com.alkemytech.sophia.commons.sqs.McmdbMessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.mm.ingestion.repertori.jdbc.McmdbDataSource;
import com.alkemytech.sophia.mm.ingestion.repertori.jdbc.UnidentifiedDataSource;
import com.alkemytech.sophia.mm.ingestion.repertori.nosql.SupportNoSqlDb;
import com.alkemytech.sophia.mm.ingestion.repertori.nosql.SupportNoSqlEntity;
import com.alkemytech.sophia.mm.ingestion.repertori.util.GsonCollectors;
import com.alkemytech.sophia.mm.ingestion.repertori.util.IngestionRepertoriUtils;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.*;
import java.net.ServerSocket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class IngestionCodificaManuale {

    private static final Logger logger = LoggerFactory.getLogger(IngestionCodificaManuale.class);

    private static class GuiceModuleExtension extends GuiceModule {

        public GuiceModuleExtension(String[] args, String resourceName) {
            super(args, resourceName);
        }

        @Override
        protected void configure() {
            super.configure();
            // amazon service(s)
            bind(S3.class)
                    .toInstance(new S3(configuration));
            bind(SQS.class)
                    .toInstance(new SQS(configuration));
            /*bind(HTTP.class)
                    .toInstance(new HTTP(configuration));*/
            // data source(s)
            bind(DataSource.class)
                    .annotatedWith(Names.named("MCMDB"))
                    .to(McmdbDataSource.class)
                    .asEagerSingleton();

            bind(DataSource.class)
                    .to(UnidentifiedDataSource.class)
                    .asEagerSingleton();
            // data access object(s)
            /*bind(McmdbDAO.class)
                    .asEagerSingleton();

            bind(NoSqlWorkDatabase.class)
                    .asEagerSingleton();

            bind(NoSqlCollectingDatabase.class)
                    .asEagerSingleton();

            bind(NoSqlDocumentStore.class)
                    .asEagerSingleton();*/

            bind(MessageDeduplicator.class)
                    .to(McmdbMessageDeduplicator.class);
            /*bind(WorkSearchService.class);*/

            // self
            bind(IngestionCodificaManuale.class)
                    .asEagerSingleton();
        }

    }

    public static void main(String[] args) {
        try {
            final IngestionCodificaManuale instance = Guice
                    .createInjector(new GuiceModuleExtension(args,
                            "/ingestion-codifica-manuale.properties"))
                    .getInstance(IngestionCodificaManuale.class)
                    .startup();
            try {
                instance.process(args);
            } finally {
                instance.shutdown();
            }
        } catch (Throwable e) {
            logger.error("main", e);
        } finally {
            System.exit(0);
        }
    }

    private final Properties configuration;
    private final S3 s3;
    private final SQS sqs;
    private final MessageDeduplicator messageDeduplicator;
//    private final WorkSearchService workSearchService;
    private final UnidentifiedDataSource unidentifiedDataSource;

    @Inject
    protected IngestionCodificaManuale(@Named("configuration") Properties configuration,
                                       S3 s3, SQS sqs,
                                       MessageDeduplicator messageDeduplicator,
//                                        WorkSearchService workSearchService,
                                       UnidentifiedDataSource unidentifiedDataSource) {
        super();
        this.configuration = configuration;
        this.s3 = s3;
        this.sqs = sqs;
        this.messageDeduplicator = messageDeduplicator;
//        this.workSearchService = workSearchService;
        this.unidentifiedDataSource = unidentifiedDataSource;
    }

    public IngestionCodificaManuale startup() {
        s3.startup();
        sqs.startup();
//        workSearchService.startup();
        return this;
    }

    public IngestionCodificaManuale shutdown() {
        sqs.shutdown();
        s3.shutdown();
//        workSearchService.shutdown();
        return this;
    }

    private IngestionCodificaManuale process(String[] args) throws Exception {

        final int bindPort = Integer.parseInt(configuration
                .getProperty("ingestion_codifica_manuale.bind_port", configuration
                        .getProperty("default.bind_port", "0")));

        // bind lock tcp port
        try (final ServerSocket socket = new ServerSocket(bindPort)) {
            logger.info("socket bound to {}", socket.getLocalSocketAddress());

            process();

        }

        return this;
    }


    private void process() throws Exception {
        final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "ingestion_codifica_manuale.sqs");
        sqsMessagePump.pollingLoop(messageDeduplicator, new SqsMessagePump.Consumer() {

            private JsonObject output;
            private JsonObject error;

            @Override
            public JsonObject getStartedMessagePayload(JsonObject message) {
                return SqsMessageHelper.formatContext();
            }

            @Override
            public boolean consumeMessage(JsonObject message) {
                try {
                    output = processMessage(message);
                    return true;
                } catch (Exception e) {
                    logger.error("", e);
                    error = SqsMessageHelper.formatError(e);
                    return false;
                }
            }

            @Override
            public JsonObject getCompletedMessagePayload(JsonObject message) {
                return output;
            }

            @Override
            public JsonObject getFailedMessagePayload(JsonObject message) {
                return error;
            }
        });
    }

    private JsonObject processMessage(JsonObject message) throws IOException {
        final JsonObject output = new JsonObject();
        final JsonObject body = GsonUtils.getAsJsonObject(message, "body");

        final String workingDirectory = configuration.getProperty("ingestion_codifica_manuale.working_directory");

        final String kbFileRegex = configuration.getProperty("ingestion_codifica_manuale.kb_file_regex"); //^[0-9]{4}_[0-9]{2}_[0-9]{2}_export_mm_kb_[0-9]{3}\.json\.gz$
        final String kbS3KeyRegex = configuration.getProperty("ingestion_codifica_manuale.kb_s3_key_regex"); //mm_kb/([0-9]{4}_[0-9]{2}_[0-9]{2})/[0-9]{4}_[0-9]{2}_[0-9]{2}_export_mm_kb_[0-9]{3}\.json\.gz
        final String kbS3BasePath = configuration.getProperty("ingestion_codifica_manuale.kb_s3_base_path"); //s3://siae-sophia-datalake/env/mm_kb/
        final String kbS3BucketName = configuration.getProperty("ingestion_codifica_manuale.kb_s3_bucket_name"); //siae-sophia-datalake
        final String kbS3KeyPattern = configuration.getProperty("ingestion_codifica_manuale.kb_s3_key_pattern");
        final String kbS3FilenamePattern = configuration.getProperty("ingestion_codifica_manuale.kb_s3_filename_pattern");


        DSLContext dslContext = DSL.using(unidentifiedDataSource, SQLDialect.MYSQL);

        String kbS3Path = Optional.ofNullable(GsonUtils.getAsString(body, "kbS3Path")).orElseGet(() -> {
            Pattern folderRegEx = Pattern.compile(kbS3KeyRegex);
            String lastExported = s3.listObjects(new S3.Url(kbS3BasePath)).stream()
                    .map(S3ObjectSummary::getKey)
                    .map(folderRegEx::matcher)
                    .filter(Matcher::matches)
                    .map(matcher -> matcher.group(1))
                    .max(Comparator.naturalOrder())
                    .orElseThrow(() -> new RuntimeException("kb export not found"));

            return kbS3BasePath + lastExported;
        });

        List<S3ObjectSummary> kbS3PathObjects = s3.listObjects(new S3.Url(kbS3Path));

        Pattern filenamePattern = Pattern.compile(kbFileRegex);
        for (S3ObjectSummary kbS3PathObject : kbS3PathObjects) {
            String filename = kbS3PathObject.getKey().substring(kbS3PathObject.getKey().lastIndexOf("/") + 1);

            if (!filenamePattern.matcher(filename).matches())
                continue;

            File file = new File(workingDirectory + "kb.json/" + filename);
            file.deleteOnExit();

            S3.Url s3Url = new S3.Url(kbS3PathObject.getBucketName(), kbS3PathObject.getKey());
            if (!s3.download(s3Url, file))
                throw new IOException("an error occured downloading " + s3Url);

        }


        HeartBeat kb = HeartBeat.constant("kb: ", 100_000);
        HeartBeat codman = HeartBeat.constant("codman: ", 100);
        HeartBeat totalOutput = HeartBeat.constant("output: ", 100_000);



        SupportNoSqlDb codManNoSqlDb = new SupportNoSqlDb(configuration, "support_nosql_db", workingDirectory + "/codman.nosql");
        SupportNoSqlDb kbNoSqlDb = new SupportNoSqlDb(configuration, "support_nosql_db", workingDirectory + "/kb.nosql");


        IngestionRepertoriUtils.filesToLines(Paths.get(workingDirectory, "kb.json"))
                .map(line -> GsonUtils.fromJson(line, JsonObject.class))
                .map(jsonObject -> new SupportNoSqlEntity(GsonUtils.getAsString(jsonObject, "uuid"), GsonUtils.toJson(jsonObject)))
                .filter(supportNoSqlEntity -> supportNoSqlEntity.id != null)
                .peek(supportNoSqlEntity -> kb.pump())
                .forEach(kbNoSqlDb::put);

        logger.info("kb inserted " + kb.getTotalPumps());
        logger.info("kb count " + kbNoSqlDb.count());

        Supplier<Stream<Record>> result = () -> dslContext.fetchStream(configuration.getProperty("ingestion_codifica_manuale.sql.select_codifiche_manuali"));


        /*Supplier<Stream<JsonObject>> codManWorkAlreadyInKb = () -> result.get()
                .filter(record -> kbNoSqlDb.get(record.get("siae_work_code", String.class)) != null)
                .map(this::codificataManualeRecordToJsonObject);
        Supplier<Stream<JsonObject>> codManNotInKb = () -> result.get()
                .filter(record -> kbNoSqlDb.get(record.get("siae_work_code", String.class)) == null)
                .map(this::codificataManualeRecordToJsonObject);

        logger.info("cod man already in kb: " + codManWorkAlreadyInKb.get().count());
        logger.info("cod man not in kb: " + codManNotInKb.get().count());*/


        result.get()
                .map(this::codificataManualeRecordToJsonObject)
                .peek(entitty -> codman.pump())
                .forEach(jsonObject -> {
                    String codiceOperaSiae = GsonUtils.getAsString(jsonObject, "codiceOperaSiae");
                    if (codiceOperaSiae != null) {
                        SupportNoSqlEntity codMan = codManNoSqlDb.get(codiceOperaSiae);
                        if (codMan != null) {
                            JsonObject merged = IngestionRepertoriUtils.mergeJsonObjects(GsonUtils.fromJson(codMan.json, JsonObject.class), jsonObject);
                            codManNoSqlDb.put(jsonObjetToNoSqlEntity(merged));
                        } else {
                            codManNoSqlDb.put(jsonObjetToNoSqlEntity(jsonObject));
                        }
                    }
                });
        logger.info("codman index ended: " + codman.getTotalPumps());


        //writing output

        File outputFile = File.createTempFile("__output__" + System.currentTimeMillis(), ".json.gz", new File(workingDirectory));
        outputFile.deleteOnExit();

        try (OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new CompressionAwareFileOutputStream(outputFile), StandardCharsets.UTF_8);
             BufferedWriter bw = new BufferedWriter(outputStreamWriter);
             PrintWriter pw = new PrintWriter(bw)) {
            kbNoSqlDb.select(entity -> {
                JsonObject jsonObject = GsonUtils.fromJson(entity.json, JsonObject.class);

                String codiceOperaSiae = GsonUtils.getAsString(jsonObject, "codiceOperaSiae");

                if (codiceOperaSiae != null) {
                    SupportNoSqlEntity codMan = codManNoSqlDb.get(codiceOperaSiae);
                    if (codMan != null) {
                        JsonObject merged = IngestionRepertoriUtils.mergeJsonObjects(jsonObject,
                                GsonUtils.fromJson(codMan.json, JsonObject.class));
                        pw.println(GsonUtils.toJson(merged));
                        codManNoSqlDb.deleteByPrimaryKey(codiceOperaSiae);
                    } else {
                        pw.println(entity.json);
                    }
                } else {
                    pw.println(entity.json);
                }

                totalOutput.pump();
            });

        }


        logger.info("total output: " + totalOutput.getTotalPumps());

        String yyyy_MM_dd = new SimpleDateFormat("yyyy_MM_dd_HH_mm").format(new Date());
        List<File> parts = IngestionRepertoriUtils.splitFile(outputFile,
                kbS3FilenamePattern.replace("{yyyy_MM_dd}", yyyy_MM_dd), 1_000_000, workingDirectory, true);


        for (File part : parts) {
            s3.upload(new S3.Url(kbS3BucketName, kbS3KeyPattern.replace("{yyyy_MM_dd}", yyyy_MM_dd).replace("{filename}", part.getName())), part);
        }

        output.addProperty("updatedKb", kbS3BasePath + yyyy_MM_dd);
        return output;
    }

    private JsonObject codificataManualeRecordToJsonObject(Record record) {
        String siaeWorkCode = record.get("siae_work_code", String.class);
        String titles = record.get("titles", String.class);
        String artists = record.get("artists", String.class);
        String roles = record.get("roles", String.class);
        String siadaTitles = record.get("siada_titles", String.class);

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("codiceOperaSiae", siaeWorkCode);

        List<String> allTitles = new ArrayList<>();
        Optional.of(Arrays.asList(titles.split("\\|"))).ifPresent(allTitles::addAll);
        Optional.of(Arrays.asList(siadaTitles.split("\\|"))).ifPresent(allTitles::addAll);

        jsonObject.add("titoliOt", allTitles.stream().collect(GsonCollectors.toJsonArray()));

        List<String> artistsSpilt = Arrays.asList(artists.split("\\|"));
        List<String> rolesSplit = Arrays.asList(roles.split("\\|"));

        if (artistsSpilt.size() != rolesSplit.size())
            return null;

        /*
            NotProvided
            MainArtist
            Composer
            ComposerAuthor
        */

        List<String> autori = new ArrayList<>();
        List<String> compositori = new ArrayList<>();
        List<String> interpreti = new ArrayList<>();


        for (int i = 0; i < artistsSpilt.size(); i++) {
            switch (rolesSplit.get(i)) {
                case "MainArtist":
                    interpreti.add(artistsSpilt.get(i));
                    break;
                case "Composer":
                    compositori.add(artistsSpilt.get(i));
                    break;
                case "ComposerAuthor":
                    compositori.add(artistsSpilt.get(i));
                    autori.add(artistsSpilt.get(i));
                    break;
            }
        }

        JsonArray autoriArray = autori.stream().map(s -> {
            JsonObject j = new JsonObject();
            j.addProperty("nominativo", s);
            return j;
        }).collect(GsonCollectors.toJsonArray());

        JsonArray compositoriArray = compositori.stream().map(s -> {
            JsonObject j = new JsonObject();
            j.addProperty("nominativo", s);
            return j;
        }).collect(GsonCollectors.toJsonArray());

        JsonArray interpretiArray = interpreti.stream()
                .collect(GsonCollectors.toJsonArray());

        jsonObject.add("autori", autoriArray);
        jsonObject.add("compositori", compositoriArray);
        jsonObject.add("interpreti", interpretiArray);

        return jsonObject;
    }

    private SupportNoSqlEntity jsonObjetToNoSqlEntity(JsonObject jsonObject) {
        return new SupportNoSqlEntity(
                GsonUtils.getAsString(jsonObject, "codiceOperaSiae"),
                GsonUtils.toJson(jsonObject));
    }

}

