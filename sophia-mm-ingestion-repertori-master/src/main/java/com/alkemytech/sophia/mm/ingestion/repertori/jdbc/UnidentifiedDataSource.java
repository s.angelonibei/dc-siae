package com.alkemytech.sophia.mm.ingestion.repertori.jdbc;


import com.alkemytech.sophia.commons.jdbc.DBCP2DataSource;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class UnidentifiedDataSource extends DBCP2DataSource {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Inject
	public UnidentifiedDataSource(@Named("configuration") Properties configuration,
                                  @Named("unidentified_data_source") String propertyPrefix) {
		super(configuration, propertyPrefix);
		logger.debug("propertyPrefix: {}", propertyPrefix);
	}

}


