package com.alkemytech.sophia.mm.ingestion.repertori;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.http.HTTP;
import com.alkemytech.sophia.commons.sqs.McmdbMessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.mm.ingestion.repertori.jdbc.McmdbDAO;
import com.alkemytech.sophia.mm.ingestion.repertori.jdbc.McmdbDataSource;
import com.alkemytech.sophia.mm.ingestion.repertori.jdbc.UnidentifiedDAO;
import com.alkemytech.sophia.mm.ingestion.repertori.jdbc.UnidentifiedDataSource;
import com.alkemytech.sophia.mm.ingestion.repertori.orchestrator.IngestionCodificaManuale;
import com.alkemytech.sophia.mm.ingestion.repertori.orchestrator.IngestionIsrc;
import com.alkemytech.sophia.mm.ingestion.repertori.orchestrator.IngestionUcmrAda;
import com.alkemytech.sophia.mm.ingestion.repertori.orchestrator.IngestionUlisseIpiDump;
import com.alkemytech.sophia.mm.ingestion.repertori.orchestrator.*;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.multibindings.MapBinder;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import org.jooq.lambda.Unchecked;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.net.ServerSocket;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.concurrent.CompletableFuture.allOf;
import static java.util.concurrent.CompletableFuture.runAsync;
import static org.jooq.lambda.Unchecked.function;
import static org.jooq.lambda.Unchecked.runnable;

public class IngestionOrchestrator {

    private static final Logger logger = LoggerFactory.getLogger(IngestionOrchestrator.class);

    private static class GuiceModuleExtension extends GuiceModule {

        public GuiceModuleExtension(String[] args, String resourceName) {
            super(args, resourceName);
        }

        @Override
        protected void configure() {
            super.configure();
            // amazon service(s)
            bind(S3.class)
                    .toInstance(new S3(configuration));
            bind(SQS.class)
                    .toInstance(new SQS(configuration));
            bind(HTTP.class)
                    .toInstance(new HTTP(configuration));
            // data source(s)
            bind(DataSource.class)
                    .annotatedWith(Names.named("MCMDB"))
                    .to(McmdbDataSource.class)
                    .asEagerSingleton();

            bind(DataSource.class)
                    .annotatedWith(Names.named("unidentified"))
                    .to(UnidentifiedDataSource.class)
                    .asEagerSingleton();

            bind(McmdbDAO.class)
                    .asEagerSingleton();

            bind(UnidentifiedDAO.class)
                    .asEagerSingleton();

            bind(MessageDeduplicator.class)
                    .to(McmdbMessageDeduplicator.class);

            bind(IngestionUlisseIpiDump.class)
                    .asEagerSingleton();
            MapBinder<String, UpdateKbProcessHelper> updateKbProcessBinder = MapBinder.newMapBinder(binder(), String.class, UpdateKbProcessHelper.class);
            updateKbProcessBinder.addBinding(IngestionUlisseIpiDump.PROCESS_NAME).to(IngestionUlisseIpiDump.class);
            updateKbProcessBinder.addBinding(IngestionUcmrAda.PROCESS_NAME).to(IngestionUcmrAda.class);
            updateKbProcessBinder.addBinding(IngestionCodificaManuale.PROCESS_NAME).to(IngestionCodificaManuale.class);
            updateKbProcessBinder.addBinding(IngestionIsrc.PROCESS_NAME).to(IngestionIsrc.class);
            updateKbProcessBinder.addBinding(CodiciDump.PROCESS_NAME).to(CodiciDump.class);
            updateKbProcessBinder.addBinding(IndexUpdater.PROCESS_NAME).to(IndexUpdater.class);

            // self
            bind(IngestionOrchestrator.class)
                    .asEagerSingleton();
        }

    }

    public static void main(String[] args) {
        try {
            final IngestionOrchestrator instance = Guice
                    .createInjector(new GuiceModuleExtension(args,
                            "/ingestion-orchestrator.properties"))
                    .getInstance(IngestionOrchestrator.class)
                    .startup();
            try {
                instance.process(args);
            } finally {
                instance.shutdown();
            }
        } catch (Throwable e) {
            logger.error("main", e);
        } finally {
            System.exit(0);
        }
    }

    private final Properties configuration;
    private final S3 s3;
    private final SQS sqs;
    private final MessageDeduplicator messageDeduplicator;
    private final McmdbDAO mcmdbDAO;
    private final UnidentifiedDAO unidentifiedDAO;
    private final String[] processes;
    private final Map<String, UpdateKbProcessHelper> processHelpers;

    @Inject
    protected IngestionOrchestrator(@Named("configuration") Properties configuration,
                                    S3 s3, SQS sqs,
                                    MessageDeduplicator messageDeduplicator,
                                    McmdbDAO mcmdbDAO, UnidentifiedDAO unidentifiedDAO,
                                    Map<String, UpdateKbProcessHelper> processHelpers) {
        super();
        this.configuration = configuration;
        this.s3 = s3;
        this.sqs = sqs;
        this.messageDeduplicator = messageDeduplicator;
        this.mcmdbDAO = mcmdbDAO;
        this.processes = configuration.getProperty("ingestion_orchestrator.processes").split(",");
        this.unidentifiedDAO = unidentifiedDAO;
        this.processHelpers = processHelpers;
    }

    public IngestionOrchestrator startup() {
        s3.startup();
        sqs.startup();
        return this;
    }

    public IngestionOrchestrator shutdown() {
        sqs.shutdown();
        s3.shutdown();
        return this;
    }

    private IngestionOrchestrator process(String[] args) throws Exception {

        final int bindPort = Integer.parseInt(configuration
                .getProperty("ingestion_ulisse_ipi_dump.bind_port", configuration
                        .getProperty("default.bind_port", "0")));


        // bind lock tcp port
        try (final ServerSocket socket = new ServerSocket(bindPort)) {
            logger.info("socket bound to {}", socket.getLocalSocketAddress());

            List<CompletableFuture<Void>> futures = new ArrayList<>();
            ExecutorService executorService = Executors.newCachedThreadPool();

            CompletableFuture<Void> toProcessOrchestrator =
                    runAsync(
                            runnable(() ->
                                    sqsMessagePump(this::getStartedMessageToProcessOrchestrator,
                                            function(this::processMessageToProcessOrchestrator),
                                            SqsMessageHelper::formatError,
                                            "ingestion_orchestrator.sqs")),
                            executorService);
            futures.add(toProcessOrchestrator);

            for (String process : processes) {
                CompletableFuture<Void> completedQueue = runAsync(completedSqsMessagePump(process), executorService);
                CompletableFuture<Void> failedQueue = runAsync(failedSqsMessagePump(process), executorService);

                futures.add(completedQueue);
                futures.add(failedQueue);
            }

            allOf(futures.toArray(new CompletableFuture[0])).join();
        }

        return this;
    }

    private Runnable completedSqsMessagePump(String processName) {
        return runnable(() ->
                sqsMessagePump(message -> null,
                        function(message -> processHelpers.get(processName).onCompletedMessage(message)),
                        SqsMessageHelper::formatError,
                        String.format("%s.%s.sqs", processName, "completed")));
    }

    private Runnable failedSqsMessagePump(String processName) {
        return runnable(() ->
                sqsMessagePump(message -> null,
                        function(message -> processHelpers.get(processName).onFailedMessage(message)),
                        e -> GsonUtils.fromJson(e.getMessage(), JsonObject.class),
                        String.format("%s.%s.sqs", processName, "failed")));
    }

    private JsonObject getStartedMessageToProcessOrchestrator(JsonObject message) {
        JsonObject body = GsonUtils.getAsJsonObject(message, "body");
        return body != null && body.has("launchId") ? null : SqsMessageHelper.formatContext();
    }

    private void sqsMessagePump(Function<JsonObject, JsonObject> startedMessage,
                                Function<JsonObject, JsonObject> processMessage,
                                Function<Exception, JsonObject> getError,
                                String propertyPrefix) throws Exception {
        final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, propertyPrefix);
        sqsMessagePump.pollingLoop(messageDeduplicator, new SqsMessagePump.Consumer() {

            private JsonObject output;
            private JsonObject error;

            @Override
            public JsonObject getStartedMessagePayload(JsonObject message) {
                return startedMessage.apply(message);
            }

            @Override
            public boolean consumeMessage(JsonObject message) {
                try {
                    output = processMessage.apply(message);
                    return true;
                } catch (Exception e) {
                    logger.error("", e);
                    error = getError.apply(e);
                    return false;
                }
            }

            @Override
            public JsonObject getCompletedMessagePayload(JsonObject message) {
                return output;
            }

            @Override
            public JsonObject getFailedMessagePayload(JsonObject message) {
                return error;
            }
        });
    }

    private JsonObject processMessageToProcessOrchestrator(JsonObject message) throws SQLException {
        final JsonObject output = new JsonObject();
        JsonObject body = GsonUtils.getAsJsonObject(message, "body");
        if (body != null && body.has("launchId")) {
            Map<String, String> param = new HashMap<>();
            String launchId = GsonUtils.getAsString(body, "launchId");
            param.put("launchId", launchId);
            List<Map<String, String>> steps = mcmdbDAO.executeQuery("ingestion_orchestrator.sql.select_steps_by_launch_id", param);

            List<Map<String, String>> failed = steps.stream().filter(step -> step.get("STATUS").equals("failed"))
                    .collect(Collectors.toList());
            if (failed.size() > 0) {
                throw new RuntimeException("failed " + failed.get(0).get("SERVICE_NAME"));
            }

            Optional<Map<String, String>> toProcess = steps.stream().filter(step -> step.get("STATUS").equals("to_process"))
                    .findFirst();
            if (toProcess.isPresent()) {
                Map<String, String> step = toProcess.get();
                String serviceName = step.get("SERVICE_NAME");
                //send to process message
                param.put("serviceName", serviceName);
                mcmdbDAO.executeUpdate("ingestion_orchestrator.sql.set_step_to_started", param);

                /*String templateBodyMessage = configuration.getProperty(serviceName + ".sqs.to_process_body_template", "{}");*/

                UpdateKbProcessHelper updateKbProcessHelper = processHelpers.get(serviceName);


                SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, serviceName + ".sqs");
                sqsMessagePump.sendToProcessMessage(updateKbProcessHelper.getToProcessMessage(launchId), false);
                return null;
            }

            if (steps.stream().allMatch(step -> step.get("STATUS").equals("completed"))) {
                return output;
            }

        } else {

            String launchId = UUID.randomUUID().toString();
            List<Map<String, String>> parametersList = new ArrayList<>();

            String[] kbIngestionProcesses = configuration.getProperty("ingestion_orchestrator.kb_ingestion.processes").split(",");
            for (String processName : kbIngestionProcesses) {
                UpdateKbProcessHelper processHelper = processHelpers.get(processName);
                if (processHelper.isEnabled() && processHelper.updateNeeded()) {
                    Map<String, String> parameters = new HashMap<>();
                    parameters.put("launchId", launchId);
                    parameters.put("serviceName", processHelper.getProcessName());
                    parameters.put("status", "to_process");
                    parametersList.add(parameters);
                }
            }

            //processes that require a codici_dump update
            List<String> processes = Arrays.asList(configuration.getProperty("codici_dump.processes").split(","));
            if (parametersList.stream()
                    .map(map -> map.get("serviceName"))
                    .anyMatch(processes::contains)) {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("launchId", launchId);
                parameters.put("serviceName", "codici_dump");
                parameters.put("status", "to_process");
                parametersList.add(parameters);
            }

            if (parametersList.size() > 0) {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("launchId", launchId);
                parameters.put("serviceName", "index_updater");
                parameters.put("status", "to_process");
                parametersList.add(parameters);
            }

            for (Map<String, String> param : parametersList) {
                mcmdbDAO.executeUpdate("ingestion_orchestrator.sql.insert_step", param);
            }

            sendToProcessMessageOrchestrator(launchId);

        }
        return null;
    }

    private void sendToProcessMessageOrchestrator(String launchId) {
        JsonObject bodyToProcess = new JsonObject();
        bodyToProcess.addProperty("launchId", launchId);
        SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "ingestion_orchestrator.sqs");
        sqsMessagePump.sendToProcessMessage(bodyToProcess, true);
    }

}