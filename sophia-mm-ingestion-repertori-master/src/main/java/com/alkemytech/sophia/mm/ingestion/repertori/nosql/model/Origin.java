package com.alkemytech.sophia.mm.ingestion.repertori.nosql.model;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class Origin {

	public static final int undefined = 0;
	public static final int siae = 1;
	public static final int itunes = 2;
	public static final int spotify = 3;

	public static int valueOf(String origin) {
		if ("siae".equals(origin)) {
			return siae;
		} else if ("itunes".equals(origin)) {
			return itunes;
		} else if ("spotify".equals(origin)) {
			return spotify;
		}
		return undefined;
	}
	
	public static String toString(int origin) {
		switch (origin) {
		case siae: return "siae";
		case itunes: return "itunes";
		case spotify: return "spotify";
		default: return "undefined";
		}
	}

}
