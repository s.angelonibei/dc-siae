package com.alkemytech.sophia.mm.ingestion.repertori.nosql.work;

import com.alkemytech.sophia.mm.ingestion.repertori.nosql.model.Work;

import java.io.File;
import java.io.IOException;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface WorkDatabase {

	public WorkDatabase startup() throws IOException;
	public WorkDatabase shutdown();
	public boolean isReadOnly();
	public File getHomeFolder();
	public long getNextId() throws IOException;
	public Work getById(long id) throws IOException;
	public Work upsert(Work work) throws IOException;
	public boolean delete(long id) throws IOException;
	public WorkDatabase select(Work.Selector selector) throws Exception;
	public WorkDatabase truncate() throws IOException;
	public WorkDatabase defrag() throws IOException;

}
