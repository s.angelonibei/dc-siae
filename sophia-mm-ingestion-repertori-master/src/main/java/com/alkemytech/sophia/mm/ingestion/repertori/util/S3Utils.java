package com.alkemytech.sophia.mm.ingestion.repertori.util;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.UncheckedIOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public final class S3Utils {

    private S3Utils() {
    }

    public static S3.Url getS3UrlFromS3Message(JsonObject message) {
        JsonArray records = GsonUtils.getAsJsonArray(message, "Records");
        JsonObject record = GsonUtils.getAsJsonObject(records, 0);

        JsonObject s3 = GsonUtils.getAsJsonObject(record, "s3");
        JsonObject bucket = GsonUtils.getAsJsonObject(s3, "bucket");
        String bucketName = GsonUtils.getAsString(bucket, "name");

        JsonObject object = GsonUtils.getAsJsonObject(s3, "object");
        String key = uncheckedUrlDecode(GsonUtils.getAsString(object, "key"));

        return new S3.Url(bucketName, key);
    }

    private static String uncheckedUrlDecode(String url) {
        try {
            return URLDecoder.decode(url, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new UncheckedIOException(e);
        }
    }
}
