package com.alkemytech.sophia.mm.ingestion.repertori.orchestrator;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.mm.ingestion.repertori.jdbc.McmdbDAO;
import com.google.gson.JsonObject;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public abstract class UpdateKbProcessHelper {

    protected final S3 s3;
    protected final SQS sqs;
    protected final Properties configuration;
    protected final McmdbDAO mcmdbDAO;

    protected UpdateKbProcessHelper(S3 s3, SQS sqs, Properties configuration, McmdbDAO mcmdbDAO) {
        this.s3 = s3;
        this.sqs = sqs;
        this.configuration = configuration;
        this.mcmdbDAO = mcmdbDAO;
    }

    public abstract String getProcessName();

    public abstract boolean updateNeeded();

    public abstract boolean isEnabled();

    public JsonObject getToProcessMessage(String launchId) {
        JsonObject body = new JsonObject();
        body.addProperty("launchId", launchId);
        return body;
    }

    public JsonObject onFailedMessage(JsonObject message) throws SQLException {
        updateStepStatusAndSendToProcessMessageOrchestrator(message, "ingestion_orchestrator.sql.update_failed_step");
        return null;
    }

    public JsonObject onCompletedMessage(JsonObject message) throws SQLException {
        updateStepStatusAndSendToProcessMessageOrchestrator(message, "ingestion_orchestrator.sql.update_completed_step");
        return null;
    }

    private void updateStepStatusAndSendToProcessMessageOrchestrator(JsonObject message, String propertyPrefix) throws SQLException {
        JsonObject input = GsonUtils.getAsJsonObject(message, "input");
        JsonObject body = GsonUtils.getAsJsonObject(input, "body");
        String launchId = GsonUtils.getAsString(body, "launchId");

        if (launchId == null)
            return;

        Map<String, String> param = new HashMap<>();
        param.put("launchId", launchId);
        mcmdbDAO.executeUpdate(propertyPrefix, param);
        sendToProcessMessageOrchestrator(launchId);
    }

    private void sendToProcessMessageOrchestrator(String launchId) {
        JsonObject bodyToProcess = new JsonObject();
        bodyToProcess.addProperty("launchId", launchId);
        SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "ingestion_orchestrator.sqs");
        sqsMessagePump.sendToProcessMessage(bodyToProcess, true);
    }
}
