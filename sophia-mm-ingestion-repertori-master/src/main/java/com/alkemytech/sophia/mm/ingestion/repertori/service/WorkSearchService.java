package com.alkemytech.sophia.mm.ingestion.repertori.service;

import com.alkemytech.sophia.commons.http.HTTP;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.mm.ingestion.repertori.util.GsonCollectors;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.name.Named;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Properties;

public class WorkSearchService {

    private final HTTP http;
    private final Properties configuration;
    private final String propertyPrefix;

    @Inject
    public WorkSearchService(HTTP http,
                             @Named("configuration") Properties configuration,
                             @Named("identification_repertoire_documentation.work_search_service") String propertyPrefix) {
        this.http = http;
        this.configuration = configuration;
        this.propertyPrefix = propertyPrefix;
    }

    public WorkSearchService startup()  {
        http.startup();
        return this;
    }

    public WorkSearchService shutdown() throws IOException {
        http.shutdown();
        return this;
    }

    public JsonArray titleArtistQuery(List<String> titles,
                                      List<String> authors,
                                      List<String> composers) throws IOException {
        JsonObject request = new JsonObject();
        JsonObject query = new JsonObject();
        JsonObject texts = new JsonObject();
        request.addProperty("version", "1");

        JsonArray title = titles.stream()
                .collect(GsonCollectors.toJsonArray());
        JsonArray artist_composer = authors.stream()
                .collect(GsonCollectors.toJsonArray());
        JsonArray artist_author = composers.stream()
                .collect(GsonCollectors.toJsonArray());

        texts.add("title", title);
        texts.add("artist_composer", artist_composer);
        texts.add("artist_author", artist_author);

        query.add("texts", texts);
        request.add("query", query);

        HTTP.Response response = http.post(configuration.getProperty(propertyPrefix + ".url"),
                GsonUtils.toJson(request), "application/json");

        String bodyAsString;
        if (response.isOK())
            bodyAsString = response.getBodyAsString(StandardCharsets.UTF_8);
        else
            return null;

        return GsonUtils.fromJson(bodyAsString, JsonArray.class);
    }

    public JsonArray iswcQuery(List<String> iswc) throws IOException {
        JsonObject request = new JsonObject();
        JsonObject query = new JsonObject();
        JsonObject texts = new JsonObject();
        request.addProperty("version", "1");

        JsonArray iswcArray = iswc.stream()
                .collect(GsonCollectors.toJsonArray());

        texts.add("iswc", iswcArray);
        query.add("texts", texts);
        request.add("query", query);

        HTTP.Response response = http.post(configuration.getProperty(propertyPrefix + ".url"),
                GsonUtils.toJson(request), "application/json");

        String bodyAsString;
        if (response.isOK())
            bodyAsString = response.getBodyAsString(StandardCharsets.UTF_8);
        else
            return null;

        return GsonUtils.fromJson(bodyAsString, JsonArray.class);
    }
}
