package com.alkemytech.sophia.mm.ingestion.repertori.jdbc;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.sql.DataSource;

public class DSLContextProvider implements Provider<DSLContext> {

    private final DataSource dataSource;

    @Inject
    public DSLContextProvider(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public DSLContext get() {
        return DSL.using(dataSource, SQLDialect.MYSQL);
    }
}