package com.alkemytech.sophia.mm.ingestion.repertori;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.http.HTTP;
import com.alkemytech.sophia.commons.sqs.McmdbMessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.mm.ingestion.repertori.jdbc.McmdbDataSource;
import com.alkemytech.sophia.mm.ingestion.repertori.service.WorkSearchService;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import one.util.streamex.StreamEx;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.lang.StringUtils;
import org.jooq.lambda.Seq;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.*;
import java.net.ServerSocket;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class IdentificationUcmrAdaDocumentation {

    private static final Logger logger = LoggerFactory.getLogger(IdentificationUcmrAdaDocumentation.class);

    private static class GuiceModuleExtension extends GuiceModule {

        public GuiceModuleExtension(String[] args, String resourceName) {
            super(args, resourceName);
        }

        @Override
        protected void configure() {
            super.configure();
            // amazon service(s)
            bind(S3.class)
                    .toInstance(new S3(configuration));
            bind(SQS.class)
                    .toInstance(new SQS(configuration));
            bind(HTTP.class)
                    .toInstance(new HTTP(configuration));
            // data source(s)
            bind(DataSource.class)
                    .annotatedWith(Names.named("MCMDB"))
                    .to(McmdbDataSource.class)
                    .asEagerSingleton();

            // data access object(s)

            bind(MessageDeduplicator.class)
                    .to(McmdbMessageDeduplicator.class);
            bind(WorkSearchService.class);

            // self
            bind(IdentificationUcmrAdaDocumentation.class)
                    .asEagerSingleton();
        }

    }

    public static void main(String[] args) {
        try {
            final IdentificationUcmrAdaDocumentation instance = Guice
                    .createInjector(new GuiceModuleExtension(args,
                            "/validation-repertoire-documentation.properties"))
                    .getInstance(IdentificationUcmrAdaDocumentation.class)
                    .startup();
            try {
                instance.process(args);
            } finally {
                instance.shutdown();
            }
        } catch (Throwable e) {
            logger.error("main", e);
        } finally {
            System.exit(0);
        }
    }

    private final Properties configuration;
    private final S3 s3;
    private final SQS sqs;
    private final MessageDeduplicator messageDeduplicator;
    private final WorkSearchService workSearchService;

    @Inject
    protected IdentificationUcmrAdaDocumentation(@Named("configuration") Properties configuration,
                                                 S3 s3, SQS sqs,
                                                 MessageDeduplicator messageDeduplicator,
                                                 WorkSearchService workSearchService) {
        super();
        this.configuration = configuration;
        this.s3 = s3;
        this.sqs = sqs;
        this.messageDeduplicator = messageDeduplicator;
        this.workSearchService = workSearchService;
    }

    public IdentificationUcmrAdaDocumentation startup() throws IOException {
        s3.startup();
        sqs.startup();
        workSearchService.startup();
        return this;
    }

    public IdentificationUcmrAdaDocumentation shutdown() throws IOException {
        sqs.shutdown();
        s3.shutdown();
        workSearchService.shutdown();
        return this;
    }

    private IdentificationUcmrAdaDocumentation process(String[] args) throws Exception {

        final int bindPort = Integer.parseInt(configuration
                .getProperty("ingestion-ucmr-ada-documentation.bind_port", configuration
                        .getProperty("default.bind_port", "0")));

        // bind lock tcp port
        try (final ServerSocket socket = new ServerSocket(bindPort)) {
            logger.info("socket bound to {}", socket.getLocalSocketAddress());

            // process database record(s)
            process();

        }

        return this;
    }

    private void process() throws Exception {

        final long startTimeMillis = System.currentTimeMillis();

        // config parameter(s)


        final Map<String, String> cisacRolesMapping = GsonUtils.decodeJsonMap(configuration.getProperty("validation_repertoire_documentation.roles_mapping_cisac"));


        // heart beat
        final HeartBeat heartbeat = HeartBeat.constant("record", Integer.parseInt(configuration
                .getProperty("request_adapter.heartbeat", configuration.getProperty("default.heartbeat", "1000"))));
        final HeartBeat righeLette = HeartBeat.constant("righe lette", 10_000);
        final HeartBeat opere = HeartBeat.constant("opere lette", 1_000);

        final SqsMessagePump toProcessRequestAdapter = new SqsMessagePump(sqs, configuration, "validation_repertoire_documentation.sqs");

        /*JsonArray records = GsonUtils.getAsJsonArray(message, "Records");
        JsonObject record = GsonUtils.getAsJsonObject(records, 0);

        JsonObject s3 = GsonUtils.getAsJsonObject(record, "s3");
        JsonObject bucket = GsonUtils.getAsJsonObject(s3, "bucket");
        String bucketName = GsonUtils.getAsString(bucket, "name");

        JsonObject object = GsonUtils.getAsJsonObject(s3, "object");
        String key = GsonUtils.getAsString(object, "key");*/


        File file = new File("D:\\siae\\dati_sophia\\UCMR-ADA Repertoire 20190614.csv");


        try (InputStream fileStream = new FileInputStream(file);
             Reader reader = new InputStreamReader(fileStream, StandardCharsets.UTF_8)) {
            CSVParser csvParser = new CSVParser(reader, CSVFormat.newFormat(',')
                    .withFirstRecordAsHeader()
                    .withAllowMissingColumnNames()
                    .withSkipHeaderRecord()
                    .withQuote('"')
            );

                    /*SupportNoSqlDb kbNoSqlDb = new SupportNoSqlDb(configuration, "support_nosql_db", "D:\\kb.nosql");


                    HeartBeat heartBeat = HeartBeat.constant("kb rows:", 100_000);
                    StreamEx.of(Files.list(Paths.get("C:\\mm_kb")))
                            .map(Unchecked.function(Files::newInputStream))
                            .map(Unchecked.function(GZIPInputStream::new))
                            .map(gzipInputStream -> new InputStreamReader(gzipInputStream, StandardCharsets.UTF_8))
                            .map(BufferedReader::new)
                            .flatMap(BufferedReader::lines)
                            .map(line -> GsonUtils.fromJson(line, JsonObject.class))
                            .map(jsonObject -> new SupportNoSqlEntity(GsonUtils.getAsString(jsonObject, "codiceOperaSiae"),
                                    GsonUtils.toJson(jsonObject)))
                            .filter(entity -> Objects.nonNull(entity.id))
                            .peek(o -> heartBeat.pump())
                            .forEach(kbNoSqlDb::put);*/


                    /*
                    put("titoloOt", next.get(2));
                    put("titoloAt", next.get(12));
                    put("nominativo", next.get(4));
                    put("ipiNameNr", next.get(9));
                    if (isNotBlank(next.get(10)))
                        put("iswc", "T" + StringUtils.leftPad(next.get(10), 10, '0'));
                    put("qualifica", next.get(3));
                    put("interprete", next.get(11));
                     */


            StreamEx.of(csvParser.spliterator())
                    .groupRuns((r1, r2) -> {
                        righeLette.pump();
                        return r1.get(0).equals(r2.get(0));
                    })
                    .map(recordList -> {
                        opere.pump();
                        return StreamEx.of(recordList).groupingBy(r -> r.get(0));
                    })
                    .map(map -> {
                        Set<String> titoliOt = new HashSet<>();
                        Set<String> titoloAt = new HashSet<>();
                        Set<String> iswc = new HashSet<>();
                        Set<String> interpreti = new HashSet<>();
                        Multimap<String, JsonObject> autoriCompositori = ArrayListMultimap.create();

                        assert map.keySet().size() == 1;
                        String masterCode = map.keySet().toArray(new String[0])[0];
                        map.get(masterCode).forEach(r -> {
                            titoliOt.add(r.get(2));
                            titoloAt.add(r.get(12));
                            if (StringUtils.isNotBlank(r.get(10)))
                                iswc.add("T" + StringUtils.leftPad(r.get(10), 10, '0'));
                            interpreti.add(r.get(11));


                            String ruolo = cisacRolesMapping.get(r.get(3));
                            if (ruolo != null && !StringUtils.isEmpty(r.get(4))) {
                                JsonObject autoreCompositore = new JsonObject();
                                autoreCompositore.addProperty("nominativo", r.get(4));
                                if (StringUtils.isNotBlank(r.get(9)))
                                    autoreCompositore.addProperty("ipiNameNr", r.get(9));


                                autoriCompositori.put(ruolo, autoreCompositore);
                            }
                        });

                        JsonObject jsonObject = new JsonObject();
                        JsonArray titoliOtArray = new JsonArray();
                        JsonArray titoliAtArray = new JsonArray();
                        JsonArray iswcArray = new JsonArray();
                        JsonArray interpretiArray = new JsonArray();
                        JsonArray autoriArray = new JsonArray();
                        JsonArray compositoriArray = new JsonArray();
                        JsonArray codici = new JsonArray();

                        titoliOt.forEach(titoliOtArray::add);
                        titoloAt.forEach(titoliAtArray::add);
                        iswc.forEach(iswcArray::add);
                        interpreti.forEach(interpretiArray::add);

                        jsonObject.addProperty("cociceOperaUcmrAda", masterCode);
                        JsonObject codice = new JsonObject();
                        codice.addProperty("codice", masterCode);
                        codice.addProperty("societa", "UCMR-ADA");
                        codici.add(codice);
                        jsonObject.add("titoliOt", titoliOtArray);
                        jsonObject.add("titoliAt", titoliAtArray);
                        jsonObject.add("iswc", iswcArray);
                        jsonObject.add("interpreti", interpretiArray);

                        if (autoriCompositori.get("artist_author") != null)
                            new ArrayList<>(autoriCompositori.get("artist_author"))
                                    //.map(GsonUtils::fromMap)
                                    //.map(string -> GsonUtils.fromJson(string, JsonObject.class))
                                    .forEach(autoriArray::add);

                        if (autoriCompositori.get("artist_composer") != null)
                            new ArrayList<>(autoriCompositori.get("artist_composer"))
                                    //.map(GsonUtils::fromMap)
                                    //.map(GsonUtils.fromJson(string, JsonObject.class))
                                    .forEach(compositoriArray::add);

                        jsonObject.add("autori", autoriArray);
                        jsonObject.add("compositori", compositoriArray);


                        return jsonObject;

                    })
                    .forEach(jsonObject -> {
                        try {
                            JsonArray results = null;
                            JsonArray iswc = GsonUtils.getAsJsonArray(jsonObject, "iswc");
                            if (iswc != null && Seq.seq(iswc).isNotEmpty()) {
                                results = workSearchService.iswcQuery(Seq.seq(iswc).map(JsonElement::getAsString).toList());
                            }
                            if (results == null || Seq.seq(results).isEmpty()) {
                                results = workSearchService.titleArtistQuery(
                                        Seq.seq(GsonUtils.getAsJsonArray(jsonObject, "titoliOt")).map(JsonElement::getAsString).toList(),
                                        Seq.seq(GsonUtils.getAsJsonArray(jsonObject, "autori"))
                                                .map(jsonElement -> GsonUtils.getAsString(GsonUtils.getAsJsonObject(jsonElement), "nominativo")).toList(),
                                        Seq.seq(GsonUtils.getAsJsonArray(jsonObject, "compositori"))
                                                .map(jsonElement -> GsonUtils.getAsString(GsonUtils.getAsJsonObject(jsonElement), "nominativo")).toList()
                                );
                            }
                            if (results != null && Seq.seq(results).isNotEmpty()) {
                                if (GsonUtils.getAsString(GsonUtils.getAsJsonObject(results.get(0)), "codiceSiae") != null)
                                    System.out.println(GsonUtils.getAsString(jsonObject, "cociceOperaUcmrAda") + ", " +
                                            GsonUtils.getAsString(GsonUtils.getAsJsonObject(results.get(0)), "codiceSiae"));
                            }
                        } catch (IOException e) {
                            logger.error("", e);
                        }

                    });


            logger.info("totale righe: " + righeLette.getTotalPumps());
            logger.info("totale opere: " + opere.getTotalPumps());


        } catch (Exception e) {
            logger.error("", e);
        }


    }


}

