package com.alkemytech.sophia.mm.ingestion.repertori.nosql.model;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class TitleType {

	public static final int undefined = 0;
	public static final int album = 1;
	public static final int song = 2;
	public static final int series = 3;
	public static final int movie = 4;

	public static int valueOf(String titleType) {
		if ("album".equals(titleType)) {
			return album;
		} else if ("song".equals(titleType)) {
			return song;
		} else if ("series".equals(titleType)) {
			return series;
		} else if ("movie".equals(titleType)) {
			return movie;
		}
		return undefined;
	}
	
	public static String toString(int titleType) {
		switch (titleType) {
		case album: return "album";
		case song: return "song";
		case series: return "series";
		case movie: return "movie";
		default: return "undefined";
		}
	}
	
}
