package com.alkemytech.sophia.mm.ingestion.repertori.nosql;

import com.alkemytech.sophia.commons.nosql.NoSqlEntity;
import com.alkemytech.sophia.commons.nosql.NoSqlException;
import com.google.gson.GsonBuilder;

public class DocumentNoSqlEntity implements NoSqlEntity {

    public long id;
    public String json;

    public DocumentNoSqlEntity(long id, String json) {
        this.id = id;
        this.json = json;
    }

    @Override
    public String getPrimaryKey() throws NoSqlException {
        return Long.toString(id);
    }

    @Override
    public String getSecondaryKey() throws NoSqlException {
        return null;
    }

    @Override
    public String toString() {
        return new GsonBuilder()
                .create().toJson(this);
    }

}
