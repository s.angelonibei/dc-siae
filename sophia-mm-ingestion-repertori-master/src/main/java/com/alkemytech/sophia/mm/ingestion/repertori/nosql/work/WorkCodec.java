package com.alkemytech.sophia.mm.ingestion.repertori.nosql.work;


import com.alkemytech.sophia.commons.mmap.ObjectCodec;
import com.alkemytech.sophia.mm.ingestion.repertori.nosql.model.*;

import java.nio.charset.Charset;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class WorkCodec extends ObjectCodec<Work> {
		
	private final Charset charset;

	public WorkCodec(Charset charset) {
		super();
		this.charset = charset;
	}

	@Override
	public Work bytesToObject(byte[] bytes) {
		beginDecoding(bytes);
		final Work work = new Work(getLong());
		// code(s)
		for (int size = getPackedInt(); size > 0; size --) {
			final String text = getString(charset);
			final int type = getPackedInt();
			final int origin = getPackedInt();			
			work.add(new Code(text, type, origin));
		}
		// title(s)
		for (int size = getPackedInt(); size > 0; size --) {
			final String text = getString(charset);
			final int type = getPackedInt();
			final int category = getPackedInt();			
			final int origin = getPackedInt();			
			work.add(new Title(text, type, category, origin));
		}
		// artist(s)
		for (int size = getPackedInt(); size > 0; size --) {
			final String text = getString(charset);
			final String ipi = getString(charset);
			final int role = getPackedInt();
			final int origin = getPackedInt();			
			work.add(new Artist(text, role, ipi, origin));
		}
		// attribute(s)
		for (int size = getPackedInt(); size > 0; size --) {
			final int key = getPackedInt();
			final String value = getString(charset);
			final int origin = getPackedInt();			
			work.add(new Attribute(key, value, origin));
		}
		return work;
	}

	@Override
	public byte[] objectToBytes(Work work) {
		beginEncoding();
		putLong(work.id);
		// code(s)
		if (null == work.codes) {
			putPackedInt(-1);
		} else {
			putPackedInt(work.codes.size());
			for (Code code : work.codes) {
				putString(code.text, charset);
				putPackedInt(code.type);
				putPackedInt(code.origin);
			}
		}
		// title(s)
		if (null == work.titles) {
			putPackedInt(-1);
		} else {
			putPackedInt(work.titles.size());
			for (Title title : work.titles) {
				putString(title.text, charset);
				putPackedInt(title.type);
				putPackedInt(title.category);
				putPackedInt(title.origin);
			}
		}
		// artist(s)
		if (null == work.artists) {
			putPackedInt(-1);
		} else {
			putPackedInt(work.artists.size());
			for (Artist artist : work.artists) {
				putString(artist.text, charset);
				putString(artist.ipiNumber, charset);
				putPackedInt(artist.role);
				putPackedInt(artist.origin);
			}
		}
		// attribute(s)
		if (null == work.attributes) {
			putPackedInt(-1);
		} else {
			putPackedInt(work.attributes.size());
			for (Attribute attribute : work.attributes) {
				putPackedInt(attribute.key);
				putString(attribute.value, charset);
				putPackedInt(attribute.origin);
			}
		}				
		return commitEncoding();
	}

}
