package com.alkemytech.sophia.mm.ingestion.repertori.nosql.model;

import com.google.gson.GsonBuilder;
import com.sleepycat.persist.model.Persistent;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@Persistent
public class Title {

	public String text;
	public int type;
	public int category;
	public int origin;

	public Title() {
		super();
	}

	public Title(String text, int type, int category, int origin) {
		super();
		this.text = text;
		this.type = type;
		this.category = category;
		this.origin = origin;
	}

	@Override
	public int hashCode() {
		return null == text ? 31 : 31 + text.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (null == obj) {
			return false;
		} else if (getClass() != obj.getClass()) {
			return false;
		}
		final Title other = (Title) obj;
		if (null == text) {
			if (null != other.text) {
				return false;
			}
		} else if (!text.equals(other.text)) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return new GsonBuilder()
				.disableHtmlEscaping()
				.create()
				.toJson(this);
	}

}
