package com.alkemytech.sophia.mm.ingestion.repertori.nosql.model;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class CodeType {

	public static final int undefined = 0;
	public static final int iswc = 1;
	public static final int isrc = 2;
	public static final int siae = 3;
	public static final int uuid = 5;

	public static int valueOf(String codeType) {
		if ("iswc".equals(codeType)) {
			return iswc;
		} else if ("isrc".equals(codeType)) {
			return isrc;
		} else if ("siae".equals(codeType)) {
			return siae;
		} else if ("uuid".equals(codeType)) {
			return uuid;
		}
		return undefined;
	}
	
	public static String toString(int codeType) {
		switch (codeType) {
		case iswc: return "iswc";
		case isrc: return "isrc";
		case siae: return "siae";
		case uuid: return "uuid";
		default: return "undefined";
		}
	}

}
