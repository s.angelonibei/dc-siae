package com.alkemytech.sophia.mm.ingestion.repertori.nosql;

import com.alkemytech.sophia.commons.mmap.MemoryMapException;
import com.alkemytech.sophia.commons.mmap.ObjectCodec;

import java.nio.charset.Charset;

public class SupportNoSqlEntityObjectCodec extends ObjectCodec<SupportNoSqlEntity> {

    private final Charset charset;

    public SupportNoSqlEntityObjectCodec(Charset charset) {
        super();
        this.charset = charset;
    }

    @Override
    public SupportNoSqlEntity bytesToObject(byte[] bytes) throws MemoryMapException {
        beginDecoding(bytes);
        return new SupportNoSqlEntity(getString(charset), getString(charset));
    }

    @Override
    public byte[] objectToBytes(SupportNoSqlEntity object) throws MemoryMapException {
        beginEncoding();
        putString(object.id, charset);
        putString(object.json, charset);
        return commitEncoding();
    }
}
