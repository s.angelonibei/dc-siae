package com.alkemytech.sophia.mm.ingestion.repertori.nosql.document;

import com.alkemytech.sophia.mm.ingestion.repertori.nosql.model.Document;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ScoredDocument {


    public final Document document;
    public final double score;
    public String title;
    public String artist;
    public int flagDoppioDeposito = 0;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getFlagDoppioDeposito() {
        return flagDoppioDeposito;
    }

    public int setFlagDoppioDeposito(int flagDoppioDeposito) {
        this.flagDoppioDeposito = flagDoppioDeposito;
        return flagDoppioDeposito;
    }

    public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public ScoredDocument(Document document, double score,String title,String artist) {
        super();
        this.document = document;
        this.score = score;
        this.title = title;
        this.artist = artist;
    }


}
