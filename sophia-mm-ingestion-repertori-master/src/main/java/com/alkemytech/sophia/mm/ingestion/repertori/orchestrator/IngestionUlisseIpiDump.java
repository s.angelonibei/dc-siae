package com.alkemytech.sophia.mm.ingestion.repertori.orchestrator;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.mm.ingestion.repertori.jdbc.McmdbDAO;
import com.google.gson.JsonObject;
import com.google.inject.name.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.*;
import java.sql.SQLException;
import java.util.Date;
import java.util.Properties;

public class IngestionUlisseIpiDump extends UpdateKbProcessHelper {

    private static final Logger logger = LoggerFactory.getLogger(IngestionUlisseIpiDump.class);

    public static final String PROCESS_NAME = "ingestion_ulisse_ipi_dump";

    @Inject
    public IngestionUlisseIpiDump(@Named("configuration") Properties configuration, S3 s3, SQS sqs, McmdbDAO mcmdbDAO) {
        super(s3, sqs, configuration, mcmdbDAO);
    }

    @Override
    public String getProcessName() {
        return PROCESS_NAME;
    }

    @Override
    public boolean updateNeeded() {
        try (ByteArrayOutputStream kbMetadataBaos = new ByteArrayOutputStream();
             ByteArrayOutputStream ulisseMetadataBaos = new ByteArrayOutputStream()) {

            String kbMetadataUrl = configuration.getProperty("ingestion_orchestrator.mm_kb.metadata.s3_path");
            logger.debug("downloading kb metadata file: {}", kbMetadataUrl);
            s3.download(new S3.Url(kbMetadataUrl), kbMetadataBaos);
            Properties kbMetadata = new Properties();
            kbMetadata.load(new ByteArrayInputStream(kbMetadataBaos.toByteArray()));

            String ulisseIpiLatest = configuration.getProperty("ingestion_orchestrator.ulisse_ipi_dump.metadata.s3_path");
            logger.debug("downloading ulisse_ipi_dump latest file: {}", ulisseIpiLatest);
            s3.download(new S3.Url(ulisseIpiLatest), ulisseMetadataBaos);
            Properties ulisseMetadata = new Properties();
            ulisseMetadata.load(new ByteArrayInputStream(ulisseMetadataBaos.toByteArray()));

            String latestIngested = kbMetadata.getProperty("ulisse_ipi_dump.latest", "");
            String latestDump = ulisseMetadata.getProperty("location");

            logger.debug("latest ulisse_ipi_dump ingested: {}", latestIngested);
            logger.debug("latest ulisse_ipi_dump: {}", latestDump);

            return !latestIngested.equals(latestDump);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean isEnabled() {
        return Boolean.parseBoolean(configuration.getProperty(PROCESS_NAME + ".enabled"));
    }

    @Override
    public JsonObject onCompletedMessage(JsonObject message) throws SQLException {
        JsonObject output = GsonUtils.getAsJsonObject(message, "output");
        String updatedKb = GsonUtils.getAsString(output, "updatedKb");
        String codici = GsonUtils.getAsString(output, "codici");
        String ulisseIpiDumpImported = GsonUtils.getAsString(output, "ulisseIpiDumpImported");
        try (ByteArrayOutputStream metadataBaos = new ByteArrayOutputStream()) {
            String kbMetadatS3Path = configuration.getProperty("ingestion_orchestrator.mm_kb.metadata.s3_path");
            s3.download(new S3.Url(kbMetadatS3Path), metadataBaos);
            Properties kbMetadata = new Properties();
            kbMetadata.load(new ByteArrayInputStream(metadataBaos.toByteArray()));
            kbMetadata.put("kb.latest", updatedKb);
            kbMetadata.put("codici_dump.latest", codici);
            kbMetadata.put("ulisse_ipi_dump.latest", ulisseIpiDumpImported);

            File latestVersion = File.createTempFile("__tmp_", System.currentTimeMillis() + "_latest_version",
                    new File(configuration.getProperty("ingestion_orchestrator.working_directory")));
            latestVersion.deleteOnExit();
            FileOutputStream fos = new FileOutputStream(latestVersion);
            kbMetadata.store(fos, new Date().toString());

            s3.upload(new S3.Url(kbMetadatS3Path), latestVersion);

            return super.onCompletedMessage(message);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
