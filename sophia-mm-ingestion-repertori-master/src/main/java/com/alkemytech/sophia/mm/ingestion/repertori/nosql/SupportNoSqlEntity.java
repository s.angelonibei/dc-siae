package com.alkemytech.sophia.mm.ingestion.repertori.nosql;

import com.alkemytech.sophia.commons.nosql.NoSqlEntity;
import com.alkemytech.sophia.commons.nosql.NoSqlException;
import com.google.gson.GsonBuilder;

public class SupportNoSqlEntity implements NoSqlEntity {

    public String id;
    public String json;

    public SupportNoSqlEntity(String id, String json) {
        this.id = id;
        this.json = json;
    }

    @Override
    public String getPrimaryKey() throws NoSqlException {
        return id;
    }

    @Override
    public String getSecondaryKey() throws NoSqlException {
        return null;
    }

    @Override
    public String toString() {
        return new GsonBuilder()
                .create().toJson(this);
    }

}
