package com.alkemytech.sophia.mm.ingestion.repertori.jdbc;


import com.alkemytech.sophia.commons.jdbc.GenericDAO;
import com.google.inject.Inject;
import com.google.inject.name.Named;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class UnidentifiedDAO extends GenericDAO {

	@Inject
	public UnidentifiedDAO(@Named("configuration") Properties configuration,
                           @Named("unidentified") DataSource dataSource) {
		super(configuration, dataSource);
	}

}


