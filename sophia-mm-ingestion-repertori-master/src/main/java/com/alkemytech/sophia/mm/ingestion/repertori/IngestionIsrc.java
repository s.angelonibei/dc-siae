package com.alkemytech.sophia.mm.ingestion.repertori;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.io.CompressionAwareFileOutputStream;
import com.alkemytech.sophia.commons.sqs.McmdbMessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.mm.ingestion.repertori.jdbc.DSLContextProvider;
import com.alkemytech.sophia.mm.ingestion.repertori.jdbc.McmdbDataSource;
import com.alkemytech.sophia.mm.ingestion.repertori.nosql.SupportNoSqlDb;
import com.alkemytech.sophia.mm.ingestion.repertori.nosql.SupportNoSqlEntity;
import com.alkemytech.sophia.mm.ingestion.repertori.service.WorkSearchService;
import com.alkemytech.sophia.mm.ingestion.repertori.util.GsonCollectors;
import com.alkemytech.sophia.mm.ingestion.repertori.util.IngestionRepertoriUtils;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import one.util.streamex.StreamEx;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.UpdateQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.*;
import java.net.ServerSocket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static com.alkemytech.sophia.mm.ingestion.repertori.util.IngestionRepertoriUtils.mergeJsonArray;
import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;

public class IngestionIsrc {

    private static final Logger logger = LoggerFactory.getLogger(IngestionIsrc.class);

    private static class GuiceModuleExtension extends GuiceModule {

        public GuiceModuleExtension(String[] args, String resourceName) {
            super(args, resourceName);
        }

        @Override
        protected void configure() {
            super.configure();
            // amazon service(s)
            bind(S3.class)
                    .toInstance(new S3(configuration));
            bind(SQS.class)
                    .toInstance(new SQS(configuration));
            // data source(s)
            bind(DataSource.class)
                    .annotatedWith(Names.named("MCMDB"))
                    .to(McmdbDataSource.class)
                    .asEagerSingleton();

            bind(DataSource.class)
                    .to(McmdbDataSource.class)
                    .asEagerSingleton();

            // data access object(s)
            bind(DSLContext.class)
                    .toProvider(DSLContextProvider.class)
                    .asEagerSingleton();

            bind(MessageDeduplicator.class)
                    .to(McmdbMessageDeduplicator.class);

            // self
            bind(IngestionIsrc.class)
                    .asEagerSingleton();
        }

    }

    public static void main(String[] args) {
        try {
            final IngestionIsrc instance = Guice
                    .createInjector(new GuiceModuleExtension(args,
                            "/ingestion-isrc.properties"))
                    .getInstance(IngestionIsrc.class)
                    .startup();
            try {
                instance.process(args);
            } finally {
                instance.shutdown();
            }
        } catch (Throwable e) {
            logger.error("main", e);
        } finally {
            System.exit(0);
        }
    }

    private final Properties configuration;
    private final S3 s3;
    private final SQS sqs;
    private final MessageDeduplicator messageDeduplicator;
    private final DSLContext dslContext;

    @Inject
    protected IngestionIsrc(@Named("configuration") Properties configuration,
                            S3 s3, SQS sqs,
                            MessageDeduplicator messageDeduplicator,
                            DSLContextProvider dslContextProvider) {
        super();
        this.configuration = configuration;
        this.s3 = s3;
        this.sqs = sqs;
        this.messageDeduplicator = messageDeduplicator;
        this.dslContext = dslContextProvider.get();
    }

    public IngestionIsrc startup() {
        s3.startup();
        sqs.startup();
        return this;
    }

    public IngestionIsrc shutdown() {
        sqs.shutdown();
        s3.shutdown();
        return this;
    }

    private IngestionIsrc process(String[] args) throws Exception {

        final int bindPort = Integer.parseInt(configuration
                .getProperty("ingestion_isrc.bind_port", configuration
                        .getProperty("default.bind_port", "0")));

        // bind lock tcp port
        try (final ServerSocket socket = new ServerSocket(bindPort)) {
            logger.info("socket bound to {}", socket.getLocalSocketAddress());

            process();

        }

        return this;
    }

    private void process() throws Exception {

        final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "ingestion_isrc.sqs");
        sqsMessagePump.pollingLoop(messageDeduplicator, new SqsMessagePump.Consumer() {

            private JsonObject output;
            private JsonObject error;

            @Override
            public JsonObject getStartedMessagePayload(JsonObject message) {
                return SqsMessageHelper.formatContext();
            }

            @Override
            public boolean consumeMessage(JsonObject message) {
                try {
                    output = processMessage(message);
                    return true;
                } catch (Exception e) {
                    logger.error("", e);
                    error = SqsMessageHelper.formatError(e);
                    return false;
                }
            }

            @Override
            public JsonObject getCompletedMessagePayload(JsonObject message) {
                return output;
            }

            @Override
            public JsonObject getFailedMessagePayload(JsonObject message) {
                return error;
            }
        });


    }

    private JsonObject processMessage(JsonObject message) throws IOException {
        final JsonObject output = new JsonObject();
        final JsonObject body = GsonUtils.getAsJsonObject(message, "body");

        final String workingDirectory = configuration.getProperty("ingestion_isrc.working_directory");

        final String kbFileRegex = configuration.getProperty("ingestion_isrc.kb_file_regex"); //^[0-9]{4}_[0-9]{2}_[0-9]{2}_export_mm_kb_[0-9]{3}\.json\.gz$
        final String kbS3KeyRegex = configuration.getProperty("ingestion_isrc.kb_s3_key_regex"); //mm_kb/([0-9]{4}_[0-9]{2}_[0-9]{2})/[0-9]{4}_[0-9]{2}_[0-9]{2}_export_mm_kb_[0-9]{3}\.json\.gz
        final String kbS3BasePath = configuration.getProperty("ingestion_isrc.kb_s3_base_path"); //s3://siae-sophia-datalake/env/mm_kb/
        final String kbS3BucketName = configuration.getProperty("ingestion_isrc.kb_s3_bucket_name"); //siae-sophia-datalake
        final String kbS3KeyPattern = configuration.getProperty("ingestion_isrc.kb_s3_key_pattern");
        final String kbS3FilenamePattern = configuration.getProperty("ingestion_isrc.kb_s3_filename_pattern");


        String kbS3Path = Optional.ofNullable(GsonUtils.getAsString(body, "kbS3Path")).orElseGet(() -> {
            Pattern folderRegEx = Pattern.compile(kbS3KeyRegex);
            String lastExported = s3.listObjects(new S3.Url(kbS3BasePath)).stream()
                    .map(S3ObjectSummary::getKey)
                    .map(folderRegEx::matcher)
                    .filter(Matcher::matches)
                    .map(matcher -> matcher.group(1))
                    .max(Comparator.naturalOrder())
                    .orElseThrow(() -> new RuntimeException("kb export not found"));

            return kbS3BasePath + lastExported;
        });

        List<S3ObjectSummary> kbS3PathObjects = s3.listObjects(new S3.Url(kbS3Path));

        Pattern filenamePattern = Pattern.compile(kbFileRegex);
        for (S3ObjectSummary kbS3PathObject : kbS3PathObjects) {
            String filename = kbS3PathObject.getKey().substring(kbS3PathObject.getKey().lastIndexOf("/") + 1);

            if (!filenamePattern.matcher(filename).matches())
                continue;

            File file = new File(workingDirectory + "kb.json/" + filename);
            file.deleteOnExit();

            S3.Url s3Url = new S3.Url(kbS3PathObject.getBucketName(), kbS3PathObject.getKey());
            if (!s3.download(s3Url, file))
                throw new IOException("an error occured downloading " + s3Url);

        }


        HeartBeat kb = HeartBeat.constant("kb: ", 100_000);
        HeartBeat isrc = HeartBeat.constant("isrc: ", 100);
        HeartBeat totalOutput = HeartBeat.constant("output: ", 100_000);


        SupportNoSqlDb isrcNoSqlDb = new SupportNoSqlDb(configuration, "support_nosql_db", workingDirectory + "/isrc.nosql");
        SupportNoSqlDb kbNoSqlDb = new SupportNoSqlDb(configuration, "support_nosql_db", workingDirectory + "/kb.nosql");


        IngestionRepertoriUtils.filesToLines(Paths.get(workingDirectory, "kb.json"))
                .map(line -> GsonUtils.fromJson(line, JsonObject.class))
                .map(jsonObject -> new SupportNoSqlEntity(GsonUtils.getAsString(jsonObject, "uuid"), GsonUtils.toJson(jsonObject)))
                .filter(supportNoSqlEntity -> supportNoSqlEntity.id != null)
                .peek(supportNoSqlEntity -> kb.pump())
                .forEach(kbNoSqlDb::put);

        logger.info("kb inserted " + kb.getTotalPumps());
        logger.info("kb count " + kbNoSqlDb.count());

        try {
            dslContext.execute("LOCK TABLES MM_KB_INGESTION_ISRC write");
            Stream<Record> result = dslContext.fetchStream(configuration.getProperty("ingestion_isrc.sql.select_isrc"));

            StreamEx.of(result)
                    .peek((record) -> isrc.pump())
                    .groupRuns((record1, record2) -> record1.get("UUID").equals(record2.get("UUID")))
                    .flatMapToEntry(records -> StreamEx.of(records).groupingBy(r -> (String) r.get(0)))
                    .mapValues(records -> records.stream()
                            .map(record -> record.get("ISRC"))
                            .collect(GsonCollectors.toJsonArray()))
                    .mapKeyValue((uuid, isrcArray) -> new SupportNoSqlEntity(uuid, isrcArray.toString()))
                    .forEach(isrcNoSqlDb::put);


            UpdateQuery<Record> update = dslContext.updateQuery(table("MM_KB_INGESTION_ISRC"));
            update.addValue(field("INSERTED"), true);


            File outputFile = File.createTempFile("__output__" + System.currentTimeMillis(), ".json.gz", new File(workingDirectory));
            outputFile.deleteOnExit();
            try (OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new CompressionAwareFileOutputStream(outputFile), StandardCharsets.UTF_8);
                 BufferedWriter bw = new BufferedWriter(outputStreamWriter);
                 PrintWriter pw = new PrintWriter(bw)) {
                kbNoSqlDb.select(entity -> {
                    JsonObject jsonObject = GsonUtils.fromJson(entity.json, JsonObject.class);

                    String uuid = GsonUtils.getAsString(jsonObject, "uuid");

                    if (uuid != null) {
                        SupportNoSqlEntity isrcEntity = isrcNoSqlDb.get(uuid);
                        if (isrcEntity != null) {
                            JsonArray isrcToAdd = GsonUtils.fromJson(isrcEntity.json, JsonArray.class);

                            if (jsonObject.has("isrc")) {
                                jsonObject.add("isrc",
                                        mergeJsonArray(
                                                jsonObject.getAsJsonArray("isrc"), isrcToAdd));
                            } else {
                                jsonObject.add("isrc", isrcToAdd);
                            }


                            pw.println(GsonUtils.toJson(jsonObject));
                            //isrcNoSqlDb.deleteByPrimaryKey(uuid);
                        } else {
                            pw.println(entity.json);
                        }
                    } else {
                        pw.println(entity.json);
                    }

                    totalOutput.pump();
                });
            }
            String yyyy_MM_dd = new SimpleDateFormat("yyyy_MM_dd_HH_mm").format(new Date());
            List<File> parts = IngestionRepertoriUtils.splitFile(outputFile,
                    kbS3FilenamePattern.replace("{yyyy_MM_dd}", yyyy_MM_dd), 1_000_000, workingDirectory, true);



            for (File part : parts) {
                s3.upload(new S3.Url(kbS3BucketName, kbS3KeyPattern.replace("{yyyy_MM_dd}", yyyy_MM_dd).replace("{filename}", part.getName())), part);
            }

            output.addProperty("updatedKb", kbS3BasePath + yyyy_MM_dd);
            dslContext.execute("UPDATE MM_KB_INGESTION_ISRC SET INSERTED = TRUE WHERE INSERTED = FALSE");
            return output;
        } finally {
            dslContext.execute("UNLOCK TABLES");
        }
    }

}

