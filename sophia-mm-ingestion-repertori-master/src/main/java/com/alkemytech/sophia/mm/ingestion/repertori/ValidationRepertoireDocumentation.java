package com.alkemytech.sophia.mm.ingestion.repertori;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.sqs.McmdbMessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.mm.ingestion.repertori.jdbc.McmdbDAO;
import com.alkemytech.sophia.mm.ingestion.repertori.jdbc.McmdbDataSource;
import com.alkemytech.sophia.mm.ingestion.repertori.util.S3Utils;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import one.util.streamex.StreamEx;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.apache.commons.lang.StringUtils;
import org.jooq.lambda.Seq;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.*;
import java.net.ServerSocket;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;
import java.util.stream.StreamSupport;

public class ValidationRepertoireDocumentation {

    private static final Logger logger = LoggerFactory.getLogger(ValidationRepertoireDocumentation.class);

    private static class GuiceModuleExtension extends GuiceModule {

        public GuiceModuleExtension(String[] args, String resourceName) {
            super(args, resourceName);
        }

        @Override
        protected void configure() {
            super.configure();
            // amazon service(s)
            bind(S3.class)
                    .toInstance(new S3(configuration));
            bind(SQS.class)
                    .toInstance(new SQS(configuration));
            // data source(s)
            bind(DataSource.class)
                    .annotatedWith(Names.named("MCMDB"))
                    .to(McmdbDataSource.class)
                    .asEagerSingleton();
            // data access object(s)
            bind(McmdbDAO.class)
                    .asEagerSingleton();

            bind(MessageDeduplicator.class)
                    .to(McmdbMessageDeduplicator.class);
            // self
            bind(ValidationRepertoireDocumentation.class)
                    .asEagerSingleton();
        }

    }

    public static void main(String[] args) {
        try {
            final ValidationRepertoireDocumentation instance = Guice
                    .createInjector(new GuiceModuleExtension(args,
                            "/validation-repertoire-documentation.properties"))
                    .getInstance(ValidationRepertoireDocumentation.class)
                    .startup();
            try {
                instance.process(args);
            } finally {
                instance.shutdown();
            }
        } catch (Throwable e) {
            logger.error("main", e);
        } finally {
            System.exit(0);
        }
    }

    private final Properties configuration;
    private final S3 s3;
    private final SQS sqs;
    private final McmdbDAO mcmdbDAO;

    @Inject
    protected ValidationRepertoireDocumentation(@Named("configuration") Properties configuration,
                                                S3 s3, SQS sqs,
                                                McmdbDAO mcmdbDAO) {
        super();
        this.configuration = configuration;
        this.s3 = s3;
        this.sqs = sqs;
        this.mcmdbDAO = mcmdbDAO;
    }

    private ValidationRepertoireDocumentation startup() {
        s3.startup();
        sqs.startup();
        return this;
    }

    private ValidationRepertoireDocumentation shutdown() {
        sqs.shutdown();
        s3.shutdown();
        return this;
    }

    private ValidationRepertoireDocumentation process(String[] args) throws Exception {

        final int bindPort = Integer.parseInt(configuration
                .getProperty("ingestion-ucmr-ada-documentation.bind_port", configuration
                        .getProperty("default.bind_port", "0")));

        // bind lock tcp port
        try (final ServerSocket socket = new ServerSocket(bindPort)) {
            logger.info("socket bound to {}", socket.getLocalSocketAddress());

            process();

        }

        return this;
    }

    private void process() throws Exception {

        // config parameter(s)
        final Map<String, String> cisacRolesMapping = GsonUtils.decodeJsonMap(configuration.getProperty("validation_repertoire_documentation.roles_mapping_cisac"));
        final String workingDirectory = configuration.getProperty("validation_repertoire_documentation.working_directory");

        // heart beat
        final HeartBeat righeLette = HeartBeat.constant("righe lette", 10_000);
        final HeartBeat opere = HeartBeat.constant("opere lette", 1_000);

        final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "validation_repertoire_documentation.sqs");
        sqsMessagePump.pollingLoop(null, new SqsMessagePump.Consumer() {
            @Override
            public JsonObject getStartedMessagePayload(JsonObject message) {
                return null;
            }

            @Override
            public boolean consumeMessage(JsonObject message) {

                final AtomicLong valide = new AtomicLong();
                final AtomicLong scarti = new AtomicLong();

                final S3.Url s3Path = S3Utils.getS3UrlFromS3Message(message);
                File tempFile = new File(workingDirectory + UUID.randomUUID().toString());
                tempFile.deleteOnExit();
                if (!s3.download(s3Path, tempFile))
                    throw new RuntimeException("download " + s3Path.toString() + " failed");


                //TODO applicare questa ^(?<env>dev|test|pre-prod|prod)\/([a-zA-Z]+)\/(?<society>[a-zA-Z0-9_-]+)\/(?<filename>.+).csv$
                String[] split = s3Path.key.split("/");
                logger.debug("enviroment: {}", split[0]);
                logger.debug("folder: {}", split[1]);
                String society = split[2].toLowerCase();
                logger.debug("society: {}", society);

                String societyQueue = Pattern.compile("[^a-zA-Z0-9_]").matcher(society).replaceAll("_");

                String startedQueue = configuration.getProperty("validation_repertoire_documentation.sqs._started_queue").replace("{society}", societyQueue);
                sqsMessagePump.sendStartedMessage(startedQueue, message, null, false);


                try (InputStream fileStream = new FileInputStream(tempFile);
                     Reader decoder = new InputStreamReader(fileStream, StandardCharsets.UTF_8)) {
                    CSVParser csvParser = new CSVParser(decoder, CSVFormat.newFormat(',')
                            .withFirstRecordAsHeader()
                            .withAllowMissingColumnNames()
                            .withSkipHeaderRecord()
                            .withQuote('"').withQuoteMode(QuoteMode.MINIMAL)
                    );

                    final Iterator<CSVRecord> iterator = csvParser.iterator();


                    StreamEx<CSVRecord> csvRecords = StreamEx.of(iterator)
                            .parallel()
                            .sortedBy(r -> r.get(0));

                    String result = csvRecords.sequential()
                            .peek(record -> righeLette.pump())
                            .groupRuns((r1, r2) -> r1.get(0).equals(r2.get(0)))
                            .peek(recordsGroup -> opere.pump())
                            .map(recordList -> StreamEx.of(recordList).groupingBy(r -> r.get(0)))
                            .map(map -> {
                                Set<String> titoliOt = new HashSet<>();
                                Multimap<String, String> autoriCompositori = ArrayListMultimap.create();

                                assert map.keySet().size() == 1;
                                String masterCode = map.keySet().toArray(new String[0])[0];
                                map.get(masterCode).forEach(r -> {
                                    titoliOt.add(r.get(2));

                                    String ruolo = cisacRolesMapping.get(r.get(3));
                                    if (ruolo != null && !StringUtils.isEmpty(r.get(4)))
                                        autoriCompositori.put(ruolo, r.get(4));
                                });

                                if (titoliOt.isEmpty()) {
                                    scarti.incrementAndGet();
                                    return masterCode + ": titolo assente";
                                }

                                /*if (autoriCompositori.isEmpty()) {
                                    scarti.incrementAndGet();
                                    return masterCode + ": autori e compositori assenti";
                                }*/

                                valide.incrementAndGet();
                                return null;
                            })
                            .filter(Objects::nonNull)
                            .joining(", ");


                    logger.info("totale righe: " + righeLette.getTotalPumps());
                    logger.info("totale opere: " + opere.getTotalPumps());

                    logger.info("opere valide: " + valide.get());
                    logger.info("opere scartate: " + scarti.get());

                    int length = result.length();

                    if (length > 500)
                        result = result.substring(0, 500);


                    HashMap<String, String> parameters = new HashMap<>();
                    parameters.put("totaleOpere", Long.toString(opere.getTotalPumps()));
                    parameters.put("societa", society.toUpperCase());
                    parameters.put("totaleOpereAcquisite", Integer.toString(0));
                    parameters.put("esito", result.equals("") ? "OK" : "KO");
                    parameters.put("fileLocation", "/" + s3Path.key);
                    parameters.put("message", result);
                    parameters.put("stato", result.equals("") ? "VALIDATO" : "ERRORE");
                    parameters.put("fileName", s3Path.key.substring(s3Path.key.lastIndexOf("/") + 1));

                    mcmdbDAO.executeUpdate("validation_repertoire_documentation.sql.insert", parameters);

                    String completedQueue = configuration.getProperty("validation_repertoire_documentation.sqs._completed_queue").replace("{society}", societyQueue);
                    JsonObject output = new JsonObject();
                    sqsMessagePump.sendCompletedMessage(completedQueue, message, output, false);

                    return true;

                } catch (Exception e) {
                    logger.error("", e);
                    String failedQueue = configuration.getProperty("validation_repertoire_documentation.sqs._failed_queue").replace("{society}", societyQueue);
                    sqsMessagePump.sendFailedMessage(failedQueue, message, SqsMessageHelper.formatError(e), false);
                }

                return false;
            }

            @Override
            public JsonObject getCompletedMessagePayload(JsonObject message) {
                return null;
            }

            @Override
            public JsonObject getFailedMessagePayload(JsonObject message) {
                return null;
            }
        });


    }

}

