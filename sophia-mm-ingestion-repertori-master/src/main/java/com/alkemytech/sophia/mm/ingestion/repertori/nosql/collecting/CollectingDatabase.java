package com.alkemytech.sophia.mm.ingestion.repertori.nosql.collecting;

import com.alkemytech.sophia.mm.ingestion.repertori.nosql.model.CollectingCode;

import java.io.File;
import java.io.IOException;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface CollectingDatabase {

	public CollectingDatabase startup() throws IOException;
	public CollectingDatabase shutdown();
	public boolean isReadOnly();
	public File getHomeFolder();
	public CollectingCode getByCode(String code) throws IOException;
	public CollectingCode getByWorkId(long workId) throws IOException;
	public CollectingCode upsert(CollectingCode collectingCode) throws IOException;
	public CollectingDatabase selectOrderByWeight(CollectingCode.Selector selector, boolean ascending) throws Exception;
	public CollectingDatabase truncate() throws IOException;
	public CollectingDatabase defrag() throws IOException;

}
