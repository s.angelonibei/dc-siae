package com.alkemytech.sophia.mm.ingestion.repertori.nosql.document;

import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.mm.ingestion.repertori.nosql.model.Document;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityCursor;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.StoreConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class BdbDocumentStore implements DocumentStore {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Properties configuration;
	private final String propertyPrefix;
	private final AtomicInteger startupCalls;

	private Environment environment;
	private EntityStore store;

	@Inject
	public BdbDocumentStore(@Named("configuration") Properties configuration,
			@Named("document_store") String propertyPrefix) {
		super();
		logger.debug("propertyPrefix: {}", propertyPrefix);
		this.configuration = configuration;
		this.propertyPrefix = propertyPrefix;
		this.startupCalls = new AtomicInteger(0);
	}

	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.identification.document.DocumentStore#startup()
	 */
	@Override
	public BdbDocumentStore startup() {
		if (0 == startupCalls.getAndIncrement()) {
			try {
				final boolean readOnly = isReadOnly();
				final File environmentHome = new File(configuration
						.getProperty(propertyPrefix + ".home_folder"));
				final long cacheSize = TextUtils.parseLongSize(configuration
						.getProperty(propertyPrefix + ".cache_size", "0"));
				final int cachePercent = Integer.parseInt(configuration
						.getProperty(propertyPrefix + ".cache_percent", "60"));
				final boolean deferredWrite = "true".equals(configuration
						.getProperty(propertyPrefix + ".deferred_write", "true"));
				environmentHome.mkdirs();
				environment = new Environment(environmentHome,
						(EnvironmentConfig) EnvironmentConfig.DEFAULT
							.setAllowCreate(!readOnly)
							.setReadOnly(readOnly)
							.setCacheSize(cacheSize)
							.setCachePercent(cachePercent));
				environment = new Environment(environmentHome,
						EnvironmentConfig.DEFAULT
							.setAllowCreate(!readOnly)
							.setReadOnly(readOnly));
				store = new EntityStore(environment, "document",
						StoreConfig.DEFAULT
							.setAllowCreate(!readOnly)
							.setReadOnly(readOnly)
							.setDeferredWrite(deferredWrite)); 
			} catch (DatabaseException e) {
				logger.error("startup", e);
			}
		}
		return this;
	}

	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.identification.document.DocumentStore#shutdown()
	 */
	@Override
	public BdbDocumentStore shutdown() {
		if (0 == startupCalls.decrementAndGet()) {
			try {
				if (null != store) {
					if (!isReadOnly()) {
						store.sync();
					}
					store.close();
				}
			} catch (DatabaseException e) {
				logger.error("shutdown", e);
			}
			try {
				if (null != environment) {
					if (!isReadOnly()) {
						environment.sync();
					}
					environment.close();
				}
			} catch (DatabaseException e) {
				logger.error("shutdown", e);
			}
		}
		return this;
	}
	
	@Override
	public boolean isReadOnly() {
		return "true".equalsIgnoreCase(configuration
				.getProperty(propertyPrefix + ".read_only", "true"));
	}
	
	public BdbDocumentStore sync() {
		store.sync();
		return this;
	}

	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.identification.document.DocumentStore#getById(long)
	 */
	@Override
	public Document getById(long id) {
		if (0L == id)
			throw new IllegalArgumentException("id");
		final PrimaryIndex<Long, Document> primaryIndex = store
				.getPrimaryIndex(Long.class, Document.class);
		return primaryIndex.get(id);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.identification.document.DocumentStore#upsert(com.alkemytech.sophia.identification.model.Document)
	 */
	@Override
	public Document upsert(Document document) {
		if (!Document.isValid(document))
			throw new IllegalArgumentException("document");
		final PrimaryIndex<Long, Document> primaryIndex = store
				.getPrimaryIndex(Long.class, Document.class);
		return primaryIndex.put(document);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.identification.document.DocumentStore#truncate()
	 */
	@Override
	public BdbDocumentStore truncate() {
		store.truncateClass(Document.class);
		return this;
	}

	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.identification.document.DocumentStore#select(com.alkemytech.sophia.identification.model.Document.Selector)
	 */
	@Override
	public BdbDocumentStore select(Document.Selector selector) throws Exception {
		final PrimaryIndex<Long, Document> primaryIndex = store
				.getPrimaryIndex(Long.class, Document.class);
		try (EntityCursor<Document> documents = primaryIndex.entities()) {
			for (Document document = documents.first();
					null != document;
					document = documents.next()) {
				selector.select(document);
			}
		}
		return this;
	}
	
	@Override
	public BdbDocumentStore defrag() throws IOException {
		return this;
	}
	
}
