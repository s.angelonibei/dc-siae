package com.alkemytech.sophia.mm.ingestion.repertori.nosql.collecting;

import com.alkemytech.sophia.commons.mmap.ObjectCodec;
import com.alkemytech.sophia.mm.ingestion.repertori.nosql.model.CollectingCode;

import java.nio.charset.Charset;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class CollectingCodeCodec extends ObjectCodec<CollectingCode> {
		
	private final Charset charset;

	public CollectingCodeCodec(Charset charset) {
		super();
		this.charset = charset;
	}

	@Override
	public CollectingCode bytesToObject(byte[] bytes) {
		beginDecoding(bytes);
		return new CollectingCode(getString(charset), getLong(), getBigDecimal());
//		return new CollectingCode(getString(charset), getLong(), getDouble());
	}

	@Override
	public byte[] objectToBytes(CollectingCode collectingCode) {
		beginEncoding();
		putString(collectingCode.code, charset);
		putLong(collectingCode.workId);
		putBigDecimal(collectingCode.weight);
//		putDouble(collectingCode.weight);
		return commitEncoding();
	}

}
