package com.alkemytech.sophia.mm.ingestion.repertori.nosql.model;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class ArtistRole {

	public static final int undefined = 0;
	public static final int unknown = 1;
	public static final int author = 2;
	public static final int composer = 3;
	public static final int performer = 4;
	
	public static int valueOf(String artistRole) {
		if ("unknown".equals(artistRole)) {
			return unknown;
		} else if ("author".equals(artistRole)) {
			return author;
		} else if ("composer".equals(artistRole)) {
			return composer;
		} else if ("performer".equals(artistRole)) {
			return performer;
		}
		return undefined;
	}
	
	public static String toString(int artistRole) {
		switch (artistRole) {
		case unknown: return "unknown";
		case author: return "author";
		case composer: return "composer";
		case performer: return "performer";
		default: return "undefined";
		}
	}
	
}
