package com.alkemytech.sophia.mm.ingestion.repertori.util;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import java.util.stream.Collector;

public final class GsonCollectors {

    private GsonCollectors() {}

    public static Collector<Object, JsonArray, JsonArray> toJsonArray() {
        return Collector.of(
                JsonArray::new,
                (jsonArray, obj) -> {
                    if (obj instanceof Boolean)
                        jsonArray.add((Boolean) obj);
                    else if (obj instanceof Character)
                        jsonArray.add((Character) obj);
                    else if (obj instanceof Number)
                        jsonArray.add((Number) obj);
                    else if (obj instanceof String)
                        jsonArray.add((String) obj);
                    else if (obj instanceof JsonElement)
                        jsonArray.add((JsonElement) obj);
                    else
                        throw new UnsupportedOperationException("GsonCollectors.toJsonArray(): " +
                                "incompatible type JsonArray.add(" + obj.getClass().getName() + ")");
                },
                (jsoArray1, jsonArray2) -> {
                    jsoArray1.addAll(jsonArray2);
                    return jsoArray1;
                }
        );
    }
}
