package com.alkemytech.sophia.mm.ingestion.repertori;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.http.HTTP;
import com.alkemytech.sophia.commons.index.LevenshteinIndex;
import com.alkemytech.sophia.commons.index.ReverseIndex;
import com.alkemytech.sophia.commons.index.ReverseIndexImpl;
import com.alkemytech.sophia.commons.io.CompressionAwareFileOutputStream;
import com.alkemytech.sophia.commons.sqs.McmdbMessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.commons.util.LongArray;
import com.alkemytech.sophia.mm.ingestion.repertori.jdbc.McmdbDataSource;
import com.alkemytech.sophia.mm.ingestion.repertori.nosql.DocumentNoSqlDb;
import com.alkemytech.sophia.mm.ingestion.repertori.nosql.DocumentNoSqlEntity;
import com.alkemytech.sophia.mm.ingestion.repertori.nosql.SupportNoSqlDb;
import com.alkemytech.sophia.mm.ingestion.repertori.nosql.SupportNoSqlEntity;
import com.alkemytech.sophia.mm.ingestion.repertori.service.WorkSearchService;
import com.alkemytech.sophia.mm.ingestion.repertori.util.IngestionRepertoriUtils;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.*;
import java.net.ServerSocket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class IngestionUlisseIpiDump {

    private static final Logger logger = LoggerFactory.getLogger(IngestionUlisseIpiDump.class);

    private static class GuiceModuleExtension extends GuiceModule {

        public GuiceModuleExtension(String[] args, String resourceName) {
            super(args, resourceName);
        }

        @Override
        protected void configure() {
            super.configure();
            // amazon service(s)
            bind(S3.class)
                    .toInstance(new S3(configuration));
            bind(SQS.class)
                    .toInstance(new SQS(configuration));
            bind(HTTP.class)
                    .toInstance(new HTTP(configuration));
            // data source(s)
            bind(DataSource.class)
                    .annotatedWith(Names.named("MCMDB"))
                    .to(McmdbDataSource.class)
                    .asEagerSingleton();

            // data access object(s)


            bind(MessageDeduplicator.class)
                    .to(McmdbMessageDeduplicator.class);
            bind(WorkSearchService.class);

            // self
            bind(IngestionUlisseIpiDump.class)
                    .asEagerSingleton();
        }

    }

    public static void main(String[] args) {
        try {
            final IngestionUlisseIpiDump instance = Guice
                    .createInjector(new GuiceModuleExtension(args,
                            "/ingestion-ulisse-ipi-dump.properties"))
                    .getInstance(IngestionUlisseIpiDump.class)
                    .startup();
            try {
                instance.process(args);
            } finally {
                instance.shutdown();
            }
        } catch (Throwable e) {
            logger.error("main", e);
        } finally {
            System.exit(0);
        }
    }

    private final Properties configuration;
    private final S3 s3;
    private final SQS sqs;
    private final MessageDeduplicator messageDeduplicator;
    private final WorkSearchService workSearchService;
    private Map<String, String> siaeRolesMapping;

    @Inject
    protected IngestionUlisseIpiDump(@Named("configuration") Properties configuration,
                                     S3 s3, SQS sqs,
                                     MessageDeduplicator messageDeduplicator,
                                     WorkSearchService workSearchService) {
        super();
        this.configuration = configuration;
        this.s3 = s3;
        this.sqs = sqs;
        this.messageDeduplicator = messageDeduplicator;
        this.workSearchService = workSearchService;
    }

    public IngestionUlisseIpiDump startup() {
        s3.startup();
        sqs.startup();
        workSearchService.startup();
        return this;
    }

    public IngestionUlisseIpiDump shutdown() throws IOException {
        sqs.shutdown();
        s3.shutdown();
        workSearchService.shutdown();
        return this;
    }

    private IngestionUlisseIpiDump process(String[] args) throws Exception {

        final int bindPort = Integer.parseInt(configuration
                .getProperty("ingestion_ulisse_ipi_dump.bind_port", configuration
                        .getProperty("default.bind_port", "0")));

        // bind lock tcp port
        try (final ServerSocket socket = new ServerSocket(bindPort)) {
            logger.info("socket bound to {}", socket.getLocalSocketAddress());

            process();

        }

        return this;
    }

    private void process() throws Exception {

        // config parameter(s)
        siaeRolesMapping = GsonUtils.decodeJsonMap(configuration.getProperty("ingestion_ulisse_ipi_dump.roles_mapping_siae"));

        final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "ingestion_ulisse_ipi_dump.sqs");
        sqsMessagePump.pollingLoop(messageDeduplicator, new SqsMessagePump.Consumer() {

            private JsonObject output;
            private JsonObject error;

            @Override
            public JsonObject getStartedMessagePayload(JsonObject message) {
                return SqsMessageHelper.formatContext();
            }

            @Override
            public boolean consumeMessage(JsonObject message) {
                try {
                    output = processMessage(message);
                    return true;
                } catch (Exception e) {
                    logger.error("", e);
                    error = SqsMessageHelper.formatError(e);
                    return false;
                }
            }

            @Override
            public JsonObject getCompletedMessagePayload(JsonObject message) {
                return output;
            }

            @Override
            public JsonObject getFailedMessagePayload(JsonObject message) {
                return error;
            }
        });


    }

    public static String getLastUlisseIpiDump(final S3 s3, final Properties configuration) {
        Pattern folderRegEx = Pattern.compile(configuration.getProperty("ingestion_ulisse_ipi_dump.s3_key_regex"));
        String basePath = configuration.getProperty("ingestion_ulisse_ipi_dump.s3_base_path");
        String suffix = configuration.getProperty("ingestion_ulisse_ipi_dump.s3_folder_suffix");
        String lastExported = s3.listObjects(new S3.Url(basePath)).stream()
                .map(S3ObjectSummary::getKey)
                .map(folderRegEx::matcher)
                .filter(Matcher::matches)
                .map(matcher -> matcher.group(1))
                .max(Comparator.naturalOrder())
                .orElseThrow(() -> new RuntimeException("ulisse ipi export not found"));

        return basePath + lastExported + suffix;
    }

    private JsonObject processMessage(JsonObject message) throws IOException, ExecutionException, InterruptedException {
        final JsonObject output = new JsonObject();

        final String workingDirectory = configuration.getProperty("ingestion_ulisse_ipi_dump.working_directory");
        final String dtpEnvironment = configuration.getProperty("default.environment_dtp");
        final String kbS3KeyRegex = configuration.getProperty("ingestion_ulisse_ipi_dump.kb_s3_key_regex"); //mm_kb/([0-9]{4}_[0-9]{2}_[0-9]{2})/[0-9]{4}_[0-9]{2}_[0-9]{2}_export_mm_kb_[0-9]{3}\.json\.gz
        final String kbS3BasePath = configuration.getProperty("ingestion_ulisse_ipi_dump.kb_s3_base_path"); //s3://siae-sophia-datalake/env/mm_kb/

        final JsonObject body = GsonUtils.getAsJsonObject(message, "body");

        String dumpUlisseIpiS3Path = Optional.ofNullable(GsonUtils.getAsString(body, "dumpUlisseIpiS3Path"))
                .orElseGet(() -> getLastUlisseIpiDump(s3, configuration));

        String kbS3Path = Optional.ofNullable(GsonUtils.getAsString(body, "kbS3Path")).orElseGet(() -> {
            Pattern folderRegEx = Pattern.compile(kbS3KeyRegex);
            String lastExported = s3.listObjects(new S3.Url(kbS3BasePath)).stream()
                    .map(S3ObjectSummary::getKey)
                    .map(folderRegEx::matcher)
                    .filter(Matcher::matches)
                    .map(matcher -> matcher.group(1))
                    .max(Comparator.naturalOrder())
                    .orElseThrow(() -> new RuntimeException("kb export not found"));

            return kbS3BasePath + lastExported;
        });

        logger.info("path last ulisse ipi dump: {}", dumpUlisseIpiS3Path);
        logger.info("path last kb: {}", kbS3Path);
        /*Pattern fileRegex = Pattern.compile(dumpUlisseIpiS3Path + "([0-9]{4}_[0-9]{2}_[0-9]{2}_.*export.*\\.json\\.gz)$");

        List<String> ulisseIpiDumpFiles = s3.listObjects(new S3.Url(dumpUlisseIpiS3Path)).stream()
                .map(S3ObjectSummary::getKey)
                .map(fileRegex::matcher)
                .filter(Matcher::matches)
                .map(Matcher::group)
                .collect(Collectors.toList());*/


        /*File ulisseIpiDumpLocalFolder = new File(workingDirectory + "/ulisse_ipi_dump");
        ulisseIpiDumpLocalFolder.deleteOnExit();

        s3.download(new S3.Url(dumpUlisseIpiS3Path), ulisseIpiDumpLocalFolder);*/




        SupportNoSqlDb ulisseDumpNoSqlDb = new SupportNoSqlDb(configuration, "support_nosql_db", workingDirectory + "ulisse.nosql");
        SupportNoSqlDb kbNoSqlDb = new SupportNoSqlDb(configuration, "support_nosql_db", workingDirectory + "kb.nosql");
        SupportNoSqlDb updatedKbNoSqlDb = new SupportNoSqlDb(configuration, "support_nosql_db", workingDirectory + "updated_kb.nosql");
        DocumentNoSqlDb documentKbNoSqlDb = new DocumentNoSqlDb(configuration, "nosql_document_store", workingDirectory + "document_store.nosql");
        AtomicLong documentId = new AtomicLong();

        ReverseIndex codSiaeKbIndex = new ReverseIndexImpl(configuration, "cod_opera_kb_index");
        ReverseIndex.Editor editor = codSiaeKbIndex.edit();
        editor.rewind();

        ExecutorService executorService = Executors.newCachedThreadPool();

        Future<Long> loadUlisseIndex = executorService.submit(() -> {
            HeartBeat heartBeat = HeartBeat.constant("ulisse rows:", 100_000);

//                String kbS3Path = configuration.getProperty("ingestion_ulisse_ipi_dump.ulisse_ipi_dump_s3_path");
            List<S3ObjectSummary> kbS3PathObjects = s3.listObjects(new S3.Url(dumpUlisseIpiS3Path));


            Pattern filenamePattern = Pattern.compile(configuration.getProperty("ingestion_ulisse_ipi_dump.s3_file_regex"));
            for (S3ObjectSummary kbS3PathObject : kbS3PathObjects) {
                String filename = kbS3PathObject.getKey().substring(kbS3PathObject.getKey().lastIndexOf("/") + 1);

                if (!filenamePattern.matcher(filename).matches())
                    continue;

                File file = new File(workingDirectory + "ulisse_ipi_dump/" + filename);
                file.deleteOnExit();

                S3.Url s3Url = new S3.Url(kbS3PathObject.getBucketName(), kbS3PathObject.getKey());
                if (!s3.download(s3Url, file))
                    throw new IOException("error downloading file: " + s3Url);

            }

            IngestionRepertoriUtils.filesToLines(Paths.get(workingDirectory, "ulisse_ipi_dump"))
                    .map(line -> GsonUtils.fromJson(line, JsonObject.class))
                    .map(jsonObject -> IngestionRepertoriUtils.ulisseJsonToKbJson(jsonObject, siaeRolesMapping))
//                    .groupRuns((json1, json2) -> GsonUtils.getAsString(json1, "codiceOperaSiae")
//                            .equals(GsonUtils.getAsString(json2, "codiceOperaSiae")))
//                    .map(jsonList -> IngestionRepertoriUtils.mergeJsonObjects(jsonList.toArray(new JsonObject[0])))
                    .peek(o -> heartBeat.pump())
                    .forEach(jsonObject -> {
                        SupportNoSqlEntity entity = ulisseDumpNoSqlDb.get(GsonUtils.getAsString(jsonObject, "codiceOperaSiae"));

                        if (entity != null) {
                            JsonObject merged = IngestionRepertoriUtils.mergeJsonObjects(GsonUtils.fromJson(entity.json, JsonObject.class), jsonObject);
                            ulisseDumpNoSqlDb.put(IngestionRepertoriUtils.jsonObjetToNoSqlEntity(merged, "codiceOperaSiae"));
                        } else {
                            ulisseDumpNoSqlDb.put(IngestionRepertoriUtils.jsonObjetToNoSqlEntity(jsonObject, "codiceOperaSiae"));
                        }
                    });


            return heartBeat.getTotalPumps();
        });


        Future<Long> loadKbIndex = executorService.submit(() -> {
            HeartBeat heartBeat = HeartBeat.constant("kb rows:", 100_000);
            HeartBeat heartBeatCodOpera = HeartBeat.constant("cod opera index:", 100_000);


//                String kbS3Path = configuration.getProperty("ingestion_ulisse_ipi_dump.kb_s3_path");
            List<S3ObjectSummary> kbS3PathObjects = s3.listObjects(new S3.Url(kbS3Path));


            Pattern filenamePattern = Pattern.compile(configuration.getProperty("ingestion_ulisse_ipi_dump.kb_s3_file_regex"));
            for (S3ObjectSummary kbS3PathObject : kbS3PathObjects) {
                String filename = kbS3PathObject.getKey().substring(kbS3PathObject.getKey().lastIndexOf("/") + 1);

                if (!filenamePattern.matcher(filename).matches())
                    continue;

                File file = new File(workingDirectory + "kb/" + filename);
                file.deleteOnExit();

                S3.Url s3Url = new S3.Url(kbS3PathObject.getBucketName(), kbS3PathObject.getKey());
                if (!s3.download(s3Url, file))
                    throw new IOException("cannot download file: " + s3Url);

            }


            IngestionRepertoriUtils.filesToLines(Paths.get(workingDirectory, "kb"))
                    .map(line -> GsonUtils.fromJson(line, JsonObject.class))
                    //.map(jsonObject -> new SupportNoSqlEntity(GsonUtils.getAsString(jsonObject, "uuid"), GsonUtils.toJson(jsonObject)))

//                    .peek(o -> heartBeat.pump())
                    .forEach(jsonObject -> {
                        String uuid = GsonUtils.getAsString(jsonObject, "uuid");
                        JsonArray codiciOpera = GsonUtils.getAsJsonArray(jsonObject, "codiciOpera");
                        Map<String, String> societaCodice = StreamSupport.stream(codiciOpera.spliterator(), false)
                                .map(GsonUtils::getAsHashMap)
                                .filter(map -> map.get("societa").equals("SIAE"))
                                .findFirst().orElse(new HashMap<>());
                        String codiceOperaSiae =  societaCodice.get("codice");
                        SupportNoSqlEntity entity = new SupportNoSqlEntity(uuid, GsonUtils.toJson(jsonObject));

                        long id = documentId.incrementAndGet();
                        documentKbNoSqlDb.put(new DocumentNoSqlEntity(id, uuid));
                        kbNoSqlDb.put(entity);
                        heartBeat.pump();

                        if (codiceOperaSiae != null) {
                            editor.add(codiceOperaSiae, id);
                            heartBeatCodOpera.pump();
                        }
                    });


            editor.commit();
            editor.rewind();

            return heartBeat.getTotalPumps();
        });


        logger.info("done kb " + loadKbIndex.get());
        logger.info("done ulisse " + loadUlisseIndex.get());

        executorService.shutdown();

        ulisseDumpNoSqlDb.select(ulisseEntity -> {
            LevenshteinIndex.TermSet termSet = codSiaeKbIndex.search(ulisseEntity.id);
            if (termSet.next()) {
                LongArray postings = termSet.getPostings();
                DocumentNoSqlEntity documentNoSqlEntity = documentKbNoSqlDb.get(Long.toString(postings.get(0)));
                SupportNoSqlEntity supportNoSqlEntity = kbNoSqlDb.get(documentNoSqlEntity.json);
                //pw.println(supportNoSqlEntity.json);
                updatedKbNoSqlDb.put(IngestionRepertoriUtils.jsonObjetToNoSqlEntity(GsonUtils.fromJson(supportNoSqlEntity.json, JsonObject.class), "uuid"));
            } else {
                JsonObject kbJson = GsonUtils.fromJson(ulisseEntity.json, JsonObject.class);
                //JsonObject kbJson = IngestionRepertoriUtils.ulisseJsonToKbJson(ulisseJson, siaeRolesMapping);
                kbJson.addProperty("uuid", UUID.randomUUID().toString());
                //pw.println(GsonUtils.toJson(kbJson));
                updatedKbNoSqlDb.put(IngestionRepertoriUtils.jsonObjetToNoSqlEntity(kbJson, "uuid"));

            }
        });
        ulisseDumpNoSqlDb.truncate();

        kbNoSqlDb.select(kbEntity -> {
            if (updatedKbNoSqlDb.get(kbEntity.id) == null)
                updatedKbNoSqlDb.put(kbEntity);
        });
        kbNoSqlDb.truncate();


        String yyyy_MM_dd = new SimpleDateFormat("yyyy_MM_dd_HH_mm").format(new Date());
        File result = new File(workingDirectory + "__temp_ingestion_ulisse_ipi_dump_" + System.currentTimeMillis() + ".json.gz");
        File codici = new File(workingDirectory + yyyy_MM_dd + "_codici.json.gz");
        result.deleteOnExit();
        codici.deleteOnExit();
        try (CompressionAwareFileOutputStream fileOutputStream = new CompressionAwareFileOutputStream(result);
             OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, StandardCharsets.UTF_8);
             BufferedWriter bw = new BufferedWriter(outputStreamWriter);
             PrintWriter pw = new PrintWriter(bw);
             CompressionAwareFileOutputStream fileOutputStreamCodici = new CompressionAwareFileOutputStream(codici);
             OutputStreamWriter outputStreamWriterCodici = new OutputStreamWriter(fileOutputStreamCodici, StandardCharsets.UTF_8);
             BufferedWriter bwCodici = new BufferedWriter(outputStreamWriterCodici);
             PrintWriter pwCodici = new PrintWriter(bwCodici)) {

            updatedKbNoSqlDb.select(kbEntity -> {
                pw.println(kbEntity.json);
                JsonObject jsonObject = GsonUtils.fromJson(kbEntity.json, JsonObject.class);
                JsonObject jsonCodici = new JsonObject();
                jsonCodici.add("uuid", jsonObject.get("uuid"));
                jsonCodici.add("codiciOpera", jsonObject.get("codiciOpera"));
                pwCodici.println(GsonUtils.toJson(jsonCodici));
            });

        }

        updatedKbNoSqlDb.truncate();

        S3.Url codiciS3 = new S3.Url("siae-sophia-datalake", dtpEnvironment + "/mm_kb/" + yyyy_MM_dd + "/codici/" + codici.getName());
        s3.upload(codiciS3, codici);

        List<File> parts = IngestionRepertoriUtils.splitFile(result,
                yyyy_MM_dd + "_export_mm_kb_{part}.json.gz", 1_000_000, workingDirectory, true);

        for (File part : parts) {
            s3.upload(new S3.Url("siae-sophia-datalake", dtpEnvironment + "/mm_kb/" + yyyy_MM_dd + "/" + part.getName()), part);
        }

        output.addProperty("updatedKb", kbS3BasePath + yyyy_MM_dd);
        output.addProperty("codici", codiciS3.toString());
        output.addProperty("ulisseIpiDumpImported", dumpUlisseIpiS3Path);

        return output;
    }

}

