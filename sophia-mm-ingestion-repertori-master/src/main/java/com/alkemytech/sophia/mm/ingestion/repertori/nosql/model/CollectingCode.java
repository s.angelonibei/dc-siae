package com.alkemytech.sophia.mm.ingestion.repertori.nosql.model;

import com.alkemytech.sophia.commons.nosql.NoSqlEntity;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;
import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
import com.sleepycat.persist.model.Relationship;
import com.sleepycat.persist.model.SecondaryKey;

import java.math.BigDecimal;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@Entity
public class CollectingCode implements NoSqlEntity {
	
	public static interface Selector {
		public void select(CollectingCode collectingCode) throws Exception;
	}

	public static boolean isValid(CollectingCode collectingCode) {
		if (null == collectingCode) {
			return false;
		} else if (Strings.isNullOrEmpty(collectingCode.code)) {
			return false;
		} else if (0L == collectingCode.workId) {
			return false;
		}
		return true;
	}
	
	@PrimaryKey
	public String code;
	@SecondaryKey(relate=Relationship.ONE_TO_ONE)
	public long workId;
	@SecondaryKey(relate=Relationship.MANY_TO_ONE)
	public BigDecimal weight;

	public CollectingCode() {
		super();
	}

	public CollectingCode(String code, long workId, BigDecimal weight) {
		super();
		this.code = code;
		this.workId = workId;
		this.weight = weight;
	}

	@Override
	public String getPrimaryKey() {
		return code;
	}

	@Override
	public String getSecondaryKey() {
		return Long.toString(workId);
	}

	@Override
	public int hashCode() {
		int result = 31 + (null == code ? 0 : code.hashCode());
		result = 31 * result + (null == weight ? 0 : weight.hashCode());
		return 31 * result + (int) (workId ^ (workId >>> 32));
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (null == obj) {
			return false;
		} else if (getClass() != obj.getClass()) {
			return false;
		}
		final CollectingCode other = (CollectingCode) obj;
		if (null == code) {
			if (null != other.code) {
				return false;
			}
		} else if (!code.equals(other.code)) {
			return false;
		}
		if (null == weight) {
			if (null != other.weight) {
				return false;
			}
		} else if (!weight.equals(other.weight)) {
			return false;
		}
		if (workId != other.workId) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.disableHtmlEscaping()
				.serializeSpecialFloatingPointValues()
				.create()
				.toJson(this);
	}
	
}
