package com.alkemytech.sophia.mm.ingestion.repertori.util;

import com.alkemytech.sophia.commons.io.CompressionAwareFileInputStream;
import com.alkemytech.sophia.commons.io.CompressionAwareFileOutputStream;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.mm.ingestion.repertori.nosql.SupportNoSqlEntity;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.commons.lang.StringUtils;
import org.jooq.lambda.Seq;
import org.jooq.lambda.Unchecked;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.StreamSupport.stream;

public class IngestionRepertoriUtils {

    private IngestionRepertoriUtils() {
    }

    public static SupportNoSqlEntity jsonObjetToNoSqlEntity(JsonObject jsonObject, String idField) {
        return new SupportNoSqlEntity(
                GsonUtils.getAsString(jsonObject, idField),
                GsonUtils.toJson(jsonObject));
    }


    public static JsonObject mergeJsonObjects(JsonObject... jsonObjects) {
        if (jsonObjects.length == 1)
            return jsonObjects[0];

        JsonObject result = null;
        if (jsonObjects.length > 1) {
            result = GsonUtils.deepCopy(jsonObjects[0]);

            for (JsonObject jsonObject : jsonObjects)
                result = mergeJsonObjects(result, jsonObject);
        }
        return result;
    }

    private static JsonObject mergeJsonObjects(final JsonObject json1, final JsonObject json2) {
        JsonObject result = GsonUtils.deepCopy(json1);

        Seq.of("titoliOt", "autori", "compositori", "interpreti", "iswc", "isrc").forEach(field -> {
            JsonArray fieldJson1 = GsonUtils.getAsJsonArray(json1, field);
            JsonArray fieldJson2 = GsonUtils.getAsJsonArray(json2, field);
            JsonArray mergedField = mergeJsonArray(fieldJson1, fieldJson2);
            result.remove(field);
            result.add(field, mergedField);
        });

        /*JsonArray titoliOtJson1 = GsonUtils.getAsJsonArray(json1, "titoliOt");
        JsonArray titoliOtJson2 = GsonUtils.getAsJsonArray(json2, "titoliOt");
        JsonArray mergedTitoliOt = mergeJsonArray(titoliOtJson1, titoliOtJson2);
        result.remove("titoliOt");
        result.add("titoliOt", mergedTitoliOt);

        JsonArray autoriJson1 = GsonUtils.getAsJsonArray(json1, "autori");
        JsonArray autoriJson2 = GsonUtils.getAsJsonArray(json2, "autori");
        JsonArray mergedAutori = mergeJsonArray(autoriJson1, autoriJson2);
        result.remove("autori");
        result.add("autori", mergedAutori);

        JsonArray compositoriJson1 = GsonUtils.getAsJsonArray(json1, "compositori");
        JsonArray compositoriJson2 = GsonUtils.getAsJsonArray(json2, "compositori");
        JsonArray mergedCompositori = mergeJsonArray(compositoriJson1, compositoriJson2);
        result.remove("compositori");
        result.add("compositori", mergedCompositori);

        JsonArray interpretiJson1 = GsonUtils.getAsJsonArray(json1, "interpreti");
        JsonArray interpretiJson2 = GsonUtils.getAsJsonArray(json2, "interpreti");
        JsonArray mergedInterpreti = mergeJsonArray(interpretiJson1, interpretiJson2);
        result.remove("interpreti");
        result.add("interpreti", mergedInterpreti);

        JsonArray iswcJson1 = GsonUtils.getAsJsonArray(json1, "iswc");
        JsonArray iswcJson2 = GsonUtils.getAsJsonArray(json2, "iswc");
        JsonArray mergedIswc = mergeJsonArray(iswcJson1, iswcJson2);
        result.remove("iswc");
        result.add("iswc", mergedIswc);

        JsonArray isrcJson1 = GsonUtils.getAsJsonArray(json1, "isrc");
        JsonArray isrcJson2 = GsonUtils.getAsJsonArray(json2, "isrc");
        JsonArray mergedIsrc = mergeJsonArray(isrcJson1, isrcJson2);
        result.remove("isrc");
        result.add("isrc", mergedIsrc);*/

        return result;
    }

    public static JsonArray mergeJsonArray(JsonArray array1, JsonArray array2) {
        return Seq.seq(array1)
                .append((Iterable<JsonElement>) array2)
                .distinct()
                .collect(GsonCollectors.toJsonArray());
    }

    public static Stream<String> filesToLines(Path path) {
        return Unchecked.function(Files::list).apply(path)
                //Files.list(Paths.get(path))
                .map(Path::toFile)
                .map(Unchecked.function(CompressionAwareFileInputStream::new))
                .map(fileInputStream -> new InputStreamReader(fileInputStream, StandardCharsets.UTF_8))
                .map(BufferedReader::new)
                .flatMap(IngestionRepertoriUtils::bufferedStreamToStreamOfLines);
    }

    private static Stream<String> bufferedStreamToStreamOfLines(BufferedReader br) {
        return br.lines().onClose(Unchecked.runnable(br::close));
    }


    public static List<File> splitFile(File toSplit, String filename, int linesForFile, String path, boolean deleteOnExit) {
        List<File> files = new ArrayList<>();

        try (CompressionAwareFileInputStream in = new CompressionAwareFileInputStream(toSplit);
             InputStreamReader inputStreamReader = new InputStreamReader(in, StandardCharsets.UTF_8);
             BufferedReader br = new BufferedReader(inputStreamReader)) {
            String line;
            int filePartNr = 0;
            int rowWritten = 0;
            File part = new File(path + filename.replace("{part}", String.format("%03d", filePartNr++)));
            PrintWriter pw = new PrintWriter(new CompressionAwareFileOutputStream(part));
            files.add(part);
            while ((line = br.readLine()) != null) {
                if (rowWritten == linesForFile) {
                    pw.close();
                    part = new File(path + filename.replace("{part}", String.format("%03d", filePartNr++)));
                    pw = new PrintWriter(new CompressionAwareFileOutputStream(part));
                    files.add(part);
                    rowWritten = 0;
                }
                pw.println(line);
                rowWritten++;
            }
            pw.close();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }

        if (deleteOnExit)
            files.forEach(File::deleteOnExit);

        return files;
    }

    public static JsonObject ulisseJsonToKbJson(JsonObject jsonObject, Map<String, String> siaeRolesMapping) {
        JsonObject result = new JsonObject();

        String codiceOpera = GsonUtils.getAsString(jsonObject, "codiceOpera");
        JsonArray iswc = GsonUtils.getAsJsonArray(jsonObject, "iswc");
        JsonArray isrc = GsonUtils.getAsJsonArray(jsonObject, "isrc");
        String rilevanza = GsonUtils.getAsString(jsonObject, "rilevanza");
        String maturato = GsonUtils.getAsString(jsonObject, "maturato");
        String rischio = GsonUtils.getAsString(jsonObject, "rischio");
        String statoOpera = GsonUtils.getAsString(jsonObject, "statoOpera");
        String ambito = GsonUtils.getAsString(jsonObject, "ambito");
        String flagDoppioDeposito = GsonUtils.getAsString(jsonObject, "flagDoppioDeposito");
        String flagIrregolare = GsonUtils.getAsString(jsonObject, "flagIrregolare");
        String flagEL = GsonUtils.getAsString(jsonObject, "flagEL");
        String flagPD = GsonUtils.getAsString(jsonObject, "flagPD");
        JsonArray titoliOt = GsonUtils.getAsJsonArray(jsonObject, "titoliOt");
        JsonArray titoliAt = GsonUtils.getAsJsonArray(jsonObject, "titoliAt");
        JsonArray quoteRiparto = GsonUtils.getAsJsonArray(jsonObject, "quoteRiparto");
        JsonArray ipi = GsonUtils.getAsJsonArray(jsonObject, "ipi");
        JsonArray interpreti = GsonUtils.getAsJsonArray(jsonObject, "interpreti");


        if (codiceOpera != null) {
            result.addProperty("codiceOperaSiae", codiceOpera);
            JsonArray array = new JsonArray();
            JsonObject codice = new JsonObject();
            codice.addProperty("codice", codiceOpera);
            codice.addProperty("societa", "SIAE");
            array.add(codice);
            result.add("codiciOpera", array);
        }

        if (titoliOt != null) {
            JsonArray array = new JsonArray();
            titoliOt.forEach(jsonElement ->
                    array.add(GsonUtils.getAsString(
                            GsonUtils.getAsJsonObject(jsonElement), "titolo")));
            result.add("titoliOt", array);
        }

        if (titoliAt != null) {
            JsonArray array = new JsonArray();
            titoliAt.forEach(jsonElement ->
                    array.add(GsonUtils.getAsString(
                            GsonUtils.getAsJsonObject(jsonElement), "titolo")));
            result.add("titoliAt", array);
        }

        if (iswc != null) {
            JsonArray array = new JsonArray();
            iswc.forEach(jsonElement ->
                    array.add(GsonUtils.getAsString(
                            GsonUtils.getAsJsonObject(jsonElement), "codice")));
            result.add("iswc", array);
        }

        if (isrc != null) {
            JsonArray array = new JsonArray();
            isrc.forEach(jsonElement ->
                    array.add(GsonUtils.getAsString(
                            GsonUtils.getAsJsonObject(jsonElement), "codice")));
            result.add("isrc", array);
        }

        if (interpreti != null) {
            JsonArray array = stream(interpreti.spliterator(), false)
                    .map(JsonElement::getAsJsonObject)
                    .map(json -> GsonUtils.getAsString(json, "nome"))
                    .collect(GsonCollectors.toJsonArray());

            result.add("interpreti", array);
        }

        if (rilevanza != null)
            result.addProperty("rilevanza", rilevanza);

        if (maturato != null)
            result.addProperty("maturato", maturato);

        if (rischio != null)
            result.addProperty("rischio", rischio);

        if (statoOpera != null)
            result.addProperty("statoOpera", statoOpera);

        if (ambito != null)
            result.addProperty("ambito", ambito);

        if (flagDoppioDeposito != null)
            result.addProperty("flagDoppioDeposito", flagDoppioDeposito);

        if (flagIrregolare != null)
            result.addProperty("flagIrregolare", flagIrregolare);

        if (flagEL != null)
            result.addProperty("flagEL", flagEL);

        if (flagPD != null)
            result.addProperty("flagPD", flagPD);

        Multimap<String, JsonObject> autoriCompositori = ArrayListMultimap.create();
        if (quoteRiparto != null) {
            for (JsonElement element : quoteRiparto) {
                JsonObject quota = GsonUtils.getAsJsonObject(element);
                String codiceIpi = GsonUtils.getAsString(quota, "codiceIpi");
                String nominativo = GsonUtils.getAsString(quota, "nominativo");
                String qualifica = GsonUtils.getAsString(quota, "qualifica");

                String ruolo;
                if (nominativo != null && qualifica != null && (ruolo = siaeRolesMapping.get(qualifica)) != null) {
                    JsonObject autoreCompositore = new JsonObject();
                    autoreCompositore.addProperty("nominativo", nominativo);
                    if (codiceIpi != null)
                        autoreCompositore.addProperty("ipiNameNr", codiceIpi);
                    autoriCompositori.put(ruolo, autoreCompositore);
                }
            }
        }

        if (ipi != null) {
            for (JsonElement ipiElement : ipi) {
                JsonArray codiciIpiNameNumber = GsonUtils.getAsJsonArray(
                        GsonUtils.getAsJsonObject(ipiElement), "codiciIpiNameNumber");
                JsonArray qualifiche = GsonUtils.getAsJsonArray(
                        GsonUtils.getAsJsonObject(ipiElement), "qualifiche");

                //genero tutte le permutazioni tra i due array qualifiche e codiciIpiNameNumber
                Seq.seq(qualifiche)
                        .map(JsonElement::getAsString)
                        .filter(StringUtils::isNotBlank)
                        .filter(siaeRolesMapping::containsKey)
                        .crossJoin(Seq.seq(codiciIpiNameNumber)
                                .map(GsonUtils::getAsJsonObject)
                                .filter(Objects::nonNull))
                        //.peek(tuple -> logger.debug(tuple.toString()))
                        .forEach(tuple -> {
                            String qualifica = tuple.v1();
                            JsonObject codiceIpi = tuple.v2();

                            String nominativo = GsonUtils.getAsString(
                                    GsonUtils.getAsJsonObject(codiceIpi), "nominativo");
                            String ipiNameNumber = GsonUtils.getAsString(
                                    GsonUtils.getAsJsonObject(codiceIpi), "ipiNameNumber");

                            String ruolo;
                            if (nominativo != null && qualifica != null && (ruolo = siaeRolesMapping.get(qualifica)) != null) {
                                JsonObject autoreCompositore = new JsonObject();
                                autoreCompositore.addProperty("nominativo", nominativo);
                                if (codiceIpi != null)
                                    autoreCompositore.addProperty("ipiNameNr", ipiNameNumber);
                                autoriCompositori.put(ruolo, autoreCompositore);
                            }
                        });
            }

        }

        Collection<JsonObject> artistAuthor = autoriCompositori.get("artist_author");
        Collection<JsonObject> artistComposer = autoriCompositori.get("artist_composer");

        if (artistAuthor != null) {
            JsonArray array = new JsonArray();
            new ArrayList<>(artistAuthor).forEach(array::add);
            result.add("autori", array);
        }

        if (artistComposer != null) {
            JsonArray array = new JsonArray();
            new ArrayList<>(artistComposer).forEach(array::add);
            result.add("compositori", array);
        }

        return result;
    }

}
