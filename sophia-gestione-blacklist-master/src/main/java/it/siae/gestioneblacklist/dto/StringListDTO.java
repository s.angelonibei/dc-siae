package it.siae.gestioneblacklist.dto;

import lombok.Data;

import java.util.List;

@Data
public class StringListDTO {
    private List<String> names;
}
