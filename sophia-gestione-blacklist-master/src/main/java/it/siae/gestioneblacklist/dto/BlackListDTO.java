package it.siae.gestioneblacklist.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BlackListDTO implements Serializable {
    private String societaDiTutela;
    private String territorio;
    private String dataInizioValidita;
    private String nomeFileCsv;
    private List<Dettaglio> recordDettaglio;

    @Data
    public static class Dettaglio {
        private String descrizione;
        private String tipoRegola;
        private List<String> subEditori;
        private List<String> societa;
        private List<String> societaEscluse;
        private List<String> tipoQuotaDSP;
        private List<String> dspPeriodoList;
    }

}
