package it.siae.gestioneblacklist.entity;

import com.google.gson.GsonBuilder;
import lombok.Data;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Data
@Entity(name="Blacklist")
@Table(name="BLACKLIST")
public class Blacklist {

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="ID", nullable=false)
	private Long id;

	@Column(name="COUNTRY", nullable=false)
	private String country;

	@Column(name="VALID_FROM", nullable=false)
	private Timestamp validFrom;

	@Column(name="XLS_S3_URL", nullable=false)
	private String xlsS3Url;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CODICE_SOCIETA_TUTELA", referencedColumnName = "CODICE")
	private SocietaTutela societaTutela;

	public Blacklist() {};

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}

	public SocietaTutela getSocieta() {
		return societaTutela;
	}

	public void setSocieta(SocietaTutela societaTutela) {
		this.societaTutela = societaTutela;
	}
}
