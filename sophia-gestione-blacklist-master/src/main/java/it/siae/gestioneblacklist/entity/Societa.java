package it.siae.gestioneblacklist.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Table(name="MM_ANAG_BL_SOCIETA",schema = "MCMDB_debug")
@Entity
public class Societa implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	protected Long idSocieta;

	@Column(name = "NOME")
	private String nomeSocieta;

	public Societa() {};

	public Societa(Long idSocieta, String nomeSocieta) {
		this.idSocieta = idSocieta;
		this.nomeSocieta = nomeSocieta;
	}
}

