package it.siae.gestioneblacklist.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="MM_ANAG_BL_SUB_EDITORI")
public class SubEditori {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    protected Long idSubEditore;

    @Column(name = "NOME")
    private String nomeSubEditore;

    public SubEditori() {};

    public SubEditori(Long idSubEditore, String nomeSubEditore) {
        this.idSubEditore = idSubEditore;
        this.nomeSubEditore = nomeSubEditore;
    }
}
