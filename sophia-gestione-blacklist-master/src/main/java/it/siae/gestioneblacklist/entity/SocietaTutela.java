package it.siae.gestioneblacklist.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name="SOCIETA_TUTELA")
public class SocietaTutela implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    protected Long id;

    @Column(name = "CODICE")
    private String codice;

    @Column(name = "NOMINATIVO")
    private String nominativo;

    @Column(name = "HOME_TERRITORY")
    private String homeTerritoryCode;

    @Column(name = "POSIZIONE_SIAE")
    private String posizioneSIAE;

    @Column(name = "CODICE_SIADA")
    private String codiceSIADA;

    @Column(name = "TENANT")
    private Long tenant;

    public SocietaTutela() {
        super();
    }


    public SocietaTutela(String codice, String nominativo, String homeTerritoryCode, String posizioneSIAE, String codiceSIADA,
                         Long tenant) {
        super();
        this.codice = codice;
        this.nominativo = nominativo;
        this.homeTerritoryCode = homeTerritoryCode;
        this.posizioneSIAE = posizioneSIAE;
        this.codiceSIADA = codiceSIADA;
        this.tenant = tenant;
    }

    public SocietaTutela(Long id, String codice) {
        this.id = id;
        this.codice = codice;
    }
}
