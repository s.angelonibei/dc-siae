/*
 * @Autor A.D.C.
 */

package it.siae.gestioneblacklist.query;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:queries/blacklist.properties")
@ConfigurationProperties(prefix = "blacklist", ignoreUnknownFields = false)
@Data
public class BlacklistProperites {
    private BlacklistAppends appends;
}
