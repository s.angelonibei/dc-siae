/*
 * @Autor A.D.C.
 */

package it.siae.gestioneblacklist.query;

import lombok.Data;

@Data
public abstract class AbstractQuery {

    private String count;
    private String select;
    private String pagination;
    private String update;
    private String segregation;
    private String orderBy;
    private String groupBy;
    private String delete;
    private String maxFirstRow;

}
