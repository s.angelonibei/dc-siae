/*
 * @Autor A.D.C.
 */

package it.siae.gestioneblacklist.query;

import lombok.Data;

@Data
public class BlacklistAppends extends AbstractQuery {
    private String societa;
    private String territorio;
    private String dataInizioValidita;
    private String maxDateFrom;
    private String dataMaxStart;
    private String dataMaxEnd;
}
