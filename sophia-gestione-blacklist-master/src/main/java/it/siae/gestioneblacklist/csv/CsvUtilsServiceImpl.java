package it.siae.gestioneblacklist.csv;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressEventType;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.PutObjectRequest;
import it.siae.gestioneblacklist.aws.S3;
import it.siae.gestioneblacklist.dto.BlackListDTO;
import it.siae.gestioneblacklist.errors.BadRequestAlertException;
import it.siae.gestioneblacklist.errors.ErrorConstants;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.QuoteMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
@Transactional
public class CsvUtilsServiceImpl implements CsvUtilsService {
    Logger logger = LoggerFactory.getLogger(CsvUtilsServiceImpl.class);

    @Value("${s3.retry.put_object}")
    private String put_object;

    @Value("${spring.cloud.aws.credentials.accessKey}")
    private String accessKey;

    @Value("${spring.cloud.aws.credentials.secretKey}")
    private String secretKey;

    @Value("${spring.cloud.aws.region.static}")
    private String region;

    public boolean upload(S3.Url url, final File file) {
        try {
            for (int retries = Integer.parseInt(put_object);
                 retries > 0; retries--) {
                try {
                    logger.debug("I'm uploading CSV FILE " + file.getName() + " to {}", file.getName(), url);
                    final PutObjectRequest putObjectRequest = new PutObjectRequest(url.bucket,
                            url.key.replace("//", "/"), file);
                    putObjectRequest.setGeneralProgressListener(new ProgressListener() {

                        private long accumulator = 0L;
                        private long total = 0L;
                        private long chunk = Math.max(256 * 1024, file.length() / 10L);

                        @Override
                        public void progressChanged(ProgressEvent event) {
                            if (ProgressEventType.REQUEST_BYTE_TRANSFER_EVENT
                                    .equals(event.getEventType())) {
                                accumulator += event.getBytes();
                                total += event.getBytes();
                                if (accumulator >= chunk) {
                                    accumulator -= chunk;
                                    logger.info("transferred {}Kb/{}Kb of file {}",
                                            total / 1024L, file.length() / 1024L, file.getName());
                                }
                            } else if (ProgressEventType.TRANSFER_COMPLETED_EVENT
                                    .equals(event.getEventType())) {
                                logger.debug("transfer of file " + file.getName() + " completed");
                            }
                        }

                    });
                    AWSCredentials credentials = new BasicAWSCredentials(
                            accessKey,
                            secretKey
                    );
                    AmazonS3 s3client = AmazonS3ClientBuilder
                            .standard()
                            .withCredentials(new AWSStaticCredentialsProvider(credentials))
                            .withRegion(region)
                            .build();
                    if (!s3client.doesBucketExist(url.bucket)) {
                        s3client.createBucket(url.bucket);
                    }
                    s3client.putObject(url.bucket, url.key + file.getName(), file);
                    logger.info("file {} successfully uploaded to {}", file.getName(), url);
                    return true;
                } catch (Exception e) {

                    logger.error("upload failed " + file.getName() + "", e);
                }
            }
        } catch (NumberFormatException | NullPointerException e) {
            logger.error("upload failed " + file.getName() + "", e);
        }
        logger.error("upload failed " + file.getName());

        return false;
    }

    public boolean uploadCsvFile(File file, String amazonpath) throws Exception {
        final S3.Url s3Uri = new S3.Url(amazonpath);
        logger.info("I'm uploading CSV FILE " + file.getName() + " s3Uri: {}", s3Uri);
        if (this.upload(s3Uri, file)) {
            logger.info("uploadCsvFile " + file.getName() + " s3Uri {}", s3Uri);
            System.out.println(s3Uri);
            return true;
        }
        return false;
    }

    public boolean saveCsvFile(BlackListDTO blackListDTO, File file) throws Exception {
        boolean created = true;
        try (final FileWriter writer = new FileWriter(file, false)) {
            try (final CSVPrinter printer = new CSVPrinter(new BufferedWriter(writer)
                    ,
                    CSVFormat.EXCEL.withDelimiter(';').withQuoteMode(QuoteMode.MINIMAL)
//                            .withHeader(
//                            "DESCRIZIONE",
//                            "TIPO REGOLA",
//                            "SUB EDITORI",
//                            "SOCIETA'",
//                            "TIPO QUOTA DSP",
//                            "DSP PERIODO"
//                    )
            )) {
                final int cols = 6;
                logger.debug("Numero cols :" + cols);

                logger.debug("saveCsvFile of " + "BlackList " + blackListDTO.getSocietaDiTutela() + " - " + blackListDTO.getTerritorio() + " - " + blackListDTO.getDataInizioValidita() + " : output columns number: {}", cols);
                int rows = 0;
                for (BlackListDTO.Dettaglio d : blackListDTO.getRecordDettaglio()) {
                    final String[] values = new String[cols];
                    values[0] = d.getDescrizione();
                    values[1] = d.getTipoRegola();
                    values[2] = d.getSubEditori() != null ? d.getSubEditori().stream()
                            .collect(Collectors.joining(":")) : " ";
                    List<String> societaToAdd = d.getSocieta() != null ? d.getSocieta().stream().collect(Collectors.toList()) : new ArrayList<>();

                    //Con un ciclio annidato controllo che non vi siano duplicati tra societa incluse ed escluse ...
                    boolean notPresent = false;
                    if (d.getSocietaEscluse() != null && !(d.getSocietaEscluse().isEmpty())) {
                        if (d.getSocieta() != null && !(d.getSocieta().isEmpty())) {
                            for (String escluse : d.getSocietaEscluse()) {
                                notPresent = false;
                                for (String s : d.getSocieta()) {
                                    if (s.equalsIgnoreCase(escluse)) {
                                        throw new BadRequestAlertException(ErrorConstants.PARAMETERIZED_TYPE.toString(), "SAVE BLACKLIST", "errorBlacklist");
                                    } else {
                                        notPresent = true;
                                    }
                                }
                                if (notPresent)
                                    societaToAdd.add("!".concat(escluse));
                            }
                        } else {
                            for (String escluse : d.getSocietaEscluse()) {
                                societaToAdd.add("!".concat(escluse));
                            }
                        }
                    }
                    //

                    values[3] = societaToAdd != null ? societaToAdd.stream().collect(Collectors.joining(":")) : " ";
                    values[4] = d.getTipoQuotaDSP() != null ? d.getTipoQuotaDSP().stream().collect(Collectors.joining(":")) : " ";
                    values[5] = d.getDspPeriodoList() != null ? d.getDspPeriodoList().stream().collect(Collectors.joining(":")) : " ";

                    // print columns
                    for (int i = 0; i < cols; i++) {
                        printer.print(values[i]);
                    }
                    printer.println();
                    rows++;
                    if (0 == (rows % 1000)) {
                        logger.debug("I'm finishing BlackList's CSV " + blackListDTO.getSocietaDiTutela() + " - " + blackListDTO.getTerritorio() + " - " + blackListDTO.getDataInizioValidita() + " : rows {}", rows);
                    }
                }
                // footer with number of rows
//                if (rows > 0) {
//                    printer.print(rows);
//                    printer.println();
//                    logger.debug("BlackList's CSV created" + blackListDTO.getSocietaDiTutela() + " - " + blackListDTO.getTerritorio() + " - " + blackListDTO.getDataInizioValidita() + " : rows {}", rows);
//                }
            } catch (Exception e) {
                created = false;
                logger.error("ERROR while creating BlackList's CSV " + blackListDTO.getSocietaDiTutela() + " - " + blackListDTO.getTerritorio() + " - " + blackListDTO.getDataInizioValidita() + "", e);
            }
        }
        return created;
    }
}
