/*
 * @Autor A.D.C.
 */

package it.siae.gestioneblacklist.csv;


import it.siae.gestioneblacklist.dto.BlackListDTO;

import java.io.File;

public interface CsvUtilsService {
    boolean saveCsvFile(BlackListDTO blackListDTO, File file) throws Exception;

    boolean uploadCsvFile(File file,String amazonpath) throws Exception;

}
