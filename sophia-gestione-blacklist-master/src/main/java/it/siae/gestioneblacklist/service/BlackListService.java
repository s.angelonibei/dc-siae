package it.siae.gestioneblacklist.service;

import it.siae.gestioneblacklist.dto.BlackListDTO;
import it.siae.gestioneblacklist.dto.StringListDTO;

public interface BlackListService {
    boolean saveBlacklist(BlackListDTO blackListDTO) throws Exception;

    String getImportBlackList(String societa, String territorio, String dataInizioValidita) throws Exception;

    StringListDTO getAllSocietiesNames();

    StringListDTO getAllSubEditoriNames();
}
