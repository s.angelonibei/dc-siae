package it.siae.gestioneblacklist.service.impl;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.google.gson.Gson;
import it.siae.gestioneblacklist.aws.S3;
import it.siae.gestioneblacklist.csv.CsvUtilsServiceImpl;
import it.siae.gestioneblacklist.dto.BlackListDTO;
import it.siae.gestioneblacklist.dto.StringListDTO;
import it.siae.gestioneblacklist.entity.Blacklist;
import it.siae.gestioneblacklist.entity.Societa;
import it.siae.gestioneblacklist.entity.SocietaTutela;
import it.siae.gestioneblacklist.entity.SubEditori;
import it.siae.gestioneblacklist.errors.BadRequestAlertException;
import it.siae.gestioneblacklist.errors.ErrorConstants;
import it.siae.gestioneblacklist.repository.BlacklistRepository;
import it.siae.gestioneblacklist.repository.SocietaRepository;
import it.siae.gestioneblacklist.repository.SocietaTutelaRepository;
import it.siae.gestioneblacklist.repository.SubEditoriRepository;
import it.siae.gestioneblacklist.service.BlackListService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ejb.DuplicateKeyException;
import java.io.*;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class BlackListServiceImpl implements BlackListService {

    Logger logger = LoggerFactory.getLogger(BlackListServiceImpl.class);

    @Autowired(required = true)
    SocietaRepository societaRepository;

    @Autowired
    SubEditoriRepository subeditoriRepository;

    @Autowired
    BlacklistRepository blacklistRepository;

    @Autowired
    CsvUtilsServiceImpl csvUtilsServiceImpl;

    @Autowired
    SocietaTutelaRepository societaTutelaRepository;

    @Autowired
    private Environment env;

    @Value("${spring.s3uri.path}")
    public String amazonpath;

    @Value("${spring.cloud.aws.credentials.accessKey}")
    private String accessKey;

    @Value("${spring.cloud.aws.credentials.secretKey}")
    private String secretKey;

    @Value("${spring.cloud.aws.region.static}")
    private String region;

    @Value("${spring.datasource.url}")
    private String url;

    @Value("${spring.datasource.username}")
    private String user;

    @Value("${spring.datasource.password}")
    private String password;


    @Override
    public boolean saveBlacklist(BlackListDTO blackListDTO) throws Exception {
        boolean response = false;
        //CHECK IF SUB EDITOR & SOCIETA INSERITE ESISTONO A DB
        if (blackListDTO.getRecordDettaglio() != null && !(blackListDTO.getRecordDettaglio().isEmpty())) {
            for (BlackListDTO.Dettaglio b : blackListDTO.getRecordDettaglio()) {
                List<Societa> societalist = new ArrayList<>();
                List<SubEditori> edlist = new ArrayList<>();
                if (b.getSocieta() != null && !(b.getSocieta().isEmpty())) {
                    for (String st : b.getSocieta()) {
                        societalist = societaRepository.findByNomeSocieta(st);
                        if (!(societalist.size() > 0)) {
                            Societa s = new Societa(null, st);
                            societalist.add(s);
                        }
                    }
                }

                if (b.getSocietaEscluse() != null && !(b.getSocietaEscluse().isEmpty())) {
                    for (String st : b.getSocietaEscluse()) {
                        societalist = societaRepository.findByNomeSocieta(st.replace("!", ""));
                        if (!(societalist.size() > 0)) {
                            Societa s = new Societa(null, st);
                            societalist.add(s);
                        }
                    }
                }
                if (societalist != null && !(societalist.isEmpty())) {
                    logger.debug("Save of all new Societa Tutela ...");
                    societaRepository.saveAll(societalist);
                    logger.debug("Saved ...");
                }

                if (b.getSubEditori() != null && !(b.getSubEditori().isEmpty())) {
                    for (String ed : b.getSubEditori()) {
                        edlist = subeditoriRepository.findByNomeSubEditore(ed);
                        if (!(edlist.size() > 0)) {
                            SubEditori s = new SubEditori(null, ed);
                            edlist.add(s);
                        }
                    }
                    if (edlist != null && !(edlist.isEmpty())) {
                        logger.debug("Save of all new Sub Editori ...");
                        subeditoriRepository.saveAll(edlist);
                        logger.debug("Saved ...");
                    }
                }
            }
        }
        // TO DO REST OF SAVE BLACKLIST
        //  1. create CSV FILE
        String amazonUrl = null;
        //CONTROLLO SE SONO IN MODIFICA
        File csv = null;
        boolean newcsv = false;
        Blacklist blacklist = new Blacklist();
        if (blackListDTO.getNomeFileCsv() == null || blackListDTO.getNomeFileCsv().isEmpty()) {
            //NEW
            String dataforname = new SimpleDateFormat("yyyyMMdd").format(new SimpleDateFormat("dd/MM/yyyy").parse(blackListDTO.getDataInizioValidita()));
            amazonUrl = amazonpath.concat(blackListDTO.getSocietaDiTutela()).concat("/").concat(blackListDTO.getTerritorio()).concat("/").concat(dataforname).concat("/");
            newcsv = true;
            csv = new File("blacklist_" + blackListDTO.getSocietaDiTutela() + "_" + dataforname + ".csv");
            blacklist.setId(null);
            blacklist.setCountry(blackListDTO.getTerritorio());
            //Abbiamo forzato l'ora della data di validità portandolo alle ore 23 della data in input per bypassare l'UTC-1 impostato sul Db
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new SimpleDateFormat("dd/MM/yyyy").parse(blackListDTO.getDataInizioValidita()));
            calendar.add(Calendar.HOUR, 23);
            blacklist.setValidFrom(new Timestamp(calendar.getTimeInMillis()));
            blacklist.setXlsS3Url(amazonUrl.concat(csv.getName()));
            SocietaTutela sct = societaTutelaRepository.findByCodice(blackListDTO.getSocietaDiTutela());
            blacklist.setSocietaTutela(sct != null ? sct : null);
        } else {
            blacklist = blacklistRepository.findPathCsvBlacklistByParams(blackListDTO.getSocietaDiTutela(), blackListDTO.getTerritorio(), blackListDTO.getDataInizioValidita());
            if(blacklist == null){
                throw new BadRequestAlertException(ErrorConstants.CSV_NOT_FOUND_TYPE.toString(), "SAVE BLACKLIST", "errorBlacklist");
            }
            amazonUrl = blacklist.getXlsS3Url().replace(blackListDTO.getNomeFileCsv(),"");
            csv = new File(blackListDTO.getNomeFileCsv());
        }
        boolean created = csvUtilsServiceImpl.saveCsvFile(blackListDTO, csv);
        //  2. save row on BLACKLIST table with CSV PATH CREATED
        if (created) {
            // upload file to S3
            if (csv.length() > 0L) {
                logger.info("Upload FILE : " + csv.getName() + " to S3");
                if (csvUtilsServiceImpl.uploadCsvFile(csv, amazonUrl)) {
                    response = true;
                    logger.debug("BLACKLIST CSV " + csv.getName() + " COMPLETE");

                    if (newcsv) {
                        if ((blacklistRepository.findByCountryAndValidFrom(blacklist.getCountry(), blacklist.getValidFrom())) == 0) {
                            blacklistRepository.save(blacklist);
                        } else {
                            csv.delete();
                            throw new DuplicateKeyException("Duplicate BLACKLIST: Esiste già una configurazione per il periodo e il territorio scelti.");
                        }
                    }
                    csv.delete();
                }
            }

        }
        return response;
    }

    @Override
    public String getImportBlackList(String societa, String territorio, String dataInizioValidita) throws Exception {
        String date = null;
//        if (dataInizioValidita != null) {
//            try {
//                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//                SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd");
//                date = sdf2.format(sdf.parse(dataInizioValidita));
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//        }
        Blacklist blacklist = blacklistRepository.findPathCsvBlacklistByParams(societa, territorio, dataInizioValidita);
        //TO DO CSV TO BLACKLISTDTO
        Gson gson = new Gson();
        if (blacklist.getXlsS3Url() != null && !(blacklist.getXlsS3Url().isEmpty())) {
            return gson.toJson(this.leggiCsv(this.getAmazonS3File(blacklist), blacklist));
        } else {
            return gson.toJson(new Blacklist());
        }
    }

    @Override
    public StringListDTO getAllSocietiesNames() {
        StringListDTO dto = new StringListDTO();
        dto.setNames(societaRepository.findAll().stream()
                .map(Societa::getNomeSocieta)
                .collect(Collectors.toList()));
        return dto;
    }

    @Override
    public StringListDTO getAllSubEditoriNames() {
        StringListDTO dto = new StringListDTO();
        dto.setNames(subeditoriRepository.findAll().stream()
                .map(SubEditori::getNomeSubEditore)
                .collect(Collectors.toList()));
        return dto;
    }

    public BlackListDTO leggiCsv(File f, Blacklist b) throws IOException, ParseException {
//        columns = {"Descrizione", "Tipo Regole", "Sub Editori", "Societa","Tipo Quota DSP", "Dsp Periodo"}
        BufferedReader br = new BufferedReader(new FileReader(f.getName()));
        String line;
        BlackListDTO dto = new BlackListDTO();
        //Set della testata dell'oggetto
        if (b.getValidFrom() != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd/mm/yyyy");
            dto.setDataInizioValidita(sdf2.format(sdf.parse(b.getValidFrom().toString())));
        }
        dto.setTerritorio(b.getCountry());
        dto.setSocietaDiTutela(b.getSocietaTutela().getCodice());
        dto.setRecordDettaglio(new ArrayList<BlackListDTO.Dettaglio>());
        //
        logger.debug("TESTATA BLACKLIST : SOCIETA ->" + dto.getSocietaDiTutela() + " TERRITORIO ->" + dto.getTerritorio() + " DATA VALIDITA ->" + dto.getDataInizioValidita());
        logger.debug("STAMPO RIGHE DAL CSV FILE ...");
        while ((line = br.readLine()) != null) {

            //DIVIDO I VALORI DELLE COLONNE
            String[] row = line.split(";");
            BlackListDTO.Dettaglio d = new BlackListDTO.Dettaglio();
            d.setDescrizione(row[0]); // Descrizione
            d.setTipoRegola(row[1]); // Tipo Regole

            //LISTA EDITORI
            if (row[2] != null) {
                String[] editori = row[2].length() > 0 ? row[2].split(":") : new String[0];
                ;
                if (editori.length > 0) {
                    d.setSubEditori(new ArrayList<String>());
                    for (String e : editori) {
                        d.getSubEditori().add(e);
                    }
                    if (d.getSubEditori().isEmpty())
                        d.setSubEditori(null);
                }
            }
            //LISTA SOCIETA
            if (row[3] != null) {
                String[] societa = row[3].length() > 0 ? row[3].split(":") : new String[0];
                if (societa.length > 0) {
                    d.setSocieta(new ArrayList<String>());
                    d.setSocietaEscluse(new ArrayList<String>());
                    for (String s : societa) {
                        if (s.startsWith("!")) {
                            d.getSocietaEscluse().add(s.replace("!", ""));
                        } else {
                            d.getSocieta().add(s);
                        }
                    }
                    if (d.getSocieta().isEmpty())
                        d.setSocieta(null);
                    if (d.getSocietaEscluse().isEmpty())
                        d.setSocietaEscluse(null);
                }
            }
            //LISTA TIPO QUOTA DSP
            if (row[4] != null) {
                String[] tqdsp = row[4].length() > 0 ? row[4].split(":") : new String[0];
                ;
                if (tqdsp.length > 0) {
                    d.setTipoQuotaDSP(new ArrayList<String>());
                    for (String dsp : tqdsp) {
                        d.getTipoQuotaDSP().add(dsp);
                    }
                    if (d.getTipoQuotaDSP().isEmpty())
                        d.setTipoQuotaDSP(null);
                }
            }
            //LISTA DSP PERIODO
            if (row[5] != null) {
                String[] dspperiodo = row[5].split(":");
                if (dspperiodo.length > 0) {
                    d.setDspPeriodoList(new ArrayList<String>());
                    for (String p : dspperiodo) {
                        d.getDspPeriodoList().add(p);
                    }
                    if (d.getDspPeriodoList().isEmpty())
                        d.setDspPeriodoList(null);
                }
            }
            if (d != null)
                dto.getRecordDettaglio().add(d);
        }
        dto.toString();
        return dto;
    }


    // ********* UTILS METHOD *********
    public File getAmazonS3File(Blacklist b) throws Exception {
        if (b.getXlsS3Url() != null) {
            File file;
            ByteArrayOutputStream out;
            file = new File(Paths.get(b.getXlsS3Url().substring(5)).getFileName().toString());
            final S3.Url url = new S3.Url(b.getXlsS3Url());
            out = new ByteArrayOutputStream();
            AWSCredentials credentials = new BasicAWSCredentials(
                    accessKey,
                    secretKey
            );
            AmazonS3 s3client = AmazonS3ClientBuilder
                    .standard()
                    .withCredentials(new AWSStaticCredentialsProvider(credentials))
                    .withRegion(region)
                    .build();
            s3client.getObject(new GetObjectRequest(url.bucket, url.key), file);
            return file;
        } else {
            throw new Exception("Selezione non disponibile");
        }

    }
}
