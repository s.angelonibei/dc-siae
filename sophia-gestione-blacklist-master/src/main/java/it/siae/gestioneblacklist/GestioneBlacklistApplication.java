/*
 * @Autor A.D.C.
 */

package it.siae.gestioneblacklist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan
@EnableJpaRepositories
@SpringBootApplication
@EnableEurekaClient
public class GestioneBlacklistApplication {

    public static void main(String[] args)  {

        SpringApplication.run(GestioneBlacklistApplication.class, args);
    }

}
