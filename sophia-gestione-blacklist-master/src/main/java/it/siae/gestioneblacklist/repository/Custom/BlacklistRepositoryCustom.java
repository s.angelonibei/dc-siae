/*
 * @Autor A.D.C.
 */

package it.siae.gestioneblacklist.repository.Custom;

import it.siae.gestioneblacklist.entity.Blacklist;

import java.sql.Timestamp;
import java.text.ParseException;

public interface BlacklistRepositoryCustom {
    Blacklist findPathCsvBlacklistByParams(String societa, String territorio, String dataInizioValidita ) throws ParseException;
    Long findByCountryAndValidFrom(String country, Timestamp validFrom);
}
