package it.siae.gestioneblacklist.repository;

import it.siae.gestioneblacklist.entity.SocietaTutela;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SocietaTutelaRepository extends JpaRepository<SocietaTutela,Long> {
   SocietaTutela findByCodice(String codice);
}
