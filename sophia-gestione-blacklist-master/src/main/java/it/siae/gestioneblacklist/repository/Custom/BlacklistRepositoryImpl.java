/*
 * @Autor A.D.C.
 */

package it.siae.gestioneblacklist.repository.Custom;

import it.siae.gestioneblacklist.entity.Blacklist;
import it.siae.gestioneblacklist.entity.SocietaTutela;
import it.siae.gestioneblacklist.query.BlacklistProperites;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class BlacklistRepositoryImpl implements BlacklistRepositoryCustom {
    private final Logger logger = LoggerFactory.getLogger(BlacklistRepositoryImpl.class);

    @Autowired
    private BlacklistProperites blacklistProperites;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public Blacklist findPathCsvBlacklistByParams(String societa, String territorio, String dataInizioValidita) throws ParseException {
        Blacklist b = new Blacklist();
        logger.info("Request to findPathCsvBlacklistByParams : {}");
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        StringBuffer sb = new StringBuffer();
        sb.append(blacklistProperites.getAppends().getSelect());
        if (societa != null) {
            sb.append(" ").append(blacklistProperites.getAppends().getSocieta());
            namedParameters.addValue("societa", societa);
        }
        if (territorio != null) {
            sb.append(" ").append(blacklistProperites.getAppends().getTerritorio());
            namedParameters.addValue("territorio", territorio);
        }
        if (dataInizioValidita != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date date = formatter.parse(dataInizioValidita);
            String d = new SimpleDateFormat("yyyy/MM/dd").format(date);
            sb.append(" ").append(blacklistProperites.getAppends().getDataInizioValidita());
            namedParameters.addValue("dataInizioValidita", d);
        } else {
            //COSTRUISCO UNA SELECT NELLA WHERE PER CALCOLARE LA DATA MASSIMA PER LE RIGHE OTTENUTE
            sb.append(" ").append(blacklistProperites.getAppends().getDataMaxStart());
            sb.append(" ").append(blacklistProperites.getAppends().getMaxDateFrom());
            if (societa != null) {
                sb.append(" ").append(blacklistProperites.getAppends().getSocieta());
                namedParameters.addValue("societa", societa);
            }
            if (territorio != null) {
                sb.append(" ").append(blacklistProperites.getAppends().getTerritorio());
                namedParameters.addValue("territorio", territorio);
            }
            sb.append(" ").append(blacklistProperites.getAppends().getDataMaxEnd());
        }

        try {

            logger.info("findAllByFilter query {} - namedParameters {}", sb.toString(), namedParameters.getValues());
            b
                    = this.namedParameterJdbcTemplate.queryForObject(sb.toString(),
                    namedParameters, (ResultSet rs, int rowNum) -> {
                        Blacklist dto = new Blacklist();
                        dto.setId(rs.getLong("b.ID"));
                        dto.setCountry(rs.getString("b.COUNTRY"));
                        dto.setValidFrom(rs.getTimestamp("b.VALID_FROM"));
                        dto.setXlsS3Url(rs.getString("b.XLS_S3_URL"));
                        dto.setSocieta(new SocietaTutela(rs.getLong("s.ID"), rs.getString("s.CODICE")));
                        return dto;
                    });

        } catch (Exception e) {
            logger.info("searchAll, Exception " + e.getMessage());
        }

        return b;
    }

    @Override
    public Long findByCountryAndValidFrom(String country, Timestamp validFrom) {
        logger.info("Request to findByCountryAndValidFrom : {}");
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        StringBuffer sb = new StringBuffer();
        sb.append(blacklistProperites.getAppends().getCount());
        Long c = null;
        if (validFrom != null) {
            sb.append(" ").append(blacklistProperites.getAppends().getDataInizioValidita());
            namedParameters.addValue("dataInizioValidita", validFrom.toString().substring(0,10));
        } if (country != null) {
            sb.append(" ").append(blacklistProperites.getAppends().getTerritorio());
            namedParameters.addValue("territorio", country);
        }
        try {

            logger.info("findAllByFilter query {} - namedParameters {}", sb.toString(), namedParameters.getValues());
            c
                    = this.namedParameterJdbcTemplate.queryForObject(sb.toString(),
                    namedParameters, Long.class);

        } catch (Exception e) {
            logger.info("searchAll, Exception " + e.getMessage());
        }
        return c;
    }

}
