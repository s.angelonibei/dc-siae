package it.siae.gestioneblacklist.repository;

import it.siae.gestioneblacklist.entity.SubEditori;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubEditoriRepository extends JpaRepository<SubEditori,Long> {
    List<SubEditori> findByNomeSubEditore(String codice);
}
