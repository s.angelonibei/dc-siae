package it.siae.gestioneblacklist.repository;

import it.siae.gestioneblacklist.entity.Societa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SocietaRepository extends JpaRepository<Societa,Long> {
    List<Societa> findByNomeSocieta(String societa);
}
