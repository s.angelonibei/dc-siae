package it.siae.gestioneblacklist.repository;

import it.siae.gestioneblacklist.entity.Blacklist;
import it.siae.gestioneblacklist.repository.Custom.BlacklistRepositoryCustom;
import org.joda.time.DateTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Repository
public interface BlacklistRepository extends JpaRepository<Blacklist,Long>, BlacklistRepositoryCustom {

}
