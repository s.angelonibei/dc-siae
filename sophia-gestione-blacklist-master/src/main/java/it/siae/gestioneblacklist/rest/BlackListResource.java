/*
 * @Autor A.D.C.
 */

package it.siae.gestioneblacklist.rest;

import it.siae.gestioneblacklist.dto.BlackListDTO;
import it.siae.gestioneblacklist.dto.StringListDTO;
import it.siae.gestioneblacklist.entity.Blacklist;
import it.siae.gestioneblacklist.repository.BlacklistRepository;
import it.siae.gestioneblacklist.service.BlackListService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.ws.rs.QueryParam;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("blacklist")
public class BlackListResource {

    private final Logger logger = LoggerFactory.getLogger(BlackListResource.class);

    @Autowired
    BlackListService service;

    @Autowired
    BlacklistRepository blacklistRepository;

    @PostMapping("/addBlacklist")
    @Description("Api for create report csv with new Blacklists")
    public boolean createReportRequest(@Valid @RequestBody BlackListDTO blackListDTO) throws Exception {
        logger.info("REST request to create report csv with new Blacklists: {} ", blackListDTO);
        return this.service.saveBlacklist(blackListDTO);
    }

    @GetMapping("/getBlackList")
    @Description("Api for get all BlackList ordered by id desc")
    public List<Blacklist> getAllRequest() {
        logger.info("REST request to find all BlackList ordered by id desc : {} ");
        List<Blacklist> b = (this.blacklistRepository.findAll()
                .stream()
                .sorted(Comparator.comparing(Blacklist::getId)
                        .reversed())
                .collect(Collectors.toList()));
        return b;
    }

    //TODO PUT IMPORT BLACKLIST
    @GetMapping("/importBlackList")
    @Description("Api for get a BlackList filtered by params")
    public String getBlacklistDTO(@QueryParam("societa") String societa, @QueryParam("territorio") String territorio, @QueryParam("dataInizioValidita") String dataInizioValidita) throws Exception {
        logger.info("REST request to find a BlackList filtered by params : {} ", societa, territorio, dataInizioValidita);
        String b = service.getImportBlackList(societa, territorio, dataInizioValidita);
        return b;
    }

    @GetMapping("societa")
    @Description("Api for get all Blacklist's societies")
    public StringListDTO getAllSocietiesNames() {
        return service.getAllSocietiesNames();
    }

    @GetMapping("subeditori")
    @Description("Api for get all Blacklist's sub-editori")
    public StringListDTO getAllSubEditoriNames() {
        return service.getAllSubEditoriNames();
    }

}
