package com.alkemytech.sophia.identification.index;


import java.io.IOException;
import java.util.Properties;

import com.alkemytech.sophia.commons.index.ReverseIndexImpl;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class SiaeReverseIndex extends ReverseIndexImpl {
	
	@Inject
	public SiaeReverseIndex(@Named("configuration") Properties configuration) throws IOException {
		super(configuration, "siae_index");
	}

}


