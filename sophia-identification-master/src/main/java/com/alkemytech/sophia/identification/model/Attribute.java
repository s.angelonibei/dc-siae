package com.alkemytech.sophia.identification.model;

import com.google.gson.GsonBuilder;
import com.sleepycat.persist.model.Persistent;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@Persistent
public class Attribute {

	public int key;
	public String value;
	public int origin;

	public Attribute() {
		super();
	}
	
	public Attribute(int key, String value, int origin) {
		super();
		this.key = key;
		this.value = value;
		this.origin = origin;
	}

	@Override
	public int hashCode() {
		int result = 31 + key;
		result = 31 * result + origin;
		return 31 * result + (null == value ? 0 : value.hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (null == obj) {
			return false;
		} else if (getClass() != obj.getClass()) {
			return false;
		}
		final Attribute other = (Attribute) obj;
		if (key != other.key) {
			return false;
		}
		if (origin != other.origin) {
			return false;
		}
		if (null == value) {
			if (null != other.value) {
				return false;
			}
		} else if (!value.equals(other.value)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.disableHtmlEscaping()
				.create()
				.toJson(this);
	}

}
