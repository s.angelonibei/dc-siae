package com.alkemytech.sophia.identification;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

import javax.sql.DataSource;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.collecting.AdaCode;
import com.alkemytech.sophia.commons.collecting.SiaeCode;
import com.alkemytech.sophia.commons.collecting.UUIDCode;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.index.ReverseIndex;
import com.alkemytech.sophia.commons.spelling.NumberSpeller;
import com.alkemytech.sophia.commons.sqs.McmdbMessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.text.TextNormalizer;
import com.alkemytech.sophia.commons.text.TextTokenizer;
import com.alkemytech.sophia.commons.util.BigDecimals;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.identification.collecting.CollectingDatabase;
import com.alkemytech.sophia.identification.document.DocumentStore;
import com.alkemytech.sophia.identification.guice.IndexModule;
import com.alkemytech.sophia.identification.guice.TextProcessingModule;
import com.alkemytech.sophia.identification.index.ReverseIndexMetadata;
import com.alkemytech.sophia.identification.jdbc.McmdbDataSource;
import com.alkemytech.sophia.identification.model.Artist;
import com.alkemytech.sophia.identification.model.ArtistRole;
import com.alkemytech.sophia.identification.model.Attribute;
import com.alkemytech.sophia.identification.model.AttributeKey;
import com.alkemytech.sophia.identification.model.Category;
import com.alkemytech.sophia.identification.model.Code;
import com.alkemytech.sophia.identification.model.CodeType;
import com.alkemytech.sophia.identification.model.CollectingCode;
import com.alkemytech.sophia.identification.model.Document;
import com.alkemytech.sophia.identification.model.Origin;
import com.alkemytech.sophia.identification.model.Text;
import com.alkemytech.sophia.identification.model.TextArtist;
import com.alkemytech.sophia.identification.model.TextType;
import com.alkemytech.sophia.identification.model.Title;
import com.alkemytech.sophia.identification.model.TitleType;
import com.alkemytech.sophia.identification.model.Work;
import com.alkemytech.sophia.identification.work.WorkDatabase;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

public class IndexUpdaterMultimediale {

	private static final Logger logger = LoggerFactory.getLogger(IndexUpdater.class);

	private static class GuiceModuleExtension extends GuiceModule {

		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
//			bind(DynamoDb.class)
//			.toInstance(new DynamoDb(configuration));
			bind(S3.class)
				.toInstance(new S3(configuration));
			bind(SQS.class)
				.toInstance(new SQS(configuration));
			
			// work database
			bind(WorkDatabase.class)
				.to(classForPrefix(WorkDatabase.class,
						configuration.getProperty("work_database")))
				.asEagerSingleton();
			// collecting database
			bind(CollectingDatabase.class)
				.to(classForPrefix(CollectingDatabase.class,
						configuration.getProperty("collecting_database")))
				.asEagerSingleton();
			// document store
			bind(DocumentStore.class)
				.to(classForPrefix(DocumentStore.class,
						configuration.getProperty("document_store")))
				.asEagerSingleton();
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("MCMDB"))
				.to(McmdbDataSource.class)
				.asEagerSingleton();
			// other binding(s)
			bind(MessageDeduplicator.class)
				.to(McmdbMessageDeduplicator.class);
			// self
			bind(IndexUpdaterMultimediale.class)
				.asEagerSingleton();
		}

	}

	public static void main(String[] args) {
		try {			
			final IndexUpdaterMultimediale instance =
					Guice
					.createInjector(new GuiceModuleExtension(args,
							"/index-updater.properties"),
							new IndexModule(args,"/index-updater.properties"),
							new TextProcessingModule())
					.getInstance(IndexUpdaterMultimediale.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Properties configuration;
	private final Charset charset;
	//private final DynamoDb dynamoDb;
	private final S3 s3;
	private final SQS sqs;
	private final WorkDatabase workDatabase;
	private final CollectingDatabase collectingDatabase;
	private final DocumentStore documentStore;
	private final Provider<ReverseIndex> iswcIndex;
	private final Provider<ReverseIndex> isrcIndex;
	private final Provider<ReverseIndex> titleIndex;
	private final Provider<ReverseIndex> artistIndex;
	private final Provider<ReverseIndex> uuidIndex;
	private final Provider<ReverseIndex> siaeIndex;
	private final Provider<ReverseIndex> adaIndex;
	private final Provider<TextNormalizer> iswcNormalizer;
	private final Provider<TextNormalizer> isrcNormalizer;
	private final Provider<TextNormalizer> titleNormalizer;
	private final Provider<TextNormalizer> artistNormalizer;
	private final Provider<TextNormalizer> uuidNormalizer;
	private final Provider<TextTokenizer> titleTokenizer;
	private final Provider<TextTokenizer> artistTokenizer;
	private final MessageDeduplicator messageDeduplicator;
	
	@Inject
	protected IndexUpdaterMultimediale(@Named("configuration") Properties configuration,
			@Named("charset") Charset charset,
			//DynamoDb dynamoDb,
			S3 s3, 
			SQS sqs, 
			WorkDatabase workDatabase,
			CollectingDatabase collectingDatabase,
			DocumentStore documentStore,
			@Named("iswc_index") Provider<ReverseIndex> iswcIndex,
			@Named("isrc_index") Provider<ReverseIndex> isrcIndex,
			@Named("title_index") Provider<ReverseIndex> titleIndex,
			@Named("artist_index") Provider<ReverseIndex> artistIndex,
			@Named("uuid_index") Provider<ReverseIndex> uuidIndex,
			@Named("siae_index") Provider<ReverseIndex> siaeIndex,
			@Named("ada_index") Provider<ReverseIndex> adaIndex,
			@Named("iswc_normalizer") Provider<TextNormalizer> iswcNormalizer,
			@Named("isrc_normalizer") Provider<TextNormalizer> isrcNormalizer,
			@Named("uuid_normalizer") Provider<TextNormalizer> uuidNormalizer,
			@Named("title_normalizer") Provider<TextNormalizer> titleNormalizer,
			@Named("artist_normalizer") Provider<TextNormalizer> artistNormalizer,
			@Named("title_tokenizer") Provider<TextTokenizer> titleTokenizer,
			@Named("artist_tokenizer") Provider<TextTokenizer> artistTokenizer,
			MessageDeduplicator messageDeduplicator) {
		super();
		this.configuration = configuration;
		this.charset = charset;
		this.s3 = s3;
		this.sqs = sqs;
		//this.dynamoDb = dynamoDb;
		this.workDatabase = workDatabase;
		this.collectingDatabase = collectingDatabase;
		this.documentStore = documentStore;
		this.iswcIndex = iswcIndex;
		this.isrcIndex = isrcIndex;
		this.titleIndex = titleIndex;
		this.artistIndex = artistIndex;
		this.siaeIndex = siaeIndex;
		this.adaIndex = adaIndex;
		this.iswcNormalizer = iswcNormalizer;
		this.isrcNormalizer = isrcNormalizer;
		this.uuidIndex = uuidIndex;
		this.titleNormalizer = titleNormalizer;
		this.artistNormalizer = artistNormalizer;
		this.titleTokenizer = titleTokenizer;
		this.artistTokenizer = artistTokenizer;
		this.uuidNormalizer = uuidNormalizer;
		this.messageDeduplicator = messageDeduplicator;
	}

	public IndexUpdaterMultimediale startup() throws SQLException, IOException {
		if ("true".equalsIgnoreCase(configuration
				.getProperty("index_updater.locked", "true"))) {
			throw new IllegalStateException("application locked");
		}
		s3.startup();
		sqs.startup();
		//dynamoDb.startup();
		workDatabase.startup();
		collectingDatabase.startup();
		documentStore.startup();
		return this;
	}

	public IndexUpdaterMultimediale shutdown() {
		documentStore.shutdown();
		collectingDatabase.shutdown();
		workDatabase.shutdown();
		sqs.shutdown();
		s3.shutdown();
		return this;
	}

	private HashSet<Text> normalizeWork(Work work, Set<Integer> includeOrigins) {
		final HashSet<Text> texts = new HashSet<Text>();
		// title(s)
		int titles = 0;
		if (null != work.titles) {
			for (Title title : work.titles) {
				if (includeOrigins.contains(title.origin)) {
					final String text = titleNormalizer.get()
							.normalize(title.text);
					if (!Strings.isNullOrEmpty(text)) {
						texts.add(new Text(text, TextType.title));
						titles ++;
					}
				}
			}
		}
		// artist(s)
		int artists = 0;
		if (null != work.artists) {
			for (Artist artist : work.artists) {
				if (includeOrigins.contains(artist.origin)) {
					final String text = artistNormalizer.get()
							.normalize(artist.text);
					if (!Strings.isNullOrEmpty(text)) {
						texts.add(new TextArtist(text, artist.ipiNumber, TextType.artist));
						artists ++;
					}
				}
			}
		}
		// code(s)
		int codes = 0;
		if (null != work.codes) {
			for (Code code : work.codes) {
				if (includeOrigins.contains(code.origin)) {
					if (CodeType.iswc == code.type) { // iswc(s)
						final String text = iswcNormalizer.get().normalize(code.text);
						if (!Strings.isNullOrEmpty(text)) {
							texts.add(new Text(text, TextType.iswc));
							codes ++;
						}
					} else if (CodeType.isrc == code.type) { // isrc(s)
						final String text = isrcNormalizer.get().normalize(code.text);
						if (!Strings.isNullOrEmpty(text)) {
							texts.add(new Text(text, TextType.isrc));
							codes ++;
						}
					} else if (CodeType.uuid == code.type) { // isrc(s)
						final String text = uuidNormalizer.get().normalize(code.text);
						if (!Strings.isNullOrEmpty(text)) {
							texts.add(new Text(text, TextType.uuid));
							codes ++;
						}
					}
				}
			}
		}
		int codOpere = 0;		
		if (null != work.attributes) {
			for (Attribute attribute : work.attributes) {
				if (includeOrigins.contains(attribute.origin)) {
					if (AttributeKey.codice_siae == attribute.key) { // codiceSiae(s)
						final String text =  SiaeCode.normalize(attribute.value);
						if (!Strings.isNullOrEmpty(text)) {
							texts.add(new Text(text, TextType.codiceSiae));
							codOpere ++;
						}
					} else if (AttributeKey.codice_ada == attribute.key) { // codiceAda(s)
						final String text = AdaCode.normalize(attribute.value);
						if (!Strings.isNullOrEmpty(text)) {
							texts.add(new Text(text, TextType.codiceAda));
							codOpere ++;
						}
					}
				}
			}
		}
		
		
		if (0 == titles || 0 == artists) {
			logger.debug("invalid work {} titles, {} artists, {} codes, {} codOpere", titles, artists, codes, codOpere);
			logger.debug("  {}", work);
			return null;
		}
		return texts;
	}

	private IndexUpdaterMultimediale process(String[] args) throws Exception {

		// bind lock tcp port
		final int bindPort = Integer.parseInt(configuration.getProperty("index_updater.bind_port",
				configuration.getProperty("default.bind_port", "0")));
		try (ServerSocket socket = new ServerSocket(bindPort)) {
			logger.debug("socket bound to {}", socket.getLocalSocketAddress());
			// standalone mode
			if (args.length > 1 && "standalone".equals(args[1])) {
				logger.debug(args[0] + ":" + args[1]);
				logger.debug(configuration.getProperty("index_builder.standalone_json"));
				final JsonObject input = GsonUtils.fromJson(configuration.getProperty("index_builder.standalone_json"), JsonObject.class);
				final JsonObject output = new JsonObject();
				logger.debug("input {}", new GsonBuilder().setPrettyPrinting().create().toJson(input));
				processMessage(input, output);
				logger.debug("output {}", new GsonBuilder().setPrettyPrinting().create().toJson(output));
			}else if (args.length > 1 && "event".equals(args[1])){
				final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "index_updater.sqs");
				sqsMessagePump.pollingLoop(messageDeduplicator, new SqsMessagePump.Consumer() {

					private JsonObject output;
					private JsonObject error;

					@Override
					public JsonObject getStartedMessagePayload(JsonObject message) {
						output = null;
						error = null;
						return SqsMessageHelper.formatContext();
					}

					@Override
					public boolean consumeMessage(JsonObject message) {
						try {
							output = new JsonObject();
							processMessage(message, output);
							error = null;
							return true;
						} catch (Exception e) {
							output = null;
							error = SqsMessageHelper.formatError(e);
							return false;
						}
					}

					@Override
					public JsonObject getCompletedMessagePayload(JsonObject message) {
						return output;
					}

					@Override
					public JsonObject getFailedMessagePayload(JsonObject message) {
						return error;
					}

				});
			}
		}
		return this;

	}

	private void processMessage(JsonObject input, JsonObject output) throws Exception {

//		{
//		  "header": {
//		    "uuid": "6479449c-221d-4600-92c1-1c8d1f7a6xxx",
//		    "timestamp": "2018-09-27T00:00:00.000Z",
//		    "queue": "{env}_to_process_core_index_updater",
//		    "sender": "aws-console"
//		  },
//		  "body": {
//		    "force": false,
//		    "__comment": "optional additional field(s) will be echoed to other sqs messages inside \"input\" field"
//		  }
//		}

		final long startTimeMillis = System.currentTimeMillis();

		// parse input json message
		final JsonObject inputBody = input.getAsJsonObject("body");
		final boolean force = GsonUtils.getAsBoolean(inputBody, "force", false);
		logger.debug("force {}", force);

		// configuration
		final File latestFile = new File(configuration.getProperty("index_updater.latest_file", "_LATEST"));
		final String outputS3FolderUrl = configuration.getProperty("index_updater.output.s3_folder_url");
		final File localFolder = new File(configuration.getProperty("index_updater.local_folder"));
		final File resumeFile = new File(configuration.getProperty("index_updater.resume_file"));
		final String dumpFolderUrl = GsonUtils.getAsString(inputBody, "dump", null);
		final int threadCount = Integer.parseInt(configuration.getProperty("index_updater.thread_count", "1"));
		 
		
		// load last processed file
		Map<String, String> lastProcessed = this.readResume(resumeFile);
		//logger.debug("lastProcessed {}", lastProcessed);

		final List<String> unprocessedUlisseFileUrls = getUnprocessedMultimedialeFileUrls(lastProcessed,dumpFolderUrl);
		logger.debug("unprocessedFileUrls {}", unprocessedUlisseFileUrls);

		// return immediately if no unprocessed file(s) exist and no force flag
		if (!force && unprocessedUlisseFileUrls.isEmpty()) {
			// output
			output.addProperty("startTime", DateFormat
					.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM)
						.format(new Date(startTimeMillis)));
			output.addProperty("totalDuration", TextUtils
					.formatDuration(System.currentTimeMillis() - startTimeMillis));
			output.addProperty("additionalInfo", String
					.format("no unprocessed json file(s) exists, last processed file %s", lastProcessed));
			logger.info("no unprocessed json file(s) exists, last processed file {}", lastProcessed);
			logger.debug("message processed in {}",
					TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
			return;
		}

		// check index _LATEST version
		final String latestS3FileUrl = new StringBuilder()
				.append(outputS3FolderUrl)
				.append('/')
				.append(latestFile.getName())
				.toString();
		final String latestVersion = s3
				.getObjectContents(new S3.Url(latestS3FileUrl), charset);
		logger.debug("latestVersion {}", latestVersion);
		final int indexVersion = Strings.isNullOrEmpty(latestVersion) ?
				1 : 1 + Integer.parseInt(latestVersion);
		logger.debug("indexVersion {}", indexVersion);
		// actual s3 output folder
		final String versionedOutputS3FolderUrl = new StringBuilder()
				.append(outputS3FolderUrl)
				.append('/')
				.append(indexVersion)
				.toString();
		logger.debug("versionedOutputS3FolderUrl {}", versionedOutputS3FolderUrl);

		// index metadata
		final ReverseIndexMetadata metadata = new ReverseIndexMetadata(localFolder);
		logger.debug("metadata version {}", metadata
				.getProperty(ReverseIndexMetadata.VERSION, null));
		metadata.setProperty(ReverseIndexMetadata.VERSION,
				Integer.toString(indexVersion));
		metadata.commit();

		// parse dynamodb
		dynamoJsonParser(unprocessedUlisseFileUrls,output);


		// create document(s)
		documentsBuilder(output);

		// create iswc(s) index
		if ("true".equalsIgnoreCase(configuration.getProperty("index_updater.uuid.create")))
			createIndex(uuidIndex.get(), TextType.uuid, output);

		// create iswc(s) index
		if ("true".equalsIgnoreCase(configuration.getProperty("index_updater.iswc.create")))
			createIndex(iswcIndex.get(), TextType.iswc, output);

		// create isrc(s) index
		if ("true".equalsIgnoreCase(configuration.getProperty("index_updater.isrc.create")))
			createIndex(isrcIndex.get(), TextType.isrc, output);

		// create title(s) index
		if ("true".equalsIgnoreCase(configuration.getProperty("index_updater.title.create")))
			createIndex(titleIndex.get(), TextType.title, titleTokenizer.get(), output);

		// create artist(s) index
		if ("true".equalsIgnoreCase(configuration.getProperty("index_updater.artist.create")))
			createIndex(artistIndex.get(), TextType.artist, artistTokenizer.get(), output);

		// create codiceSiae(s) index
		if ("true".equalsIgnoreCase(configuration.getProperty("index_updater.siae.create")))
			createIndex(siaeIndex.get(), TextType.codiceSiae, output);
		// create codiciAda(s) index
		if ("true".equalsIgnoreCase(configuration.getProperty("index_updater.ada.create")))
			createIndex(adaIndex.get(), TextType.codiceAda, output);
		
		// upload nosql files to s3 folder
//*******************INIZIO upload kb		
		final AtomicInteger uploadedFiles = new AtomicInteger(0);
		final AtomicLong outputSize = new AtomicLong(0L);
		if ("true".equalsIgnoreCase(configuration
				.getProperty("index_updater.upload_to_s3", "true"))) {

			// delete dirty files in s3 output folder
			logger.debug("deleting existing files in {}", versionedOutputS3FolderUrl);
			deleteS3FolderContents(versionedOutputS3FolderUrl);

			// upload to s3 folder
			logger.debug("uploading nosql files to {}", versionedOutputS3FolderUrl);
			final ConcurrentLinkedDeque<File> files = new ConcurrentLinkedDeque<>();
			files.addAll(Arrays.asList(localFolder.listFiles()));
			for (final Iterator<File> iterator = files.iterator(); iterator.hasNext(); ) {
				final File file = iterator.next();
				if (file.isDirectory()) {
					iterator.remove();
					files.addAll(Arrays.asList(file.listFiles()));
				}
			}
			final Iterator<File> iterator = files.iterator();

			// start upload thread(s)
			final Object monitor = new Object();
			final ExecutorService executorService = Executors.newCachedThreadPool();
			final AtomicInteger runningThreads = new AtomicInteger(0);
			final AtomicReference<Exception> threadException = new AtomicReference<>(null);
			while (runningThreads.get() < threadCount) {
				logger.debug("starting thread {}", runningThreads.incrementAndGet());
				executorService.execute(new Runnable() {

					@Override
					public void run() {
						try {
							while (null == threadException.get()) {
								final File file;
								synchronized (iterator) {
									if (!iterator.hasNext())
										return;
									file = iterator.next();
								}
								// compute index size on disk
								outputSize.addAndGet(file.length());
								// upload database file
								String relativePath = file.getAbsolutePath()
										.substring(localFolder
												.getAbsolutePath().length());
								while (relativePath.startsWith("/"))
									relativePath = relativePath.substring(1);
								final String s3FileUrl = new StringBuilder()
										.append(versionedOutputS3FolderUrl)
										.append('/')
										.append(relativePath)
										.toString();
								if (!s3.upload(new S3.Url(s3FileUrl), file)) {
									logger.error("error uploading file {} to {}", relativePath, s3FileUrl);
									throw new IOException(String
											.format("error uploading file %s to %s", relativePath, s3FileUrl));
								} else {
									logger.debug("file {} uploaded to {}", relativePath, s3FileUrl);
									uploadedFiles.incrementAndGet();
								}
							}
						} catch (Exception e) {
							logger.error("run", e);
							threadException.compareAndSet(null, e);
						} finally {
							logger.debug("thread {} finished", runningThreads.getAndDecrement());
							synchronized (monitor) {
								monitor.notify();
							}
						}
					}
				});
			}

			// join upload thread(s)
			while (runningThreads.get() > 0) {
				synchronized (monitor) {
					monitor.wait(1000L);
				}
			}

			// re-throw thread exception
			if (null != threadException.get())
				throw threadException.getAndSet(null);

			// create & upload index latest file
			try (final FileWriter writer = new FileWriter(latestFile)) {
				writer.write(Integer.toString(indexVersion));
			}
			latestFile.deleteOnExit();
			if (!s3.upload(new S3.Url(latestS3FileUrl), latestFile)) {
				logger.error("error uploading file {} to {}", latestFile.getName(), latestS3FileUrl);
				throw new IOException(String
						.format("error uploading file %s to %s", latestFile.getName(), latestS3FileUrl));
			} else {
				logger.debug("file {} uploaded to {}", latestFile.getName(), latestS3FileUrl);
			}
			latestFile.delete();
		}

//*******************fine upload kb		
		
		output.addProperty("uploadedFiles", uploadedFiles.get());
		output.addProperty("outputSizeInBytes", outputSize.get());
		output.addProperty("outputSize", TextUtils.formatSize(outputSize.get()));
		output.addProperty("startTime", DateFormat
				.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM)
					.format(new Date(startTimeMillis)));
		output.addProperty("totalDuration", TextUtils
				.formatDuration(System.currentTimeMillis() - startTimeMillis));

		logger.debug("message processed in {}", TextUtils
				.formatDuration(System.currentTimeMillis() - startTimeMillis));

	}

	private void documentsBuilder(JsonObject output) throws Exception {

		// configuration
		final Set<Integer> includeOrigins = new HashSet<>();
		for (String origin : configuration
				.getProperty("index_updater.include_origins").split(","))  {
			includeOrigins.add(Origin.valueOf(origin));
		}
		final HeartBeat heartbeat = HeartBeat.constant("documents",
				Integer.parseInt(configuration.getProperty("index_updater.heartbeat",
						configuration.getProperty("default.heartbeat", "1000"))));

		logger.debug("creating document(s)");
		final AtomicLong totalWorks = new AtomicLong(0L);
		final AtomicLong skippedWorks = new AtomicLong(0L);
		final long startTimeMillis = System.currentTimeMillis();

		documentStore.truncate();
		collectingDatabase.selectOrderByWeight(new CollectingCode.Selector() {

			private final AtomicLong documentId = new AtomicLong(0L);

			@Override
			public void select(CollectingCode collecting) throws Exception {
				totalWorks.incrementAndGet();
				final Work work = workDatabase.getById(collecting.workId);
				final HashSet<Text> texts = normalizeWork(work, includeOrigins);
				if (null != texts && !texts.isEmpty()) {
					final Document document = new Document(documentId
							.incrementAndGet(), work.id, texts);
					documentStore.upsert(document);
				} else {
					skippedWorks.incrementAndGet();
				}
				heartbeat.pump();
			}

		}, false); // largest weight first

		logger.debug("{} processed collecting code(s)", heartbeat.getTotalPumps());
		logger.info("skipped {} works", skippedWorks);
		logger.info("document(s) created in {}",TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		// processing stats
		final JsonObject processingStats = new JsonObject();
		processingStats.addProperty("totalWorks", totalWorks.get());
		processingStats.addProperty("skippedWorks", skippedWorks.get());
		processingStats.addProperty("documentsCreationDuration",TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		// output
		output.add("documentsBuilder", processingStats);
	}

	private void createIndex(ReverseIndex reverseIndex, final int textType, JsonObject output) throws Exception {

		final HeartBeat heartbeat = HeartBeat.constant(TextType.toString(textType),
				Integer.parseInt(configuration.getProperty("index_updater.heartbeat",
						configuration.getProperty("default.heartbeat", "1000"))));

		logger.debug("adding terms to {} index...", TextType.toString(textType));
		long startTimeMillis = System.currentTimeMillis();
		final AtomicLong totalDocuments = new AtomicLong(0L);
		final AtomicLong totalTexts = new AtomicLong(0L);

		final ReverseIndex.Editor editor = reverseIndex.edit();
		editor.rewind();
		documentStore.select(new Document.Selector() {

			@Override
			public void select(Document document) throws IOException {
				totalDocuments.incrementAndGet();
				for (Text text : document.texts) {
					if (textType == text.type) {
						editor.add(text.text, document.id);
						totalTexts.incrementAndGet();
					}
				}
				heartbeat.pump();
			}

		});
		logger.debug("{} processed documents", heartbeat.getTotalPumps());
		logger.info("terms added to {} index in {}", TextType.toString(textType),
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		// processing stats
		final JsonObject processingStats = new JsonObject();
		processingStats.addProperty("totalDocuments", totalDocuments.get());
		processingStats.addProperty("totalTexts", totalTexts.get());
		processingStats.addProperty("indexCreationDuration",
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		logger.debug("sorting postings of {} index...", TextType.toString(textType));
		startTimeMillis = System.currentTimeMillis();
		editor.commit();
		editor.rewind();
		logger.info("{} index postings saved in {}", TextType.toString(textType),
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
		processingStats.addProperty("postingsSortingDuration",
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		// output
		output.add(TextType.toString(textType) + "IndexBuilder", processingStats);

	}

	private void createIndex(ReverseIndex reverseIndex, final int textType, final TextTokenizer tokenizer, JsonObject output) throws Exception {

		final NumberSpeller numberSpeller = "true".equalsIgnoreCase(configuration
				.getProperty("index_updater." + textType + ".spell_numbers")) ?
						new NumberSpeller(configuration, "number_speller") : null;
		final HeartBeat heartbeat = HeartBeat.constant(TextType.toString(textType),
				Integer.parseInt(configuration.getProperty("index_updater.heartbeat",
						configuration.getProperty("default.heartbeat", "1000"))));

		logger.debug("adding terms to {} index...", TextType.toString(textType));
		long startTimeMillis = System.currentTimeMillis();
		final AtomicLong totalDocuments = new AtomicLong(0L);
		final AtomicLong totalTexts = new AtomicLong(0L);
		final AtomicLong totalSpelled = new AtomicLong(0L);

		final ReverseIndex.Editor editor = reverseIndex.edit();
		editor.rewind();
		documentStore.select(new Document.Selector() {

			@Override
			public void select(Document document) throws IOException {
				totalDocuments.incrementAndGet();
				tokenizer.clear();
				for (Text text : document.texts) {
					if (textType == text.type) {
						tokenizer.tokenize(text.text);
					}
					if (null != numberSpeller) {
						for (String spelled : numberSpeller.spell(text.text)) {
							tokenizer.tokenize(spelled);
							totalSpelled.incrementAndGet();
						}
					}
				}
				final Collection<String> tokens = tokenizer.getTokens();
				editor.addAll(tokens, document.id);
				totalTexts.addAndGet(tokens.size());
				heartbeat.pump();
			}

		});
		logger.debug("{} processed documents", heartbeat.getTotalPumps());
		logger.info("terms added to {} index in {}", TextType.toString(textType),
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		// processing stats
		final JsonObject processingStats = new JsonObject();
		processingStats.addProperty("totalDocuments", totalDocuments.get());
		processingStats.addProperty("totalTexts", totalTexts.get());
		processingStats.addProperty("totalSpelled", totalSpelled.get());
		processingStats.addProperty("indexCreationDuration",
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		logger.debug("sorting postings of {} index...", TextType.toString(textType));
		startTimeMillis = System.currentTimeMillis();
		editor.commit();
		editor.rewind();
		logger.info("{} index postings saved in {}", TextType.toString(textType),
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
		processingStats.addProperty("postingsSortingDuration",
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		// output
		output.add(TextType.toString(textType) + "IndexBuilder", processingStats);

	}


	//////////////////////////7
	private void dynamoJsonParser(List<String> s3FileUrls,JsonObject output) throws IOException {
		final int s3Retries = Integer.parseInt(configuration.getProperty("index_updater.s3_retries", "1"));
		final File resumeFile = new File(configuration.getProperty("index_updater.resume_file"));

		final int origin = Origin.valueOf(configuration.getProperty("index_updater.origin"));
		final boolean overwriteExisting = "true".equalsIgnoreCase(configuration.getProperty("index_updater.overwrite_existing"));

		final String codeField                	= configuration.getProperty("multimediale_feed_parser.field.code_siae");
		final String iswcField 					= configuration.getProperty("multimediale_feed_parser.field.code_iswc");
		final String isrcField 					= configuration.getProperty("multimediale_feed_parser.field.code_isrc");
		final String elField 					= configuration.getProperty("multimediale_feed_parser.field.flag_el");
		final String pdField 					= configuration.getProperty("multimediale_feed_parser.field.flag_pd");
		final String irregolareField 			= configuration.getProperty("multimediale_feed_parser.field.flagIrregolare");
		final String scopeField 				= configuration.getProperty("multimediale_feed_parser.field.scope");
		final String weightField 				= configuration.getProperty("multimediale_feed_parser.field.relevance");
		final String accruedField 				= configuration.getProperty("multimediale_feed_parser.field.accrued");
		final String riskField 				    = configuration.getProperty("multimediale_feed_parser.field.risk");
		final String statusField 				= configuration.getProperty("multimediale_feed_parser.field.status");


		//TITOLO ORIGINARII
		final String originalTitleFields    	= configuration.getProperty("multimediale_feed_parser.field.original_title");

		//TITOLO ALTERNATIVI
		final String alternativeTitleFields 	= configuration.getProperty("multimediale_feed_parser.field.alternative_title");

		//AUTORI - COMPOSITORI
		final String authorSiaeField 				= configuration.getProperty("multimediale_feed_parser.field.autore");
		final String authorSiaeNameField 			= configuration.getProperty("multimediale_feed_parser.field.autore.nominativo");
		final String  authorSiaeIpiNumberField		= configuration.getProperty("multimediale_feed_parser.field.autore.ipi");
		final String composerSiaeField 				= configuration.getProperty("multimediale_feed_parser.field.compositori");
		final String composerSiaeNameField 			= configuration.getProperty("multimediale_feed_parser.field.compositori.nominativo");
		final String composerSiaeIpiNumberField		= configuration.getProperty("multimediale_feed_parser.field.compositori.ipi");

		//INTERPRETI
		final String performerField 			= configuration.getProperty("multimediale_feed_parser.field.performer");

		final Map<String,String> artistOriginMapping 			= GsonUtils.decodeJsonMap(configuration.getProperty("multimediale_feed_parser.roles.artist_mapping"));

		final HeartBeat heartbeat = HeartBeat.constant("json",Integer.parseInt(configuration.getProperty("index_updater.heartbeat",configuration.getProperty("default.heartbeat", "1000"))));

		logger.debug("parsing ulisse json file(s)");
		long startTimeMillis = System.currentTimeMillis();

		// stats
		final AtomicLong totalRecords = new AtomicLong(0L);
		final AtomicLong invalidRecords = new AtomicLong(0L);
		final AtomicLong insertedWorks = new AtomicLong(0L);
		final AtomicLong modifiedWorks = new AtomicLong(0L);
		final AtomicLong codePrimaryKey = new AtomicLong(1L);

		// process s3 file(s)
		int s3Retry = 0;
		for (String s3FileUrl : s3FileUrls) {
			for (; s3Retry < s3Retries; s3Retry ++) {
				try {
					logger.debug("processing file {}", s3FileUrl);
					final S3Object object = s3.getObject(new S3.Url(s3FileUrl));
					try (final S3ObjectInputStream inputStream = object.getObjectContent();
							final GZIPInputStream gzipInputStream = new GZIPInputStream(inputStream);
							final InputStreamReader inputStreamReader = new InputStreamReader(gzipInputStream);
							final BufferedReader bufferedReader = new BufferedReader(inputStreamReader)) {
						for (String line; null != (line = bufferedReader.readLine()); ){
							totalRecords.incrementAndGet();
							final JsonObject jsonObject = GsonUtils.fromJson(line, JsonObject.class);
							JsonElement jsonElement;

							// siae code
							String code = codePrimaryKey.incrementAndGet()+"";
							
//							jsonElement = jsonObject.get(codeField);
//							if (null != jsonElement) {
//								String text = UUIDCode.normalize(jsonElement.getAsString());
//								if (!Strings.isNullOrEmpty(text)) {
//									code = text;
//								}
//							}
//							asdasd
							// codici opere
//							jsonElement = jsonObject.get("codiciOpera");
//							if (null != jsonElement) {
//								for (JsonElement jsonNameText : jsonElement.getAsJsonArray()) {
//									JsonElement jsonCodiceText = jsonNameText.getAsJsonObject().get("codice");
//									JsonElement jsonSocietaText = jsonNameText.getAsJsonObject().get("societa");
//									if("SIAE".equals(jsonSocietaText.getAsString())){
//										String text = SiaeCode.normalize(jsonCodiceText.getAsString());
//										if (!Strings.isNullOrEmpty(text)) {
//											code = text;
//										}										
//									}
//									
//								}
//							}

							
							
							if (null == code) {
								//logger.warn("missing code: {}", jsonObject);
								invalidRecords.incrementAndGet();
								continue;
							}
							// code(s)
							final HashSet<Code> codes = new HashSet<>();
							// siae
							codes.add(new Code(code, CodeType.siae, origin));
							// iswc(s)
							jsonElement = jsonObject.get(iswcField);
							if (null != jsonElement) {
								if (jsonElement.isJsonArray()) {
									for (JsonElement jsonText : jsonElement.getAsJsonArray()) {
										if(jsonText != null){
											String text = iswcNormalizer.get().normalize(jsonText.getAsString());
											if (!Strings.isNullOrEmpty(text)) {
												codes.add(new Code(text, CodeType.iswc, origin));
											}
										}
									}
								}
							}
							// isrc(s)
							jsonElement = jsonObject.get(isrcField);
							if (null != jsonElement) {
								if (jsonElement.isJsonArray()) {
									for (JsonElement jsonText : jsonElement.getAsJsonArray()) {
										if(jsonText != null){
											String text = isrcNormalizer.get().normalize(jsonText.getAsString());
											if (!Strings.isNullOrEmpty(text)) {
												codes.add(new Code(text, CodeType.isrc, origin));
											}
										}
									}
								}
							}
							
							// uuid(s)
							jsonElement = jsonObject.get(codeField);
							if (null != jsonElement) {
								String text = UUIDCode.normalize(jsonElement.getAsString());
								if (!Strings.isNullOrEmpty(text)) {
									codes.add(new Code(text, CodeType.uuid, origin));									
								}
							}

							// title(s)
							final HashSet<Title> titles = new HashSet<>();
							// original title
							jsonElement = jsonObject.get(originalTitleFields);
							if (null != jsonElement) {
								if (jsonElement.isJsonArray()) {
									for (JsonElement jsonText : jsonElement.getAsJsonArray()) {
										if(jsonText != null){
											String text = jsonText.getAsString();
											if (!Strings.isNullOrEmpty(text)) {
												titles.add(new Title(text, TitleType.song, Category.original, origin));
//												break;
											}
										}
									}
								}
							}
							// alternative title(s)
							jsonElement = jsonObject.get(alternativeTitleFields);
							if (jsonElement != null && jsonElement.isJsonArray()) {
								for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
									if(jsonArrayElement != null){
										String text = jsonArrayElement.getAsString();
										if (!Strings.isNullOrEmpty(text)) {
											titles.add(new Title(text, TitleType.song, Category.alternative, origin));
//											break;
										}
									}
								}
							}

							if (titles.isEmpty()) {
								logger.warn("missing original title: {}", jsonObject);
								invalidRecords.incrementAndGet();
								continue;
							}

							// artist(s)
							final HashSet<Artist> artists = new HashSet<>();
							// SIAE author(s)
							jsonElement = jsonObject.get(authorSiaeField);
							if (null != jsonElement && jsonElement.isJsonArray()) {
								for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
									JsonElement jsonNameText = jsonArrayElement.getAsJsonObject().get(authorSiaeNameField);
									JsonElement jsonIpiNumberText = jsonArrayElement.getAsJsonObject().get(authorSiaeIpiNumberField);
									if(jsonNameText != null){
										String text = jsonNameText.getAsString();
										if (!Strings.isNullOrEmpty(text)) {
											String ipiNumber = null;
											if(jsonIpiNumberText != null && !(jsonIpiNumberText instanceof JsonNull)) {
												ipiNumber = jsonIpiNumberText.getAsString();
											}
											artists.add(new Artist(text, ArtistRole.author, ipiNumber, origin));
										}
									}
								}
							}
							// SIAE composer(s)
							jsonElement = jsonObject.get(composerSiaeField);
							if (null != jsonElement && jsonElement.isJsonArray()) {
								for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
									JsonElement jsonNameText = jsonArrayElement.getAsJsonObject().get(composerSiaeNameField);
									JsonElement jsonIpiNumberText = jsonArrayElement.getAsJsonObject().get(composerSiaeIpiNumberField);
									if(jsonNameText != null){
										String text = jsonNameText.getAsString();
										if (!Strings.isNullOrEmpty(text)) {
											String ipiNumber = null;
											if(jsonIpiNumberText != null && !(jsonIpiNumberText instanceof JsonNull)) {
												ipiNumber = jsonIpiNumberText.getAsString();
											}
											artists.add(new Artist(text, ArtistRole.composer, ipiNumber , origin));
										}
									}
								}
							}
							// performer(s)
							jsonElement = jsonObject.get(performerField);
							if (null != jsonElement) {
								for (JsonElement jsonNameText : jsonElement.getAsJsonArray()) {
									String text = jsonNameText.getAsString();
									if (!Strings.isNullOrEmpty(text)) {
										artists.add(new Artist(text, ArtistRole.performer, origin));
									}
								}
							}
							if (artists.isEmpty()) {
								//logger.warn("missing artists: {}", jsonObject);
								invalidRecords.incrementAndGet();
								continue;
							}

							// attribute(s)
							final HashSet<Attribute> attributes = new HashSet<>();
							// flag EL
							jsonElement = jsonObject.get(elField);
							if (null != jsonElement) {
								String text = jsonElement.getAsString();
								if (!Strings.isNullOrEmpty(text)) {
									attributes.add(new Attribute(AttributeKey.flag_el, text, origin));
								}
							}
							// flag PD
							jsonElement = jsonObject.get(pdField);
							if (null != jsonElement) {
								String text = jsonElement.getAsString();
								if (!Strings.isNullOrEmpty(text)) {
									attributes.add(new Attribute(AttributeKey.flag_pd, text, origin));
								}
							}
							// flag Irregolare
							jsonElement = jsonObject.get(irregolareField);
							if (null != jsonElement) {
								String text = jsonElement.getAsString();
								if (!Strings.isNullOrEmpty(text)) {
									attributes.add(new Attribute(AttributeKey.irregularity, text, origin));
								}
							}
							// ambito
							jsonElement = jsonObject.get(scopeField);
							if (null != jsonElement) {
								String text = jsonElement.getAsString();
								if (!Strings.isNullOrEmpty(text)) {
									attributes.add(new Attribute(AttributeKey.ambito, text, origin));
								}
							}
							// rilevanza
							BigDecimal weight = BigDecimal.ZERO;
							jsonElement = jsonObject.get(weightField);
							if (null != jsonElement) {
								String text = jsonElement.getAsString();
								if (!Strings.isNullOrEmpty(text)) {
									attributes.add(new Attribute(AttributeKey.rilevanza, text, origin));
									weight = new BigDecimal(text);
								}
							}
							// accrued
							jsonElement = jsonObject.get(accruedField);
							if (null != jsonElement) {
								final String accrued = jsonElement.getAsString();
								if (!Strings.isNullOrEmpty(accrued)) {
									attributes.add(new Attribute(AttributeKey.accrued, BigDecimals.toPlainString(new BigDecimal(accrued)), origin));
								}
							}
							// risk
							jsonElement = jsonObject.get(riskField);
							if (null != jsonElement) {
								final String risk = jsonElement.getAsString();
								if (!Strings.isNullOrEmpty(risk)) {
									attributes.add(new Attribute(AttributeKey.risk, BigDecimals.toPlainString(new BigDecimal(risk)), origin));
								}
							}
							// status
							jsonElement = jsonObject.get(statusField);
							if (null != jsonElement) {
								String text = jsonElement.getAsString();
								if (!Strings.isNullOrEmpty(text)) {
									attributes.add(new Attribute(AttributeKey.status, text, origin));
								}
							}
							// codici opere
							jsonElement = jsonObject.get("codiciOpera");
							if (null != jsonElement) {
								for (JsonElement jsonNameText : jsonElement.getAsJsonArray()) {
									JsonElement jsonCodiceText = jsonNameText.getAsJsonObject().get("codice");
									JsonElement jsonSocietaText = jsonNameText.getAsJsonObject().get("societa");
									if("UCMR-ADA".equals(jsonSocietaText.getAsString())) {
										attributes.add(new Attribute(AttributeKey.codice_ada, jsonCodiceText.getAsString(), origin));
									}else if("SIAE".equals(jsonSocietaText.getAsString())){										
										attributes.add(new Attribute(AttributeKey.codice_siae, jsonCodiceText.getAsString(), origin));
									}
									
								}
							}
							
//							jsonElement = jsonObject.get(codeField);
//							if (null != jsonElement) {
//								String text = UUIDCode.normalize(jsonElement.getAsString());
//								if (!Strings.isNullOrEmpty(text)) {
//									attributes.add(new Attribute(AttributeKey.codice_uuid, text, origin));
//								}
//							}



							// find existing or generate new work id
							final long id;
							final CollectingCode collecting = collectingDatabase.getByCode(code);
							if (null == collecting) {
								id = workDatabase.getNextId();
								// insert collecting code
								collectingDatabase.upsert(new CollectingCode(code, id, weight));
							} else {
								id = collecting.workId;
								if (!BigDecimals.almostEquals(weight, collecting.weight)) {
									// update collecting code
									collecting.weight = weight;
									collectingDatabase.upsert(collecting);
								}
							}

							// merge data from other origin(s)
							if (!overwriteExisting && null != collecting) {
								final Work work = workDatabase.getById(id);
								if (null != work) {
									// merge existing code(s)
									if (null != work.codes) {
										for (Code workCode : work.codes) {
											if (origin != workCode.origin) {
												codes.add(workCode);
											}
										}
									}
									// merge existing title(s)
									if (null != work.titles) {
										for (Title workTitle : work.titles) {
											if (origin != workTitle.origin) {
												titles.add(workTitle);
											}
										}
									}
									// merge existing artist(s)
									if (null != work.artists) {
										for (Artist workArtist : work.artists) {
											if (origin != workArtist.origin) {
												artists.add(workArtist);
											}
										}
									}
									// merge existing attribute(s)
									if (null != work.attributes) {
										for (Attribute workAttribute : work.attributes) {
											if (origin != workAttribute.origin) {
												attributes.add(workAttribute);
											}
										}
									}
								}
							}

							// insert or replace work
							final Work work = new Work(id, codes, titles, artists, attributes);
							if (null == workDatabase.upsert(work)) {
								insertedWorks.incrementAndGet();
							} else {
								modifiedWorks.incrementAndGet();
							}
							heartbeat.pump();
						}
					}
					// save last processed
					writeResumeFile(resumeFile, s3FileUrl);
					break;
				} catch (IOException e) {
					logger.error("process", e);
				}
			}
		}
		logger.debug("{} records imported", heartbeat.getTotalPumps());
		logger.debug("import completed in {}",TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		// processing stats
		final JsonObject processingStats = new JsonObject();
		processingStats.addProperty("totalRecords", totalRecords.get());
		processingStats.addProperty("invalidRecords", invalidRecords.get());
		processingStats.addProperty("insertedWorks", insertedWorks.get());
		processingStats.addProperty("modifiedWorks", modifiedWorks.get());
		processingStats.addProperty("modifiedWorks", modifiedWorks.get());
		processingStats.addProperty("jsonParsingDuration",TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		// defrag work database
		startTimeMillis = System.currentTimeMillis();
		logger.debug("work database defrag started");
		workDatabase.defrag();
		logger.debug("work database defrag completed in {}",TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
		processingStats.addProperty("workDatabaseDefragDuration",TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		// defrag collecting code database
		startTimeMillis = System.currentTimeMillis();
		logger.debug("collecting database defrag started");
		collectingDatabase.defrag();
		logger.debug("collecting database defrag completed in {}",TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
		processingStats.addProperty("collectingDatabaseDefragDuration",TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		// output
		output.add("multimedialeJsonParser", processingStats);
	}
	
	
	
	public void callHttp() {
		HttpClient httpClient = HttpClientBuilder.create().build();
		try {
			for(int offset=0;; offset++) {
				HttpGet httpGetRequest = new HttpGet("http://localhost:8090/api/findAllOpera?offset="+offset);
				HttpResponse httpResponse = httpClient.execute(httpGetRequest);
				HttpEntity entity = httpResponse.getEntity();
				if (entity != null) {
	
					final JsonArray jsonArray = GsonUtils.fromJson(EntityUtils.toString(entity), JsonArray.class);
					if(jsonArray.isJsonNull() || jsonArray.getAsJsonArray().size() == 0) {
						break;
					}
					for(JsonElement jsonElement:jsonArray) {
						System.out.println(jsonElement.getAsJsonObject().get("uuid").getAsString());
						System.out.println(jsonElement.getAsJsonObject().get("iswc").getAsString());
						System.out.println(jsonElement.getAsJsonObject().get("isrc").getAsString());
						System.out.println(jsonElement.getAsJsonObject().get("rilevanza").getAsString());
						System.out.println(jsonElement.getAsJsonObject().get("maturato").getAsString());
						System.out.println(jsonElement.getAsJsonObject().get("rischio").getAsString());
						System.out.println(jsonElement.getAsJsonObject().get("statoOpera").getAsString());
						System.out.println(jsonElement.getAsJsonObject().get("ambito").getAsString());
						System.out.println(jsonElement.getAsJsonObject().get("flagIrregolare").getAsString());
						System.out.println(jsonElement.getAsJsonObject().get("flagEL").getAsString());
						System.out.println(jsonElement.getAsJsonObject().get("titoliOt").getAsString());
						System.out.println(jsonElement.getAsJsonObject().get("titoliAt").getAsString());
					}
					
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// When HttpClient instance is no longer needed,
			// shut down the connection manager to ensure
			// immediate deallocation of all system resources
			// httpclient.getConnectionManager().shutdown();
		}
	}
	private Map<String, String> readResume(File file) throws IOException, ClassNotFoundException {
		final Map<String, String> processedUrls = new ConcurrentHashMap<String, String>();
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line = reader.readLine();
		while (line != null) {
			StringTokenizer et = new StringTokenizer(line,"\t");
			if(et.countTokens()>1){
				String key = et.nextToken();
			    String value = et.nextToken();
			    processedUrls.put(key, value);
			}
			line = reader.readLine();
		}
		reader.close();
		return processedUrls;
	}
	private List<String> getUnprocessedMultimedialeFileUrls(Map<String, String> processedUrls,String dump) throws IOException {

		final String s3FolderUrl = configuration.getProperty("index_builder.multimediale.input.s3_folder_url") + (dump== null?"":dump);
		final Pattern filterRegex = Pattern.compile(configuration.getProperty("index_builder.multimediale.input.filter_regex"));

		logger.debug("getUnprocessedMultimedialeFileUrls: s3FolderUrl {}", s3FolderUrl);
		logger.debug("getUnprocessedMultimedialeFileUrls: filterRegex {}", filterRegex);

		// build s3 file(s) list
		final List<S3ObjectSummary> objects = s3.listObjects(new S3.Url(s3FolderUrl));
		final List<String> s3FileUrls = new ArrayList<>(objects.size());
		for (S3ObjectSummary object : objects) {
			final String s3FileUrl = new StringBuilder()
					.append("s3://")
					.append(object.getBucketName())
					.append('/')
					.append(object.getKey())
					.toString();
			if (filterRegex.matcher(s3FileUrl).matches()){
				s3FileUrls.add(s3FileUrl);
			}
		}

		// sort lexicographically
		Collections.sort(s3FileUrls);

		// remove already processed url(s) and deduplicate unprocessed url(s)
		final Set<String> deduplicated = new HashSet<>();
		for (Iterator<String> iterator = s3FileUrls.iterator(); iterator.hasNext(); ) {
			final String s3FileUrl = iterator.next();
			final String s3FileName = s3FileUrl
					.substring(1 + s3FileUrl.lastIndexOf('/'));
			if (processedUrls.containsKey(s3FileName)) {
				iterator.remove();
			} else if (deduplicated.contains(s3FileName)) {
				iterator.remove();
			}
			deduplicated.add(s3FileName);
		}
		return s3FileUrls;
	}
	private void writeResumeFile(File file, String text) throws IOException {
		try (final FileOutputStream out = new FileOutputStream(file)) {
			out.write(text.getBytes("UTF-8"));
		}
	}
	private void deleteS3FolderContents(String s3FolderUrl) {
		if (!s3FolderUrl.endsWith("/"))
			s3FolderUrl += '/';
		final List<S3ObjectSummary> objects = s3
				.listObjects(new S3.Url(s3FolderUrl));
		for (S3ObjectSummary object : objects) {
			final String s3FileUrl = new StringBuilder()
					.append("s3://")
					.append(object.getBucketName())
					.append('/')
					.append(object.getKey())
					.toString();
			if (s3.delete(new S3.Url(s3FileUrl)))
				logger.debug("file deleted {}", s3FileUrl);
			else
				logger.error("error deleting file {}", s3FileUrl);
		}
	}

}