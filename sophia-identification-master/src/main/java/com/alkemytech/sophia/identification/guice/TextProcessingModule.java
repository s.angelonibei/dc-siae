package com.alkemytech.sophia.identification.guice;

import java.util.Properties;

import com.alkemytech.sophia.commons.text.RegExTextNormalizer;
import com.alkemytech.sophia.commons.text.RegExTextTokenizer;
import com.alkemytech.sophia.commons.text.TextNormalizer;
import com.alkemytech.sophia.commons.text.TextTokenizer;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class TextProcessingModule extends AbstractModule {

	@Override
	protected void configure() {
	
	}
	
	@Provides
	@Named("iswc_normalizer")
	protected TextNormalizer provideIswcNormalizer(@Named("configuration") Properties configuration) {
		return new RegExTextNormalizer(configuration, "iswc_normalizer");
	}

	@Provides
	@Named("isrc_normalizer")
	protected TextNormalizer provideIsrcNormalizer(@Named("configuration") Properties configuration) {
		return new RegExTextNormalizer(configuration, "isrc_normalizer");
	}
	@Provides
	@Named("uuid_normalizer")
	protected TextNormalizer provideUUIDNormalizer(@Named("configuration") Properties configuration) {
		return new RegExTextNormalizer(configuration, "uuid_normalizer");
	}

	@Provides
	@Named("title_normalizer")
	protected TextNormalizer provideTitleNormalizer(@Named("configuration") Properties configuration) {
		return new RegExTextNormalizer(configuration, "title_normalizer");
	}

	@Provides
	@Named("artist_normalizer")
	protected TextNormalizer provideArtistNormalizer(@Named("configuration") Properties configuration) {
		return new RegExTextNormalizer(configuration, "artist_normalizer");
	}

	@Provides
	@Named("title_tokenizer")
	protected TextTokenizer provideTitleTokenizer(@Named("configuration") Properties configuration) {
		return new RegExTextTokenizer(configuration, "title_tokenizer");
	}

	@Provides
	@Named("artist_tokenizer")
	protected TextTokenizer provideArtistTokenizer(@Named("configuration") Properties configuration) {
		return new RegExTextTokenizer(configuration, "artist_tokenizer");
	}

}
