package com.alkemytech.sophia.identification.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.collecting.AdaCode;
import com.alkemytech.sophia.commons.collecting.SiaeCode;
import com.alkemytech.sophia.commons.guice.ConfigurationLoader;
import com.alkemytech.sophia.commons.index.ReverseIndex;
import com.alkemytech.sophia.commons.index.LevenshteinIndex.TermSet;
import com.alkemytech.sophia.commons.query.QueryChain;
import com.alkemytech.sophia.commons.query.TitleArtistQuery;
import com.alkemytech.sophia.commons.text.TextNormalizer;
import com.alkemytech.sophia.commons.text.TextTokenizer;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.LongArray;
import com.alkemytech.sophia.commons.util.SplitCharSequence;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.identification.MatchType;
import com.alkemytech.sophia.identification.collecting.CollectingDatabase;
import com.alkemytech.sophia.identification.document.DocumentStore;
import com.alkemytech.sophia.identification.document.ScoredDocument;
import com.alkemytech.sophia.identification.jdbc.KbdbDAO;
import com.alkemytech.sophia.identification.model.Attribute;
import com.alkemytech.sophia.identification.model.AttributeKey;
import com.alkemytech.sophia.identification.model.Document;
import com.alkemytech.sophia.identification.model.Text;
import com.alkemytech.sophia.identification.model.TextArtist;
import com.alkemytech.sophia.identification.model.TextType;
import com.alkemytech.sophia.identification.model.Work;
import com.alkemytech.sophia.identification.result.ResultFormat;
import com.alkemytech.sophia.identification.result.ResultFormats;
import com.alkemytech.sophia.identification.score.DocumentScore;
import com.alkemytech.sophia.identification.work.WorkDatabase;
import com.google.common.base.Strings;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@SuppressWarnings("serial")
public class SearchServlet extends HttpServlet {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Properties configuration;
	private final S3 s3;
//	private final Cache<String, Properties> configCache;
	private final Provider<WorkDatabase> workDatabaseProvider;
	private final Provider<CollectingDatabase> collectingDatabaseProvider;
	private final Provider<DocumentStore> documentStoreProvider;
	private final Provider<ReverseIndex> titleIndexProvider;
	private final Provider<ReverseIndex> artistIndexProvider;
	private final Provider<ReverseIndex> iswcIndexProvider;
	private final Provider<ReverseIndex> isrcIndexProvider;
	private final Provider<ReverseIndex> uuidIndexProvider;
	private final Provider<ReverseIndex> siaeIndexProvider;
	private final Provider<ReverseIndex> adaIndexProvider;
	private final Provider<TextNormalizer> titleNormalizerProvider;
	private final Provider<TextNormalizer> artistNormalizerProvider;
	private final Provider<TextTokenizer> titleTokenizerProvider;
	private final Provider<TextTokenizer> artistTokenizerProvider;
	private final Provider<TextNormalizer> iswcNormalizerProvider;
	private final Provider<TextNormalizer> isrcNormalizerProvider;
	private final Provider<TextNormalizer> uuidNormalizerProvider;
	private final Provider<TitleArtistQuery> titleArtistQueryProvider;
	private final Map<String, Provider<DocumentScore>> documentScoreProvider;
	private QueryChain titleQueryChain;
	private QueryChain artistQueryChain;
	private final KbdbDAO dao;
	@Inject
	protected SearchServlet(@Named("configuration") Properties configuration,
			KbdbDAO dao,
			S3 s3,
			Provider<WorkDatabase> workDatabaseProvider,
			Provider<CollectingDatabase> collectingDatabaseProvider,
			Provider<DocumentStore> documentStoreProvider,
			@Named("title_index") Provider<ReverseIndex> titleIndexProvider,
			@Named("artist_index") Provider<ReverseIndex> artistIndexProvider,
			@Named("iswc_index") Provider<ReverseIndex> iswcIndexProvider,
			@Named("isrc_index") Provider<ReverseIndex> isrcIndexProvider,
			@Named("uuid_index") Provider<ReverseIndex> uuidIndexProvider,
			@Named("title_normalizer") Provider<TextNormalizer> titleNormalizerProvider,
			@Named("artist_normalizer") Provider<TextNormalizer> artistNormalizerProvider,
			@Named("iswc_normalizer") Provider<TextNormalizer> iswcNormalizerProvider,
			@Named("isrc_normalizer") Provider<TextNormalizer> isrcNormalizerProvider,
			@Named("uuid_normalizer") Provider<TextNormalizer> uuidNormalizerProvider,
			@Named("siae_index") Provider<ReverseIndex> siaeIndexProvider,
			@Named("ada_index") Provider<ReverseIndex> adaIndexProvider,
			@Named("title_tokenizer") Provider<TextTokenizer> titleTokenizerProvider,
			@Named("artist_tokenizer") Provider<TextTokenizer> artistTokenizerProvider,
			Provider<TitleArtistQuery> titleArtistQueryProvider,
			@Named("document_score_providers") Map<String, Provider<DocumentScore>> documentScoreProvider
			) {
		super();
		this.dao = dao;
		this.configuration = configuration;
		this.s3 = s3;
		this.workDatabaseProvider = workDatabaseProvider;
		this.collectingDatabaseProvider = collectingDatabaseProvider;
		this.documentStoreProvider = documentStoreProvider;
		this.titleIndexProvider = titleIndexProvider;
		this.artistIndexProvider = artistIndexProvider;
		this.titleNormalizerProvider = titleNormalizerProvider;
		this.artistNormalizerProvider = artistNormalizerProvider;
		this.titleTokenizerProvider = titleTokenizerProvider;
		this.artistTokenizerProvider = artistTokenizerProvider;
		this.titleArtistQueryProvider = titleArtistQueryProvider;
		this.documentScoreProvider = documentScoreProvider;
		this.iswcNormalizerProvider = iswcNormalizerProvider;
		this.isrcNormalizerProvider = isrcNormalizerProvider;
		this.iswcIndexProvider = iswcIndexProvider;
		this.isrcIndexProvider = isrcIndexProvider;
		this.uuidIndexProvider = uuidIndexProvider;
		this.uuidNormalizerProvider = uuidNormalizerProvider;
		this.siaeIndexProvider = siaeIndexProvider;
		this.adaIndexProvider = adaIndexProvider;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final TextNormalizer titleNormalizer 	= 	this.titleNormalizerProvider.get();
		final TextNormalizer artistNormalizer 	= 	this.artistNormalizerProvider.get();
		final TextTokenizer titleTokenizer 		= 	this.titleTokenizerProvider.get();
		final TextTokenizer artistTokenizer 	= 	this.artistTokenizerProvider.get();
		final TextNormalizer iswcNormalizer 	=   this.iswcNormalizerProvider.get();
		final TextNormalizer isrcNormalizer 	=   this.isrcNormalizerProvider.get();
		final TextNormalizer uuidNormalizer 	=   this.uuidNormalizerProvider.get();
		
		// retrieve request attribute(s)
		final JsonObject requestJson = (JsonObject)request.getAttribute(GsonFilter.REQUEST_JSON);

		// parse request
		final String version = GsonUtils.getAsString(requestJson, "version");
		if (!"1".equals(version))
			throw new ServletException(String.format("unsupported version: %s", version));
		final JsonObject config = GsonUtils.getAsJsonObject(requestJson, "config");
		final String configUrl = GsonUtils.getAsString(config, "url");
		final JsonObject configProperties = GsonUtils.getAsJsonObject(config, "properties");
		final JsonObject query = GsonUtils.getAsJsonObject(requestJson, "query");
		final JsonObject queryTexts = GsonUtils.getAsJsonObject(query, "texts");
		final String queryWhere = GsonUtils.getAsString(query, "where");

		// config properties
		logger.debug("configUrl {}", configUrl);
		//search();
		// override properties
		if (null != configProperties) {
			final String document_score = configuration.getProperty("document_score", "custom_score");
			for (Map.Entry<String, JsonElement> entry : configProperties.entrySet()) {
				configuration.setProperty(document_score + "." + entry.getKey(),
						entry.getValue().getAsString());
			}
		}
		ConfigurationLoader.expandVariables(configuration);
		logger.debug("configuration {}", configuration);
		
		// parse query text(s)
		SplitCharSequence normalizedTitle =null; 
		final Set<SplitCharSequence> normalizedArtists = new HashSet<>();
		final ArrayList<Text> recordTexts = new ArrayList<>();
		MatchType matchType = MatchType.title_artist;
		for (Map.Entry<String, JsonElement> entry : queryTexts.entrySet()) {
			final String textType = entry.getKey();
			for (JsonElement text : GsonUtils.getAsJsonArray(entry.getValue())) {
				//devo normalizzare il test;
				String textMessage = null;
				switch(TextType.valueOf(textType)) {
					case TextType.title:
					case TextType.title_original:
						textMessage = titleNormalizer.normalize(text.getAsString());
						normalizedTitle = new SplitCharSequence(textMessage);
						titleTokenizer.clear().tokenize(normalizedTitle.toString());
						break;
					case TextType.title_alternative:
						textMessage = titleNormalizer.normalize(text.getAsString());
						normalizedTitle = new SplitCharSequence(textMessage);
						if(titleTokenizer.getTokens().size() == 0 )
							titleTokenizer.tokenize(normalizedTitle.toString());
						break;
					case TextType.artist:
					case TextType.artist_author:
					case TextType.artist_composer:
					case TextType.artist_performer:
						textMessage = artistNormalizer.normalize(text.getAsString());
						final SplitCharSequence normalizedArtist =new SplitCharSequence(textMessage);
						normalizedArtists.add(normalizedArtist);
						artistTokenizer.tokenize(normalizedArtist.toString());
						break;
					case TextType.iswc:
						textMessage = iswcNormalizer.normalize(text.getAsString());
						matchType = MatchType.iswc;
						break;
					case TextType.isrc:
						textMessage = isrcNormalizer.normalize(text.getAsString());
						matchType = MatchType.isrc;
						break;
					case TextType.uuid:
						textMessage = uuidNormalizer.normalize(text.getAsString());
						matchType = MatchType.uuid;
						break;
					case TextType.codiceSiae:
						textMessage = SiaeCode.normalize(text.getAsString());
						matchType = MatchType.codiceSiae;
						break;
					case TextType.codiceAda:
						textMessage = AdaCode.normalize(text.getAsString());
						matchType = MatchType.codiceAda;
						break;
					case TextType.codice:
						textMessage =text.getAsString();
						matchType = MatchType.codice;
						break;
				}
				if (!Strings.isNullOrEmpty(textMessage)) {
					recordTexts.add(new Text(textMessage,TextType.valueOf(textType)));
				}
				
				
			}
		}		
		final List<ScoredDocument> scoredDocuments = this.search(recordTexts,
																	titleTokenizer.getTokens(), 
																	artistTokenizer.getTokens(),
																	normalizedTitle,
																	normalizedArtists,
																	matchType);
		
		response.setContentType("application/json");
		final List<ResultFormat> resultFormats = new ArrayList<>();
		for (String prefix : configuration.getProperty("identifier.output.result_formats", "").split(","))
			if (!Strings.isNullOrEmpty(prefix))
				resultFormats.add(ResultFormats.getInstance(configuration, prefix));
		try {
			final String formattedResult = resultFormats.get(0).format(recordTexts,scoredDocuments, matchType);
			response.getWriter().write(formattedResult);
		}catch(Exception ex) {
			ex.printStackTrace();
		}

	}

	private List<ScoredDocument> getScoredDocuments(List<ScoredDocument> results, WorkDatabase workDatabase, DocumentStore documentStore, DocumentScore documentScore, SplitCharSequence normalizedTitle, Collection<SplitCharSequence> normalizedArtists, LongArray postings, int maxPostings, int maxResults, double minScore, double autoScore) throws IOException {
		return maxResults > 1 ?
			findTopDocuments(results, workDatabase, documentStore, documentScore, normalizedTitle, normalizedArtists, postings, maxPostings, maxResults, minScore) :
			findBestDocument(results, workDatabase, documentStore, documentScore, normalizedTitle, normalizedArtists, postings, maxPostings, minScore  , autoScore);
	}

	private List<ScoredDocument> findBestDocument(List<ScoredDocument> results, WorkDatabase workDatabase, DocumentStore documentStore, DocumentScore documentScore, SplitCharSequence normalizedTitle, Collection<SplitCharSequence> normalizedArtists, LongArray postings, int maxPostings, double minScore, double autoScore) throws IOException {
		final int limit = Math.min(maxPostings, postings.size());
		double bestScore = 0.0;
		ScoredDocument scoreDocument= null;
		for (int i = 0; i < limit && bestScore < autoScore; i ++) {
			final Document document = documentStore.getById(postings.get(i));
			final Work work = workDatabase.getById(document.workId);
			ScoredDocument score = documentScore.getScore(normalizedTitle, normalizedArtists, document, work);
			if (bestScore < score.score) {
				scoreDocument = score;
			}
		}
		if (bestScore >= minScore) {
			results.add(scoreDocument);
		}
		return results;
	}
	
	private List<ScoredDocument> findTopDocuments(List<ScoredDocument> results, WorkDatabase workDatabase, DocumentStore documentStore, DocumentScore documentScore, SplitCharSequence normalizedTitle, Collection<SplitCharSequence> normalizedArtists, LongArray postings, int maxPostings, int maxResults, double minScore) throws IOException {
		//calcolo per ogni elemento lo score
		List<ScoredDocument> _results = new ArrayList<>();
		for(int index=0; index < postings.size(); index++){
			final Document document = documentStore.getById(postings.get(index));
			final Work work = workDatabase.getById(document.workId);
			ScoredDocument score = documentScore.getScore(normalizedTitle, normalizedArtists, document, work);
			if (score.score >= minScore) {				
				//lo score minimo è garantito faccio ordinamento
				_results.add(score);
			}
		}
		if(_results.size()>0){
			//ordino per score
			Collections.sort(_results, new Comparator<ScoredDocument>() {
		        @Override
		        public int compare(ScoredDocument score2, ScoredDocument score1)
		        {
		            return score2.score > score1.score ? -1 : (score2.score < score1.score) ? 1 : this.doppioDeposito(score1,score2);
		        }
				private int doppioDeposito(ScoredDocument score1, ScoredDocument score2) {
					if (score1.title.equals(score2.title)) {
						if(!Collections.disjoint(score1.ipi, score2.ipi)) {
	                        score1.setFlagDoppioDeposito(1);
	                        score2.setFlagDoppioDeposito(1);
						}
                    }
					return 0;
				}
		    });
			final int limit = Math.min(maxResults, _results.size()); //numero massimo di elementi trovati
			results.addAll(_results.subList(0, limit));
		}
		return results;
	}
	
	
	
	
	private List<ScoredDocument> search(ArrayList<Text> recordTexts,
										final Collection<String> titleTerms,
										final Collection<String> artistTerms,
										SplitCharSequence normalizedTitle,
										Set<SplitCharSequence> normalizedArtists,
										MatchType matchType) throws IOException{
//		titles.add("VULCAN");
//		artists.add("MASSIMO AMORE");
		final double autoScore = Double.parseDouble(configuration.getProperty("identifier.auto_score", "1.0"));
		final double titleArtistMinScore = Double.parseDouble(configuration.getProperty("identifier.min_score.title_artist"));
		final double iswcMinScore = Double.parseDouble(configuration.getProperty("identifier.min_score.iswc"));
		final int maxPostings = TextUtils.parseIntSize(configuration.getProperty("identifier.max_postings"));
		final int maxResults = TextUtils.parseIntSize(configuration.getProperty("identifier.max_results"));
		final ReverseIndex titleIndex 			= 	this.titleIndexProvider.get();
		final ReverseIndex artistIndex 			= 	this.artistIndexProvider.get();
		final TitleArtistQuery titleArtistQuery = 	this.titleArtistQueryProvider.get();
		final ReverseIndex iswcIndex = 				this.iswcIndexProvider.get();
		final ReverseIndex isrcIndex = 				this.isrcIndexProvider.get();		
		final ReverseIndex uuidIndex = 				this.uuidIndexProvider.get();
		final ReverseIndex siaeIndex = 				this.siaeIndexProvider.get();
		final ReverseIndex adaIndex  = 				this.adaIndexProvider.get();
		
		
		DocumentScore documentScore =documentScoreProvider.get(configuration.getProperty("document_score")).get();
		final WorkDatabase workDatabase = workDatabaseProvider.get();
		final CollectingDatabase collectingDatabase = collectingDatabaseProvider.get();
		final DocumentStore documentStore = documentStoreProvider.get();
		workDatabase.startup();
		collectingDatabase.startup();
		documentStore.startup();
		final List<ScoredDocument> scoredDocuments = new ArrayList<>();
		LongArray postings = null;
		
		if(matchType == MatchType.uuid){
			for (Text record : recordTexts) {
				if(record.type == TextType.uuid) {
					final TermSet termSet = uuidIndex.search(record.text);
					postings = termSet.next() ? termSet.getPostings() : null;
				}
			}
	
			if(postings!= null && postings.size()>0) {
				final Document document = documentStore.getById(postings.get(0));
				String title = null;
				String artist = null;
				List<String> ipi = new ArrayList<String>();
				for (Text text : document.texts) {
					if (TextType.title == text.type && title == null) {
						title = text.text;
					} else if (TextType.artist == text.type && artist == null) {
						artist = text.text;
						ipi.add(((TextArtist)text).ipi);
					}
				}
				scoredDocuments.add(new ScoredDocument(document, 1.0,title,artist,ipi));
			}
		} else if(matchType == MatchType.iswc) {
			for (Text record : recordTexts) {
				if(record.type == TextType.iswc) {
					final TermSet termSet = iswcIndex.search(record.text);
					postings = termSet.next() ? termSet.getPostings() : null;
				}
			}
			if(postings!= null && postings.size()>0) {
				final Document document = documentStore.getById(postings.get(0));
				String title = null;
				String artist = null;
				List<String> ipi = new ArrayList<String>();
				for (Text text : document.texts) {
					if (TextType.title == text.type && title == null) {
						title = text.text;
					} else if (TextType.artist == text.type && artist == null) {
						artist = text.text;
						ipi.add(((TextArtist)text).ipi);
					}
				}
				scoredDocuments.add(new ScoredDocument(document, 1.0,title,artist,ipi));
			}
		} else if(matchType == MatchType.isrc) {
			for (Text record : recordTexts) {
				if(record.type == TextType.isrc) {
					final TermSet termSet = isrcIndex.search(record.text);
					postings = termSet.next() ? termSet.getPostings() : null;
				}
			}
			if(postings!= null && postings.size()>0) {
				final Document document = documentStore.getById(postings.get(0));
				String title = null;
				String artist = null;
				List<String> ipi = new ArrayList<String>();
				for (Text text : document.texts) {
					if (TextType.title == text.type && title == null) {
						title = text.text;
					} else if (TextType.artist == text.type && artist == null) {
						artist = text.text;
						ipi.add(((TextArtist)text).ipi);
					}
				}
				scoredDocuments.add(new ScoredDocument(document, 1.0,title,artist,ipi));
			}
		} else if(matchType == MatchType.codiceSiae) {
			for (Text record : recordTexts) {
				if(record.type == TextType.codiceSiae) {
					final TermSet termSet = siaeIndex.search(record.text);
					postings = termSet.next() ? termSet.getPostings() : null;
				}
			}
			if(postings!= null && postings.size()>0) {
				final Document document = documentStore.getById(postings.get(0));
				String title = null;
				String artist = null;
				List<String> ipi = new ArrayList<String>();
				for (Text text : document.texts) {
					if (TextType.title == text.type && title == null) {
						title = text.text;
					} else if (TextType.artist == text.type && artist == null) {
						artist = text.text;
						ipi.add(((TextArtist)text).ipi);
					}
				}
				scoredDocuments.add(new ScoredDocument(document, 1.0,title,artist,ipi));
			}
		} else if(matchType == MatchType.codiceAda) {
			for (Text record : recordTexts) {
				if(record.type == TextType.codiceAda) {
					final TermSet termSet = adaIndex.search(record.text);
					postings = termSet.next() ? termSet.getPostings() : null;
				}
			}
			if(postings!= null && postings.size()>0) {
				final Document document = documentStore.getById(postings.get(0));
				String title = null;
				String artist = null;
				List<String> ipi = new ArrayList<String>();
				for (Text text : document.texts) {
					if (TextType.title == text.type && title == null) {
						title = text.text;
					} else if (TextType.artist == text.type && artist == null) {
						artist = text.text;
						ipi.add(((TextArtist)text).ipi);
					}
				}
				scoredDocuments.add(new ScoredDocument(document, 1.0,title,artist,ipi));
			}
		} else if(matchType == MatchType.codice) {
			for (Text record : recordTexts) {
				if(record.type == TextType.codice) {
					postings = new LongArray();
					try {
						final TermSet termSet = adaIndex.search(AdaCode.normalize(record.text));
						if(termSet.next()) {
							postings.addAll(termSet.getPostings());
						}
					}catch(Throwable t) {
					}
					try {
						final TermSet termSet = siaeIndex.search(SiaeCode.normalize(record.text));
						if(termSet.next()) {
							postings.addAll(termSet.getPostings());
						}
					}catch(Throwable t) {
					}
				}
			}
			if(postings!= null && postings.size()>0) {
				for( int x = 0; x< postings.size(); x++) {
					final Document document = documentStore.getById(postings.get(x));
					String title = null;
					String artist = null;
					List<String> ipi = new ArrayList<String>();
					for (Text text : document.texts) {
						if (TextType.title == text.type && title == null) {
							title = text.text;
						} else if (TextType.artist == text.type && artist == null) {
							artist = text.text;
							ipi.add(((TextArtist)text).ipi);
						}
					}
					scoredDocuments.add(new ScoredDocument(document, 1.0,title,artist,ipi));
				}
			}
		} else {
			postings = titleArtistQuery.search(titleIndex, artistIndex, titleTerms, artistTerms);
			if(postings!= null && postings.size()>0) {
				this.getScoredDocuments(scoredDocuments, workDatabase, documentStore, documentScore, normalizedTitle, normalizedArtists, postings, maxPostings, maxResults, titleArtistMinScore, autoScore);
			}
		}

		
		
		
		
		
		
		
		
		
		
		
		
		//"T9051168695"
//		double minScore = titleArtistMinScore;
//		if(postings == null) {
//			postings = titleArtistQuery.search(titleIndex, artistIndex, titleTerms, artistTerms);
//			this.getScoredDocuments(scoredDocuments, workDatabase, documentStore, documentScore, normalizedTitle, normalizedArtists, postings, maxPostings, maxResults, minScore, autoScore);
//		}else {
//			minScore = iswcMinScore;
//			final Document document = documentStore.getById(postings.get(0));
//			String title = null;
//			String artist = null;
//			for (Text text : document.texts) {
//				if (TextType.title == text.type && title == null) {
//					title = text.text;
//				} else if (TextType.artist == text.type && artist == null) {
//					artist = text.text;
//				}
//			}
//			scoredDocuments.add(new ScoredDocument(document, 1.0,title,artist));
//		}
		//devo filtrare i posting per la tipologia di richiesta		
		return scoredDocuments;
	}
}
