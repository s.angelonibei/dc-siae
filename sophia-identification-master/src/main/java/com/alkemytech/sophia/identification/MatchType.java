package com.alkemytech.sophia.identification;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public enum MatchType {

	unidentified, work_code, iswc, isrc, title_artist, uuid, codiceSiae, codiceAda, codice;

}
