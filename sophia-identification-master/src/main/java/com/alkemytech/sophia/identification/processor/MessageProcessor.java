package com.alkemytech.sophia.identification.processor;

import com.google.gson.JsonObject;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface MessageProcessor {

	/**
	 * Process a standard json message.
	 * 
	 * @param message the json message to process
	 * @return <code>true</code> if message was successfully processed, <code>false</code> otherwise.
	 */
	public boolean process(JsonObject message);
	
	/**
	 * Returns a result json object if message was successfully processed, <code>null</code> otherwise.
	 * 
	 * @return a result json object if message was successfully processed, <code>null</code> otherwise.
	 */
	public JsonObject getResultJson();

	/**
	 * Returns an error json object if message processing failed, <code>null</code> otherwise.
	 * 
	 * @return an error json object if message processing failed, <code>null</code> otherwise.
	 */
	public JsonObject getErrorJson();

}
