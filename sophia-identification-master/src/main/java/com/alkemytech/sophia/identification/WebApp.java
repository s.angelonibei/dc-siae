package com.alkemytech.sophia.identification;

import java.io.File;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.util.EnumSet;
import java.util.List;
import java.util.Properties;

import javax.servlet.DispatcherType;
import javax.sql.DataSource;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.index.ReverseIndex;
import com.alkemytech.sophia.commons.query.ExactTitleArtistQuery;
import com.alkemytech.sophia.commons.query.FuzzyTitleArtistQuery;
import com.alkemytech.sophia.commons.query.TitleArtistQuery;
import com.alkemytech.sophia.commons.text.RegExTextNormalizer;
import com.alkemytech.sophia.commons.text.RegExTextTokenizer;
import com.alkemytech.sophia.commons.text.TextNormalizer;
import com.alkemytech.sophia.commons.text.TextTokenizer;
import com.alkemytech.sophia.commons.util.FileUtils;
import com.alkemytech.sophia.identification.collecting.CollectingDatabase;
import com.alkemytech.sophia.identification.document.DocumentStore;
import com.alkemytech.sophia.identification.guice.ApplicationServletModule;
import com.alkemytech.sophia.identification.guice.DocumentScoreModule;
import com.alkemytech.sophia.identification.guice.IndexModule;
import com.alkemytech.sophia.identification.guice.JettyGuiceBootstrap;
import com.alkemytech.sophia.identification.guice.TextProcessingModule;
import com.alkemytech.sophia.identification.index.ReverseIndexMetadata;
import com.alkemytech.sophia.identification.jdbc.KbdbDAO;
import com.alkemytech.sophia.identification.jdbc.KbdbDataSource;
import com.alkemytech.sophia.identification.jdbc.McmdbDAO;
import com.alkemytech.sophia.identification.jdbc.McmdbDataSource;
import com.alkemytech.sophia.identification.result.ConfigurableResultFormat;
import com.alkemytech.sophia.identification.result.ResultFormats;
import com.alkemytech.sophia.identification.score.CustomDocumentScore;
import com.alkemytech.sophia.identification.score.DocumentScore;
import com.alkemytech.sophia.identification.score.GeometricDocumentScore;
import com.alkemytech.sophia.identification.score.LinearDocumentScore;
import com.alkemytech.sophia.identification.work.WorkDatabase;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.common.base.Strings;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import com.google.inject.servlet.GuiceFilter;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class WebApp {
	
	private static final Logger logger = LoggerFactory.getLogger(WebApp.class);
		
	private static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.in(Scopes.SINGLETON);
			// work database
			bind(WorkDatabase.class)
				.to(classForPrefix(WorkDatabase.class,
						configuration.getProperty("work_database")))
				.in(Scopes.SINGLETON);
			// collecting database
			bind(CollectingDatabase.class)
				.to(classForPrefix(CollectingDatabase.class,
						configuration.getProperty("collecting_database")))
				.in(Scopes.SINGLETON);
			// document store
			bind(DocumentStore.class)
				.to(classForPrefix(DocumentStore.class,
						configuration.getProperty("document_store")))
				.in(Scopes.SINGLETON);
			// result format(s)
			ResultFormats.loopOnFormats(new ResultFormats.Observer() {				
				@Override
				public void notify(String formatName, Class<? extends ConfigurableResultFormat> formatClass) {
					bind(ConfigurableResultFormat.class)
						.annotatedWith(Names.named(formatName))
						.to(formatClass);
				}
			});
			bind(ResultFormats.class)
				.asEagerSingleton();
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("sophia_kb"))
				.to(KbdbDataSource.class)
				.asEagerSingleton();

			// self
			bind(WebApp.class)
				.asEagerSingleton();
		}

		@Provides
		protected TitleArtistQuery provideTitleArtistQuery() {
			final String propertyPrefix = configuration
					.getProperty("title_artist_query");
			final String className = configuration
					.getProperty(propertyPrefix + ".class_name");
			if (ExactTitleArtistQuery.class.getName().equals(className)) {
				return new ExactTitleArtistQuery(configuration, propertyPrefix);
			} else if (FuzzyTitleArtistQuery.class.getName().equals(className)) {
				return new FuzzyTitleArtistQuery(configuration, propertyPrefix);
			}
			throw new IllegalArgumentException(String
					.format("unknown class name \"%s\"", className));
		}
		
		
	}
	
	public static Injector createInjector(String[] args) {
		return Guice.createInjector(new GuiceModuleExtension(args, "/webapp.properties"),
//				new TextProcessingModule(),
//				new IndexModule(),
//				new DocumentScoreModule(),
				//new SearchEngineModule()
//				new ResultFormatModule(),
				new TextProcessingModule(),
				new DocumentScoreModule(),
				new IndexModule(args,"/webapp.properties"),
				new ApplicationServletModule()
				);
	}
	
	public static void main(String[] args) {
		try {
			// WARNING: same injector used for container guice bootstrap
			final Injector injector = createInjector(args);
			final WebApp instance = injector
					.getInstance(WebApp.class)
					.startup();
			try {
				instance.process(injector);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Properties configuration;
	private final S3 s3;
	private final Charset charset;
	private final KbdbDAO dao;
	@Inject
	protected WebApp(@Named("configuration") Properties configuration,
			KbdbDAO dao,
			S3 s3,
			@Named("charset") Charset charset) {
		super();
		this.configuration = configuration;
		this.s3 = s3;
		this.charset = charset;
		this.dao = dao;
	}
	
	public WebApp startup() {
		if ("true".equalsIgnoreCase(configuration
				.getProperty("webapp.locked", "true"))) {
			throw new IllegalStateException("application locked");
		}
		return this;
	}

	public WebApp shutdown() {
		return this;
	}

	private WebApp process(Injector injector) throws Exception {
		final int port = Integer.parseInt(configuration
				.getProperty("webapp.port", "8080"));
		final int bindPort = Integer.parseInt(configuration
				.getProperty("webapp.bind_port", configuration
						.getProperty("default.bind_port", "0")));
		try (final ServerSocket socket = new ServerSocket(bindPort)) {
			logger.debug("lock socket bound to {}", socket.getLocalSocketAddress());	
			
			//verifico se la kb è aggiornata all'ultima versione
			this.loadKb();
			
			
			final Server server = new Server(port);
			final ServletContextHandler servletContextHandler = new ServletContextHandler(server,
					"/", ServletContextHandler.NO_SECURITY | ServletContextHandler.NO_SESSIONS);
			servletContextHandler.addEventListener(new JettyGuiceBootstrap(injector));
			servletContextHandler.addFilter(GuiceFilter.class, "/*", EnumSet.allOf(DispatcherType.class));
			servletContextHandler.addServlet(DefaultServlet.class, "/");
			server.start();
			server.join();
		}
		return this;
	}
	
	private void loadKb() {
		s3.startup();
		final File indexLocalFolder = new File(configuration.getProperty("identifier.index.local_folder"));
		final File indexLatestFile = new File(configuration.getProperty("identifier.index.latest_file", "_LATEST"));
		final String indexS3FolderUrl = configuration.getProperty("identifier.index.s3_folder_url");

		// check index _LATEST version
		final String latestS3FileUrl = new StringBuilder()
				.append(indexS3FolderUrl)
				.append('/')
				.append(indexLatestFile.getName())
				.toString();
		final String latestVersion = s3.getObjectContents(new S3.Url(latestS3FileUrl), charset);
		logger.debug("knowledge-base latestVersion {}", latestVersion);
		if (Strings.isNullOrEmpty(latestVersion)) {
			logger.error("latest knowledge-base not found on s3: {}", latestS3FileUrl);
			throw new IllegalStateException(String
					.format("latest knowledge-base not found on s3: %s", latestS3FileUrl));
		}
		// index metadata
		final ReverseIndexMetadata metadata = new ReverseIndexMetadata(indexLocalFolder);
		final String localVersion = metadata
				.getProperty(ReverseIndexMetadata.VERSION, null);
		logger.debug("knowledge-base localVersion {}", localVersion);
		// download latest index version from s3
		if (!latestVersion.equalsIgnoreCase(localVersion)) {
			
			// delete existing file(s)
			logger.debug("deleting local knowledge-base files");
			if (indexLocalFolder.exists())
				FileUtils.deleteFolder(indexLocalFolder, true);
			indexLocalFolder.mkdirs();

			// download s3 file(s)
			logger.debug("downloading latest knowledge-base files from s3");
			final String s3FolderUrl = new StringBuilder()
					.append(indexS3FolderUrl)
					.append('/')
					.append(latestVersion)
					.toString();
			final List<S3ObjectSummary> objects = s3.listObjects(new S3.Url(s3FolderUrl));
			for (S3ObjectSummary object : objects) {
				final String key = object.getKey();
				final String s3BucketUrl = new StringBuilder()
						.append("s3://")
						.append(object.getBucketName())
						.append('/')
						.toString();
				final String s3FileUrl = s3BucketUrl + key;
				logger.debug("s3FileUrl: {}", s3FileUrl);
				String relativePath = key.substring(s3FolderUrl.length() - s3BucketUrl.length());
				logger.debug("relativePath: {}", relativePath);
				if (Strings.isNullOrEmpty(relativePath) || relativePath.endsWith("/"))
					continue;
				while (relativePath.startsWith("/"))
					relativePath = relativePath.substring(1);
				logger.debug("relativePath: {}", relativePath);
				final File file = new File(indexLocalFolder, relativePath);
				logger.debug("file: {}", file);
				if (-1 != relativePath.indexOf('/'))
					file.getParentFile().mkdirs();
				if (!s3.download(new S3.Url(s3FileUrl), file)) {
					logger.error("knowledge-base file download error: {}", s3FileUrl);
					throw new IllegalStateException(String
							.format("knowledge-base file download error: %s", s3FileUrl));
				} else {
					logger.debug("knowledge-base file downloaded: {}", s3FileUrl);
				}
			}

		}
		s3.shutdown();
	}
}
