package com.alkemytech.sophia.identification.guice;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.alkemytech.sophia.identification.score.CustomDocumentScore;
import com.alkemytech.sophia.identification.score.DocumentScore;
import com.alkemytech.sophia.identification.score.GeometricDocumentScore;
import com.alkemytech.sophia.identification.score.LinearDocumentScore;
import com.google.inject.AbstractModule;
import com.google.inject.Provider;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class DocumentScoreModule extends AbstractModule {

	@Override
	protected void configure() {
		
		// WARNING: must NOT be bound in scopes singleton for thread safety

//		bind(DocumentScore.class)
//			.annotatedWith(Names.named("simple_document_score"))
//			.to(SimpleDocumentScore.class);
		
		bind(DocumentScore.class)
			.annotatedWith(Names.named("linear_score"))
			.to(LinearDocumentScore.class);

		bind(DocumentScore.class)
			.annotatedWith(Names.named("geometric_score"))
			.to(GeometricDocumentScore.class);

//		bind(DocumentScore.class)
//				.annotatedWith(Names.named("collision_document_score"))
//				.to(CollisionDocumentScore.class);

		bind(DocumentScore.class)
				.annotatedWith(Names.named("custom_score"))
				.to(CustomDocumentScore.class);
		
	}
	
	@Provides
	@Singleton
	@Named("document_score_providers")
	protected Map<String, Provider<DocumentScore>> provideDocumentScoreProviders(
			//@Named("simple_document_score") Provider<DocumentScore> simpleDocumentScore,
			@Named("linear_score") Provider<DocumentScore> linearDocumentScore,
			@Named("geometric_score") Provider<DocumentScore> geometricDocumentScore,
		 	//@Named("collision_document_score") Provider<DocumentScore> collisionDocumentScore,
		 	@Named("custom_score") Provider<DocumentScore> customDocumentScore) {
		final Map<String, Provider<DocumentScore>> providers = new ConcurrentHashMap<>();
		//providers.put("simple_document_score", simpleDocumentScore);
		providers.put("linear_score", linearDocumentScore);
		providers.put("geometric_score", geometricDocumentScore);
		//providers.put("collision_document_score", collisionDocumentScore);
		providers.put("custom_score", customDocumentScore);
		return providers;
	}

}
