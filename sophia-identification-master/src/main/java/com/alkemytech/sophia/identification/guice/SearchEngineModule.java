package com.alkemytech.sophia.identification.guice;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.alkemytech.sophia.identification.processor.IdentificationV1;
import com.alkemytech.sophia.identification.processor.IdentificationV2;
import com.alkemytech.sophia.identification.processor.IdentificationV3;
import com.alkemytech.sophia.identification.processor.MessageProcessor;
import com.google.inject.AbstractModule;
import com.google.inject.Provider;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class SearchEngineModule extends AbstractModule {

	@Override
	protected void configure() {
		
		// WARNING: must NOT be bound in scopes singleton for thread safety

		// message processor(s)
		bind(MessageProcessor.class)
			.annotatedWith(Names.named("identification_v1"))
			.to(IdentificationV1.class);
		bind(MessageProcessor.class)
			.annotatedWith(Names.named("identification_v2"))
			.to(IdentificationV2.class);
		bind(MessageProcessor.class)
		.annotatedWith(Names.named("identification_v3"))
		.to(IdentificationV3.class);
	}
	
	@Provides
	@Singleton
	@Named("identification_providers")
	protected Map<String, Provider<MessageProcessor>> provideIdentificationProviders(
			@Named("identification_v1") Provider<MessageProcessor> identificationV1,
			@Named("identification_v2") Provider<MessageProcessor> identificationV2,
			@Named("identification_v3") Provider<MessageProcessor> identificationV3
		) {
		final Map<String, Provider<MessageProcessor>> providers = new ConcurrentHashMap<>();
		providers.put("identification_v1", identificationV1);
		providers.put("identification_v2", identificationV2);
		providers.put("identification_v3", identificationV3);
		return providers;
	}

}
