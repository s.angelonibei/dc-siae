package com.alkemytech.sophia.identification.model;

import com.google.gson.GsonBuilder;
import com.sleepycat.persist.model.Persistent;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@Persistent
public class Maturato {

	public String valore;
	public String periodoLabel;
	public String dataDa;
	public String dataA;

	public Maturato() {
		super();
	}
	
	public Maturato(String valore, String periodoLabel, String dataDa, String dataA) {
		super();
		this.valore = valore;
		this.periodoLabel = periodoLabel==null?"":periodoLabel;
		this.dataDa = dataDa==null?"":dataDa;
		this.dataA = dataA==null?"":dataA;
	}

	@Override
	public int hashCode() {
		int result = 31 + (null == valore ? 0 : valore.hashCode());
		result = 31 * result + (null == periodoLabel ? 0 : periodoLabel.hashCode());
		result = 31 * result + (null == dataDa ? 0 : dataDa.hashCode());
		return 31 * result + (null == dataA ? 0 : dataA.hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (null == obj) {
			return false;
		} else if (getClass() != obj.getClass()) {
			return false;
		}
		final Maturato other = (Maturato) obj;
		
		if (null == valore) {
			if (null != other.valore) {
				return false;
			}
		} else if (!valore.equals(other.valore)) {
			return false;
		}
		if (null == periodoLabel) {
			if (null != other.periodoLabel) {
				return false;
			}
		} else if (!periodoLabel.equals(other.periodoLabel)) {
			return false;
		}
		if (null == dataDa) {
			if (null != other.dataDa) {
				return false;
			}
		} else if (!dataDa.equals(other.dataDa)) {
			return false;
		}
		if (null == dataA) {
			if (null != other.dataA) {
				return false;
			}
		} else if (!dataA.equals(other.dataA)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.disableHtmlEscaping()
				.create()
				.toJson(this);
	}

}
