package com.alkemytech.sophia.identification.score;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.score.TextScore;
import com.alkemytech.sophia.commons.util.SplitCharSequence;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.identification.document.ScoredDocument;
import com.alkemytech.sophia.identification.model.Document;
import com.alkemytech.sophia.identification.model.Maturato;
import com.alkemytech.sophia.identification.model.Text;
import com.alkemytech.sophia.identification.model.TextArtist;
import com.alkemytech.sophia.identification.model.TextType;
import com.alkemytech.sophia.identification.model.Work;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class CustomDocumentScore implements DocumentScore {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final double titleWeight;
	private final double titleExponent;
	private final TextScore titleScore;
	private final double artistWeight;
	private final double artistExponent;
	private final TextScore artistScore;
	private final double relevanceWeight;
	private final double relevanceExponent;
	private final int relevanceFlag;
	private final int relevancePeriod;
	
	private final double riskExponent;
	private final double riskWeight;
	private final int riskFlag;
	private final int riskPeriod;
	private final int maturatoTreshold1;
	private final int maturatoTreshold2;
	private final int maturatoTreshold3;
	private final double rischioTreshold1;
	private final double rischioTreshold2;
	private final double rischioTreshold3;
	
	@Inject
	public CustomDocumentScore(@Named("configuration") Properties configuration,
			@Named("document_score") String propertyPrefix) {
		super();
		logger.debug("propertyPrefix: {}", propertyPrefix);
		titleWeight = Double.parseDouble(configuration.getProperty(propertyPrefix + ".title_weight"));
		titleExponent = Double.parseDouble(configuration.getProperty(propertyPrefix + ".title_exponent"));
		titleScore = newScoreInstance(configuration.getProperty(propertyPrefix + ".title_score"));
		artistWeight = Double.parseDouble(configuration.getProperty(propertyPrefix + ".artist_weight"));
		artistExponent = Double.parseDouble(configuration.getProperty(propertyPrefix + ".artist_exponent"));
		artistScore = newScoreInstance(configuration.getProperty(propertyPrefix + ".artist_score"));

		relevanceWeight = Double.parseDouble(configuration.getProperty(propertyPrefix + ".relevance_weight"));
		relevanceExponent =  Double.parseDouble(configuration.getProperty(propertyPrefix + ".relevance_exponent"));
		relevanceFlag = Integer.parseInt(configuration.getProperty(propertyPrefix + ".relevance_flag"));
        relevancePeriod = TextUtils.parseIntSize(configuration.getProperty(propertyPrefix + ".relevance_period", "0"));
		
		riskWeight = Double.parseDouble(configuration.getProperty(propertyPrefix + ".risk_weight"));
		riskExponent =  Double.parseDouble(configuration.getProperty(propertyPrefix + ".risk_exponent"));
		riskFlag = Integer.parseInt(configuration.getProperty(propertyPrefix + ".risk_flag"));
        riskPeriod =      TextUtils.parseIntSize(configuration.getProperty(propertyPrefix + ".risk_period", "0"));

		maturatoTreshold1 = Integer.parseInt(configuration.getProperty(propertyPrefix + ".maturato.threshold.1"));
		maturatoTreshold2 = Integer.parseInt(configuration.getProperty(propertyPrefix + ".maturato.threshold.2"));
		maturatoTreshold3 = Integer.parseInt(configuration.getProperty(propertyPrefix + ".maturato.threshold.3"));

		rischioTreshold1 = Double.parseDouble(configuration.getProperty(propertyPrefix + ".rischio.threshold.1"));
		rischioTreshold2 = Double.parseDouble(configuration.getProperty(propertyPrefix + ".rischio.threshold.2"));
		rischioTreshold3 = Double.parseDouble(configuration.getProperty(propertyPrefix + ".rischio.threshold.3"));
		
		final double weightsSum = Math.abs(titleWeight + artistWeight + relevanceWeight + riskWeight);
		if (weightsSum > 1.0) {
			logger.warn("non unit weights sum {}", weightsSum);
		}
	}
	
	private TextScore newScoreInstance(String className) {
		try {
			return (TextScore) Class.forName(className).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			throw new IllegalArgumentException(String
					.format("unknown class name \"%s\"", className), e);
		}
	}

	public double getScore(SplitCharSequence title, Collection<SplitCharSequence> artists,Document document) {
		throw new NotImplementedException();
	}
	
	
	
	@Override
	public ScoredDocument getScore(SplitCharSequence title, Collection<SplitCharSequence> artists,Document document,Work work) {
		double relevanceScore = work.getMaturato(relevancePeriod);
		double riskScore = work.getRischisita(riskPeriod);
		return this.getScore(title, artists,  relevanceScore, riskScore, document );
	}

	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.identification.score.DocumentScore#getScore(com.alkemytech.sophia.commons.util.SplitCharSequence, java.util.Collection, com.alkemytech.sophia.identification.model.Document)
	 */
	public ScoredDocument getScore(SplitCharSequence title, Collection<SplitCharSequence> artists, double relevanceScore,  double riskScore, Document document) {
		double titleScore = 0.0;
		String titleDocument = "";
		double artistScore = 0.0;		
		String artistDocument = "";
		List<String> ipiDocument = new ArrayList<String>();
		for (Text text : document.texts) {
			if (TextType.title == text.type) {
				if (titleScore < 1.0) {
					final double score = this.titleScore
							.getScore(title, new SplitCharSequence(text.text));
					if (score > titleScore) {
						titleScore = score;
						titleDocument = text.text;
					}
				}
			} else if (TextType.artist == text.type) {
				if(((TextArtist)text).ipi != null && ((TextArtist)text).ipi.length()>0)
					ipiDocument.add(((TextArtist)text).ipi);
				if (artistScore < 1.0) {
					for (SplitCharSequence artist : artists) {
						final double score = this.artistScore.getScore(artist, new SplitCharSequence(text.text));
						if (score > artistScore) {
							artistScore = score;
							artistDocument = text.text;
						}
					}
				}
			}
		}
		double score = titleWeight * Math.pow(titleScore, titleExponent) 
				+ artistWeight * Math.pow(artistScore, artistExponent) 
				+ (relevanceFlag == 0 ? 0 : (relevanceWeight * Math.pow(normalizeRelevance(relevanceScore), relevanceExponent)))
				+ (riskFlag == 0 ? 0: (riskWeight * Math.pow(normalizeRisk(riskScore), riskExponent)))
				;
		 
		return new ScoredDocument(document, score,titleDocument,artistDocument,ipiDocument);
	}

	public double normalizeRelevance(double value) {
		if (value > 0 && value < maturatoTreshold1)
			return normalizeRelevance(value, 0, 0, maturatoTreshold1, 0.33);

		if (value >= maturatoTreshold1 && value < maturatoTreshold2)
			return normalizeRelevance(value, maturatoTreshold1, 0.33, maturatoTreshold2, 0.66);

		if (value >= maturatoTreshold2 && value < maturatoTreshold3)
			return normalizeRelevance(value, maturatoTreshold2, 0.66, maturatoTreshold3, 1.0);

		if (value >= maturatoTreshold3)
			return 1.0;

		return 0;
	}

	private double normalizeRelevance(double value, double x1, double y1, double x2, double y2) {
		return y1 + (value - x1) * (y2 - y1) / (x2 - x1);
	}

	public double normalizeRisk(double risk) {
		if (risk < rischioTreshold1) {
			return 1 - 0.5 * (risk / rischioTreshold1);
		} else if (risk < rischioTreshold2) {
			return 0.5 - 0.5 * (risk - rischioTreshold1) / (rischioTreshold2 - rischioTreshold1);
		} else {
			return 0.0;
		}
	}

}
