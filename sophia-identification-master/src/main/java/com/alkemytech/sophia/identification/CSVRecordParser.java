package com.alkemytech.sophia.identification;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.util.GsonUtils;
import com.google.common.base.Strings;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class CSVRecordParser {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final int column;
	private final String defaultValue;
	private final String emptyValue;
	private final String splitRegex;
	private final Map<String,String> mapping;
	private final String defaultMapping;

	@Override
	public String toString() {
		return "CSVRecordParser{" +
				"column=" + column +
				", defaultValue='" + defaultValue + '\'' +
				", emptyValue='" + emptyValue + '\'' +
				", splitRegex='" + splitRegex + '\'' +
				", mapping=" + mapping +
				", defaultMapping='" + defaultMapping + '\'' +
				'}';
	}

	public CSVRecordParser(Properties configuration, String propertyPrefix) {
		super();
		
//		# csv record parser parameter(s)
//		# <prefix>.column=<zero based index of column; if not specified column value is not read from file>
//		# <prefix>.default_value=<default value for columns not read from file; if not specified column value remains null>
//		# <prefix>.empty_value=<default value for empty columns; if not specified column value remains empty>
//		# <prefix>.split_regex=<regular expression used to split columns containing multiple values; if not specified column is not split>
//		# <prefix>.mapping=<json containing mapping to decode column values; if not specified column value is not mapped>
//		# <prefix>.default_mapping=<default value for unmapped values; if not specified unmapped column value remains unchanged>

		this.column = Integer.parseInt(configuration
				.getProperty(propertyPrefix + ".column", "-1"));
		this.defaultValue = configuration
				.getProperty(propertyPrefix + ".default_value");
		this.emptyValue = configuration
				.getProperty(propertyPrefix + ".empty_value");
		this.splitRegex = configuration
				.getProperty(propertyPrefix + ".split_regex");
		final Map<String,String> mapping = GsonUtils
				.decodeJsonMap(configuration
						.getProperty(propertyPrefix + ".mapping"));
		this.mapping = null == mapping || mapping.isEmpty() ?
				null : new ConcurrentHashMap<>(mapping);
		this.defaultMapping = configuration
				.getProperty(propertyPrefix + ".default_mapping");
						
		// log config value(s)
		final boolean debug = "true".equalsIgnoreCase(configuration
				.getProperty("identifier.debug", configuration
						.getProperty("default.debug")));

			logger.debug("propertyPrefix: {}", propertyPrefix);
			logger.debug("\tcolumn: {}", column);
			logger.debug("\tdefaultValue: {}", defaultValue);
			logger.debug("\temptyValue: {}", emptyValue);
			logger.debug("\tsplitRegex: {}", splitRegex);
			logger.debug("\tmapping: {}", mapping);
			logger.debug("\tdefaultMapping: {}", defaultMapping);

	}
	
	private String getMappedValue(String value) {
		value = mapping.get(value);
		if (null == value && null != defaultMapping) {
			return defaultMapping;
		}
		return value;
	}
	
	public void parse(List<String> results, CSVRecord record) {
		
		String value = defaultValue;
		if (-1 != column) {
			value = record.get(column);
			if (null != emptyValue && Strings.isNullOrEmpty(value)) {
				value = emptyValue;
			}
		}
		if (null != mapping) {
			if (null != splitRegex) {
				for (String substring : value.split(splitRegex)) {
					results.add(getMappedValue(substring));					
				}
			} else {
				results.add(getMappedValue(value));					
			}
		} else {
			if (null != splitRegex) {
				for (String substring : value.split(splitRegex)) {
					results.add(substring);					
				}
			} else {
				results.add(value);					
			}
		}

	}
	
}
