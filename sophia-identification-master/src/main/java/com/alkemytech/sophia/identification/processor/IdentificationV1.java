package com.alkemytech.sophia.identification.processor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.guice.ConfigurationLoader;
import com.alkemytech.sophia.commons.index.LevenshteinIndex.TermSet;
import com.alkemytech.sophia.commons.io.CompressionAwareFileInputStream;
import com.alkemytech.sophia.commons.io.CompressionAwareFileOutputStream;
import com.alkemytech.sophia.commons.index.ReverseIndex;
import com.alkemytech.sophia.commons.query.TitleArtistQuery;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.text.TextNormalizer;
import com.alkemytech.sophia.commons.text.TextTokenizer;
import com.alkemytech.sophia.commons.util.FileUtils;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.LongArray;
import com.alkemytech.sophia.commons.util.SplitCharSequence;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.identification.CSVRecordParser;
import com.alkemytech.sophia.identification.MatchType;
import com.alkemytech.sophia.identification.collecting.CollectingDatabase;
import com.alkemytech.sophia.identification.document.DocumentStore;
import com.alkemytech.sophia.identification.document.ScoredDocument;
import com.alkemytech.sophia.identification.index.ReverseIndexMetadata;
import com.alkemytech.sophia.identification.model.CodeType;
import com.alkemytech.sophia.identification.model.CollectingCode;
import com.alkemytech.sophia.identification.model.Document;
import com.alkemytech.sophia.identification.result.ResultFormat;
import com.alkemytech.sophia.identification.result.ResultFormats;
import com.alkemytech.sophia.identification.result.ResultPosition;
import com.alkemytech.sophia.identification.score.DocumentScore;
import com.alkemytech.sophia.identification.work.WorkDatabase;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class IdentificationV1 implements MessageProcessor {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	private final Properties configuration;
	private final Charset charset;	
	private final S3 s3;
	private final Provider<WorkDatabase> workDatabaseProvider;
	private final Provider<CollectingDatabase> collectingDatabaseProvider;
	private final Provider<DocumentStore> documentStoreProvider;
	private final Provider<ReverseIndex> iswcIndexProvider;
	private final Provider<ReverseIndex> isrcIndexProvider;
	private final Provider<ReverseIndex> titleIndexProvider;
	private final Provider<ReverseIndex> artistIndexProvider;
	private final Provider<TextNormalizer> iswcNormalizerProvider;
	private final Provider<TextNormalizer> isrcNormalizerProvider;
	private final Provider<TextNormalizer> titleNormalizerProvider;
	private final Provider<TextNormalizer> artistNormalizerProvider;
	private final Provider<TextTokenizer> titleTokenizerProvider;
	private final Provider<TextTokenizer> artistTokenizerProvider;
	private final Provider<TitleArtistQuery> titleArtistQueryProvider;
	private final Map<String, Provider<DocumentScore>> documentScoreProvider;

	private JsonObject result;
	private JsonObject error;
	
	@Inject
	protected IdentificationV1(@Named("configuration") Properties configuration,
			@Named("charset") Charset charset, S3 s3,
			Provider<WorkDatabase> workDatabaseProvider,
			Provider<CollectingDatabase> collectingDatabaseProvider,
			Provider<DocumentStore> documentStoreProvider,
			@Named("iswc_index") Provider<ReverseIndex> iswcIndexProvider,
			@Named("isrc_index") Provider<ReverseIndex> isrcIndexProvider,
			@Named("title_index") Provider<ReverseIndex> titleIndexProvider,
			@Named("artist_index") Provider<ReverseIndex> artistIndexProvider,
			@Named("iswc_normalizer") Provider<TextNormalizer> iswcNormalizerProvider,
			@Named("isrc_normalizer") Provider<TextNormalizer> isrcNormalizerProvider,
			@Named("title_normalizer") Provider<TextNormalizer> titleNormalizerProvider,
			@Named("artist_normalizer") Provider<TextNormalizer> artistNormalizerProvider,
			@Named("title_tokenizer") Provider<TextTokenizer> titleTokenizerProvider,
			@Named("artist_tokenizer") Provider<TextTokenizer> artistTokenizerProvider,
			Provider<TitleArtistQuery> titleArtistQueryProvider,
			@Named("document_score_providers") Map<String, Provider<DocumentScore>> documentScoreProvider) throws IOException {
		super();
		this.configuration = configuration;
		this.charset = charset;
		this.s3 = s3;
		this.workDatabaseProvider = workDatabaseProvider;
		this.collectingDatabaseProvider = collectingDatabaseProvider;
		this.documentStoreProvider = documentStoreProvider;
		this.iswcIndexProvider = iswcIndexProvider;
		this.isrcIndexProvider = isrcIndexProvider;
		this.titleIndexProvider = titleIndexProvider;
		this.artistIndexProvider = artistIndexProvider;
		this.iswcNormalizerProvider = iswcNormalizerProvider;
		this.isrcNormalizerProvider = isrcNormalizerProvider;
		this.titleNormalizerProvider = titleNormalizerProvider;
		this.artistNormalizerProvider = artistNormalizerProvider;
		this.titleTokenizerProvider = titleTokenizerProvider;
		this.artistTokenizerProvider = artistTokenizerProvider;
		this.titleArtistQueryProvider = titleArtistQueryProvider;
		this.documentScoreProvider = documentScoreProvider;
	}
	
	private List<ScoredDocument> findBestDocument(List<ScoredDocument> results, DocumentStore documentStore, DocumentScore documentScore, SplitCharSequence normalizedTitle, Collection<SplitCharSequence> normalizedArtists, LongArray postings, int maxPostings, double minScore, double autoScore) throws IOException {
		final int limit = Math.min(maxPostings, postings.size());
		double bestScore = 0.0;
		Document bestDocument = null;
		for (int i = 0; i < limit && bestScore < autoScore; i ++) {
			final Document document = documentStore
					.getById(postings.get(i));
			final double score = documentScore
					.getScore(normalizedTitle, normalizedArtists, document);
			if (bestScore < score) {
				bestScore = score;
				bestDocument = document;
			}
		}
		if (bestScore >= minScore) {
			results.add(new ScoredDocument(bestDocument, bestScore,null,null,null));
		}
		return results;
	}
	
	private List<ScoredDocument> findTopDocuments(List<ScoredDocument> results, DocumentStore documentStore, DocumentScore documentScore, SplitCharSequence normalizedTitle, Collection<SplitCharSequence> normalizedArtists, LongArray postings, int maxPostings, int maxResults, double minScore) throws IOException {
		final int limit = Math.min(maxPostings, postings.size());
		final double[] bestScores = new double[maxResults];
		final Document[] bestDocuments = new Document[maxResults];
		int count = 0;
		for (int i = 0; i < limit; i ++) {
			final Document document = documentStore
					.getById(postings.get(i));
			final double score = documentScore
					.getScore(normalizedTitle, normalizedArtists, document);
			if (score >= minScore) {
				boolean append = true;
				for (int j = 0; j < count; j ++) {
					if (score > bestScores[j]) {
						if (count < maxResults)
							count ++;
						for (int k = count - 1; k > j; k --) {
							bestScores[k] = bestScores[k-1];
							bestDocuments[k] = bestDocuments[k-1];
						}							
						bestScores[j] = score;
						bestDocuments[j] = document;
						append = false;
						break;
					}
				}
				if (append && count < maxResults) {
					bestScores[count] = score;
					bestDocuments[count] = document;
					count ++;
				}
			}
		}
		for (int i = 0; i < count; i ++) {
			results.add(new ScoredDocument(bestDocuments[i], bestScores[i],null,null,null));
		}
		return results;
	}

	private List<ScoredDocument> getScoredDocuments(List<ScoredDocument> results, DocumentStore documentStore, DocumentScore documentScore, SplitCharSequence normalizedTitle, Collection<SplitCharSequence> normalizedArtists, LongArray postings, int maxPostings, int maxResults, double minScore, double autoScore) throws IOException {
		return maxResults > 1 ?
			findTopDocuments(results, documentStore, documentScore, normalizedTitle, normalizedArtists, postings, maxPostings, maxResults, minScore) :
				findBestDocument(results, documentStore, documentScore, normalizedTitle, normalizedArtists, postings, maxPostings, minScore, autoScore);
	}

	private void processMessage(JsonObject input, JsonObject output) throws Exception {
		
		logger.debug("processing single item message");
		final long beginTimeMillis = System.currentTimeMillis();
		
		// parse input json message
		final JsonObject inputBody = GsonUtils.getAsJsonObject(input, "body");
		final String compression = GsonUtils.getAsString(inputBody, "compression", "guess");
		final String configUrl = GsonUtils.getAsString(inputBody, "configUrl");
		final JsonArray inputItems = GsonUtils.getAsJsonArray(inputBody, "items");
		if (null == inputItems || 1 != inputItems.size()) {
			throw new IllegalArgumentException("invalid input message: single item v1 message expected");
		}
		final JsonObject inputItem = GsonUtils.getAsJsonObject(inputItems, 0);
		final String inputUrl = GsonUtils.getAsString(inputItem, "inputUrl");
		final long inputFirstRow = Long.parseLong(GsonUtils
				.getAsString(inputItem, "inputFirstRow", "1"));
		final long inputLastRow = Long.parseLong(GsonUtils
				.getAsString(inputItem, "inputLastRow", Long.toString(Long.MAX_VALUE)));
		final String outputUrl = GsonUtils.getAsString(inputItem, "outputUrl");
		
		// non-overridable default parameters
		final File homeFolder = new File(configuration
				.getProperty("default.home_folder"));
		final int threadCount = Integer.parseInt(configuration
				.getProperty("identifier.thread_count", "1"));

		final File indexLatestFile = new File(configuration
				.getProperty("identifier.index.latest_file", "_LATEST"));
		final String indexS3FolderUrl = configuration
				.getProperty("identifier.index.s3_folder_url");
		final File indexLocalFolder = new File(configuration
				.getProperty("identifier.index.local_folder"));
		
		final long statsFrequency = TextUtils.parseLongDuration(configuration
				.getProperty("identifier.stats_frequency"));
		final boolean debug = "true".equalsIgnoreCase(configuration
				.getProperty("identifier.debug", configuration
						.getProperty("default.debug")));
		
		// override default parameters with properties from config url
		logger.debug("configUrl {}", configUrl);
		if (Strings.isNullOrEmpty(configUrl))
			throw new IllegalArgumentException("missing mandatory parameter \"configUrl\"");
		final File localConfigFile = File.createTempFile("__cfg__",
				"__" + configUrl.substring(1 + configUrl.lastIndexOf('/')), homeFolder);
		localConfigFile.deleteOnExit();
		logger.debug("downloading config file {}", configUrl);
		if (!s3.download(new S3.Url(configUrl), localConfigFile)) {
			logger.warn("config file download error: {}", configUrl);
			throw new IllegalStateException(String
					.format("config file download error: %s", configUrl));
		}
		final Properties properties = new ConfigurationLoader()
			.withFilePath(localConfigFile.getAbsolutePath())
				.load(configuration);
		ConfigurationLoader.expandVariables(properties);
		configuration.putAll(properties);
		if (debug)
			logger.debug("configuration: {}", new GsonBuilder()
					.disableHtmlEscaping().setPrettyPrinting().create().toJson(properties));
		
		// overridable config parameters
		final double iswcMinScore = Double.parseDouble(configuration
				.getProperty("identifier.min_score.iswc"));
		final double isrcMinScore = Double.parseDouble(configuration
				.getProperty("identifier.min_score.isrc"));
		final double titleArtistMinScore = Double.parseDouble(configuration
				.getProperty("identifier.min_score.title_artist"));
		final double autoScore = Double.parseDouble(configuration
				.getProperty("identifier.auto_score", "1.0"));
		final int maxPostings = TextUtils.parseIntSize(configuration
				.getProperty("identifier.max_postings"));
		final int maxResults = TextUtils.parseIntSize(configuration
				.getProperty("identifier.max_results"));
		
		int rowsToSkip = Integer.parseInt(configuration
				.getProperty("identifier.input.rows_to_skip", "0"));
		final char inputDelimiter = configuration
				.getProperty("identifier.input.delimiter", ";").trim().charAt(0);

		final char outputDelimiter = configuration
				.getProperty("identifier.output.delimiter", ";").trim().charAt(0);	
		final boolean copySkippedRows = "true".equalsIgnoreCase(configuration
				.getProperty("identifier.output.copy_skipped_rows"));
		final boolean echoParsedRows = "true".equalsIgnoreCase(configuration
				.getProperty("identifier.output.echo_parsed_rows"));
		
		logger.debug("iswcMinScore: {}", iswcMinScore);
		logger.debug("isrcMinScore: {}", isrcMinScore);
		logger.debug("titleArtistMinScore: {}", titleArtistMinScore);
		logger.debug("maxPostings: {}", maxPostings);
		logger.debug("maxResults: {}", maxResults);
		logger.debug("rowsToSkip: {}", rowsToSkip);
		logger.debug("inputDelimiter: {}", inputDelimiter);
		logger.debug("outputDelimiter: {}", outputDelimiter);
		logger.debug("copySkippedRows: {}", copySkippedRows);
		logger.debug("echoParsedRows: {}", echoParsedRows);
		
		// check index _LATEST version
		final String latestS3FileUrl = new StringBuilder()
				.append(indexS3FolderUrl)
				.append('/')
				.append(indexLatestFile.getName())
				.toString();
		final String latestVersion = s3
				.getObjectContents(new S3.Url(latestS3FileUrl), charset);
		logger.debug("knowledge-base latestVersion {}", latestVersion);
		if (Strings.isNullOrEmpty(latestVersion)) {
			logger.error("latest knowledge-base not found on s3: {}", latestS3FileUrl);
			throw new IllegalStateException(String
					.format("latest knowledge-base not found on s3: %s", latestS3FileUrl));
		}
		// index metadata
		final ReverseIndexMetadata metadata = new ReverseIndexMetadata(indexLocalFolder);
		final String localVersion = metadata
				.getProperty(ReverseIndexMetadata.VERSION, null);
		logger.debug("knowledge-base localVersion {}", localVersion);
		// download latest index version from s3
		if (!latestVersion.equalsIgnoreCase(localVersion)) {
			
			// delete existing file(s)
			logger.debug("deleting local knowledge-base files");
			if (indexLocalFolder.exists())
				FileUtils.deleteFolder(indexLocalFolder, true);
			indexLocalFolder.mkdirs();

			// download s3 file(s)
			logger.debug("downloading latest knowledge-base files from s3");
			final String s3FolderUrl = new StringBuilder()
					.append(indexS3FolderUrl)
					.append('/')
					.append(latestVersion)
					.toString();
			final List<S3ObjectSummary> objects = s3.listObjects(new S3.Url(s3FolderUrl));
			for (S3ObjectSummary object : objects) {
				final String key = object.getKey();
				final String s3BucketUrl = new StringBuilder()
						.append("s3://")
						.append(object.getBucketName())
						.append('/')
						.toString();
				final String s3FileUrl = s3BucketUrl + key;
				logger.debug("s3FileUrl: {}", s3FileUrl);
				String relativePath = key.substring(s3FolderUrl.length() - s3BucketUrl.length());
				logger.debug("relativePath: {}", relativePath);
				if (Strings.isNullOrEmpty(relativePath) || relativePath.endsWith("/"))
					continue;
				while (relativePath.startsWith("/"))
					relativePath = relativePath.substring(1);
				logger.debug("relativePath: {}", relativePath);
				final File file = new File(indexLocalFolder, relativePath);
				logger.debug("file: {}", file);
				if (-1 != relativePath.indexOf('/'))
					file.getParentFile().mkdirs();
				if (!s3.download(new S3.Url(s3FileUrl), file)) {
					logger.error("knowledge-base file download error: {}", s3FileUrl);
					throw new IllegalStateException(String
							.format("knowledge-base file download error: %s", s3FileUrl));
				} else {
					logger.debug("knowledge-base file downloaded: {}", s3FileUrl);
				}
			}

		}
		
		// startup database(s)
		final WorkDatabase workDatabase = workDatabaseProvider.get();
		final CollectingDatabase collectingDatabase = collectingDatabaseProvider.get();
		final DocumentStore documentStore = documentStoreProvider.get();
		workDatabase.startup();
		collectingDatabase.startup();
		documentStore.startup();

		// setup index(es)
		final ReverseIndex iswcIndex = iswcIndexProvider.get();
		final ReverseIndex isrcIndex = isrcIndexProvider.get();
		final ReverseIndex titleIndex = titleIndexProvider.get();
		final ReverseIndex artistIndex = artistIndexProvider.get();

		// temporary local file(s)
		final File localInputFile = File.createTempFile("__in__",
				"__" + inputUrl.substring(1 + inputUrl.lastIndexOf('/')), homeFolder);
		final File localOutputFile = File.createTempFile("__out__",
				"__" + outputUrl.substring(1 + outputUrl.lastIndexOf('/')), homeFolder);
		localInputFile.deleteOnExit();
		localOutputFile.deleteOnExit();

		// download file from s3
		logger.debug("downloading input file: {}", inputUrl);
		if (!s3.download(new S3.Url(inputUrl), localInputFile)) {
			logger.warn("input file download error: {}", inputUrl);
			throw new IOException(String
					.format("input file download error: %s", inputUrl));
		}
		
		// stats
		final AtomicLong totalRecords = new AtomicLong(0L);
		final AtomicLong identifiedRecords = new AtomicLong(0L);
		final AtomicLong identifiedRecordsWorkCode = new AtomicLong(0L);
		final AtomicLong identifiedRecordsIswc = new AtomicLong(0L);
		final AtomicLong identifiedRecordsIsrc = new AtomicLong(0L);
		final AtomicLong identifiedRecordsTitleArtist = new AtomicLong(0L);
		final AtomicLong invalidRecords = new AtomicLong(0L);
		final AtomicLong workCodeQueries = new AtomicLong(0L);
		final AtomicLong iswcQueries = new AtomicLong(0L);
		final AtomicLong iswcQueriesWithResults = new AtomicLong(0L);
		final AtomicLong isrcQueries = new AtomicLong(0L);
		final AtomicLong isrcQueriesWithResults = new AtomicLong(0L);
		final AtomicLong titleArtistQueries = new AtomicLong(0L);
		final AtomicLong titleArtistQueriesWithResults = new AtomicLong(0L);
		final AtomicLong iswcPostings = new AtomicLong(0L);
		final AtomicLong isrcPostings = new AtomicLong(0L);
		final AtomicLong titleArtistPostings = new AtomicLong(0L);

		logger.debug("processing input file {}", localInputFile);
		long elapsedTimeMillis = 0L;
		
		try (final CompressionAwareFileOutputStream outputStream = new CompressionAwareFileOutputStream(localOutputFile, compression);
				final OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, charset);
				final BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
				final CSVPrinter csvPrinter = new CSVPrinter(bufferedWriter,  CSVFormat.newFormat(outputDelimiter)
						.withQuoteMode(QuoteMode.MINIMAL)
						.withQuote('"')
						.withRecordSeparator('\n'));
				final CompressionAwareFileInputStream inputStream = new CompressionAwareFileInputStream(localInputFile, compression);
				final InputStreamReader inputStreamReader = new InputStreamReader(inputStream, charset);
				final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				final CSVParser csvParser = new CSVParser(bufferedReader, CSVFormat.newFormat(inputDelimiter)
						.withIgnoreEmptyLines()
						.withIgnoreSurroundingSpaces()
						.withQuoteMode(QuoteMode.MINIMAL)
						.withQuote('"'))) {	
			
			final Iterator<CSVRecord> csvIterator = csvParser.iterator();
			final AtomicLong rownum = new AtomicLong(1L);
			
			// goto input first row
			for ( ; rownum.get() < inputFirstRow && csvIterator.hasNext();
					rownum.incrementAndGet(), rowsToSkip --) {
				csvIterator.next();
			}
			
			// skip initial row(s)
			for ( ; rowsToSkip > 0 && rownum.get() <= inputLastRow &&
					csvIterator.hasNext(); rownum.incrementAndGet(), rowsToSkip --) {
				final CSVRecord record = csvIterator.next();
				logger.debug("record skipped {}", record);
				if (copySkippedRows) {
					for (String value : record) {
						csvPrinter.print(value);
					}
					csvPrinter.print("");
					csvPrinter.println();
					logger.debug("record copied to output");
				}
			}
			
			final Object monitor = new Object();
			final ExecutorService executorService = Executors.newCachedThreadPool();
			final AtomicInteger runningThreads = new AtomicInteger(0);
			final AtomicReference<Exception> threadException = new AtomicReference<>(null);
			while (runningThreads.get() < threadCount) {
				logger.debug("starting version 1.0.6.1");
				logger.debug("starting thread {}", runningThreads.incrementAndGet());
				executorService.execute(new Runnable() {
					
					@Override
					public void run() {
						try {
							// WARNING: duplicate instances for thread safety
							
							// csv record parser(s)
							final List<CSVRecordParser> titlesParsers = new ArrayList<>();
							for (String prefix : configuration.getProperty("identifier.input.titles", "").split(","))
								if (!Strings.isNullOrEmpty(prefix))
									titlesParsers.add(new CSVRecordParser(configuration, prefix));
							final List<CSVRecordParser> titleTypesParsers = new ArrayList<>();
							for (String prefix : configuration.getProperty("identifier.input.title_types", "").split(","))
								if (!Strings.isNullOrEmpty(prefix))
									titleTypesParsers.add(new CSVRecordParser(configuration, prefix));
							final List<CSVRecordParser> artistsParsers = new ArrayList<>();
							for (String prefix : configuration.getProperty("identifier.input.artists", "").split(","))
								if (!Strings.isNullOrEmpty(prefix))
									artistsParsers.add(new CSVRecordParser(configuration, prefix));
							final List<CSVRecordParser> artistTypesParsers = new ArrayList<>();
							for (String prefix : configuration.getProperty("identifier.input.artist_types", "").split(","))
								if (!Strings.isNullOrEmpty(prefix))
									artistTypesParsers.add(new CSVRecordParser(configuration, prefix));
							final List<CSVRecordParser> codesParsers = new ArrayList<>();
							for (String prefix : configuration.getProperty("identifier.input.codes", "").split(","))
								if (!Strings.isNullOrEmpty(prefix))
									codesParsers.add(new CSVRecordParser(configuration, prefix));
							final List<CSVRecordParser> codeTypesParsers = new ArrayList<>();
							for (String prefix : configuration.getProperty("identifier.input.code_types", "").split(","))
								if (!Strings.isNullOrEmpty(prefix))
									codeTypesParsers.add(new CSVRecordParser(configuration, prefix));

							// result format(s)
							final List<ResultFormat> resultFormats = new ArrayList<>();
							for (String prefix : configuration.getProperty("identifier.output.result_formats", "").split(","))
								if (!Strings.isNullOrEmpty(prefix))
									resultFormats.add(ResultFormats.getInstance(configuration, prefix));
							final List<ResultPosition> resultPositions = new ArrayList<>();
							for (String position : configuration.getProperty("identifier.output.result_positions", "").split(","))
								if (!Strings.isNullOrEmpty(position))
									resultPositions.add(ResultPosition.valueOf(position));

							final TextNormalizer iswcNormalizer = iswcNormalizerProvider.get();
							final TextNormalizer isrcNormalizer = isrcNormalizerProvider.get();
							final TextNormalizer titleNormalizer = titleNormalizerProvider.get();
							final TextNormalizer artistNormalizer = artistNormalizerProvider.get();
							final TextTokenizer titleTokenizer = titleTokenizerProvider.get();
							final TextTokenizer artistTokenizer = artistTokenizerProvider.get();
							
							final TitleArtistQuery titleArtistQuery = titleArtistQueryProvider.get();
							final DocumentScore documentScore = documentScoreProvider.get(configuration.getProperty("document_score")).get();
							final List<String> titles = new ArrayList<>();
							final List<String> titleTypes = new ArrayList<>();
							final List<String> artists = new ArrayList<>();
							final List<String> artistTypes = new ArrayList<>();
							final List<String> codes = new ArrayList<>();
							final List<String> codeTypes = new ArrayList<>();
							final Set<String> iswcCodes = new HashSet<>();
							final Set<String> isrcCodes = new HashSet<>();
							final Set<String> workCodes = new HashSet<>();
							final List<ScoredDocument> scoredDocuments = new ArrayList<>();
							final List<String> outputRecord = new ArrayList<>();

							while (null == threadException.get()) {

								// read next csv line
								final CSVRecord inputRecord;

								synchronized (csvIterator) {
									if (rownum.get() <= inputLastRow && csvIterator.hasNext()) {
										inputRecord = csvIterator.next();
										logger.debug("CSVRecord: " + inputRecord.toString());
										rownum.incrementAndGet();
									} else {
										return;
									}
								}
								
								// initialize variables
								totalRecords.incrementAndGet();
								scoredDocuments.clear();
								MatchType matchType = MatchType.unidentified;
								
								// parse line
								logger.debug("Inizio parse line");

								if (!titlesParsers.isEmpty()) { // title(s)
									titles.clear();
									for (CSVRecordParser parser : titlesParsers)
										parser.parse(titles, inputRecord);									
								}
								if (!titleTypesParsers.isEmpty()) { // title type(s)
									titleTypes.clear();
									for (CSVRecordParser parser : titleTypesParsers)
										parser.parse(titleTypes, inputRecord);									
								}
								if (!artistsParsers.isEmpty()) { // artist(s)
									artists.clear();
									for (CSVRecordParser parser : artistsParsers)
										parser.parse(artists, inputRecord);									
								}
								if (!artistTypesParsers.isEmpty()) { // artist type(s)
									artistTypes.clear();
									for (CSVRecordParser parser : artistTypesParsers)
										parser.parse(artistTypes, inputRecord);
								}
								if (!codesParsers.isEmpty()) { // code(s)
									codes.clear();
									for (CSVRecordParser parser : codesParsers)
										parser.parse(codes, inputRecord);
								}								
								if (!codeTypesParsers.isEmpty()) { // code type(s)
									codeTypes.clear();
									for (CSVRecordParser parser : codeTypesParsers)
										parser.parse(codeTypes, inputRecord);
								}

									logger.debug("titles: {}", titles);
									logger.debug("titleTypes: {}", titleTypes);
									logger.debug("artists: {}", artists);
									logger.debug("artistTypes: {}", artistTypes);
									logger.debug("codes: {}", codes);
									logger.debug("codeTypes: {}", codeTypes);


								// title(s)
								// WARNING: uses first title
								logger.debug("uses first title");
								String title = titles.isEmpty() ? null : titles.get(0);
								title = titleNormalizer.normalize(title);
								if (Strings.isNullOrEmpty(title)) {
									logger.debug("titles: {}", title);
									invalidRecords.incrementAndGet();
									continue;
								}
								final SplitCharSequence normalizedTitle = new SplitCharSequence(title);

								logger.debug("normalizedTitle: \"{}\"", normalizedTitle);

								// artist(s)
								final Set<SplitCharSequence> normalizedArtists = new HashSet<>();
								for (String artist : artists) {
									artist = artistNormalizer.normalize(artist);
									if (!Strings.isNullOrEmpty(artist)) {
										normalizedArtists.add(new SplitCharSequence(artist));
									}
								}
								logger.debug("normalizedArtists: {}", normalizedArtists);

								if (normalizedArtists.isEmpty()) {
									logger.debug("artists: {}", artists);
									invalidRecords.incrementAndGet();
									continue;
								}
								
								// code(s)
								if (!codes.isEmpty() && !codeTypes.isEmpty()) {
									iswcCodes.clear();
									isrcCodes.clear();
									workCodes.clear();
									for (int i = codes.size() - 1; i >= 0; i --) {
										String code;
										switch (CodeType.valueOf(codeTypes.get(i))) {
										case CodeType.iswc:
											code = codes.get(i);
											if (!Strings.isNullOrEmpty(code))
												iswcCodes.add(code);
											break;
										case CodeType.isrc:
											code = codes.get(i);
											if (!Strings.isNullOrEmpty(code))
												isrcCodes.add(code);
											break;
//										case CodeType.siae:
//											code = codes.get(i);
//											if (SiaeCode.isValid(code))
//												collectingCodes.add(code);
//											break;
										default:
											code = codes.get(i);
											if (!Strings.isNullOrEmpty(code))
												workCodes.add(code);
											break;
										}
									}
									
									// collecting code
									for (String workCode : workCodes) {
										if (!Strings.isNullOrEmpty(workCode)) {
											workCodeQueries.incrementAndGet();
											final CollectingCode collectingCode = collectingDatabase.getByCode(workCode);
											if (null != collectingCode) {
												// WARNING: fake document must be handled correctly by result format(s)
												scoredDocuments.add(new ScoredDocument(new Document(-1, collectingCode.workId, null), autoScore,null,null,null));
												matchType = MatchType.work_code;
												identifiedRecordsWorkCode.incrementAndGet();
												identifiedRecords.incrementAndGet();
												break;														
											}
										}
									}

									// iswc code
									if (scoredDocuments.isEmpty()) {
										for (String iswc : iswcCodes) {
											iswc = iswcNormalizer.normalize(iswc);
											if (!Strings.isNullOrEmpty(iswc)) {
												iswcQueries.incrementAndGet();
												final TermSet termSet = iswcIndex.search(iswc);
												final LongArray postings = termSet.next() ? termSet.getPostings() : null;
												if (null != postings && !postings.isEmpty()) {
													iswcPostings.addAndGet(postings.size());
													iswcQueriesWithResults.incrementAndGet();
													getScoredDocuments(scoredDocuments, documentStore, documentScore,
															normalizedTitle, normalizedArtists, postings, maxPostings, maxResults, iswcMinScore, autoScore);
													if (!scoredDocuments.isEmpty()) {
														matchType = MatchType.iswc;
														identifiedRecordsIswc.incrementAndGet();
														identifiedRecords.incrementAndGet();
														break;
													}
												}
											}
										}
									}

									// isrc code
									if (scoredDocuments.isEmpty()) {
										for (String isrc : isrcCodes) {
											isrc = isrcNormalizer.normalize(isrc);
											if (!Strings.isNullOrEmpty(isrc)) {
												isrcQueries.incrementAndGet();
												final TermSet termSet = isrcIndex.search(isrc);
												final LongArray postings = termSet.next() ? termSet.getPostings() : null;
												if (null != postings && !postings.isEmpty()) {
													isrcPostings.addAndGet(postings.size());
													isrcQueriesWithResults.incrementAndGet();
													getScoredDocuments(scoredDocuments, documentStore, documentScore,
															normalizedTitle, normalizedArtists, postings, maxPostings, maxResults, isrcMinScore, autoScore);
													if (!scoredDocuments.isEmpty()) {
														matchType = MatchType.isrc;
														identifiedRecordsIsrc.incrementAndGet();
														identifiedRecords.incrementAndGet();
														break;
													}
												}
											}
										}
									}
								}
								
								// title_artists
								if (scoredDocuments.isEmpty()) {
									// title term(s)
									final Collection<String> titleTerms = titleTokenizer.clear()
											.tokenize(normalizedTitle.toString()).getTokens();
									if (!titleTerms.isEmpty()) {
										// artist term(s)
										artistTokenizer.clear();
										for (SplitCharSequence normalizedArtist : normalizedArtists) {
											artistTokenizer.tokenize(normalizedArtist.toString());
										}
										final Collection<String> artistTerms = artistTokenizer.getTokens();
										if (!artistTerms.isEmpty()) {
											titleArtistQueries.incrementAndGet();
											// search posting(s)
											final LongArray postings = titleArtistQuery
													.search(titleIndex, artistIndex, titleTerms, artistTerms);
											if (null != postings && !postings.isEmpty()) {
												titleArtistPostings.addAndGet(postings.size());
												titleArtistQueriesWithResults.incrementAndGet();
												getScoredDocuments(scoredDocuments, documentStore, documentScore,
														normalizedTitle, normalizedArtists, postings, maxPostings, maxResults, titleArtistMinScore, autoScore);
												if (!scoredDocuments.isEmpty()) {
													matchType = MatchType.title_artist;
													identifiedRecordsTitleArtist.incrementAndGet();
													identifiedRecords.incrementAndGet();
												}
											}
										}
									}
								}
								
								if (debug)
									logger.debug("scoredDocuments: {}", scoredDocuments.size());
								
								// format output record
								outputRecord.clear();
								if (echoParsedRows) {
									for (String value : inputRecord)
										outputRecord.add(value);
								}						
								for (int i = 0, size = resultFormats.size(); i < size; i ++) {
									resultPositions.get(i).apply(outputRecord, resultFormats.get(i)
											.format(titles, titleTypes, artists, artistTypes, codes, codeTypes,
													scoredDocuments, matchType));
								}

								// print output record
								synchronized (csvPrinter) {
									csvPrinter.printRecord(outputRecord);
								}

							}

						} catch (Exception e) {
							logger.error("run", e);
							threadException.compareAndSet(null, e);
						} finally {
							logger.debug("thread {} finished", runningThreads.getAndDecrement());
							synchronized (monitor) {
								monitor.notify();
							}
						}
					}
					
				});				
			}
			
			while (runningThreads.get() > 0) {
				synchronized (monitor) {
					monitor.wait(statsFrequency);
				}
				elapsedTimeMillis = System.currentTimeMillis() - beginTimeMillis;
				// print stats
				logger.debug("---------- stats ----------");
				logger.debug("processing time {}",
						TextUtils.formatDuration(elapsedTimeMillis));
				logger.debug("average line processing time {}ms", 
						Math.round((double) elapsedTimeMillis / (double) totalRecords.get()));
				logger.debug("processed {}", totalRecords);
				logger.debug("  invalid {} ({}%)", invalidRecords,
						Math.round(100.0 * invalidRecords.get() / (double) totalRecords.get()));
				logger.debug("  unidentified {} ({}%)", totalRecords.get() - invalidRecords.get() - identifiedRecords.get(),
						Math.round(100.0 * (totalRecords.get() - invalidRecords.get() - identifiedRecords.get()) / (double) totalRecords.get()));
				logger.debug("  identified {} ({}%)", identifiedRecords,
						Math.round(100.0 * identifiedRecords.get() / (double) totalRecords.get()));
				logger.debug("    work_code {} ({}%)", identifiedRecordsWorkCode,
						Math.round(100.0 * identifiedRecordsWorkCode.get() / (double) totalRecords.get()));
				logger.debug("    iswc {} ({}%)", identifiedRecordsIswc,
						Math.round(100.0 * identifiedRecordsIswc.get() / (double) totalRecords.get()));
				logger.debug("    isrc {} ({}%)", identifiedRecordsIsrc,
						Math.round(100.0 * identifiedRecordsIsrc.get() / (double) totalRecords.get()));
				logger.debug("    title_artist {} ({}%)", identifiedRecordsTitleArtist,
						Math.round(100.0 * identifiedRecordsTitleArtist.get() / (double) totalRecords.get()));
			}
			
			// re-throw thread exception
			if (null != threadException.get())
				throw threadException.get();
		}

		logger.debug("{} records processed", totalRecords);
		elapsedTimeMillis = System.currentTimeMillis() - beginTimeMillis;
		logger.debug("identification completed in {}",
				TextUtils.formatDuration(elapsedTimeMillis));

		// upload output file to s3
		logger.debug("uploading file {} to {}", localOutputFile, outputUrl);
		if (!s3.upload(new S3.Url(outputUrl), localOutputFile)) {
			logger.error("output file upload error: {}", outputUrl);
			throw new IOException(String
					.format("output file upload error: %s", outputUrl));
		}
		
		// format output json copying input items
		final long endTimeMillis = System.currentTimeMillis();
		final JsonObject outputItem = GsonUtils.deepCopy(inputItem);
		outputItem.addProperty("inputFileSize", localInputFile.length());
		outputItem.addProperty("outputFileSize", localOutputFile.length());
		outputItem.addProperty("totalRecords", totalRecords.get());
		outputItem.addProperty("identifiedRecords", identifiedRecords.get());
		outputItem.addProperty("identifiedRecordsWorkCode", identifiedRecordsWorkCode.get());
		outputItem.addProperty("identifiedRecordsIswc", identifiedRecordsIswc.get());
		outputItem.addProperty("identifiedRecordsIsrc", identifiedRecordsIsrc.get());
		outputItem.addProperty("identifiedRecordsTitleArtist", identifiedRecordsTitleArtist.get());
		outputItem.addProperty("unidentifiedRecords", totalRecords.get() - identifiedRecords.get());
		outputItem.addProperty("invalidRecords", invalidRecords.get());
		outputItem.addProperty("workCodeQueries", workCodeQueries.get());
		outputItem.addProperty("iswcQueries", iswcQueries.get());
		outputItem.addProperty("iswcQueriesWithResults", iswcQueriesWithResults.get());
		outputItem.addProperty("iswcPostings", iswcPostings.get());
		outputItem.addProperty("isrcQueries", isrcQueries.get());
		outputItem.addProperty("isrcQueriesWithResults", isrcQueriesWithResults.get());
		outputItem.addProperty("isrcPostings", isrcPostings.get());
		outputItem.addProperty("titleArtistQueries", titleArtistQueries.get());
		outputItem.addProperty("titleArtistQueriesWithResults", titleArtistQueriesWithResults.get());
		outputItem.addProperty("titleArtistPostings", titleArtistPostings.get());
		outputItem.addProperty("beginTimeMillis", beginTimeMillis);
		outputItem.addProperty("endTimeMillis", endTimeMillis);
		outputItem.addProperty("elapsedTimeMillis", endTimeMillis - beginTimeMillis);
		outputItem.addProperty("beginTime", DateFormat
				.getDateTimeInstance().format(new Date(beginTimeMillis)));
		outputItem.addProperty("endTime", DateFormat
				.getDateTimeInstance().format(new Date(endTimeMillis)));
		outputItem.addProperty("elapsedTime", TextUtils
				.formatDuration(endTimeMillis - beginTimeMillis));
		output.add("items", GsonUtils.newJsonArray(outputItem));
		logger.debug("output {}",  new GsonBuilder()
				.disableHtmlEscaping().setPrettyPrinting().create().toJson(output));
	
		// shutdown database(s)
		workDatabase.shutdown();
		collectingDatabase.shutdown();
		documentStore.shutdown();

		// delete temporary files
		localConfigFile.delete();
		localInputFile.delete();
		localOutputFile.delete();
				
		logger.debug("message processed in {}", TextUtils
				.formatDuration(System.currentTimeMillis() - beginTimeMillis));				
	}

	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.identification.processor.MessageProcessor#process(com.google.gson.JsonObject)
	 */
	@Override
	public boolean process(JsonObject message) {
		result = new JsonObject();
		error = null;
		try {
			processMessage(message, result);
			return true;
		} catch (Exception e) {
			result = null;
			error = SqsMessageHelper.formatError(e);
			return false;
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.identification.processor.MessageProcessor#getResultJson()
	 */
	@Override
	public JsonObject getResultJson() {
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.identification.processor.MessageProcessor#getErrorJson()
	 */
	@Override
	public JsonObject getErrorJson() {
		return error;
	}
	
}
