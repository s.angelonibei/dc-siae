package com.alkemytech.sophia.identification.score;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.score.TextScore;
import com.alkemytech.sophia.commons.util.SplitCharSequence;
import com.alkemytech.sophia.identification.document.ScoredDocument;
import com.alkemytech.sophia.identification.model.Document;
import com.alkemytech.sophia.identification.model.Maturato;
import com.alkemytech.sophia.identification.model.Text;
import com.alkemytech.sophia.identification.model.TextArtist;
import com.alkemytech.sophia.identification.model.TextType;
import com.alkemytech.sophia.identification.model.Work;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class GeometricDocumentScore implements DocumentScore {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final double weight;
	private final double exponent;
	private final TextScore titleScore;
	private final TextScore artistScore;

	@Inject
	public GeometricDocumentScore(@Named("configuration") Properties configuration,
			@Named("document_score") String propertyPrefix) {
		super();
		logger.debug("propertyPrefix: {}", propertyPrefix);
		weight = Double.parseDouble(configuration
				.getProperty(propertyPrefix + ".weight"));
		exponent = Double.parseDouble(configuration
				.getProperty(propertyPrefix + ".exponent"));
		titleScore = newScoreInstance(configuration
				.getProperty(propertyPrefix + ".title_score"));
		artistScore = newScoreInstance(configuration
				.getProperty(propertyPrefix + ".artist_score"));
	}
	
	private TextScore newScoreInstance(String className) {
		try {
			return (TextScore) Class.forName(className).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			throw new IllegalArgumentException(String
					.format("unknown class name \"%s\"", className), e);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.identification.score.DocumentScore#getScore(com.alkemytech.sophia.commons.util.SplitCharSequence, java.util.Collection, com.alkemytech.sophia.identification.model.Document)
	 */
	@Override
	public double getScore(SplitCharSequence title, Collection<SplitCharSequence> artists, Document document) {
		ScoredDocument score = this._getScore(title, artists, document);
		return score.score;
	}
	@Override
	public ScoredDocument getScore(SplitCharSequence title, Collection<SplitCharSequence> artists, Document document,Work work) {
		return this._getScore(title, artists, document);
	}

	public ScoredDocument _getScore(SplitCharSequence title, Collection<SplitCharSequence> artists, Document document) {
		double titleScore = 0.0;
		double artistScore = 0.0;
		String titleDocument = "";
		String artistDocument = "";
		List<String> ipiDocument = new ArrayList<String>();
		for (Text text : document.texts) {
			if (TextType.title == text.type) {
				if (titleScore < 1.0) {
					final double score = this.titleScore
							.getScore(title, new SplitCharSequence(text.text));
					if (score > titleScore) {
						titleScore = score;
						titleDocument = text.text;
					}
				}
			} else if (TextType.artist == text.type) {
				if(((TextArtist)text).ipi != null && ((TextArtist)text).ipi.length()>0)
					ipiDocument.add(((TextArtist)text).ipi);
				if (artistScore < 1.0) {
					for (SplitCharSequence artist : artists) {
						final double score = this.artistScore.getScore(artist, new SplitCharSequence(text.text));
						if (score > artistScore) {
							artistScore = score;
							artistDocument = text.text;
						}
					}
				}
			}
		}
		double score =  Math.pow(weight * titleScore * artistScore, exponent);
		return new ScoredDocument(document, score,titleDocument,artistDocument,ipiDocument);
	}

}
