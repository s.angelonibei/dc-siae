package com.alkemytech.sophia.identification;

import java.io.IOException;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.query.ExactTitleArtistQuery;
import com.alkemytech.sophia.commons.query.FuzzyTitleArtistQuery;
import com.alkemytech.sophia.commons.query.TitleArtistQuery;
import com.alkemytech.sophia.commons.sqs.McmdbMessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.MySqlUtils;
import com.alkemytech.sophia.commons.util.OsUtils;
import com.alkemytech.sophia.identification.collecting.CollectingDatabase;
import com.alkemytech.sophia.identification.document.DocumentStore;
import com.alkemytech.sophia.identification.guice.DocumentScoreModule;
import com.alkemytech.sophia.identification.guice.IndexModule;
import com.alkemytech.sophia.identification.guice.SearchEngineModule;
import com.alkemytech.sophia.identification.guice.TextProcessingModule;
import com.alkemytech.sophia.identification.jdbc.KbdbDataSource;
import com.alkemytech.sophia.identification.jdbc.McmdbDAO;
import com.alkemytech.sophia.identification.jdbc.McmdbDataSource;
import com.alkemytech.sophia.identification.processor.MessageProcessor;
import com.alkemytech.sophia.identification.result.ConfigurableResultFormat;
import com.alkemytech.sophia.identification.result.ResultFormats;
import com.alkemytech.sophia.identification.work.WorkDatabase;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class Identifier implements SqsMessagePump.Consumer {
	
	private static final Logger logger = LoggerFactory.getLogger(Identifier.class);
	
	private static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.toInstance(new S3(configuration));
			bind(SQS.class)
				.toInstance(new SQS(configuration));
			// document store
			bind(DocumentStore.class)
				.to(classForPrefix(DocumentStore.class,
						configuration.getProperty("document_store")))
				.in(Scopes.SINGLETON);
			// work database
			bind(WorkDatabase.class)
				.to(classForPrefix(WorkDatabase.class,
						configuration.getProperty("work_database")))
				.in(Scopes.SINGLETON);
			// collecting database
			bind(CollectingDatabase.class)
				.to(classForPrefix(CollectingDatabase.class,
						configuration.getProperty("collecting_database")))
				.in(Scopes.SINGLETON);
			// result format(s)
			ResultFormats.loopOnFormats(new ResultFormats.Observer() {				
				@Override
				public void notify(String formatName, Class<? extends ConfigurableResultFormat> formatClass) {
					bind(ConfigurableResultFormat.class)
						.annotatedWith(Names.named(formatName))
						.to(formatClass);
				}
			});
			bind(ResultFormats.class)
				.asEagerSingleton();
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("MCMDB"))
				.to(McmdbDataSource.class)
				.asEagerSingleton();
			// data access object(s)
			bind(McmdbDAO.class)
				.asEagerSingleton();
			bind(DataSource.class)
			.annotatedWith(Names.named("sophia_kb"))
			.to(KbdbDataSource.class)
			.asEagerSingleton();
			
			// other binding(s)
			bind(MessageDeduplicator.class)
				.to(McmdbMessageDeduplicator.class);
			// self
			bind(Identifier.class)
				.asEagerSingleton();
		}
		
		
		@Provides
		protected TitleArtistQuery provideTitleArtistQuery() {
			final String propertyPrefix = configuration
					.getProperty("title_artist_query");
			final String className = configuration
					.getProperty(propertyPrefix + ".class_name");
			if (ExactTitleArtistQuery.class.getName().equals(className)) {
				return new ExactTitleArtistQuery(configuration, propertyPrefix);
			} else if (FuzzyTitleArtistQuery.class.getName().equals(className)) {
				return new FuzzyTitleArtistQuery(configuration, propertyPrefix);
			}
			throw new IllegalArgumentException(String
					.format("unknown class name \"%s\"", className));
		}
		
	}
		
	public static void main(String[] args) {
		try {
			final Injector injector = Guice
					.createInjector(new GuiceModuleExtension(args,"/identifier.properties"),
							new SearchEngineModule(),
							new TextProcessingModule(),
							new DocumentScoreModule(),
							new IndexModule(args,"/identifier.properties")
							);
			final Identifier instance = injector
					.getInstance(Identifier.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			if (e.getMessage().contains("Address already in use"))
				logger.warn("gia' attivo");
			else
				logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Properties configuration;
	private final Gson gson;	
	private final S3 s3;
	private final SQS sqs;
	private final McmdbDAO dao;
	private final MessageDeduplicator messageDeduplicator;
	private final SqsMessagePump sqsMessagePump;
	private final Map<String, Provider<MessageProcessor>> identificationProviders;
	private JsonObject output;
	private JsonObject error;
	private long idSplitMessage;
	private long idSplitMessagePart;
	private boolean splitMessage;

	@Inject
	protected Identifier(@Named("configuration") Properties configuration,
			@Named("charset") Charset charset, Gson gson,
			McmdbDAO dao, S3 s3, SQS sqs,
			MessageDeduplicator messageDeduplicator,
			@Named("identification_providers") Map<String, Provider<MessageProcessor>> identificationProviders
			) throws IOException {
		super();
		this.configuration = configuration;
		this.gson = gson;
		this.dao = dao;
		this.s3 = s3;
		this.sqs = sqs;
		this.messageDeduplicator = messageDeduplicator;
		this.identificationProviders = identificationProviders;
		this.sqsMessagePump = new SqsMessagePump(sqs, configuration, "identifier.sqs",s3);
	}
	
	public Identifier startup() throws IOException {
		if ("true".equalsIgnoreCase(configuration
				.getProperty("identifier.locked", "true"))) {
			throw new IllegalStateException("application locked");
		}
		s3.startup();
		sqs.startup();
		return this;
	}

	public Identifier shutdown() throws IOException {
		sqs.shutdown();
		s3.shutdown();
		return this;
	}

	private Identifier process(String[] args) throws Exception {
		// bind lock tcp port
		final int bindPort = Integer.parseInt(configuration
				.getProperty("identifier.bind_port", configuration
						.getProperty("default.bind_port", "0")));
		try (final ServerSocket socket = new ServerSocket(bindPort)) {
			logger.debug("socket bound to {}", socket.getLocalSocketAddress());
			if (args.length > 1 && "emulate".equals(args[1])) {
				this.emulate();	
			}else{
				// poll sqs queue
				sqsMessagePump.pollingLoop(messageDeduplicator, this);
			}
		}
		return this;
	}
	private void emulate(){
		///FINE EMULAZIONE
		String emulate=configuration.getProperty("identifier.standalone_json");
		final JsonObject messageEmulateJson = GsonUtils.fromJson(emulate, JsonObject.class);
		this.consumeMessage(messageEmulateJson);
		///FINE EMULAZIONE
	}


	private void rdbmsAwareProcessMessageV1(JsonObject input) throws Exception {
		
		// parse input json message
		final JsonObject inputItem = GsonUtils
				.getAsJsonObject(GsonUtils
					.getAsJsonArray(GsonUtils
						.getAsJsonObject(input, "body"), "items"), 0);
		Long idSplitMessage = Long.parseLong(GsonUtils
				.getAsString(inputItem, "idSplitMessage", "-1"));
		Long idSplitMessagePart = Long.parseLong(GsonUtils.getAsString(inputItem, "idSplitMessagePart", "-1"));
		logger.debug("idSplitMessage: {}", idSplitMessage);
		logger.debug("idSplitMessagePart: {}", idSplitMessagePart);

		// config parameter(s)
		final boolean abortOnFailed = "true".equalsIgnoreCase(configuration
				.getProperty("identifier.multi.abort_on_failed", "true"));

		// count split message failed part(s)
		int failedParts = 0;
		if (abortOnFailed && splitMessage) {
			final Map<String,String> parameters = new HashMap<>();
			parameters.clear();
			parameters.put("idSplitMessage", idSplitMessage.toString());
			Map<String,String> singleRow = dao.executeSingleRowQuery("identifier.multi.sql.select_failed_parts", parameters);
			failedParts = Integer.parseInt(singleRow.get("failedParts"));
			logger.debug("failedParts: {}", failedParts);
		}
		
		this.dbOperation(idSplitMessagePart,idSplitMessage,input);
		
		// process single item message
		JsonObject resultJson = null;
		String failed = null;
		
		
		final MessageProcessor identificationV1 = identificationProviders.get(configuration.getProperty("identifier.identificationProvider","identification_v2")).get();
		
		if (0 == failedParts) {
			if (identificationV1.process(input)) {
				resultJson = identificationV1.getResultJson();
				failed = "0";
				output = resultJson;
			} else {
				resultJson = identificationV1.getErrorJson();
				failed = "1";
				error = resultJson;
			}			
		} else {
			resultJson = new JsonObject();
			resultJson.addProperty("message", String
					.format("identification skipped after %d failed part(s)", failedParts));
			resultJson.add("items", GsonUtils
					.deepCopy(GsonUtils
						.getAsJsonArray(GsonUtils
							.getAsJsonObject(input, "body"), "items")));
			failed = "0";
			output = resultJson;
		}

		// update database table(s)
		if (splitMessage) {

			// update split_message_part table
			final Map<String,String> parameters = new HashMap<>();
			parameters.put("resultJson", MySqlUtils.escapeJson(gson.toJson(resultJson)));
			parameters.put("failed", failed);
			parameters.put("idSplitMessagePart", idSplitMessagePart.toString());
			dao.executeUpdate("identifier.multi.sql.update_split_message_part", parameters);
							
			// handle multi message completion
			handleMultiMessageTermination(idSplitMessage);
			
		} else {
			
			// cleanup database
			if ("true".equalsIgnoreCase(configuration
					.getProperty("identifier.multi.cleanup_database"))) {
				// delete split message part table
				final Map<String,String> parameters = new HashMap<>();			
				parameters.put("idSplitMessage", idSplitMessage.toString());
				dao.executeUpdate("identifier.multi.sql.delete_split_message_part", parameters);
				// delete split message table
				dao.executeUpdate("identifier.multi.sql.delete_split_message", parameters);					
			}
			
			// update database tables
			else {					
				// update split_message_part table
				final Map<String,String> parameters = new HashMap<>();
				parameters.put("resultJson", MySqlUtils.escapeJson(gson.toJson(resultJson)));
				parameters.put("failed", failed);
				parameters.put("idSplitMessage", idSplitMessage.toString());
				parameters.put("idSplitMessagePart", idSplitMessagePart.toString());
				dao.executeUpdate("identifier.multi.sql.update_split_message_part", parameters);
				// update split_message table
				dao.executeUpdate("identifier.multi.sql.update_split_message", parameters);
			}
			
		}

	}

	@Override
	public JsonObject getStartedMessagePayload(JsonObject message) {
		// WARNING: first method invoked when a message is received; initialize state here
		output = null;
		error = null;
		final JsonObject item = GsonUtils
				.getAsJsonObject(GsonUtils
					.getAsJsonArray(GsonUtils
						.getAsJsonObject(message, "body"), "items"), 0);
		idSplitMessage = Long.parseLong(GsonUtils.getAsString(item, "idSplitMessage", "-1"));
		idSplitMessagePart = Long.parseLong(GsonUtils.getAsString(item, "idSplitMessagePart", "-1"));
		splitMessage = (-1 != idSplitMessage && -1 != idSplitMessagePart);
		return splitMessage ? null : SqsMessageHelper.formatContext();
	}

	@Override
	public boolean consumeMessage(JsonObject message) {
		
		// ALGORITHM
		//  1. if single item message
		//    1.1. process item right away
		//    1.2. if message contains db keys
		//      1.2.1. if all parts are completed
		//        1.2.1.1. send completed/failed message
		//    1.3. if message does not contain db keys
		//      1.3.1. send completed/failed message
		//  2. if multiple items message
		//    2.1. insert into split_message table
		//    2.2. foreach item
		//      2.2.1. insert into split_message_part table
		//      2.2.2. send single item to_process message with db keys

		// parse input json message
		final JsonObject messageBody = GsonUtils.getAsJsonObject(message, "body");
		final String version = GsonUtils.getAsString(messageBody, "version");
		final JsonArray messageItems = GsonUtils.getAsJsonArray(messageBody, "items");
		final String startedQueue = GsonUtils.getAsString(messageBody, "startedQueue");
		final String completedQueue = GsonUtils.getAsString(messageBody, "completedQueue");
		final String failedQueue = GsonUtils.getAsString(messageBody, "failedQueue");
		final boolean skipServiceBus = GsonUtils.getAsBoolean(messageBody, "skipServiceBus", true);

		// process message
		try {
			
			// send started notification (if message contains startedQueue)
			if (!Strings.isNullOrEmpty(startedQueue)) {
				sqsMessagePump.sendStartedMessage(startedQueue,
						message, SqsMessageHelper.formatContext(), skipServiceBus);
			}
			
			// check version
			logger.debug("version {}", version);
			if (!"1".equals(version)) {
				error = SqsMessageHelper.formatError("0", String.format("unsupported message version: %s", version));
				return false; // return false on error
			}
			
			// split multiple item(s) V1 message
			if (messageItems.size() > 1) {
				splitMultiMessageV1(message);
				output = null; // do not send completed message
				return true; // return true on success
			}
			
			// process single item V1 message
			rdbmsAwareProcessMessageV1(message);
			
		} catch (Exception e) {	
			
			logger.error("error consuming message", e);
			
			// error from exception
			error = SqsMessageHelper.formatError(e);
			output = null;

			// TODO implement split message recovery...
			if (splitMessage) {
				logger.warn("split message identification may never terminate");
			}

		}
		
		// message successfully processed
		if (null == error && null != output) {			
			// send completed notification (if message contains completedQueue)
			if (!Strings.isNullOrEmpty(completedQueue)) {
				sqsMessagePump.sendCompletedMessage(completedQueue,
						message, output, skipServiceBus);
			}
			return true; // return true on success
		}
		
		// message processing failed
		if (null == error) {
			error = SqsMessageHelper.formatError("0", "unespected internal error");
		}
		// send failed notification (if message contains failedQueue)
		if (!Strings.isNullOrEmpty(failedQueue)) {
			sqsMessagePump.sendFailedMessage(failedQueue,
					message, error, skipServiceBus);
		}
		return false; // return false on error
		
	}
	
	@Override
	public JsonObject getCompletedMessagePayload(JsonObject message) {
		return splitMessage ? null : output;
	}
	
	@Override
	public JsonObject getFailedMessagePayload(JsonObject message) {
		return splitMessage ? null : error;
	}

	
	
	
	
	private void dbOperation(Long idSplitMessagePart,Long idSplitMessage,JsonObject input)throws Exception  {
		// insert/update database table(s)
		this.dao.beginTransaction();
		try {
			if (splitMessage) {
				
				// update split_message_part table
				final Map<String,String> parameters = new HashMap<>();
				parameters.put("hostname", OsUtils.getHostname());
				parameters.put("pid", Integer.toString(OsUtils.getProcessId()));
				parameters.put("instanceId", configuration
						.getProperty("identifier.instance_id"));
				parameters.put("idSplitMessagePart", idSplitMessagePart.toString());
				int count = dao.executeUpdate("identifier.multi.sql.update_split_message_part.with_node_info", parameters);
				if (count < 1)
					throw new SQLException(String.format("zero row(s) updated for statement: %s",
							configuration.getProperty("identifier.multi.sql.update_split_message_part.with_node_info")));
				
			} else {
				
				// insert into split_message table
				final Map<String,String> parameters = new HashMap<>();			
				parameters.put("receivedMessage", MySqlUtils.escapeJson(gson.toJson(input)));
				int count = dao.executeUpdate("identifier.multi.sql.insert_split_message", parameters, true);
				if (count < 1)
					throw new SQLException(String.format("zero row(s) inserted for statement: %s",
							configuration.getProperty("identifier.multi.sql.insert_split_message")));
				idSplitMessage = dao.getLastGeneratedKey();
				logger.debug("idSplitMessage: {}", idSplitMessage);

				// insert into split_message_part table
				parameters.clear();			
				parameters.put("idSplitMessage", idSplitMessage.toString());
				parameters.put("hostname", OsUtils.getHostname());
				parameters.put("pid", Integer.toString(OsUtils.getProcessId()));
				parameters.put("instanceId", configuration.getProperty("identifier.instance_id"));
				count = dao.executeUpdate("identifier.multi.sql.insert_split_message_part.with_node_info", parameters, true);
				if (count < 1)
					throw new SQLException(String.format("zero row(s) inserted for statement: %s",configuration.getProperty("identifier.multi.sql.insert_split_message_part.with_node_info")));
				idSplitMessagePart = dao.getLastGeneratedKey();
				logger.debug("idSplitMessagePart: {}", idSplitMessagePart);

			}
			dao.commitTransaction();
		} catch (Exception e) {
			dao.rollbackTransaction();
			throw e;
		}	
	}
	
	private void splitMultiMessageV1(JsonObject multiMessage) throws SQLException {

		logger.debug("splitting multiple item(s) v1 message");

		// ALGORITHM
		//  1. insert into split_message table
		//  2. foreach item
		//    2.1. insert into split_message_part table
		//    2.2. send single item to_process message with custom notification queues & db keys

		// parse multiple items json message
		final JsonObject multiBody = GsonUtils.getAsJsonObject(multiMessage, "body");
		final JsonArray multiItems = GsonUtils.getAsJsonArray(multiBody, "items");
		
		// config parameter(s)
		final boolean hideSplitMessages = "true".equalsIgnoreCase(configuration
				.getProperty("identifier.multi.hide_split_messages", "true"));

		// single item to_process message(s)
		final List<JsonObject> bodies = new ArrayList<>(multiItems.size());
		
		// insert into database tables
		dao.beginTransaction();
		try {
			// insert into split_message table
			final Map<String,String> parameters = new HashMap<>();			
			parameters.put("receivedMessage", MySqlUtils.escapeJson(gson.toJson(multiMessage)));
			int count = dao.executeUpdate("identifier.multi.sql.insert_split_message", parameters, true);
			if (count < 1)
				throw new SQLException(String.format("zero row(s) inserted for statement: %s",
						configuration.getProperty("identifier.multi.sql.insert_split_message")));
			final Long idSplitMessage = dao.getLastGeneratedKey();

			// loop on multiple item(s)
			for (JsonElement multiItem : multiItems) {
			
				// insert into split_message_part table
				parameters.clear();			
				parameters.put("idSplitMessage", idSplitMessage.toString());
				count = dao.executeUpdate("identifier.multi.sql.insert_split_message_part", parameters, true);
				if (count < 1)
					throw new SQLException(String.format("zero row(s) inserted for statement: %s",
							configuration.getProperty("identifier.multi.sql.insert_split_message_part")));
				final Long idSplitMessagePart = dao.getLastGeneratedKey();

				// format single item to_process message copying original message body
				final JsonObject body = GsonUtils
						.deepCopy(multiBody);
				// remove original item(s)
				body.remove("items");
				// remove notification(s)
				body.remove("startedQueue");
				body.remove("completedQueue");
				body.remove("failedQueue");
				body.remove("skipServiceBus");
				// add single item
				final JsonObject item = GsonUtils
						.deepCopy(multiItem.getAsJsonObject());
				item.addProperty("idSplitMessage", idSplitMessage);
				item.addProperty("idSplitMessagePart", idSplitMessagePart);
				body.add("items", GsonUtils.newJsonArray(item));
				bodies.add(body);

			}
			
			dao.commitTransaction();
		} catch (Exception e) {
			dao.rollbackTransaction();
			throw e;
		}

		// send to_process message(s)
		for (JsonObject body : bodies) {
			sqsMessagePump.sendToProcessMessage(body, hideSplitMessages);
		}

		logger.debug("multiple item(s) message successfully split into {} parts", bodies.size());
		
	}

	private void handleMultiMessageTermination(Long idSplitMessage) throws Exception {
		
		logger.debug("handling split message {} termination", idSplitMessage);
		
		// ALGORITHM
		//  1. count pending parts				
		//  2. if no pending part exists
		//    2.1. update split_message table
		//    2.2. if update is successful
		//      2.2.1. select from split_message
		//      2.2.2. select from split_message_part
		//      2.2.3. if any one part is failed
		//        2.2.3.1. send failed message
		//      2.2.4. if no part is failed
		//        2.2.4.1. send completed message with all items
				
		// select pending part(s)
		final Map<String,String> parameters = new HashMap<>();			
		parameters.put("idSplitMessage", idSplitMessage.toString());
		Map<String,String> singleRow = dao
				.executeSingleRowQuery("identifier.multi.sql.select_pending_parts", parameters);
		final int pendingParts = Integer.parseInt(singleRow.get("pendingParts"));
		logger.debug("pendingParts: {}", pendingParts);

		// if no pending part exists
		if (0 == pendingParts) {

			// update split_message table
			int count = dao.executeUpdate("identifier.multi.sql.update_split_message", parameters);
			
			// if update is successful
			if (count >= 1) {
				
				// select from split_message
				singleRow = dao.executeSingleRowQuery("identifier.multi.sql.select_split_message", parameters);
				final JsonObject originalMessage = gson
						.fromJson(singleRow.get("receivedMessage"), JsonObject.class);
				
				// select from split_message_part
				final List<Map<String,String>> rows = dao
						.executeQuery("identifier.multi.sql.select_split_message_part", parameters);
				
				// loop on parts
				JsonObject error = null;
				final JsonArray outputItems = new JsonArray();
				for (Map<String,String> row : rows) {
					// WARNING: result json contains either output or error object, not a full message
					final JsonObject resultJson = gson
							.fromJson(row.get("resultJson"), JsonObject.class);
					if ("1".equals(row.get("failed"))) {
						error = resultJson;
						break;
					}
					final JsonObject item = GsonUtils
							.getAsJsonObject(GsonUtils
								.getAsJsonArray(resultJson, "items"), 0);
					if (null == item) {
						error = SqsMessageHelper
								.formatError("0", "invalid completed message output element");
						break;
					}
					outputItems.add(item);
				}

				// if any one part is failed
				if (null != error) {

					// send failed message
					sqsMessagePump.sendFailedMessage(originalMessage, error, false);
					
					// send failed notification (if message contains failedQueue)
					final JsonObject messageBody = GsonUtils
							.getAsJsonObject(originalMessage, "body");
					final String failedQueue = GsonUtils
							.getAsString(messageBody, "failedQueue");
					if (!Strings.isNullOrEmpty(failedQueue)) {
						final boolean skipServiceBus = GsonUtils
								.getAsBoolean(messageBody, "skipServiceBus", true);
						sqsMessagePump.sendFailedMessage(failedQueue,
								originalMessage, error, skipServiceBus);
					}

				}

				// if no part is failed
				else {
					
					// send completed message with all item(s)
					final JsonObject output = new JsonObject();
					output.add("items", outputItems);
					sqsMessagePump.sendCompletedMessage(originalMessage, output, false);
					
					// send completed notification (if message contains completedQueue)
					final JsonObject messageBody = GsonUtils
							.getAsJsonObject(originalMessage, "body");
					final String completedQueue = GsonUtils
							.getAsString(messageBody, "completedQueue");
					if (!Strings.isNullOrEmpty(completedQueue)) {
						final boolean skipServiceBus = GsonUtils
								.getAsBoolean(messageBody, "skipServiceBus", true);
						sqsMessagePump.sendCompletedMessage(completedQueue,
								output, output, skipServiceBus);
					}

				}

				// cleanup database
				if ("true".equalsIgnoreCase(configuration
						.getProperty("identifier.multi.cleanup_database"))) {
					dao.executeUpdate("identifier.multi.sql.delete_split_message_part", parameters);
					dao.executeUpdate("identifier.multi.sql.delete_split_message", parameters);					
				}
				
			}

		}
		
	}
	
}
