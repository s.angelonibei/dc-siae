package com.alkemytech.sophia.identification.result;

import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.score.TextScore;
import com.alkemytech.sophia.commons.text.TextNormalizer;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.SplitCharSequence;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.identification.MatchType;
import com.alkemytech.sophia.identification.document.ScoredDocument;
import com.alkemytech.sophia.identification.model.ArtistRole;
import com.alkemytech.sophia.identification.model.Attribute;
import com.alkemytech.sophia.identification.model.AttributeKey;
import com.alkemytech.sophia.identification.model.Category;
import com.alkemytech.sophia.identification.model.CodeType;
import com.alkemytech.sophia.identification.model.Origin;
import com.alkemytech.sophia.identification.model.Text;
import com.alkemytech.sophia.identification.model.TitleType;
import com.alkemytech.sophia.identification.model.Work;
import com.alkemytech.sophia.identification.work.WorkDatabase;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class JsonWithScoresFormat implements ConfigurableResultFormat {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private static final class ScoredText {

        public static final ScoredText empty = new ScoredText(0.0, "");

        public final double score;
        public final String text;

        public ScoredText(double score, String text) {
            super();
            this.score = score;
            this.text = text;
        }

    }

    private final Gson gson;
    private final TextNormalizer titleNormalizer;
    private final TextNormalizer artistNormalizer;
    private final WorkDatabase workDatabase;

    @Inject
    public JsonWithScoresFormat(Gson gson,
                                @Named("title_normalizer") TextNormalizer titleNormalizer,
                                @Named("artist_normalizer") TextNormalizer artistNormalizer,
                                WorkDatabase workDatabase) {
        this.gson = gson;
        this.titleNormalizer = titleNormalizer;
        this.artistNormalizer = artistNormalizer;
        this.workDatabase = workDatabase;
    }

    private TextScore newScoreInstance(String className) {
        try {
            return (TextScore) Class.forName(className).newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            throw new IllegalArgumentException(String
                    .format("unknown class name \"%s\"", className), e);
        }
    }

    private String getFieldLabel(String field, Map<String, String> fieldsMapping) {
        return null == fieldsMapping ? field : fieldsMapping.get(field);
    }

    private ScoredText getBestScoredText(TextScore textScore, TextNormalizer textNormalizer, List<String> workTexts, List<String> recordTexts) {
        if (null == workTexts || workTexts.isEmpty())
            return ScoredText.empty;
        if (null == recordTexts || recordTexts.isEmpty())
            return ScoredText.empty;
        double bestScore = 0.0;
        String bestText = "";
        for (String workText : workTexts) {
            final String normalizedWorkText = textNormalizer.normalize(workText);
            if (Strings.isNullOrEmpty(normalizedWorkText))
                continue;
            final SplitCharSequence left = new SplitCharSequence(normalizedWorkText);
            for (String recordText : recordTexts) {
                recordText = textNormalizer.normalize(recordText);
                if (Strings.isNullOrEmpty(recordText))
                    continue;
                final double score = textScore
                        .getScore(left, new SplitCharSequence(recordText));
                if (score > bestScore) {
                    if (score >= 1.0)
                        return new ScoredText(score, workText);
                    bestScore = score;
                    bestText = workText;
                }
            }
        }
        return new ScoredText(bestScore, bestText);
    }

    /*
     * (non-Javadoc)
     * @see com.alkemytech.sophia.identification.result.ConfigurableResultFormat#getInstance(java.util.Properties, java.lang.String)
     */
    @Override
    public ResultFormat getInstance(Properties configuration, String propertyPrefix) {

//		[
//		   {
//	          "work_code":"11872371100",
//	          "score":"0.950",
//            "title":"BENNY HILL SHOW",
//	          "title_score":"0.550",
//		      "author":"",
//	          "author_score":"0.0",
//		      "composer":"BERLIN IRVING",
//	          "composer_score":"0.750",
//	          "performer":"",
//		      "performer_score":"0.0",
//		   }
//		]
    	final String document_score = configuration.getProperty("document_score", "custom_score");

        final Map<String, String> mapping = GsonUtils
                .decodeJsonMap(configuration
                        .getProperty(propertyPrefix + ".mapping"));
        final TextScore titleScore = newScoreInstance(configuration
                .getProperty(propertyPrefix + ".title_score"));
        final TextScore artistScore = newScoreInstance(configuration
                .getProperty(propertyPrefix + ".artist_score"));
        final double scoreScale = Double.parseDouble(configuration
                .getProperty(propertyPrefix + ".score_scale", "1"));
        final String scoreStringFormat = configuration
                .getProperty(propertyPrefix + ".score_string_format", "%.3f");
        final String labelWorkCode = getFieldLabel("work_code", mapping);
        final String labelTitle = getFieldLabel("title", mapping);
        final String labelAuthor = getFieldLabel("author", mapping);
        final String labelComposer = getFieldLabel("composer", mapping);
        final String labelPerformer = getFieldLabel("performer", mapping);
        final String labelScore = getFieldLabel("score", mapping);
        final String labelTitleScore = getFieldLabel("title_score", mapping);
        final String labelAuthorScore = getFieldLabel("author_score", mapping);
        final String labelComposerScore = getFieldLabel("composer_score", mapping);
        final String labelPerformerScore = getFieldLabel("performer_score", mapping);
        final String labelMatchType = getFieldLabel("match_type", mapping);
        final String labelMaturato = getFieldLabel("maturato", mapping);
        final String labelRischio = getFieldLabel("rischio", mapping);
        final String labelFlagPD = getFieldLabel("flagPD", mapping);
        final String labelFlagEL = getFieldLabel("flagEL", mapping);
        final String labelTitoloOriginale = getFieldLabel("titoloOriginale", mapping);
        final String labelStatoOpera = getFieldLabel("statoOpera", mapping);
        final String labelFlagIrregolare = getFieldLabel("flagIrregolare", mapping);
        final String labelFlagDoppioDeposito = getFieldLabel("flagDoppioDeposito", mapping);
        final int riskPeriod =      TextUtils.parseIntSize(configuration.getProperty(document_score + ".risk_period", "0"));
        final int relevancePeriod = TextUtils.parseIntSize(configuration.getProperty(document_score + ".relevance_period", "0"));


        // log config value(s)
        final boolean debug = "true".equalsIgnoreCase(configuration
                .getProperty("identifier.debug", configuration
                        .getProperty("default.debug")));
        if (debug) {
            logger.debug("propertyPrefix: {}", propertyPrefix);
            logger.debug("\tmapping: {}", mapping);
            logger.debug("\ttitleScore: {}", titleScore);
            logger.debug("\tartistScore: {}", artistScore);
            logger.debug("\tescoreScale: {}", scoreScale);
            logger.debug("\tscoreStringFormat: {}", scoreStringFormat);
            logger.debug("\tlabelWorkCode: {}", labelWorkCode);
            logger.debug("\tlabelTitle: {}", labelTitle);
            logger.debug("\tlabelAuthor: {}", labelAuthor);
            logger.debug("\tlabelComposer: {}", labelComposer);
            logger.debug("\tlabelPerformer: {}", labelPerformer);
            logger.debug("\tlabelScore: {}", labelScore);
            logger.debug("\tlabelTitleScore: {}", labelTitleScore);
            logger.debug("\tlabelAuthorScore: {}", labelAuthorScore);
            logger.debug("\tlabelComposerScore: {}", labelComposerScore);
            logger.debug("\tlabelPerformerScore: {}", labelPerformerScore);
            logger.debug("\tlabelMatchType: {}", labelMatchType);
        }

        return new ResultFormat() {
			@Override
			public String format(List<Text> text, List<ScoredDocument> scoredDocuments, MatchType matchType)
					throws Exception {
				// TODO Auto-generated method stub
				return "";
			}

            /*
             * (non-Javadoc)
             * @see com.alkemytech.sophia.identification.result.ResultFormat#format(java.util.List, java.util.List, java.util.List, java.util.List, java.util.List, java.util.List, java.util.List, com.alkemytech.sophia.identification.MatchType)
             */
            @Override
            public String format(List<String> titles, List<String> titleTypes, List<String> artists, List<String> artistTypes, List<String> codes, List<String> codeTypes, List<ScoredDocument> scoredDocuments, MatchType matchType) throws Exception {
                final JsonArray jsonArray = new JsonArray();
                for (ScoredDocument scoredDocument : scoredDocuments) {
                    final Work work = workDatabase
                            .getById(scoredDocument.document.workId);
                    if (null != work) {
                        final String workCode = work.getFirstCode(CodeType.siae);
                        if (!Strings.isNullOrEmpty(workCode)) {
                            final JsonObject jsonObject = new JsonObject();
                            // work_code
                            if (null != labelWorkCode)
                                jsonObject.addProperty(labelWorkCode, workCode);
                            // score
                            if (null != labelScore)
                                jsonObject.addProperty(labelScore, String
                                        .format(scoreStringFormat, scoredDocument.score * scoreScale));


                            // title & title_score
                            final ScoredText bestTitle = getBestScoredText(titleScore, titleNormalizer, work.getTitles(TitleType.song), titles);


                            if (null != labelTitle)
                                jsonObject.addProperty(labelTitle, bestTitle.text);
                            if (null != labelTitleScore)
                                jsonObject.addProperty(labelTitleScore,
                                        String.format(scoreStringFormat, bestTitle.score * scoreScale));

                            // titoloOriginale
                            if (work.getTitles(TitleType.song, Category.original) != null) {
                                if (null != labelTitoloOriginale)
                                    jsonObject.addProperty(labelTitoloOriginale, work.getTitles(TitleType.song, Category.original).get(0));
                            }


                            // author & author_score
                            final ScoredText bestAuthor = getBestScoredText(artistScore,
                                    artistNormalizer, work.getArtists(ArtistRole.author), artists);
                            if (null != labelAuthor)
                                jsonObject.addProperty(labelAuthor, bestAuthor.text);
                            if (null != labelAuthorScore)
                                jsonObject.addProperty(labelAuthorScore,
                                        String.format(scoreStringFormat, bestAuthor.score * scoreScale));

                            // composer & composer_score
                            final ScoredText bestComposer = getBestScoredText(artistScore,
                                    artistNormalizer, work.getArtists(ArtistRole.composer), artists);
                            if (null != labelComposer)
                                jsonObject.addProperty(labelComposer, bestComposer.text);
                            if (null != labelComposerScore)
                                jsonObject.addProperty(labelComposerScore,
                                        String.format(scoreStringFormat, bestComposer.score * scoreScale));

                            // performer & performer_score
                            final ScoredText bestPerformer = getBestScoredText(artistScore,
                                    artistNormalizer, work.getArtists(ArtistRole.performer), artists);
                            if (null != labelPerformer)
                                jsonObject.addProperty(labelPerformer, bestPerformer.text);
                            if (null != labelPerformerScore)
                                jsonObject.addProperty(labelPerformerScore,
                                        String.format(scoreStringFormat, bestPerformer.score * scoreScale));
                            // match type
                            if (null != labelMatchType)
                                jsonObject.addProperty(labelMatchType, matchType.toString());

                            // maturato
                            if (null != labelMaturato)
                                jsonObject.addProperty(labelMaturato, work.getMaturato(relevancePeriod));
                            // rischio
                            if (null != labelRischio)
                                jsonObject.addProperty(labelRischio, work.getRischisita(riskPeriod));

                            // flagPD
                            if (null != labelFlagPD)
                                jsonObject.addProperty(labelFlagPD, work.getAttribute(AttributeKey.flag_pd, Origin.siae));

                            // flagEL
                            if (null != labelFlagEL)
                                jsonObject.addProperty(labelFlagEL, work.getAttribute(AttributeKey.flag_el, Origin.siae));
                            // flagIrregolare
                            if (null != labelFlagIrregolare)
                                jsonObject.addProperty(labelFlagIrregolare, work.getAttribute(AttributeKey.irregularity, Origin.siae));
                            // flagDoppioDeposito
                            if (null != labelFlagDoppioDeposito)
                                jsonObject.addProperty(labelFlagDoppioDeposito, (String.valueOf(scoredDocument.flagDoppioDeposito)));
                            //jsonObject.addProperty(labelFlagDoppioDeposito, work.getAttribute(ScoredDocument.flagDoppioDeposito,Origin.siae));
                            // status
                            if (null != labelStatoOpera)
                                jsonObject.addProperty(labelStatoOpera, work.getAttribute(AttributeKey.status, Origin.siae));

                            jsonArray.add(jsonObject);
                        }
                    }
                }
                return gson.toJson(jsonArray);
            }

        };

    }

}
