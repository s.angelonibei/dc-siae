package com.alkemytech.sophia.identification.result;

import java.util.Properties;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface ConfigurableResultFormat {
	
	public ResultFormat getInstance(Properties configuration, String propertyPrefix);
	
}
