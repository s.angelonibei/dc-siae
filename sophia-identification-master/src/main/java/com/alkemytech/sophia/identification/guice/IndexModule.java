package com.alkemytech.sophia.identification.guice;

import static java.lang.Class.forName;

import java.util.Properties;

import com.alkemytech.sophia.commons.guice.ConfigurationLoader;
import com.alkemytech.sophia.commons.index.ReverseIndex;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class IndexModule extends AbstractModule {
	private final String[] args;
	private final String resourceName;
	
	protected Properties configuration;
	
	public IndexModule(String[] args) {
		super();
		this.args = args;
		this.resourceName = "/configuration.properties";
	}

	public IndexModule(String[] args, String resourceName) {
		super();
		this.args = args;
		this.resourceName = resourceName;
	}

	@Override
	protected void configure() {
		// default properties
		final Properties defaults = new ConfigurationLoader()
				.withResourceName("/defaults.properties").load();
		// configuration properties
		configuration = null != args && args.length > 0 ?
				new ConfigurationLoader()
					.withCommandLineArgs(args)
						.load(defaults) :
				new ConfigurationLoader()
					.withResourceName(resourceName)
						.load(defaults);
		ConfigurationLoader.expandVariables(configuration);
		if ("true".equals(configuration.getProperty("default.debug"))) {
			configuration.list(System.out);
		}
		
		bind(ReverseIndex.class)
			.annotatedWith(Names.named("uuid_index"))
			.to(classForPrefix(ReverseIndex.class, "uuid_index"))
			.in(Scopes.SINGLETON);
		bind(ReverseIndex.class)
			.annotatedWith(Names.named("iswc_index"))
			.to(classForPrefix(ReverseIndex.class, "iswc_index"))
			.in(Scopes.SINGLETON);
		bind(ReverseIndex.class)
			.annotatedWith(Names.named("isrc_index"))
			.to(classForPrefix(ReverseIndex.class, "isrc_index"))
			.in(Scopes.SINGLETON);
		bind(ReverseIndex.class)
			.annotatedWith(Names.named("title_index"))
			.to(classForPrefix(ReverseIndex.class, "title_index"))
			.in(Scopes.SINGLETON);
		bind(ReverseIndex.class)
			.annotatedWith(Names.named("artist_index"))
			.to(classForPrefix(ReverseIndex.class, "artist_index"))
			.in(Scopes.SINGLETON);
		bind(ReverseIndex.class)
			.annotatedWith(Names.named("siae_index"))
			.to(classForPrefix(ReverseIndex.class, "siae_index"))
			.in(Scopes.SINGLETON);
		bind(ReverseIndex.class)
			.annotatedWith(Names.named("ada_index"))
			.to(classForPrefix(ReverseIndex.class, "ada_index"))
			.in(Scopes.SINGLETON);

	}
	
	@SuppressWarnings("unchecked")
	private <C extends B, B> C classForPrefix(B baseClass, String propertyPrefix) {
		try {
			return (C) forName(configuration.getProperty(propertyPrefix + ".class_name"));
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
}
