package com.alkemytech.sophia.identification;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.collecting.SiaeCode;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.index.ReverseIndex;
import com.alkemytech.sophia.commons.spelling.NumberSpeller;
import com.alkemytech.sophia.commons.sqs.McmdbMessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.text.RegExTextNormalizer;
import com.alkemytech.sophia.commons.text.RegExTextTokenizer;
import com.alkemytech.sophia.commons.text.TextNormalizer;
import com.alkemytech.sophia.commons.text.TextTokenizer;
import com.alkemytech.sophia.commons.util.BigDecimals;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.identification.collecting.CollectingDatabase;
import com.alkemytech.sophia.identification.document.DocumentStore;
import com.alkemytech.sophia.identification.index.ReverseIndexMetadata;
import com.alkemytech.sophia.identification.jdbc.McmdbDataSource;
import com.alkemytech.sophia.identification.model.Artist;
import com.alkemytech.sophia.identification.model.ArtistRole;
import com.alkemytech.sophia.identification.model.Attribute;
import com.alkemytech.sophia.identification.model.AttributeKey;
import com.alkemytech.sophia.identification.model.Category;
import com.alkemytech.sophia.identification.model.Code;
import com.alkemytech.sophia.identification.model.CodeType;
import com.alkemytech.sophia.identification.model.CollectingCode;
import com.alkemytech.sophia.identification.model.Document;
import com.alkemytech.sophia.identification.model.Maturato;
import com.alkemytech.sophia.identification.model.Origin;
import com.alkemytech.sophia.identification.model.Text;
import com.alkemytech.sophia.identification.model.TextArtist;
import com.alkemytech.sophia.identification.model.TextType;
import com.alkemytech.sophia.identification.model.Title;
import com.alkemytech.sophia.identification.model.TitleType;
import com.alkemytech.sophia.identification.model.Work;
import com.alkemytech.sophia.identification.work.WorkDatabase;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class IndexUpdater2 {

	private static final Logger logger = LoggerFactory.getLogger(IndexUpdater.class);

	private static class GuiceModuleExtension extends GuiceModule {

		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.toInstance(new S3(configuration));
			bind(SQS.class)
				.toInstance(new SQS(configuration));
			// reverse index(es)
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("iswc_index"))
				.to(classForPrefix(ReverseIndex.class, "iswc_index"))
				.asEagerSingleton();
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("isrc_index"))
				.to(classForPrefix(ReverseIndex.class, "isrc_index"))
				.asEagerSingleton();
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("title_index"))
				.to(classForPrefix(ReverseIndex.class, "title_index"))
				.asEagerSingleton();
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("artist_index"))
				.to(classForPrefix(ReverseIndex.class, "artist_index"))
				.asEagerSingleton();
			// work database
			bind(WorkDatabase.class)
				.to(classForPrefix(WorkDatabase.class,
						configuration.getProperty("work_database")))
				.asEagerSingleton();
			// collecting database
			bind(CollectingDatabase.class)
				.to(classForPrefix(CollectingDatabase.class,
						configuration.getProperty("collecting_database")))
				.asEagerSingleton();
			// document store
			bind(DocumentStore.class)
				.to(classForPrefix(DocumentStore.class,
						configuration.getProperty("document_store")))
				.asEagerSingleton();
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("MCMDB"))
				.to(McmdbDataSource.class)
				.asEagerSingleton();
			// other binding(s)
			bind(MessageDeduplicator.class)
				.to(McmdbMessageDeduplicator.class);
			// self
			bind(IndexUpdater.class)
				.asEagerSingleton();
		}

		@Provides
		@Named("iswc_normalizer")
		protected TextNormalizer provideIswcNormalizer() {
			return new RegExTextNormalizer(configuration, "iswc_normalizer");
		}

		@Provides
		@Named("isrc_normalizer")
		protected TextNormalizer provideIsrcNormalizer() {
			return new RegExTextNormalizer(configuration, "isrc_normalizer");
		}

		@Provides
		@Named("title_normalizer")
		protected TextNormalizer provideTitleNormalizer() {
			return new RegExTextNormalizer(configuration, "title_normalizer");
		}

		@Provides
		@Named("artist_normalizer")
		protected TextNormalizer provideArtistNormalizer() {
			return new RegExTextNormalizer(configuration, "artist_normalizer");
		}

		@Provides
		@Named("title_tokenizer")
		protected TextTokenizer provideTitleTokenizer() {
			return new RegExTextTokenizer(configuration, "title_tokenizer");
		}

		@Provides
		@Named("artist_tokenizer")
		protected TextTokenizer provideArtistTokenizer() {
			return new RegExTextTokenizer(configuration, "artist_tokenizer");
		}

	}

	public static void main(String[] args) {
		try {
			final IndexUpdater2 instance =
					Guice
					.createInjector(new GuiceModuleExtension(args,
							"/index-updater.properties"))
					.getInstance(IndexUpdater2.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Properties configuration;
	private final Charset charset;
	private final S3 s3;
	private final SQS sqs;
	private final Gson gson;
	private final WorkDatabase workDatabase;
	private final CollectingDatabase collectingDatabase;
	private final DocumentStore documentStore;
	private final ReverseIndex iswcIndex;
	private final ReverseIndex isrcIndex;
	private final ReverseIndex titleIndex;
	private final ReverseIndex artistIndex;
	private final TextNormalizer iswcNormalizer;
	private final TextNormalizer isrcNormalizer;
	private final TextNormalizer titleNormalizer;
	private final TextNormalizer artistNormalizer;
	private final TextTokenizer titleTokenizer;
	private final TextTokenizer artistTokenizer;
	private final MessageDeduplicator messageDeduplicator;

	@Inject
	protected IndexUpdater2(@Named("configuration") Properties configuration,
			@Named("charset") Charset charset, S3 s3, SQS sqs, Gson gson,
			WorkDatabase workDatabase,
			CollectingDatabase collectingDatabase,
			DocumentStore documentStore,
			@Named("iswc_index") ReverseIndex iswcIndex,
			@Named("isrc_index") ReverseIndex isrcIndex,
			@Named("title_index") ReverseIndex titleIndex,
			@Named("artist_index") ReverseIndex artistIndex,
			@Named("iswc_normalizer") TextNormalizer iswcNormalizer,
			@Named("isrc_normalizer") TextNormalizer isrcNormalizer,
			@Named("title_normalizer") TextNormalizer titleNormalizer,
			@Named("artist_normalizer") TextNormalizer artistNormalizer,
			@Named("title_tokenizer") TextTokenizer titleTokenizer,
			@Named("artist_tokenizer") TextTokenizer artistTokenizer,
			MessageDeduplicator messageDeduplicator) {
		super();
		this.configuration = configuration;
		this.charset = charset;
		this.s3 = s3;
		this.sqs = sqs;
		this.gson = gson;
		this.workDatabase = workDatabase;
		this.collectingDatabase = collectingDatabase;
		this.documentStore = documentStore;
		this.iswcIndex = iswcIndex;
		this.isrcIndex = isrcIndex;
		this.titleIndex = titleIndex;
		this.artistIndex = artistIndex;
		this.iswcNormalizer = iswcNormalizer;
		this.isrcNormalizer = isrcNormalizer;
		this.titleNormalizer = titleNormalizer;
		this.artistNormalizer = artistNormalizer;
		this.titleTokenizer = titleTokenizer;
		this.artistTokenizer = artistTokenizer;
		this.messageDeduplicator = messageDeduplicator;
	}

	public IndexUpdater2 startup() throws SQLException, IOException {
		if ("true".equalsIgnoreCase(configuration
				.getProperty("index_updater.locked", "true"))) {
			throw new IllegalStateException("application locked");
		}
		s3.startup();
		sqs.startup();
		workDatabase.startup();
		collectingDatabase.startup();
		documentStore.startup();
		return this;
	}

	public IndexUpdater2 shutdown() {
		documentStore.shutdown();
		collectingDatabase.shutdown();
		workDatabase.shutdown();
		sqs.shutdown();
		s3.shutdown();
		return this;
	}

//	private String readResumeFile(File file) throws IOException {
//		int length = (int) file.length();
//		if (length > 0) {
//			try (final FileInputStream in = new FileInputStream(file)) {
//				final byte[] buffer = new byte[length];
//				for (length = 0; length < buffer.length; ) {
//					final int count = in.read(buffer, length, buffer.length - length);
//					if (-1 == count) {
//						break;
//					}
//					length += count;
//				}
//				return new String(buffer, 0, length, "UTF-8");
//			}			
//		}
//		return null;
//	}

	private void writeResumeFile(File file, String text) throws IOException {
		try (final FileOutputStream out = new FileOutputStream(file)) {
			out.write(text.getBytes("UTF-8"));
		}
	}

	private void deleteS3FolderContents(String s3FolderUrl) {
		if (!s3FolderUrl.endsWith("/"))
			s3FolderUrl += '/';
		final List<S3ObjectSummary> objects = s3
				.listObjects(new S3.Url(s3FolderUrl));
		for (S3ObjectSummary object : objects) {
			final String s3FileUrl = new StringBuilder()
					.append("s3://")
					.append(object.getBucketName())
					.append('/')
					.append(object.getKey())
					.toString();
			if (s3.delete(new S3.Url(s3FileUrl)))
				logger.debug("file deleted {}", s3FileUrl);
			else
				logger.error("error deleting file {}", s3FileUrl);
		}
	}

	private HashSet<Text> normalizeWork(Work work, Set<Integer> includeOrigins) {
		final HashSet<Text> texts = new HashSet<Text>();
		// title(s)
		int titles = 0;
		if (null != work.titles) {
			for (Title title : work.titles) {
				if (includeOrigins.contains(title.origin)) {
					final String text = titleNormalizer
							.normalize(title.text);
					if (!Strings.isNullOrEmpty(text)) {
						texts.add(new Text(text, TextType.title));
						titles ++;
					}
				}
			}
		}
		// artist(s)
		int artists = 0;
		if (null != work.artists) {
			for (Artist artist : work.artists) {
				if (includeOrigins.contains(artist.origin)) {
					final String text = artistNormalizer
							.normalize(artist.text);
					if (!Strings.isNullOrEmpty(text)) {
						texts.add(new TextArtist(text, artist.ipiNumber, TextType.artist));
						artists ++;
					}

				}
			}
		}
		// code(s)
		int codes = 0;
		if (null != work.codes) {
			for (Code code : work.codes) {
				if (includeOrigins.contains(code.origin)) {
					if (CodeType.iswc == code.type) { // iswc(s)
						final String text = iswcNormalizer
								.normalize(code.text);
						if (!Strings.isNullOrEmpty(text)) {
							texts.add(new Text(text, TextType.iswc));
							codes ++;
						}
					} else if (CodeType.isrc == code.type) { // isrc(s)
						final String text = isrcNormalizer
								.normalize(code.text);
						if (!Strings.isNullOrEmpty(text)) {
							texts.add(new Text(text, TextType.isrc));
							codes ++;
						}
					}
				}
			}
		}
		if (0 == titles || 0 == artists) {
			logger.debug("invalid work {} titles, {} artists, {} codes", titles, artists, codes);
			logger.debug("  {}", work);
			return null;
		}
		return texts;
	}

//	private List<String> getUnprocessedFileUrls(String lastProcessed) throws IOException {
//
//		final String s3FolderUrl = configuration
//				.getProperty("index_updater.input.s3_folder_url");
//		final Pattern filterRegex = Pattern.compile(configuration
//				.getProperty("index_updater.input.filter_regex"));
//		final Pattern isFullRegex = Pattern.compile(configuration
//				.getProperty("index_updater.input.is_full_regex"));
//
//		// build s3 file(s) list
//		final List<S3ObjectSummary> objects = s3.listObjects(new S3.Url(s3FolderUrl));
//		final List<String> s3FileUrls = new ArrayList<>(objects.size());
//		for (S3ObjectSummary object : objects) {
//			final String s3FileUrl = new StringBuilder()
//					.append("s3://")
//					.append(object.getBucketName())
//					.append('/')
//					.append(object.getKey())
//					.toString();
//			if (filterRegex.matcher(s3FileUrl).matches())
//				s3FileUrls.add(s3FileUrl);
//		}
//		
//		// sort lexicographically
//		Collections.sort(s3FileUrls);
//		
//		// find first file of last full export
//		int fullIndex = 0;
//		boolean previousWasDelta = true;
//		boolean fullExportFound = false;
//		for (int index = 0; index < s3FileUrls.size(); index ++) {
//			final String s3FileUrl = s3FileUrls.get(index);
//			if (isFullRegex.matcher(s3FileUrl).matches()) {
//				fullExportFound = true;
//				if (previousWasDelta) {
//					fullIndex = index;
//					previousWasDelta = false;
//				}
//			} else {
//				previousWasDelta = true;
//			}
//		}
//		if (!fullExportFound) {
//			throw new IllegalStateException(String
//					.format("itaca full export files not found in %s", s3FolderUrl));
//		}
//		s3FileUrls.subList(0, fullIndex).clear();
//		
//		// skip already processed files
//		if (!Strings.isNullOrEmpty(lastProcessed)) {
//			for (int i = 0; i < s3FileUrls.size(); i ++) {
//				final String s3FileUrl = s3FileUrls.get(i);
//				if (s3FileUrl.equals(lastProcessed)) {
//					s3FileUrls.subList(0, i + 1).clear();
//					break;
//				}
//			}
//		}
//		
//		return s3FileUrls;
//	}

	private IndexUpdater2 process(String[] args) throws Exception {

		// bind lock tcp port
		final int bindPort = Integer.parseInt(configuration.getProperty("index_updater.bind_port",
				configuration.getProperty("default.bind_port", "0")));
		try (ServerSocket socket = new ServerSocket(bindPort)) {
			logger.debug("socket bound to {}", socket.getLocalSocketAddress());

			// standalone mode
			if (args.length > 1 && "standalone".equals(args[1])) {
				logger.debug(args[0] + ":" + args[1]);
				logger.debug(configuration.getProperty("index_builder.standalone_json"));
				final JsonObject input = GsonUtils.fromJson(configuration
						.getProperty("index_builder.standalone_json"), JsonObject.class);
				final JsonObject output = new JsonObject();
				logger.debug("input {}", new GsonBuilder()
						.setPrettyPrinting().create().toJson(input));
				processMessage(input, output);
				logger.debug("output {}", new GsonBuilder()
						.setPrettyPrinting().create().toJson(output));
			}
			else{
				// sqs message pump
				final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "index_updater.sqs");
				sqsMessagePump.pollingLoop(messageDeduplicator, new SqsMessagePump.Consumer() {

					private JsonObject output;
					private JsonObject error;

					@Override
					public JsonObject getStartedMessagePayload(JsonObject message) {
						output = null;
						error = null;
						return SqsMessageHelper.formatContext();
					}

					@Override
					public boolean consumeMessage(JsonObject message) {
						try {
							output = new JsonObject();
							processMessage(message, output);
							error = null;
							return true;
						} catch (Exception e) {
							output = null;
							error = SqsMessageHelper.formatError(e);
							return false;
						}
					}

					@Override
					public JsonObject getCompletedMessagePayload(JsonObject message) {
						return output;
					}

					@Override
					public JsonObject getFailedMessagePayload(JsonObject message) {
						return error;
					}

				});
			}
		}
		return this;

	}

	private void processMessage(JsonObject input, JsonObject output) throws Exception {

//		{
//		  "header": {
//		    "uuid": "6479449c-221d-4600-92c1-1c8d1f7a6xxx",
//		    "timestamp": "2018-09-27T00:00:00.000Z",
//		    "queue": "{env}_to_process_core_index_updater",
//		    "sender": "aws-console"
//		  },
//		  "body": {
//		    "force": false,
//		    "__comment": "optional additional field(s) will be echoed to other sqs messages inside \"input\" field"
//		  }
//		}

		final long startTimeMillis = System.currentTimeMillis();

		// parse input json message
		final JsonObject inputBody = input.getAsJsonObject("body");
		final boolean force = GsonUtils.getAsBoolean(inputBody, "force", false);
		logger.debug("force {}", force);

		// configuration
		final File resumeFile = new File(configuration.getProperty("index_updater.resume_file"));
		final File latestFile = new File(configuration.getProperty("index_updater.latest_file", "_LATEST"));
		final String outputS3FolderUrl = configuration.getProperty("index_updater.output.s3_folder_url");
		final File localFolder = new File(configuration.getProperty("index_updater.local_folder"));
		final int threadCount = Integer.parseInt(configuration.getProperty("index_updater.thread_count", "1"));
		final String dumpFolderUrl = GsonUtils.getAsString(inputBody, "dump", null);

		// load last processed file
		//final String lastProcessed = readResumeFile(resumeFile);
		Map<String, String> lastProcessed = readResumeFile2(resumeFile);
		//logger.debug("lastProcessed {}", lastProcessed);

		// find unprocessed json file(s)
//		final List<String> unprocessedFileUrls = getUnprocessedFileUrls(lastProcessed);
//		logger.debug("unprocessedFileUrls {}", unprocessedFileUrls);

		final List<String> unprocessedUlisseFileUrls = getUnprocessedElasticFileUrls(lastProcessed,dumpFolderUrl);
		logger.debug("unprocessedFileUrls {}", unprocessedUlisseFileUrls);

		// return immediately if no unprocessed file(s) exist and no force flag
		if (!force && unprocessedUlisseFileUrls.isEmpty()) {
			// output
			output.addProperty("startTime", DateFormat
					.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM)
						.format(new Date(startTimeMillis)));
			output.addProperty("totalDuration", TextUtils
					.formatDuration(System.currentTimeMillis() - startTimeMillis));
			output.addProperty("additionalInfo", String
					.format("no unprocessed json file(s) exists, last processed file %s", lastProcessed));
			logger.info("no unprocessed json file(s) exists, last processed file {}", lastProcessed);
			logger.debug("message processed in {}",
					TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
			return;
		}

		// check index _LATEST version
		final String latestS3FileUrl = new StringBuilder()
				.append(outputS3FolderUrl)
				.append('/')
				.append(latestFile.getName())
				.toString();
		final String latestVersion = s3
				.getObjectContents(new S3.Url(latestS3FileUrl), charset);
		logger.debug("latestVersion {}", latestVersion);
		final int indexVersion = Strings.isNullOrEmpty(latestVersion) ?
				1 : 1 + Integer.parseInt(latestVersion);
		logger.debug("indexVersion {}", indexVersion);
		// actual s3 output folder
		final String versionedOutputS3FolderUrl = new StringBuilder()
				.append(outputS3FolderUrl)
				.append('/')
				.append(indexVersion)
				.toString();
		logger.debug("versionedOutputS3FolderUrl {}", versionedOutputS3FolderUrl);

		// index metadata
		final ReverseIndexMetadata metadata = new ReverseIndexMetadata(localFolder);
		logger.debug("metadata version {}", metadata
				.getProperty(ReverseIndexMetadata.VERSION, null));
		metadata.setProperty(ReverseIndexMetadata.VERSION,
				Integer.toString(indexVersion));
		metadata.commit();

		// parse itaca json file(s)
		//itacaJsonParser(unprocessedFileUrls, output);
		elasticJsonParser(unprocessedUlisseFileUrls,output);


		// create document(s)
		documentsBuilder(output);

		// create iswc(s) index
		if ("true".equalsIgnoreCase(configuration.getProperty("index_updater.iswc.create")))
			createIndex(iswcIndex, TextType.iswc, output);

		// create isrc(s) index
		if ("true".equalsIgnoreCase(configuration.getProperty("index_updater.isrc.create")))
			createIndex(isrcIndex, TextType.isrc, output);

		// create title(s) index
		if ("true".equalsIgnoreCase(configuration.getProperty("index_updater.title.create")))
			createIndex(titleIndex, TextType.title, titleTokenizer, output);

		// create artist(s) index
		if ("true".equalsIgnoreCase(configuration.getProperty("index_updater.artist.create")))
			createIndex(artistIndex, TextType.artist, artistTokenizer, output);

		// upload nosql files to s3 folder
		final AtomicInteger uploadedFiles = new AtomicInteger(0);
		final AtomicLong outputSize = new AtomicLong(0L);
		if ("true".equalsIgnoreCase(configuration
				.getProperty("index_updater.upload_to_s3", "true"))) {

			// delete dirty files in s3 output folder
			logger.debug("deleting existing files in {}", versionedOutputS3FolderUrl);
			deleteS3FolderContents(versionedOutputS3FolderUrl);

			// upload to s3 folder
			logger.debug("uploading nosql files to {}", versionedOutputS3FolderUrl);
			final ConcurrentLinkedDeque<File> files = new ConcurrentLinkedDeque<>();
			files.addAll(Arrays.asList(localFolder.listFiles()));
			for (final Iterator<File> iterator = files.iterator(); iterator.hasNext(); ) {
				final File file = iterator.next();
				if (file.isDirectory()) {
					iterator.remove();
					files.addAll(Arrays.asList(file.listFiles()));
				}
			}
			final Iterator<File> iterator = files.iterator();

			// start upload thread(s)
			final Object monitor = new Object();
			final ExecutorService executorService = Executors.newCachedThreadPool();
			final AtomicInteger runningThreads = new AtomicInteger(0);
			final AtomicReference<Exception> threadException = new AtomicReference<>(null);
			while (runningThreads.get() < threadCount) {
				logger.debug("starting thread {}", runningThreads.incrementAndGet());
				executorService.execute(new Runnable() {

					@Override
					public void run() {
						try {
							while (null == threadException.get()) {
								final File file;
								synchronized (iterator) {
									if (!iterator.hasNext())
										return;
									file = iterator.next();
								}
								// compute index size on disk
								outputSize.addAndGet(file.length());
								// upload database file
								String relativePath = file.getAbsolutePath()
										.substring(localFolder
												.getAbsolutePath().length());
								while (relativePath.startsWith("/"))
									relativePath = relativePath.substring(1);
								final String s3FileUrl = new StringBuilder()
										.append(versionedOutputS3FolderUrl)
										.append('/')
										.append(relativePath)
										.toString();
								if (!s3.upload(new S3.Url(s3FileUrl), file)) {
									logger.error("error uploading file {} to {}", relativePath, s3FileUrl);
									throw new IOException(String
											.format("error uploading file %s to %s", relativePath, s3FileUrl));
								} else {
									logger.debug("file {} uploaded to {}", relativePath, s3FileUrl);
									uploadedFiles.incrementAndGet();
								}
							}
						} catch (Exception e) {
							logger.error("run", e);
							threadException.compareAndSet(null, e);
						} finally {
							logger.debug("thread {} finished", runningThreads.getAndDecrement());
							synchronized (monitor) {
								monitor.notify();
							}
						}
					}
				});
			}

			// join upload thread(s)
			while (runningThreads.get() > 0) {
				synchronized (monitor) {
					monitor.wait(1000L);
				}
			}

			// re-throw thread exception
			if (null != threadException.get())
				throw threadException.getAndSet(null);

			// create & upload index latest file
			try (final FileWriter writer = new FileWriter(latestFile)) {
				writer.write(Integer.toString(indexVersion));
			}
			latestFile.deleteOnExit();
			if (!s3.upload(new S3.Url(latestS3FileUrl), latestFile)) {
				logger.error("error uploading file {} to {}", latestFile.getName(), latestS3FileUrl);
				throw new IOException(String
						.format("error uploading file %s to %s", latestFile.getName(), latestS3FileUrl));
			} else {
				logger.debug("file {} uploaded to {}", latestFile.getName(), latestS3FileUrl);
			}
			latestFile.delete();
		}

		// output
		output.addProperty("uploadedFiles", uploadedFiles.get());
		output.addProperty("outputSizeInBytes", outputSize.get());
		output.addProperty("outputSize", TextUtils.formatSize(outputSize.get()));
		output.addProperty("startTime", DateFormat
				.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM)
					.format(new Date(startTimeMillis)));
		output.addProperty("totalDuration", TextUtils
				.formatDuration(System.currentTimeMillis() - startTimeMillis));

		logger.debug("message processed in {}", TextUtils
				.formatDuration(System.currentTimeMillis() - startTimeMillis));

	}

	private void itacaJsonParser(List<String> s3FileUrls, JsonObject output) throws IOException {

		final int s3Retries = Integer.parseInt(configuration
				.getProperty("index_updater.s3_retries", "1"));
		final File resumeFile = new File(configuration
				.getProperty("index_updater.resume_file"));

		final int origin = Origin.valueOf(configuration
				.getProperty("index_updater.origin"));
		final boolean overwriteExisting = "true".equalsIgnoreCase(configuration
				.getProperty("index_updater.overwrite_existing"));

		final String codeField = configuration
				.getProperty("index_updater.field.code");
		final String[] originalTitleFields = configuration
				.getProperty("index_updater.field.original_title").split(",");
		final String[] alternativeTitleFields = configuration
				.getProperty("index_updater.field.alternative_title").split(",");
		final String authorField = configuration
				.getProperty("index_updater.field.author");
		final String composerField = configuration
				.getProperty("index_updater.field.composer");
		final String performerField = configuration
				.getProperty("index_updater.field.performer");
		final String iswcField = configuration
				.getProperty("index_updater.field.iswc");
		final String isrcField = configuration
				.getProperty("index_updater.field.isrc");
		final String elField = configuration
				.getProperty("index_updater.field.el");
		final String pdField = configuration
				.getProperty("index_updater.field.pd");
		final String scopeField = configuration
				.getProperty("index_updater.field.scope");
		final String weightField = configuration
				.getProperty("index_updater.field.weight");

		final HeartBeat heartbeat = HeartBeat.constant("json",
				Integer.parseInt(configuration.getProperty("index_updater.heartbeat",
						configuration.getProperty("default.heartbeat", "1000"))));

		logger.debug("parsing itaca json file(s)");
		long startTimeMillis = System.currentTimeMillis();

		// stats
		final AtomicLong totalRecords = new AtomicLong(0L);
		final AtomicLong invalidRecords = new AtomicLong(0L);
		final AtomicLong insertedWorks = new AtomicLong(0L);
		final AtomicLong modifiedWorks = new AtomicLong(0L);

		// process s3 file(s)
		int s3Retry = 0;
		for (String s3FileUrl : s3FileUrls) {
			for (; s3Retry < s3Retries; s3Retry ++) {
				try {
					logger.debug("processing file {}", s3FileUrl);
					final S3Object object = s3.getObject(new S3.Url(s3FileUrl));
					try (final S3ObjectInputStream inputStream = object.getObjectContent();
							final GZIPInputStream gzipInputStream = new GZIPInputStream(inputStream);
							final InputStreamReader inputStreamReader = new InputStreamReader(gzipInputStream);
							final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
							final JsonReader jsonReader = new JsonReader(bufferedReader)) {

						jsonReader.beginArray();
						while (jsonReader.hasNext()) {
							totalRecords.incrementAndGet();
							final JsonObject jsonObject = gson.fromJson(jsonReader, JsonObject.class);
							JsonElement jsonElement;

							// siae code
							String code = null;
							jsonElement = jsonObject.get(codeField);
							if (null != jsonElement) {
								String text = SiaeCode.normalize(jsonElement.getAsString());
								if (!Strings.isNullOrEmpty(text)) {
									code = text;
								}
							}
							if (null == code) {
								logger.warn("missing code: {}", jsonObject);
								invalidRecords.incrementAndGet();
								continue;
							}

							// code(s)
							final HashSet<Code> codes = new HashSet<>();
							// siae
							codes.add(new Code(code, CodeType.siae, origin));
							// iswc(s)
							jsonElement = jsonObject.get(iswcField);
							if (null != jsonElement) {
								if (jsonElement.isJsonArray()) {
									for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
										String text = iswcNormalizer
												.normalize(jsonArrayElement.getAsString());
										if (!Strings.isNullOrEmpty(text)) {
											codes.add(new Code(text, CodeType.iswc, origin));
										}
									}
								} else if (jsonElement.isJsonPrimitive()) {
									String text = iswcNormalizer
											.normalize(jsonElement.getAsString());
									if (!Strings.isNullOrEmpty(text)) {
										codes.add(new Code(text, CodeType.iswc, origin));
									}
								}
							}
							// isrc(s)
							jsonElement = jsonObject.get(isrcField);
							if (null != jsonElement) {
								if (jsonElement.isJsonArray()) {
									for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
										String text = isrcNormalizer
												.normalize(jsonArrayElement.getAsString());
										if (!Strings.isNullOrEmpty(text)) {
											codes.add(new Code(text, CodeType.isrc, origin));
										}
									}
								} else if (jsonElement.isJsonPrimitive()) {
									String text = isrcNormalizer
											.normalize(jsonElement.getAsString());
									if (!Strings.isNullOrEmpty(text)) {
										codes.add(new Code(text, CodeType.isrc, origin));
									}
								}
							}

							// title(s)
							final HashSet<Title> titles = new HashSet<>();
							// original title
							for (String originalTitleField : originalTitleFields) {
								jsonElement = jsonObject.get(originalTitleField);
								if (null != jsonElement) {
									String text = jsonElement.getAsString();
									if (!Strings.isNullOrEmpty(text)) {
										titles.add(new Title(text, TitleType.song, Category.original, origin));
										break;
									}
								}
							}
							if (titles.isEmpty()) {
								logger.warn("missing original title: {}", jsonObject);
								invalidRecords.incrementAndGet();
								continue;
							}
							// alternative title(s)
							for (String alternativeTitleField : alternativeTitleFields) {
								jsonElement = jsonObject.get(alternativeTitleField);
								if (null != jsonElement) {
									final Set<String> texts = new HashSet<>();
									if (jsonElement.isJsonArray()) {
										for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
											texts.add(jsonArrayElement.getAsString());
										}
									} else if (jsonElement.isJsonPrimitive()) {
										texts.add(jsonElement.getAsString());
									}
									for (String text : texts) {
										if (!Strings.isNullOrEmpty(text)) {
											titles.add(new Title(text, TitleType.song, Category.alternative, origin));
										}
									}
								}
							}

							// artist(s)
							final HashSet<Artist> artists = new HashSet<>();
							// author(s)
							jsonElement = jsonObject.get(authorField);
							if (null != jsonElement) {
								final Set<String> texts = new HashSet<>();
								if (jsonElement.isJsonArray()) {
									for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
										texts.add(jsonArrayElement.getAsString());
									}
								} else if (jsonElement.isJsonPrimitive()) {
									texts.add(jsonElement.getAsString());
								}
								for (String text : texts) {
									if (!Strings.isNullOrEmpty(text)) {
										artists.add(new Artist(text, ArtistRole.author, origin));
									}
								}
							}
							// composer(s)
							jsonElement = jsonObject.get(composerField);
							if (null != jsonElement) {
								final Set<String> texts = new HashSet<>();
								if (jsonElement.isJsonArray()) {
									for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
										texts.add(jsonArrayElement.getAsString());
									}
								} else if (jsonElement.isJsonPrimitive()) {
									texts.add(jsonElement.getAsString());
								}
								for (String text : texts) {
									if (!Strings.isNullOrEmpty(text)) {
										artists.add(new Artist(text, ArtistRole.composer, origin));
									}
								}
							}
							// performer(s)
							jsonElement = jsonObject.get(performerField);
							if (null != jsonElement) {
								final Set<String> texts = new HashSet<>();
								if (jsonElement.isJsonArray()) {
									for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
										texts.add(jsonArrayElement.getAsString());
									}
								} else if (jsonElement.isJsonPrimitive()) {
									texts.add(jsonElement.getAsString());
								}
								for (String text : texts) {
									if (!Strings.isNullOrEmpty(text)) {
										artists.add(new Artist(text, ArtistRole.performer, origin));
									}
								}
							}
							if (artists.isEmpty()) {
								logger.warn("missing artists: {}", jsonObject);
								invalidRecords.incrementAndGet();
								continue;
							}

							// attribute(s)
							final HashSet<Attribute> attributes = new HashSet<>();
							// flag EL
							jsonElement = jsonObject.get(elField);
							if (null != jsonElement) {
								String text = jsonElement.getAsString();
								if (!Strings.isNullOrEmpty(text)) {
									attributes.add(new Attribute(AttributeKey.flag_el, text, origin));
								}
							}
							// flag PD
							jsonElement = jsonObject.get(pdField);
							if (null != jsonElement) {
								String text = jsonElement.getAsString();
								if (!Strings.isNullOrEmpty(text)) {
									attributes.add(new Attribute(AttributeKey.flag_pd, text, origin));
								}
							}
							// ambito
							jsonElement = jsonObject.get(scopeField);
							if (null != jsonElement) {
								String text = jsonElement.getAsString();
								if (!Strings.isNullOrEmpty(text)) {
									attributes.add(new Attribute(AttributeKey.ambito, text, origin));
								}
							}
							// rilevanza
							BigDecimal weight = BigDecimal.ZERO;
							jsonElement = jsonObject.get(weightField);
							if (null != jsonElement) {
								String text = jsonElement.getAsString();
								if (!Strings.isNullOrEmpty(text)) {
									attributes.add(new Attribute(AttributeKey.rilevanza, text, origin));
									weight = new BigDecimal(text);
								}
							}
							// find existing or generate new work id
							final long id;
							final CollectingCode collecting = collectingDatabase.getByCode(code);
							if (null == collecting) {
								id = workDatabase.getNextId();
								// insert collecting code
								collectingDatabase.upsert(new CollectingCode(code, id, weight));
							} else {
								id = collecting.workId;
								if (!BigDecimals.almostEquals(weight, collecting.weight)) {
									// update collecting code
									collecting.weight = weight;
									collectingDatabase.upsert(collecting);
								}
							}

							// merge data from other origin(s)
							if (!overwriteExisting && null != collecting) {
								final Work work = workDatabase.getById(id);
								if (null != work) {
									// merge existing code(s)
									if (null != work.codes) {
										for (Code workCode : work.codes) {
											if (origin != workCode.origin) {
												codes.add(workCode);
											}
										}
									}
									// merge existing title(s)
									if (null != work.titles) {
										for (Title workTitle : work.titles) {
											if (origin != workTitle.origin) {
												titles.add(workTitle);
											}
										}
									}
									// merge existing artist(s)
									if (null != work.artists) {
										for (Artist workArtist : work.artists) {
											if (origin != workArtist.origin) {
												artists.add(workArtist);
											}
										}
									}
									// merge existing attribute(s)
									if (null != work.attributes) {
										for (Attribute workAttribute : work.attributes) {
											if (origin != workAttribute.origin) {
												attributes.add(workAttribute);
											}
										}
									}
								}
							}

							// insert or replace work
							final Work work = new Work(id, codes, titles, artists, attributes);
							if (null == workDatabase.upsert(work)) {
								insertedWorks.incrementAndGet();
							} else {
								modifiedWorks.incrementAndGet();
							}

							heartbeat.pump();

						}
						jsonReader.endArray();
					}
					// save last processed
					writeResumeFile(resumeFile, s3FileUrl);
					break;
				} catch (IOException e) {
					logger.error("process", e);
				}
			}
		}
		logger.debug("{} records imported", heartbeat.getTotalPumps());
		logger.debug("import completed in {}",
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		// processing stats
		final JsonObject processingStats = new JsonObject();
		processingStats.addProperty("totalRecords", totalRecords.get());
		processingStats.addProperty("invalidRecords", invalidRecords.get());
		processingStats.addProperty("insertedWorks", insertedWorks.get());
		processingStats.addProperty("modifiedWorks", modifiedWorks.get());
		processingStats.addProperty("modifiedWorks", modifiedWorks.get());
		processingStats.addProperty("jsonParsingDuration",
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		// defrag work database
		startTimeMillis = System.currentTimeMillis();
		logger.debug("work database defrag started");
		workDatabase.defrag();
		logger.debug("work database defrag completed in {}",
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
		processingStats.addProperty("workDatabaseDefragDuration",
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		// defrag collecting code database
		startTimeMillis = System.currentTimeMillis();
		logger.debug("collecting database defrag started");
		collectingDatabase.defrag();
		logger.debug("collecting database defrag completed in {}",
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
		processingStats.addProperty("collectingDatabaseDefragDuration",
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		// output
		output.add("itacaJsonParser", processingStats);
	}

	private void documentsBuilder(JsonObject output) throws Exception {

		// configuration
		final Set<Integer> includeOrigins = new HashSet<>();
		for (String origin : configuration
				.getProperty("index_updater.include_origins").split(","))  {
			includeOrigins.add(Origin.valueOf(origin));
		}
		final HeartBeat heartbeat = HeartBeat.constant("documents",
				Integer.parseInt(configuration.getProperty("index_updater.heartbeat",
						configuration.getProperty("default.heartbeat", "1000"))));

		logger.debug("creating document(s)");
		final AtomicLong totalWorks = new AtomicLong(0L);
		final AtomicLong skippedWorks = new AtomicLong(0L);
		final long startTimeMillis = System.currentTimeMillis();

		documentStore.truncate();
		collectingDatabase.selectOrderByWeight(new CollectingCode.Selector() {

			private final AtomicLong documentId = new AtomicLong(0L);

			@Override
			public void select(CollectingCode collecting) throws Exception {
				totalWorks.incrementAndGet();
				final Work work = workDatabase.getById(collecting.workId);
				final HashSet<Text> texts = normalizeWork(work, includeOrigins);
				if (null != texts && !texts.isEmpty()) {
					final Document document = new Document(documentId
							.incrementAndGet(), work.id, texts);
					documentStore.upsert(document);
				} else {
					skippedWorks.incrementAndGet();
				}
				heartbeat.pump();
			}

		}, false); // largest weight first

		logger.debug("{} processed collecting code(s)", heartbeat.getTotalPumps());
		logger.info("skipped {} works", skippedWorks);
		logger.info("document(s) created in {}",TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		// processing stats
		final JsonObject processingStats = new JsonObject();
		processingStats.addProperty("totalWorks", totalWorks.get());
		processingStats.addProperty("skippedWorks", skippedWorks.get());
		processingStats.addProperty("documentsCreationDuration",TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		// output
		output.add("documentsBuilder", processingStats);
	}

	private void createIndex(ReverseIndex reverseIndex, final int textType, JsonObject output) throws Exception {

		final HeartBeat heartbeat = HeartBeat.constant(TextType.toString(textType),
				Integer.parseInt(configuration.getProperty("index_updater.heartbeat",
						configuration.getProperty("default.heartbeat", "1000"))));

		logger.debug("adding terms to {} index...", TextType.toString(textType));
		long startTimeMillis = System.currentTimeMillis();
		final AtomicLong totalDocuments = new AtomicLong(0L);
		final AtomicLong totalTexts = new AtomicLong(0L);

		final ReverseIndex.Editor editor = reverseIndex.edit();
		editor.rewind();
		documentStore.select(new Document.Selector() {

			@Override
			public void select(Document document) throws IOException {
				totalDocuments.incrementAndGet();
				for (Text text : document.texts) {
					if (textType == text.type) {
						editor.add(text.text, document.id);
						totalTexts.incrementAndGet();
					}
				}
				heartbeat.pump();
			}

		});
		logger.debug("{} processed documents", heartbeat.getTotalPumps());
		logger.info("terms added to {} index in {}", TextType.toString(textType),
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		// processing stats
		final JsonObject processingStats = new JsonObject();
		processingStats.addProperty("totalDocuments", totalDocuments.get());
		processingStats.addProperty("totalTexts", totalTexts.get());
		processingStats.addProperty("indexCreationDuration",
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		logger.debug("sorting postings of {} index...", TextType.toString(textType));
		startTimeMillis = System.currentTimeMillis();
		editor.commit();
		editor.rewind();
		logger.info("{} index postings saved in {}", TextType.toString(textType),
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
		processingStats.addProperty("postingsSortingDuration",
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		// output
		output.add(TextType.toString(textType) + "IndexBuilder", processingStats);

	}

	private void createIndex(ReverseIndex reverseIndex, final int textType, final TextTokenizer tokenizer, JsonObject output) throws Exception {

		final NumberSpeller numberSpeller = "true".equalsIgnoreCase(configuration
				.getProperty("index_updater." + TextType.toString(textType) + ".spell_numbers")) ?
						new NumberSpeller(configuration, "number_speller") : null;
		final HeartBeat heartbeat = HeartBeat.constant(TextType.toString(textType),
				Integer.parseInt(configuration.getProperty("index_updater.heartbeat",
						configuration.getProperty("default.heartbeat", "1000"))));

		logger.debug("adding terms to {} index...", TextType.toString(textType));
		long startTimeMillis = System.currentTimeMillis();
		final AtomicLong totalDocuments = new AtomicLong(0L);
		final AtomicLong totalTexts = new AtomicLong(0L);
		final AtomicLong totalSpelled = new AtomicLong(0L);

		final ReverseIndex.Editor editor = reverseIndex.edit();
		editor.rewind();
		documentStore.select(new Document.Selector() {

			@Override
			public void select(Document document) throws IOException {
				totalDocuments.incrementAndGet();
				tokenizer.clear();
				for (Text text : document.texts) {
					if (textType == text.type) {
						tokenizer.tokenize(text.text);
						if (null != numberSpeller) {
							for (String spelled : numberSpeller.spell(text.text)) {
								tokenizer.tokenize(spelled);
								totalSpelled.incrementAndGet();
							}
						}
					}
				}
				final Collection<String> tokens = tokenizer.getTokens();
				editor.addAll(tokens, document.id);
				totalTexts.addAndGet(tokens.size());
				heartbeat.pump();
			}

		});
		logger.debug("{} processed documents", heartbeat.getTotalPumps());
		logger.info("terms added to {} index in {}", TextType.toString(textType),
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		// processing stats
		final JsonObject processingStats = new JsonObject();
		processingStats.addProperty("totalDocuments", totalDocuments.get());
		processingStats.addProperty("totalTexts", totalTexts.get());
		processingStats.addProperty("totalSpelled", totalSpelled.get());
		processingStats.addProperty("indexCreationDuration",
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		logger.debug("sorting postings of {} index...", TextType.toString(textType));
		startTimeMillis = System.currentTimeMillis();
		editor.commit();
		editor.rewind();
		logger.info("{} index postings saved in {}", TextType.toString(textType),
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
		processingStats.addProperty("postingsSortingDuration",
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		// output
		output.add(TextType.toString(textType) + "IndexBuilder", processingStats);

	}


	//////////////////////////7
	private void elasticJsonParser(List<String> s3FileUrls, JsonObject output) throws IOException {

		final int s3Retries = Integer.parseInt(configuration
				.getProperty("index_updater.s3_retries", "1"));
		final File resumeFile = new File(configuration
				.getProperty("index_updater.resume_file"));

		final int origin = Origin.valueOf(configuration
				.getProperty("index_updater.origin"));
		final boolean overwriteExisting = "true".equalsIgnoreCase(configuration
				.getProperty("index_updater.overwrite_existing"));

		final String codeField                	= configuration.getProperty("elastic_feed_parser.field.code_siae");
//		final String composerField 				= configuration.getProperty("index_updater.field.composer");
//		final String performerOriginField 			= configuration.getProperty("elastic_feed_parser.field.performer.origin");
		final String iswcField 					= configuration.getProperty("elastic_feed_parser.field.code_iswc");
		final String iswcTextField 				= configuration.getProperty("elastic_feed_parser.field.code_iswc.text");
		final String isrcField 					= configuration.getProperty("elastic_feed_parser.field.code_isrc");
		final String isrcTextField 				= configuration.getProperty("elastic_feed_parser.field.code_isrc.text");
		final String elField 					= configuration.getProperty("elastic_feed_parser.field.flag_el");
		final String pdField 					= configuration.getProperty("elastic_feed_parser.field.flag_pd");
		final String irregolareField 		    = configuration.getProperty("elastic_feed_parser.field.irregularity");
		//final String doppioDepositoField 		= configuration.getProperty("elastic_feed_parser.field.flag_deposito");
		final String scopeField 				= configuration.getProperty("elastic_feed_parser.field.scope");
		final String weightField 				= configuration.getProperty("elastic_feed_parser.field.relevance");
		final String accruedField 				= configuration.getProperty("elastic_feed_parser.field.accrued");
		final String riskField 				    = configuration.getProperty("elastic_feed_parser.field.risk");
		final String statusField 				= configuration.getProperty("elastic_feed_parser.field.status");

		final String maturatiField 				= configuration.getProperty("elastic_feed_parser.field.maturati");
		final String maturatiValoreField 		= configuration.getProperty("elastic_feed_parser.field.maturati.valore");
		final String maturatiPeriodoField 		= configuration.getProperty("elastic_feed_parser.field.maturati.periodoLabel");
		final String maturatiDataDaField 		= configuration.getProperty("elastic_feed_parser.field.maturati.dataDa");
		final String maturatiDataAField 		= configuration.getProperty("elastic_feed_parser.field.maturati.dataA");

		

		//TITOLO ORIGINARII
		final String originalTitleFields    	= configuration.getProperty("elastic_feed_parser.field.original_title");
		final String originalTextTitleFields    = configuration.getProperty("elastic_feed_parser.field.original_title.text");

		//TITOLO ALTERNATIVI
		final String alternativeTitleFields 	= configuration.getProperty("elastic_feed_parser.field.alternative_title");
		final String alternativeTextTitleFields = configuration.getProperty("elastic_feed_parser.field.alternative_title.text");

		//AUTORI - COMPOSITORI
		final String authorSiaeField 				= configuration.getProperty("elastic_feed_parser.field.schema");
		final String authorSiaeNameField 			= configuration.getProperty("elastic_feed_parser.field.schema.artist");
		final String authorSiaeRoleField 			= configuration.getProperty("elastic_feed_parser.field.schema.role");
		final String authorSiaeIpiNumberField 			= configuration.getProperty("elastic_feed_parser.field.schema.ipinr");
		
		final String authorIPIField 				= configuration.getProperty("elastic_feed_parser.field.ipi");
		final String authorIPIRoleField 			= configuration.getProperty("elastic_feed_parser.field.ipi.roles");
		final String authorIPINameField 			= configuration.getProperty("elastic_feed_parser.field.ipi.name_numbers.artist");
		final String authorIPINumberField 			= configuration.getProperty("elastic_feed_parser.field.ipi.name_numbers.ipinr");
		final String authorIPIBaseField 			= configuration.getProperty("elastic_feed_parser.field.ipi.base_number");

		//INTERPRETI
		final String performerField 			= configuration.getProperty("elastic_feed_parser.field.performer");
		final String performerArtistField 			= configuration.getProperty("elastic_feed_parser.field.performer.artist");

		final Map<String,String> artistOriginMapping 			= GsonUtils.decodeJsonMap(configuration.getProperty("elastic_feed_parser.roles.artist_mapping"));

		final HeartBeat heartbeat = HeartBeat.constant("json",
				Integer.parseInt(configuration.getProperty("index_updater.heartbeat",
						configuration.getProperty("default.heartbeat", "1000"))));

		logger.debug("parsing ulisse json file(s)");
		long startTimeMillis = System.currentTimeMillis();

		// stats
		final AtomicLong totalRecords = new AtomicLong(0L);
		final AtomicLong invalidRecords = new AtomicLong(0L);
		final AtomicLong insertedWorks = new AtomicLong(0L);
		final AtomicLong modifiedWorks = new AtomicLong(0L);

		// process s3 file(s)
		int s3Retry = 0;
		for (String s3FileUrl : s3FileUrls) {
			for (; s3Retry < s3Retries; s3Retry ++) {
				try {
					logger.debug("processing file {}", s3FileUrl);
					final S3Object object = s3.getObject(new S3.Url(s3FileUrl));
					try (final S3ObjectInputStream inputStream = object.getObjectContent();
							final GZIPInputStream gzipInputStream = new GZIPInputStream(inputStream);
							final InputStreamReader inputStreamReader = new InputStreamReader(gzipInputStream);
							final BufferedReader bufferedReader = new BufferedReader(inputStreamReader)) {
						for (String line; null != (line = bufferedReader.readLine()); ){
							totalRecords.incrementAndGet();
							final JsonObject jsonObject = GsonUtils.fromJson(line, JsonObject.class);
							JsonElement jsonElement;

							// siae code
							String code = null;
							jsonElement = jsonObject.get(codeField);
							if (null != jsonElement) {
								String text = SiaeCode.normalize(jsonElement.getAsString());
								if (!Strings.isNullOrEmpty(text)) {
									code = text;
								}
							}
							if (null == code) {
								logger.warn("missing code: {}", jsonObject);
								invalidRecords.incrementAndGet();
								continue;
							}

							// code(s)
							final HashSet<Code> codes = new HashSet<>();
							// siae
							codes.add(new Code(code, CodeType.siae, origin));
							// iswc(s)
							jsonElement = jsonObject.get(iswcField);
							if (null != jsonElement) {
								if (jsonElement.isJsonArray()) {
									for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
										String text = iswcNormalizer.normalize(jsonArrayElement.getAsJsonObject().get(iswcTextField).getAsString());
										if (!Strings.isNullOrEmpty(text)) {
											codes.add(new Code(text, CodeType.iswc, origin));
										}
									}
								} else if (jsonElement.isJsonPrimitive()) {
									String text = iswcNormalizer.normalize(jsonElement.getAsString());
									if (!Strings.isNullOrEmpty(text)) {
										codes.add(new Code(text, CodeType.iswc, origin));
									}
								}
							}
							// isrc(s)
							jsonElement = jsonObject.get(isrcField);
							if (null != jsonElement) {
								if (jsonElement.isJsonArray()) {
									for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
										String text = isrcNormalizer.normalize(jsonArrayElement.getAsJsonObject().get(isrcTextField).getAsString());
										if (!Strings.isNullOrEmpty(text)) {
											codes.add(new Code(text, CodeType.isrc, origin));
											System.out.println("isrc:" + text);
										}
									}
								} else if (jsonElement.isJsonPrimitive()) {
									String text = isrcNormalizer.normalize(jsonElement.getAsString());
									if (!Strings.isNullOrEmpty(text)) {
										codes.add(new Code(text, CodeType.isrc, origin));
										System.out.println("isrc:" + text);
									}
								}
							}

							// title(s)
							final HashSet<Title> titles = new HashSet<>();
							// original title
							jsonElement = jsonObject.get(originalTitleFields);
							if (null != jsonElement) {
								if (jsonElement.isJsonArray()) {
									for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
										JsonElement jsonText = jsonArrayElement.getAsJsonObject().get(originalTextTitleFields);
										if(jsonText != null){
											String text = jsonText.getAsString();
											if (!Strings.isNullOrEmpty(text)) {
												titles.add(new Title(text, TitleType.song, Category.original, origin));
												break;
											}
										}
									}
								}
							}
							// alternative title(s)
							jsonElement = jsonObject.get(alternativeTitleFields);
							if (jsonElement != null && jsonElement.isJsonArray()) {
								for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
									JsonElement jsonText = jsonArrayElement.getAsJsonObject().get(alternativeTextTitleFields);
									if(jsonText != null){
										String text = jsonText.getAsString();
										if (!Strings.isNullOrEmpty(text)) {
											titles.add(new Title(text, TitleType.song, Category.alternative, origin));
											break;
										}
									}
								}
							}

							if (titles.isEmpty()) {
								logger.warn("missing original title: {}", jsonObject);
								invalidRecords.incrementAndGet();
								continue;
							}

							// artist(s)
							final HashSet<Artist> artists = new HashSet<>();
							
							// SIAE author(s)
							jsonElement = jsonObject.get(authorSiaeField);
							if (jsonElement.isJsonArray()) {
								for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
									JsonElement jsonNameText = jsonArrayElement.getAsJsonObject().get(authorSiaeNameField);
									JsonElement jsonRoleText = jsonArrayElement.getAsJsonObject().get(authorSiaeRoleField);
									JsonElement jsonIpiText = jsonArrayElement.getAsJsonObject().get(authorSiaeIpiNumberField);
									String roles = "";
									if(jsonNameText != null && (roles= artistOriginMapping.get(jsonRoleText)) != null){
										String text = jsonNameText.getAsString();
										String ipi = jsonIpiText.getAsString();
										if (!Strings.isNullOrEmpty(text)) {
											artists.add(new Artist(text, "artist_author".equals(roles)?ArtistRole.author:ArtistRole.composer, ipi, origin));
										}
									}
								}
							}

							// IPI author(s)
							jsonElement = jsonObject.get(authorIPIField);
							if (jsonElement.isJsonArray()) {
								for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
									JsonArray jsonRoleArray = jsonArrayElement.getAsJsonObject().getAsJsonArray(authorIPIRoleField);
									for(JsonElement jsonRoleElement : jsonRoleArray){
										String jsonRoleText = jsonRoleElement.getAsString();
										JsonElement jsonBaseArray = jsonArrayElement.getAsJsonObject().get(authorIPIBaseField);
										for(JsonElement jsonTextElement : jsonBaseArray.getAsJsonArray()){
											JsonElement jsonNameText = jsonTextElement.getAsJsonObject().get(authorIPINameField);
											JsonElement jsonNameIpiText = jsonTextElement.getAsJsonObject().get(authorIPINumberField);
											String roles = "";
											if(jsonNameText != null && (roles= artistOriginMapping.get(jsonRoleText)) != null){
												String text = jsonNameText.getAsString();
												String ipi = jsonNameIpiText.getAsString();
												if (!Strings.isNullOrEmpty(text)) {
													artists.add(new Artist(text, "artist_author".equals(roles)?ArtistRole.author:ArtistRole.composer, ipi, origin));
												}
											}
										}
									}
								}
							}
							// performer(s)
							jsonElement = jsonObject.get(performerField);
							if (null != jsonElement) {
								for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
									JsonElement jsonNameText = jsonArrayElement.getAsJsonObject().get(performerArtistField);
									String text = jsonNameText.getAsString();
									if (!Strings.isNullOrEmpty(text)) {
										artists.add(new Artist(text, ArtistRole.performer, origin));
									}
								}
							}
							if (artists.isEmpty()) {
								//logger.warn("missing artists: {}", jsonObject);
								invalidRecords.incrementAndGet();
								continue;
							}

							// attribute(s)
							final HashSet<Attribute> attributes = new HashSet<>();
							// flag EL
							jsonElement = jsonObject.get(elField);
							if (null != jsonElement) {
								String text = jsonElement.getAsString();
								if (!Strings.isNullOrEmpty(text)) {
									attributes.add(new Attribute(AttributeKey.flag_el, text, origin));
								}
							}
							// flag PD
							jsonElement = jsonObject.get(pdField);
							if (null != jsonElement) {
								String text = jsonElement.getAsString();
								if (!Strings.isNullOrEmpty(text)) {
									attributes.add(new Attribute(AttributeKey.flag_pd, text, origin));
								}
							}
							// flag Irregolare
							jsonElement = jsonObject.get(irregolareField);
							if (null != jsonElement) {
								String text = jsonElement.getAsString();
								if (!Strings.isNullOrEmpty(text)) {
									attributes.add(new Attribute(AttributeKey.irregularity, text, origin));
								}
							}


							// ambito
							jsonElement = jsonObject.get(scopeField);
							if (null != jsonElement) {
								String text = jsonElement.getAsString();
								if (!Strings.isNullOrEmpty(text)) {
									attributes.add(new Attribute(AttributeKey.ambito, text, origin));
								}
							}
							// rilevanza
							BigDecimal weight = BigDecimal.ZERO;
							jsonElement = jsonObject.get(weightField);
							if (null != jsonElement) {
								String text = jsonElement.getAsString();
								if (!Strings.isNullOrEmpty(text)) {
									attributes.add(new Attribute(AttributeKey.rilevanza, text, origin));
									weight = new BigDecimal(text);
								}
							}
							// accrued
							jsonElement = jsonObject.get(accruedField);
							if (null != jsonElement) {
								final String accrued = jsonElement.getAsString();
								if (!Strings.isNullOrEmpty(accrued)) {
									attributes.add(new Attribute(AttributeKey.accrued, BigDecimals.toPlainString(new BigDecimal(accrued)), origin));
								}
							}
							// risk
							jsonElement = jsonObject.get(riskField);
							if (null != jsonElement) {
								final String risk = jsonElement.getAsString();
								if (!Strings.isNullOrEmpty(risk)) {
									attributes.add(new Attribute(AttributeKey.risk, BigDecimals.toPlainString(new BigDecimal(risk)), origin));
								}
							}
							// status
							jsonElement = jsonObject.get(statusField);
							if (null != jsonElement) {
								String text = jsonElement.getAsString();
								if (!Strings.isNullOrEmpty(text)) {
									attributes.add(new Attribute(AttributeKey.status, text, origin));
								}
							}

							// maturati(s)
							final HashSet<Maturato> maturati = new HashSet<>();
							jsonElement = jsonObject.get(maturatiField);
							if (null != jsonElement) {
								for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
									JsonElement jsonValoreText = jsonArrayElement.getAsJsonObject().get(maturatiValoreField);
									JsonElement jsonPeriodoText = jsonArrayElement.getAsJsonObject().get(maturatiPeriodoField);
									JsonElement jsonDataDaText = jsonArrayElement.getAsJsonObject().get(maturatiDataDaField);
									JsonElement jsonDataAText = jsonArrayElement.getAsJsonObject().get(maturatiDataAField);
									String text1 = jsonValoreText.getAsString();
									String text2 = jsonPeriodoText.getAsString();
									String text3 = jsonDataDaText.getAsString();
									String text4 = jsonDataAText.getAsString();
									if (!Strings.isNullOrEmpty(text1)) {
										maturati.add(new Maturato(text1, text2, text3, text4 ));										
									}
								}
							}

							// find existing or generate new work id
							final long id;
							final CollectingCode collecting = collectingDatabase.getByCode(code);
							if (null == collecting) {
								id = workDatabase.getNextId();
								// insert collecting code
								collectingDatabase.upsert(new CollectingCode(code, id, weight));
							} else {
								id = collecting.workId;
								if (!BigDecimals.almostEquals(weight, collecting.weight)) {
									// update collecting code
									collecting.weight = weight;
									collectingDatabase.upsert(collecting);
								}
							}

							// merge data from other origin(s)
							if (!overwriteExisting && null != collecting) {
								final Work work = workDatabase.getById(id);
								if (null != work) {
									// merge existing code(s)
									if (null != work.codes) {
										for (Code workCode : work.codes) {
											if (origin != workCode.origin) {
												codes.add(workCode);
											}
										}
									}
									// merge existing title(s)
									if (null != work.titles) {
										for (Title workTitle : work.titles) {
											if (origin != workTitle.origin) {
												titles.add(workTitle);
											}
										}
									}
									// merge existing artist(s)
									if (null != work.artists) {
										for (Artist workArtist : work.artists) {
											if (origin != workArtist.origin) {
												artists.add(workArtist);
											}
										}
									}
									// merge existing attribute(s)
									if (null != work.attributes) {
										for (Attribute workAttribute : work.attributes) {
											if (origin != workAttribute.origin) {
												attributes.add(workAttribute);
											}
										}
									}
									
									// merge existing maturati(s)
									if (null != work.maturati) {
										for (Maturato workAttribute : work.maturati) {
											maturati.add(workAttribute);
										}
									}
								}
							}

							// insert or replace work
							final Work work = new Work(id, codes, titles, artists, attributes,maturati);
							if (null == workDatabase.upsert(work)) {
								insertedWorks.incrementAndGet();
							} else {
								modifiedWorks.incrementAndGet();
							}
							heartbeat.pump();
						}
					}
					// save last processed
					writeResumeFile(resumeFile, s3FileUrl);
					break;
				} catch (IOException e) {
					logger.error("process", e);
				}
			}
		}
		logger.debug("{} records imported", heartbeat.getTotalPumps());
		logger.debug("import completed in {}",TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		// processing stats
		final JsonObject processingStats = new JsonObject();
		processingStats.addProperty("totalRecords", totalRecords.get());
		processingStats.addProperty("invalidRecords", invalidRecords.get());
		processingStats.addProperty("insertedWorks", insertedWorks.get());
		processingStats.addProperty("modifiedWorks", modifiedWorks.get());
		processingStats.addProperty("modifiedWorks", modifiedWorks.get());
		processingStats.addProperty("jsonParsingDuration",TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		// defrag work database
		startTimeMillis = System.currentTimeMillis();
		logger.debug("work database defrag started");
		workDatabase.defrag();
		logger.debug("work database defrag completed in {}",TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
		processingStats.addProperty("workDatabaseDefragDuration",TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		// defrag collecting code database
		startTimeMillis = System.currentTimeMillis();
		logger.debug("collecting database defrag started");
		collectingDatabase.defrag();
		logger.debug("collecting database defrag completed in {}",TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
		processingStats.addProperty("collectingDatabaseDefragDuration",TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		// output
		output.add("itacaJsonParser", processingStats);
	}

	private List<String> getUnprocessedElasticFileUrls(Map<String, String> processedUrls,String dump) throws IOException {

		final String s3FolderUrl = configuration.getProperty("index_builder.elastic.input.s3_folder_url") + (dump== null?"":dump);
		final Pattern filterRegex = Pattern.compile(configuration.getProperty("index_builder.elastic.input.filter_regex"));

		logger.debug("getUnprocessedElasticFileUrls: s3FolderUrl {}", s3FolderUrl);
		logger.debug("getUnprocessedElasticFileUrls: filterRegex {}", filterRegex);

		// build s3 file(s) list
		final List<S3ObjectSummary> objects = s3.listObjects(new S3.Url(s3FolderUrl));
		final List<String> s3FileUrls = new ArrayList<>(objects.size());
		for (S3ObjectSummary object : objects) {
			final String s3FileUrl = new StringBuilder()
					.append("s3://")
					.append(object.getBucketName())
					.append('/')
					.append(object.getKey())
					.toString();
			if (filterRegex.matcher(s3FileUrl).matches()){
				s3FileUrls.add(s3FileUrl);
			}
		}

		// sort lexicographically
		Collections.sort(s3FileUrls);

		// remove already processed url(s) and deduplicate unprocessed url(s)
		final Set<String> deduplicated = new HashSet<>();
		for (Iterator<String> iterator = s3FileUrls.iterator(); iterator.hasNext(); ) {
			final String s3FileUrl = iterator.next();
			final String s3FileName = s3FileUrl
					.substring(1 + s3FileUrl.lastIndexOf('/'));
			if (processedUrls.containsKey(s3FileName)) {
				iterator.remove();
			} else if (deduplicated.contains(s3FileName)) {
				iterator.remove();
			}
			deduplicated.add(s3FileName);
		}
		return s3FileUrls;
	}

	private Map<String, String> readResumeFile2(File file) throws IOException, ClassNotFoundException {
		final Map<String, String> processedUrls = new ConcurrentHashMap<String, String>();
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line = reader.readLine();
		while (line != null) {
			StringTokenizer et = new StringTokenizer(line,"\t");
			if(et.countTokens()>1){
				String key = et.nextToken();
			    String value = et.nextToken();
			    processedUrls.put(key, value);
			}
			line = reader.readLine();
		}
		reader.close();
		return processedUrls;
	}
}