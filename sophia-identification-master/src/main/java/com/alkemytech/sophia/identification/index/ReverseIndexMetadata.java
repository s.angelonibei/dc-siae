package com.alkemytech.sophia.identification.index;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ReverseIndexMetadata {

	public static final String VERSION = "version";
	
	private final Logger logger = LoggerFactory.getLogger(ReverseIndexMetadata.class);
	
	private final File homeFolder;
	private final Properties properties;
	
	public ReverseIndexMetadata(File homeFolder) {
		super();
		this.homeFolder = homeFolder;
		this.properties = load(homeFolder);
	}

	private Properties load(File folder) {
		final Properties properties = new Properties();
		if (folder.exists()) {
			try {
				final File file = new File(folder, "metadata");
				if (file.exists()) {
					try (final FileInputStream in = new FileInputStream(file)) {
						properties.load(in);					
					}
				}
			} catch (IOException e) {
				logger.error("load", e);
			}
		}
		return properties;
	}
	
	private void store(Properties properties, File folder) throws IOException {
		folder.mkdirs();
		final File file = new File(folder, "metadata");
		try (final FileOutputStream out = new FileOutputStream(file)) {
			properties.store(out, "metadata");
		} 
	}
	
	public String getProperty(String key, String defaultValue) {
		return properties.getProperty(key, defaultValue);
	}
	
	public ReverseIndexMetadata setProperty(String key, String value) {
		properties.setProperty(key, value);
		return this;
	}
	
	public ReverseIndexMetadata commit() throws IOException {
		store(properties, homeFolder);
		return this;
	}

	@Override
	public String toString() {
		return new GsonBuilder().disableHtmlEscaping()
				.create().toJson(properties);
	}

}
