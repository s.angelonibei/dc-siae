package com.alkemytech.sophia.identification.score;

import java.util.Collection;

import com.alkemytech.sophia.commons.util.SplitCharSequence;
import com.alkemytech.sophia.identification.document.ScoredDocument;
import com.alkemytech.sophia.identification.model.Document;
import com.alkemytech.sophia.identification.model.Work;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface DocumentScore {

	public double getScore(SplitCharSequence title, Collection<SplitCharSequence> artists, Document document);
	public ScoredDocument getScore(SplitCharSequence title, Collection<SplitCharSequence> artists,Document document,Work work);	
}
