package com.alkemytech.sophia.identification.guice;

import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class JettyGuiceBootstrap extends GuiceServletContextListener {

	private final Injector injector;

	public JettyGuiceBootstrap(Injector injector) {
		super();
		this.injector = injector;
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.inject.servlet.GuiceServletContextListener#getInjector()
	 */
	@Override
	protected Injector getInjector() {
		return injector;
	}
	
}
