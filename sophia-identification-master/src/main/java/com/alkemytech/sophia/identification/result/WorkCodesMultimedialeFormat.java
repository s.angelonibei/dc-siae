package com.alkemytech.sophia.identification.result;

import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.identification.MatchType;
import com.alkemytech.sophia.identification.collecting.CollectingDatabase;
import com.alkemytech.sophia.identification.document.ScoredDocument;
import com.alkemytech.sophia.identification.model.AttributeKey;
import com.alkemytech.sophia.identification.model.CodeType;
import com.alkemytech.sophia.identification.model.CollectingCode;
import com.alkemytech.sophia.identification.model.Origin;
import com.alkemytech.sophia.identification.model.Text;
import com.alkemytech.sophia.identification.model.Work;
import com.alkemytech.sophia.identification.work.WorkDatabase;
import com.google.inject.Inject;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class WorkCodesMultimedialeFormat implements ConfigurableResultFormat {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final CollectingDatabase collectingDatabase;
	private final WorkDatabase workDatabase;
	
	@Inject
	public WorkCodesMultimedialeFormat(CollectingDatabase collectingDatabase,WorkDatabase workDatabase) {
		this.collectingDatabase = collectingDatabase;
		this.workDatabase = workDatabase;
	}

	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.identification.result.ConfigurableResultFormat#getInstance(java.util.Properties, java.lang.String)
	 */
	@Override
	public ResultFormat getInstance(Properties configuration, String propertyPrefix) {
		
		final char delimiter = configuration
				.getProperty(propertyPrefix + ".delimiter", "|").charAt(0);
		final boolean printScore = "true".equalsIgnoreCase(configuration
				.getProperty(propertyPrefix + ".print_score"));
		final char scoreDelimiter = printScore ? configuration
				.getProperty(propertyPrefix + ".score_delimiter", ":").charAt(0) : '\0';		

		// log config value(s)
		final boolean debug = "true".equalsIgnoreCase(configuration
				.getProperty("identifier.debug", configuration
						.getProperty("default.debug")));
		if (debug) {
			logger.debug("propertyPrefix: {}", propertyPrefix);
			logger.debug("\tdelimiter: {}", new Character(delimiter));
			logger.debug("\tprintScore: {}", printScore);
			logger.debug("\tscoreDelimiter: {}", scoreDelimiter);
		}
				
		return new ResultFormat() {
			@Override
			public String format(List<Text> text, List<ScoredDocument> scoredDocuments, MatchType matchType)
					throws Exception {
				final String layout = printScore ? "%s" + scoreDelimiter + "%.3f" : "%s";
				int count = 0;
				
				final StringBuilder result = new StringBuilder();
				for (ScoredDocument scoredDocument : scoredDocuments) {
					//final CollectingCode collectingCode = collectingDatabase.getByWorkId(scoredDocument.document.workId);
					 final Work work = workDatabase.getById(scoredDocument.document.workId);
					if (null != work) {
						if (count > 0)
							result.append(delimiter);
						result.append(String.format(layout,work.getCodes(CodeType.uuid).get(0), scoredDocument.score));
						count ++;
					}
				}
				return result.toString();
			}

			/*
			 * (non-Javadoc)
			 * @see com.alkemytech.sophia.identification.result.ResultFormat#format(java.util.List, java.util.List, java.util.List, java.util.List, java.util.List, java.util.List, java.util.List, com.alkemytech.sophia.identification.MatchType)
			 */
			@Override
			public String format(List<String> titles, List<String> titleTypes, List<String> artists, List<String> artistTypes, List<String> codes, List<String> codeTypes, List<ScoredDocument> scoredDocuments, MatchType matchType) throws Exception {
				final String layout = printScore ? "%s" + scoreDelimiter + "%.3f" : "%s";
				int count = 0;
				
				final StringBuilder result = new StringBuilder();
				for (ScoredDocument scoredDocument : scoredDocuments) {
					//final CollectingCode collectingCode = collectingDatabase.getByWorkId(scoredDocument.document.workId);
					 final Work work = workDatabase.getById(scoredDocument.document.workId);
					if (null != work) {
						if (count > 0)
							result.append(delimiter);
						result.append(String.format(layout,work.getCodes(CodeType.uuid).get(0), scoredDocument.score));
						count ++;
					}
				}
				return result.toString();
			}
			
		};
		
	}
	
}
