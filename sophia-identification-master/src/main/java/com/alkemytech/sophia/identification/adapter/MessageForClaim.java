package com.alkemytech.sophia.identification.adapter;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQSExtendedClient;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.sqs.McmdbMessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.identification.jdbc.McmdbDAO;
import com.alkemytech.sophia.identification.jdbc.McmdbDataSource;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Properties;


public class MessageForClaim {

    private static final Logger logger = LoggerFactory.getLogger(MessageForClaim.class);

    private static class GuiceModuleExtension extends GuiceModule {

        GuiceModuleExtension(String[] args, String resourceName) {
            super(args, resourceName);
        }

        @Override
        protected void configure() {
            super.configure();
            // amazon service(s)
            bind(S3.class)
                    .toInstance(new S3(configuration));
            bind(SQSExtendedClient.class)
                    .toInstance(new SQSExtendedClient(configuration));
            // data source(s)
            bind(DataSource.class)
                    .annotatedWith(Names.named("MCMDB"))
                    .to(McmdbDataSource.class)
                    .asEagerSingleton();
            // data access object(s)
            bind(McmdbDAO.class)
                    .asEagerSingleton();
            // other binding(s)
            bind(MessageDeduplicator.class)
                    .to(McmdbMessageDeduplicator.class);
            // self
            bind(MessageForClaim.class)
                    .asEagerSingleton();
        }

    }

    public static void main(String[] args) {
        try {
            final MessageForClaim instance = Guice
                    .createInjector(new GuiceModuleExtension(args,
                            "/mm-response-adapter.properties"))
                    .getInstance(MessageForClaim.class)
                    .startup();
            try {
                BufferedReader reader = new BufferedReader(new FileReader("./dsr.txt"));
                String line;
                while ((line = reader.readLine()) != null)
                {
                    if(!line.equals("")){
                        logger.info("sending pricing message for:" + line.trim()+"|");
                        instance.getMessageForClaim(line.trim());
                    }

                }
                reader.close();

            } finally {
                instance.shutdown();
            }
        } catch (Throwable e) {
            logger.error("main", e);
        } finally {
            System.exit(0);
        }
    }

    private final Properties configuration;
    private final Charset charset;
    private final Gson gson;
    private final S3 s3;
    private final SQSExtendedClient sqs;
    private final McmdbDAO dao;
    private final MessageDeduplicator messageDeduplicator;



    @Inject
    protected MessageForClaim(@Named("configuration") Properties configuration,
                              @Named("charset") Charset charset, Gson gson, S3 s3, SQSExtendedClient sqs,
                              McmdbDAO dao, MessageDeduplicator messageDeduplicator) {
        super();
        this.configuration = configuration;
        this.charset = charset;
        this.gson = gson;
        this.s3 = s3;
        this.sqs = sqs;
        this.dao = dao;
        this.messageDeduplicator = messageDeduplicator;
    }

    public MessageForClaim startup() throws Exception {
        if ("true".equalsIgnoreCase(configuration.getProperty("response_adapter.locked", "true"))) {
            throw new IllegalStateException("application locked");
        }
        s3.startup();
        sqs.startup();
        return this;
    }

    public MessageForClaim shutdown() {
        sqs.shutdown();
        s3.shutdown();
        return this;
    }



    private void pollCompletedQueue() throws Exception {

        final String wsDsrMetadata = configuration.getProperty("response_adapter.ws.dsr_metadata");

        // sqs message pump(s)
        final SqsMessagePump newClaimsSqsMessagePump = new SqsMessagePump(sqs,
                configuration, "response_adapter.sqs.new_claim_queue");
        final SqsMessagePump completedSqsMessagePump = new SqsMessagePump(sqs,
                configuration, "response_adapter.completed.sqs");
        completedSqsMessagePump.pollingLoop(messageDeduplicator, new SqsMessagePump.Consumer() {

            @Override
            public JsonObject getStartedMessagePayload(JsonObject message) {
                return null;
            }

            @Override
            public JsonObject getFailedMessagePayload(JsonObject message) {
                return null;
            }

            @Override
            public JsonObject getCompletedMessagePayload(JsonObject message) {
                return null;
            }

            @Override
            public boolean consumeMessage(JsonObject message) {return false;}

        });

    }
    private JsonObject getMessageForClaim(String idDsr)throws Exception{

        // sqs message pump(s)
        final SqsMessagePump newClaimsSqsMessagePump = new SqsMessagePump(sqs,
                configuration, "response_adapter.sqs.new_claim_queue");
        final String wsDsrMetadata = configuration.getProperty("response_adapter.ws.dsr_metadata");
        JsonObject wsClaim = parseJSonFromWs(wsDsrMetadata.replace("{idDsr}", idDsr));


        JsonObject body = new JsonObject();
        body.addProperty("idDsr", idDsr);
        body.addProperty("dsp", GsonUtils.getAsString(wsClaim,"dspFtpPath"));
        body.addProperty("dspCode", GsonUtils.getAsString(wsClaim,"dspCode"));
        body.addProperty("sliceDspCode", GsonUtils.getAsString(wsClaim,"sliceDspCode"));
        body.addProperty("original_price_basis_currency", GsonUtils.getAsString(wsClaim,"dsrCurrency"));
        body.addProperty("notEncoded", GsonUtils.getAsString(wsClaim,"notEncoded"));
        body.addProperty("country", GsonUtils.getAsString(wsClaim,"country"));
        body.addProperty("territory", GsonUtils.getAsString(wsClaim,"territory"));
        body.addProperty("service_type", GsonUtils.getAsString(wsClaim,"serviceType"));
        body.addProperty("ccidVersion", GsonUtils.getAsString(wsClaim,"ccidVersion"));
        body.addProperty("applied_tariff", GsonUtils.getAsString(wsClaim,"appliedTariff"));
        body.addProperty("trading_brand", GsonUtils.getAsString(wsClaim,"tradingBrand"));
        body.addProperty("encoded", GsonUtils.getAsString(wsClaim,"encoded"));
        body.addProperty("periodStartDate", GsonUtils.getAsString(wsClaim,"periodStartDate"));
        body.addProperty("periodEndDate", GsonUtils.getAsString(wsClaim,"periodEndDate"));
        body.addProperty("encodedProvisional", GsonUtils.getAsString(wsClaim,"encodedProvisional"));
        body.addProperty("use_type", GsonUtils.getAsString(wsClaim,"useType"));
        body.addProperty("conversion_rate", 1 / GsonUtils.getAsDouble(wsClaim,"conversionRate", 1) * 100000);
        body.addProperty("sender", GsonUtils.getAsString(wsClaim,"sender"));
        body.addProperty("receiver", GsonUtils.getAsString(wsClaim,"receiver"));
        body.addProperty("ccid_id", GsonUtils.getAsString(wsClaim,"headerCcidID"));
        body.addProperty("work_code_type", GsonUtils.getAsString(wsClaim,"workCodeType"));
        body.addProperty("split_perf", GsonUtils.getAsString(wsClaim,"demSplit"));
        body.addProperty("split_mech", GsonUtils.getAsString(wsClaim,"drmSplit"));
        body.addProperty("baseRefId", GsonUtils.getAsString(wsClaim,"ccidId"));
        body.addProperty("periodType", GsonUtils.getAsString(wsClaim,"periodType"));
        body.addProperty("period", GsonUtils.getAsString(wsClaim,"period"));
        body.addProperty("idUtilizationType", GsonUtils.getAsString(wsClaim,"idUtilizationType"));
        body.addProperty("sliceMonth", GsonUtils.getAsString(wsClaim,"sliceMonth"));
        body.addProperty("businessOffer", GsonUtils.getAsString(wsClaim,"commercialOffer"));
        body.addProperty("commercialOffer", GsonUtils.getAsString(wsClaim,"commercialOffer"));
        body.addProperty("claimPeriodType", GsonUtils.getAsString(wsClaim,"claimPeriodType"));
        body.addProperty("claimUtilizationType", GsonUtils.getAsString(wsClaim,"claimUtilizationType"));
        body.addProperty("PARAM_APPLIED_TARIFF", GsonUtils.getAsString(wsClaim,"APPLIED_TARIFF"));
        body.addProperty("PARAM_AUX", GsonUtils.getAsString(wsClaim,"AUX"));
        body.addProperty("year", GsonUtils.getAsString(wsClaim,"year"));
        body.addProperty("month", GsonUtils.getAsString(wsClaim,"month"));
        body.add("tenant", GsonUtils.getAsJsonObject(wsClaim, "tenant"));
        body.add("mandators", GsonUtils.getAsJsonArray(wsClaim, "mandators"));
        body.addProperty("decisionTableUrl", GsonUtils.getAsString(wsClaim, "dtableS3Url"));
        body.addProperty("priceModel", GsonUtils.getAsString(wsClaim, "idUtilizationType"));
        body.addProperty("totalSales", GsonUtils.getAsString(wsClaim, "salesLineNum"));
        body.addProperty("totalAmount", GsonUtils.getAsString(wsClaim, "totalValue"));
        body.addProperty("totalSubscriptions", GsonUtils.getAsString(wsClaim, "subscriptionsNum"));

        //bug currency claim ORIG
        body.addProperty("currency", GsonUtils.getAsString(wsClaim, "currency"));
        //bug INC0010083
//                    body.addProperty("royalty_currency", GsonUtils.getAsString(wsClaim, "currency"));

        if (GsonUtils.getAsString(wsClaim, "ccidCurrency").equals("ORIG"))
            body.addProperty("royalty_currency", GsonUtils.getAsString(wsClaim, "dsrCurrency"));
        else
            body.addProperty("royalty_currency", GsonUtils.getAsString(wsClaim, "ccidCurrency"));


        newClaimsSqsMessagePump.sendToProcessMessage(body, false);

        return body;
    }

    private JsonObject parseJSonFromWs(String urlString) throws Exception{
//        URL url = new URL("http://gestioneutilizzazioni-sviluppo.alkemytech.it/cruscottoUtilizzazioni/rest/dsrMetadata/extractDsrMetadata?dsrName=Report_YouTube_bmat_IT_201704_20170509002453_masterlist&dsp=youtube");
        logger.debug("invoking ws {}", urlString);
        URL url = new URL(urlString);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");


        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer content = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();



        return GsonUtils.fromJson(content.toString(),JsonObject.class);
    }


}

