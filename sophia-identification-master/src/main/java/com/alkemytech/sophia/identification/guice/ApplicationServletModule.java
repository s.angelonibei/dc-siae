package com.alkemytech.sophia.identification.guice;

import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.identification.servlet.GsonFilter;
import com.alkemytech.sophia.identification.servlet.HealthServlet;
import com.alkemytech.sophia.identification.servlet.NetmaskFilter;
import com.alkemytech.sophia.identification.servlet.SearchServlet;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.google.inject.servlet.ServletModule;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ApplicationServletModule extends ServletModule {

	/*
	 * (non-Javadoc)
	 * @see com.google.inject.servlet.ServletModule#configureServlets()
	 */
	@Override
	protected void configureServlets() {
		
		// servlet filter(s)
		
		bind(NetmaskFilter.class)
			.in(Scopes.SINGLETON);
		filter("/*")
			.through(NetmaskFilter.class);

		bind(GsonFilter.class).in(Scopes.SINGLETON);
		filter("/search").through(GsonFilter.class);

		// servlet(s)
		

		bind(SearchServlet.class)
			.in(Scopes.SINGLETON);		
		serve("/search")
			.with(SearchServlet.class);
		
		bind(HealthServlet.class)
		.in(Scopes.SINGLETON);		
		serve("/status")
		.with(HealthServlet.class);


	}
	
	@Provides
	@Singleton
	@Named("configurations_cache")
	public Cache<String, Properties> getConfigCache(@Named("configuration") Properties configuration) {
		final long maximumSize = TextUtils.parseLongSize(configuration
				.getProperty("servlet.cache.config.max_size", "1K"));
		final long expireAfter = TextUtils.parseLongDuration(configuration
				.getProperty("servlet.cache.config.expire_after", "5m"));
		return CacheBuilder.newBuilder()
				.maximumSize(maximumSize)
				.expireAfterWrite(expireAfter, TimeUnit.MILLISECONDS)
				.build();
	}
	
}
