package com.alkemytech.sophia.identification.model;

import com.alkemytech.sophia.commons.util.SplitCharSequence;
import com.alkemytech.sophia.commons.util.TokenizableText;
import com.google.gson.GsonBuilder;
import com.sleepycat.persist.model.Persistent;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@Persistent
public class TextArtist extends Text{

	public String ipi;

	public TextArtist() {
		super();
	}

	public TextArtist(String text, String ipi, int type) {
		super();
		this.text = text;
		this.type = type;
		this.ipi = ipi;
	}

	@Override
	public int hashCode() {
		int result = null == text ? 31 : 31 + text.hashCode();
		return 31 * result + type;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (null == obj) {
			return false;
		} else if (getClass() != obj.getClass()) {
			return false;
		}
		TextArtist other = (TextArtist) obj;
		if (null == text) {
			if (null != other.text) {
				return false;
			}
		} else if (!text.equals(other.text)) {
			return false;
		}
		if (type != other.type) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return new GsonBuilder()
				.disableHtmlEscaping()
				.create()
				.toJson(this);
	}
	public TokenizableText toTokenizableText() {
		return new SplitCharSequence(text);
	}

}
