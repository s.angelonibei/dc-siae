package com.alkemytech.sophia.identification.servlet;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Properties;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.net.CidrNetmask;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class NetmaskFilter implements Filter {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Properties configuration;

	private ArrayList<CidrNetmask> whitelist;
	private ArrayList<CidrNetmask> blacklist;
	
	@Inject
	protected NetmaskFilter(@Named("configuration") Properties configuration) {
		super();
		this.configuration = configuration;
		this.whitelist = new ArrayList<>();
		this.blacklist = new ArrayList<>();
	}
	
	/*
	 * (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig config) throws ServletException {
		
		// whitelist netmask(s)
		whitelist.clear();
		for (String netmask : TextUtils.split(configuration
				.getProperty("servlet.netmask_filter.whitelist", ""), ",")) {
			if (!Strings.isNullOrEmpty(netmask)) {
				logger.debug("whitelist netmask {}", netmask);
				try {
					final CidrNetmask cidrNetmask = new CidrNetmask(netmask);
					whitelist.add(cidrNetmask);
					logger.debug("whitelist cidr netmask {}", cidrNetmask);
				} catch (UnknownHostException e) {
					logger.error("init", e);
				}
			}
		}
		
		// blacklist netmask(s)
		blacklist.clear();
		for (String netmask : TextUtils.split(configuration
				.getProperty("servlet.netmask_filter.blacklist", ""), ",")) {
			if (!Strings.isNullOrEmpty(netmask)) {
				logger.debug("blacklist netmask {}", netmask);
				try {
					final CidrNetmask cidrNetmask = new CidrNetmask(netmask);
					blacklist.add(cidrNetmask);
					logger.debug("blacklist cidr netmask {}", cidrNetmask);
				} catch (UnknownHostException e) {
					logger.error("init", e);
				}
			}
		}
		
	}

	/*
	 * (non-Javadoc)
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
		
	}
	
	/*
	 * (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		// remote IP address
		final String remoteAddr = request.getRemoteAddr();

		// check whitelist
		boolean whitelisted = false;
		for (CidrNetmask netmask : whitelist) {
			if (netmask.isInRange(remoteAddr)) {
				whitelisted = true;
				break;
			}
		}

		// check blacklist
		if (!whitelisted) {
			for (CidrNetmask netmask : blacklist) {
				if (netmask.isInRange(remoteAddr))
					throw new ServletException("request address blocked");
			}
		}

		// pass on request
		chain.doFilter(request, response);

	}
	
}
