package com.alkemytech.sophia.identification.result;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ResultFormats {
	
	private static final Map<String, Provider<ConfigurableResultFormat>> providers = new ConcurrentHashMap<>();

	public static interface Observer {
		public void notify(String formatName, Class<? extends ConfigurableResultFormat> formatClass);
	}
	
	public static ResultFormat getInstance(Properties configuration, String propertyPrefix) {
		return providers.get(configuration
				.getProperty(propertyPrefix + ".class_name"))
				.get()
				.getInstance(configuration, propertyPrefix);
	}
	
	public static void loopOnFormats(Observer observer) {
		observer.notify("match_type_format", MatchTypeFormat.class);
		observer.notify("work_codes_format", WorkCodesFormat.class);
		observer.notify("json_with_scores_format", JsonWithScoresFormat.class);
		observer.notify("rest_with_scores_format", RestServiceFormat.class);
		observer.notify("rest_multimediale_with_scores_format", RestMultimedialeServiceFormat.class);
		observer.notify("work_codes_multimediale_format", WorkCodesMultimedialeFormat.class);
	}
	
	@Inject
	protected ResultFormats(@Named("match_type_format") Provider<ConfigurableResultFormat> matchTypeFormatProvider,
			@Named("work_codes_format") Provider<ConfigurableResultFormat> workCodesFormatProvider,
			@Named("json_with_scores_format") Provider<ConfigurableResultFormat> jsonWithScoresFormatProvider,
			@Named("rest_with_scores_format") Provider<ConfigurableResultFormat> restWithScoresFormatProvider,
			@Named("rest_multimediale_with_scores_format") Provider<ConfigurableResultFormat> restMultimedialeWithScoresFormatProvider,
			@Named("work_codes_multimediale_format") Provider<ConfigurableResultFormat> workCodesMultimedialeFormatProvider
			) {
		super();
		providers.put(MatchTypeFormat.class.getName(), matchTypeFormatProvider);
		providers.put(WorkCodesFormat.class.getName(), workCodesFormatProvider);
		providers.put(JsonWithScoresFormat.class.getName(), jsonWithScoresFormatProvider);
		providers.put(RestServiceFormat.class.getName(), restWithScoresFormatProvider);
		providers.put(RestMultimedialeServiceFormat.class.getName(), restMultimedialeWithScoresFormatProvider);
		providers.put(WorkCodesMultimedialeFormat.class.getName(), workCodesMultimedialeFormatProvider);
	}
	
}
