package com.alkemytech.sophia.identification.adapter;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.io.CompressionAwareFileInputStream;
import com.alkemytech.sophia.commons.sqs.McmdbMessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.identification.jdbc.McmdbDAO;
import com.alkemytech.sophia.identification.jdbc.McmdbDataSource;
import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.*;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPOutputStream;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class MassiveResponseAdapter {

	private static final Logger logger = LoggerFactory.getLogger(MassiveResponseAdapter.class);

	private static class GuiceModuleExtension extends GuiceModule {

		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.toInstance(new S3(configuration));
			bind(SQS.class)
				.toInstance(new SQS(configuration));
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("MCMDB"))
				.to(McmdbDataSource.class)
				.asEagerSingleton();
			// data access object(s)
			bind(McmdbDAO.class)
				.asEagerSingleton();
			// other binding(s)
			bind(MessageDeduplicator.class)
				.to(McmdbMessageDeduplicator.class);
			// self
			bind(MassiveResponseAdapter.class)
				.asEagerSingleton();
		}

	}

	public static void main(String[] args) {
		try {
			final MassiveResponseAdapter instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/massive-response-adapter.properties"))
					.getInstance(MassiveResponseAdapter.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Properties configuration;
	private final Charset charset;
	private final Gson gson;
	private final S3 s3;
	private final SQS sqs;
	private final McmdbDAO dao;
	private final MessageDeduplicator messageDeduplicator;

	@Inject
	protected MassiveResponseAdapter(@Named("configuration") Properties configuration,
                                     @Named("charset") Charset charset, Gson gson, S3 s3, SQS sqs,
                                     McmdbDAO dao, MessageDeduplicator messageDeduplicator) {
		super();
		this.configuration = configuration;
		this.charset = charset;
		this.gson = gson;
		this.s3 = s3;
		this.sqs = sqs;
		this.dao = dao;
		this.messageDeduplicator = messageDeduplicator;
	}
	
	public MassiveResponseAdapter startup() throws Exception {
		if ("true".equalsIgnoreCase(configuration.getProperty("response_adapter.locked", "true"))) {
			throw new IllegalStateException("application locked");
		}
		s3.startup();
		sqs.startup();
		return this;
	}

	public MassiveResponseAdapter shutdown() {
		sqs.shutdown();
		s3.shutdown();
		return this;
	}
	
	private MassiveResponseAdapter process(String[] args) throws Exception {
		
		final int bindPort = Integer.parseInt(configuration
				.getProperty("response_adapter.bind_port", configuration
						.getProperty("default.bind_port", "0")));

		// bind lock tcp port
		try (final ServerSocket socket = new ServerSocket(bindPort)) {
			logger.info("socket bound to {}", socket.getLocalSocketAddress());
			
			// sqs polling thread(s)
			final ExecutorService executorService = Executors.newCachedThreadPool();
			
			// completed notification(s)
			executorService.execute(new Runnable() {
				
				@Override
				public void run() {
					try {
						pollCompletedQueue();
					} catch (Exception e) {
						logger.error("polling completed queue", e);
					}
				}
				
			});
			
			// failed notification(s)
			executorService.execute(new Runnable() {
				
				@Override
				public void run() {
					try {
						pollFailedQueue();					
					} catch (Exception e) {
						logger.error("polling failed queue", e);
					}
				}
				
			});

			// wait sqs polling thread(s) termination
			executorService.shutdown();
			while (!executorService.awaitTermination(1L, TimeUnit.MINUTES))
				logger.debug("waiting sqs polling thread(s) termination...");
			
		}

		return this;
	}

	public void parseOutputFile(File outputFile, String compression, File finalFile) throws Exception {

		final char delimiter = configuration
				.getProperty("response_adapter.file.delimiter").charAt(0);
		final int columnResultJson = Integer.parseInt(configuration
				.getProperty("response_adapter.file.column.in.result_json"));

		String[] columnOrder1 = configuration
				.getProperty("response_adapter.file.column.in_to_out_order").split(",");
		String[] columnOrder2 = configuration
				.getProperty("response_adapter.file.column.json_to_out_order").split(",");
		int rowsToSkip = Integer.parseInt(configuration
				.getProperty("response_adapter.file.rows_to_skip", "1"));

		// heart beat
		final HeartBeat heartbeat = HeartBeat.constant("record", Integer.parseInt(configuration
				.getProperty("response_adapter.heartbeat", configuration.getProperty("default.heartbeat", "1000"))));

		// parse local file
		try (final CompressionAwareFileInputStream inputStream = new CompressionAwareFileInputStream(outputFile, compression);
			 final InputStreamReader inputStreamReader = new InputStreamReader(inputStream, charset);
			 final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
			 final CSVParser csvParser = new CSVParser(bufferedReader, CSVFormat.newFormat(delimiter)
						.withIgnoreEmptyLines()
						.withIgnoreSurroundingSpaces()
						.withQuoteMode(QuoteMode.MINIMAL)
						.withQuote('"'));
			 final FileWriter fw = new FileWriter(finalFile, true);
			 final BufferedWriter bw = new BufferedWriter(fw);
			 final PrintWriter outCsv = new PrintWriter(bw)) {

			final Iterator<CSVRecord> csvIterator = csvParser.iterator();

			// skip initial row(s)
			for ( ; rowsToSkip > 0 && csvIterator.hasNext(); rowsToSkip --) {
				final CSVRecord record = csvIterator.next();
				logger.debug("record skipped {}", record);
			}

			// loop on row(s)
			while (csvIterator.hasNext()) {
				final CSVRecord csvRecord = csvIterator.next();


				final String resultJson = csvRecord.get(columnResultJson);
				// decode json
				logger.debug("resultJson: {}", resultJson);
				final JsonArray resultArray = gson.fromJson(resultJson, JsonArray.class);

				int numRecordToDuplicate = resultArray.size();



				for(int i=0;i<numRecordToDuplicate; i++){
					final JsonObject currentcandidate = GsonUtils.getAsJsonObject(resultArray, i);

					List<String> listInputField = new ArrayList<>();
					//add input column
					for (String s : columnOrder1) {
						listInputField.add(csvRecord.get(Integer.parseInt(s)));
					}
					//add numero risultati
					listInputField.add(numRecordToDuplicate+"");
					//add progressivo
					listInputField.add((i+1)+"");


					//add json properties
					for (String s : columnOrder2) {
						String c = GsonUtils.getAsString(currentcandidate, s);
						listInputField.add(c==null?"" : c );
					}

					//save to csv
					outCsv.println(Joiner.on(delimiter).join(listInputField));
				}


				heartbeat.pump();
			}
			outCsv.flush();
			
		}
		
	}

	private static void compressGzipFile(File file, File gzipFile) {
		try {
			FileInputStream fis = new FileInputStream(file);
			FileOutputStream fos = new FileOutputStream(gzipFile);
			GZIPOutputStream gzipOS = new GZIPOutputStream(fos);
			byte[] buffer = new byte[1024];
			int len;
			while((len=fis.read(buffer)) != -1){
				gzipOS.write(buffer, 0, len);
			}
			//close resources
			gzipOS.close();
			fos.close();
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void pollCompletedQueue() throws Exception {
		
		// config parameter(s)
		final String TEMP_SUFFIX = "__TMP";
		final File workingDirectory = new File(configuration
				.getProperty("response_adapter.working_directory"));
		workingDirectory.mkdirs();
		final boolean deleteInputUrls = "true".equalsIgnoreCase(configuration
				.getProperty("response_adapter.delete_input_urls", "true"));
		final boolean deleteOutputUrls = "true".equalsIgnoreCase(configuration
				.getProperty("response_adapter.delete_output_urls", "true"));
		
		// sqs message pump(s)
		final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs,
				configuration, "response_adapter.sqs");
		final SqsMessagePump completedSqsMessagePump = new SqsMessagePump(sqs,
				configuration, "response_adapter.completed.sqs");
		completedSqsMessagePump.pollingLoop(messageDeduplicator, new SqsMessagePump.Consumer() {
			
			@Override
			public JsonObject getStartedMessagePayload(JsonObject message) {
				return null;
			}
			
			@Override
			public JsonObject getFailedMessagePayload(JsonObject message) {
				return null;
			}
			
			@Override
			public JsonObject getCompletedMessagePayload(JsonObject message) {
				return null;
			}
			
			@Override
			public boolean consumeMessage(JsonObject message) {
				
				// parse completed json message
				final JsonObject input = GsonUtils
						.getAsJsonObject(message, "input");
				final JsonObject output = GsonUtils
						.getAsJsonObject(message, "output");
				final JsonArray items = GsonUtils
						.getAsJsonArray(output, "items");
				final String compression = GsonUtils
						.getAsString(GsonUtils
							.getAsJsonObject(input, "body"), "compression");
				JsonObject error = null;

				// send started message
				sqsMessagePump.sendStartedMessage(input,
						SqsMessageHelper.formatContext(), false);
				
				logger.info("message is {}",input.toString());
				
				final JsonObject header = GsonUtils
						.getAsJsonObject(input, "header");
				
				final String uuid = getUuid(input);
				//final String uuid = GsonUtils.getAsString(header, "uuid");

				String token =uuid;
				
				// statement paramter(s)
				final Map<String, String> parameters = new HashMap<>();
				parameters.put("token", token);

				String outputFileString = null;
				S3.Url url = null;
				try {
					List<Map<String, String>> maps = dao.executeQuery("response_adapter.sql.select_codifica_massiva", parameters);
					outputFileString = maps.get(0).get("FILE_NAME"); //<--- from DB
					url = new S3.Url("s3:/"+maps.get(0).get("FILE_OUTPUT")); //<--- from DB

				} catch (SQLException e) {
					logger.error("error getting info massive by token "+ token,e);
					return true;
				}
				if(outputFileString == null || url == null){
					logger.error("error getting info massive by token "+ token);
					return true;
				}
				File outputFile = new File(outputFileString+ TEMP_SUFFIX);


				List<File> listOutPutFile = new ArrayList<File>();

				for (JsonElement element : items) {
					final JsonObject item = element.getAsJsonObject();
					// delete input file
//					if (deleteInputUrls) {
//						final String inputUrl = GsonUtils
//								.getAsString(item, "inputUrl");
//						logger.debug("deleting input file: {}", inputUrl);
//						if (!s3.delete(new S3.Url(inputUrl))) {
//							logger.warn("error input deleting file: {}", inputUrl);
//						}
//					}
					// download, parse & delete output file
					final String outputUrl = GsonUtils
							.getAsString(item, "outputUrl");
					final File localFile = new File(workingDirectory,
							outputUrl.substring(1 + outputUrl.lastIndexOf('/')));
					localFile.deleteOnExit();
					listOutPutFile.add(localFile);
					logger.debug("downloading output file: {}", outputUrl);
					if (s3.download(new S3.Url(outputUrl), localFile)) {
						//nothing to do
					} else {
						logger.warn("error downloading output file: {}", outputUrl);
						error = SqsMessageHelper.formatError("0", String
								.format("error downloading output file: %s", outputUrl));
						break;
					}

//					if (deleteOutputUrls) {
//						logger.debug("deleting output file: {}", outputUrl);
//						if (!s3.delete(new S3.Url(outputUrl))) {
//							logger.warn("error deleting output file: {}", outputUrl);
//						}
//					}
				}


				String headerCsv = Joiner.on(";").join(configuration.getProperty("response_adapter.file.column.header").split(","));
				headerCsv += "\r\n";
				try {
					Files.write(Paths.get(outputFile.toURI()),headerCsv.getBytes());
				} catch (IOException e) {
					logger.warn("error processing header{}", headerCsv);
					error = SqsMessageHelper.formatError("0", String
							.format("error processing header: %s", headerCsv));
				}



				for (File file : listOutPutFile) {
					//1 -process file
					try {
						parseOutputFile(file, compression, outputFile);
						//2 - delete
//						file.delete();
					}catch (Exception e){
						logger.warn("error processing output file: {}", file.getName());
						error = SqsMessageHelper.formatError("0", String
								.format("error processing output file: %s", file.getName()));
						logger.error("", e);
						break;
					}

				}
				File outputFileGzip = new File(outputFileString);
				compressGzipFile(outputFile, outputFileGzip);

// insert into perf_codifica_massiva

				// send completed/failed message
				if (null == error){
					//upload local file to s3
					s3.upload(url, outputFileGzip);

					int count = 0;
					try {
						count = dao.executeUpdate("response_adapter.sql.update_success_codifica_massiva", parameters);
						sqsMessagePump.sendCompletedMessage(input, output, false);
					} catch (SQLException e) {
						logger.error("", e);
					}
					if (count < 1) {
						logger.warn("zero row(s) inserted executing statement: response_adapter.sql.update_success_codifica_massiva");
					}




				} else {
					sqsMessagePump.sendFailedMessage(input, error, false);
				}				
				
				return false;
			}
			
		});
		
	}

	private void pollFailedQueue() throws Exception {
		
		// configuration
		final boolean deleteInputUrls = "true".equalsIgnoreCase(configuration
				.getProperty("response_adapter.delete_input_urls", "true"));
		final boolean deleteOutputUrls = "true".equalsIgnoreCase(configuration
				.getProperty("response_adapter.delete_output_urls", "true"));

		// sqs message pump(s)
		final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs,
				configuration, "response_adapter.sqs");
		final SqsMessagePump failedSqsMessagePump = new SqsMessagePump(sqs,
				configuration, "response_adapter.failed.sqs");
		failedSqsMessagePump.pollingLoop(messageDeduplicator, new SqsMessagePump.Consumer() {
			
			@Override
			public JsonObject getStartedMessagePayload(JsonObject message) {
				return null;
			}
			
			@Override
			public JsonObject getFailedMessagePayload(JsonObject message) {
				return null;
			}
			
			@Override
			public JsonObject getCompletedMessagePayload(JsonObject message) {
				return null;
			}
			
			@Override
			public boolean consumeMessage(JsonObject message) {

				// parse failed json message
				final JsonObject input = GsonUtils
						.getAsJsonObject(message, "input");
				final JsonArray items = GsonUtils
						.getAsJsonArray(GsonUtils
							.getAsJsonObject(input, "body"), "items");
				final JsonObject error = GsonUtils
						.getAsJsonObject(message, "error");

				// send started message
				sqsMessagePump.sendStartedMessage(input,
						SqsMessageHelper.formatContext(), false);

				final String token = getUuid(input);

				// statement paramter(s)
				final Map<String, String> parameters = new HashMap<>();
				parameters.put("token", token);
				parameters.put("message", error.toString().substring(0, 256-3) + "...");
				
				// insert into perf_codifica_massiva
				if (!Strings.isNullOrEmpty(token)) {				
					int count = 0;
					try {
						count = dao.executeUpdate("response_adapter.sql.update_failed_codifica_massiva", parameters);
					} catch (SQLException e) {
						logger.error("", e);
					}
					if (count < 1) {
						logger.warn("zero row(s) inserted executing statement: response_adapter.sql.update_failed_codifica_massiva");
					}
				}				
				
				// send failed message
				sqsMessagePump.sendFailedMessage(input, error, false);
				
				return true;

			}
			
		});

	}

	private String getUuid(JsonObject input) {
		JsonObject body = GsonUtils.getAsJsonObject(input, "body");
		JsonObject input1 = GsonUtils.getAsJsonObject(body, "input");
		JsonObject header = GsonUtils.getAsJsonObject(input1, "header");
		return GsonUtils.getAsString(header, "uuid");
	}

}

