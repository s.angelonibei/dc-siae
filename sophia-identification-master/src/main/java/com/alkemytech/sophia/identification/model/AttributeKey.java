package com.alkemytech.sophia.identification.model;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface AttributeKey {

	public static final int undefined = 0;
	public static final int flag_pd = 1;
	public static final int flag_el = 2;
	public static final int ambito = 3;
	public static final int rilevanza = 4;
	public static final int accrued = 10; // maturato					type: decimal	operators: == != > >= < <=
	public static final int risk = 13;
	public static final int status = 11;
	public static final int irregularity = 12;

	public static final int codice_siae = 20;
	public static final int codice_ada = 21;
	//public static final int codice_uuid = 22;

}
