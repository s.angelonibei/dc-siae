package com.alkemytech.sophia.identification.model;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class Category {

	public static final int undefined = 0;
	public static final int original = 1;
	public static final int alternative = 2;

	public static int valueOf(String titleType) {
		if ("original".equals(titleType)) {
			return original;
		} else if ("alternative".equals(titleType)) {
			return alternative;
		}
		return undefined;
	}
	
	public static String toString(int titleType) {
		switch (titleType) {
		case original: return "original";
		case alternative: return "alternative";
		default: return "undefined";
		}
	}
	
}
