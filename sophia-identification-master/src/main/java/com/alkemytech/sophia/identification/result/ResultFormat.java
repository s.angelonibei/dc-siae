package com.alkemytech.sophia.identification.result;

import java.util.List;

import com.alkemytech.sophia.identification.MatchType;
import com.alkemytech.sophia.identification.document.ScoredDocument;
import com.alkemytech.sophia.identification.model.Text;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface ResultFormat {
	
	public String format(List<String> titles, List<String> titleTypes, List<String> artists, List<String> artistTypes, List<String> codes, List<String> codeTypes, List<ScoredDocument> scoredDocuments, MatchType matchType) throws Exception;
	public String format(List<Text> text,  List<ScoredDocument> scoredDocuments, MatchType matchType) throws Exception;
	
}
