package com.alkemytech.sophia.identification.adapter;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQSExtendedClient;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.sqs.McmdbMessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.identification.jdbc.McmdbDAO;
import com.alkemytech.sophia.identification.jdbc.McmdbDataSource;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ServerSocket;
import java.net.URL;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class MMResponseAdapter {

    private static final Logger logger = LoggerFactory.getLogger(MMResponseAdapter.class);

    private static class GuiceModuleExtension extends GuiceModule {

        GuiceModuleExtension(String[] args, String resourceName) {
            super(args, resourceName);
        }

        @Override
        protected void configure() {
            super.configure();
            // amazon service(s)
            bind(S3.class)
                    .toInstance(new S3(configuration));
            bind(SQSExtendedClient.class)
                    .toInstance(new SQSExtendedClient(configuration));
            // data source(s)
            bind(DataSource.class)
                    .annotatedWith(Names.named("MCMDB"))
                    .to(McmdbDataSource.class)
                    .asEagerSingleton();
            // data access object(s)
            bind(McmdbDAO.class)
                    .asEagerSingleton();
            // other binding(s)
            bind(MessageDeduplicator.class)
                    .to(McmdbMessageDeduplicator.class);
            // self
            bind(MMResponseAdapter.class)
                    .asEagerSingleton();
        }

    }

    public static void main(String[] args) {
        try {
            final MMResponseAdapter instance = Guice
                    .createInjector(new GuiceModuleExtension(args,
                            "/mm-response-adapter.properties"))
                    .getInstance(MMResponseAdapter.class)
                    .startup();
            try {
                instance.process(args);
            } finally {
                instance.shutdown();
            }
        } catch (Throwable e) {
            logger.error("main", e);
        } finally {
            System.exit(0);
        }
    }

    private final Properties configuration;
    private final Charset charset;
    private final Gson gson;
    private final S3 s3;
    private final SQSExtendedClient sqs;
    private final McmdbDAO dao;
    private final MessageDeduplicator messageDeduplicator;


    @Inject
    protected MMResponseAdapter(@Named("configuration") Properties configuration,
                                @Named("charset") Charset charset, Gson gson, S3 s3, SQSExtendedClient sqs,
                                McmdbDAO dao, MessageDeduplicator messageDeduplicator) {
        super();
        this.configuration = configuration;
        this.charset = charset;
        this.gson = gson;
        this.s3 = s3;
        this.sqs = sqs;
        this.dao = dao;
        this.messageDeduplicator = messageDeduplicator;
    }

    public MMResponseAdapter startup() throws Exception {
        if ("true".equalsIgnoreCase(configuration.getProperty("response_adapter.locked", "true"))) {
            throw new IllegalStateException("application locked");
        }
        s3.startup();
        sqs.startup();
        return this;
    }

    public MMResponseAdapter shutdown() {
        sqs.shutdown();
        s3.shutdown();
        return this;
    }

    private MMResponseAdapter process(String[] args) throws Exception {

        // sqs message pump
        final SqsMessagePump toProcessPricingClaim = new SqsMessagePump(sqs, configuration, "core_identification.sqs");

        final int bindPort = Integer.parseInt(configuration
                .getProperty("response_adapter.bind_port", configuration
                        .getProperty("default.bind_port", "0")));

        // bind lock tcp port
        try (final ServerSocket socket = new ServerSocket(bindPort)) {
            logger.info("socket bound to {}", socket.getLocalSocketAddress());

            // sqs polling thread(s)
            final ExecutorService executorService = Executors.newCachedThreadPool();

            // completed notification(s)
            executorService.execute(new Runnable() {

                @Override
                public void run() {
                    try {
                        pollCompletedQueue();
                    } catch (Exception e) {
                        logger.error("polling completed queue", e);
                    }
                }

            });

            // failed notification(s)
            executorService.execute(new Runnable() {

                @Override
                public void run() {
                    try {
                        pollFailedQueue();
                    } catch (Exception e) {
                        logger.error("polling failed queue", e);
                    }
                }

            });

            // wait sqs polling thread(s) termination
            executorService.shutdown();
            while (!executorService.awaitTermination(1L, TimeUnit.MINUTES))
                logger.debug("waiting sqs polling thread(s) termination...");

        }

        return this;
    }

    private void pollCompletedQueue() throws Exception {

        final String wsDsrMetadata = configuration.getProperty("response_adapter.ws.dsr_metadata");

        // sqs message pump(s)
        final SqsMessagePump newClaimsSqsMessagePump = new SqsMessagePump(sqs,
                configuration, "response_adapter.sqs.new_claim_queue");
        final SqsMessagePump completedSqsMessagePump = new SqsMessagePump(sqs,
                configuration, "response_adapter.completed.sqs");
        completedSqsMessagePump.pollingLoop(messageDeduplicator, new SqsMessagePump.Consumer() {

            private JsonObject output = new JsonObject();
            private JsonObject error = new JsonObject();

            @Override
            public JsonObject getStartedMessagePayload(JsonObject message) {
                return SqsMessageHelper.formatContext();
            }

            @Override
            public JsonObject getFailedMessagePayload(JsonObject message) {
                return error;
            }

            @Override
            public JsonObject getCompletedMessagePayload(JsonObject message) {
                return output;
            }

            @Override
            public boolean consumeMessage(JsonObject message) {

                // parse completed json message
                final JsonObject input = GsonUtils
                        .getAsJsonObject(message, "input");
                final JsonObject output = GsonUtils
                        .getAsJsonObject(message, "output");
                final JsonArray outputItems = GsonUtils
                        .getAsJsonArray(output, "items");

                //back input
                final JsonObject inputBodyObject = GsonUtils.getAsJsonObject(GsonUtils.getAsJsonObject(GsonUtils.getAsJsonObject(input, "body"), "input"), "body");


                logger.info("message is {}", input.toString());


                int totalRecords = 0;
                int identifiedRecords = 0;
                int identifiedRecordsIsrc = 0;
                int identifiedRecordsIswc = 0;
                int identifiedRecordsWorkCode = 0;
                int identifiedRecordsTitleArtist = 0;

                for (JsonElement element : outputItems) {
                    final JsonObject item = element.getAsJsonObject();

                    //final int isrcQueries = GsonUtils.getAsInt(item, "isrcQueries", 0);
                    //final int iswcQueries = GsonUtils.getAsInt(item, "iswcQueries", 0);
                    //final int isrcPostings = GsonUtils.getAsInt(item, "isrcPostings", 0);
                    //final int iswcPostings = GsonUtils.getAsInt(item, "iswcPostings", 0);
                    //final int endTimeMillis = GsonUtils.getAsInt(item, "endTimeMillis", 0);
                    //final int inputFileSize = GsonUtils.getAsInt(item, "inputFileSize", 0);
                    //final int idSplitMessage = GsonUtils.getAsInt(item, "idSplitMessage", 0);
                    //final int invalidRecords = GsonUtils.getAsInt(item, "invalidRecords", 0);
                    //final int outputFileSize = GsonUtils.getAsInt(item, "outputFileSize", 0);
                    //final int beginTimeMillis = GsonUtils.getAsInt(item, "beginTimeMillis", 0);
                    //final int workCodeQueries = GsonUtils.getAsInt(item, "workCodeQueries", 0);
                    //final int elapsedTimeMillis = GsonUtils.getAsInt(item, "elapsedTimeMillis", 0);
                    //final int idSplitMessagePart = GsonUtils.getAsInt(item, "idSplitMessagePart", 0);
                    //final int titleArtistQueries = GsonUtils.getAsInt(item, "titleArtistQueries", 0);
                    //final int titleArtistPostings = GsonUtils.getAsInt(item, "titleArtistPostings", 0);
                    //final int unidentifiedRecords = GsonUtils.getAsInt(item, "unidentifiedRecords", 0);
                    //final int isrcQueriesWithResults = GsonUtils.getAsInt(item, "isrcQueriesWithResults", 0);
                    //final int iswcQueriesWithResults = GsonUtils.getAsInt(item, "iswcQueriesWithResults", 0);
                    //final int titleArtistQueriesWithResults = GsonUtils.getAsInt(item, "titleArtistQueriesWithResults", 0);

                    final int totalRecordsPart = GsonUtils.getAsInt(item, "totalRecords", 0); //TOTAL_LINES
                    final int identifiedRecordsPart = GsonUtils.getAsInt(item, "identifiedRecords", 0); //TOTAL_RECOGNIZED
                    final int identifiedRecordsIsrcPart = GsonUtils.getAsInt(item, "identifiedRecordsIsrc", 0); //TOTAL_ISRC
                    final int identifiedRecordsIswcPart = GsonUtils.getAsInt(item, "identifiedRecordsIswc", 0); //TOTAL_ISWC
                    final int identifiedRecordsWorkCodePart = GsonUtils.getAsInt(item, "identifiedRecordsWorkCode", 0); //TOTAL_IDSONG
                    final int identifiedRecordsTitleArtistPart = GsonUtils.getAsInt(item, "identifiedRecordsTitleArtist", 0); //TOTAL_TITLE_ARTISTS

                    totalRecords += totalRecordsPart;
                    identifiedRecords += identifiedRecordsPart;
                    identifiedRecordsIsrc += identifiedRecordsIsrcPart;
                    identifiedRecordsIswc += identifiedRecordsIswcPart;
                    identifiedRecordsWorkCode += identifiedRecordsWorkCodePart;
                    identifiedRecordsTitleArtist += identifiedRecordsTitleArtistPart;


                }

                HashMap<String, String> parameters = new HashMap<>();
                String idDsr = GsonUtils.getAsString(inputBodyObject, "idDsr");
                parameters.put("idDsr", idDsr);
                parameters.put("totalRecords", Integer.toString(totalRecords));
                parameters.put("identifiedRecords", Integer.toString(identifiedRecords));
                parameters.put("identifiedRecordsIsrc", Integer.toString(identifiedRecordsIsrc));
                parameters.put("identifiedRecordsIswc", Integer.toString(identifiedRecordsIswc));
                parameters.put("identifiedRecordsWorkCode", Integer.toString(identifiedRecordsWorkCode));
                parameters.put("identifiedRecordsTitleArtist", Integer.toString(identifiedRecordsTitleArtist));


                try {
                    dao.executeUpdate("response_adapter.sql.insert_dsr_statistics", parameters);
                } catch (SQLException e) {
                    logger.error("error executing insert on dsr_statistics");
                    error = SqsMessageHelper.formatError(e);
//                    sqsMessagePump.sendFailedMessage(input, error, false);
                    return false;
                }


                try {
                    //invoco i servizi di pricing e claim e faccio il merge dei due servizi
//                    JsonObject wsPricing = parseJSonFromWs("servizio pricing");
                    JsonObject wsClaim = parseJSonFromWs(wsDsrMetadata.replace("{idDsr}", idDsr));

                    if ("KO".equalsIgnoreCase(GsonUtils.getAsString(wsClaim, "status"))) {
                        throw new Exception(GsonUtils.getAsString(wsClaim, "errorMessage"));
                    }

                    JsonObject body = new JsonObject();
                    body.addProperty("idDsr", idDsr);
                    body.addProperty("dsp", GsonUtils.getAsString(wsClaim, "dspFtpPath"));
                    body.addProperty("dspCode", GsonUtils.getAsString(wsClaim, "dspCode"));
                    body.addProperty("sliceDspCode", GsonUtils.getAsString(wsClaim, "sliceDspCode"));
                    body.addProperty("original_price_basis_currency", GsonUtils.getAsString(wsClaim, "dsrCurrency"));
                    body.addProperty("notEncoded", GsonUtils.getAsString(wsClaim, "notEncoded"));
                    body.addProperty("country", GsonUtils.getAsString(wsClaim, "country"));
                    body.addProperty("territory", GsonUtils.getAsString(wsClaim, "territory"));
                    body.addProperty("service_type", GsonUtils.getAsString(wsClaim, "serviceType"));
                    body.addProperty("ccidVersion", GsonUtils.getAsString(wsClaim, "ccidVersion"));
                    body.addProperty("applied_tariff", GsonUtils.getAsString(wsClaim, "appliedTariff"));
                    body.addProperty("trading_brand", GsonUtils.getAsString(wsClaim, "tradingBrand"));
                    body.addProperty("encoded", GsonUtils.getAsString(wsClaim, "encoded"));
                    body.addProperty("periodStartDate", GsonUtils.getAsString(wsClaim, "periodStartDate"));
                    body.addProperty("periodEndDate", GsonUtils.getAsString(wsClaim, "periodEndDate"));
                    body.addProperty("encodedProvisional", GsonUtils.getAsString(wsClaim, "encodedProvisional"));
                    body.addProperty("use_type", GsonUtils.getAsString(wsClaim, "useType"));
                    body.addProperty("conversion_rate", 1 / GsonUtils.getAsDouble(wsClaim, "conversionRate", 1) * 100000);
                    body.addProperty("sender", GsonUtils.getAsString(wsClaim, "sender"));
                    body.addProperty("receiver", GsonUtils.getAsString(wsClaim, "receiver"));
                    body.addProperty("ccid_id", GsonUtils.getAsString(wsClaim, "headerCcidID"));
                    body.addProperty("work_code_type", GsonUtils.getAsString(wsClaim, "workCodeType"));
                    body.addProperty("split_perf", GsonUtils.getAsString(wsClaim, "demSplit"));
                    body.addProperty("split_mech", GsonUtils.getAsString(wsClaim, "drmSplit"));
                    body.addProperty("baseRefId", GsonUtils.getAsString(wsClaim, "ccidId"));
                    body.addProperty("periodType", GsonUtils.getAsString(wsClaim, "periodType"));
                    body.addProperty("period", GsonUtils.getAsString(wsClaim, "period"));
                    body.addProperty("idUtilizationType", GsonUtils.getAsString(wsClaim, "idUtilizationType"));
                    body.addProperty("sliceMonth", GsonUtils.getAsString(wsClaim, "sliceMonth"));
                    body.addProperty("businessOffer", GsonUtils.getAsString(wsClaim, "commercialOffer"));
                    body.addProperty("commercialOffer", GsonUtils.getAsString(wsClaim, "commercialOffer"));
                    body.addProperty("claimPeriodType", GsonUtils.getAsString(wsClaim, "claimPeriodType"));
                    body.addProperty("claimUtilizationType", GsonUtils.getAsString(wsClaim, "claimUtilizationType"));
                    body.addProperty("PARAM_APPLIED_TARIFF", GsonUtils.getAsString(wsClaim, "APPLIED_TARIFF"));
                    body.addProperty("PARAM_AUX", GsonUtils.getAsString(wsClaim, "AUX"));
                    body.addProperty("year", GsonUtils.getAsString(wsClaim, "year"));
                    body.addProperty("month", GsonUtils.getAsString(wsClaim, "month"));
                    body.add("tenant", GsonUtils.getAsJsonObject(wsClaim, "tenant"));
                    body.add("mandators", GsonUtils.getAsJsonArray(wsClaim, "mandators"));
                    body.addProperty("decisionTableUrl", GsonUtils.getAsString(wsClaim, "dtableS3Url"));
                    body.addProperty("priceModel", GsonUtils.getAsString(wsClaim, "idUtilizationType"));
                    body.addProperty("totalSales", GsonUtils.getAsString(wsClaim, "salesLineNum"));
                    body.addProperty("totalAmount", GsonUtils.getAsString(wsClaim, "totalValue"));
                    body.addProperty("totalSubscriptions", GsonUtils.getAsString(wsClaim, "subscriptionsNum"));
                    body.addProperty("COMMERCIAL_MODEL", GsonUtils.getAsString(wsClaim, "COMMERCIAL_MODEL"));
                    body.addProperty("CLAIM_TYPE", GsonUtils.getAsString(wsClaim, "CLAIM_TYPE"));

                    //bug currency claim ORIG
                    body.addProperty("currency", GsonUtils.getAsString(wsClaim, "currency"));
                    //bug INC0010083
//                    body.addProperty("royalty_currency", GsonUtils.getAsString(wsClaim, "currency"));

                    if (GsonUtils.getAsString(wsClaim, "ccidCurrency").equals("ORIG"))
                        body.addProperty("royalty_currency", GsonUtils.getAsString(wsClaim, "dsrCurrency"));
                    else
                        body.addProperty("royalty_currency", GsonUtils.getAsString(wsClaim, "ccidCurrency"));

                    newClaimsSqsMessagePump.sendToProcessMessage(body, false);
//                    sqsMessagePump.sendToProcessMessage(toProcessNewClaimQueue, input, body, false);

                } catch (Exception e) {
                    logger.error("error executing ws services", e);
                    error = SqsMessageHelper.formatError(e);
                    return false;
                }
                //sendMessage

                return true;
            }

        });

    }

    private JsonObject parseJSonFromWs(String urlString) throws Exception {
//        URL url = new URL("http://gestioneutilizzazioni-sviluppo.alkemytech.it/cruscottoUtilizzazioni/rest/dsrMetadata/extractDsrMetadata?dsrName=Report_YouTube_bmat_IT_201704_20170509002453_masterlist&dsp=youtube");
        logger.debug("invoking ws {}", urlString);
        URL url = new URL(urlString);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");


        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer content = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();


        return GsonUtils.fromJson(content.toString(), JsonObject.class);
    }

    private void pollFailedQueue() throws Exception {
        // sqs message pump(s)
        final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs,
                configuration, "response_adapter.sqs");
        final SqsMessagePump failedSqsMessagePump = new SqsMessagePump(sqs,
                configuration, "response_adapter.failed.sqs");
        failedSqsMessagePump.pollingLoop(messageDeduplicator, new SqsMessagePump.Consumer() {

            @Override
            public JsonObject getStartedMessagePayload(JsonObject message) {
                return null;
            }

            @Override
            public JsonObject getFailedMessagePayload(JsonObject message) {
                return null;
            }

            @Override
            public JsonObject getCompletedMessagePayload(JsonObject message) {
                return null;
            }

            @Override
            public boolean consumeMessage(JsonObject message) {

                // parse failed json message
                final JsonObject input = GsonUtils
                        .getAsJsonObject(message, "input");
                final JsonArray items = GsonUtils
                        .getAsJsonArray(GsonUtils
                                .getAsJsonObject(input, "body"), "items");
                final JsonObject error = GsonUtils
                        .getAsJsonObject(message, "error");

                // send started message
                sqsMessagePump.sendStartedMessage(input,
                        SqsMessageHelper.formatContext(), false);


                // send failed message
                sqsMessagePump.sendFailedMessage(input, error, false);

                return true;

            }

        });

    }

}

