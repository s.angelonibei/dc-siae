package com.alkemytech.sophia.identification;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.index.ReverseIndex;
import com.alkemytech.sophia.commons.query.QueryParser;
import com.alkemytech.sophia.commons.util.LongArray;
import com.alkemytech.sophia.identification.collecting.CollectingDatabase;
import com.alkemytech.sophia.identification.document.DocumentStore;
import com.alkemytech.sophia.identification.model.CollectingCode;
import com.alkemytech.sophia.identification.model.Document;
import com.alkemytech.sophia.identification.model.Work;
import com.alkemytech.sophia.identification.work.WorkDatabase;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class InteractiveSearch {
	
	private static final Logger logger = LoggerFactory.getLogger(InteractiveSearch.class);

	private static interface SearchEngine {
		public void search(String line) throws Exception;
	}
	
	protected static class GuiceModuleExtension extends GuiceModule {

		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// reverse index(es)
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("iswc_index"))
				.to(classForPrefix(ReverseIndex.class, "iswc_index"))
				.in(Scopes.SINGLETON);
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("isrc_index"))
				.to(classForPrefix(ReverseIndex.class, "isrc_index"))
				.in(Scopes.SINGLETON);
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("title_index"))
				.to(classForPrefix(ReverseIndex.class, "title_index"))
				.in(Scopes.SINGLETON);
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("artist_index"))
				.to(classForPrefix(ReverseIndex.class, "artist_index"))
				.in(Scopes.SINGLETON);
			// work database
			bind(WorkDatabase.class)
				.to(classForPrefix(WorkDatabase.class,
						configuration.getProperty("work_database")))
				.in(Scopes.SINGLETON);
			// collecting database
			bind(CollectingDatabase.class)
				.to(classForPrefix(CollectingDatabase.class,
						configuration.getProperty("collecting_database")))
				.in(Scopes.SINGLETON);
			// document store
			bind(DocumentStore.class)
				.to(classForPrefix(DocumentStore.class,
						configuration.getProperty("document_store")))
				.in(Scopes.SINGLETON);
			// self
			bind(InteractiveSearch.class)
				.asEagerSingleton();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			final InteractiveSearch instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/interactive-search.properties"))
					.getInstance(InteractiveSearch.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	private final Properties configuration;
	private final WorkDatabase workDatabase;
	private final CollectingDatabase collectingDatabase;
	private final DocumentStore documentStore;
	private final ReverseIndex iswcIndex;
	private final ReverseIndex isrcIndex;
	private final ReverseIndex titleIndex;
	private final ReverseIndex artistIndex;

	@Inject
	protected InteractiveSearch(@Named("configuration") Properties configuration,
			WorkDatabase workDatabase, 
			CollectingDatabase collectingDatabase,
			DocumentStore documentStore,
			@Named("iswc_index") ReverseIndex iswcIndex,
			@Named("isrc_index") ReverseIndex isrcIndex,
			@Named("title_index") ReverseIndex titleIndex,
			@Named("artist_index") ReverseIndex artistIndex) {
		super();
		this.configuration = configuration;
		this.workDatabase = workDatabase;
		this.collectingDatabase = collectingDatabase;
		this.documentStore = documentStore;
		this.iswcIndex = iswcIndex;
		this.isrcIndex = isrcIndex;
		this.titleIndex = titleIndex;
		this.artistIndex = artistIndex;
	}
	
	public InteractiveSearch startup() throws IOException {
		workDatabase.startup();
		collectingDatabase.startup();
		documentStore.startup();
		return this;
	}

	public InteractiveSearch shutdown() throws IOException {
		documentStore.shutdown();
		collectingDatabase.shutdown();
		workDatabase.shutdown();
		return this;
	}
	
	public InteractiveSearch process(String[] args) throws Exception {
		
		final boolean printWorks = "true".equalsIgnoreCase(configuration
				.getProperty("interactive_search.print_works"));
		final String useIndexCmd = configuration
				.getProperty("interactive_search.command.index_search", "index");
		final String useCollectingCmd = configuration
				.getProperty("interactive_search.command.collecting_search", "collecting");
		final String useWorkCmd = configuration
				.getProperty("interactive_search.command.work_search", "work");
		final String useDocumentCmd = configuration
				.getProperty("interactive_search.command.document_search", "document");
				
		final QueryParser queryParser = QueryParser.create();
		queryParser.addIndex(iswcIndex);
		queryParser.addIndex(isrcIndex);
		queryParser.addIndex(titleIndex);
		queryParser.addIndex(artistIndex);

		String using = useIndexCmd;
		SearchEngine searchEngine = new SearchEngine() {
			
			@Override
			public void search(String line) throws IOException {
				final LongArray postings = queryParser.evaluate(line);
				System.out.println("results: " + postings);
				if (printWorks) {
					for (int i = 0; i < postings.size(); i ++) {
						final Document document = documentStore.getById(postings.get(i));
						final Work work = workDatabase.getById(document.workId);
						System.out.println("  " + work);
					}					
				}				
			}
			
		};

		System.out.println();
		System.out.println(String.format("type \"use <%s|%s|%s|%s>\" to select search target",
				useIndexCmd, useCollectingCmd, useWorkCmd, useDocumentCmd));
		System.out.println("type \"exit\" to quit");
		System.out.println("using " + using);
		try (final BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in))) {
			for (String line = stdin.readLine(); !"exit".equals(line); line = stdin.readLine()) {
				if (null == line || "".equals(line)) {
					continue;
				}
				System.out.println("input text: \"" + line + "\"");
				
				if (line.startsWith("use ")) {
					
					if (("use " + useIndexCmd).equals(line)) {
						if (!using.equals(useIndexCmd)) {
							using = useIndexCmd;
							searchEngine = new SearchEngine() {
								
								@Override
								public void search(String line) throws IOException {
									final LongArray postings = queryParser.evaluate(line);
									System.out.println("results: " + postings);
									if (printWorks) {
										for (int i = 0; i < postings.size(); i ++) {
											final Document document = documentStore.getById(postings.get(i));
											final Work work = workDatabase.getById(document.workId);
											System.out.println("  " + work);
										}					
									}				
								}
								
							};							
						}
					} else if (("use " + useCollectingCmd).equals(line)) {
						if (!using.equals(useCollectingCmd)) {
							using = useCollectingCmd;			
							searchEngine = new SearchEngine() {
								
								@Override
								public void search(String line) throws IOException {
									final CollectingCode collectingCode = collectingDatabase.getByCode(line);
									System.out.println("results: " + collectingCode);
									if (printWorks && null != collectingCode) {
										final Work work = workDatabase.getById(collectingCode.workId);
										System.out.println("  " + work);
									}				
								}
								
							};	
						}
					} else if (("use " + useWorkCmd).equals(line)) {
						if (!using.equals(useWorkCmd)) {
							using = useWorkCmd;
							searchEngine = new SearchEngine() {
								
								@Override
								public void search(String line) throws IOException {
									final Work work = workDatabase.getById(Long.parseLong(line));
									System.out.println("results: " + work);
								}
								
							};							
						}
					} else if (("use " + useDocumentCmd).equals(line)) {
						if (!using.equals(useDocumentCmd)) {
							using = useDocumentCmd;
							searchEngine = new SearchEngine() {
								
								@Override
								public void search(String line) throws IOException {
									final Document document = documentStore.getById(Long.parseLong(line));
									System.out.println("results: " + document);
									if (printWorks && null != document) {
										final Work work = workDatabase.getById(document.workId);
										System.out.println("  " + work);
									}				
								}
								
							};							
						}
					} else {
						System.out.println("unknown command " + line);
					}
					System.out.println("using " + using);
				
				} else {
					
					searchEngine.search(line);

				}
				
				System.out.println();				
			}
		}
		
		return this;
	}

}