package com.alkemytech.sophia.identification.result;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ResultPosition {

	private static final Pattern replacePattern = Pattern.compile("replace.(\\d)");
	private static final Pattern insertPattern = Pattern.compile("insert.(\\d)");
	
	public static ResultPosition valueOf(String resultPosition) {
		// "append"
		if ("append".equals(resultPosition))
			return new ResultPosition(true, false, false, 0);
		// "replace:<column>"
		final Matcher replaceMatcher = replacePattern.matcher(resultPosition);
		if (replaceMatcher.matches())
			return new ResultPosition(false, true, false,
					Integer.parseInt(replaceMatcher.group(1)));
		// "insert:<column>"
		final Matcher insertMatcher = insertPattern.matcher(resultPosition);
		if (insertMatcher.matches())
			return new ResultPosition(false, false, true,
					Integer.parseInt(insertMatcher.group(1)));
		// invalid
		throw new IllegalArgumentException(String
				.format("invalid result position: %s", resultPosition));
	}
	
	public final boolean append;
	public final boolean replace;
	public final boolean insert;
	public final int column;
	
	private ResultPosition(boolean append, boolean replace, boolean insert, int column) {
		super();
		this.append = append;
		this.replace = replace;
		this.insert = insert;
		this.column = column;
	}

	public void apply(List<String> record, String result) {
		if (append) {
			record.add(result);
		} else if (replace) {
			record.set(column, result);
		} else {
			record.add(column, result);
		}
	}

	@Override
	public String toString() {
		if (append)
			return "append";
		if (replace)
			return "replace:" + column;
		return "insert:" + column;
	}
	
}
