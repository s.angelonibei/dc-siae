package com.alkemytech.sophia.identification.jdbc;


import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.jdbc.DBCP2DataSource;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class KbdbDataSource extends DBCP2DataSource {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Inject
	public KbdbDataSource(@Named("configuration") Properties configuration,
			@Named("sophia_kb_data_source") String propertyPrefix) {
		super(configuration, propertyPrefix);
		logger.debug("propertyPrefix: {}", propertyPrefix);
	}

}


