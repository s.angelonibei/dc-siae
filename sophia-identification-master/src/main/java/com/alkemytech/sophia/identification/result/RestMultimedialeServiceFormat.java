package com.alkemytech.sophia.identification.result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.score.TextScore;
import com.alkemytech.sophia.commons.text.TextNormalizer;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.SplitCharSequence;
import com.alkemytech.sophia.identification.MatchType;
import com.alkemytech.sophia.identification.document.ScoredDocument;
import com.alkemytech.sophia.identification.jdbc.KbdbDAO;
import com.alkemytech.sophia.identification.model.Artist;
import com.alkemytech.sophia.identification.model.ArtistRole;
import com.alkemytech.sophia.identification.model.AttributeKey;
import com.alkemytech.sophia.identification.model.Category;
import com.alkemytech.sophia.identification.model.CodeType;
import com.alkemytech.sophia.identification.model.Origin;
import com.alkemytech.sophia.identification.model.Text;
import com.alkemytech.sophia.identification.model.TextType;
import com.alkemytech.sophia.identification.model.TitleType;
import com.alkemytech.sophia.identification.model.Work;
import com.alkemytech.sophia.identification.work.WorkDatabase;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class RestMultimedialeServiceFormat implements ConfigurableResultFormat {


    private final Logger logger = LoggerFactory.getLogger(getClass());

    private static final class ScoredText {

        public static final ScoredText empty = new ScoredText(0.0, "");

        public final double score;
        public final String text;

        public ScoredText(double score, String text) {
            super();
            this.score = score;
            this.text = text;
        }

    }

    private final Gson gson;
    private final TextNormalizer titleNormalizer;
    private final TextNormalizer artistNormalizer;
    private final WorkDatabase workDatabase;
	private final KbdbDAO dao;

    @Inject
    public RestMultimedialeServiceFormat(Gson gson,
    		KbdbDAO dao,
            @Named("title_normalizer") TextNormalizer titleNormalizer,
	        @Named("artist_normalizer") TextNormalizer artistNormalizer,
	        WorkDatabase workDatabase) {
        this.gson = gson;
        this.dao = dao;
        this.titleNormalizer = titleNormalizer;
        this.artistNormalizer = artistNormalizer;
        this.workDatabase = workDatabase;
    }

    private TextScore newScoreInstance(String className) {
        try {
            return (TextScore) Class.forName(className).newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            throw new IllegalArgumentException(String
                    .format("unknown class name \"%s\"", className), e);
        }
    }

    private String getFieldLabel(String field, Map<String, String> fieldsMapping) {
        return null == fieldsMapping ? field : fieldsMapping.get(field);
    }

    private ScoredText getBestScoredText(TextScore textScore, TextNormalizer textNormalizer, List<String> workTexts, List<String> recordTexts) {
        if (null == workTexts || workTexts.isEmpty())
            return ScoredText.empty;
        if (null == recordTexts || recordTexts.isEmpty())
            return ScoredText.empty;
        double bestScore = 0.0;
        String bestText = "";
        for (String workText : workTexts) {
            final String normalizedWorkText = textNormalizer.normalize(workText);
            if (Strings.isNullOrEmpty(normalizedWorkText))
                continue;
            final SplitCharSequence left = new SplitCharSequence(normalizedWorkText);
            for (String recordText : recordTexts) {
                recordText = textNormalizer.normalize(recordText);
                if (Strings.isNullOrEmpty(recordText))
                    continue;
                final double score = textScore
                        .getScore(left, new SplitCharSequence(recordText));
                if (score > bestScore) {
                    if (score >= 1.0)
                        return new ScoredText(score, workText);
                    bestScore = score;
                    bestText = workText;
                }
            }
        }
        return new ScoredText(bestScore, bestText);
    }

    /*
     * (non-Javadoc)
     * @see com.alkemytech.sophia.identification.result.ConfigurableResultFormat#getInstance(java.util.Properties, java.lang.String)
     */
    @Override
    public ResultFormat getInstance(Properties configuration, String propertyPrefix) {

//		[
//		   {
//     		"work_code":"codiceOpera",\
//     		"score":"gradoDiConfidenza",\
//      	"titole_original":"titoloOriginale",\
//     		"title_original_score":"confidenzaTitoloOriginale",\
//     		"title_alternative":"titoloAlternativo",\
//     		"title_alternative_score":"confidenzaTitoloAlternativo",\
//     		"author":"autore",\
//     		"author_score":"confidenzaAutore",\
//     		"composer":"compositore",\
//     		"composer_score":"confCompositore",\
//     		"performer":"interprete",\
//     		"performer_score":"confInterprete",\
//     		"match_type":"tipoCodifica"\
//		   }
//		]
        final Map<String, String> mapping = GsonUtils.decodeJsonMap(configuration.getProperty(propertyPrefix + ".mapping"));
        final TextScore titleScore = newScoreInstance(configuration.getProperty(propertyPrefix + ".title_score"));
        final TextScore artistScore = newScoreInstance(configuration.getProperty(propertyPrefix + ".artist_score"));
        final double scoreScale = Double.parseDouble(configuration.getProperty(propertyPrefix + ".score_scale", "1"));
        final String scoreStringFormat = configuration.getProperty(propertyPrefix + ".score_string_format", "%.3f");
        
        
        final String labelWorkCode 				= getFieldLabel("work_code", mapping);
        final String labelScore 				= getFieldLabel("score", mapping);
        final String labelTitleOriginale		= getFieldLabel("title_original", mapping);
        final String labelTitleOriginaleScore 	= getFieldLabel("title_original_score", mapping);
        final String labelTitleAlternative		= getFieldLabel("title_alternative", mapping);
        final String labelTitleAlternativeScore = getFieldLabel("title_alternative_score", mapping);
        final String labelAuthor 				= getFieldLabel("author", mapping);
        final String labelAuthorScore 			= getFieldLabel("author_score", mapping);
        final String labelComposer 				= getFieldLabel("composer", mapping);
        final String labelComposerScore 		= getFieldLabel("composer_score", mapping);
        final String labelPerformer 			= getFieldLabel("performer", mapping);
        final String labelPerformerScore 		= getFieldLabel("performer_score", mapping);
        final String labelMatchType 			= getFieldLabel("match_type", mapping);
        final String labelCodiceSiae			= getFieldLabel("codice_siae", mapping);
        final String labelCodiceAda				= getFieldLabel("codice_ada", mapping);
        final String labelUtilizzazioni			= getFieldLabel("utilizzazioni", mapping);
        
        

        // log config value(s)
        final boolean debug = "true".equalsIgnoreCase(configuration
                .getProperty("identifier.debug", configuration
                        .getProperty("default.debug")));
        if (debug) {
            logger.debug("propertyPrefix: {}", propertyPrefix);
            logger.debug("\tmapping: {}", mapping);
            logger.debug("\ttitleScore: {}", titleScore);
            logger.debug("\tartistScore: {}", artistScore);
            logger.debug("\tescoreScale: {}", scoreScale);
            logger.debug("\tscoreStringFormat: {}", scoreStringFormat);
            logger.debug("\tlabelWorkCode: {}", labelWorkCode);
            logger.debug("\tlabelAuthor: {}", labelAuthor);
            logger.debug("\tlabelComposer: {}", labelComposer);
            logger.debug("\tlabelPerformer: {}", labelPerformer);
            logger.debug("\tlabelScore: {}", labelScore);
            logger.debug("\tlabelAuthorScore: {}", labelAuthorScore);
            logger.debug("\tlabelComposerScore: {}", labelComposerScore);
            logger.debug("\tlabelPerformerScore: {}", labelPerformerScore);
            logger.debug("\tlabelMatchType: {}", labelMatchType);
            logger.debug("\tlabelCodiceSiae: {}", labelCodiceSiae);
            logger.debug("\tlabelCodiceAda: {}", labelCodiceAda);
        }

        return new ResultFormat() {

            /*
             * (non-Javadoc)
             * @see com.alkemytech.sophia.identification.result.ResultFormat#format(java.util.List, java.util.List, java.util.List, java.util.List, java.util.List, java.util.List, java.util.List, com.alkemytech.sophia.identification.MatchType)
             */
            @Override
			public String format(List<Text> texts, List<ScoredDocument> scoredDocuments, MatchType matchType)  throws Exception {
                final JsonArray jsonArray = new JsonArray();
                List<String> titleOriginalTexts = new ArrayList<String>();
                List<String> titleAlternativeTexts = new ArrayList<String>();
                List<String> authorTexts = new ArrayList<String>();
                List<String> composerText = new ArrayList<String>();
                List<String> performerText = new ArrayList<String>();
                for(Text text:texts ) {
    				switch(text.type) {
						case TextType.title:
						case TextType.title_original:
							titleOriginalTexts.add(text.text);
							break;
						case TextType.title_alternative:
							titleAlternativeTexts.add(text.text);
							break;
						case TextType.artist:
						case TextType.artist_author:
							authorTexts.add(text.text);
							break;
						case TextType.artist_composer:
							composerText.add(text.text);
							break;
						case TextType.artist_performer:
							performerText.add(text.text);
							break;
					}
                }
                
                
                for (ScoredDocument scoredDocument : scoredDocuments) {
                    final Work work = workDatabase.getById(scoredDocument.document.workId);
                    if (null != work) {
                        final String workCode = work.getFirstCode(CodeType.siae);
                        if (!Strings.isNullOrEmpty(workCode)) {
                            final JsonObject jsonObject = new JsonObject();

                            if(null != labelUtilizzazioni) {
	                            String codeSiae = work.getAttribute(AttributeKey.codice_siae, Origin.siae);
	                            if(codeSiae != null && codeSiae.length()>0) {
		                            JsonArray arr = new JsonArray();
		                            final Map<String,String> parameters = new HashMap<>();
		                			parameters.clear();
		                			//parameters.put("codiceOpera", "91356087300");
		                			parameters.put("codiceOpera", codeSiae);
		                			List<Map<String,String>> rows = dao.executeQuery("identifier.opera.sql.select_offerta", parameters);
		                			
		                			for(Map<String,String> row:rows) {
		                    			JsonObject jsonproperty1 = new JsonObject();
		                    			jsonproperty1.addProperty("periodo", row.get("periodo"));
		                    			jsonproperty1.addProperty("dsr", row.get("dsr"));
		                    			jsonproperty1.addProperty("dsp", row.get("dsp"));                				
		                    			jsonproperty1.addProperty("versione", row.get("version"));                				
		                    			arr.add(jsonproperty1);
		                			}
		                			jsonObject.add(labelUtilizzazioni, arr);
	                            }
                            }
                            // titoloOriginale
                            String originalTitle = "";
                            if (null != labelTitleOriginale) {
                                List<String> lista = work.getTitles(TitleType.song, Category.original);
                               
                                if (lista != null) {
                                    for (String s : lista) {
                                    	originalTitle = originalTitle.concat(s + ",");
                                    }
                                }
                                jsonObject.addProperty(labelTitleOriginale, originalTitle.length() > 0 ? originalTitle.substring(0, originalTitle.length() - 1) : originalTitle);
                            }
                            // titoloOriginale Score
                            final ScoredText bestOriginalTitle = getBestScoredText(titleScore, titleNormalizer, work.getTitles(TitleType.song, Category.original), titleOriginalTexts);
                            if (null != labelTitleOriginaleScore) {
                                jsonObject.addProperty(labelTitleOriginaleScore, String.format(scoreStringFormat, bestOriginalTitle.score * scoreScale));
                            }

                            // titoloAlternativo Score
                            final ScoredText bestAlternativeTitle = getBestScoredText(titleScore, titleNormalizer, work.getTitles(TitleType.song, Category.alternative), titleOriginalTexts);
                            if(bestAlternativeTitle != null && (bestOriginalTitle.score < bestAlternativeTitle.score || originalTitle.length() ==0 ) ) {
	                            if (null != labelTitleAlternativeScore) {
	                                jsonObject.addProperty(labelTitleAlternativeScore, String.format(scoreStringFormat, bestAlternativeTitle.score * scoreScale));
	                            }
	                            // titoloAlternativo
	                            if (null != labelTitleAlternative) {
	                                List<String> lista = work.getTitles(TitleType.song, Category.alternative);
	                                String result = "";
	                                if (lista != null) {
	                                    for (String s : lista) {
	                                        result = result.concat(s + ",");
	                                    }
	                                }
	                                jsonObject.addProperty(labelTitleAlternative, result.length() > 0 ? result.substring(0, result.length() - 1) : result);
	                            }
                            }

                            // author & author_score
                            if (null != labelAuthor) {
                                List<Artist> lista = work.getArtistsFull(ArtistRole.author);
                                String result = "";
                                if (lista != null) {
                                    for (Artist s : lista) {
                                        result = result.concat(s.text + "[" + s.ipiNumber + "]" + ",");
                                    }
                                }
                                jsonObject.addProperty(labelAuthor, result.length() > 0 ? result.substring(0, result.length() - 1) : result);
                            }

                            final ScoredText bestAuthor = getBestScoredText(artistScore,artistNormalizer, work.getArtists(ArtistRole.author), authorTexts);
                            if (null != labelAuthorScore)
                                jsonObject.addProperty(labelAuthorScore,String.format(scoreStringFormat, bestAuthor.score * scoreScale));
                            
                            // composer & composer_score
                            final ScoredText bestComposer = getBestScoredText(artistScore, artistNormalizer, work.getArtists(ArtistRole.composer), composerText);
                            if (null != labelComposer) {
                            	List<Artist> lista = work.getArtistsFull(ArtistRole.composer);
                                String result = "";
                                if (lista != null) {
                                    for (Artist s : lista) {
                                    	result = result.concat(s.text + "[" + s.ipiNumber + "]" + ",");
                                    }
                                }
                                jsonObject.addProperty(labelComposer, result.length() > 0 ? result.substring(0, result.length() - 1) : result);
                            }
                            if (null != labelComposerScore)
                                jsonObject.addProperty(labelComposerScore,String.format(scoreStringFormat, bestComposer.score * scoreScale));
                            
                            // performer & performer_score
                            final ScoredText bestPerformer = getBestScoredText(artistScore,artistNormalizer, work.getArtists(ArtistRole.performer), performerText);
                            if (null != labelPerformer) {
                            	List<Artist> lista = work.getArtistsFull(ArtistRole.performer);
                                String result = "";
                                if (lista != null) {
                                    for (Artist s : lista) {
                                    	result = result.concat(s.text + "[" + s.ipiNumber + "]" + ",");
                                    }
                                }
                                jsonObject.addProperty(labelPerformer, result.length() > 0 ? result.substring(0, result.length() - 1) : result);
                            }
                            if (null != labelPerformerScore)
                                jsonObject.addProperty(labelPerformerScore,String.format(scoreStringFormat, bestPerformer.score * scoreScale));

                            
                            
                            
                            
                            
                            // score
                            if (null != labelScore)
                                jsonObject.addProperty(labelScore, String.format(scoreStringFormat, scoredDocument.score * scoreScale));
                           
                            // work_code
                            if (null != labelWorkCode) {
                                List<String> uuid = work.getCodes(CodeType.uuid);
                                if( uuid != null && uuid.size()>0)
                                	jsonObject.addProperty(labelWorkCode, uuid.get(0)); ;
                            }
                            List<String> iswcs = work.getCodes(CodeType.iswc);
                            if( iswcs != null && iswcs.size()>0) {
                            	String result = "";
                            	for(String iswc:iswcs) {
                            		result = result.concat(iswc + ",");
                            	}
                            	jsonObject.addProperty("iswc", result.length() > 0 ? result.substring(0, result.length() - 1) : result);
                        	}
                            
                            // match type
                            if (null != labelMatchType)
                                jsonObject.addProperty(labelMatchType, matchType.toString());
                            
                            
                            // codiceSiae
                            if (null != labelCodiceSiae)
                                jsonObject.addProperty(labelCodiceSiae, work.getAttribute(AttributeKey.codice_siae, Origin.siae));
                            // codiceSiae
                            if (null != labelCodiceAda)
                                jsonObject.addProperty(labelCodiceAda, work.getAttribute(AttributeKey.codice_ada, Origin.siae));
                            jsonArray.add(jsonObject);
                        }
                    }
                }
                return gson.toJson(jsonArray);
            }

			@Override
            public String format(List<String> titles, List<String> titleTypes, List<String> artists, List<String> artistTypes, List<String> codes, List<String> codeTypes, List<ScoredDocument> scoredDocuments, MatchType matchType) throws Exception {					
				// TODO Auto-generated method stub
				return "";
			}

        };

    }
}
