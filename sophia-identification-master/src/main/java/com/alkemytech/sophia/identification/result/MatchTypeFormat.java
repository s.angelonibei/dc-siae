package com.alkemytech.sophia.identification.result;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.identification.MatchType;
import com.alkemytech.sophia.identification.document.ScoredDocument;
import com.alkemytech.sophia.identification.model.Text;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class MatchTypeFormat implements ConfigurableResultFormat {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.identification.result.ConfigurableResultFormat#getInstance(java.util.Properties, java.lang.String)
	 */
	@Override
	public ResultFormat getInstance(Properties configuration, String propertyPrefix) {
		
		final Map<String,String> mapping = GsonUtils
				.decodeJsonMap(configuration
						.getProperty(propertyPrefix + ".mapping"));
		final Map<String,String> concurrentMapping = null == mapping ||
				mapping.isEmpty() ? null : new ConcurrentHashMap<>(mapping);
		final String defaultMapping = configuration
				.getProperty(propertyPrefix + ".default_mapping");

		// log config value(s)
		final boolean debug = "true".equalsIgnoreCase(configuration
				.getProperty("identifier.debug", configuration
						.getProperty("default.debug")));
		if (debug) {
			logger.debug("propertyPrefix: {}", propertyPrefix);
			logger.debug("\tmapping: {}", mapping);
			logger.debug("\tdefaultMapping: {}", defaultMapping);
		}

		return new ResultFormat() {
			@Override
			public String format(List<Text> text, List<ScoredDocument> scoredDocuments, MatchType matchType)
					throws Exception {
				String result = matchType.toString();
				if (null == concurrentMapping) {
					if(matchType == MatchType.unidentified)
						result="";
					return result;
				}
				result = concurrentMapping.get(result);
				return null == result ? defaultMapping : result;
			}

			/*
			 * (non-Javadoc)
			 * @see com.alkemytech.sophia.identification.result.ResultFormat#format(java.util.List, java.util.List, java.util.List, java.util.List, java.util.List, java.util.List, java.util.List, com.alkemytech.sophia.identification.MatchType)
			 */
			@Override
			public String format(List<String> titles, List<String> titleTypes, List<String> artists, List<String> artistTypes, List<String> codes, List<String> codeTypes, List<ScoredDocument> scoredDocuments, MatchType matchType) throws Exception {
				String result = matchType.toString();
				if (null == concurrentMapping) {
					if(matchType == MatchType.unidentified)
						result="";
					return result;
				}
				result = concurrentMapping.get(result);
				return null == result ? defaultMapping : result;
			}
			
		};
	}

}
