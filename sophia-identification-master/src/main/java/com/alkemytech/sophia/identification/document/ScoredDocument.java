package com.alkemytech.sophia.identification.document;

import java.util.List;

import com.alkemytech.sophia.identification.model.Document;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ScoredDocument {


    public final Document document;
    public final double score;
    public String title;
    public String artist;
    public List<String> ipi;
    public int flagDoppioDeposito = 0;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getFlagDoppioDeposito() {
        return flagDoppioDeposito;
    }

    public int setFlagDoppioDeposito(int flagDoppioDeposito) {
        this.flagDoppioDeposito = flagDoppioDeposito;
        return flagDoppioDeposito;
    }

    public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public ScoredDocument(Document document, double score,String title,String artist, List<String> ipi) {
        super();
        this.document = document;
        this.score = score;
        this.title = title;
        this.artist = artist;
        this.ipi = ipi;
    }


}
