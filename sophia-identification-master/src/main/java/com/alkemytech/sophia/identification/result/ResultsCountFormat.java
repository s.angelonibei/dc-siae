package com.alkemytech.sophia.identification.result;

import java.util.List;
import java.util.Properties;

import com.alkemytech.sophia.identification.MatchType;
import com.alkemytech.sophia.identification.document.ScoredDocument;
import com.alkemytech.sophia.identification.model.Text;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ResultsCountFormat implements ConfigurableResultFormat {

	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.identification.result.ConfigurableResultFormat#getInstance(java.util.Properties, java.lang.String)
	 */
	@Override
	public ResultFormat getInstance(Properties configuration, String propertyPrefix) {
		return new ResultFormat() {
			@Override
			public String format(List<Text> text, List<ScoredDocument> scoredDocuments, MatchType matchType)
					throws Exception {
				// TODO Auto-generated method stub
				return "";
			}

			/*
			 * (non-Javadoc)
			 * @see com.alkemytech.sophia.identification.result.ResultFormat#format(java.util.List, java.util.List, java.util.List, java.util.List, java.util.List, java.util.List, java.util.List, com.alkemytech.sophia.identification.MatchType)
			 */
			@Override
			public String format(List<String> titles, List<String> titleTypes, List<String> artists, List<String> artistTypes, List<String> codes, List<String> codeTypes, List<ScoredDocument> scoredDocuments, MatchType matchType) throws Exception {
				return null == scoredDocuments ? "0" : Integer.toString(scoredDocuments.size());
			}
			
		};
	}

}
