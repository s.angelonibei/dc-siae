package com.alkemytech.sophia.identification.adapter;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQSExtendedClient;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.sqs.McmdbMessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.identification.jdbc.McmdbDAO;
import com.alkemytech.sophia.identification.jdbc.McmdbDataSource;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

public class MMRequestAdapter {

    private static final Logger logger = LoggerFactory.getLogger(MMRequestAdapter.class);

    private static class GuiceModuleExtension extends GuiceModule {

        public GuiceModuleExtension(String[] args, String resourceName) {
            super(args, resourceName);
        }

        @Override
        protected void configure() {
            super.configure();
            // amazon service(s)
            bind(S3.class)
                    .toInstance(new S3(configuration));
            bind(SQSExtendedClient.class)
                    .toInstance(new SQSExtendedClient(configuration));
            // data source(s)
            bind(DataSource.class)
                    .annotatedWith(Names.named("MCMDB"))
                    .to(McmdbDataSource.class)
                    .asEagerSingleton();
            // data access object(s)
            bind(McmdbDAO.class)
                    .asEagerSingleton();

            bind(MessageDeduplicator.class)
                    .to(McmdbMessageDeduplicator.class);
            // self
            bind(MMRequestAdapter.class)
                    .asEagerSingleton();
        }

    }

    public static void main(String[] args) {
        try {
            final MMRequestAdapter instance = Guice
                    .createInjector(new GuiceModuleExtension(args,
                            "/mm-request-adapter.properties"))
                    .getInstance(MMRequestAdapter.class)
                    .startup();
            try {
                instance.process(args);
            } finally {
                instance.shutdown();
            }
        } catch (Throwable e) {
            logger.error("main", e);
        } finally {
            System.exit(0);
        }
    }

    private final Properties configuration;
    private final S3 s3;
    private final SQSExtendedClient sqs;
    private final MessageDeduplicator messageDeduplicator;

    @Inject
    protected MMRequestAdapter(@Named("configuration") Properties configuration,
                               S3 s3, SQSExtendedClient sqs,
                               MessageDeduplicator messageDeduplicator) {
        super();
        this.configuration = configuration;
        this.s3 = s3;
        this.sqs = sqs;
        this.messageDeduplicator = messageDeduplicator;
    }

    public MMRequestAdapter startup() {
        if ("true".equalsIgnoreCase(configuration.getProperty("request_adapter.locked", "false"))) {
            throw new IllegalStateException("application locked");
        }
        s3.startup();
        sqs.startup();
        return this;
    }

    public MMRequestAdapter shutdown() {
        sqs.shutdown();
        s3.shutdown();
        return this;
    }

    private MMRequestAdapter process(String[] args) throws Exception {

        final int bindPort = Integer.parseInt(configuration
                .getProperty("request_adapter.bind_port", configuration
                        .getProperty("default.bind_port", "0")));

        // bind lock tcp port
        try (final ServerSocket socket = new ServerSocket(bindPort)) {
            logger.info("socket bound to {}", socket.getLocalSocketAddress());

            // process database record(s)
            process();

        }

        return this;
    }

    private void process() throws Exception {

        final long startTimeMillis = System.currentTimeMillis();

        // config parameter(s)
        final String version = configuration
                .getProperty("request_adapter.message.version", "1");
        final String compression = configuration
                .getProperty("request_adapter.message.compression", "guess");
        final String configUrl = configuration
                .getProperty("request_adapter.message.config_url");
        final String inputUrl = configuration
                .getProperty("request_adapter.message.input_url");
        final String identifiedOutputUrl = configuration
                .getProperty("request_adapter.message.identified_output_url");
        final String unidentifiedOutputUrl = configuration
                .getProperty("request_adapter.message.unidentified_output_url");
        final String startedQueue = configuration
                .getProperty("request_adapter.message.started_queue");
        final String completedQueue = configuration
                .getProperty("request_adapter.message.completed_queue");
        final String failedQueue = configuration
                .getProperty("request_adapter.message.failed_queue");
        final String filePattern = configuration.getProperty("request_adapter.message.file_pattern");
        final Pattern filterRegex = Pattern.compile(filePattern);

        final JsonObject configurazioneIdentificazione = GsonUtils.fromJson(configuration
                .getProperty("request_adapter.message.identification_json"),JsonObject.class);


        // sqs message pump
        final SqsMessagePump toProcessCoreIdentification = new SqsMessagePump(sqs, configuration, "core_identification.sqs");


        final SqsMessagePump toProcessRequestAdapter = new SqsMessagePump(sqs, configuration, "request_adapter.sqs");
        toProcessRequestAdapter.pollingLoop(messageDeduplicator, new SqsMessagePump.Consumer() {

            private JsonObject error;

            @Override
            public JsonObject getStartedMessagePayload(JsonObject message) {
                return SqsMessageHelper.formatContext();
            }

            @Override
            public boolean consumeMessage(JsonObject message) {
                try {
                    String normFilePath = GsonUtils.getAsString(
                            GsonUtils.getAsJsonObject(message, "body"), "dsr_file_norm");
                    String idDsr = GsonUtils.getAsString(
                            GsonUtils.getAsJsonObject(message, "body"), "idDsr");


                    if (normFilePath == null) {
                        return false;
                    }

                    normFilePath = "s3://" + normFilePath;

                    String dspYearMonth = normFilePath.replace(inputUrl, "");
                    logger.debug("dsp year month: {}", dspYearMonth);
                    logger.debug("identifiedOutputUrl: {}", identifiedOutputUrl);
                    logger.debug("unidentifiedOutputUrl: {}", unidentifiedOutputUrl);

                    List<S3ObjectSummary> normalizedFiles = s3.listObjects(new S3.Url(normFilePath+ "/"));
                    String identifiedOutputS3UrlFolder = identifiedOutputUrl.replace("{dsp_year_month}", dspYearMonth).replace("{filename}", "");
                    String unidentifiedOutputS3UrlFolder = unidentifiedOutputUrl.replace("{dsp_year_month}", dspYearMonth).replace("{filename}", "");

                    List<S3ObjectSummary> identifiedOutputS3UrlObjects = s3.listObjects(new S3.Url(identifiedOutputS3UrlFolder));
                    List<S3ObjectSummary> unidentifiedOutputS3UrlObjects = s3.listObjects(new S3.Url(unidentifiedOutputS3UrlFolder));

                    for (S3ObjectSummary identifiedOutputS3UrlObject : identifiedOutputS3UrlObjects)
                        s3.delete(new S3.Url(identifiedOutputS3UrlObject.getBucketName(), identifiedOutputS3UrlObject.getKey()));

                    for (S3ObjectSummary unidentifiedOutputS3UrlObject : unidentifiedOutputS3UrlObjects)
                        s3.delete(new S3.Url(unidentifiedOutputS3UrlObject.getBucketName(), unidentifiedOutputS3UrlObject.getKey()));


                    final JsonArray items = new JsonArray();
                    for (S3ObjectSummary normalizedFile : normalizedFiles) {
                        final String bucketName = normalizedFile.getBucketName();
                        final String key = normalizedFile.getKey();
                        final String[] split = key.split("/");
                        final String fileName = split[split.length - 1];
                        if (filterRegex.matcher(fileName).matches()) {
    //                        normalizedFileNames.add(normalizedFile.getKey().replace(inputUrl,""));
                            final JsonObject item = new JsonObject();
                            item.addProperty("inputUrl", new S3.Url(bucketName, key).toString());
                            final String identifiedOutput = identifiedOutputUrl.replace("{dsp_year_month}", dspYearMonth)
                                    .replace("{filename}", fileName);
                            item.addProperty("identifiedOutputUrl", identifiedOutput);
                            final String unidentifiedOutput = unidentifiedOutputUrl.replace("{dsp_year_month}", dspYearMonth)
                                    .replace("{filename}", fileName);
                            item.addProperty("unidentifiedOutputUrl", unidentifiedOutput);
                            items.add(item);
                        }
                    }


                    // send sqs message
                    final JsonObject body = new JsonObject();

                    body.addProperty("idDsr", idDsr);
                    body.addProperty("version", version);
                    body.addProperty("compression", compression);
                    body.addProperty("configUrl", configUrl);
                    body.add("input", message);
                    body.add("items", items);
                    body.add("configuration", configurazioneIdentificazione);
                    if (!Strings.isNullOrEmpty(startedQueue)) {
                        body.addProperty("startedQueue", startedQueue);
                    }
                    if (!Strings.isNullOrEmpty(completedQueue)) {
                        body.addProperty("completedQueue", completedQueue);
                    }
                    if (!Strings.isNullOrEmpty(failedQueue)) {
                        body.addProperty("failedQueue", failedQueue);
                    }
                    logger.debug("sending sqs message with body {}", new GsonBuilder()
                            .disableHtmlEscaping().setPrettyPrinting().create().toJson(body));
                    toProcessCoreIdentification.sendToProcessMessage(body, false);


                    logger.info("adapter completed in {}",
                            TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));


                    return true;
                } catch (Exception e) {
                    logger.error("", e);
                    error = SqsMessageHelper.formatError(e);
                    return false;
                }
            }

            @Override
            public JsonObject getCompletedMessagePayload(JsonObject message) {
                return new JsonObject();
            }

            @Override
            public JsonObject getFailedMessagePayload(JsonObject message) {
                return error;
            }
        });


    }


}

