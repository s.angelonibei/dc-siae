package com.alkemytech.sophia.identification.model;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class TextType {

	public static final int undefined = 0;
	public static final int iswc = 1;
	public static final int isrc = 2;
	public static final int title = 3;
	public static final int artist = 4;
	public static final int uuid = 5;
	public static final int codiceSiae = 6;
	public static final int codiceAda = 7;
	public static final int codice = 8;

	
	// title(s)
	public static final int title_original = 12;
	public static final int title_alternative = 13;
	// artist(s)
	public static final int artist_author = 15;
	public static final int artist_composer = 16;
	public static final int artist_performer = 17;
	
	
	
	public static int valueOf(String textType) {
		if ("iswc".equals(textType)) {
			return iswc;
		} else if ("isrc".equals(textType)) {
			return isrc;
		} else if ("title".equals(textType)) {
			return title;
		} else if ("artist".equals(textType)) {
			return artist;
		} else if ("uuid".equals(textType)) {
			return uuid;
		} else if ("siae".equals(textType)) {
			return codiceSiae;
		} else if ("ada".equals(textType)) {
			return codiceAda;
		} else if ("codice".equals(textType)) {
			return codice;
		}
		
		//nuovo
		if ("title_original".equals(textType)) return title_original;
		if ("title_alternative".equals(textType)) return title_alternative;
		// artist(s)
		if ("artist_author".equals(textType)) return artist_author;
		if ("artist_composer".equals(textType)) return artist_composer;
		if ("artist_performer".equals(textType)) return artist_performer;
		return undefined;
	}
	
	public static String toString(int textType) {
		switch (textType) {
		case iswc: return "iswc";
		case isrc: return "isrc";
		case title: return "title";
		case artist: return "artist";
		case uuid: return "uuid";
		case codiceSiae: return "siae";
		case codiceAda: return "ada";
		case codice: return "codice";
		default: return "undefined";
		}
	}
	
}
