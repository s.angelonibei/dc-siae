package com.alkemytech.sophia.identification.work;

import java.io.File;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.identification.model.Work;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.je.SequenceConfig;
import com.sleepycat.persist.EntityCursor;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.StoreConfig;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class BdbWorkDatabase implements WorkDatabase {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final File homeFolder;
	private final boolean readOnly;
	private final long cacheSize;
	private final int cachePercent;
	private final boolean deferredWrite;
	private final AtomicInteger startupCalls;

	private Environment environment;
	private EntityStore store;

	@Inject
	public BdbWorkDatabase(@Named("configuration") Properties configuration,
			@Named("work_database") String propertyPrefix) {
		super();
		logger.debug("propertyPrefix: {}", propertyPrefix);
		this.readOnly = "true".equalsIgnoreCase(configuration
				.getProperty(propertyPrefix + ".read_only", "true"));
		this.homeFolder = new File(configuration
				.getProperty(propertyPrefix + ".home_folder"));
		this.cacheSize = TextUtils.parseLongSize(configuration
				.getProperty(propertyPrefix + ".cache_size", "0"));
		this.cachePercent = Integer.parseInt(configuration
				.getProperty(propertyPrefix + ".cache_percent", "60"));
		this.deferredWrite = "true".equals(configuration
				.getProperty(propertyPrefix + ".deferred_write", "true"));
		this.startupCalls = new AtomicInteger(0);
	}

	@Override
	public BdbWorkDatabase startup() {
		if (0 == startupCalls.getAndIncrement()) {
			try {
				homeFolder.mkdirs();
				environment = new Environment(homeFolder,
						(EnvironmentConfig) EnvironmentConfig.DEFAULT
							.setAllowCreate(!readOnly)
							.setReadOnly(readOnly)
							.setCacheSize(cacheSize)
							.setCachePercent(cachePercent));
				store = new EntityStore(environment, "work",
						StoreConfig.DEFAULT
							.setAllowCreate(!readOnly)
							.setReadOnly(readOnly)
							.setDeferredWrite(deferredWrite)); 
				store.setSequenceConfig("id", 
						SequenceConfig.DEFAULT
							.setAllowCreate(!readOnly)
							.setInitialValue(1));
			} catch (DatabaseException e) {
				logger.error("startup", e);
			}
		}
		return this;
	}

	@Override
	public BdbWorkDatabase shutdown() {
		if (0 == startupCalls.decrementAndGet()) {
			try {
				if (null != store) {
					if (!readOnly) {
						store.sync();
					}
					store.close();
				}
			} catch (DatabaseException e) {
				logger.error("shutdown", e);
			}
			try {
				if (null != environment) {
					if (!readOnly) {
						environment.sync();
					}
					environment.close();
				}
			} catch (DatabaseException e) {
				logger.error("shutdown", e);
			}
		}
		return this;
	}
	
	@Override
	public boolean isReadOnly() {
		return readOnly;
	}
	
	@Override
	public File getHomeFolder() {
		return homeFolder;
	}

	@Override
	public long getNextId() {
		return store.getSequence("id").get(null, 1);
	}
	
	@Override
	public Work getById(long id) {
		final PrimaryIndex<Long, Work> primaryIndex = store
				.getPrimaryIndex(Long.class, Work.class);
		return primaryIndex.get(id);
	}
	
	@Override
	public Work upsert(Work work) {
		if (!Work.isValid(work))
			throw new IllegalArgumentException("work");
		final PrimaryIndex<Long, Work> primaryIndex = store
				.getPrimaryIndex(Long.class, Work.class);
		primaryIndex.putNoReturn(work);
		return work;
	}

	@Override
	public WorkDatabase truncate() {
		store.truncateClass(Work.class);
		return this;
	}

	@Override
	public WorkDatabase select(Work.Selector selector) throws Exception {
		final PrimaryIndex<Long, Work> primaryIndex = store
				.getPrimaryIndex(Long.class, Work.class);
		try (EntityCursor<Work> works = primaryIndex.entities()) {
			for (Work work = works.first();
					null != work;
					work = works.next()) {
				selector.select(work);
			}
		}
		return this;
	}

	@Override
	public boolean delete(long id) {
		final PrimaryIndex<Long, Work> primaryIndex = store
				.getPrimaryIndex(Long.class, Work.class);
		return primaryIndex.delete(id);
	}

	@Override
	public WorkDatabase defrag() {
		if (null != store && !readOnly)
			store.sync();
		return this;
	}
	
}
