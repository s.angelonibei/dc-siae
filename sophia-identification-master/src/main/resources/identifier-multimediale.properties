# identifier.properties
# WARNING: defaults loaded from resource classpath:/defaults.properties

# override(s)
default.environment=debug
default.environment_dtp=test
default.home_folder=/data/work/sophia/multimediale
MCMDB_debug.jdbc.url=jdbc:mysql://localhost:3306/MCMDB_dev?verifyServerCertificate=false&useSSL=true
sophia_kb_debug.jdbc.url=jdbc:mysql://localhost:3306/sophia_kb_debug?verifyServerCertificate=false&useSSL=true

work_database=nosql_work_database
nosql_work_database.home_folder=${identifier.index.local_folder}/work.nosql
nosql_work_database.cache_size=4K
nosql_work_database.read_only=true

collecting_database=nosql_collecting_database
nosql_collecting_database.home_folder=${identifier.index.local_folder}/siae.nosql
nosql_collecting_database.cache_size=4K
nosql_collecting_database.read_only=true

document_store=nosql_document_store
nosql_document_store.home_folder=${identifier.index.local_folder}/document.nosql
nosql_document_store.cache_size=4K
nosql_document_store.read_only=true

iswc_index.home_folder=${identifier.index.local_folder}/iswc.index
iswc_index.read_only=true

isrc_index.home_folder=${identifier.index.local_folder}/isrc.index
isrc_index.read_only=true

title_index.home_folder=${identifier.index.local_folder}/title.index
title_index.read_only=true

artist_index.home_folder=${identifier.index.local_folder}/artist.index
artist_index.read_only=true

uuid_index.home_folder=${identifier.index.local_folder}/uuid.index
uuid_index.read_only=true

# identifier
identifier.locked=false
#identifier.debug=true
identifier.bind_port=15151
identifier.thread_count=4
identifier.stats_frequency=10s
identifier.instance_id=instance-1

identifier.identificationProvider=identification_v3

# sqs message pump
identifier.sqs.extends_messages=true
identifier.sqs.extends_buket_name=siae-sophia-sqs-messages
identifier.sqs.max_messages_per_run=10
identifier.sqs.max_polling_duration=4m30s
identifier.sqs.polling_period=10s
identifier.sqs.sender_name=sophia-identification
identifier.sqs.service_bus_queue=${default.environment}_service_bus
identifier.sqs.to_process_queue=${default.environment}_to_process_core_identification
identifier.sqs.started_queue=${default.environment}_started_core_identification
identifier.sqs.completed_queue=${default.environment}_completed_core_identification
identifier.sqs.failed_queue=${default.environment}_failed_core_identification


# multiple items message(s)
identifier.multi.hide_split_messages=true
identifier.multi.cleanup_database=true
identifier.multi.abort_on_failed=true

identifier.multi.sql.insert_split_message=\
	insert into COREID_SPLIT_MESSAGE (RECEIVED_MESSAGE) values ({receivedMessage})

identifier.multi.sql.insert_split_message_part=\
	insert into COREID_SPLIT_MESSAGE_PART(ID_SPLIT_MESSAGE) values ({idSplitMessage})

identifier.multi.sql.insert_split_message_part.with_node_info=\
	insert into COREID_SPLIT_MESSAGE_PART \
	     ( ID_SPLIT_MESSAGE \
	     , HOSTNAME \
	     , PID \
	     , INSTANCE_ID \
	     ) values \
	     ( {idSplitMessage} \
	     , {hostname} \
	     , {pid} \
	     , {instanceId} \
	     )

identifier.multi.sql.update_split_message_part.with_node_info=\
	update COREID_SPLIT_MESSAGE_PART \
	   set HOSTNAME = {hostname} \
	     , PID = {pid} \
	     , INSTANCE_ID = {instanceId} \
	 where ID_SPLIT_MESSAGE_PART = {idSplitMessagePart} \
	   and PENDING = 1
   
identifier.multi.sql.update_split_message_part=\
	update COREID_SPLIT_MESSAGE_PART \
	   set RESULT_JSON = {resultJson} \
	     , PENDING = 0 \
	     , FAILED = {failed} \
	 where ID_SPLIT_MESSAGE_PART = {idSplitMessagePart} \
	   and PENDING = 1

identifier.multi.sql.update_split_message=\
	update COREID_SPLIT_MESSAGE \
	   set PENDING = 0 \
	 where ID_SPLIT_MESSAGE = {idSplitMessage} \
	   and PENDING = 1
   
identifier.multi.sql.select_pending_parts=\
	select count(*) as pendingParts \
	  from COREID_SPLIT_MESSAGE_PART \
	 where ID_SPLIT_MESSAGE = {idSplitMessage} \
	   and PENDING = 1

identifier.multi.sql.select_failed_parts=\
	select count(*) as failedParts \
	  from COREID_SPLIT_MESSAGE_PART \
	 where ID_SPLIT_MESSAGE = {idSplitMessage} \
	   and PENDING = 0 \
	   and FAILED = 1

identifier.multi.sql.select_split_message=\
	select RECEIVED_MESSAGE as receivedMessage \
	  from COREID_SPLIT_MESSAGE \
	 where ID_SPLIT_MESSAGE = {idSplitMessage}

identifier.multi.sql.select_split_message_part=\
	select RESULT_JSON as resultJson \
	     , FAILED as failed \
	  from COREID_SPLIT_MESSAGE_PART \
	 where ID_SPLIT_MESSAGE = {idSplitMessage} \
	 order by ID_SPLIT_MESSAGE_PART asc
 
identifier.multi.sql.delete_split_message_part=\
	delete from COREID_SPLIT_MESSAGE_PART \
	 where ID_SPLIT_MESSAGE = {idSplitMessage}

identifier.multi.sql.delete_split_message=\
	delete from COREID_SPLIT_MESSAGE \
	 where ID_SPLIT_MESSAGE = {idSplitMessage}


# knowlede base
identifier.index.s3_folder_url=s3://siae-sophia-datalake/identification/${default.environment_dtp}/knowledge-base
identifier.index.local_folder=${default.home_folder}/identifier-kb
##identifier.index.local_folder=${default.home_folder}/index-updater-kb
identifier.index.latest_filename=_LATEST


# title artist query
title_artist_query=fuzzy_query

# exact query
exact_query.class_name=com.alkemytech.sophia.commons.query.ExactTitleArtistQuery
exact_query.max_terms=4
exact_query.min_terms=3

# fuzzy query
fuzzy_query.class_name=com.alkemytech.sophia.commons.query.FuzzyTitleArtistQuery
fuzzy_query.max_terms=4
fuzzy_query.min_terms=3
fuzzy_query.fuzzy_costs=6,12,18


# document score
document_score=custom_score

# linear score
# Wt * (St(title) ^ Et) + Wa * (Sa(artist) ^ Ea)
linear_score.class_name=com.alkemytech.sophia.identification.score.LinearDocumentScore
linear_score.title_weight=0.667
linear_score.title_exponent=1.0
linear_score.title_score=com.alkemytech.sophia.commons.score.TitleScore
linear_score.artist_weight=0.333
linear_score.artist_exponent=1.0
linear_score.artist_score=com.alkemytech.sophia.commons.score.ArtistScore

# geometric score
# (W * St(title) * Sa(artist)) ^ E
geometric_score.class_name=com.alkemytech.sophia.identification.score.GeometricDocumentScore
geometric_score.weight=1.0
geometric_score.exponent=0.42
geometric_score.title_score=com.alkemytech.sophia.commons.score.TitleScore
geometric_score.artist_score=com.alkemytech.sophia.commons.score.ArtistScore

# custom score
# Wt * (Mt(title) ^ Et) + Wa * (Ma(artist) ^ Ea) + Wv * (Nv(value) ^ Ev) + Wr * (Ir(value) ^ Er)  
custom_score.class_name=com.alkemytech.sophia.identification.score.CustomDocumentScore
custom_score.title_weight=0.667
custom_score.title_exponent=1.0
custom_score.title_score=com.alkemytech.sophia.commons.score.TitleScore

custom_score.artist_weight=0.333
custom_score.artist_exponent=1.0
custom_score.artist_score=com.alkemytech.sophia.commons.score.ArtistScore

custom_score.relevance_weight=0.333
custom_score.relevance_exponent=1.0
custom_score.relevance.period= 4
custom_score.relevance_flag=0

custom_score.risk_weight=0.333
custom_score.risk_exponent=1.0
custom_score.risk.period= 4
custom_score.risk_flag=0

custom_score.maturato.threshold.1=1000
custom_score.maturato.threshold.2=10000
custom_score.maturato.threshold.3=100000
custom_score.maturato.threshold.4=100000

custom_score.rischio.threshold.1=0.25
custom_score.rischio.threshold.2=0.5
custom_score.rischio.threshold.3=0.5

# identification result(s)
identifier.min_score.iswc=0.667
identifier.min_score.isrc=0.667
identifier.min_score.title_artist=0.665
identifier.max_postings=4K
identifier.max_results=1
identifier.auto_score=0.999


# input
identifier.input.rows_to_skip=0
identifier.input.delimiter=;

# WARNING: must override input column(s) inside identification request config file

# input columns (generic)
identifier.input.titles=default_column_titles
#identifier.input.title_types=default_column_title_types
identifier.input.artists=default_column_artists
#identifier.input.artist_types=default_column_artist_types
identifier.input.iswc=default_column_iswc
identifier.input.isrc=default_column_isrc
#dentifier.input.code_types=default_column_isrc_types,default_column_iswc_types

# csv record parser parameter(s)
# <prefix>.column=<zero based index of column; if not specified column value is not read from file>
# <prefix>.default_value=<default value for columns not read from file; if not specified column value is null>
# <prefix>.empty_value=<default value for empty columns; if not specified column value is empty>
# <prefix>.split_regex=<regular expression used to split columns containing multiple values; if not specified column is not split>
# <prefix>.mapping=<json containing mapping to decode column values; if not specified column value is not mapped>
# <prefix>.default_mapping=<default value for unmapped values; if not specified unmapped column value remains unchanged>

# valid title type(s): album, song, series, movie
# valid artist type(s): author, composer, performer, arranger, unknown
# valid code type(s): iswc, isrc, siae

# input columns layout (generic)
# titles
default_column_titles.column=56
default_column_titles.split_regex=\\|
# title types
#default_column_title_types.column=1
#default_column_title_types.split_regex=\\|
# artists
default_column_artists.column=57
default_column_artists.split_regex=\\|
# artist types
#default_column_artist_types.column=9
#default_column_artist_types.split_regex=\\|
# codes
default_column_iswc.column=10
default_column_iswc.split_regex=\\|
default_column_isrc.column=12
default_column_isrc.split_regex=\\|
# code types
#default_column_iswc_types.column=10
#default_column_iswc_types.split_regex=\\|
#default_column_isrc_types.column=12
#default_column_isrc_types.split_regex=\\|


# output
identifier.output.copy_skipped_rows=true
identifier.output.echo_parsed_rows=true
identifier.output.delimiter=;

# WARNING: must override result format(s) inside identification request config file

identifier.output.result_formats=default_match_type,default_work_codes
# valid result position(s): append | replace:<column> | insert:<column>
identifier.output.result_positions=append,append

# default match type
default_match_type.class_name=com.alkemytech.sophia.identification.result.MatchTypeFormat
#default_match_type.mapping={"unidentified":"KO", "work_code":"OK", "iswc":"OK", "isrc":"OK", "title_artist":"OK"}
#default_match_type.default_mapping=KO

# default results count
default_results_count.class_name=com.alkemytech.sophia.identification.result.ResultsCountFormat

# default work codes
default_work_codes.class_name=com.alkemytech.sophia.identification.result.WorkCodesMultimedialeFormat
default_work_codes.delimiter=|
#default_work_codes.score_delimiter=:
#default_work_codes.print_score=true

# default json with scores
default_json_with_scores.class_name=com.alkemytech.sophia.identification.result.JsonWithScoresFormat
# missing or empty mapping(s) are removed from output
default_json_with_scores.mapping={\
	"#match_type":"tipoCodifica",\
	"work_code":"codiceOpera",\
	"score":"gradoDiConf",\
	"title":"titolo",\
	"author":"autore",\
	"composer":"compositore",\
	"performer":"interprete",\
	"title_score":"confTitolo",\
	"author_score":"confAutore",\
	"composer_score":"confCompositore",\
	"performer_score":"confInterprete",\
	"maturato":"maturato",\
    "rischio":"rischio",\
    "flagPD":"flagPD",\
    "flagEL":"flagEL",\
    "titoloOriginale":"titoloOriginale",\
    "statoOpera":"statoOpera",\
    "flagIrregolare:"flagIrregolare",\
    "flagDoppioDeposito:"flagDoppioDeposito"\
}
default_json_with_scores.title_score=com.alkemytech.sophia.commons.score.TitleScore
default_json_with_scores.artist_score=com.alkemytech.sophia.commons.score.ArtistScore
default_json_with_scores.score_scale=100
default_json_with_scores.score_string_format=%.1f



identifier.standalone_json={\
		  "header": { \
		    "uuid": "6479449c-221d-4600-92c1-1c8d1f7a6c24",\
		    "timestamp": "2018-09-18T00:00:00.000Z",\
		    "queue": "dev_to_process_core_identification",\
		    "sender": "che ne so"\
		  },\
		  "body": {\
		    "version": "1",\
		    "compression": "gz",\
		    "items":[\
		      {\
		        "inputUrl":"s3://siae-sophia-datalake/identification/test/in/part-00098-9bc36570-cb08-4069-ac6a-55251a96d5e2.csv",\
		        "identifiedOutputUrl":"s3://siae-sophia-datalake/identification/test/out/idepart-00098-9bc36570-cb08-4069-ac6a-55251a96d5e2.csv",\
		        "unidentifiedOutputUrl":"s3://siae-sophia-datalake/identification/test/out/unipart-00098-9bc36570-cb08-4069-ac6a-55251a96d5e2.csv",\
	            "idSplitMessage" : 261,\
	            "idSplitMessagePart" : 3647,\
		        "__comment": "optional additional field(s) will be echoed to other sqs messages inside "\
		      }\
		    ],\
		    "skipServiceBus": true,\
		    "__comment": "optional additional field(s) will be echoed to other sqs messages inside "\
		  } \
		}
