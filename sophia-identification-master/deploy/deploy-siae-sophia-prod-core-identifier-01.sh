#!/bin/sh

DEPLOY_DIR=/home/roberto.cerfogli/sophia-identification
DEPLOY_ADDR=10.3.16.216

scp -r prod/* roberto.cerfogli@$DEPLOY_ADDR:$DEPLOY_DIR
scp -r lib roberto.cerfogli@$DEPLOY_ADDR:$DEPLOY_DIR
#scp -r lib/sophia*.jar roberto.cerfogli@$DEPLOY_ADDR:$DEPLOY_DIR/lib
scp -r sophia*.jar roberto.cerfogli@$DEPLOY_ADDR:$DEPLOY_DIR

ssh roberto.cerfogli@$DEPLOY_ADDR



