#!/bin/sh

HOME=/home/sophia/sophia-identification

JPACKAGE=com.alkemytech.sophia.identification
JCLASS=IndexUpdater
JCONFIG=index-updater.properties
#JSTDOUT=$HOME/index-updater.out
JSTDOUT=/dev/null
JOPTIONS="-Xmx16G"
JVERSION=1.0.2

#cd $HOME

nohup java -cp "$HOME/sophia-identification-$JVERSION.jar:$HOME/lib/*" $JOPTIONS $JPACKAGE.$JCLASS $HOME/$JCONFIG 2>&1 >> $JSTDOUT &


