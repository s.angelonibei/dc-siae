#!/bin/sh

HOME=/home/roberto.cerfogli/sophia-identification/dev

JPACKAGE=com.alkemytech.sophia.identification
JCLASS=InteractiveSearch
JCONFIG=interactive-search.properties
JOPTIONS="-Xmx4G"
JVERSION=1.0.2

#cd $HOME

java -cp "$HOME/sophia-identification-$JVERSION.jar:$HOME/lib/*" $JOPTIONS $JPACKAGE.$JCLASS $HOME/$JCONFIG


