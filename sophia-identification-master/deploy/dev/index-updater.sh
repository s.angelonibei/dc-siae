#!/bin/sh

HOME=/home/roberto.cerfogli/sophia-identification/dev

JPACKAGE=com.alkemytech.sophia.identification
JCLASS=IndexUpdater
JCONFIG=index-updater.properties
#JSTDOUT=$HOME/index-updater.out
JSTDOUT=/dev/null
JOPTIONS="-Xms2G -Xmx24G"
JVERSION=1.0.2

#cd $HOME

nohup java -cp "$HOME/sophia-identification-$JVERSION.jar:$HOME/lib/*" $JOPTIONS $JPACKAGE.$JCLASS $HOME/$JCONFIG 2>&1 >> $JSTDOUT &


