package com.alkemy.sophia.loader.sap;

import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Scopes;
import com.google.inject.name.Named;

import java.util.Properties;

public class Test {



    private static class GuiceModuleExtension extends GuiceModule {

        public GuiceModuleExtension(String[] args, String resourceName) {
            super(args, resourceName);
        }

        @Override
        protected void configure() {
            super.configure();
            // amazon service(s)
            bind(SQS.class)
                    .in(Scopes.SINGLETON);

            bind(Loader.class).asEagerSingleton();
        }

    }

    private final SQS sqs;
    private final Properties configuration;

    @Inject
    protected Test(@Named("configuration") Properties configuration,
                     @Named("default.bind_port") int lockPort,
                     SQS sqs) {
        super();
        this.configuration = configuration;
        this.sqs = sqs;
    }

    public static void main(String[] args) {
        try {
            final Injector injector = Guice
                    .createInjector(new Test.GuiceModuleExtension(args, "/configuration.properties"));
            final Test instance = injector
                    .getInstance(Test.class)
                    .startup();




        } catch (Throwable e) {

        } finally {
            System.exit(0);
        }
    }

    private Test startup() {
        sqs.startup();

        final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "saploader.sqs");

        JsonObject jsObj = new JsonObject();
        jsObj.addProperty("fileName","file_bello_da_sucare.csv");

        sqsMessagePump.sendToProcessMessage(jsObj,false);
        return this;
    }
}
