package com.alkemy.sophia.loader.sap;

import java.io.File;
import java.net.ServerSocket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import com.alkemytech.sophia.commons.util.GsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Scopes;
import com.google.inject.name.Named;

public class Loader {

    private static final Logger logger = LoggerFactory.getLogger(Loader.class);

    private static class GuiceModuleExtension extends GuiceModule {

        public GuiceModuleExtension(String[] args, String resourceName) {
            super(args, resourceName);
        }

        @Override
        protected void configure() {
            super.configure();
            // amazon service(s)
            bind(S3.class)
                    .in(Scopes.SINGLETON);
            bind(SQS.class)
                    .in(Scopes.SINGLETON);

            bind(Loader.class).asEagerSingleton();
        }

    }

    public static void main(String[] args) {
        try {
            final Injector injector = Guice
                    .createInjector(new GuiceModuleExtension(args, "/configuration.properties"));
            final Loader instance = injector
                    .getInstance(Loader.class)
                    .startup();
            try {
                instance.process();
            } finally {
                instance.shutdown();
            }
        } catch (Throwable e) {
            logger.error("main", e);
        } finally {
            System.exit(0);
        }
    }

    private final Properties configuration;
    private final int lockPort;
    private final SQS sqs;
    private final S3 s3;

    @Inject
    protected Loader(@Named("configuration") Properties configuration,
                        @Named("default.bind_port") int lockPort,
                        SQS sqs,S3 s3) {
        super();
        this.configuration = configuration;
        this.lockPort = lockPort;
        this.sqs = sqs;
        this.s3 = s3;
    }

    private Loader startup() {
        sqs.startup();
        s3.startup();
        return this;
    }

    private Loader shutdown() {
        sqs.shutdown();
        s3.shutdown();
        return this;
    }

    private void process() throws Exception {

        logger.debug("process: start time {}",
                DateFormat.getTimeInstance().format(new Date()));

        try (final ServerSocket lockSocket = new ServerSocket(lockPort)) { // avoid duplicate executions
            final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "saploader.sqs");
            sqsMessagePump.pollingLoop(null, new SqsMessagePump.Consumer() {

                private JsonObject output;
                private JsonObject error;

                @Override
                public JsonObject getStartedMessagePayload(JsonObject message) {
                    output = null;
                    error = null;
                    return SqsMessageHelper.formatContext();
                }

                @Override
                public boolean consumeMessage(JsonObject message) {
                    try {
                        output = new JsonObject();
                        processMessage(message, output);
                        error = null;
                        return true;
                    } catch (Exception e) {
                        output = null;
                        error = SqsMessageHelper.formatError(e);
                        return false;
                    }
                }

                @Override
                public JsonObject getCompletedMessagePayload(JsonObject message) {
                    return output;
                }

                @Override
                public JsonObject getFailedMessagePayload(JsonObject message) {
                    return error;
                }

            });
        }

        logger.debug("dump: end time {}", DateFormat.getTimeInstance().format(new Date()));

    }

    private void processMessage(JsonObject input, JsonObject output) throws Exception {

//		{
//			"body": {
//				"pathfileIn": "/mnt/.....",
//				"pathFileOut": "s3:......"
//			},
//			"header": {
//				"queue": "dev_to_process_saploader",
//				"timestamp": "2017-05-09T01:15:00.000000+02:00",
//				"uuid": "b3e00ae5-5145-40e6-96d4-f63cac680aaa",
//				"sender": "aws-console"
//			}
//		}

        this.dump(input.getAsJsonObject("body"), output);

    }


    public void dump(JsonObject inputJsonBody, JsonObject outputJson) throws Exception {

        String fileName = GsonUtils.getAsString(inputJsonBody, "fileName");
        final String s3Bucket = configuration.getProperty("saploader.s3.bucket");
        final String s3Path = configuration.getProperty("saploader.s3.path");
        final String siaePath = configuration.getProperty("saploader.siae.path");


        logger.debug("dump: s3bucket {}", s3Bucket);
        logger.debug("dump: s3folder {}", s3Path);
        logger.debug("dump: filename {}", fileName);
        logger.debug("dump: siaePath{}", siaePath);

        // final DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        final File file = new File(siaePath+fileName);
        logger.debug("dump: file {}", file);

        if (!file.exists()) {
            throw new Exception("missing sap file");
        }
        uploadToS3AndDelete(s3Bucket,s3Path,file);
        outputJson.addProperty("siaePath", siaePath);
        outputJson.addProperty("filename", fileName);
        outputJson.addProperty("url", String.format("s3://%s/%s/%s", s3Bucket, s3Path, fileName));


    }

    private void uploadToS3AndDelete(String s3bucket, String s3folder, File file) {

        if ("true".equalsIgnoreCase(configuration.getProperty("saploader.s3.upload_flag"))) {
            final String key = String.format("%s/%s", s3folder, file.getName());
            if (s3.upload(new S3.Url(s3bucket, key), file)) {
                if ("true".equalsIgnoreCase(configuration.getProperty("saploader.siae.delete_on_success"))) {
                    file.delete();
                }
            }
        }

    }

}
