select a.proprietary_id,
 a.album_title,
 a.titolo,
 a.artist,
 a.roles,
 a.sales_count,
 a.isrc,
 a.dsp,
 a.id_dsr,
 a.id_util,
 a.siada_artist,
 a.siada_title,
 a.uuid from :table a, (
select sum(cast( sales_count as int)) as tot_sales, uuid from :table where uuid <> ''
group by uuid
HAVING sum(cast( sales_count as int)) >= :minValue
order by tot_sales desc
limit :limitValue) t
where t.uuid = a.uuid
and cast(sales_count as int) >= :minValue

