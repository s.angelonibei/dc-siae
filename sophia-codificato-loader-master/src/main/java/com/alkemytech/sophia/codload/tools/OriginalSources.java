package com.alkemytech.sophia.codload.tools;

import java.io.PrintStream;
import java.util.List;
import java.util.Properties;

import com.alkemytech.sophia.common.rest.RestClient;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.codload.GuiceModule;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;

public class OriginalSources {
	
	private final Properties configuration;
	private final S3Service s3Service;

	@Inject
	protected OriginalSources(@Named("configuration") Properties configuration,
			S3Service s3Service) {
		super();
		this.configuration = configuration;
		this.s3Service = s3Service;
	}
	
	public void dump() throws Exception {
		
		final String url = configuration.getProperty("original_sources.ws.url",
				"http://gestioneutilizzazioni.alkemytech.it/cruscottoUtilizzazioni/rest/dsrMetadata/extractDsrMetadata?dsrName={dsr}&dsp={dsp}");
		final String s3Bucket = configuration.getProperty("original_sources.s3.bucket", "siae-sophia-datalake");
		final String s3Key = configuration.getProperty("original_sources.s3.key", "original-source");
		
		final PrintStream out = System.out;
//		out = new PrintStream(new FileOutputStream("original-source.csv"));

		final List<S3ObjectSummary> listing = s3Service.listObjects(s3Bucket, s3Key);
		for (S3ObjectSummary objectSummary : listing) {
			String dsr = objectSummary.getKey();
			String dsp = dsr;
			int slash = dsp.indexOf('/');
			if (slash > 0) {
				dsp = dsp.substring(slash + 1);
				slash = dsp.indexOf('/');
				if (slash > 0) {
					dsp = dsp.substring(0, slash);
					slash = dsr.lastIndexOf('/');
					if (slash > 0) {
						dsr = dsr.substring(slash + 1);
						int dot = dsr.indexOf('.');
						if (dot > 0) {
							dsr = dsr.substring(0, dot);
							if (!dsr.endsWith("_V") && dsr.length() > 0) {
								final RestClient.Result response = RestClient
										.get(url.replace("{dsr}", dsr).replace("{dsp}", dsp));
								if (200 == response.getStatusCode()) {
									final JsonObject resultJson = response.getJsonElement().getAsJsonObject();
									//out.println("dsp=" + dsp + ", dsr=" + dsr + ", result=" + resultJson);
									if ("OK".equals(resultJson.get("status").getAsString())) {
										out.print(dsr);
										out.print(";");
										out.print(dsp);
										out.print(";");
										out.print(resultJson.get("dspCode").getAsString());
										out.print(";");
										out.print(resultJson.get("year").getAsString());
										out.print(";");
										String periodType = resultJson.get("periodType").getAsString();
										if ("quarter".equalsIgnoreCase(periodType)) {
											out.print("Q" + resultJson.get("period").getAsString());
										} else {
											out.print(resultJson.get("period").getAsString());
										}
										out.print(";");
										out.print(resultJson.get("country").getAsString());
										out.println();
										continue;
									}
								}
								out.print(dsr);
								out.print(";");
								out.print(dsp);
								out.println(";;;;");
							}
						}
					}
				}
			}
	    }
	}

	public static void main(String[] args) throws Exception {
		try {	
			// guice injector
			final Injector injector = Guice.createInjector(new GuiceModule(args));
			// startup service(s)
			//injector.getInstance(SQSService.class).startup();
			injector.getInstance(S3Service.class).startup();
			try {
				injector.getInstance(OriginalSources.class).dump();
			} finally {
				// shutdown service(s)
				//injector.getInstance(SQSService.class).shutdown();
				injector.getInstance(S3Service.class).shutdown();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.exit(0);
		}
	}

}
