package com.alkemytech.sophia.codload;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.knallgrau.utils.textcat.TextCategorizer;

import com.carrotsearch.labs.langid.LangIdV3;
import com.google.common.base.Optional;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.optimaize.langdetect.LanguageDetector;
import com.optimaize.langdetect.LanguageDetectorBuilder;
import com.optimaize.langdetect.i18n.LdLocale;
import com.optimaize.langdetect.ngram.NgramExtractors;
import com.optimaize.langdetect.profiles.LanguageProfile;
import com.optimaize.langdetect.profiles.LanguageProfileReader;
import com.optimaize.langdetect.text.CommonTextObjectFactories;
import com.optimaize.langdetect.text.TextObjectFactory;

public class LanguageGuess {

	private static final int TEXTCAT = 0;
	private static final int LANGID_JAVA = 1;
	private static final int LANG_DETECT = 2;

	private int libraryId;
	private TextCategorizer textCategorizer;
	private LangIdV3 langIdV3;
	private TextObjectFactory textObjectFactory;
	private LanguageDetector languageDetector;
	
	@Inject
	protected LanguageGuess(@Named("configuration") Properties configuration) {
		super();
		final String library = configuration.getProperty("language.library", "textcat");
		if ("textcat".equalsIgnoreCase(library)) {
			this.libraryId = TEXTCAT;
			this.textCategorizer = new TextCategorizer();
		} else if ("langid-java".equalsIgnoreCase(library)) {
			this.libraryId = LANGID_JAVA;
			this.langIdV3 = new LangIdV3();
		} else if ("language-detector".equalsIgnoreCase(library)) {
			try {
				final List<LanguageProfile> languageProfiles = new LanguageProfileReader().readAllBuiltIn();
				languageDetector = LanguageDetectorBuilder.create(NgramExtractors.standard())
				        .withProfiles(languageProfiles)
				        .build();
				this.libraryId = LANG_DETECT;
				this.textObjectFactory = CommonTextObjectFactories.forDetectingShortCleanText();
			} catch (IllegalStateException | IOException e) {
				throw new IllegalStateException("error initializing language.library " + library, e);
			}
		} else {
			throw new IllegalArgumentException("unknown language.library " + library);
		}
	}
	
	public boolean isItalianText(String text) {
		switch (libraryId) {
		case LANGID_JAVA: return isItalianLangIdJava(text);
		case TEXTCAT: return isItalianTextCat(text);
		case LANG_DETECT: return isItalianLangDetect(text);
		}
		return false;
	}
	
	private boolean isItalianLangIdJava(String text) {
		langIdV3.reset();
		return "it".equalsIgnoreCase(langIdV3.classify(text, false).getLangCode());
	}
	
	private boolean isItalianTextCat(String text) {
		return "italian".equalsIgnoreCase(textCategorizer.categorize(text));
	}

	private boolean isItalianLangDetect(String text) {
		final Optional<LdLocale> ldLocale = languageDetector.detect(textObjectFactory.forText(text));
		return null == ldLocale ? false :
			(ldLocale.isPresent() ? "it".equalsIgnoreCase(ldLocale.get().getLanguage()) : false);
	}
	
}
