package com.alkemytech.sophia.codload;

import java.util.Properties;
import java.util.TimeZone;

import javax.sql.DataSource;

import com.alkemytech.sophia.common.config.ConfigurationLoader;
import com.alkemytech.sophia.common.dbcp.DBCPDataSourceProvider;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.common.sqs.SQSService;
import com.alkemytech.sophia.common.tools.Log4j2Tools;
import com.alkemytech.sophia.codload.tools.OriginalSources;
import com.alkemytech.sophia.codload.tools.SqsLoader;
import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.google.inject.name.Names;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class GuiceModule extends AbstractModule {

	private final String[] args;
	
	public GuiceModule(String[] args) {
		super();
		this.args = args;
	}

	@Override
	protected void configure() {
		
		// properties
		final Properties properties = new ConfigurationLoader()
				.withCommandLineArgs(args).load();
		Names.bindProperties(binder(), properties);
		bind(Properties.class)
			.annotatedWith(Names.named("configuration"))
			.toInstance(properties);

		// default time zone
		TimeZone.setDefault(TimeZone.getTimeZone(properties.getProperty("defaultTimezone", "UTC")));
		
		// initialize log4j2
		Log4j2Tools.initialize(properties);
		
		// mysql datasource(s)
		bind(DataSource.class)
			.annotatedWith(Names.named("MCMDB"))
			.toProvider(new DBCPDataSourceProvider(properties, "MCMDB"))
			.in(Scopes.SINGLETON);
//		bind(DataSource.class)
//			.annotatedWith(Names.named("sophia_kb"))
//			.toProvider(new DBCPDataSourceProvider(properties, "sophia_kb"))
//			.in(Scopes.SINGLETON);
		bind(DataSource.class)
			.annotatedWith(Names.named("unidentified"))
			.toProvider(new DBCPDataSourceProvider(properties, "unidentified"))
			.in(Scopes.SINGLETON);
				
		// aws services
		bind(S3Service.class).asEagerSingleton();
		bind(SQSService.class).asEagerSingleton();

		// other
		bind(LanguageGuess.class);
		bind(CodificatoLoader.class).asEagerSingleton();
		bind(SqsLoader.class).asEagerSingleton();
		bind(OriginalSources.class).asEagerSingleton();
		
	}
	
}
