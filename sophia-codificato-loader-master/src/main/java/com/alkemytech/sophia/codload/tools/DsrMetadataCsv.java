package com.alkemytech.sophia.codload.tools;

import java.io.File;
import java.io.FileReader;
import java.io.PrintStream;
import java.util.Properties;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.alkemytech.sophia.common.rest.RestClient;
import com.alkemytech.sophia.codload.GuiceModule;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;

public class DsrMetadataCsv {
	
	private final Properties configuration;

	@Inject
	protected DsrMetadataCsv(@Named("configuration") Properties configuration) {
		super();
		this.configuration = configuration;
	}
	
	public void dump(String filename) throws Exception {
		
		final String url = configuration.getProperty("original_sources.ws.url",
				"http://gestioneutilizzazioni.alkemytech.it/cruscottoUtilizzazioni/rest/dsrMetadata/extractDsrMetadata?dsrName={dsr}&dsp={dsp}");
		
		final PrintStream out = System.out;
//		out = new PrintStream(new FileOutputStream("original-source.csv"));

		// parse input CSV file
		try (final FileReader reader = new FileReader(new File(filename))) {
			final CSVParser parser = new CSVParser(reader, CSVFormat.newFormat(';'));
			for (CSVRecord record : parser) {
				final String dsr = record.get(0);
				final String dsp = record.get(1);
				final RestClient.Result response = RestClient
						.get(url.replace("{dsr}", dsr).replace("{dsp}", dsp));
				if (200 == response.getStatusCode()) {
					final JsonObject resultJson = response.getJsonElement().getAsJsonObject();
					if ("OK".equals(resultJson.get("status").getAsString())) {
						out.print(dsr);
						out.print(";");
						out.print(dsp);
						out.print(";");
						out.print(resultJson.get("dspCode").getAsString());
						out.print(";");
						out.print(resultJson.get("year").getAsString());
						out.print(";");
						String periodType = resultJson.get("periodType").getAsString();
						if ("quarter".equalsIgnoreCase(periodType)) {
							out.print("Q" + resultJson.get("period").getAsString());
						} else {
							out.print(resultJson.get("period").getAsString());
						}
						out.print(";");
						out.print(resultJson.get("country").getAsString());
						out.println();
						continue;
					}
				}
				out.print(dsr);
				out.print(";");
				out.print(dsp);
				out.println(";;;;");
			}
			parser.close();
		}
	}

	public static void main(String[] args) throws Exception {
		try {	
			// guice injector
			final Injector injector = Guice.createInjector(new GuiceModule(args));
			// startup service(s)
//			injector.getInstance(SQSService.class).startup();
//			injector.getInstance(S3Service.class).startup();
//			try {
				injector.getInstance(DsrMetadataCsv.class)
						.dump(args[1]); // args[0] properties file, args[1] input csv file
//			} finally {
//				// shutdown service(s)
//				injector.getInstance(SQSService.class).shutdown();
//				injector.getInstance(S3Service.class).shutdown();
//			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.exit(0);
		}
	}

}
