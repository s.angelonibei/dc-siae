package com.alkemytech.sophia.codload;

import lombok.Data;

@Data
public class GenericSaleLine implements SaleLine {

	private String idUtil;			// id utilizzazione sophia
	private String proprietaryId;		// 0rojbLrI4NCPE3rWUmCG8O
	private String title;				// Up Around The Bend
	private String artists;			// John Fogerty|creedence clearwater revival
	private String roles;				// Composer|MainArtist
	private String albumTitle;		// Cosmo's Factory
	private String iswc;
	private String isrc;
	private String salesCount;		// 2
	private String siadaTitle;		// UP AROUND THE BEND
	private String siadaArtists;	 	// JOHN FOGERTY|CREEDENCE CLEARWATER REVIVAL
	private String dsp;
	private String idDsr;
	private String UUID;
}
