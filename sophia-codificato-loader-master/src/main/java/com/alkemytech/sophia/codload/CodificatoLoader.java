package com.alkemytech.sophia.codload;

import com.alkemytech.sophia.common.dao.DsrInfo;
import com.alkemytech.sophia.common.dao.McmdbDAO;
import com.alkemytech.sophia.common.rest.RestClient;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.common.sqs.Iso8601Helper;
import com.alkemytech.sophia.common.sqs.SQSMessage;
import com.alkemytech.sophia.common.sqs.SQSService;
import com.alkemytech.sophia.common.tools.StringTools;
import com.amazonaws.services.s3.AmazonS3URI;
import com.google.common.collect.Lists;
import com.google.gson.*;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import lombok.SneakyThrows;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.URLDecoder;
import java.net.UnknownHostException;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class CodificatoLoader {
	
	private static final Logger logger = LoggerFactory.getLogger(CodificatoLoader.class);

	private final Properties configuration;
	private final SQSService sqsService;
	private final S3Service s3Service;
	private final DataSource mcmdbDS;
	private final DataSource unidentifiedDS;
	
	@Inject
	public CodificatoLoader(@Named("configuration") Properties configuration,
							SQSService sqsService,
							S3Service s3Service,
							@Named("MCMDB") DataSource mcmdbDS,
							@Named("unidentified") DataSource unidentifiedDS) {
		super();
		this.configuration = configuration;
		this.sqsService = sqsService;
		this.s3Service = s3Service;
		this.mcmdbDS = mcmdbDS;
		this.unidentifiedDS = unidentifiedDS;
	}
	
	private String convert(Throwable e) {
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		return sw.toString();
	}

	private String getAsString(JsonObject json, String key) {
		final JsonElement element = null == json ? null : json.get(key);
		return null == element ? null : element.getAsString();
	}
	
	static class Stats {
		
		final AtomicLong rows = new AtomicLong(0);
		final AtomicLong inserted = new AtomicLong(0);
		final AtomicLong updated = new AtomicLong(0);
		final AtomicLong duplicated = new AtomicLong(0);
		
		Stats update (Stats other) {
			rows.addAndGet(other.rows.get());
			inserted.addAndGet(other.inserted.get());
			updated.addAndGet(other.updated.get());
			duplicated.addAndGet(other.duplicated.get());
			return this;
		}
		
		void addProperties(JsonObject output) {
			output.addProperty("totalRows", rows.get());
			output.addProperty("insertedSongs", inserted.get());
			output.addProperty("updatedSongs", updated.get());
			output.addProperty("duplicatedSongs", duplicated.get());
		}

	}
	
	private String getHostName() {
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			return "localhost";
		}
	}
	
	private JsonObject formatFailedJson(JsonObject input, String code, String description, Exception e) {
		// send output message to failed queue
		final JsonObject header = new JsonObject();
		header.addProperty("uuid", UUID.randomUUID().toString());
		header.addProperty("timestamp", Iso8601Helper.format(new Date()));
		header.addProperty("queue", configuration.getProperty("sqs.queue.failed"));
		header.addProperty("sender", "codificato-loader");
		header.addProperty("hostname", getHostName());
		final JsonObject error = new JsonObject();
		error.addProperty("code", code);
		error.addProperty("description", description);
		if (null != e) {
			error.addProperty("stackTrace", convert(e));
		}
		final JsonObject message = new JsonObject();
		message.add("header", header);
		message.add("input", input);
		message.add("error", error);
		return message;
	}
	
	private JsonObject formatStartedJson(JsonObject input) {
		final JsonObject header = new JsonObject();
		header.addProperty("uuid", UUID.randomUUID().toString());
		header.addProperty("timestamp", Iso8601Helper.format(new Date()));
		header.addProperty("queue", configuration.getProperty("sqs.queue.started"));
		header.addProperty("sender", "codificato-loader");
		header.addProperty("hostname", getHostName());
		final JsonObject message = new JsonObject();
		message.add("header", header);
		message.add("input", input);
		return message;
	}
	
	private JsonObject formatCompletedJson(JsonObject input, JsonObject output) {
		final JsonObject header = new JsonObject();
		header.addProperty("uuid", UUID.randomUUID().toString());
		header.addProperty("timestamp", Iso8601Helper.format(new Date()));
		header.addProperty("queue", configuration.getProperty("sqs.queue.completed"));
		header.addProperty("sender", "codificato-loader");
		header.addProperty("hostname", getHostName());
		final JsonObject message = new JsonObject();
		message.add("header", header);
		message.add("input", input);
		message.add("output", output);
		return message;
	}
	
	/**
	 * Process all queued DSRs
	 */
	public void loadCodificato() throws Exception {

		// connect to db
		try (final ServerSocket socket = new ServerSocket(Integer.parseInt(configuration.getProperty("listenPort", "0")));
				final Connection mcmdb = mcmdbDS.getConnection()) {
			logger.info("loadCodificato: listening on {}", socket.getLocalSocketAddress());
			logger.info("loadCodificato: connected to {} {}", mcmdb.getMetaData()
					.getDatabaseProductName(), mcmdb.getMetaData().getURL());
			
			// data access object
			final McmdbDAO mcmdbDAO = new McmdbDAO(mcmdb);
			
			// GSON
			final Gson gson = new GsonBuilder()
				.disableHtmlEscaping()
				.setPrettyPrinting()
				.create();
			
			// create or open queues
			final String pendingQueueName = configuration.getProperty("sqs.queue.pending");
			final String pendingQueueUrl = sqsService.getOrCreateUrl(pendingQueueName);
			logger.info("loadCodificato: pendingQueueUrl {}", pendingQueueUrl);
			final String serviceBusQueueUrl = sqsService.getOrCreateUrl(configuration.getProperty("sqs.queue.service_bus"));
			logger.info("loadCodificato: serviceBusQueueUrl {}", serviceBusQueueUrl);
			
			// main loop
			final int sqsMessagesPerRun = Integer.parseInt(configuration.getProperty("sqs.messagesPerRun", "1")); // default to 1 message per run
			final long runTimeoutSeconds = Long.parseLong(configuration.getProperty("runTimeoutSeconds", "3600")); // default to 1 hour run timeout
			final long startTimeMillis = System.currentTimeMillis();
			for (int nreceivedMessages = 0; nreceivedMessages < sqsMessagesPerRun &&
					System.currentTimeMillis() - startTimeMillis < TimeUnit.SECONDS.toMillis(runTimeoutSeconds); ) {
				final List<SQSMessage> sqsMessages = sqsService.receiveMessages(pendingQueueUrl);
				logger.info("loadCodificato: read {} messages", sqsMessages.size());
				if (null == sqsMessages || sqsMessages.isEmpty()) {
					Thread.sleep(5000L);
					continue;
				}
				for (SQSMessage sqsMessage : sqsMessages) {
					nreceivedMessages ++;
					try {
						// parse input message
						final String text = sqsMessage.getText();
						logger.info("loadCodificato: received sqs message {}", text);
						final JsonObject input = gson.fromJson(text, JsonObject.class);
						try {
							// de-duplicate message by uuid
							final String uuid = getAsString(input.getAsJsonObject("header"), "uuid");
							if (1 != mcmdbDAO.insertSqsDeduplication(uuid, pendingQueueName, text)) {
								logger.info("loadCodificato: duplicate message {}", uuid);
								// WARNING: do not send message to failed queue
//								final JsonObject message = formatFailedJson(input, "1",
//										"duplicate message: " + uuid, null);
//								sqsService.sendMessage(serviceBusQueueUrl, gson.toJson(message));
							} else {
								// send message to started queue
								sqsService.sendMessage(serviceBusQueueUrl,
										gson.toJson(formatStartedJson(input)));

								// process message
								logger.info("loadCodificato: processing message...");
								final JsonObject output = new JsonObject();
								output.addProperty("beginTimeMillis", System.currentTimeMillis());
								processMessage(input, output);
								output.addProperty("endTimeMillis", System.currentTimeMillis());
								logger.info("loadCodificato: message successfully processed");
									
								// send message to completed queue
								sqsService.sendMessage(serviceBusQueueUrl, 
										gson.toJson(formatCompletedJson(input, output)));									
							}
						} catch (Exception e) {
							logger.error("loadCodificato", e);
							// send message to failed queue
							final JsonObject message = formatFailedJson(input, "0",
									"error processing message: " + e.getMessage(), e);
							sqsService.sendMessage(serviceBusQueueUrl, gson.toJson(message));
						}
						// delete from pending queue
						sqsService.deleteMessage(pendingQueueUrl, sqsMessage.getReceiptHandle());
					} catch (Exception e) {
						logger.error("loadCodificato", e);
					}
				}
			}

			logger.info("loadCodificato: exiting after {}ms", System.currentTimeMillis() - startTimeMillis);
		}
	}

	private void processMessage(JsonObject input, JsonObject output) throws Exception {

//		{
//			"idDsr":"spotify_siae_IT_201604_familyplan2_dsr",
//			"dsp":"spotify",
//			"year":"2016",
//			"month":"04",
//			"country":"ITA",
//			"idUtilizationType":"DWN"
//		}

		final JsonObject body = input.getAsJsonObject("body");
		final String idDsr = getAsString(body, "idDsr");
		final String dsp = getAsString(body, "dsp");
		final String year = getAsString(body, "year");
		final String month = getAsString(body, "month");
		final String idUtilizationType = getAsString(body, "idUtilizationType");

		if (StringUtils.isEmpty(idDsr) ||
				StringUtils.isEmpty(dsp) ||
				StringUtils.isEmpty(year) ||
				StringUtils.isEmpty(month)) {
			throw new IllegalArgumentException("missing mandatory parameter(s)");
		}

		final Stats stats = new Stats();

		// DSR info
		final DsrInfo dsrInfo;
		try (final Connection mcmdb = mcmdbDS.getConnection()) {
			final McmdbDAO mcmdbDAO = new McmdbDAO(mcmdb);
			// load DSR info
			if (StringTools.isNullOrEmpty(idUtilizationType)) {
				dsrInfo = mcmdbDAO.getDsrInfo(idDsr);
				if (null == dsrInfo) {
					throw new IllegalArgumentException("unknown dsr " + idDsr);
				}
			} else {
				dsrInfo = new DsrInfo()
					.setIdDsr(idDsr)
					.setFtpSourcePath(dsp)
					.setYear(year)
					.setMonth(month)
					.setIdUtilizationType(idUtilizationType);
			}
		}
		logger.info("processMessage: dsrInfo {}", dsrInfo);

		if ("true".equalsIgnoreCase(configuration.getProperty("checkAlreadyProcessed"))) {
			String sql = "select INSERT_TIME" + " from DSR_LOAD_IDENTIFIED" + " where IDDSR = '" + idDsr + "'";
			try(Connection connection = mcmdbDS.getConnection()) {
				PreparedStatement stmt = connection.prepareStatement(sql);
				try{
					ResultSet res = stmt.executeQuery();
					if(res.isBeforeFirst()){
						logger.info("processMessage: dsr already processed {}", idDsr);
						stats.addProperties(output);
						output.addProperty("alreadyProcessed", true);
						output.addProperty("additionalInfo", "load_identified lines already loaded");
						return;
					}
				}catch (Exception e){
					logger.error("Error on checking if already exists");
				}
			}
		}
		logger.info("processMessage: dsrInfo {}", dsrInfo);
		
		// parse unidentified partition
		parse(dsrInfo, stats, new AmazonS3URI(configuration.getProperty("s3uri.input", "")
				.replace("{dsr}", idDsr).replace("{dsp}", dsp)
				.replace("{year}", year).replace("{month}", month)));

		try (final Connection mcmdb = mcmdbDS.getConnection()) {
			String sql = "insert into DSR_LOAD_IDENTIFIED (IDDSR, TOTAL_ROW) values ('" + idDsr + "','" + stats.rows.get() +"')" +
					" ON DUPLICATE KEY UPDATE IDDSR = '" + idDsr + "', INSERT_TIME = CURRENT_TIMESTAMP, TOTAL_ROW = " + stats.rows.get();
			logger.info("executing query " + sql);
			PreparedStatement stmt = mcmdb.prepareStatement(sql);
			try {
				int result = stmt.executeUpdate(sql);
				logger.info("processMessage: insertDsrLoadIdentified {}", result);
			} catch (Exception e) {
				logger.error("Error while saving to dsr_load_identified", e);
				throw new IllegalArgumentException(e.getMessage());
			} finally {
				stmt.close();
			}
		}

		stats.addProperties(output);
	}	

	private void parse(final DsrInfo dsr, final Stats stats, AmazonS3URI s3Uri) throws Exception {
		logger.info("parse: s3Uri {}", s3Uri);
		final String s3path = "s3a://" + s3Uri.getBucket() + "/" + URLDecoder.decode(s3Uri.getKey(), "UTF-8");
		Class.forName("org.apache.hive.jdbc.HiveDriver");
		String hTableName = "";
		List<GenericSaleLine> lineList;
		final RestClient.Result result = RestClient
				.get(configuration.getProperty("hive.clusterListRestUrlHive"));
		if (200 == result.getStatusCode()) {
			final JsonObject resultJson = result.getJsonElement().getAsJsonObject();
			logger.info("parse: resultJson {}", resultJson);
			if ("OK".equals(resultJson.get("status").getAsString())) {
				final JsonArray activeEmrClusters = resultJson.getAsJsonArray("activeEmrClusters");
				if (null != activeEmrClusters) for (JsonElement activeEmrCluster : activeEmrClusters) {
					final JsonObject clusterJson = (JsonObject) activeEmrCluster;
					logger.info("parse: clusterJson {}", clusterJson);
					String hiveJdbcUrl = clusterJson.get("hiveJdbcUrl").getAsString().concat("?hive.execution.engine=" + configuration.getProperty("hive.engine"));
					String hiveJdbcUser = clusterJson.get("hiveJdbcUser").getAsString();
					String hiveJdbcPassword = clusterJson.get("hiveJdbcPassword").getAsString();
					logger.info("parse: connecting to {}...", hiveJdbcUrl);
					DriverManager.setLoginTimeout(0);
					try (final Connection connection = DriverManager
							.getConnection(hiveJdbcUrl, hiveJdbcUser, hiveJdbcPassword)) {
						logger.info("parse: connected to {}", hiveJdbcUrl);
						try (final Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
								ResultSet.CONCUR_READ_ONLY)) {

								hTableName = createHiveTable(dsr, hiveJdbcUrl, hiveJdbcUser, hiveJdbcPassword, s3path);
								String extraction = readQueryToExtract(hTableName);

								ResultSet resultSet = statement.executeQuery(extraction);
								lineList = new ArrayList<>();

								logger.info("parse: Starting extract from query...");
								while (resultSet.next()){
									GenericSaleLine line = new GenericSaleLine();

									line.setProprietaryId(resultSet.getString("proprietary_id"));
									line.setAlbumTitle(resultSet.getString("album_title"));
									line.setTitle(resultSet.getString("titolo"));
									line.setArtists(resultSet.getString("artist"));
									line.setRoles(resultSet.getString("roles"));
									line.setSalesCount(resultSet.getString("sales_count"));
									line.setIsrc(resultSet.getString("isrc"));
									line.setDsp(resultSet.getString("dsp"));
									line.setIdDsr(resultSet.getString("id_dsr"));
									line.setIdUtil(resultSet.getString("id_util"));
									line.setSiadaArtists(resultSet.getString("siada_artist"));
									line.setSiadaTitle(resultSet.getString("siada_title"));
									line.setUUID(resultSet.getString("uuid"));

									stats.rows.incrementAndGet();
									lineList.add(line);
								}


								logger.info("resultSetSize: " + lineList.size());

								if (!lineList.isEmpty()){
									int maxThread = Integer.parseInt(configuration.getProperty("multiThread.max_thread"));
									int executorTimout = Integer.parseInt(configuration.getProperty("multiThread.executor_timeout"));
									int partitionSize = lineList.size() / maxThread;

									if (lineList.size() <= maxThread){
										partitionSize = lineList.size();
									}
									ExecutorService es = Executors.newCachedThreadPool();
									for (final List<GenericSaleLine> partitions : Lists.partition(lineList, partitionSize)) {

										es.execute(new Runnable() {
											@SneakyThrows
											@Override
											public void run() {
												logger.info("parse: starting thread: " + partitions.size() + " files");
											elaborateRecords(partitions, unidentifiedDS, stats);
											}
										});
									}
									es.shutdown();
									es.awaitTermination(executorTimout, TimeUnit.MINUTES); //EXECUTOR_TIMEOUT
								}
							}
						} catch (Exception e){
						logger.error("Error on extracting data ", e);
						throw new IllegalArgumentException(e.getMessage());
					} finally {
						logger.info("DROP TABLE " + hTableName);
						final Connection connection = DriverManager
								.getConnection(hiveJdbcUrl, hiveJdbcUser, hiveJdbcPassword);
						final Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
								ResultSet.CONCUR_READ_ONLY);
						statement.executeUpdate("DROP TABLE IF EXISTS " + hTableName);
					}
				}
			}
		}
	}

	public String createHiveTable(DsrInfo dsr, String hiveJdbcUrl, String hiveJdbcUser, String hiveJdbcPassword, String s3Path) throws IOException {
		String htablename = "";
		try (final Connection connection = DriverManager
				.getConnection(hiveJdbcUrl, hiveJdbcUser, hiveJdbcPassword)) {
			try (final Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY)) {
				ClassLoader classloader = Thread.currentThread().getContextClassLoader();
				InputStream f = classloader.getResourceAsStream("hive-identified-ddl.sql");
				BufferedReader br = new BufferedReader(new InputStreamReader(f));
				String line = br.readLine();
				StringBuilder sb = new StringBuilder();
				while (line != null) {
					sb.append(line).append("\n");
					line = br.readLine();
				}
				htablename = "codificato_" + dsr.getIdDsr().replace("-", "_");
				String hql = sb.toString().replace(":table", htablename).replace(":location", "'" + s3Path + "'");
				logger.info("parse : ready to run the script : " + hql);
				//Running the script
				statement.executeUpdate(hql);
				logger.info("parse : created table " + htablename);
				logger.info("parse : Ok ...");
			}
		}catch (Exception e){
			logger.error("Error connection to create table ", e);
			throw new IllegalArgumentException(e.getMessage());
		}
		return htablename;
	}

	public String readQueryToExtract(String tableName) throws IOException{
		String query = "";
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		InputStream f = classloader.getResourceAsStream("hive-extract-ddl.sql");
		BufferedReader br = new BufferedReader(new InputStreamReader(f));
		String line = br.readLine();
		StringBuilder sb = new StringBuilder();
		while (line != null) {
			sb.append(line).append("\n");
			line = br.readLine();
		}
		query = sb.toString().replace(":table", tableName)
				.replace(":minValue", configuration.getProperty("query.minValue"))
				.replace(":limitValue", configuration.getProperty("query.limitValue"));
		logger.info("creatingQuery: query created " + query);
		return query;
	}

	public void elaborateRecords(List<GenericSaleLine> partition, DataSource unidentifiedDS, Stats stats) throws Exception {
		final Connection unidentifiedConn = unidentifiedDS.getConnection();
		CodificatoDAO codDao = new CodificatoDAO(unidentifiedConn);
		for (GenericSaleLine line : partition){

			int count = codDao.insertLoadIdentifiedDsr(line.getIdDsr(), line.getDsp(), line);
			boolean inserted = (count == 1);
			boolean duplicate = (count == 0);
			if (!inserted || duplicate) {
				stats.duplicated.incrementAndGet();
				continue;
			}

			// insert into unidentified_song
			count = codDao.insertLoadIdentified(line, Long.parseLong(line.getSalesCount()));
			inserted = (1 == count);
			duplicate = (0 == count);

			// update existing line in unidentified_song
			boolean updated = false;
			if (duplicate) {
				count = codDao.updateIdentified(line);
				updated = (1 == count);
			}

			// delete from unidentified_song_dsr
			if (!inserted && !updated) {
				count = codDao.deleteIdentifiedDsr(line);
			}

			if (inserted) {
				stats.inserted.incrementAndGet();
			}
			if (updated) {
				stats.updated.incrementAndGet();
			}
		}
	}


	public static void main(String[] args) throws Exception {
		try {	
			final Injector injector = Guice.createInjector(new GuiceModule(args));
			injector.getInstance(SQSService.class).startup();
			injector.getInstance(S3Service.class).startup();
			try {
				injector.getInstance(CodificatoLoader.class)
					.loadCodificato();
			} finally {
				injector.getInstance(SQSService.class).shutdown();
				injector.getInstance(S3Service.class).shutdown();
			}
		} catch (Exception e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

}
