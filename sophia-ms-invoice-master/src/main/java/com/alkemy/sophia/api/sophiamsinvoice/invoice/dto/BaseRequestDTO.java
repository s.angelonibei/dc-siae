package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;

import lombok.Data;

@Data
public class BaseRequestDTO {
    private Integer currentPage;
    private Integer maxRows;
}
