package com.alkemy.sophia.api.sophiamsinvoice.entities;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "MM_RIPARTIZIONE_CARICO")
public class MmRipartizioneCarico {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "ID_RIPARTIZIONE_SIADA")
    private Long idRipartizioneSiada;
    @Column(name = "DATA_ORA_CREAZIONE")
    private Timestamp dataOraCreazione;
    @Column(name = "VALORE_TOTALE_RIPARTIZIONE")
    private BigDecimal valoreTotaleRipartizione;
    @Column(name = "STATO")
    @Enumerated(EnumType.STRING)
    private STATO stato;

    @Column(name = "VERSIONE")
    private Integer versione;


    public enum STATO{
       PRONTO,
       APPROVATO,
       ANNULLATO
    }
}
