package com.alkemy.sophia.api.sophiamsinvoice.repository;

import com.alkemy.sophia.api.sophiamsinvoice.entities.CCIDMetadata;
import com.alkemy.sophia.api.sophiamsinvoice.entities.CcidMetadataDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CCIDMetadataDetailsRepo extends JpaRepository<CcidMetadataDetails, Long> {
    List<CcidMetadataDetails> findAllByIdDsr(String idDsr);
}
