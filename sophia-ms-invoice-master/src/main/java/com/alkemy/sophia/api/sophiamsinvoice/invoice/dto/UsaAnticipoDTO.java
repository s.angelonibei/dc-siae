package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;

import lombok.Data;


import java.util.List;

@Data
public class UsaAnticipoDTO {

	private ImportiAnticipoDTO idAdvancePayment;
	private List<CcidInfoDTO> ccidList;

}
