package com.alkemy.sophia.api.sophiamsinvoice.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
//import java.sql.Timestamp;

@Entity(name="InvoiceReclamo")
@Table(name = "INVOICE_RECLAMO")
@Data
public class InvoiceReclamo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_INVOICE_RECLAMO", nullable = false)
    private Integer idInvoiceReclamo;

    @Basic
    @Column(name = "ID_DSP")
    private String idDsp;

    @Basic
    @Column(name = "COUNTRY")
    private String country;

    @Basic
    @Column(name = "DATA_INSERIMENTO")
    private Date dataInserimento;

    @Basic
    @Column(name = "DATA_ULTIMA_MODIFICA")
    private Date dataUltimaModifica;

    @Basic
    @Column(name = "UTENTE")
    private String utente;

    @Basic
    @Column(name = "PERIOD")
    private Integer period;

    @Basic
    @Column(name = "PERIOD_TYPE")
    private String periodType;

    @Basic
    @Column(name = "YEAR")
    private Integer year;

    @Basic
    @Column(name = "STATO")
    private Integer stato;

    @Basic
    @Column(name = "COMMERCIAL_OFFER")
    private String commercialOffer;

    @Basic
    @Column(name = "ID_DSR")
    private String idDsr;

    @Basic
    @Column(name = "FILE_LOCATION")
    private String fileLocation;

    @Basic
    @Column(name = "FILE_NAME")
    private String fileName;
}
