package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.List;

@Data
@ToString
public class CaricoRipartizioneDTO {
  private String idRipartizioneSiada;
  private List<InvoiceCCIDDTO> ccidList;

}
