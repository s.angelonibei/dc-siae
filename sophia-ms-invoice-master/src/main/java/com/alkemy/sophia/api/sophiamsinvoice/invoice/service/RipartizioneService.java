package com.alkemy.sophia.api.sophiamsinvoice.invoice.service;


import static com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.Tables.*;
import static org.jooq.impl.DSL.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import com.alkemy.sophia.api.sophiamsinvoice.entities.*;
import com.alkemy.sophia.api.sophiamsinvoice.invoice.dto.*;
import com.alkemy.sophia.api.sophiamsinvoice.repository.*;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.alkemy.sophia.api.sophiamsinvoice.entities.InvoiceItemToCcid;
import com.alkemy.sophia.api.sophiamsinvoice.entities.MmRipartizione648;
import com.alkemy.sophia.api.sophiamsinvoice.entities.MmRipartizioneCarico;
import com.alkemy.sophia.api.sophiamsinvoice.entities.MmRipartizioneCaricoCcid;
import com.alkemy.sophia.api.sophiamsinvoice.invoice.dto.CaricoCsvDTO;
import com.alkemy.sophia.api.sophiamsinvoice.invoice.dto.CaricoRipartizioneDTO;
import com.alkemy.sophia.api.sophiamsinvoice.invoice.dto.CaricoRipartizioneDettaglioDTO;
import com.alkemy.sophia.api.sophiamsinvoice.invoice.dto.InvoiceCCIDDTO;
import com.alkemy.sophia.api.sophiamsinvoice.invoice.dto.RequestListaCarichiDTO;
import com.alkemy.sophia.api.sophiamsinvoice.invoice.dto.StatisticaRipartizioneDTO;
import com.alkemy.sophia.api.sophiamsinvoice.invoice.dto.StatisticaRipartizioneRequestDTO;
import com.alkemy.sophia.api.sophiamsinvoice.repository.InvoiceRepo;
import com.alkemy.sophia.api.sophiamsinvoice.repository.MmRipartizione648Repo;
import com.alkemy.sophia.api.sophiamsinvoice.repository.MmRipartizioneCaricoCcidRepo;
import com.alkemy.sophia.api.sophiamsinvoice.repository.MmRipartizioneCaricoRepo;
import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;

import javax.persistence.EntityManager;

@Service
@RequiredArgsConstructor
@CommonsLog
public class RipartizioneService {
    private final MmRipartizioneCaricoRepo mmRipartizioneCaricoRepo;
    private final MmRipartizione648Repo mmRipartizione648Repo;
    private final MmRipartizioneCaricoCcidRepo mmRipartizioneCaricoCcidRepo;
    private final CCIDMetadataDetailsRepo ccidMetadataDetailsRepo;
    private final SocietaTutelaRepo societaTutelaRepo;
    private final InvoiceRepo invoiceRepo;
    private final InvoiceItemToCcidRepo invoiceItemToCcidRepo;
    private final DSLContext dsl;
    private final QueueMessagingTemplate messagingTemplate;
    private final EntityManager em;

    @Value("#{'${default.environment}'+ '_' + '${sqs.ripartizione}'}")
    private String oldQueueOutputName;
    @Value("#{'${default.environment}'+ '_' + '${sqs.ripartizionenew}'}")
    private String newQueueOutputName;    
    @Value("#{'${default.environment}'+ '_' + '${sqs.servicebus}'}")
    private String queueServiceBusName;
    @Value("${invoice.dsr-metadata-ws}")
    private String dsrMetadataWsURL;
    @Value("#{'s3://siae-sophia-datalake/' + '${default.environment}' + '/riparto-siada/'}")
    private String s3BaseOutputPath;


    @Value("${ripartizione.society.default}")
    private String DEFAULT_SOCIETY;

    @Transactional
    public void generaCarico(CaricoRipartizioneDTO dto) {

        //genera il carico
        Long idRipartizioneSiada = Long.parseLong(dto.getIdRipartizioneSiada());

        MmRipartizioneCarico lastCarico = mmRipartizioneCaricoRepo.findFirstByIdRipartizioneSiadaOrderByVersioneDesc(idRipartizioneSiada);
        List<MmRipartizioneCaricoCcid> listaCcidCarico = new ArrayList<>();

        MmRipartizioneCarico carico = new MmRipartizioneCarico();
        carico.setIdRipartizioneSiada(Long.parseLong(dto.getIdRipartizioneSiada()));

        if(lastCarico != null)
            carico.setVersione(lastCarico.getVersione()+1);
        else
            carico.setVersione(1);

        carico.setStato(MmRipartizioneCarico.STATO.PRONTO);
        carico.setDataOraCreazione(new Timestamp(new Date().getTime()));


        carico = mmRipartizioneCaricoRepo.saveAndFlush(carico);


        BigDecimal importoTotale = BigDecimal.ZERO;
        for (InvoiceCCIDDTO cciddto : dto.getCcidList()) {

            InvoiceItemToCcid invoiceItemToCcid = invoiceItemToCcidRepo.findById(cciddto.getIdCcidInvoiceItem().intValue()).get();
                        MmRipartizioneCaricoCcid mmRipartizioneCaricoCcid = new MmRipartizioneCaricoCcid();
                        mmRipartizioneCaricoCcid.setRipartizioneCarico(carico);
                        mmRipartizioneCaricoCcid.setInvoiceItemToCcid(invoiceItemToCcid);
                        importoTotale = importoTotale.add(invoiceItemToCcid.getValore());
                        listaCcidCarico.add(mmRipartizioneCaricoCcid);
                        log.info("carico " + carico.getId() + " aggiungo record invoice id: " + invoiceItemToCcid.getId());

        }

        carico.setValoreTotaleRipartizione(importoTotale);
        mmRipartizioneCaricoRepo.save(carico);

        mmRipartizioneCaricoCcidRepo.saveAll(listaCcidCarico);



    }


    public List<CaricoRipartizioneDettaglioDTO> listaCarichi(RequestListaCarichiDTO request){


        Condition condition = DSL.trueCondition();

        if(request.getMonth() > 0 && request.getYear() > 0){
            condition = condition.and(extract(MM_RIPARTIZIONE_CARICO.DATA_ORA_CREAZIONE,DatePart.MONTH).eq(request.getMonth()));
            condition = condition.and(extract(MM_RIPARTIZIONE_CARICO.DATA_ORA_CREAZIONE,DatePart.YEAR).eq(request.getYear()));
        }

        if(StringUtils.hasLength(request.getIdRipartizioneSiada())){
            condition = condition.and(MM_RIPARTIZIONE_CARICO.ID_RIPARTIZIONE_SIADA.eq(Long.parseLong(request.getIdRipartizioneSiada())));
        }

        if(request.getStato()!= null){
            condition = condition.and(MM_RIPARTIZIONE_CARICO.STATO.eq(request.getStato().toString()));
        }

        List<CaricoRipartizioneDettaglioDTO> result = dsl.select(
                MM_RIPARTIZIONE_CARICO.ID,
                MM_RIPARTIZIONE_CARICO.ID_RIPARTIZIONE_SIADA,
                MM_RIPARTIZIONE_CARICO.VERSIONE,
                MM_RIPARTIZIONE_CARICO.VALORE_TOTALE_RIPARTIZIONE,
                MM_RIPARTIZIONE_CARICO.DATA_ORA_CREAZIONE,
                MM_RIPARTIZIONE_CARICO.STATO,
                count(MM_RIPARTIZIONE_CARICO_CCID.ID).as("NUM_CCID"))
                .from(MM_RIPARTIZIONE_CARICO_CCID)
                .join(MM_RIPARTIZIONE_CARICO).on(MM_RIPARTIZIONE_CARICO.ID.eq(MM_RIPARTIZIONE_CARICO_CCID.ID_RIPARTIZIONE_CARICO))
                .where(condition)
                .groupBy(
                        MM_RIPARTIZIONE_CARICO.ID_RIPARTIZIONE_SIADA,
                        MM_RIPARTIZIONE_CARICO.VERSIONE,
                        MM_RIPARTIZIONE_CARICO.VALORE_TOTALE_RIPARTIZIONE
                )
                .orderBy(MM_RIPARTIZIONE_CARICO.DATA_ORA_CREAZIONE.desc()
                        ,MM_RIPARTIZIONE_CARICO.VERSIONE.desc())
                .limit(request.getMaxRows() + 1).offset(request.getCurrentPage() * request.getMaxRows())
                .fetchInto(CaricoRipartizioneDettaglioDTO.class);

        return result;
    }


    public List caricoCsv(Long idCarico){

        List<CaricoCsvDTO> result = dsl.select(
                MM_RIPARTIZIONE_CARICO.ID_RIPARTIZIONE_SIADA,
                MM_RIPARTIZIONE_CARICO.VERSIONE,
                DSR_METADATA.IDDSP,
                DSR_METADATA.PERIOD_TYPE,
                DSR_METADATA.PERIOD,
                DSR_METADATA.YEAR,
                DSR_METADATA.COUNTRY,
                ANAG_UTILIZATION_TYPE.NAME.as("UTILIZATION_TYPE"),
                COMMERCIAL_OFFERS.OFFERING.as("COMMERCIAL_OFFER"),
                DSR_METADATA.IDDSR.as("ID_DSR"),
                CCID_METADATA.VALORE_FATTURABILE,
                coalesce(IMPORTI_ANTICIPO.NUMERO_FATTURA,INVOICE.INVOICE_CODE).as("NUMERO_FATTURA"),
                INVOICE.DATE_OF_PERTINENCE,
                INVOICE.TOTAL_INVOICE,
                sum(INVOICE_ITEM_TO_CCID.VALORE).as("VALORE_FATTURA"),
                INVOICE_ITEM_TO_CCID.PERIODO_RIPARTIZIONE_ID
                )
                .from(MM_RIPARTIZIONE_CARICO_CCID)
                .join(MM_RIPARTIZIONE_CARICO).on(MM_RIPARTIZIONE_CARICO.ID.eq(MM_RIPARTIZIONE_CARICO_CCID.ID_RIPARTIZIONE_CARICO))
                .join(INVOICE_ITEM_TO_CCID).on(INVOICE_ITEM_TO_CCID.ID.eq(MM_RIPARTIZIONE_CARICO_CCID.ID_INVOICE_ITEM_TO_CCID))
                .join(CCID_METADATA).on(INVOICE_ITEM_TO_CCID.ID_CCID_METADATA.eq(CCID_METADATA.ID_CCID_METADATA))
                .join(DSR_METADATA).on(DSR_METADATA.IDDSR.eq(CCID_METADATA.ID_DSR))
                .join(COMMERCIAL_OFFERS).on(DSR_METADATA.SERVICE_CODE.eq(COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS))
                .join(ANAG_UTILIZATION_TYPE).on(COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE.eq(ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE))
                .join(INVOICE_ITEM).on(INVOICE_ITEM.ID_ITEM.eq(INVOICE_ITEM_TO_CCID.ID_INVOICE_ITEM))
                .join(INVOICE).on(INVOICE.ID_INVOICE.eq(INVOICE_ITEM.ID_INVOICE))
                .leftJoin(IMPORTI_ANTICIPO).on(IMPORTI_ANTICIPO.ID_INVOICE.eq(INVOICE.ID_INVOICE))
                .where(MM_RIPARTIZIONE_CARICO.ID.eq(idCarico))
                .groupBy(
                        MM_RIPARTIZIONE_CARICO.ID_RIPARTIZIONE_SIADA,
                        MM_RIPARTIZIONE_CARICO.VERSIONE,
                        DSR_METADATA.IDDSP,
                        DSR_METADATA.PERIOD_TYPE,
                        DSR_METADATA.PERIOD,
                        DSR_METADATA.YEAR,
                        DSR_METADATA.COUNTRY,
                        ANAG_UTILIZATION_TYPE.NAME,
                        COMMERCIAL_OFFERS.OFFERING,
                        DSR_METADATA.IDDSR,
                        CCID_METADATA.VALORE_FATTURABILE,
                        coalesce(IMPORTI_ANTICIPO.NUMERO_FATTURA,INVOICE.INVOICE_CODE),
                        INVOICE.TOTAL_INVOICE,
                        INVOICE_ITEM_TO_CCID.PERIODO_RIPARTIZIONE_ID)
                .orderBy(DSR_METADATA.IDDSP.asc()
                        ,INVOICE.DATE_OF_PERTINENCE.desc())
                .fetchInto(CaricoCsvDTO.class);

        if(result != null)
            for (CaricoCsvDTO dto : result) {
                dto.setPeriodStringCustom();
            }

        return result;
    }



    @Transactional(rollbackFor = Exception.class)
    public void avviaRipartizione(Long idCarico) {


        MmRipartizioneCarico carico = mmRipartizioneCaricoRepo.findById(idCarico).get();

        //i ccid_to_metadata vanno messi a RIPARTITO (idRipartizioneSiada)
        // + check se sono stati già ripartiti
        List<MmRipartizioneCaricoCcid> listaDaRipartire = mmRipartizioneCaricoCcidRepo.findByRipartizioneCaricoId(carico.getId());

        for (MmRipartizioneCaricoCcid mmRipartizioneCaricoCcid : listaDaRipartire) {
            if(mmRipartizioneCaricoCcid.getInvoiceItemToCcid().getPeriodoRipartizioneId()!= null)
                throw new RuntimeException("CCID/Fattura gia' ripatito in ("+mmRipartizioneCaricoCcid.getInvoiceItemToCcid().getPeriodoRipartizioneId()+"). Ripartizione annullata");

            mmRipartizioneCaricoCcid.getInvoiceItemToCcid().setPeriodoRipartizioneId(carico.getIdRipartizioneSiada());
        }


        //mettere a ripartito il record
        carico.setStato(MmRipartizioneCarico.STATO.APPROVATO);

        List<MmRipartizioneCarico> listaCarichiDaAnnullare = mmRipartizioneCaricoRepo.findByIdRipartizioneSiadaAndIdNot(carico.getIdRipartizioneSiada(),carico.getId());

        //gli altri record con stesso id vanno messi ad ANNULLATO
        if(!CollectionUtils.isEmpty(listaCarichiDaAnnullare))
            for (MmRipartizioneCarico caricoNullo : listaCarichiDaAnnullare)
                caricoNullo.setStato(MmRipartizioneCarico.STATO.ANNULLATO);


         //prendo tutti le sum (valore) di INVOICE_ITEM_TO_CCID per fattura-ccid. - + totale CCID
        List<RipartizioneDTO> listaCcid = dsl.select(
                sum(INVOICE_ITEM_TO_CCID.VALORE).as("VALORE_TOT"),
                coalesce(IMPORTI_ANTICIPO.NUMERO_FATTURA,INVOICE.INVOICE_CODE).as("ID_INVOICE"),
                INVOICE.DATE_OF_PERTINENCE,
                CCID_METADATA.ID_DSR
                )
                .from(INVOICE_ITEM_TO_CCID)
                .join(MM_RIPARTIZIONE_CARICO_CCID).on(MM_RIPARTIZIONE_CARICO_CCID.ID_INVOICE_ITEM_TO_CCID.eq(INVOICE_ITEM_TO_CCID.ID))
                .join(CCID_METADATA).on(CCID_METADATA.ID_CCID_METADATA.eq(INVOICE_ITEM_TO_CCID.ID_CCID_METADATA))
                .join(INVOICE_ITEM).on(INVOICE_ITEM.ID_ITEM.eq(INVOICE_ITEM_TO_CCID.ID_INVOICE_ITEM))
                .join(INVOICE).on(INVOICE.ID_INVOICE.eq(INVOICE_ITEM.ID_INVOICE))
                .leftJoin(IMPORTI_ANTICIPO).on(IMPORTI_ANTICIPO.ID_INVOICE.eq(INVOICE.ID_INVOICE))
                .where(MM_RIPARTIZIONE_CARICO_CCID.ID_RIPARTIZIONE_CARICO.eq(carico.getId()))
                .groupBy(coalesce(IMPORTI_ANTICIPO.NUMERO_FATTURA,INVOICE.INVOICE_CODE),
                        INVOICE.DATE_OF_PERTINENCE,
                        CCID_METADATA.ID_DSR)
                .fetchInto(RipartizioneDTO.class);

        //per ogni totale di valore verifica se nuovo flusso (CCID_METADATA_DETAILs) o vecchio flusso NO CCID_METADATA_DETAILS


        MultiValueMap<String,MmRipartizione648> listaMessaggi = new LinkedMultiValueMap<>();
        for (RipartizioneDTO dto : listaCcid) {

            List<CcidMetadataDetails> allByIdDsr = ccidMetadataDetailsRepo.findAllByIdDsr(dto.getIdDsr());

            if(CollectionUtils.isEmpty(allByIdDsr)){
                //applico il vecchio flusso

                MmRipartizione648 mess= new MmRipartizione648();
                mess.setDateOfPertinence(dto.getDateOfPertinence());

                mess.setNuovoFlusso(false);
                mess.setIdInvoice(dto.getIdInvoice());
                mess.setSociety(DEFAULT_SOCIETY);
                mess.setDataOraCreazione(new Timestamp(new Date().getTime()));
                mess.setPathS3648(buildPathOutput(carico.getIdRipartizioneSiada()+"",DEFAULT_SOCIETY,dto.getIdDsr(),dto.getIdInvoice()+""));
                mess.setIdRipartizioneSiada(carico.getIdRipartizioneSiada().intValue());

                mess.setIdSocietaTutela(societaTutelaRepo.findFirstByCodice(DEFAULT_SOCIETY).getId());
                mess.setIdDsr(dto.getIdDsr());

                ParamDTO paramDTO = parametersQuery(DEFAULT_SOCIETY, dto.getIdDsr());
                mess.setIdDsr(dto.getIdDsr());

                BigDecimal valDem = dto.getValoreTot().multiply(BigDecimal.valueOf(Double.parseDouble(paramDTO.getSplitDem())));
                BigDecimal valDrm = dto.getValoreTot().multiply(BigDecimal.valueOf(Double.parseDouble(paramDTO.getSplitDrm())));

                mess.setValoreAggioDem(valDem.multiply(BigDecimal.valueOf(Double.parseDouble(paramDTO.getAggioDem()))).divide(BigDecimal.valueOf(100)));
                mess.setValoreAggioDrm(valDrm.multiply(BigDecimal.valueOf(Double.parseDouble(paramDTO.getAggioDrm()))).divide(BigDecimal.valueOf(100)));

                mess.setValoreTrattenuta(valDem.subtract(mess.getValoreAggioDem()).multiply(BigDecimal.valueOf(Double.parseDouble(paramDTO.getTrattenutaDem()))).divide(BigDecimal.valueOf(100)));
                mess.setValoreDem(valDem.subtract(mess.getValoreAggioDem()).subtract(mess.getValoreTrattenuta()));
                mess.setValoreDrm(valDrm.subtract(mess.getValoreAggioDrm()));

                mess.setValoreLordo(dto.getValoreTot());
                mess.setValoreNetto(mess.getValoreLordo().subtract(mess.getValoreAggioDem()).subtract(mess.getValoreAggioDrm()).subtract(mess.getValoreTrattenuta()));




                listaMessaggi.add(dto.getIdDsr()+"",mess);
                //query per valori punto dem e drm
//                mess.setValoreDem();
//                mess.setValoreDrm();

            }else{
                //applico il nuovo flusso
                //frazione tra società.
                //calcolo DEM DRM AGGIO LORDO NETTO E ALTRO
        //POPOLO DTO CON OGNI DATO diviso per CCID-FATTURA-SOCIETA'

                BigDecimal totAmoutPerPerc = BigDecimal.ZERO;

                for (CcidMetadataDetails details : allByIdDsr) {
                    totAmoutPerPerc = totAmoutPerPerc.add(details.getClaimValore());
                }


                for (CcidMetadataDetails details : allByIdDsr) {

                    //   x*100/(x+y)
                    //
                    BigDecimal amountToUsePerc = (details.getClaimValore().multiply(BigDecimal.valueOf(100))).divide(totAmoutPerPerc,2);
                    BigDecimal amountToUse = dto.getValoreTot().multiply(amountToUsePerc).divide(BigDecimal.valueOf(100));

                    //magica magica query:
                    //che tira fuori i valori dem drm e aggio

                    MmRipartizione648 mess= new MmRipartizione648();
                    mess.setDateOfPertinence(dto.getDateOfPertinence());
                    mess.setNuovoFlusso(true);
//                mess.setIdDsr(dto.getIdDsr());
                    mess.setIdInvoice(dto.getIdInvoice());
                    mess.setSociety(details.getSocieta());
                    mess.setDataOraCreazione(new Timestamp(new Date().getTime()));
                    mess.setPathS3648(buildPathOutput(carico.getIdRipartizioneSiada()+"",details.getSocieta(),dto.getIdDsr(),dto.getIdInvoice()+""));
                    mess.setIdRipartizioneSiada(carico.getIdRipartizioneSiada().intValue());

                    mess.setIdSocietaTutela(societaTutelaRepo.findFirstByCodice(details.getSocieta()).getId());
                    mess.setIdDsr(dto.getIdDsr());

                    ParamDTO paramDTO = parametersQuery(details.getSocieta(), dto.getIdDsr());

                    BigDecimal valDem = amountToUse.multiply(BigDecimal.valueOf(Double.parseDouble(paramDTO.getSplitDem())));
                    BigDecimal valDrm = amountToUse.multiply(BigDecimal.valueOf(Double.parseDouble(paramDTO.getSplitDrm())));

                    mess.setValoreAggioDem(valDem.multiply(BigDecimal.valueOf(Double.parseDouble(paramDTO.getAggioDem()))).divide(BigDecimal.valueOf(100)));
                    mess.setValoreAggioDrm(valDrm.multiply(BigDecimal.valueOf(Double.parseDouble(paramDTO.getAggioDrm()))).divide(BigDecimal.valueOf(100)));




                    mess.setValoreTrattenuta(valDem.subtract(mess.getValoreAggioDem()).multiply(BigDecimal.valueOf(Double.parseDouble(paramDTO.getTrattenutaDem()))).divide(BigDecimal.valueOf(100)));

                    mess.setValoreDem(valDem.subtract(mess.getValoreAggioDem()).subtract(mess.getValoreTrattenuta()));
                    mess.setValoreDrm(valDrm.subtract(mess.getValoreAggioDrm()));

                    mess.setValoreLordo(amountToUse);
                    mess.setValoreNetto(mess.getValoreLordo().subtract(mess.getValoreAggioDem()).subtract(mess.getValoreAggioDrm()).subtract(mess.getValoreTrattenuta()));

                    listaMessaggi.add(dto.getIdDsr()+"",mess);
                }

            }


        }


        for (String key : listaMessaggi.keySet()) {
            mmRipartizione648Repo.saveAll(listaMessaggi.get(key));
        }






        List<JsonObject> messageBuffer = new LinkedList<JsonObject>();
        
        Gson gson = buildGson();
        
        for (Entry<String, List<MmRipartizione648>> entry : listaMessaggi.entrySet()) {

        	String iddsr = entry.getKey();

        	JsonObject msg = new JsonObject();

			String queue = entry.getValue().size() > 0 && entry.getValue().get(0).isNuovoFlusso() ? newQueueOutputName : oldQueueOutputName;
            buildHeader(msg, queue);
            
        	JsonObject dsrMetadata = dsrMetadata(iddsr);
            JsonObject body = ripartizioneMessageBodyTemplate(iddsr, dsrMetadata);
            msg.add("body", body);

        	// TODO Salvare cose a DB
            //MmRipartizioneCarico carico = new MmRipartizioneCarico();
            //MmRipartizioneCaricoRepo.save(carico);

        	JsonArray carichiRipartizione = new JsonArray();
        	for (MmRipartizione648 dto: entry.getValue()) {
                JsonElement caricoJson = gson.toJsonTree(dto);
//                caricoJson.getAsJsonObject().addProperty("pathOutput", buildPathOutput(dto,carico.getIdRipartizioneSiada().toString()));
                // TODO aggiungere altre cose se necessario
                carichiRipartizione.add(caricoJson);
			}
        	body.add("carichiRipartizione", carichiRipartizione);

        	messageBuffer.add(msg);
		}
//TODO: decommentare
		for (JsonObject outboundMessage : messageBuffer) {
			messagingTemplate.send(queueServiceBusName, MessageBuilder.withPayload(gson.toJson(outboundMessage)).build());
		}

//		throw new RuntimeException("ohla");

    }


	protected Gson buildGson() {
		GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("yyyyMMdd");
        gsonBuilder.registerTypeAdapter(BigDecimal.class, 
        		new TypeAdapter<BigDecimal>() {

					@Override
					public void write(JsonWriter out, BigDecimal value) throws IOException {
					    if(value == null) {
                            out.nullValue();
                            return;
                        }
						BigDecimal scaled = value.setScale(4, RoundingMode.HALF_UP);
						out.value(scaled.toPlainString());
					}
					@Override
					public BigDecimal read(JsonReader in) throws IOException {
						throw new UnsupportedOperationException("Questo gson deve essere utilizzato solo per scrivere!");
					}
        });
		return gsonBuilder.create();
	}

	protected void buildHeader(JsonObject msg, String queue) {
		JsonObject header = new JsonObject();
		header.addProperty("uuid", UUID.randomUUID().toString());
		header.addProperty("timestamp", new Date().getTime());
		header.addProperty("sender", "RipartizioneService");
		header.addProperty("queue", queue);
		msg.add("header", header);
	}


	public Set<String> getAllPeriodi(){
        List<MmRipartizioneCarico> all = mmRipartizioneCaricoRepo.findAll();

        return all.stream()
                .filter(m -> m.getStato().equals(MmRipartizioneCarico.STATO.APPROVATO))
                .map(m -> m.getIdRipartizioneSiada()+"")
                .collect(Collectors.toSet());

    }


    public List<StatisticaRipartizioneDTO> statisticheRipartizione(StatisticaRipartizioneRequestDTO request){

       StringBuffer qry = new StringBuffer();
        qry.append("select\n" +
                "    MM_RIPARTIZIONE_648.ID,\n" +
                "    MM_RIPARTIZIONE_648.ID_RIPARTIZIONE_SIADA,\n" +
                "    SOCIETA_TUTELA.NOMINATIVO,\n" +
                "    MM_RIPARTIZIONE_648.ID_CCID_METADATA as ID_DSR,\n" +
                "    MM_RIPARTIZIONE_648.VALORE_LORDO,\n" +
                "    MM_RIPARTIZIONE_648.VALORE_AGGIO_DEM,\n" +
                "    MM_RIPARTIZIONE_648.VALORE_AGGIO_DRM,\n" +
                "    MM_RIPARTIZIONE_648.VALORE_TRATTENUTA,\n" +
                "    ((MM_RIPARTIZIONE_648.VALORE_LORDO * (RIGHT_SPLIT.DEM)) - (MM_RIPARTIZIONE_648.VALORE_AGGIO_DEM) - (MM_RIPARTIZIONE_648.VALORE_TRATTENUTA)) as \"VALORE_NETTO_DEM\",\n" +
                "    ((MM_RIPARTIZIONE_648.VALORE_LORDO * (RIGHT_SPLIT.DRM)) - (MM_RIPARTIZIONE_648.VALORE_AGGIO_DRM)) as \"VALORE_NETTO_DRM\",\n" +
                "    MM_RIPARTIZIONE_648.VALORE_NETTO,\n" +
                "    MM_RIPARTIZIONE_648.NUMERO_FATTURA,\n" +
                "    ANAG_DSP.NAME as \"DSP_NAME\"\n" +
                "from\n" +
                "    RIGHT_SPLIT,\n" +
                "    MM_RIPARTIZIONE_648\n" +
                "join SOCIETA_TUTELA on\n" +
                "    SOCIETA_TUTELA.ID = MM_RIPARTIZIONE_648.ID_SOCIETA_TUTELA\n" +
                "join CCID_METADATA on\n" +
                "    MM_RIPARTIZIONE_648.ID_CCID_METADATA = CCID_METADATA.ID_DSR\n" +
                "join DSR_METADATA on\n" +
                "    DSR_METADATA.IDDSR = CCID_METADATA.ID_DSR\n" +
                "join ANAG_DSP on\n" +
                "    ANAG_DSP.IDDSP = DSR_METADATA.IDDSP\n" +
                "where\n" +
                "    MM_RIPARTIZIONE_648.ID_RIPARTIZIONE_SIADA = "+ request.getIdRipartizioneSiada() +
                "    and RIGHT_SPLIT.ID = (\n" +
                "    SELECT\n" +
                "        MAX(ID) AS ID\n" +
                "    FROM\n" +
                "        RIGHT_SPLIT, DSR_METADATA dsr_m\n" +
                "    JOIN COMMERCIAL_OFFERS ON\n" +
                "        dsr_m.SERVICE_CODE = COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS\n" +
                "    WHERE\n" +
                "        RIGHT_SPLIT.COUNTRY = dsr_m.COUNTRY\n" +
                "        AND VALID_FROM <\n" +
                "        CASE\n" +
                "            WHEN (PERIOD_TYPE = 'quarter') THEN STR_TO_DATE(CONCAT(1, '-', 1 + (3 * PERIOD - 3), '-', YEAR), '%d-%m-%Y')\n" +
                "            ELSE STR_TO_DATE(CONCAT(1, '-', PERIOD, '-', YEAR), '%d-%m-%Y')\n" +
                "        END\n" +
                "        AND COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE = RIGHT_SPLIT.ID_UTILIZATION_TYPE and dsr_m.IDDSR  = DSR_METADATA.IDDSR )\n" +
                "order By\n" +
                "    CCID_METADATA.ID_DSR asc " );


        if (request.getPaginationActive()) {
            qry.append("LIMIT "+request.getMaxRows() + 1+" OFFSET "+request.getCurrentPage() * request.getMaxRows());
        }
        List<Object[]> resultList = em.createNativeQuery(qry.toString()).getResultList();

        List<StatisticaRipartizioneDTO> result = new ArrayList<>();

        for(Object[] o : resultList){
            StatisticaRipartizioneDTO dto = new StatisticaRipartizioneDTO();
            dto.setId(((BigInteger) o[0]).longValue());
            dto.setIdRipartizioneSiada(((Integer) o[1]).longValue());
            dto.setNominativo((String) o[2]);
            dto.setIdDsr((String) o[3]);
            dto.setValoreLordo((BigDecimal) o[4]);
            dto.setValoreAggioDem((BigDecimal) o[5]);
            dto.setValoreAggioDrm((BigDecimal) o[6]);
            dto.setValoreTrattenuta((BigDecimal) o[7]);
            dto.setValoreNettoDem((BigDecimal) o[8]);
            dto.setValoreNettoDrm((BigDecimal) o[9]);
            dto.setValoreNetto((BigDecimal) o[10]);
            dto.setNumeroFattura((String) o[11]);
            dto.setDspName((String) o[12]);
            result.add(dto);
        }

        return result;


    }

    public S3.Url get648UrlS3(Long idCarico){

        MmRipartizione648 mmRipartizione648 = mmRipartizione648Repo.findById(idCarico).get();


        if(StringUtils.hasLength(mmRipartizione648.getPathS3648())){
            return  new S3.Url(mmRipartizione648.getPathS3648());
        }

        throw new RuntimeException("Path 648 non valorizzato per id:"+idCarico);

    }
    
	protected JsonObject dsrMetadata(String dsr) {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		String url = new StringBuffer(dsrMetadataWsURL).append(dsr).toString();
		URI uri;
		try {
			uri = new URI(url);
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}
		ResponseEntity<String> response = restTemplate.getForEntity(uri, String.class);
		return GsonUtils.fromJson(response.getBody(), JsonObject.class);
	}
	
	protected JsonObject ripartizioneMessageBodyTemplate(String dsr, JsonObject dsrMetadata) {
        JsonObject body = new JsonObject();
        body.addProperty("idDsr", dsr);
        body.addProperty("dsp", GsonUtils.getAsString(dsrMetadata,"dspFtpPath"));
        body.addProperty("dspCode", GsonUtils.getAsString(dsrMetadata,"dspCode"));
        body.addProperty("sliceDspCode", GsonUtils.getAsString(dsrMetadata,"sliceDspCode"));
        body.addProperty("original_price_basis_currency", GsonUtils.getAsString(dsrMetadata,"dsrCurrency"));
        body.addProperty("notEncoded", GsonUtils.getAsString(dsrMetadata,"notEncoded"));
        body.addProperty("country", GsonUtils.getAsString(dsrMetadata,"country"));
        body.addProperty("territory", GsonUtils.getAsString(dsrMetadata,"territory"));
        body.addProperty("service_type", GsonUtils.getAsString(dsrMetadata,"serviceType"));
        body.addProperty("ccidVersion", GsonUtils.getAsString(dsrMetadata,"ccidVersion"));
        body.addProperty("applied_tariff", GsonUtils.getAsString(dsrMetadata,"appliedTariff"));
        body.addProperty("trading_brand", GsonUtils.getAsString(dsrMetadata,"tradingBrand"));
        body.addProperty("encoded", GsonUtils.getAsString(dsrMetadata,"encoded"));
        body.addProperty("periodStartDate", GsonUtils.getAsString(dsrMetadata,"periodStartDate"));
        body.addProperty("periodEndDate", GsonUtils.getAsString(dsrMetadata,"periodEndDate"));
        body.addProperty("encodedProvisional", GsonUtils.getAsString(dsrMetadata,"encodedProvisional"));
        body.addProperty("use_type", GsonUtils.getAsString(dsrMetadata,"useType"));
        body.addProperty("conversion_rate", 1 / GsonUtils.getAsDouble(dsrMetadata,"conversionRate", 1) * 100000);
        body.addProperty("sender", GsonUtils.getAsString(dsrMetadata,"sender"));
        body.addProperty("receiver", GsonUtils.getAsString(dsrMetadata,"receiver"));
        body.addProperty("ccid_id", GsonUtils.getAsString(dsrMetadata,"headerCcidID"));
        body.addProperty("work_code_type", GsonUtils.getAsString(dsrMetadata,"workCodeType"));
        body.addProperty("split_perf", GsonUtils.getAsString(dsrMetadata,"demSplit"));
        body.addProperty("split_mech", GsonUtils.getAsString(dsrMetadata,"drmSplit"));
        body.addProperty("baseRefId", GsonUtils.getAsString(dsrMetadata,"ccidId"));
        body.addProperty("periodType", GsonUtils.getAsString(dsrMetadata,"periodType"));
        body.addProperty("period", GsonUtils.getAsString(dsrMetadata,"period"));
        body.addProperty("idUtilizationType", GsonUtils.getAsString(dsrMetadata,"idUtilizationType"));
        body.addProperty("sliceMonth", GsonUtils.getAsString(dsrMetadata,"sliceMonth"));
        body.addProperty("businessOffer", GsonUtils.getAsString(dsrMetadata,"commercialOffer"));
        body.addProperty("commercialOffer", GsonUtils.getAsString(dsrMetadata,"commercialOffer"));
        body.addProperty("claimPeriodType", GsonUtils.getAsString(dsrMetadata,"claimPeriodType"));
        body.addProperty("claimUtilizationType", GsonUtils.getAsString(dsrMetadata,"claimUtilizationType"));
        body.addProperty("PARAM_APPLIED_TARIFF", GsonUtils.getAsString(dsrMetadata,"APPLIED_TARIFF"));
        body.addProperty("PARAM_AUX", GsonUtils.getAsString(dsrMetadata,"AUX"));
        body.addProperty("year", GsonUtils.getAsString(dsrMetadata,"year"));
        body.addProperty("month", GsonUtils.getAsString(dsrMetadata,"month"));
        body.add("tenant", GsonUtils.getAsJsonObject(dsrMetadata, "tenant"));
        body.add("mandators", GsonUtils.getAsJsonArray(dsrMetadata, "mandators"));
        body.addProperty("decisionTableUrl", GsonUtils.getAsString(dsrMetadata, "dtableS3Url"));
        body.addProperty("priceModel", GsonUtils.getAsString(dsrMetadata, "idUtilizationType"));
        body.addProperty("totalSales", GsonUtils.getAsString(dsrMetadata, "salesLineNum"));
        return body;
	}
	
	protected String buildPathOutput(String idRipartizioneSiada,String societa,String idDsr, String numFattura) {
		StringBuffer path = new StringBuffer(s3BaseOutputPath);
		path.append(idRipartizioneSiada).append("/");
		path.append(societa).append("/");
        path.append(idDsr).append("-").append(numFattura).append(".csv");
		return path.toString();
	}

	public  ParamDTO parametersQuery(String societa,String idDsr){


//
//        Converter<String,Timestamp > converter2 =
//                Converter.of(
//                        String.class,
//                        Timestamp.class,
//                        t -> t == null ? null : Timestamp.valueOf(LocalDateTime.parse(t, DateTimeFormatter.ofPattern("yyyy-MM-dd"))),
//                        u -> u == null ? null : u.toLocalDateTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
//                );
//
//        DataType<Timestamp> TIMESTAMP_TYPE = SQLDataType.VARCHAR.asConvertedDataType(converter2);
//
//
//        SelectConditionStep<Record1<Long>> rightSplit = dsl.select(max(RIGHT_SPLIT.ID)).from(RIGHT_SPLIT).where(
//                RIGHT_SPLIT.COUNTRY.eq(DSR_METADATA.COUNTRY)
//                        .and(RIGHT_SPLIT.VALID_FROM.cast(Timestamp.class).lt(when(DSR_METADATA.PERIOD_TYPE.eq("quarter"),
//                                concat(DSR_METADATA.YEAR.toString(), "-", DSR_METADATA.PERIOD.multiply(3).sub(3).add(1).toString(), "-1").cast(TIMESTAMP_TYPE))
//                                .otherwise(
//                                        concat(DSR_METADATA.YEAR.toString(), "-", DSR_METADATA.PERIOD.multiply(3).sub(3).add(1).toString(), "-1").cast(TIMESTAMP_TYPE))))
//                        .and(COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE.eq(RIGHT_SPLIT.ID_UTILIZATION_TYPE)));
//
//        List<ParamDTO> params = dsl.select(CCID_METADATA.ID_DSR,
//                groupConcat(when(PERF_SETTING.SETTING_KEY.eq("threshold.aggioDRM"), PERF_SETTING.VALUE).otherwise("")).as("AGGIO_DRM"),
//                groupConcat(when(PERF_SETTING.SETTING_KEY.eq("threshold.trattenutaDEM"), PERF_SETTING.VALUE).otherwise("")).as("'TRATTENUTA_DEM'"),
//                groupConcat(when(PERF_SETTING.SETTING_KEY.eq("threshold.aggioDEM"), PERF_SETTING.VALUE).otherwise("")).as("'AGGIO_DEM'")
//        )
//                .from(CCID_METADATA, DSR_METADATA.join(COMMERCIAL_OFFERS).on(DSR_METADATA.SERVICE_CODE.eq(COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS)), RIGHT_SPLIT,PERF_CONFIGURATION)
//                .join(PERF_CONFIGURATION_SETTINGS).on(PERF_CONFIGURATION.CONFIGURATION_ID.eq(PERF_CONFIGURATION_SETTINGS.CONFIGURATION_ID))
//                .join(PERF_SETTING).on(PERF_CONFIGURATION_SETTINGS.SETTINGS_ID.eq(PERF_SETTING.SETTING_ID))
//                .where(PERF_CONFIGURATION.DOMAIN.eq("multimediale.aggi")
//                        .and(CCID_METADATA.ID_DSR.eq(idDsr))
//                        .and(DSR_METADATA.IDDSR.eq(CCID_METADATA.ID_DSR))
//                        .and(PERF_CONFIGURATION.ACTIVE.eq(Byte.valueOf("1")))
//                        .and(PERF_CONFIGURATION.CONF_KEY.eq(societa))
//                        .and(
//                                when(DSR_METADATA.PERIOD_TYPE.eq("quarter"), concat(DSR_METADATA.YEAR.toString(), "-", DSR_METADATA.PERIOD.multiply(3).sub(3).add(1).toString(), "-1").cast(TIMESTAMP_TYPE))
//                                        .otherwise((concat(DSR_METADATA.YEAR.toString(), "-", DSR_METADATA.PERIOD.multiply(3).sub(3).add(1).toString(), "-1").cast(TIMESTAMP_TYPE))).greaterOrEqual(PERF_CONFIGURATION.VALID_FROM))
//                        .and(PERF_CONFIGURATION.VALID_TO.isNull()
//                                .or(
//                                        when(DSR_METADATA.PERIOD_TYPE.eq("quarter"), concat(DSR_METADATA.YEAR.toString(), "-", DSR_METADATA.PERIOD.multiply(3).sub(3).add(1).toString(), "-1").cast(TIMESTAMP_TYPE))
//                                                .otherwise(concat(DSR_METADATA.YEAR.toString(), "-", DSR_METADATA.PERIOD.multiply(3).sub(3).add(1).toString(), "-1").cast(TIMESTAMP_TYPE)).lt(PERF_CONFIGURATION.VALID_TO)
//                                )
//                        )
//                        .and(RIGHT_SPLIT.ID.eq(rightSplit))
//
//                ).groupBy(PERF_CONFIGURATION.CONF_KEY).fetchInto(ParamDTO.class);


        Object[] resultList = (Object[]) em.createNativeQuery("SELECT CCID_METADATA.ID_DSR,\n" +
//                "       PERF_CONFIGURATION.CONF_KEY                                                AS SOCIETA_TUTELA,\n" +
                "       GROUP_CONCAT(IF(s.SETTING_KEY = 'threshold.aggioDRM', s.value, NULL))      AS 'aggioDRM',\n" +
                "       GROUP_CONCAT(IF(s.SETTING_KEY = 'threshold.trattenutaDEM', s.value, NULL)) AS 'trattenutaDEM',\n" +
                "       GROUP_CONCAT(IF(s.SETTING_KEY = 'threshold.aggioDEM', s.value, NULL))      AS 'aggioDem', \n" +
                "       RIGHT_SPLIT.DEM,\n  " +
                "       RIGHT_SPLIT.DRM " +
                "FROM PERF_CONFIGURATION\n" +
                "       JOIN PERF_CONFIGURATION_SETTINGS ps ON PERF_CONFIGURATION.CONFIGURATION_ID = ps.CONFIGURATION_ID\n" +
                "       JOIN PERF_SETTING s ON ps.SETTINGS_ID = s.SETTING_ID,\n" +
                "     CCID_METADATA,\n" +
                "     DSR_METADATA\n" +
                "       JOIN COMMERCIAL_OFFERS on DSR_METADATA.SERVICE_CODE = COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS,\n" +
                "     RIGHT_SPLIT\n" +
                "WHERE PERF_CONFIGURATION.domain = 'multimediale.aggi'\n" +
                "  AND CCID_METADATA.ID_DSR = '" + idDsr + "'\n" +
                "  AND DSR_METADATA.IDDSR = CCID_METADATA.ID_DSR\n" +
                "  AND PERF_CONFIGURATION.active = 1\n" +
                "  AND PERF_CONFIGURATION.CONF_KEY = '" + societa + "'\n" +
                "  AND CASE\n" +
                "        when (PERIOD_TYPE = 'quarter')\n" +
                "          then STR_TO_DATE(concat(1, '-', 1 + (3 * PERIOD - 3), '-', YEAR), '%d-%m-%Y')\n" +
                "        else STR_TO_DATE(concat(1, '-', PERIOD, '-', YEAR), '%d-%m-%Y') end >= PERF_CONFIGURATION.VALID_FROM\n" +
                "  AND (VALID_TO IS NULL OR CASE\n" +
                "                             when (PERIOD_TYPE = 'quarter')\n" +
                "                               then STR_TO_DATE(concat(1, '-', 1 + (3 * PERIOD - 3), '-', YEAR), '%d-%m-%Y')\n" +
                "                             else STR_TO_DATE(concat(1, '-', PERIOD, '-', YEAR), '%d-%m-%Y') end < VALID_TO)\n" +
                "  AND RIGHT_SPLIT.ID =\n" +
                "      (select max(ID) as ID\n" +
                "       from RIGHT_SPLIT\n" +
                "       where (RIGHT_SPLIT.COUNTRY = DSR_METADATA.COUNTRY\n" +
                "         and VALID_FROM < CASE\n" +
                "                            when (PERIOD_TYPE = 'quarter')\n" +
                "                              then STR_TO_DATE(\n" +
                "                                concat(1, '-', 1 + (3 * PERIOD - 3), '-', YEAR),\n" +
                "                                '%d-%m-%Y')\n" +
                "                            else STR_TO_DATE(concat(1, '-', PERIOD, '-', YEAR), '%d-%m-%Y')\n" +
                "           end\n" +
                "         and COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE = RIGHT_SPLIT.ID_UTILIZATION_TYPE))\n" +
                "GROUP BY CONF_KEY;").getSingleResult();
        ParamDTO p = new ParamDTO();
        if(resultList != null){


            p.setIdDsr(resultList[0]+"");
            p.setAggioDem(resultList[3]+"");
            p.setAggioDrm(resultList[1]+"");
            p.setTrattenutaDem(resultList[2]+"");
            p.setSplitDem(resultList[4]+"");
            p.setSplitDrm(resultList[5]+"");
        }


        return p;


    }
}
