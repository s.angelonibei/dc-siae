package com.alkemy.sophia.api.sophiamsinvoice.repository;

import com.alkemy.sophia.api.sophiamsinvoice.entities.MmRipartizioneCaricoCcid;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MmRipartizioneCaricoCcidRepo extends JpaRepository<MmRipartizioneCaricoCcid, Integer> {

    List<MmRipartizioneCaricoCcid> findByRipartizioneCaricoId(Long idCarico);

}
