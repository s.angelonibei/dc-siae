package com.alkemy.sophia.api.sophiamsinvoice.backclaim.rest;


import com.alkemy.sophia.api.sophiamsinvoice.backclaim.dto.DsrMetadataDTO;
import com.alkemy.sophia.api.sophiamsinvoice.backclaim.dto.DsrStepsMonitoringDTO;
import com.alkemy.sophia.api.sophiamsinvoice.backclaim.service.MultimediaService;
import com.alkemy.sophia.api.sophiamsinvoice.backclaim.service.S3Service;
import com.alkemy.sophia.api.sophiamsinvoice.backclaim.utils.BackclaimCcidRecursiveTask;
import com.alkemy.sophia.api.sophiamsinvoice.utils.BackClaimType;
import com.alkemy.sophia.api.sophiamsinvoice.utils.DateUtils;
import com.alkemy.sophia.api.sophiamsinvoice.utils.Env;
import com.alkemy.sophia.api.sophiamsinvoice.utils.NotificationService;
import com.alkemytech.sophia.commons.aws.S3;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.opencsv.CSVWriter;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.apache.commons.csv.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;



@RequestMapping("multimediale")
@RestController
@RequiredArgsConstructor
@CommonsLog
@CrossOrigin
public class MultimedialeController {
    private final S3Service s3Service;
    private final MultimediaService multimediaService;
    private final NotificationService service;
//    private final static String UTF8_TEXT_EVENTSTREAM_VALUE = "text/event-stream;charset=UTF-8";
    private final Env env;

    @Value("${invoice.s3.bucket}")
    private String bucketS3;

    @GetMapping(path = "backclaim/{idDsr}/{tipoBC}")
//    @GetMapping(path = "backclaim/{idDsr}/{tipoBC}", produces = UTF8_TEXT_EVENTSTREAM_VALUE)
    //public SseEmitter startBackclaim(@PathVariable("tipoBC") String tipoBC, @PathVariable("idDsr") final String idDsr) {
    public void startBackclaim(@PathVariable("tipoBC") String tipoBC, @PathVariable("idDsr") final String idDsr) {


        if(tipoBC == null) {
            log.warn("Attenzione BC lanciato senza tipologia uso vecchio BC");
            tipoBC = "A";
        }
        final BackClaimType backClaimType = BackClaimType.valueOfCode(tipoBC);


//        final SseEmitter emitter = new SseEmitter(Long.MAX_VALUE);
//        service.addEmitter(emitter);
//        service.doNotify();
//        emitter.onCompletion(() -> service.removeEmitter(emitter));
//        emitter.onTimeout(() -> service.removeEmitter(emitter));

        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.execute(() -> {
//            SseEmitter.SseEventBuilder event = SseEmitter.event();
            DsrMetadataDTO dsrMetadataDTO = multimediaService.getDsrMetadataAndFtpSourcePath(idDsr);

            String idDsrBackclaim = idDsr;
            DsrMetadataDTO dsrMetadataDTOMT = dsrMetadataDTO;
            try {
                if (multimediaService.updateBackclaimInProgress(idDsr, (short) 1,backClaimType)) {
                    if (dsrMetadataDTO.getBackclaim() > 0) {
                        idDsrBackclaim = String.format("%s%s%s",
                                idDsr,
                                backClaimType.getFileName(),
                                StringUtils.leftPad(dsrMetadataDTO.getBackclaim() + "", 4, "0"));

                        dsrMetadataDTOMT= multimediaService.getDsrMetadataAndFtpSourcePath(idDsrBackclaim);
                    }
                    log.debug("ccidUri: "+ dsrMetadataDTOMT.getS3Path());

                    final AmazonS3URI ccidUri = new AmazonS3URI(dsrMetadataDTOMT.getS3Path());
                    final AmazonS3URI originalSourceUri = new AmazonS3URI(String.format("s3://%s/%s/original-source/%s/%d%s/%s.csv.gz",
                            bucketS3,
                            env.getEnvironment(),
                            dsrMetadataDTOMT.getFtpSourcePath(),
                            dsrMetadataDTOMT.getYear(),
                            (dsrMetadataDTOMT.getPeriodType().equals("month") ?
                                    StringUtils.leftPad(dsrMetadataDTOMT.getPeriod() + "", 2, "0") :
                                    StringUtils.leftPad(3 * dsrMetadataDTOMT.getPeriod() - 2 + "", 2, "0")),
                            idDsrBackclaim
                    ));

                    final List<S3ObjectSummary> ccidSongs = s3Service.listObjects(ccidUri.getBucket(), ccidUri.getKey());
                    ListObjectsRequest req = new ListObjectsRequest().withBucketName(originalSourceUri.getBucket()).withPrefix(originalSourceUri.getKey());
                    final List<S3ObjectSummary> originalSongs = s3Service.listObjects(req);

                    String ccidTempTableName;
                    String originalTempTableName;

//                    if (!(ccidSongs.isEmpty() || originalSongs.isEmpty())) {
                      if ((!(ccidSongs.isEmpty())
                              || !(originalSongs.isEmpty()))) {
                        DsrMetadataDTO dsrMetadataDTO1 = multimediaService.getDsrMetadataAndFtpSourcePath(idDsr);
//                        emitter.send(event.data(dsrMetadataDTO1).name("backclaimInProgress"));
//                        emitter.send("backclaim started at: " + NotificationService.DATE_FORMATTER.format(new Date()));

                        long elapsed = GregorianCalendar.getInstance().getTimeInMillis();
                        ForkJoinPool forkJoinPool = new ForkJoinPool(2);
                        ccidTempTableName = com.alkemy.sophia.api.sophiamsinvoice.utils.StringUtils
                                .getHash(String.format("%s_unidentified_BC%s", idDsr, StringUtils.leftPad(dsrMetadataDTO.getBackclaim() + 1 + "", 4, "0")));
                        ForkJoinTask ccidTask = new BackclaimCcidRecursiveTask(
                                ccidSongs,
                                s3Service,
                                ccidUri,
//                                emitter,
                                idDsrBackclaim,
//                                unidentifiedCount,
                                multimediaService,
                                ccidTempTableName,
                                backClaimType);

//                        emitter.send("csv loading started at " + NotificationService.DATE_FORMATTER.format(new Date()));

                        forkJoinPool.execute(ccidTask);

                        do {
                            log.info("******************************************");
                            log.info(String.format("Main: Parallelism: %d", forkJoinPool.getParallelism()));
                            log.info(String.format("Main: Active Threads: %d", forkJoinPool.getActiveThreadCount()));
                            log.info(String.format("Main: Task Count: %d", forkJoinPool.getQueuedTaskCount()));
                            log.info(String.format("Main: Steal Count: %d", forkJoinPool.getStealCount()));
                            log.info("******************************************");
                            try {
                                TimeUnit.SECONDS.sleep(1);
                            } catch (InterruptedException e) {
                                log.error(e.getMessage(), e);
                            }
                        } while ((!ccidTask.isDone()));
                        //Shut down ForkJoinPool using the shutdown() method.
                        forkJoinPool.shutdown();

                        Set<String> ids = (Set<String>) ccidTask.join();

                        originalTempTableName = com.alkemy.sophia.api.sophiamsinvoice.utils.StringUtils
                                .getHash(String.format("%s_original_BC%s", idDsr, StringUtils.leftPad(dsrMetadataDTO.getBackclaim() + 1 + "", 4, "0")));
                        String finalOriginalTempTableName = originalTempTableName;

                        ExecutorService executorService = Executors.newSingleThreadExecutor();
                        DsrMetadataDTO finalDsrMetadataDTO = dsrMetadataDTO;
//                        final String finalIdDsrMT = idDsrBackclaim;
                        Future future = executorService.submit(() -> {
                            try {
//                                uploadCSV(ids, originalSourceUri, finalOriginalTempTableName, finalDsrMetadataDTO, idDsr);


                                // preparo il file temporaneo con i record escludendo quelli del ccid

                                File outputFile = subCCIDRecord(ids,originalSourceUri,finalOriginalTempTableName,finalDsrMetadataDTO,idDsr,backClaimType);


                                //file di destinazione gz e V
                                String prefix = String.format("%s/original-source/%s/%d%s/%s%s%s",
                                                            env.getEnvironment(),
                                                            finalDsrMetadataDTO.getFtpSourcePath(),
                                                            finalDsrMetadataDTO.getYear(),
                                                            (finalDsrMetadataDTO.getPeriodType().equals("month") ?
                                                                    StringUtils.leftPad(finalDsrMetadataDTO.getPeriod() + "", 2, "0") :
                                                                    StringUtils.leftPad(3 * finalDsrMetadataDTO.getPeriod() - 2 + "", 2, "0")),
                                                            idDsr,
                                                            backClaimType.getFileName(),
                                                            StringUtils.leftPad(finalDsrMetadataDTO.getBackclaim() + 1 + "", 4, "0"));




                                //original V file from s3
                                String originalVFilePath = String.format("%s/original-source/%s/%d%s/%s",
                                        env.getEnvironment(),
                                        finalDsrMetadataDTO.getFtpSourcePath(),
                                        finalDsrMetadataDTO.getYear(),
                                        (finalDsrMetadataDTO.getPeriodType().equals("month") ?
                                                StringUtils.leftPad(finalDsrMetadataDTO.getPeriod() + "", 2, "0") :
                                                StringUtils.leftPad(3 * finalDsrMetadataDTO.getPeriod() - 2 + "", 2, "0")),
                                        idDsr
                                );

                                S3.Url destinationV = new S3.Url(bucketS3,prefix + "_V.csv");
                                S3.Url originalV = new S3.Url(bucketS3,originalVFilePath + "_V.csv");



                                s3Service.upload(bucketS3, prefix + ".csv.gz", outputFile);
                                outputFile.delete();
                                s3Service.copy(originalV, destinationV);

                                if(backClaimType.getCode().equals(BackClaimType.BC_ORIGINAL.getCode())){
                                    //TODO: copy original DSR in BC Original path
                                    String prefixOriginal = String.format("%s/original-source/%s/%d%s/%s%s%s",
                                            env.getEnvironment(),
                                            finalDsrMetadataDTO.getFtpSourcePath(),
                                            finalDsrMetadataDTO.getYear(),
                                            (finalDsrMetadataDTO.getPeriodType().equals("month") ?
                                                    StringUtils.leftPad(finalDsrMetadataDTO.getPeriod() + "", 2, "0") :
                                                    StringUtils.leftPad(3 * finalDsrMetadataDTO.getPeriod() - 2 + "", 2, "0")),
                                            idDsr,
                                            backClaimType.getFileName2(),
                                            StringUtils.leftPad(finalDsrMetadataDTO.getBackclaim() + 1 + "", 4, "0"));

                                    S3.Url destinationGZO = new S3.Url(bucketS3,prefixOriginal + ".csv.gz");
                                    S3.Url originalGZO = new S3.Url(bucketS3,originalVFilePath + ".csv.gz");

                                    S3.Url destinationVO = new S3.Url(bucketS3,prefixOriginal + "_V.csv");
                                    S3.Url originalVO = new S3.Url(bucketS3,originalVFilePath + "_V.csv");

                                    s3Service.copy(originalGZO, destinationGZO);
                                    s3Service.copy(originalVO, destinationVO);

                                }

                            } catch (IOException e) {
                                log.error(e.getMessage(), e);
//                                emitter.completeWithError(e);
                                multimediaService.updateBackclaimInProgress(idDsr, (short) 3,backClaimType);
                            }
                        });

//                        tryFuture(idDsr, emitter, future);

                        log.info("csv loading finished in " + DateUtils.formatMillisToHMS(GregorianCalendar.getInstance().getTimeInMillis() - elapsed));
//                        emitter.send("csv loading finished in " + DateUtils.formatMillisToHMS(GregorianCalendar.getInstance().getTimeInMillis() - elapsed));


                        //Write the number of results generated by each task to the console.
//                String tempUnidentifiedTableName = unidentifiedTask.join();
//                String tempNormaliedTableName = originalTask.join();

//                emitter.send("Uploaded to bucket? " + multimediaService.uploadBackClaimCCID("siae-sophia-datalake", "debug/norm-source/dsp_p=7digital/year_p=2016/month_p=01/dsr_p=7Digital_PDS_20160101_20160331_IT_BC/7Digital_PDS_20160101_20160331_IT_BC.csv", "tmp_7Digital_PDS_20160101_20160331_IT_original", "tmp_7Digital_PDS_20160101_20160331_IT_unidentified", idDsrBackclaim));
                        executorService = Executors.newSingleThreadExecutor();
                        dsrMetadataDTO = multimediaService.getDsrMetadataAndFtpSourcePath(idDsr);
                        String finalIdDsrBackclaim = String.format("%s%s%s",
                                idDsr,
                                backClaimType.getFileName(),
                                StringUtils.leftPad(dsrMetadataDTO.getBackclaim() + "", 4, "0"));
//                        future = executorService.submit(() -> {
//                            try {
//                                isBackclaimFinished(idDsr, emitter, finalIdDsrBackclaim, event);
//                            } catch (IOException e) {
//                                log.error(e.getMessage(), e);
////                                emitter.completeWithError(e);
//                                multimediaService.updateBackclaimInProgress(idDsr, (short) 3,backClaimType);
//                            } catch (InterruptedException e) {
//                                log.error(e.getMessage(), e);
////                                emitter.completeWithError(e);
//                                multimediaService.updateBackclaimInProgress(idDsr, (short) 3,backClaimType);
//                            }
//                        });

//                        tryFuture(idDsr, emitter, future);


                    } else {
                        DsrMetadataDTO dsrMetadataDTO1 = multimediaService.getDsrMetadataAndFtpSourcePath(idDsr);
//                        emitter.send(event.data(dsrMetadataDTO1).name("backclaimInProgress"));
                        multimediaService.updateBackclaimInProgress(idDsr, (short) 3,backClaimType);
//            nonBlockingService.shutdownNow();
                    }
//                    emitter.complete();
                } else {
//                    isBackclaimFinished(idDsr, emitter, idDsrBackclaim, event);
//                    emitter.complete();
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                multimediaService.updateBackclaimInProgress(idDsr, (short) 3,backClaimType);
//                emitter.completeWithError(e);
//                multimediaService.updateBackclaimCounter(idDsr, dsrMetadataDTO.getBackclaim(), -1);

            }
        });
        return;
    }

    private void cleanCcidFile(File ccidFile) throws IOException {
        try (RandomAccessFile f = new RandomAccessFile(ccidFile, "rw")) {
            long length = f.length() - 1;
            byte b;
            do {
                length -= 1;
                f.seek(length);
                b = f.readByte();
            } while (b != 10);
            f.setLength(length + 1);
        }
    }

    private void tryFuture(@PathVariable("idDsr") String idDsr, SseEmitter emitter, Future future) throws InterruptedException, ExecutionException {
        do {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                log.error(e.getMessage(), e);
                emitter.completeWithError(e);
                multimediaService.updateBackclaimInProgress(idDsr, (short) 3);
            }
        } while (future.get() != null);
    }

    private void isBackclaimFinished(@PathVariable("idDsr") String idDsr, SseEmitter emitter, String idDsrBackclaim, SseEmitter.SseEventBuilder event) throws InterruptedException, IOException {
        DsrStepsMonitoringDTO dsrStepsMonitoring = multimediaService.getDsrStepsMonitoring(idDsrBackclaim);
        while (dsrStepsMonitoring.getStatus().equals(0)) {
            TimeUnit.MINUTES.sleep(1);
            emitter.send(event.data(multimediaService.getDsrMetadataAndFtpSourcePath(idDsr)).name("backclaimInProgress"));
            dsrStepsMonitoring = multimediaService.getDsrStepsMonitoring(idDsrBackclaim);
        }
        if (!dsrStepsMonitoring.getStatus().equals(-1)) {
            multimediaService.updateBackclaimInProgress(idDsr, (short) 2);
        } else {
            multimediaService.updateBackclaimInProgress(idDsr, (short) 3);
        }
        emitter.send(event.data(multimediaService.getDsrMetadataAndFtpSourcePath(idDsr)).name("backclaimInProgress"));
    }


    @DeleteMapping(path = "backclaim/{idDsr}")
    public ResponseEntity voidBackclaim(@PathVariable("idDsr") final String idDsr) {
//        final SseEmitter emitter = new SseEmitter();
//        service.addEmitter(emitter);
//        service.doNotify();
//        emitter.onCompletion(() -> service.removeEmitter(emitter));
//        emitter.onTimeout(() -> service.removeEmitter(emitter));
//
//        ExecutorService executor = Executors.newSingleThreadExecutor();
//
//        executor.execute(() -> {
        DsrMetadataDTO dsrMetadataDTO = multimediaService.getDsrMetadataAndFtpSourcePath(idDsr);

        BackClaimType bcType = BackClaimType.valueOfCode(dsrMetadataDTO.getBcType());
        List<String> idDsrBackclaims = new ArrayList<>(2);

        String bc1 = String.format("%s%s%s", idDsr, bcType.getFileName(), StringUtils.leftPad(dsrMetadataDTO.getBackclaim() + "", 4, "0"));
        idDsrBackclaims.add(bc1);

//        try{
        if (multimediaService.checkIsInvoiced(bc1))
            throw new RuntimeException("CCID di backclaim già fatturato");
//        }catch(Exception e){log.error("Errore nella verifica del BC fatturato",e);}


        //in caso di backclaim di tipo C elimino entrambi i file
        if(bcType.equals(BackClaimType.BC_ORIGINAL))
            idDsrBackclaims.add(String.format("%s%s%s", idDsr, bcType.getFileName2(), StringUtils.leftPad(dsrMetadataDTO.getBackclaim() + "", 4, "0")));


        for (String idDsrBackclaim : idDsrBackclaims) {

            DsrMetadataDTO dsrMetadataBackclaimDTO = null;
            try {
                dsrMetadataBackclaimDTO = multimediaService.getDsrMetadataAndFtpSourcePath(idDsrBackclaim);
            } catch(Exception e){}



            AmazonS3URI ccidIdSourceUri = null;

            if (dsrMetadataBackclaimDTO != null && dsrMetadataBackclaimDTO.getS3Path() != null) {
                ccidIdSourceUri = new AmazonS3URI(dsrMetadataBackclaimDTO.getS3Path());
            }
        }

        String s3UriPath = String.format("s3://"+ bucketS3 +"/%s/original-source/%s/%d%s/",
                env.getEnvironment(),
                dsrMetadataDTO.getFtpSourcePath(),
                dsrMetadataDTO.getYear(),
                (dsrMetadataDTO.getPeriodType().equals("month") ?
                        org.apache.commons.lang3.StringUtils.leftPad(dsrMetadataDTO.getPeriod() + "", 2, "0") :
                        org.apache.commons.lang3.StringUtils.leftPad(3 * dsrMetadataDTO.getPeriod() - 2 + "", 2, "0"))
        );

        if (multimediaService.voidBackclaim(idDsrBackclaims, idDsr,s3UriPath)) {
            dsrMetadataDTO = multimediaService.getDsrMetadataAndFtpSourcePath(idDsr);
            return ResponseEntity.status(HttpStatus.OK).body(dsrMetadataDTO);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
//        });
//        executor.shutdown();


    private File subCCIDRecord(Set<String> ids, AmazonS3URI originalSourceUri, String tempTableName, DsrMetadataDTO dsrMetadataDTO, String idDsrOrigin,BackClaimType backClaimType) throws IOException{

        File tempOutputFile = null;
        File tempDataFile = null;
        BigDecimal utilizzazioniBC = BigDecimal.ZERO;
        CSVPrinter pw = null;

        String idDsr = idDsrOrigin;
        if (dsrMetadataDTO.getBackclaim() > 0) {
            idDsr = String.format("%s%s%s", idDsr, backClaimType.getFileName(),StringUtils.leftPad(dsrMetadataDTO.getBackclaim() + "", 4, "0"));
        }

        String idDsrBackclaim = String.format("%s%s%s", idDsrOrigin,backClaimType.getFileName(), StringUtils.leftPad(dsrMetadataDTO.getBackclaim() + 1 +"", 4, "0"));
        try {

            tempDataFile = File.createTempFile(idDsr, ".csv.gz.IN");
            tempDataFile.deleteOnExit();

            tempOutputFile = File.createTempFile(idDsr, ".csv.gz.OUT");
            tempOutputFile.deleteOnExit();


            log.debug("Inizio download da S3: " + originalSourceUri.getBucket() +" "+ originalSourceUri.getKey());
            s3Service.download(originalSourceUri.getBucket(), originalSourceUri.getKey(), tempDataFile);


            log.info("Creazione file di output");
            log.debug("Tempdatafile: " + tempDataFile.getName() + "Path: " + tempDataFile.getAbsolutePath());

            BufferedReader br = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(tempDataFile))));

            CSVParser csvParser = new CSVParser(br, CSVFormat.newFormat(';')
                    .withQuote('"').withQuoteMode(QuoteMode.MINIMAL));


            pw =  new CSVPrinter(new PrintWriter(new GZIPOutputStream(new FileOutputStream(tempOutputFile))),CSVFormat.newFormat(';')
                    .withQuote('"').withQuoteMode(QuoteMode.MINIMAL).withRecordSeparator("\n"));
            log.debug("pw : " +pw);

            for (CSVRecord line : csvParser) {
                log.debug("NEL FOR RIGA 438");
                if (!ids.contains(line.get(2))) {
                    utilizzazioniBC = utilizzazioniBC.add(new BigDecimal(line.get(16)));
                    log.debug("UTILIZZAZIONIBC :"+utilizzazioniBC);
                    pw.printRecord(line);
                }
            }
        } catch (Exception e)   {
            e.printStackTrace();
        } finally {

            pw.flush();
            pw.close();
            tempDataFile.delete();
        }

        BigDecimal valoreTotaleBC = dsrMetadataDTO.getTotalValue()
                .multiply(utilizzazioniBC)
                .divide(dsrMetadataDTO.getSalesLinesNum(),12, RoundingMode.HALF_EVEN);
        log.debug("valoreTotaleBC "+valoreTotaleBC);

        BigDecimal subscriptionBC = BigDecimal.valueOf(dsrMetadataDTO.getSubscriptionsNum())
                .multiply(utilizzazioniBC)
                .divide(dsrMetadataDTO.getSalesLinesNum(),12, RoundingMode.HALF_EVEN);


        log.info(String.format("Stat BC SALES LINES: %f SUBSCRIPTIONS: %f TOTAL VALUE: %f", utilizzazioniBC,subscriptionBC,valoreTotaleBC));

        multimediaService.upsertBcStatistics(idDsrBackclaim,utilizzazioniBC,subscriptionBC,valoreTotaleBC);
        multimediaService.updateBackclaimCounter(idDsrOrigin, dsrMetadataDTO.getBackclaim() + 1, 1);

        return tempOutputFile;

    }

//    private void uploadCSV(Set<String> ids, AmazonS3URI originalSourceUri, String tempTableName, DsrMetadataDTO dsrMetadataDTO, String idDsr) throws IOException {
//        if (originalSourceUri.getKey().contains(".csv.gz")) {
//            ByteArrayOutputStream unidentifiedGzipOutputStream = null;
//            GZIPOutputStream gzip = null;
//            ByteArrayOutputStream vTableOutputStream;
//            try {
//                unidentifiedGzipOutputStream = new ByteArrayOutputStream();
//                gzip = new GZIPOutputStream(unidentifiedGzipOutputStream);
//                vTableOutputStream = new ByteArrayOutputStream();
//
//                CsvWriteOptions unidentifiedWriteOptions =
//                        CsvWriteOptions.builder(gzip)
//                                .separator(';')
//                                .quoteChar(CSVWriter.NO_QUOTE_CHARACTER)
//                                .header(false)
//                                .build();
//
//                CsvWriteOptions vTableWriteOptions =
//                        CsvWriteOptions.builder(vTableOutputStream)
//                                .separator(';')
//                                .quoteChar(CSVWriter.NO_QUOTE_CHARACTER)
//                                .header(false)
//                                .build();
//
//
//
////                BufferedReader br = new BufferedReader(new InputStreamReader(new GZIPInputStream(
////                        s3Service.download(
////                                originalSourceUri.getBucket(),
////                                originalSourceUri.getKey())),
////                        StandardCharsets.UTF_8));
//
//                File temp = File.createTempFile("backclaim", ".tmp"); temp.deleteOnExit();
//                s3Service.download(originalSourceUri.getBucket(), originalSourceUri.getKey(),temp);
//
//                BufferedReader br = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(temp))));
//
//
//
////                File originalSongFile = StreamUtil.stream2file();
//
//                ColumnType[] columnTypes = {
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        FLOAT,
//                        INTEGER,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        INTEGER,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING,
//                        STRING
//                };
//
//                final Table[] finalTable = {null};
//
//                final AtomicInteger count = new AtomicInteger();
//                final AtomicInteger totalCount = new AtomicInteger();
//                try (Stream<String> lFileStream = br.lines().parallel()) {
////                    while (br.ready()) {
//                    final AtomicInteger threshold = new AtomicInteger();
//                    lFileStream.filter(line -> org.apache.commons.lang.StringUtils.isNotEmpty(line) && (org.apache.commons.lang.StringUtils.countMatches(line, String.valueOf(';')) + 1) == columnTypes.length)
//                            .map(line -> {
//                                log.debug((count.get() + 1) + ". " + line);
//                                totalCount.getAndIncrement();
//                                threshold.set(0);
//                                Table table = null;
//                                try {
//                                    table = Table.read().usingOptions(CsvReadOptions.builderFromString(line)
//                                            .tableName(tempTableName)
//                                            .separator(';')
//                                            .quoteChar(CSVWriter.NO_QUOTE_CHARACTER)
//                                            .header(false)
//                                            .sample(false)
//                                            .columnTypes(columnTypes)
//                                            .build());
//                                } catch (IOException e) {
//                                    log.error(e.getMessage(), e);
//                                }
//                                count.getAndIncrement();
//                                return table;
//                            }).filter(table -> !ids.contains(table.column("C2").asStringColumn().get(0)))
//                            .forEachOrdered(table -> {
//                                if (finalTable[0] != null)
//                                    finalTable[0].append(table);
//                                else
//                                    finalTable[0] = table;
//                            });
//
//                    log.info("LE RIGHE CREATE SONO BEN " + finalTable[0].rowCount() + " LE TOTALI " + (finalTable[0].rowCount() + ids.size()) + " LE SCARTATE " + (ids.size()));
//
//                    Table vTable = finalTable[0].
//                            summarize("C15",
//                                    "C30",
//                                    "C16",
//                                    sum)
//                            .apply();
//
//
//
//                    vTable = vTable.select(vTable.numberColumn(0).asDoubleColumn(),
//                            vTable.numberColumn(1).asLongColumn(),
//                            vTable.numberColumn(2).asLongColumn());
//
//                    vTable.row(0).setDouble(0, (dsrMetadataDTO.getTotalValue().divide(BigDecimal.valueOf(dsrMetadataDTO.getSalesLinesNum()), 4, BigDecimal.ROUND_HALF_UP).multiply(BigDecimal.valueOf(vTable.row(0).getLong(2)))).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
//                    vTable.row(0).setLong(1, dsrMetadataDTO.getSubscriptionsNum());
//
//                    finalTable[0].write().usingOptions(unidentifiedWriteOptions);
//                    vTable.write().usingOptions(vTableWriteOptions);
//
//                    String prefix = String.format("%s/original-source/%s/%d%s/%s_BC%s",
//                            env.getEnvironment(),
//                            dsrMetadataDTO.getFtpSourcePath(),
//                            dsrMetadataDTO.getYear(),
//                            (dsrMetadataDTO.getPeriodType().equals("month") ?
//                                    StringUtils.leftPad(dsrMetadataDTO.getPeriod() + "", 2, "0") :
//                                    StringUtils.leftPad(3 * dsrMetadataDTO.getPeriod() - 2 + "", 2, "0")),
//                            idDsr,
//                            StringUtils.leftPad(dsrMetadataDTO.getBackclaim() + 1 + "", 4, "0"));
//                    boolean isUploaded = multimediaService.uploadBackClaimCCID(
//                            "siae-sophia-datalake",
//                            prefix + ".csv.gz",
//                            prefix + "_V.csv",
//                            unidentifiedGzipOutputStream,
//                            vTableOutputStream);
//
//                    log.info("Uploaded to bucket? " + isUploaded);
//                    multimediaService.updateBackclaimCounter(idDsr, dsrMetadataDTO.getBackclaim() + 1, 1);
//                }
//            } finally {
//                if (unidentifiedGzipOutputStream != null) {
//                    unidentifiedGzipOutputStream.flush();
//                    unidentifiedGzipOutputStream.close();
//                }
//                if (gzip != null) {
//                    gzip.flush();
//                    gzip.close();
//                }
//            }
//        }
//    }

}
