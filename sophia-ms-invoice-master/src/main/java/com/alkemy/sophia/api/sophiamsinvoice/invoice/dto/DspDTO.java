package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;

import com.alkemy.sophia.api.sophiamsinvoice.entities.AnagDsp;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Produces("application/json")
@XmlRootElement 
public class DspDTO implements Serializable {

	private static final long serialVersionUID = 1086728157519763677L;

	private String idDsp;
	private String code;
	private String name;
	private String description;
	private String ftpSourcePath;

	public DspDTO() {

	}

	public DspDTO(AnagDsp anagDsp) {
		this.idDsp = anagDsp.getIdDsp();
		this.code = anagDsp.getCode();
		this.name = anagDsp.getName();
		this.description = anagDsp.getDescription();
		this.ftpSourcePath = anagDsp.getFtpSourcePath();
	}
	public String getIdDsp() {
		return idDsp;
	}

	public void setIdDsp(String idDsp) {
		this.idDsp = idDsp;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFtpSourcePath() {
		return ftpSourcePath;
	}

	public void setFtpSourcePath(String ftpSourcePath) {
		this.ftpSourcePath = ftpSourcePath;
	}

}