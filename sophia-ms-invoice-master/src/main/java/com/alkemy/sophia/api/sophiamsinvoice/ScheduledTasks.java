package com.alkemy.sophia.api.sophiamsinvoice;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class ScheduledTasks {

	private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);

	@Autowired
	private EntityManager em;

	@Scheduled(cron = "0 0-59/5 * * * ?")
//	@Scheduled(cron = "0 * * * * ?")
	@Transactional
	public void scheduledUpdateSucceedBlackclaims() {
		try {
			int count = em.createNamedQuery("updateSucceedBlackclaims").executeUpdate();
			logger.info(String.format("Successfully updated %d dsr with success status", count));
		} catch (Exception e) {
			logger.error("Error Trying updating successful backclaims", e);
		}

	}

	@Scheduled(cron = "0 1-59/5 * * * ?")
//	@Scheduled(cron = "0 * * * * ?")
	@Transactional
	public void scheduledUpdateFailedBlackclaims() {
		try {
			int count = em.createNamedQuery("updateFailedBlackclaims").executeUpdate();
			logger.info(String.format("Successfully updated %d dsr with failed status", count));
		} catch (Exception e) {
			logger.error("Error Trying updating failed backclaims", e);
		}

	}
	
	@Scheduled(cron = "0 2-59/5 * * * ?")
//	@Scheduled(cron = "0 * * * * ?")
	@Transactional
	public void scheduledUpdateFinishedBlackclaims() {
		try {
			int count = em.createNamedQuery("updateFinishedBlackclaims").executeUpdate();
			logger.info(String.format("Successfully updated %d dsr with finished status", count));
		} catch (Exception e) {
			logger.error("Error Trying updating finished backclaims", e);
		}

	}	

}