package com.alkemy.sophia.api.sophiamsinvoice.invoice.rest;


import com.alkemy.sophia.api.sophiamsinvoice.invoice.dto.ReclamoDTO;
import com.alkemy.sophia.api.sophiamsinvoice.invoice.dto.SearchImportiAnticipo;
import com.alkemy.sophia.api.sophiamsinvoice.invoice.dto.UsaAnticipoDTO;
import com.alkemy.sophia.api.sophiamsinvoice.invoice.service.ReclamoService;
import com.alkemytech.sophia.common.s3.S3Service;
//import com.alkemytech.sophia.commons.aws.S3;
//import com.alkemytech.sophia.commons.io.CompressionAwareFileOutputStream;
//import com.alkemytech.sophia.commons.util.IOUtils;
import com.alkemytech.sophia.commons.util.IOUtils;
import com.amazonaws.services.s3.AmazonS3URI;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import java.io.*;
import java.util.Map;


@RequestMapping("reclamo")
@RestController
@RequiredArgsConstructor
@CommonsLog
public class ReclamoRestService {

    private final ReclamoService reclamoService;

    private final EntityManager entityManager;
    private final Gson gson;





    @PostMapping(path = "addReclamo", produces = "application/json")
    @Transactional
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public ResponseEntity addReclamo(@RequestParam(value = "file", required = true) MultipartFile file,
                                     @RequestParam(value = "reclamo", required = true) String reclamo,
                                     @RequestHeader Map<String, String> headers
    ) throws Exception {

        ReclamoDTO dataDTO=gson.fromJson(reclamo,ReclamoDTO.class);
        String user = headers.get("user");
        if (user == null) {
            String errore = "utente non riconosciuto";
            user="testLocale";
//            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(errore);
        }
     try {
            dataDTO.setUtente(user);
            dataDTO.setFile(file.getInputStream());
            reclamoService.addReclamo(dataDTO);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            log.error("inserimento in errore", e);
            throw e;
        }
    }

    @GetMapping(path = "getReclami", produces = "application/json")
    public ResponseEntity get(@RequestHeader Map<String, String> headers) {

        try{
            return ResponseEntity.ok(reclamoService.getListInvoiceReclamo());
        } catch (Exception e) {
          log.error("getReclami error: ", e);
          return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }


    @GetMapping("downloadinput")
    public ResponseEntity downloadinput(@RequestParam("fileUrl") String fileUrl)throws Exception{

        if (null == fileUrl) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.NOT_FOUND).build();
        }
        InputStream is = reclamoService.downloadInput(fileUrl);

        byte[] bytes = IOUtils.toByteArray(is);
        return ResponseEntity.status(HttpStatus.OK).body(bytes);
    }




}
