package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;


import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;

@Data
@RequiredArgsConstructor
public class RipartizioneDTO {
    private BigDecimal valoreTot;
    private Long idInvoice;
    private String idDsr;
    private Timestamp dateOfPertinence;

}