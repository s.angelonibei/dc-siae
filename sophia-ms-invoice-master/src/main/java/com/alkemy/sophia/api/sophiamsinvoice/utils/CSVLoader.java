package com.alkemy.sophia.api.sophiamsinvoice.utils;

import lombok.extern.apachecommons.CommonsLog;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.jooq.impl.DefaultDataType;
import org.jooq.impl.SQLDataType;

import java.io.FileNotFoundException;
import java.util.*;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;

@CommonsLog
public class CSVLoader {
//	private static final String TABLE_REGEX = "\\$\\{table\\}";
//	private static final String KEYS_REGEX = "\\$\\{keys\\}";
//	private static final String VALUES_REGEX = "\\$\\{values\\}";

    private DSLContext dsl;
    private char separator;

    public CSVLoader(DSLContext dsl) {
        this.dsl = dsl;
        //Set default separator
        this.separator = ';';
    }

    public CSVLoader(DSLContext dsl, char separator) {
        this.dsl = dsl;
        this.separator = separator;
    }

//    public synchronized Collection<Field<?>> createTable(Table table, String tableName) {
//        this.dsl.transaction(configuration -> {
//            Boolean tableExists = DSL.using(configuration).selectOne()
//                    .from(table("information_schema.tables"))
//                    .where(field("table_name").eq(tableName))
//                    .fetchOneInto(Boolean.class);
//
//            if (tableExists == null || !tableExists) {
//                log.info(table + "doesn't exists");
//                Row firstRow = table.row(0);
//
//                if (null == firstRow) {
//                    throw new FileNotFoundException(
//                            "No columns defined in given CSV file." +
//                                    "Please check the CSV file format.");
//                }
//
//                CreateTableAsStep<Record> recordCreateTableAsStep = DSL.using(configuration).createTableIfNotExists(tableName);
//                CreateTableColumnStep createTableColumnStep = null;
//                for (int i = 0; i < firstRow.columnNames().size(); i++) {
//                    if (firstRow.getObject(i) == null || firstRow.getObject(i).getClass().equals(String.class)) {
//                        createTableColumnStep =
//                                recordCreateTableAsStep
//                                        .column(firstRow.columnNames().get(i),
//                                                SQLDataType.CLOB(Integer.MAX_VALUE));
//                    } else {
//                        createTableColumnStep =
//                                recordCreateTableAsStep
//                                        .column(firstRow.columnNames().get(i),
//                                                DefaultDataType.getDataType(DSL.using(configuration).dialect(),
//                                                        firstRow.getObject(i).getClass()));
//                    }
//                }
//                createTableColumnStep.execute();
//                for (int i = 0; i < firstRow.columnNames().size(); i++) {
//                    try {
//                        DSL.using(configuration)
//                                .createIndex("IDX_" + tableName + "_" + firstRow.columnNames().get(i))
//                                .on(table(tableName), field(firstRow.columnNames().get(i)))
//                                .execute();
//                    } catch (Exception e) {
////                        log.error(e.getMessage(), e);
//                    }
//                }
//            }
//        });
//        return Arrays.asList(table(tableName).fields());
//    }

//    public void loadCSV(String tableName, Table table) {
//        this.dsl.transaction(configuration -> {
//            long elapsed = GregorianCalendar.getInstance().getTimeInMillis();
//            log.info("Loading csv in " + tableName);
//            List<InsertValuesStepN<Record>> insertSetSteps = new ArrayList<>();
//            for (int i = 0; i < table.rowCount(); i++) {
//                InsertSetStep<Record> insertSetStep =
//                        DSL.using(configuration).insertInto(table(tableName));
//                List<Object> objects = new ArrayList<>();
//                for (int j = 0; j < table.row(i).columnCount(); j++) {
////                    if(table.row(i) != null) {
//                        objects.add(table.row(i).getObject(j));
////                    }
//                }
//                InsertValuesStepN insertValuesStepN = insertSetStep.values(objects);
//                insertSetSteps.add(insertValuesStepN);
//                if (insertSetSteps.size() == 1000) {
//                    DSL.using(configuration).batch(insertSetSteps).execute();
//                    insertSetSteps = new ArrayList<>();
//                }
//            }
//            DSL.using(configuration).batch(insertSetSteps).execute();
//
//            log.info("Loading csv in " + tableName + " finished in " + DateUtils.formatMillisToHMS(GregorianCalendar.getInstance().getTimeInMillis() - elapsed));
//        });
//    }

    public int deleteTemporaryTables(String... tables) {
        int count = 0;
        for (String table : tables) {
            count += this.dsl.dropTableIfExists(table(table)).execute();
        }
        return count;
    }
}