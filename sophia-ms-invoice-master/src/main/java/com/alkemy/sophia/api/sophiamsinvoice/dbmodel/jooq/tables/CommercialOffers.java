/*
 * This file is generated by jOOQ.
 */
package com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.tables;


import com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.DefaultSchema;
import com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.Indexes;
import com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.Keys;
import com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.tables.records.CommercialOffersRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.9"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class CommercialOffers extends TableImpl<CommercialOffersRecord> {

    private static final long serialVersionUID = 2146109718;

    /**
     * The reference instance of <code>COMMERCIAL_OFFERS</code>
     */
    public static final CommercialOffers COMMERCIAL_OFFERS = new CommercialOffers();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<CommercialOffersRecord> getRecordType() {
        return CommercialOffersRecord.class;
    }

    /**
     * The column <code>COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS</code>.
     */
    public final TableField<CommercialOffersRecord, Integer> ID_COMMERCIAL_OFFERS = createField("ID_COMMERCIAL_OFFERS", org.jooq.impl.SQLDataType.INTEGER.nullable(false).identity(true), this, "");

    /**
     * The column <code>COMMERCIAL_OFFERS.IDDSP</code>.
     */
    public final TableField<CommercialOffersRecord, String> IDDSP = createField("IDDSP", org.jooq.impl.SQLDataType.VARCHAR(100).nullable(false).defaultValue(org.jooq.impl.DSL.inline("*", org.jooq.impl.SQLDataType.VARCHAR)), this, "");

    /**
     * The column <code>COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE</code>.
     */
    public final TableField<CommercialOffersRecord, String> ID_UTIILIZATION_TYPE = createField("ID_UTIILIZATION_TYPE", org.jooq.impl.SQLDataType.VARCHAR(20).nullable(false).defaultValue(org.jooq.impl.DSL.inline("*", org.jooq.impl.SQLDataType.VARCHAR)), this, "");

    /**
     * The column <code>COMMERCIAL_OFFERS.OFFERING</code>.
     */
    public final TableField<CommercialOffersRecord, String> OFFERING = createField("OFFERING", org.jooq.impl.SQLDataType.VARCHAR(255).nullable(false).defaultValue(org.jooq.impl.DSL.inline("*", org.jooq.impl.SQLDataType.VARCHAR)), this, "");

    /**
     * The column <code>COMMERCIAL_OFFERS.CCID_SERVICE_TYPE_CODE</code>.
     */
    public final TableField<CommercialOffersRecord, String> CCID_SERVICE_TYPE_CODE = createField("CCID_SERVICE_TYPE_CODE", org.jooq.impl.SQLDataType.VARCHAR(10).nullable(false).defaultValue(org.jooq.impl.DSL.inline("PDS", org.jooq.impl.SQLDataType.VARCHAR)), this, "");

    /**
     * The column <code>COMMERCIAL_OFFERS.CCID_USE_TYPE_CODE</code>.
     */
    public final TableField<CommercialOffersRecord, String> CCID_USE_TYPE_CODE = createField("CCID_USE_TYPE_CODE", org.jooq.impl.SQLDataType.VARCHAR(10).nullable(false).defaultValue(org.jooq.impl.DSL.inline("ST", org.jooq.impl.SQLDataType.VARCHAR)), this, "");

    /**
     * The column <code>COMMERCIAL_OFFERS.CCID_APPLIED_TARIFF_CODE</code>.
     */
    public final TableField<CommercialOffersRecord, String> CCID_APPLIED_TARIFF_CODE = createField("CCID_APPLIED_TARIFF_CODE", org.jooq.impl.SQLDataType.VARCHAR(10).nullable(false).defaultValue(org.jooq.impl.DSL.inline("ITSP", org.jooq.impl.SQLDataType.VARCHAR)), this, "");

    /**
     * The column <code>COMMERCIAL_OFFERS.TRADING_BRAND</code>.
     */
    public final TableField<CommercialOffersRecord, String> TRADING_BRAND = createField("TRADING_BRAND", org.jooq.impl.SQLDataType.VARCHAR(25).nullable(false).defaultValue(org.jooq.impl.DSL.inline("*", org.jooq.impl.SQLDataType.VARCHAR)), this, "");

    /**
     * Create a <code>COMMERCIAL_OFFERS</code> table reference
     */
    public CommercialOffers() {
        this(DSL.name("COMMERCIAL_OFFERS"), null);
    }

    /**
     * Create an aliased <code>COMMERCIAL_OFFERS</code> table reference
     */
    public CommercialOffers(String alias) {
        this(DSL.name(alias), COMMERCIAL_OFFERS);
    }

    /**
     * Create an aliased <code>COMMERCIAL_OFFERS</code> table reference
     */
    public CommercialOffers(Name alias) {
        this(alias, COMMERCIAL_OFFERS);
    }

    private CommercialOffers(Name alias, Table<CommercialOffersRecord> aliased) {
        this(alias, aliased, null);
    }

    private CommercialOffers(Name alias, Table<CommercialOffersRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> CommercialOffers(Table<O> child, ForeignKey<O, CommercialOffersRecord> key) {
        super(child, key, COMMERCIAL_OFFERS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return DefaultSchema.DEFAULT_SCHEMA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.COMMERCIAL_OFFERS_FK_CO_APPLIED_TARIFF_IDX, Indexes.COMMERCIAL_OFFERS_FK_CO_SERVICE_TYPE_IDX, Indexes.COMMERCIAL_OFFERS_FK_CO_USE_TYPE_IDX, Indexes.COMMERCIAL_OFFERS_IDX_UQ_CONFIG, Indexes.COMMERCIAL_OFFERS_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<CommercialOffersRecord, Integer> getIdentity() {
        return Keys.IDENTITY_COMMERCIAL_OFFERS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<CommercialOffersRecord> getPrimaryKey() {
        return Keys.KEY_COMMERCIAL_OFFERS_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<CommercialOffersRecord>> getKeys() {
        return Arrays.<UniqueKey<CommercialOffersRecord>>asList(Keys.KEY_COMMERCIAL_OFFERS_PRIMARY, Keys.KEY_COMMERCIAL_OFFERS_IDX_UQ_CONFIG);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CommercialOffers as(String alias) {
        return new CommercialOffers(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CommercialOffers as(Name alias) {
        return new CommercialOffers(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public CommercialOffers rename(String name) {
        return new CommercialOffers(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public CommercialOffers rename(Name name) {
        return new CommercialOffers(name, null);
    }
}
