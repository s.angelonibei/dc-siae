package com.alkemy.sophia.api.sophiamsinvoice.utils;

public enum BackClaimType {

	//BC originale
	//in caso di bc original bisogna prevedere 2 infissi per questo i due fileName
	BC_ORIGINAL("C","_BC_C2_","_BC_C1_"),
	//BC SOlO NON IDENTIFICATO (OLD)
	BC_INC_UNI("A","_BC_A_",null),
	//BC ANCHE CLAIM A 0
	BC_INC_ALL("B","_BC_B_",null);

	private String code;
	private String fileName;
	private String fileName2;

	BackClaimType(String code, String fileName, String fileName2){
		this.code = code;
		this.fileName = fileName;
		this.fileName2 = fileName2;
	}

	public String getCode() {
		return code;
	}
	public String getFileName() { return fileName; }
	public String getFileName2() { return fileName2; }

	@Override
	public String toString(){
		return code;
	}
	
	public static BackClaimType valueOfCode(String code ){
		for(BackClaimType v : values() ){
			if( v.code.equals( code )){
				return v;
			}
		}
		throw new IllegalArgumentException( "No enum const " + BackClaimType.class + "@description." + code );
	}
	
}
