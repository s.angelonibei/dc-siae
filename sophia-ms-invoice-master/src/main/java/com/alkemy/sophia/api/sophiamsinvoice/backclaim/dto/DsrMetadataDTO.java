package com.alkemy.sophia.api.sophiamsinvoice.backclaim.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class DsrMetadataDTO {
    private String iddsr;
    private String iddsp;
    private String periodType;
    private Integer period;
    private Integer year;
    private String country;
    private BigDecimal salesLinesNum;
    private BigDecimal totalValue;
    private Long subscriptionsNum;
    private Integer backclaim;
    private String ftpSourcePath;
    private Short idBackclaimInProgress;
    private String s3Path;
    private String bcType;
}