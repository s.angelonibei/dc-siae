package com.alkemy.sophia.api.sophiamsinvoice.entities;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity(name="DsrMetadata")
@Table(name="DSR_METADATA")
@Data
@EqualsAndHashCode
@NamedNativeQueries({
    @NamedNativeQuery(
            name    = "updateSucceedBlackclaims",
            query   ="update  DSR_METADATA a " +
                    "  join   ( " +
                    "    select concat(IDDSR, '_BC_', IF(BC_TYPE = 'C', 'C2', BC_TYPE), '_', LPAD(BACKCLAIM, 4, '0')) as iddsr_bc,  " +
                    "    IDDSR " +
                    "    from DSR_METADATA " +
                    "    where BACKCLAIM > 0  " +
                    "    and ID_BACKCLAIM_IN_PROGRESS = 1 " +
                    ") bc " +
                    "    on a.IDDSR = bc.IDDSR " +
                    "    join DSR_STEPS_MONITORING " +
                    " on bc.iddsr_bc = DSR_STEPS_MONITORING.IDDSR " +
                    " set ID_BACKCLAIM_IN_PROGRESS = 2 " +
                    " where DSR_STEPS_MONITORING.CLAIM_STATUS='OK'; "
//            query   = "UPDATE DSR_METADATA dsr  " +
//                    "       JOIN (SELECT SUBSTRING(dsm1.IDDSR, 1, LENGTH(dsm1.IDDSR) - 11) as IDDSR1, SUBSTRING(dsm1.IDDSR, 1, LENGTH(dsm1.IDDSR) - 10) as IDDSR2, dsm1.CLAIM_STATUS as CLAIM_STATUS " +
//                    "             FROM   DSR_STEPS_MONITORING dsm1  " +
//                    "                    LEFT JOIN DSR_STEPS_MONITORING dsm2  " +
//                    "                           ON SUBSTRING(dsm1.IDDSR, 1, LENGTH(dsm1.IDDSR) - 4) = SUBSTRING(dsm2.IDDSR, 1,LENGTH(dsm2.IDDSR) - 4)  " +
//                    "							   AND CAST(SUBSTRING(dsm1.IDDSR, LENGTH(dsm1.IDDSR) - 3, LENGTH(dsm1.IDDSR)) AS SIGNED ) < CAST(SUBSTRING(dsm2.IDDSR, LENGTH(dsm2.IDDSR) - 3, LENGTH(dsm2.IDDSR)) AS SIGNED ) " +
//                    "             WHERE  dsm1.IDDSR LIKE CONCAT('%', '!_BC', '%') escape '!' " +
//                    "                    AND dsm2.IDDSR IS NULL " +
//                    "                    ) v1 " +
//                    "         ON (dsr.IDDSR = v1.IDDSR1 or dsr.IDDSR = v1.IDDSR2)  " +
//                    "SET    dsr.ID_BACKCLAIM_IN_PROGRESS = '2'  " +
//                    "WHERE  dsr.ID_BACKCLAIM_IN_PROGRESS = '1' AND v1.CLAIM_STATUS='OK'"
    ),
    @NamedNativeQuery(
    		name    = "updateFailedBlackclaims",
            query   ="update  DSR_METADATA a " +
                    "  join   ( " +
                    "    select concat(IDDSR, '_BC_', IF(BC_TYPE = 'C', 'C2', BC_TYPE), '_', LPAD(BACKCLAIM, 4, '0')) as iddsr_bc,  " +
                    "    IDDSR " +
                    "    from DSR_METADATA " +
                    "    where BACKCLAIM > 0  " +
                    "    and ID_BACKCLAIM_IN_PROGRESS = 1 " +
                    ") bc " +
                    "    on a.IDDSR = bc.IDDSR " +
                    "    join DSR_STEPS_MONITORING " +
                    " on bc.iddsr_bc = DSR_STEPS_MONITORING.IDDSR " +
                    " set ID_BACKCLAIM_IN_PROGRESS = 3 " +
                    " where (DSR_STEPS_MONITORING.CLAIM_STATUS='KO' " +
					" OR DSR_STEPS_MONITORING.IDENTIFY_STATUS='KO' " +
					" OR DSR_STEPS_MONITORING.CLEAN_STATUS='KO' " +
					" OR DSR_STEPS_MONITORING.EXTRACT_STATUS='KO' " +
					" ) ; "
//    		query   = "UPDATE DSR_METADATA dsr  " +
//                    "       JOIN (SELECT SUBSTRING(dsm1.IDDSR, 1, LENGTH(dsm1.IDDSR) - 11)  as IDDSR1, SUBSTRING(dsm1.IDDSR, 1, LENGTH(dsm1.IDDSR) - 10) as IDDSR2, IF(FIELD('KO', dsm1.EXTRACT_STATUS, dsm1.CLEAN_STATUS , dsm1.PRICE_STATUS , dsm1.HYPERCUBE_STATUS, dsm1.IDENTIFY_STATUS, dsm1.UNILOAD_STATUS, dsm1.CLAIM_STATUS)=0,'OK','KO') AS PROCESS_STATUS " +
//                    "             FROM   DSR_STEPS_MONITORING dsm1  " +
//                    "                    LEFT JOIN DSR_STEPS_MONITORING dsm2  " +
//                    "                           ON SUBSTRING(dsm1.IDDSR, 1, LENGTH(dsm1.IDDSR) - 4) = SUBSTRING(dsm2.IDDSR, 1,LENGTH(dsm2.IDDSR) - 4)  " +
//                    "                              AND CAST(SUBSTRING(dsm1.IDDSR, LENGTH(dsm1.IDDSR) - 3, LENGTH(dsm1.IDDSR)) AS SIGNED ) < CAST(SUBSTRING(dsm2.IDDSR, LENGTH(dsm2.IDDSR) - 3, LENGTH(dsm2.IDDSR)) AS SIGNED )  " +
//                    "             WHERE  dsm1.IDDSR LIKE CONCAT('%', '!_BC', '%') escape '!' " +
//                    "                    AND dsm2.IDDSR IS NULL " +
//                    "                    ) v1 " +
//					"         ON (dsr.IDDSR = v1.IDDSR1 or dsr.IDDSR = v1.IDDSR2)  " +
//                    "SET    dsr.ID_BACKCLAIM_IN_PROGRESS = '3'  " +
//                    "WHERE  dsr.ID_BACKCLAIM_IN_PROGRESS = '1' AND v1.PROCESS_STATUS='KO'"
    ),
    @NamedNativeQuery(
    		name    = "updateFinishedBlackclaims",
            query   ="update  DSR_METADATA a " +
                    "  join   ( " +
                    "    select concat(IDDSR, '_BC_', IF(BC_TYPE = 'C', 'C2', BC_TYPE), '_', LPAD(BACKCLAIM, 4, '0')) as iddsr_bc,  " +
                    "    IDDSR " +
                    "    from DSR_METADATA " +
                    "    where BACKCLAIM > 0  " +
                    "    and ID_BACKCLAIM_IN_PROGRESS = 3 " +
                    ") bc " +
                    "    on a.IDDSR = bc.IDDSR " +
                    "    join DSR_STEPS_MONITORING " +
                    " on bc.iddsr_bc = DSR_STEPS_MONITORING.IDDSR " +
                    " set ID_BACKCLAIM_IN_PROGRESS = 2 " +
                    " where DSR_STEPS_MONITORING.CLAIM_STATUS='OK'; "
//			query = "UPDATE  " +
//			        "DSR_METADATA dsr   " +
//			        "       JOIN (SELECT SUBSTRING(dsm1.IDDSR, 1, LENGTH(dsm1.IDDSR) - 11)  as IDDSR1, SUBSTRING(dsm1.IDDSR, 1, LENGTH(dsm1.IDDSR) - 10) as IDDSR2, IF(FIELD('KO', dsm1.EXTRACT_STATUS, dsm1.CLEAN_STATUS , dsm1.PRICE_STATUS , dsm1.HYPERCUBE_STATUS, dsm1.IDENTIFY_STATUS, dsm1.UNILOAD_STATUS, dsm1.CLAIM_STATUS)=0,'OK','KO') AS PROCESS_STATUS  " +
//			        "             FROM   DSR_STEPS_MONITORING dsm1   " +
//			        "                    LEFT JOIN DSR_STEPS_MONITORING dsm2   " +
//			        "                           ON SUBSTRING(dsm1.IDDSR, 1, LENGTH(dsm1.IDDSR) - 4) = SUBSTRING(dsm2.IDDSR, 1,LENGTH(dsm2.IDDSR) - 4)   " +
//			        "                              AND CAST(SUBSTRING(dsm1.IDDSR, LENGTH(dsm1.IDDSR) - 3, LENGTH(dsm1.IDDSR)) AS SIGNED ) < CAST(SUBSTRING(dsm2.IDDSR, LENGTH(dsm2.IDDSR) - 3, LENGTH(dsm2.IDDSR)) AS SIGNED )   " +
//			        "             WHERE  dsm1.IDDSR LIKE CONCAT('%', '!_BC', '%') escape '!'  " +
//			        "                    AND dsm2.IDDSR IS NULL  " +
//			        "                    ) v1  " +
//					"         ON (dsr.IDDSR = v1.IDDSR1 or dsr.IDDSR = v1.IDDSR2)  " +
//			        "SET    dsr.ID_BACKCLAIM_IN_PROGRESS = '2'   " +
//			        "WHERE  dsr.ID_BACKCLAIM_IN_PROGRESS = '3' AND v1.PROCESS_STATUS='OK'"
    )
})
public class DsrMetadata {

	@Id
	@Column(name="IDDSR", nullable=false)
	private String idDsr;

	@Column(name="IDDSP", nullable=false)
	private String idDsp;

	@Column(name="SALES_LINES_NUM", nullable=true)
	private Integer salesLinesNum;
	
	@Column(name="TOTAL_VALUE", nullable=true)
	private BigDecimal totalValue;

	@Column(name="SUBSCRIPTIONS_NUM", nullable=true)
	private Integer subscriptionsNum;
	
	@Column(name="CREATION_TIMESTAMP", nullable=false)
	private String creationTimestamp;

	@Column(name="SERVICE_CODE", nullable=true)
	private Integer serviceCode;

	@Column(name="PERIOD_TYPE", nullable=false)
	private String periodType;

	@Column(name="PERIOD", nullable=true)
	private Integer period;

	@Column(name="YEAR", nullable=true)
	private Integer year;

	@Column(name="COUNTRY", nullable=false)
	private String country;

	@Column(name="CURRENCY", nullable=false)
	private String currency;

}

