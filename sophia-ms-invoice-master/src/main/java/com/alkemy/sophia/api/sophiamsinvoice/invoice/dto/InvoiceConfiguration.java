package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@ConfigurationProperties(prefix = "invoice-config")
@Component
public class InvoiceConfiguration {

	
	private String direzioneSede;
	private String canale;
	private String cod;
	private Map<String,String> tipoDoc;
	private String ufficio;
	private Map<String,String> invoiceStatus;
	private String esbPath;
	
	public String getDirezioneSede() {
		return direzioneSede;
	}
	public void setDirezioneSede(String direzioneSede) {
		this.direzioneSede = direzioneSede;
	}
	public String getCanale() {
		return canale;
	}
	public void setCanale(String canale) {
		this.canale = canale;
	}
	public String getCod() {
		return cod;
	}
	public void setCod(String cod) {
		this.cod = cod;
	}
	
	public Map<String, String> getTipoDoc() {
		return tipoDoc;
	}
	public void setTipoDoc(Map<String, String> tipoDoc) {
		this.tipoDoc = tipoDoc;
	}
	public String getUfficio() {
		return ufficio;
	}
	public void setUfficio(String ufficio) {
		this.ufficio = ufficio;
	}
	public Map<String, String> getInvoiceStatus() {
		return invoiceStatus;
	}
	public void setInvoiceStatus(Map<String, String> invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}
	public String getEsbPath() {
		return esbPath;
	}
	public void setEsbPath(String esbPath) {
		this.esbPath = esbPath;
	}
	
}
