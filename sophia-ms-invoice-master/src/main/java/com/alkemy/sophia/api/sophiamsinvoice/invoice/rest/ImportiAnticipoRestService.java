package com.alkemy.sophia.api.sophiamsinvoice.invoice.rest;


import com.alkemy.sophia.api.sophiamsinvoice.invoice.dto.*;
import com.alkemy.sophia.api.sophiamsinvoice.entities.*;
import com.alkemy.sophia.api.sophiamsinvoice.invoice.service.ImportiAnticipoService;
import com.alkemy.sophia.api.sophiamsinvoice.utils.InvoiceStatus;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


@RequestMapping("importiAnticipi")
@RestController
@RequiredArgsConstructor
@CommonsLog
public class ImportiAnticipoRestService {

    private final ImportiAnticipoService importiAnticipoService;

    private final EntityManager entityManager;
    private final Gson gson;

    @PostMapping(path = "getAdvancePayment", produces = "application/json")
    public ResponseEntity get(@RequestBody SearchImportiAnticipo dataDTO, @RequestHeader Map<String, String> headers) {


        try {
            return ResponseEntity.ok(importiAnticipoService.getFilteranticipi(dataDTO));
        } catch (Exception e) {
            log.error("search anticipo error: ", e);
        }

        return ResponseEntity.status(500).build();
    }


    @DeleteMapping(path = "deleteAdvancePayment", produces = "application/json")
    public ResponseEntity deleteAnticipo(@RequestBody ImportiAnticipoDTO dataDTO) throws RuntimeException {

//	    try {
        importiAnticipoService.removeAnticipo(dataDTO);
        return ResponseEntity.ok().build();
//        }catch(Exception e){
//	    	if(StringUtils.hasLength(e.getMessage())){
//
//				return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
//			}
//	     return ResponseEntity.status(HttpStatus.CONFLICT).build();
//	    }

    }

//	public void removeInvoice(Integer idInvoice, EntityManager entityManager) {
//		try {
//			Invoice invoice = (Invoice) entityManager
//					.createQuery("Select x from Invoice x where x.idInvoice = :idInvoice")
//					.setParameter("idInvoice", idInvoice).getSingleResult();
//
//			for (InvoiceItem invoiceItem : invoice.getInvoiceItems()) {
//
//				List<InvoiceItemToCcid> invoiceItemToCcids = entityManager
//						.createQuery("Select x from InvoiceItemToCcid x where x.invoiceItem.idItem = :invoiceItem")
//						.setParameter("invoiceItem", invoiceItem.getIdItem()).getResultList();
//
//				List<CCIDMetadata> metadataList = (List<CCIDMetadata>) entityManager
//						.createQuery("select x from CCIDMetadata x where x.invoiceItem = :invoiceItemId")
//						.setParameter("invoiceItemId", invoiceItem.getIdItem()).getResultList();
//				for (CCIDMetadata metadata : metadataList) {
//					metadata.setInvoiceStatus("DA_FATTURARE");
//					metadata.setInvoiceItem(null);
//					BigDecimal tmpValoreResiduo = new BigDecimal(0);
//					for (InvoiceItemToCcid invoiceItemToCcid : invoiceItemToCcids) {
//						if (invoiceItemToCcid.getCcidMedata().equals(metadata.getIdCCIDMetadata())) {
//							tmpValoreResiduo.add(invoiceItemToCcid.getValore());
//						}
//
//					}
//					// reimposta il valore_residuo con la somma di sestesso + quota Fattura
//					// Si somma quota fattura con valore residuo perchè potrebbe essere stato
//					// stornato anche un anticipo
//
//					metadata.setValoreResiduo(tmpValoreResiduo);
//					// reimposta il valore quota_fattura a 0 dato che si sta eliminando la fattura
//					metadata.setQuotaFattura(new BigDecimal("0"));
//
//				}
//				entityManager.createQuery("delete from InvoiceItemToCcid x where x.invoiceItem.idItem = :invoiceItem")
//						.setParameter("invoiceItem", invoiceItem.getIdItem()).executeUpdate();
//				entityManager.remove(invoiceItem);
//			}
//
//			entityManager.createQuery("delete from InvoiceLog x where x.idInvoice = :idInvoice")
//					.setParameter("idInvoice", idInvoice).executeUpdate();
//			entityManager.remove(invoice);
//
//		} catch (Exception e) {
//
////			if (entityManager.getTransaction().isActive()) {
////				entityManager.getTransaction().rollback();
////			}
//			log.error("delete", e);
//			throw (e);
//		}


    @PostMapping(path = "updateAdvancePayment", produces = "application/json")
    @Transactional
    public ResponseEntity update(@RequestBody ImportiAnticipoDTO dataDTO) {

        String PATTERN_DATE_FORMAT = "dd-MM-yyyy";

        SimpleDateFormat dateFormat = new SimpleDateFormat(PATTERN_DATE_FORMAT);

        Query query = entityManager
                .createQuery("SELECT x FROM ImportiAnticipo x where x.idImportoAnticipo = :idImportoAnticipo")
                .setParameter("idImportoAnticipo", dataDTO.getIdImportoAnticipo());
        List<ImportiAnticipo> listImportiAnticipo = query.getResultList();
        ImportiAnticipo importoanticipo = listImportiAnticipo.get(0);

        if (importoanticipo != null) {

            Date periodoPertinenzaFine = importoanticipo.getPeriodoPertinenzaFine();

            String newDate = "01" + "-" + dataDTO.getPeriodTo().split("-")[0] + "-"
                    + dataDTO.getPeriodTo().split("-")[1];
            try {

                Date newPeriodoPertinenzaFine = dateFormat.parse(newDate);

                if (newPeriodoPertinenzaFine.after(periodoPertinenzaFine)
                        || newPeriodoPertinenzaFine.equals(periodoPertinenzaFine)) {
                    // aggiorno il valore
                    importoanticipo.setPeriodoPertinenzaFine(newPeriodoPertinenzaFine);
                    // apre la transazione
//					entityManager.getTransaction().begin();
                    // aggiorna l'entity con la nuova data pertinenza fine
                    entityManager.merge(importoanticipo);
                    // salva la modifica nel db
//					entityManager.getTransaction().commit();

                    return ResponseEntity.ok().build();
                } else {
                    // errore.
                    return ResponseEntity.status(HttpStatus.CONFLICT).body(
                            "{\"message\":\"La data pertinenza fine deve essere superiore a quella corrente.\"}");
                }

            } catch (ParseException e) {
//
//				if (entityManager.getTransaction().isActive()) {
//					entityManager.getTransaction().rollback();
//				}
                e.printStackTrace();
            }

        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();

    }


    @PostMapping(path = "addAdvancePayment", produces = "application/json")
    @Transactional
    public ResponseEntity add(@RequestBody ImportiAnticipoDTO dataDTO) {


        List<ImportiAnticipo> importiAnticipi = entityManager
                .createQuery("Select x from ImportiAnticipo x where x.numeroFattura=:numeroFattura")
                .setParameter("numeroFattura", dataDTO.getInvoiceNumber()).getResultList();

        if (importiAnticipi != null && importiAnticipi.size() > 0) {
            return ResponseEntity.status(406).build();
        }

        ImportiAnticipo entity = new ImportiAnticipo();
//		entity.setIdDsp(dataDTO.getIdDSP());
        entity.setImportoOriginale(dataDTO.getOriginalAmount());
        entity.setImportoUtilizzabile(dataDTO.getAmountUsed());
        entity.setNumeroFattura(dataDTO.getInvoiceNumber());

        String[] arrPeriodFrom = dataDTO.getPeriodFrom().split("-");
        GregorianCalendar gcalendarFrom = new GregorianCalendar(Integer.parseInt(arrPeriodFrom[1]),
                Integer.parseInt(arrPeriodFrom[0]) - 1, 1, 0, 0, 0);
        Date dateFrom = gcalendarFrom.getTime();
        entity.setPeriodoPertinenzaInizio(dateFrom);

        String[] arrPeriodTo = dataDTO.getPeriodTo().split("-");
        GregorianCalendar gcalendarTo = new GregorianCalendar(Integer.parseInt(arrPeriodTo[1]),
                Integer.parseInt(arrPeriodTo[0]) - 1, 1, 0, 0, 0);
        Date dateTo = gcalendarTo.getTime();
        entity.setPeriodoPertinenzaFine(dateTo);

        if (dateFrom.after(dateTo)) {
            return ResponseEntity.status(409).build();
        }
//		entityManager.getTransaction().begin();
        InvoiceDTO invoiceDTO = new InvoiceDTO();
        AnagClientDTO anagClientDTO = new AnagClientDTO();
        DspDTO dspDTO = new DspDTO();
        anagClientDTO.setIdAnagClient(dataDTO.getIdAnagClient());
        List<InvoiceItemDto> invoiceItems = new ArrayList<>();

        InvoiceItemDto invoiceItem = new InvoiceItemDto();
        invoiceItem.setCurrency("EUR");
        // invoiceItem.setIdInvoice(idInvoice);
        invoiceItem.setInvoicePosition(1);
        invoiceItem.setTotalValue(new BigDecimal(0));
        invoiceItem.setTotalValueOrig(dataDTO.getOriginalAmount());
        invoiceItems.add(invoiceItem);
//		dspDTO.setIdDsp(dataDTO.getIdDSP());
//		anagClientDTO.setIdDsp(dataDTO.getIdDSP());
        invoiceDTO.setClientData(anagClientDTO);
        invoiceDTO.setCreationDate(new Date());
        invoiceDTO.setDsp(dspDTO);
//		invoiceDTO.setIdDsp(dataDTO.getIdDSP());
        invoiceDTO.setInvoiceCode(dataDTO.getInvoiceNumber());
        invoiceDTO.setInvoiceItems(invoiceItems);
        invoiceDTO.setLastUpdateDate(new Date());
        invoiceDTO.setStatus("FATTURATO");
        invoiceDTO.setTotal(dataDTO.getOriginalAmount());
        InvoiceDTO item = newInvoice(invoiceDTO, entityManager);
        if (item == null) {
//			if (entityManager.getTransaction().isActive()) {
//				entityManager.getTransaction().rollback();
//			}
            return ResponseEntity.status(500).build();
        } else {
            entity.setIdInvoice(invoiceDTO.getIdInvoice());
        }
        entityManager.persist(entity);
//		entityManager.getTransaction().commit();
        return ResponseEntity.ok(dataDTO);
    }

    @PostMapping(path = "addInvoiceAccantonamento", produces = "application/json")
    @Transactional
    public ResponseEntity addInvoiceAccantonamento(@RequestBody ImportiAnticipoDTO dataDTO, @RequestHeader Map<String, String> headers) {


        String user = headers.get("user");
        log.info("headers prima del controllo:" + headers.toString());
        if (user == null) {
            //	user = "development";
            String errore = "utente non riconosciuto";
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(errore);
        }

        List<ImportiAnticipo> importiAnticipos = entityManager
                .createQuery("Select x from ImportiAnticipo x where x.numeroFattura=:numeroFattura")
                .setParameter("numeroFattura", dataDTO.getInvoiceNumber()).getResultList();


        if (importiAnticipos == null || importiAnticipos.size() == 0) {
            return ResponseEntity.status(406).build();
        }

        ImportiAnticipo importiAnticipo = new ImportiAnticipo();
        importiAnticipo = importiAnticipos.get(0);

        BigDecimal importoRealeUtilizzabile = importiAnticipo.getImportoUtilizzabile().add(importiAnticipo.getImportoAccantonatoUtilizzabile() == null ? new BigDecimal(0) : importiAnticipo.getImportoAccantonatoUtilizzabile());
        BigDecimal importoAccantonatoRealeUtilizzato = importiAnticipo.getImportoAccantonato() == null ? new BigDecimal(0) : importiAnticipo.getImportoAccantonato().subtract(
                importiAnticipo.getImportoAccantonatoUtilizzabile() == null ? dataDTO.getImportoAccantonato() : importiAnticipo.getImportoAccantonatoUtilizzabile());

        //BigDecimal importoAccantonabile  = importiAnticipo.getImportoUtilizzabile().subtract(importoAccantonatoRealeUtilizzato);




        BigDecimal importoUtilizzabile = importiAnticipo.getImportoUtilizzabile().add(importiAnticipo.getImportoAccantonato() == null ? new BigDecimal(0) : importiAnticipo.getImportoAccantonato()).subtract(dataDTO.getImportoAccantonato());

        if (importoRealeUtilizzabile.compareTo(new BigDecimal(0)) < 0) {

            ErrorResponse errorResponse = new ErrorResponse();
            errorResponse.setErrorMessage("Importo residuo della fattura minore di zero");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(gson.toJson(errorResponse));

        }


        if (dataDTO.getImportoAccantonato() == null || dataDTO.getImportoAccantonato().compareTo(new BigDecimal(0)) <= 0) {
            if (importoAccantonatoRealeUtilizzato.compareTo(new BigDecimal(0)) > 0) {
                ErrorResponse errorResponse = new ErrorResponse();
                errorResponse.setErrorMessage("Importo accantonato della fattura deve essere maggiore di zero");
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(gson.toJson(errorResponse));
            } else {
                dataDTO.setPercentuale(new BigDecimal(0));
                importoAccantonatoRealeUtilizzato = new BigDecimal(0);
                dataDTO.setImportoAccantonato(new BigDecimal(0));
            }
        }


        if (importoUtilizzabile.compareTo(new BigDecimal(0)) < 0) {
            ErrorResponse errorResponse = new ErrorResponse();
            errorResponse.setErrorMessage("importo residuo utilizzabile (accantonato utilizzabile + residuo fattura) non può essere minore dell'importo accantonato");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(gson.toJson(errorResponse));

        }

        if (dataDTO.getImportoAccantonato().subtract(importoAccantonatoRealeUtilizzato).compareTo(new BigDecimal(0)) < 0) {
            ErrorResponse errorResponse = new ErrorResponse();
            errorResponse.setErrorMessage("importo accantonamento minore del valore utilizzato");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(gson.toJson(errorResponse));

        }

        BigDecimal percentuale = dataDTO.getPercentuale();
        BigDecimal importoAccantonato = dataDTO.getImportoAccantonato();



        BigDecimal importoUtilizzatoReclami = importiAnticipo.getImportoUtilizzatoReclami()!= null ? importiAnticipo.getImportoUtilizzatoReclami() : new BigDecimal(0);

        BigDecimal importoAccantonatoUtilizzabileReclami = importoAccantonato.multiply(percentuale).divide(new BigDecimal(100)).subtract(importoUtilizzatoReclami);
        if(importoAccantonatoUtilizzabileReclami.compareTo(new BigDecimal(0))<0) {
            ErrorResponse errorResponse = new ErrorResponse();
            errorResponse.setErrorMessage("importo accantonamento minore del valore utilizzato");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(gson.toJson(errorResponse));

        }

        log.info("importoUtilizzatoReclami:" + importoUtilizzatoReclami);
        log.info("percentuale:" + percentuale);
        log.info("importoAccantonato:" + importoAccantonato);

		/*fine controlli*/

        importiAnticipo.setImportoUtilizzabile(importoUtilizzabile);
        importiAnticipo.setImportoAccantonato(dataDTO.getImportoAccantonato());
        importiAnticipo.setImportoAccantonatoUtilizzabile(dataDTO.getImportoAccantonato().subtract(importoAccantonatoRealeUtilizzato));
        importiAnticipo.setPercentualeAccantonamento(dataDTO.getPercentuale());
        importiAnticipo.setNote(dataDTO.getNote());

        List<Invoice> invoices = entityManager
                .createQuery("Select x from Invoice x where x.idInvoice=:numeroFattura")
                .setParameter("numeroFattura", importiAnticipo.getIdInvoice()).getResultList();


        if (invoices == null || invoices.size() == 0) {
            return ResponseEntity.status(406).build();
        }

        Invoice invoice = invoices.get(0);

        List<InvoiceAccantonamento> invoiceAccantonamenti = entityManager
                .createQuery("Select x from InvoiceAccantonamento x where x.invoice.idInvoice=:numeroFattura and x.dataFineValidita is null")
                .setParameter("numeroFattura", importiAnticipo.getIdInvoice()).getResultList();

        if (invoiceAccantonamenti != null && invoiceAccantonamenti.size() > 0) {

            InvoiceAccantonamento invoiceAccantonamentoOld = invoiceAccantonamenti.stream()
                    .filter(invoiceAccantonamento1 -> invoiceAccantonamento1.getDataFineValidita() == null)
                    .collect(Collectors.toList()).get(0);
            invoiceAccantonamentoOld.setDataFineValidita(new Date());
            entityManager.persist(invoiceAccantonamentoOld);
        }

        InvoiceAccantonamento invoiceAccantonamento = new InvoiceAccantonamento();
        invoiceAccantonamento.setDataInserimento(new Date());

        invoiceAccantonamento.setImportoAccantonato(dataDTO.getImportoAccantonato());
        invoiceAccantonamento.setImportoAccantonatoUtilizzabile(dataDTO.getImportoAccantonato().subtract(importoAccantonatoRealeUtilizzato));
        invoiceAccantonamento.setNote(dataDTO.getNote());
        invoiceAccantonamento.setPercentuale(dataDTO.getPercentuale());
        invoiceAccantonamento.setInvoice(invoice);
        invoiceAccantonamento.setUtente(user);
        entityManager.persist(invoiceAccantonamento);

        entityManager.persist(importiAnticipo);

        dataDTO.setImportoAccantonatoUtilizzabile(invoiceAccantonamento.getImportoAccantonatoUtilizzabile());
        dataDTO.setAmountUsed(importoUtilizzabile);

        return ResponseEntity.ok(dataDTO);
    }


    @PostMapping(path = "useAdvancePayment", produces = "application/json")
    @Transactional
    public ResponseEntity useAdvancePayment(@RequestBody UsaAnticipoDTO dataDTO) {
        List<ImportiAnticipo> resultQuery = null;
        List<CCIDMetadata> resultQueryCcidMetadata = new ArrayList<>();
        List<Predicate> predicates = new ArrayList<Predicate>();

        SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");
        // anticipo da utilizzare per i ccid selezionati
        ImportiAnticipoDTO anticipo = dataDTO.getIdAdvancePayment();
        // lista dei ccid per i quali si vuole utilizzare un anticipo
        List<CcidInfoDTO> ccidinfoList = dataDTO.getCcidList();
        // lista degli anticipi associati a uno o piu ccid
        List<InvoiceItemToCcid> invoiceItemToCcids = new ArrayList<InvoiceItemToCcid>();
        BigDecimal newUsedVAlue = null;
        BigDecimal ccidResidualValue = null;
        Map<BigInteger, BigDecimal> listValoreFatturabile = new HashMap<BigInteger, BigDecimal>();

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ImportiAnticipo> createQuery = criteriaBuilder.createQuery(ImportiAnticipo.class);
        Root<ImportiAnticipo> importiAnticipo = createQuery.from(ImportiAnticipo.class);

        if (dataDTO.getIdAdvancePayment().getIdImportoAnticipo() != null) {
            javax.persistence.criteria.Path<String> idImportoAnticipo = importiAnticipo.get("idImportoAnticipo");
            Predicate predicateDsp = criteriaBuilder.equal(idImportoAnticipo,
                    dataDTO.getIdAdvancePayment().getIdImportoAnticipo());
            predicates.add(predicateDsp);
        }

        createQuery.where(predicates.toArray(new Predicate[0]));

        createQuery.select(importiAnticipo);
        TypedQuery<ImportiAnticipo> q = entityManager.createQuery(createQuery);
        resultQuery = q.getResultList();
        if (resultQuery.size() == 1) {
            BigDecimal tmpAnticipo = resultQuery.get(0).getImportoUtilizzabile();
            if (tmpAnticipo.compareTo(new BigDecimal(0)) == 0) {
                return ResponseEntity.status(422).build();
            }
        }

        Invoice invoice = (Invoice) entityManager.createQuery("Select x FROM Invoice x where x.idInvoice=:idInvoice")
                .setParameter("idInvoice", resultQuery.get(0).getIdInvoice()).getSingleResult();
        // CCIDMetadata ccidMedata = (CCIDMetadata) entityManager.createQuery("Select x
        // FROM CCIDMetadata x where
        // x.idCCIDMetadata=:idCCIDMetadata").setParameter("idCCIDMetadata",
        // resultQuery.get(0).getIdInvoice()).getSingleResult();

        for (CcidInfoDTO ccidInfoDTO : ccidinfoList) {

            // valori necessari all'aggiornameto dell'anticipo
            BigDecimal ccidvalue = ccidInfoDTO.getValoreResiduo();
            BigDecimal usedvalue = anticipo.getAmountUsed();
            BigDecimal quotaAnticio = null;

            // il valore disponibile nell'anticipo è maggiroe o uguale del valore del ccid
            if (usedvalue.compareTo(ccidvalue) >= 0) {
                newUsedVAlue = usedvalue.subtract(ccidvalue);
                quotaAnticio = usedvalue.subtract(newUsedVAlue);
                ccidResidualValue = new BigDecimal(0);

                // il valore disponibile nell'anticipo è inferiore al valore del ccid
            } else if (usedvalue.compareTo(ccidvalue) < 0 && usedvalue.compareTo(new BigDecimal(0)) > 0) {
                ccidResidualValue = ccidvalue.subtract(usedvalue);
                quotaAnticio = usedvalue;
                newUsedVAlue = new BigDecimal(0);

            } else {
                break;
            }

            // aggiorna il DTO anticipo
            anticipo.setAmountUsed(newUsedVAlue);
            // crea il DTO che rappresenta la relazione tra un ccid e un anticipo

            // crea il DTO che rappresenta la relazione tra un ccid e un anticipo
            InvoiceItemToCcid invoiceItemToCcid = new InvoiceItemToCcid();


            CCIDMetadata ccidMedata = (CCIDMetadata) entityManager
                    .createQuery("Select x FROM CCIDMetadata x where x.idCCIDMetadata=:idCCIDMetadata")
                    .setParameter("idCCIDMetadata", ccidInfoDTO.getIdCCIDmetadata()).getSingleResult();
            invoiceItemToCcid.setCcidMedata(ccidMedata);
            for (InvoiceItem item : invoice.getInvoiceItems()) {
                invoiceItemToCcid.setInvoiceItem(item);
            }
            invoiceItemToCcid.setValore(quotaAnticio);
            invoiceItemToCcids.add(invoiceItemToCcid);

            listValoreFatturabile.put(new BigInteger(String.valueOf(ccidInfoDTO.getIdCCIDmetadata())),
                    ccidResidualValue);

        }
        for (Map.Entry<BigInteger, BigDecimal> map : listValoreFatturabile.entrySet()) {
//			CriteriaBuilder criteriaBuilderCCidMetadata = entityManager.getCriteriaBuilder();
//			CriteriaQuery<CCIDMetadata> createQueryCCIDMetadata = criteriaBuilder.createQuery(CCIDMetadata.class);
//			Root<CCIDMetadata> ccidMetadata = createQuery.from(CCIDMetadata.class);
//
//			javax.persistence.criteria.Path<Long> idCcidMetadata = ccidMetadata.get("idCCIDMetadata");
//
//			Predicate predicateCcidMetadata = criteriaBuilderCCidMetadata.equal(idCcidMetadata, map.getKey().longValue());
//
//			createQueryCCIDMetadata.where(predicateCcidMetadata);
//			createQueryCCIDMetadata.select(ccidMetadata);
//			TypedQuery<CCIDMetadata> ccidQuery = entityManager.createQuery(createQueryCCIDMetadata);
//			List<CCIDMetadata> listCcidMetadata = ccidQuery.getResultList();

            CCIDMetadata ccidMedata1 = (CCIDMetadata) entityManager
                    .createQuery("Select x FROM CCIDMetadata x where x.idCCIDMetadata=:idCCIDMetadata")
                    .setParameter("idCCIDMetadata", new Long(map.getKey().longValue())).getSingleResult();


            CCIDMetadata tmpCcidmetadata = ccidMedata1;
            tmpCcidmetadata.setValoreResiduo(map.getValue());
            if (map.getValue().compareTo(new BigDecimal(0)) == 0) {
                tmpCcidmetadata.setInvoiceStatus(InvoiceStatus.FATTURATO.toString());
            }
            resultQueryCcidMetadata.add(tmpCcidmetadata);
        }
        // List<CCIDMetadata> entity = entityManager.createQuery("SELECT a FROM
        // CCIDMetadata a WHERE a.idCCIDMetadata = :ID").setParameter("ID",
        // 30).getResultList();

        try {
            // inizio della transazione
//			entityManager.getTransaction().begin();

            // cicla tutte le associazioni tra anticipi e ccid per persisterli nel db
            for (InvoiceItemToCcid dto : invoiceItemToCcids) {
                entityManager.persist(dto);
            }

            // aggiorna l'entity relativa agli anticipi
            ImportiAnticipo entityIA = (ImportiAnticipo) entityManager
                    .createQuery("SELECT x FROM ImportiAnticipo x where x.idImportoAnticipo = :idImportoAnticipo")
                    .setParameter("idImportoAnticipo", dataDTO.getIdAdvancePayment().getIdImportoAnticipo())
                    .getSingleResult();

            entityIA.setImportoOriginale(anticipo.getOriginalAmount());
            entityIA.setImportoUtilizzabile(anticipo.getAmountUsed());

            entityManager.merge(entityIA);

            for (CCIDMetadata ccidMetadata : resultQueryCcidMetadata) {
                entityManager.merge(ccidMetadata);
            }

//			entityManager.getTransaction().commit();

        } catch (Exception e) {
//
//			if (entityManager.getTransaction().isActive()) {
//				entityManager.getTransaction().rollback();
//			}
            e.printStackTrace();
        }

        return ResponseEntity.ok().build();
    }


    @PostMapping(path = "useReclamoPayment", produces = "application/json")
    @Transactional
    public ResponseEntity useReclamoPayment(@RequestBody UsaAnticipoDTO dataDTO) throws Exception {
        try {
            importiAnticipoService.useReclamoPayment(dataDTO);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            log.error("error nel consumo del reclamo: ", e);
            throw e;
        }
    }


    @Transactional
    public InvoiceDTO newInvoice(InvoiceDTO dto, EntityManager entityManager) {

        List<Object[]> result;
        List<InvoiceCCIDDTO> dtoList = new ArrayList<>();
        try {
            AnagClient anagClient = (AnagClient) entityManager
                    .createQuery("SELECT x FROM AnagClient x WHERE x.id =:id").setParameter("id", dto.getClientData().getIdAnagClient())
                    .getSingleResult();
            Invoice invoice = new Invoice();
            Date date = new Date();
            invoice.setCreationDate(date);
            invoice.setAnagClient(anagClient);
            invoice.setDateOfPertinence(new Date());
            invoice.setLastUpdateDate(date);
//			invoice.setIdDsp(dto.getIdDsp());
            invoice.setTotal(dto.getTotal());
            invoice.setStatus("FATTURATO");
            invoice.setInvoiceType("ANTICIPO");
            int position = 1;

            for (InvoiceItemDto itemDao : dto.getInvoiceItems()) {
                InvoiceItem itemEntity = new InvoiceItem();
                InvoiceItemReason description = (InvoiceItemReason) entityManager
                        .createQuery("SELECT x FROM InvoiceItemReason x WHERE x.idItemInvoiceReason =:id")
                        .setParameter("id", 5).getSingleResult();
                itemEntity.setDescription(description);

                itemEntity.setNote(itemDao.getNote());
                itemEntity.setInvoicePosition(position);

                itemEntity.setTotal(itemDao.getTotalValue());
                itemEntity.setTotalOrigCurrency(itemDao.getTotalValueOrig());
                itemEntity.setOrigCurrency(itemDao.getCurrency());
                itemEntity.setInvoice(invoice);
                itemEntity.setDescription(description);
                invoice.getInvoiceItems().add(itemEntity);
                entityManager.persist(invoice);
                entityManager.persist(itemEntity);
                entityManager.flush();

                if (itemDao.getCcidList() != null && itemDao.getCcidList().size() > 0) {
                    List<CCIDMetadata> ccids = entityManager
                            .createQuery("SELECT x FROM CCIDMetadata x WHERE x.idCcid IN :idCcids")
                            .setParameter("idCcids", itemDao.getCcidList()).getResultList();
                    for (CCIDMetadata ccidMedata : ccids) {
                        InvoiceItemToCcid invoiceItemToCcid = new InvoiceItemToCcid();
                        invoiceItemToCcid.setCcidMedata(ccidMedata);
                        invoiceItemToCcid.setInvoiceItem(itemEntity);
                        invoiceItemToCcid.setValore(ccidMedata.getValoreResiduo());
                        entityManager.persist(invoiceItemToCcid);
                        ccidMedata.setQuotaFattura(new BigDecimal("0"));
                        ccidMedata.setValoreResiduo(new BigDecimal(0));
                        entityManager.merge(ccidMedata);
                        entityManager.flush();
                    }
                }
                position++;
            }
            dto.setIdInvoice(invoice.getIdInvoice());

        } catch (Exception e) {
            e.printStackTrace();
            log.error("create invoice", e);
            return null;
        }

        return dto;
    }


    @PostMapping(path = "getLinkedCcid", produces = "application/json")
    public ResponseEntity getLinkedCcid(@RequestBody ImportiAnticipoDTO dto) {
        log.info("getLinkedCcid: " + dto.toString());
        List<DettailImportiAnticipo> resultlist = new ArrayList<>();

        try {


            ImportiAnticipo importiAnticipo = (ImportiAnticipo) entityManager
                    .createQuery("SELECT x FROM ImportiAnticipo x where x.idImportoAnticipo = :idImportoAnticipo")
                    .setParameter("idImportoAnticipo", dto.getIdImportoAnticipo()).getSingleResult();
            Invoice invoice = (Invoice) entityManager
                    .createQuery("Select x from Invoice x where x.idInvoice = :idInvoice")
                    .setParameter("idInvoice", importiAnticipo.getIdInvoice()).getSingleResult();

            for (InvoiceItem invoiceItem : invoice.getInvoiceItems()) {
                for (InvoiceItemToCcid invoiceItemToCcid : invoiceItem.getInvoiceItemToCcid()) {

                    DettailImportiAnticipo dettailImportiAnticipo = new DettailImportiAnticipo(
                            importiAnticipo.getIdImportoAnticipo(), importiAnticipo.getPeriodoPertinenzaInizio(),
                            importiAnticipo.getPeriodoPertinenzaFine(), importiAnticipo.getNumeroFattura(),
                            importiAnticipo.getImportoOriginale(), importiAnticipo.getImportoUtilizzabile(),
//						importiAnticipo.getIdDsp(),
                            invoiceItemToCcid.getValore(),
                            invoiceItemToCcid.getCcidMedata().getIdCcid(), invoiceItemToCcid.getCcidMedata().getIdDsr(),
                            invoiceItemToCcid.getCcidMedata().getValoreFatturabile(),
                            invoiceItemToCcid.getCcidMedata().getTotalValue());
                    resultlist.add(dettailImportiAnticipo);

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return ResponseEntity.ok(resultlist);
    }

    @PostMapping(path = "riproporziona", produces = "application/json")
    public ResponseEntity riproporziona(@RequestBody UsaAnticipoDTO dataDTO) throws Exception {
        try {
            importiAnticipoService.riproporziona(dataDTO);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            log.error("error riproporzionamento: ", e);
            throw e;
        }
    }

    @PostMapping(path = "chiudi-anticipo", produces = "application/json")
    public ResponseEntity chiudiAnticipo(@RequestBody UsaAnticipoDTO dataDTO) throws Exception {
        try {
            importiAnticipoService.chiudiAnticipo(dataDTO);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            log.error("error chiudi-anticipo: ", e);
            throw e;
        }

    }

    @PostMapping(path = "riproporziona-fake", produces = "application/json")
    public ResponseEntity riproporzionaFake(@RequestBody UsaAnticipoDTO dataDTO) throws Exception {

        try {
            UsaAnticipoDTO result = importiAnticipoService.riproporzionaFake(dataDTO);

            return ResponseEntity.ok(result);
        } catch (Exception e) {
            log.error("error riproporzionamento fake: ", e);
            throw e;
        }

    }

    @PostMapping(path = "chiudi-anticipo-fake", produces = "application/json")
    public ResponseEntity chiudiAnticipoFake(@RequestBody UsaAnticipoDTO dataDTO) {
        try {
            UsaAnticipoDTO result = importiAnticipoService.chiudiAnticipoFake(dataDTO);

            return ResponseEntity.ok(result);
        } catch (Exception e) {
            log.error("error riproporzionamento fake: ", e);
        }
        return ResponseEntity.ok(dataDTO);

    }
}
