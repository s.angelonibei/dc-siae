package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;


import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class FiltroCaricoRipartizioneDTO {
  private String idRipartizioneSiada;
  private List<InvoiceCCIDDTO> ccidList;

}
