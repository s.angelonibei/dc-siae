package com.alkemy.sophia.api.sophiamsinvoice.invoice.service;
import com.alkemy.sophia.api.sophiamsinvoice.invoice.dto.AnagClientDTO;
import com.alkemy.sophia.api.sophiamsinvoice.invoice.dto.BaseAnagClientDTO;
import com.alkemy.sophia.api.sophiamsinvoice.invoice.dto.PagedResult;
import com.alkemy.sophia.api.sophiamsinvoice.entities.AnagClient;
import com.alkemy.sophia.api.sophiamsinvoice.entities.ClientToDsp;
import com.alkemy.sophia.api.sophiamsinvoice.repository.AnagClientRepo;
import com.alkemy.sophia.api.sophiamsinvoice.repository.AnagDspRepo;
import com.alkemy.sophia.api.sophiamsinvoice.repository.ClientToDspRepo;
import lombok.RequiredArgsConstructor;
import org.jooq.Condition;
import org.jooq.Converter;
import org.jooq.DSLContext;
import org.jooq.DataType;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.Tables.*;
import static org.jooq.impl.DSL.field;

@Service
@RequiredArgsConstructor
public class AnagClientService {

    private final DSLContext dsl;
    private final AnagClientRepo anagClientRepo;
    private final AnagDspRepo anagDspRepo;
    private final ClientToDspRepo clientToDspRepo;

    public PagedResult getFilterClient(Integer first,
                                       Integer last,
                                       Integer clientId,
                                       String dsp,
                                       String country,
                                       String sapCode) {

        Condition condition = DSL.trueCondition();

        condition = condition.and(CLIENT_TO_DSP.END_DATE.isNull());
        if(clientId != null)
            condition = condition.and(ANAG_CLIENT.ID_ANAG_CLIENT.eq(clientId));

        if(StringUtils.hasLength(dsp))
            condition = condition.and(ANAG_DSP.IDDSP.eq(dsp));

        if(StringUtils.hasLength(country))
            condition = condition.and(ANAG_CLIENT.COUNTRY.eq(country));

        if(StringUtils.hasLength(sapCode))
            condition = condition.and(ANAG_CLIENT.CODE.eq(sapCode));

        Converter<Timestamp, Date> converter =
                Converter.of(
                        Timestamp.class,
                        Date.class,
                        t -> t == null ? null : Date.from(t.toInstant()),
                        u -> u == null ? null : new Timestamp(u.getTime())
                );
        DataType<Date> DATE_TYPE = SQLDataType.TIMESTAMP.asConvertedDataType(converter);


        List<AnagClientDTO> result = dsl.select(
                ANAG_CLIENT.ID_ANAG_CLIENT,
                ANAG_CLIENT.CODE,
                ANAG_DSP.IDDSP.as("ID_DSP"),
                ANAG_DSP.NAME.as("DSP_NAME"),
                ANAG_CLIENT.COMPANY_NAME,
                ANAG_CLIENT.COUNTRY,
                ANAG_CLIENT.VAT_CODE,
                ANAG_CLIENT.VAT_DESCRIPTION,
                ANAG_CLIENT.FOREIGN_CLIENT,
                CLIENT_TO_DSP.LICENCE,
                CLIENT_TO_DSP.START_DATE,
                CLIENT_TO_DSP.ID.as("ID_CLIENT_DSP"),
                field(CLIENT_TO_DSP.START_DATE.getName(), DATE_TYPE),
                field(CLIENT_TO_DSP.END_DATE.getName(), DATE_TYPE)
                ).from(ANAG_DSP)
                .join(CLIENT_TO_DSP).on(ANAG_DSP.IDDSP.eq(CLIENT_TO_DSP.IDDSP))
                .join(ANAG_CLIENT).on(ANAG_CLIENT.ID_ANAG_CLIENT.eq(CLIENT_TO_DSP.ANAG_CLIENT_ID))
                .where(condition).limit(last).offset(first).fetch().into(AnagClientDTO.class);


        final PagedResult pagedResult = new PagedResult();
        if (null != result) {
            final int maxrows = last - first;
            final boolean hasNext = result.size() > maxrows;
            pagedResult.setRows((hasNext ? result.subList(0, maxrows) : result)).setMaxrows(maxrows).setFirst(first)
                    .setLast(hasNext ? last : first + result.size()).setHasNext(hasNext).setHasPrev(first > 0);
        } else {
            pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
        }

        return pagedResult;
    }


    @Transactional
    public void insertClient(AnagClientDTO client){
        AnagClient anagClient = new AnagClient();
        if(client.getIdAnagClient() != null)
             anagClient = anagClientRepo.findById(client.getIdAnagClient()).orElse(new AnagClient());

        if(anagClient.getIdAnagClient() == null) {
            anagClient.setCode(client.getCode());
            anagClient.setCompanyName(client.getCompanyName());
            anagClient.setCountry(client.getCountry());
            anagClient.setForeignClient(client.getForeignClient());
            anagClient.setVatCode(client.getVatCode());
            anagClient.setVatDescription(client.getVatDescription());
            anagClient = anagClientRepo.save(anagClient);
        }
        ClientToDsp clientToDsp = new ClientToDsp();
        clientToDsp.setAnagClient(anagClient);
        clientToDsp.setStartDate(client.getStartDate()== null ? new Date()  : client.getStartDate());
        clientToDsp.setEndDate(client.getEndDate());
        clientToDsp.setLicence(client.getLicence());
        clientToDsp.setAnagDsp(anagDspRepo.findById(client.getIdDsp()).get());

        clientToDspRepo.save(clientToDsp);
    }

    public List<BaseAnagClientDTO> getAllSimpleClient(Integer idClient){
        List<BaseAnagClientDTO> result = new ArrayList<>();

        if(idClient == null)
            anagClientRepo
                    .findAllByOrderByCompanyName()
                    .forEach(c -> result.add(new BaseAnagClientDTO(c,true)));
        else
            anagClientRepo
                    .findAllByIdAnagClientOrderByCompanyName(idClient)
                    .forEach(c -> result.add(new BaseAnagClientDTO(c,true)));


        return result.stream()
                .filter(baseAnagClientDTO ->
                        baseAnagClientDTO.getDspList() != null
                                &&
                                !baseAnagClientDTO.getDspList().isEmpty())
                .collect(Collectors.toList());

    }


    @Transactional
    public void removeClientToDsp(Integer idClientDsp){
        clientToDspRepo.setEndDateToClientToDsp(idClientDsp);
    }

    @Transactional
    public void updateClientToDsp(AnagClientDTO anagClientDto){

        AnagClient anagClient = anagClientRepo.findById(anagClientDto.getIdAnagClient()).get();
        ClientToDsp clientToDsp = clientToDspRepo.findById(anagClientDto.getIdClientDsp()).get();

        anagClient.setCode(anagClientDto.getCode());
        anagClient.setCompanyName(anagClientDto.getCompanyName());
        anagClient.setCountry(anagClientDto.getCountry());
        anagClient.setVatCode(anagClientDto.getVatCode());
        anagClient.setVatDescription(anagClientDto.getVatDescription());
        anagClient.setForeignClient(anagClientDto.getForeignClient());
        clientToDsp.setLicence(anagClientDto.getLicence());
        anagClientRepo.save(anagClient);
        clientToDspRepo.save(clientToDsp);

    }
}