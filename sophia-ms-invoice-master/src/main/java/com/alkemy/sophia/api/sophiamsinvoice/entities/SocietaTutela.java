package com.alkemy.sophia.api.sophiamsinvoice.entities;

import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@Table(name = "SOCIETA_TUTELA")
public class SocietaTutela {
    @Id
    @Column(name = "ID")
    private Integer id;
    @Column(name = "CODICE")
    private String codice;
    @Column(name = "NOMINATIVO")
    private String nominativo;
    @Column(name = "HOME_TERRITORY")
    private String homeTerritory;
    @Column(name = "POSIZIONE_SIAE")
    private String posizioneSiae;
    @Column(name = "CODICE_SIADA")
    private String codiceSiada;
    @Column(name = "TENANT")
    private Boolean tenant;

}
