package com.alkemy.sophia.api.sophiamsinvoice.backclaim.service;

import com.alkemy.sophia.api.sophiamsinvoice.backclaim.dto.DsrMetadataDTO;
import com.alkemy.sophia.api.sophiamsinvoice.backclaim.dto.DsrStepsMonitoringDTO;
import com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.tables.AnagDsp;
import com.alkemy.sophia.api.sophiamsinvoice.repository.CCIDMetadataRepo;
import com.alkemy.sophia.api.sophiamsinvoice.utils.BackClaimType;
import com.alkemy.sophia.api.sophiamsinvoice.utils.CSVLoader;
import com.amazonaws.services.s3.AmazonS3URI;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.jooq.tools.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.tables.CcidMetadata.CCID_METADATA;
import static com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.tables.CcidMetadataDetails.CCID_METADATA_DETAILS;
import static com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.tables.CcidS3Path.CCID_S3_PATH;
import static com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.tables.CommercialOffers.COMMERCIAL_OFFERS;
import static com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.tables.DsrMetadata.DSR_METADATA;
import static com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.tables.DsrMetadataConfig.DSR_METADATA_CONFIG;
import static com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.tables.DsrStatistics.DSR_STATISTICS;
import static com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.tables.DsrStatisticsSophia.DSR_STATISTICS_SOPHIA;
import static com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.tables.DsrStepsMonitoring.DSR_STEPS_MONITORING;
import static com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.tables.DsrUnidentified.DSR_UNIDENTIFIED;
import static com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.tables.UnidentifiedSongDsr.UNIDENTIFIED_SONG_DSR;
import static com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.tables.MmBackclaimStatistics.MM_BACKCLAIM_STATISTICS;
import static org.jooq.impl.DSL.*;

@Service
@RequiredArgsConstructor
@CommonsLog
public class MultimediaService {
    private final DSLContext dsl;
    private final S3Service s3Service;
    private final CCIDMetadataRepo ccidMetadataRepo;

    @Bean
    @Scope("singleton")
    public CSVLoader getCSVLoader() {
        return new CSVLoader(dsl, ';');
    }

    public boolean checkIsInvoiced(String idDsr){
        if(ccidMetadataRepo.findOneByIdDsr(idDsr) != null && ccidMetadataRepo.findOneByIdDsr(idDsr).getInvoiceStatus().equalsIgnoreCase("FATTURATO")){
            log.info(idDsr + " FATTURATO! impossibile svuotare BC");
            return true;
        }
        return false;
    }
    public boolean uploadBackClaimCCID(String bucket, String newBackclaimKey, String newBackclaimVFileKey, ByteArrayOutputStream unidentifiedByteArrayOutputStream, ByteArrayOutputStream vTableOutputStream) throws IOException {
        return
                s3Service.upload(bucket, newBackclaimKey, new ByteArrayInputStream(unidentifiedByteArrayOutputStream.toByteArray())) &&
                        s3Service.upload(bucket, newBackclaimVFileKey, new ByteArrayInputStream(vTableOutputStream.toByteArray()));

    }

    public DsrMetadataDTO getDsrMetadataAndFtpSourcePath(String idDsr) {
        return dsl.select(
                DSR_METADATA.IDDSR,
                AnagDsp.ANAG_DSP.IDDSP,
                DSR_METADATA.PERIOD_TYPE,
                DSR_METADATA.PERIOD,
                DSR_METADATA.YEAR,
                DSR_METADATA.COUNTRY,
                DSR_METADATA.SALES_LINES_NUM,
                DSR_METADATA.TOTAL_VALUE,
                DSR_METADATA.SUBSCRIPTIONS_NUM,
                DSR_METADATA.BACKCLAIM,
                DSR_METADATA.ID_BACKCLAIM_IN_PROGRESS,
                DSR_METADATA.BC_TYPE,
                AnagDsp.ANAG_DSP.FTP_SOURCE_PATH,
                CCID_S3_PATH.S3_PATH
        )
                .from(DSR_METADATA.leftJoin(CCID_S3_PATH).on(CCID_S3_PATH.IDDSR.eq(DSR_METADATA.IDDSR)), AnagDsp.ANAG_DSP)
                .where(DSR_METADATA.IDDSR.eq(idDsr))
                .and(replace(DSR_METADATA.IDDSP, " ", "")
                        .eq(AnagDsp.ANAG_DSP.IDDSP))
                .fetchSingleInto(DsrMetadataDTO.class);
    }

    public void upsertBcStatistics(String idDsrBC, BigDecimal utilizzazioni,BigDecimal numSubscription, BigDecimal totalValue){
        dsl.transaction(configuration -> {
            using(configuration).insertInto(MM_BACKCLAIM_STATISTICS)
                    .columns(MM_BACKCLAIM_STATISTICS.ID_DSR,
                            MM_BACKCLAIM_STATISTICS.TOTALE_UTILIZZAZIONI,
                            MM_BACKCLAIM_STATISTICS.TOTALE_SUBSCRIPTION,
                            MM_BACKCLAIM_STATISTICS.TOTALE_VALORE)
                    .values(idDsrBC,utilizzazioni,numSubscription,totalValue)
                    .onDuplicateKeyUpdate()
                    .set(MM_BACKCLAIM_STATISTICS.TOTALE_UTILIZZAZIONI, utilizzazioni)
                    .set(MM_BACKCLAIM_STATISTICS.TOTALE_SUBSCRIPTION, numSubscription)
                    .set(MM_BACKCLAIM_STATISTICS.TOTALE_VALORE, totalValue).execute();
        });
    }

    public boolean updateBackclaimCounter(String idDsr, Integer backclaim, Integer value) {
        AtomicBoolean updated = new AtomicBoolean(false);
        if (!backclaim.equals(0) && !value.equals(-1)) {
            dsl.transaction(configuration -> {
                updated.set(using(configuration).update(
                        DSR_METADATA)
                        .set(DSR_METADATA.BACKCLAIM, DSR_METADATA.BACKCLAIM.add(value))
                        .where(DSR_METADATA.IDDSR.eq(idDsr))
                        .execute() > 0);

            });

            /*SELECT DISTINCT mc.*
                    FROM COMMERCIAL_OFFERS co, DSR_METADATA_CONFIG mc, DSR_METADATA m
                    WHERE
            mc.ID_COMMERCIAL_OFFER = co.ID_COMMERCIAL_OFFERS
            AND m.SERVICE_CODE = co.ID_COMMERCIAL_OFFERS
            AND 'Report_YouTube_bmat_IT_201902_20190310065429_masterlist' REGEXP mc.REGEX*/

                    dsl.transaction(configuration -> {
                        try {
                    if (!value.equals(-1)) {

                        DSL.using(configuration).insertInto(DSR_METADATA_CONFIG).
                                columns(DSR_METADATA_CONFIG.ID_COMMERCIAL_OFFER,
                                        DSR_METADATA_CONFIG.REGEX,
                                        DSR_METADATA_CONFIG.REGEX_CONFIG,
                                        DSR_METADATA_CONFIG.REGEX_REPRESENTATION,
                                        DSR_METADATA_CONFIG.COUNTRY_CODE,
                                        DSR_METADATA_CONFIG.PERIOD_CODE,
                                        DSR_METADATA_CONFIG.START_INDEX_COUNTRY,
                                        DSR_METADATA_CONFIG.END_INDEX_COUNTRY,
                                        DSR_METADATA_CONFIG.START_INDEX_PERIOD,
                                        DSR_METADATA_CONFIG.END_INDEX_PERIOD)
                                .select(DSL.using(configuration)
                                        .selectDistinct(DSR_METADATA_CONFIG.ID_COMMERCIAL_OFFER,
                                                DSR_METADATA_CONFIG.REGEX.concat("_BC_[\\w]{1,2}_" + StringUtils.leftPad(backclaim + "", 4, "0")),
                                                DSR_METADATA_CONFIG.REGEX_CONFIG.replace("]", ",{\"stringa\":\"" + StringUtils.leftPad(backclaim + "", 4, "0") + "\"}]"),
                                                DSR_METADATA_CONFIG.REGEX_REPRESENTATION.concat("_" + StringUtils.leftPad(backclaim + "", 4, "0")),
                                                DSR_METADATA_CONFIG.COUNTRY_CODE,
                                                DSR_METADATA_CONFIG.PERIOD_CODE,
                                                DSR_METADATA_CONFIG.START_INDEX_COUNTRY,
                                                DSR_METADATA_CONFIG.END_INDEX_COUNTRY,
                                                DSR_METADATA_CONFIG.START_INDEX_PERIOD,
                                                DSR_METADATA_CONFIG.END_INDEX_PERIOD)
                                        .from(DSR_METADATA_CONFIG)
                                        .join(COMMERCIAL_OFFERS)
                                        .on(DSR_METADATA_CONFIG.ID_COMMERCIAL_OFFER
                                                .eq(COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS))
                                        .join(DSR_METADATA)
                                        .on(DSR_METADATA.SERVICE_CODE
                                                .eq(COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS))
                                        .where("'" + idDsr + "' REGEXP REGEX"))
                                .execute();
                    } else {
                        deleteDsrMetadataConfig(idDsr, configuration);
                    }
                }catch (Exception e){log.error("Errore BC DSR_METADATA_CONFIG: ",e);}
                });


        }
        return updated.get();
    }

    public boolean updateBackclaimInProgress(String idDsr, Short value) {
        UpdateConditionStep updateConditionStep = this.dsl.update(DSR_METADATA)
                .set(DSR_METADATA.ID_BACKCLAIM_IN_PROGRESS, value)
                .where(DSR_METADATA.IDDSR.eq(idDsr));
        if (value.equals(0)) {
            updateConditionStep = updateConditionStep.and(DSR_METADATA.ID_BACKCLAIM_IN_PROGRESS.ne((short) 1));
        }
        return updateConditionStep.execute() > 0;
    }

    public boolean updateBackclaimInProgress(String idDsr, Short value, BackClaimType bcType) {
        UpdateConditionStep updateConditionStep = this.dsl.update(DSR_METADATA)
                .set(DSR_METADATA.ID_BACKCLAIM_IN_PROGRESS, value)
                .set(DSR_METADATA.BC_TYPE, bcType.getCode())
                .where(DSR_METADATA.IDDSR.eq(idDsr));
        if (value.equals(0)) {
            updateConditionStep = updateConditionStep.and(DSR_METADATA.ID_BACKCLAIM_IN_PROGRESS.ne((short) 1));
        }
        return updateConditionStep.execute() > 0;
    }

    public boolean isFinishedBackclaim(String idDsrBackclaim) {
        return this.dsl.fetchExists(
                this.dsl.selectOne()
                        .from(DSR_METADATA)
                        .join(CCID_METADATA)
                        .on(DSR_METADATA.IDDSR.eq(CCID_METADATA.ID_DSR))
                        .where(DSR_METADATA.IDDSR.equalIgnoreCase(idDsrBackclaim)
                                .and(CCID_METADATA.ID_CCID.isNotNull())));
    }

    public boolean voidBackclaim(List<String> idDsrBackclaims, String originalIdDsr, String s3UriPath){ //}, AmazonS3URI originalSourceUri, AmazonS3URI originalSourceVUri, AmazonS3URI ccidIdSourceUri) {
        final boolean[] isUpdated = {false,false};

        this.dsl.transaction(configuration -> isUpdated[0] =
                        deleteDsrMetadataConfig(originalIdDsr, configuration) +
                                updateDsrMetadata(originalIdDsr, configuration) > 0
        );

        for (String idDsrBackclaim : idDsrBackclaims) {
            DsrMetadataDTO dsrMetadataBackclaimDTO = null;
            AmazonS3URI ccidIdSourceUri = null;
            try {
                dsrMetadataBackclaimDTO = getDsrMetadataAndFtpSourcePath(idDsrBackclaim);
            } catch(Exception e){}

            if (dsrMetadataBackclaimDTO != null && dsrMetadataBackclaimDTO.getS3Path() != null) {
                ccidIdSourceUri = new AmazonS3URI(dsrMetadataBackclaimDTO.getS3Path());
            }

            final AmazonS3URI originalSourceUri = new AmazonS3URI(s3UriPath + idDsrBackclaim + ".csv.gz");
            final AmazonS3URI originalSourceVUri = new AmazonS3URI(s3UriPath + idDsrBackclaim + "_V.csv");



            this.dsl.transaction(configuration -> isUpdated[1] =
                                        deleteDsrStatistics(idDsrBackclaim, configuration)
                                    + deleteDsrStatisticsSophia(idDsrBackclaim, configuration)
                                    + deleteDsrStepsMonitoring(idDsrBackclaim, configuration)
//                        + deleteDsrUnidentified(idDsrBackclaim, configuration)
                                    + deleteCcidMetadataDetails(idDsrBackclaim, configuration)
                                    + deleteCcidMetadata(idDsrBackclaim, configuration)
                                    + deleteCcidS3Path(idDsrBackclaim, configuration)
//                        + deleteUnidentifiedSongDsr(idDsrBackclaim, configuration)
                                    + deleteDsrMetadata(idDsrBackclaim, configuration) > 0
            );


            s3Service.delete(originalSourceUri.getBucket(), originalSourceUri.getKey());
            s3Service.delete(originalSourceVUri.getBucket(), originalSourceVUri.getKey());
            if(ccidIdSourceUri != null)
                s3Service.delete(ccidIdSourceUri.getBucket(), ccidIdSourceUri.getKey());

        }
        return isUpdated[0] || isUpdated[1];



    }


    //DA TESTARE
    public int updateDsrMetadata(String idDsr, Configuration configuration) {
        return DSL.using(configuration).update(DSR_METADATA)
                .set(DSR_METADATA.BACKCLAIM, when(DSR_METADATA.BACKCLAIM.lessOrEqual(0L),(long)0)
                        .otherwise(DSR_METADATA.BACKCLAIM.minus(1)))
                .set(DSR_METADATA.ID_BACKCLAIM_IN_PROGRESS,
                        when(DSR_METADATA.BACKCLAIM.eq(0L), (short) 0)
                                .otherwise((short) 2))
                .set(DSR_METADATA.BC_TYPE, when(DSR_METADATA.BACKCLAIM.greaterOrEqual(1L),DSR_METADATA.BC_TYPE)
                        .otherwise(inline(null ,DSR_METADATA.BC_TYPE)))
                .where(DSR_METADATA.IDDSR.eq(idDsr)).execute();
    }

    private int deleteDsrStatistics(String idDsr, Configuration configuration) {
        return DSL.using(configuration).delete(DSR_STATISTICS)
                .where(DSR_STATISTICS.IDDSR.eq(idDsr)).execute();
    }

    private int deleteDsrMetadataConfig(String idDsr, Configuration configuration) {
        Integer result = DSL.using(configuration)
                .select()
                .from(DSR_METADATA)
                .join(COMMERCIAL_OFFERS)
                .on(DSR_METADATA.SERVICE_CODE
                        .eq(COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS))
                .join(DSR_METADATA_CONFIG)
                .on(DSR_METADATA_CONFIG.ID_COMMERCIAL_OFFER
                        .eq(COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS))
                .where(DSR_METADATA.IDDSR.eq(idDsr).and(DSR_METADATA_CONFIG.REGEX.contains("_BC_")))
                .orderBy(DSR_METADATA_CONFIG.ID_DSR_METADATA_CONFIG.desc())
                .limit(1)
                .fetchOne(DSR_METADATA_CONFIG.ID_DSR_METADATA_CONFIG);

        return DSL.using(configuration).delete(DSR_METADATA_CONFIG)
                .where(DSR_METADATA_CONFIG.ID_DSR_METADATA_CONFIG.eq(result))
                .execute();
    }

    private int deleteDsrMetadata(String idDsr, Configuration configuration) {
        return DSL.using(configuration).delete(DSR_METADATA)
                .where(DSR_METADATA.IDDSR.eq(idDsr)).execute();
    }

    private int deleteDsrStatisticsSophia(String idDsr, Configuration configuration) {
        return DSL.using(configuration).delete(DSR_STATISTICS_SOPHIA)
                .where(DSR_STATISTICS_SOPHIA.IDDSR.eq(idDsr)).execute();
    }

    private int deleteDsrStepsMonitoring(String idDsr, Configuration configuration) {
        return DSL.using(configuration).delete(DSR_STEPS_MONITORING)
                .where(DSR_STEPS_MONITORING.IDDSR.eq(idDsr)).execute();
    }

    private int deleteDsrUnidentified(String idDsr, Configuration configuration) {
        return DSL.using(configuration).delete(DSR_UNIDENTIFIED)
                .where(DSR_UNIDENTIFIED.IDDSR.eq(idDsr)).execute();
    }
        
    private int deleteCcidMetadata(String idDsr, Configuration configuration) {
        return DSL.using(configuration).delete(CCID_METADATA)
                .where(CCID_METADATA.ID_DSR.eq(idDsr)).execute();
    }
    
    private int deleteCcidMetadataDetails(String idDsr, Configuration configuration) {
        return DSL.using(configuration).delete(CCID_METADATA_DETAILS)
                .where(CCID_METADATA_DETAILS.ID_DSR.eq(idDsr)).execute();
    }

    private int deleteCcidS3Path(String idDsr, Configuration configuration) {
        return DSL.using(configuration).delete(CCID_S3_PATH)
                .where(CCID_S3_PATH.IDDSR.eq(idDsr)).execute();
    }

    private int deleteUnidentifiedSongDsr(String idDsr, Configuration configuration) {
        return DSL.using(configuration).delete(UNIDENTIFIED_SONG_DSR)
                .where(UNIDENTIFIED_SONG_DSR.ID_DSR.eq(idDsr)).execute();
    }

    public DsrStepsMonitoringDTO getDsrStepsMonitoring(String idDsrBackclaim) {
        try {
            Table nested = this.dsl.select(
                    when(DSR_STEPS_MONITORING.EXTRACT_STATUS.eq("OK"), 1)
                            .when(DSR_STEPS_MONITORING.EXTRACT_STATUS.eq("KO"), -100).otherwise(0).as("EXTRACT_STATUS"),
                    when(DSR_STEPS_MONITORING.CLEAN_STATUS.eq("OK"), 1)
                            .when(DSR_STEPS_MONITORING.CLEAN_STATUS.eq("KO"), -100).otherwise(0).as("CLEAN_STATUS"),
                    when(DSR_STEPS_MONITORING.PRICE_STATUS.eq("OK"), 1)
                            .when(DSR_STEPS_MONITORING.PRICE_STATUS.eq("KO"), -100).otherwise(0).as("PRICE_STATUS"),
                    when(DSR_STEPS_MONITORING.HYPERCUBE_STATUS.eq("OK"), 1)
                            .when(DSR_STEPS_MONITORING.HYPERCUBE_STATUS.eq("KO"), -100).otherwise(0).as("HYPERCUBE_STATUS"),
                    when(DSR_STEPS_MONITORING.IDENTIFY_STATUS.eq("OK"), 1)
                            .when(DSR_STEPS_MONITORING.IDENTIFY_STATUS.eq("KO"), -100).otherwise(0).as("IDENTIFY_STATUS"),
                    when(DSR_STEPS_MONITORING.UNILOAD_STATUS.eq("OK"), 1)
                            .when(DSR_STEPS_MONITORING.UNILOAD_STATUS.eq("KO"), -100).otherwise(0).as("UNILOAD_STATUS"),
                    when(DSR_STEPS_MONITORING.CLAIM_STATUS.eq("OK"), 1)
                            .when(DSR_STEPS_MONITORING.CLAIM_STATUS.eq("KO"), -100).otherwise(0).as("CLAIM_STATUS"))
                    .from(DSR_STEPS_MONITORING)
                    .where(DSR_STEPS_MONITORING.IDDSR.eq(idDsrBackclaim)).asTable("DSR_STEPS_MONITORING");

            Field status = coalesce(nested.field("EXTRACT_STATUS", Integer.class), 0)
                    .add(coalesce(nested.field("CLEAN_STATUS", Integer.class), 0))
                    .add(coalesce(nested.field("PRICE_STATUS", Integer.class), 0))
                    .add(coalesce(nested.field("IDENTIFY_STATUS", Integer.class), 0))
                    .add(coalesce(nested.field("UNILOAD_STATUS", Integer.class), 0))
                    .add(coalesce(nested.field("CLAIM_STATUS", Integer.class), 0));
            return this.dsl.select(
                    when(status.eq(6)
                            .and(coalesce(nested.field("HYPERCUBE_STATUS", Integer.class), 0).eq(0)
                                    .or(coalesce(nested.field("HYPERCUBE_STATUS", Integer.class), 0).eq(1))), 1)

                            .when(status.lessThan(0), -1)
                            .otherwise(0).as("STATUS"),
                    nested.field("EXTRACT_STATUS"),
                    nested.field("CLEAN_STATUS"),
                    nested.field("PRICE_STATUS"),
                    nested.field("HYPERCUBE_STATUS"),
                    nested.field("IDENTIFY_STATUS"),
                    nested.field("UNILOAD_STATUS"),
                    nested.field("CLAIM_STATUS"))
                    .from(nested).fetchOne().into(DsrStepsMonitoringDTO.class);
        } catch (NullPointerException e) {
            log.error("DSR_STEPS_MONITORING STILL NOT UPDATED", e);
        }
        DsrStepsMonitoringDTO dsrStepsMonitoringDTO = new DsrStepsMonitoringDTO();
        dsrStepsMonitoringDTO.setStatus(0);
        return dsrStepsMonitoringDTO;
    }
}