package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;

public class InvoiceStatus {

	private String code;
	private String description;

	public InvoiceStatus() {

	}

	public InvoiceStatus(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
