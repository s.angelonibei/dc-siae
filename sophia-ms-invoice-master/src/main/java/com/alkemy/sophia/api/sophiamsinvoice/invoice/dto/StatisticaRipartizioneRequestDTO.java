package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;

import lombok.*;

import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor
public class StatisticaRipartizioneRequestDTO extends BaseRequestDTO {

    @NonNull
    private Integer idRipartizioneSiada;
    private Boolean paginationActive = true;

    @Builder
    @SuppressWarnings("unused")
    private StatisticaRipartizioneRequestDTO(Integer idRipartizioneSiada, Boolean paginationActive) {
        this.idRipartizioneSiada = idRipartizioneSiada;
        this.paginationActive = Optional.ofNullable(paginationActive).orElse(this.paginationActive);
    }

}
