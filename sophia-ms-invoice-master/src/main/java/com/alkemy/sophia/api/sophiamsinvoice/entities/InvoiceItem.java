package com.alkemy.sophia.api.sophiamsinvoice.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity(name = "InvoiceItem")
@Table(name = "INVOICE_ITEM")
@NamedQueries({@NamedQuery(name = "InvoiceItem.GetAll", query = "SELECT x FROM InvoiceItem x")})
@Data
@EqualsAndHashCode
public class InvoiceItem {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "ID_ITEM", nullable = false)
    private Integer idItem;

    @ManyToOne
    @JoinColumn(name="ID_INVOICE", referencedColumnName = "ID_INVOICE")
    private Invoice invoice;

    @ManyToOne
    @JoinColumn(name="DESCRIPTION")
    private InvoiceItemReason description;

    @Column(name = "INVOICE_POSITION")
    private Integer invoicePosition;

    @Column(name = "TOTAL")
    private BigDecimal total;

    @Column(name = "NOTE")
    private String note;

    @Column(name = "TOTAL_ORIG_CURRENCY")
    private BigDecimal totalOrigCurrency;

    @Column(name = "ORIG_CURRENCY")
    private String origCurrency;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "invoiceItem")
    private List<InvoiceItemToCcid> invoiceItemToCcid;

}
