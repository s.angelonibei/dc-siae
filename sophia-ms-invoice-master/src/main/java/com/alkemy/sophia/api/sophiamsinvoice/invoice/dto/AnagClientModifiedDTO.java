package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;

import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement
@Data
public class AnagClientModifiedDTO  {

	private String prevCode;
	private String prevCompanyName;
	private String prevCountry;
	private String prevVatDescription;
	private String prevVatCode;
	private String prevLicences;
	private String code;
	private String idDsp;
	private String companyName;
	private String country;
	private String vatCode;
	private String vatDescription;
	private String licences;
	

}
