package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;



import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@RequiredArgsConstructor
public class CaricoCsvDTO {


    private Long idRipartizioneSiada;
    private Integer versione;
    private String iddsp;
    private String periodType;
    private Integer period;
    private String periodString;
    private Integer year;
    private String country;
    private String utilizationType;
    private String commercialOffer;
    private String idDsr;
    private BigDecimal valoreFatturabile;
    private String numeroFattura;
    private Timestamp dateOfPertinence;
    private BigDecimal totalInvoice;
    private BigDecimal valoreFattura;
    private Long periodoRipartizioneId;


    public void setPeriodStringCustom() {
        String result;
        if(periodType!= null && periodType.equalsIgnoreCase("month"))
            result = mesi.get(period);
        else
            result = quarter.get(period);

        this.periodString= result +" " + year;
    }

    static Map<Integer, String> mesi = Stream.of(new Object[][] {
            { 1 , "Gen" },
            { 2 , "Feb" },
            { 3 , "Mar" },
            { 4 , "Apr" },
            { 5 , "Mag" },
            { 6 , "Giu" },
            { 7 , "Lug" },
            { 8 , "Ago" },
            { 9 , "Set" },
            { 10 , "Ott" },
            { 11 , "Nov" },
            { 12 , "Dic" }
    }).collect(Collectors.toMap(data -> (Integer) data[0], data -> (String) data[1]));

    static Map<Integer, String> quarter = Stream.of(new Object[][] {
            { 1 , "Gen - Mar" },
            { 2 , "Apr - Giu" },
            { 3 , "Lug - Set" },
            { 4 , "Ott - Dic" }
    }).collect(Collectors.toMap(data -> (Integer) data[0], data -> (String) data[1]));

}
