package com.alkemy.sophia.api.sophiamsinvoice.conifg;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.inject.Singleton;

//@PropertySource("classpath:aws.properties")
//@ConfigurationProperties(prefix = "aws")

@Configuration
@Singleton
@Data
public class Aws {

    @Value("${cloud.aws.credentials.accessKey}")
    private String accessKey;

    @Value("${cloud.aws.credentials.secretKey}")
    private String secretKey;

    @Value("${cloud.aws.credentials.username}")
    private String userName;

    @Value("${cloud.aws.region.static}")
    private String region;

    @Value("${cloud.aws.toProcessQueue}")
    private String toProcessQueue;
}
