package com.alkemy.sophia.api.sophiamsinvoice.invoice.service;

import com.alkemy.sophia.api.sophiamsinvoice.entities.InvoiceLog;
import com.alkemytech.sophia.invoice.integration.service.Tracer;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.Date;

@Service
@RequiredArgsConstructor
@CommonsLog
public class InvoiceTracer implements Tracer {

	private final EntityManager em;

	@Override
	@Transactional
	public void trace(String message, String requestId, Integer idInvoice) {

		InvoiceLog invoiceLog = null;
		try {
			String queryString = "select x from InvoiceLog x where x.requestId = :requestId ";
			Query query = em.createQuery(queryString);
			query.setParameter("requestId", requestId);

			invoiceLog = (InvoiceLog) query.getSingleResult();

			// esiste tracciamento per il messaggio, quindi bisogna inserire la
			// response
			long millis = new Date().getTime() - invoiceLog.getStartDate().getTime();

			String response = message.contains("\"KO\":false")? "KO: false": message;
//			em.getTransaction().begin();
			invoiceLog.setResponseParams(response);
			invoiceLog.setDuration(millis);
//			em.getTransaction().commit();

		} catch (NoResultException nRe) {

			try {
				// non esiste un tracciamento per il messaggio, quindi va creato
				invoiceLog = new InvoiceLog();
				invoiceLog.setRequestId(requestId);
				invoiceLog.setRequestParams(message);
				invoiceLog.setStartDate(new Date());
				invoiceLog.setIdInvoice(idInvoice);
//				em.getTransaction().begin();
				em.persist(invoiceLog);
//				em.getTransaction().commit();

			} catch (Exception e) {
				log.error("invoice tracer", e);
			}

		} catch (Exception ex) {
			log.error("invoice tracer", ex);

//			if (em.getTransaction().isActive()) {
//				em.getTransaction().rollback();
//			}
		}
	}

}
