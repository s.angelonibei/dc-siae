package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;


import com.alkemy.sophia.api.sophiamsinvoice.entities.Invoice;
import com.alkemy.sophia.api.sophiamsinvoice.entities.InvoiceAccantonamento;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@NoArgsConstructor
public class InvoiceAccantonamentoDTO {

    private Integer idInvoiceAccantonamento;
    private Invoice invoice;
    private String note;
    private BigDecimal percentuale;
    private BigDecimal importoAccantonato;
    private BigDecimal importoAccantonatoUtilizzabile;
    private Date dataInserimento;
    private Date dataFineValidita;
    private String utente;


    public Integer getidInvoiceAccantonamento() {
        return idInvoiceAccantonamento;
    }

    public InvoiceAccantonamentoDTO(InvoiceAccantonamento entity) {

        this.idInvoiceAccantonamento = new Integer(entity.getIdInvoiceAccantonamento());
        this.invoice = entity.getInvoice();
        this.note = entity.getNote();
        this.percentuale = entity.getPercentuale();
        this.importoAccantonato = entity.getImportoAccantonato();
        this.importoAccantonatoUtilizzabile = entity.getImportoAccantonatoUtilizzabile();
        this.dataInserimento = entity.getDataInserimento();
        this.dataFineValidita = entity.getDataFineValidita();
        this.utente = entity.getUtente();

    }


}
