package com.alkemy.sophia.api.sophiamsinvoice.repository;

import com.alkemy.sophia.api.sophiamsinvoice.entities.ImportiAnticipo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImportiAnticipoRepo extends JpaRepository<ImportiAnticipo, Integer> {
}
