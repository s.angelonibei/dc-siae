package com.alkemy.sophia.api.sophiamsinvoice.entities;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity(name = "InvoiceAccantonamento")
@Table(name = "INVOICE_ACCANTONAMENTO")
@Data
public class InvoiceAccantonamento {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_INVOICE_ACCANTONAMENTO", nullable = false)
    private Integer idInvoiceAccantonamento;

    @ManyToOne
    @JoinColumn(name = "ID_INVOICE", referencedColumnName = "ID_INVOICE")
    private Invoice invoice;

    @Column(name = "NOTE")
    private String note;

    @Column(name = "IMPORTO_ACCANTONATO")
    private BigDecimal importoAccantonato;

    @Column(name = "IMPORTO_ACCANTONATO_UTILIZZABILE")
    private BigDecimal importoAccantonatoUtilizzabile;

    @Column(name = "PERCENTUALE")
    private BigDecimal percentuale;

    @Column(name = "DATA_INSERIMENTO")
    private Date dataInserimento;

    @Column(name = "DATA_FINE_VALIDITA")
    private Date dataFineValidita;

    @Column(name = "UTENTE")
    private String utente;

}
