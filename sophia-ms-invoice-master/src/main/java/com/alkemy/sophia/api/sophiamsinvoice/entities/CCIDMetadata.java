package com.alkemy.sophia.api.sophiamsinvoice.entities;



import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

@Entity(name="CCIDMetadata")
@Table(name="CCID_METADATA")
@NamedQueries({@NamedQuery(name="CCIDMetadata.GetAll", query="SELECT x FROM CCIDMetadata x")})
@Data
@EqualsAndHashCode
public class CCIDMetadata {

    @Id
    @Column(name = "ID_CCID_METADATA", nullable = false)
    private Long idCCIDMetadata;

    @Column(name = "ID_CCID", nullable = false)
    private String idCcid;

    @Column(name = "ID_DSR", nullable = false)
    private String idDsr;

    @Column(name = "TOTAL_VALUE", nullable = false)
    private BigDecimal totalValue;

    @Column(name = "CURRENCY")
    private String currency;

    @Column(name = "CCID_VERSION")
    private String ccidVersion;

    @Column(name = "CCID_ENCODED")
    private Boolean ccidEncoded;

    @Column(name = "CCID_ENCODED_PROVISIONAL")
    private Boolean ccidEncodedProvisional;

    @Column(name = "CCID_NOT_ENCODED")
    private Boolean ccidNotEncoded;
    
    @Column(name = "INVOICE_ITEM")
    private Integer invoiceItem;

    @Column(name="INVOICE_STATUS")
    private String invoiceStatus;
        
    @Column(name="VALORE_RESIDUO")
    private BigDecimal valoreResiduo;
    
    @Column(name="VALORE_FATTURABILE")
    private BigDecimal valoreFatturabile;
    
    @Column(name="QUOTA_FATTURA")
    private BigDecimal quotaFattura; 

    @Column(name="SUM_USE_QUANTITY")
    private BigInteger sumUseQuantity;

    @Column(name="SUM_USE_QUANTITY_MATCHED")
    private BigInteger sumUseQuantityMatched;

    @Column(name="SUM_USE_QUANTITY_UNMATCHED")
    private BigInteger sumUseQuantityUnmatched;

    @Column(name="SUM_AMOUNT_LICENSOR")
    private BigDecimal sumAmountLicensor;

    @Column(name="SUM_AMOUNT_PAI")
    private BigDecimal sumAmountPai;
    
    @Column(name="SUM_AMOUNT_UNMATCHED")
    private BigDecimal sumAmountUnmatched;
    
    @Column(name="SUM_USE_QUANTITY_SIAE")
    private BigDecimal sumUseQuantitySiae;
    
    @Column(name="IDENTIFICATO_VALORE_PRICING")
    private BigDecimal identificatoValorePricing;
    
    @OneToMany
	@JoinColumn(name="ID_CCID_METADATA")
	private List<InvoiceItemToCcid> invoiceItemToCcid;

//    @OneToMany
//    @JoinColumn(name="ID_CCID_METADATA")
//    private List<InvoiceItemToCcid> invoiceItemToCcid;

	
}
