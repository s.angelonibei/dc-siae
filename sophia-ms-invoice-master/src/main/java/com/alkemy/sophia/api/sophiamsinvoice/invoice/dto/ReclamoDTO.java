package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import java.io.InputStream;
import java.util.Date;


@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class ReclamoDTO {
	private Integer idInvoiceReclamo;
	private String idDsp;
	private String idDsr;
	private String country;
	private String commercialOffer;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	private Date dataInserimento;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	private Date dataUltimaModifica;

	private String utente;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMM")
	private Integer period;

	private String stato;

	private String periodType;
	private Integer year;
	private Integer month;
	private String fileName;
	private InputStream file;
}