package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;


import lombok.Data;

@Data
public class ParamDTO {
    private String idDsr;
    private String aggioDrm;
    private String aggioDem;
    private String trattenutaDem;
    private String splitDem;
    private String splitDrm;
}
