package com.alkemy.sophia.api.sophiamsinvoice.entities;




import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;

@XmlRootElement
@Entity
@Table(name = "INVOICE_ITEM_TO_CCID")
@Data
@EqualsAndHashCode
public class InvoiceItemToCcid {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "ID_CCID_METADATA", referencedColumnName = "ID_CCID_METADATA")
	private CCIDMetadata ccidMedata;

	@ManyToOne
	@JoinColumn(name = "ID_INVOICE_ITEM", referencedColumnName = "ID_ITEM")
	private InvoiceItem invoiceItem;

	@Column(name="VALORE", nullable=false)
	private BigDecimal valore;

	@Column(name="VALORE_APPLICATO_IN_CHIUSURA")
	private BigDecimal valoreApplicatoInChiusura;

	@Column(name="OPERAZIONE")
	private String operazione;

	@Column(name="PERIODO_RIPARTIZIONE_ID")
	private Long periodoRipartizioneId;

}
