package com.alkemy.sophia.api.sophiamsinvoice.utils;

import org.joda.time.DateTime;

import java.util.concurrent.TimeUnit;

public class DateUtils {

    public static DateTime latest(DateTime ... list)
    {
        DateTime result = list[0];
        for (DateTime dTmp : list) {
            if(dTmp != null)
                result = dTmp.compareTo(result) > 0 ? dTmp : result;
        }

        return result;
    }

    public static DateTime oldest(DateTime ... list)
    {
        DateTime result = list[0];
        for (DateTime dTmp : list) {
            if(dTmp != null)
                result = dTmp.compareTo(result) < 0 ? dTmp : result;
        }

        return result;
    }

    public static String formatMillisToHMS(long millis) {
        return String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1),
                TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1));

    }


}
