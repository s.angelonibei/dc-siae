package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import java.util.HashMap;

@Data
@EnableConfigurationProperties
@EnableAutoConfiguration
@ConfigurationProperties(prefix ="reclami.dsr")
public class ReclamoConfigDTO {


	@Value("${reclami.csv.fieldSeparator}")
	private String fieldSeparator;

	@Value("${reclami.csv.textDelimiter}")
	private char textDelimiter;

	@Value("${reclami.csv.recordSeparator}")
	private String recordSeparator;

	@Value("${reclami.dsr.defaultValue.currency}")
	private String currency;

	@Value("${reclami.dsr.defaultValue.territory}")
	private String territory;

	private HashMap<String,Integer> columnsIndexOutput ;

	private HashMap<Integer,String> columnsIndexInput ;

}
