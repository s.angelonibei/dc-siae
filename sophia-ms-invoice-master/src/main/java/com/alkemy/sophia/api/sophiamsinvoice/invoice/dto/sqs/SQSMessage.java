package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto.sqs;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class SQSMessage {

    private Body body;
    private Header header;
    private Map<String, Object> additionalProperties = new HashMap<>();

    protected boolean canEqual(final Object other) {
        return other instanceof SQSMessage;
    }

}
