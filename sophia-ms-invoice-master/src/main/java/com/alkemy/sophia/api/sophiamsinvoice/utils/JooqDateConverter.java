package com.alkemy.sophia.api.sophiamsinvoice.utils;

import org.jooq.Converter;

import java.sql.Timestamp;
import java.util.Date;

public class JooqDateConverter implements Converter<Timestamp, Date> {

    @Override
    public Date from(Timestamp databaseObject) {

        return Date.from(databaseObject.toInstant());
    }

    @Override
    public Timestamp to(Date userObject) {
        return new Timestamp(userObject.getTime());
    }

    @Override
    public Class<Timestamp> fromType() {
        return Timestamp.class;
    }

    @Override
    public Class<Date> toType() {
        return Date.class;
    }
}
