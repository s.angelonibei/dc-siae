package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;



import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@RequiredArgsConstructor
public class InvoiceCCIDDTO {

   public enum SORT{
//        idCCID,
        idDsr ,
        totalValue,
        totalValueOrigCurrency,
        idCCIDmetadata,
//        currency,
//        ccidVersion,
//        ccidEncoded,
//        ccidEncodedProvisional,
//        ccidNotEncoded,
        invoiceStatus,
//        idDsp,
        dspName,
//        idUtilizationType,
        utilizationType,
//        idCommercialOffer,
        commercialOffer,
        country,
//        periodType,
//        period,
        periodString,
        year,
        invoiceAmount,
        valoreResiduo,
//        idAnagClient,
        companyName,
       ccidParziale,
       backclaim,
       idBackclaimInProgress,
       description,
       //solo per i risultati ccid-fattura
       numeroFattura,
       valoreFattura,
       periodoRipartizioneId

    }


    
    
    private String idCCID;
    private String idDsr;
    private BigDecimal totalValue;
    private BigDecimal totalValueOrigCurrency;
    private BigInteger idCCIDmetadata;
    private String currency;
    private String ccidVersion;
    private Boolean ccidEncoded;
    private Boolean ccidEncodedProvisional;
    private Boolean ccidNotEncoded;
    private String invoiceStatus;
    private String idDsp;
    private String dspName;
    private String idUtilizationType;
    private String utilizationType;
    private Integer idCommercialOffer;
    private String commercialOffer;
    private String country;
    private String periodType;
    private Integer period;
    private String periodString;
    private Integer year;
    private BigDecimal invoiceAmount;
    private BigDecimal valoreResiduo;
    private Integer idAnagClient;
    private String companyName;
    private Boolean ccidParziale;
    private BigDecimal backclaim;
    private String bcType;
    private Short idBackclaimInProgress;
    private String description;
    private Boolean ripartito;
    private BigDecimal valoreFattura;
    private String numeroFattura;
    private Long idInvoice;
    private Long periodoRipartizioneId;

    private Long idCcidInvoiceItem;

    public InvoiceCCIDDTO(Object[] obj) {

        if (null != obj && obj.length == 23) {
            try {

               idCCID = (String) obj[0];
               idDsr = (String) obj[1];
               totalValue = (BigDecimal) obj[2];
               idCCIDmetadata = (BigInteger) obj[3];
               currency = (String) obj[4];
               ccidVersion = (String) obj[5];
               ccidEncoded = (Boolean) obj[6];
               ccidEncodedProvisional = (Boolean) obj[7];
               ccidNotEncoded = (Boolean) obj[8];
               invoiceStatus = (String) obj[9];
               idDsp = (String) obj[10];
               dspName = (String) obj[11];
               idUtilizationType = (String) obj[12];
               utilizationType = (String) obj[13];
               idCommercialOffer = (Integer) obj[14];
               commercialOffer = (String) obj[15];
               country = (String) obj[16];
               periodType = (String) obj[17];
               period = (Integer) obj[18];
               periodString = (String) obj[19];
               year = (Integer) obj[20];
               invoiceAmount = (BigDecimal) obj[21];
               valoreResiduo =(BigDecimal) obj[22];

            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    public void setPeriodStringCustom() {
        String result;
        if(periodType!= null && periodType.equalsIgnoreCase("month"))
            result = mesi.get(period);
        else
            result = quarter.get(period);

        this.periodString= result +" " + year;
    }

    static Map<Integer, String> mesi = Stream.of(new Object[][] {
            { 1 , "Gen" },
            { 2 , "Feb" },
            { 3 , "Mar" },
            { 4 , "Apr" },
            { 5 , "Mag" },
            { 6 , "Giu" },
            { 7 , "Lug" },
            { 8 , "Ago" },
            { 9 , "Set" },
            { 10 , "Ott" },
            { 11 , "Nov" },
            { 12 , "Dic" }
    }).collect(Collectors.toMap(data -> (Integer) data[0], data -> (String) data[1]));

    static Map<Integer, String> quarter = Stream.of(new Object[][] {
            { 1 , "Gen - Mar" },
            { 2 , "Apr - Giu" },
            { 3 , "Lug - Set" },
            { 4 , "Ott - Dic" }
    }).collect(Collectors.toMap(data -> (Integer) data[0], data -> (String) data[1]));

}
