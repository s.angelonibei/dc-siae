package com.alkemy.sophia.api.sophiamsinvoice.backclaim.utils;

import com.alkemy.sophia.api.sophiamsinvoice.backclaim.service.MultimediaService;
import com.alkemy.sophia.api.sophiamsinvoice.backclaim.service.S3Service;
import com.alkemy.sophia.api.sophiamsinvoice.utils.BackClaimType;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;

import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

@RequiredArgsConstructor
@CommonsLog
public class BackclaimCcidRecursiveTask extends RecursiveTask<Set<String>> {
    private final List<S3ObjectSummary> s3ObjectSummaries;
    private final S3Service s3Service;
    private final AmazonS3URI amazonS3URI;
//    private final SseEmitter emitter;
    private final String idDsr;
    private final MultimediaService multimediaService;
    private final String tempTableName;
    private final BackClaimType backClaimType;



    private static final int THRESHOLD = 2;

    @Override
    protected Set<String> compute() {
        Set<String> longs = new HashSet<>();
        if (s3ObjectSummaries.size() > THRESHOLD) {
            Collection<BackclaimCcidRecursiveTask> backclaimRecursiveTasks =
                    createSubtasks();
            ForkJoinTask.invokeAll(backclaimRecursiveTasks);
            for (BackclaimCcidRecursiveTask backclaimRecursiveTask : backclaimRecursiveTasks) {
                longs.addAll(backclaimRecursiveTask.join());
            }
            return longs;

        } else {
            return processing(s3ObjectSummaries,backClaimType);
        }
    }

    private Collection<BackclaimCcidRecursiveTask> createSubtasks() {
        List<BackclaimCcidRecursiveTask> dividedTasks = new ArrayList<>();
        dividedTasks.add(new BackclaimCcidRecursiveTask(
                s3ObjectSummaries.subList(0, s3ObjectSummaries.size() / 2),
                s3Service,
                amazonS3URI,
//                emitter,
                idDsr,
                multimediaService,
                tempTableName,
                backClaimType));
        dividedTasks.add(new BackclaimCcidRecursiveTask(
                s3ObjectSummaries.subList(s3ObjectSummaries.size() / 2, s3ObjectSummaries.size()),
                s3Service,
                amazonS3URI,
//                emitter,
                idDsr,
                multimediaService,
                tempTableName,
                backClaimType));
        return dividedTasks;
    }

    private Set<String> processing(List<S3ObjectSummary> s3ObjectSummaries, BackClaimType tipoBC) {
        Set<String> result = new HashSet<>();
        for (final S3ObjectSummary s3ObjectSummary : s3ObjectSummaries) {
//            if (s3ObjectSummary.getKey().matches("(.)+.csv$")) {
            result.addAll(collectIDsEasy(s3Service.download(amazonS3URI.getBucket(), s3ObjectSummary.getKey()), tempTableName, s3ObjectSummary.getKey(),tipoBC));
//            }
//            } else if (s3ObjectSummary.getKey().contains(".csv.gz")) {
//                try {
//                    loadCSVOriginal(new GZIPInputStream(s3Service.download(amazonS3URI.getBucket(), s3ObjectSummary.getKey())), tempTableName, s3ObjectSummary.getKey());
//                } catch (IOException e) {
//                    log.error(e.getMessage(), e);
//                }
//            }
        }
        return result;
    }
    private char getSeparatorfromHeader(String header){
        String separator = "|";
        if (isCCIDV14(header)) {
            separator = "\t";
        }

        return separator.charAt(0);
    }

    private boolean isCCIDV14(String header){
        if (header.split("\\|").length == 1) {
           return true;
        }
        return false;
    }

    private Set<String> collectIDsEasy(InputStream inputStream, String tempTableName, String key, BackClaimType backClaimType){


        CSVParser csvParser = null;
        Set<String> result = new HashSet<>();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream,StandardCharsets.UTF_8));

            //bisogna capire il tipo di csv;
            String header = br.readLine();

            boolean isCCID14 = isCCIDV14(header);



            csvParser = new CSVParser(br, CSVFormat.newFormat(getSeparatorfromHeader(header))
//                    .withFirstRecordAsHeader()
                    .withAllowMissingColumnNames()
                    .withSkipHeaderRecord()
                    .withQuote('"')
                    .withQuoteMode(QuoteMode.MINIMAL)
            );


            log.info(key + " is being processed");



            for (CSVRecord record : csvParser) {
                try{

                    boolean aggiungiRecord = true;
                    final String codiceRiga=record.get(4);
                    //nn escludo dal dsr il non identificato
                    try{
                        final String codiceOperaUnid= record.get(10);
                        final String codiceOperaOrig= record.get(2);
                        if(codiceOperaUnid!= null && codiceOperaUnid.indexOf("U")==0 && codiceOperaUnid.substring(1).equals(codiceOperaOrig))
                            aggiungiRecord=false;
                        if(backClaimType.equals(BackClaimType.BC_ORIGINAL) ||
                                backClaimType.equals(BackClaimType.BC_INC_ALL)) {

                            //V13 X 0-23<-
                            //V14 V 0-21<-
                            String claim = isCCID14? record.get(21) : record.get(23);

                            try{
                                if(new Double(claim).equals(0.0))
                                    aggiungiRecord=false;

                            }catch(Exception e){}

                        }
                    }catch (Exception e){}

                    if(aggiungiRecord){
                        result.add(codiceRiga);
                    }

                }catch(Exception e){}
            }
            log.debug("Result collectIDsEasy size: "+ result.size());

        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }


//    private Set<Integer> collectIDs(InputStream inputStream, String tempTableName, String key) {
//        final Set<Integer>[] result = new Set[]{new HashSet<>()};
//        log.info(key + " is being processed");
//
//
////        CSVParser csvParser = new CSVParser(decoder, CSVFormat.newFormat(',')
////                .withFirstRecordAsHeader()
////                .withAllowMissingColumnNames()
////                .withSkipHeaderRecord()
////                .withQuote('"')
////        );
//
//        //File ccidSongFile = StreamUtil.stream2file(inputStream);
//
//        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream,
//                StandardCharsets.UTF_8));
//
//        try {
//            final Table[] finalTable = {null};
//            char separator = '|';
//            String header = br.readLine();
//
//            if (header.split(Pattern.quote(String.valueOf(separator))).length == 1) {
//                separator = '\t';
//            }
//            char finalSeparator = separator;
//            final AtomicInteger i = new AtomicInteger();
//            final ColumnType[][] columnTypes = {new ColumnType[0]};
//
//            columnTypes[0] = new ColumnType[StringUtils.countMatches(br.readLine(), String.valueOf(finalSeparator)) + 1];
//            Arrays.fill(columnTypes[0], SKIP);
//            columnTypes[0][4] = INTEGER;
//
//            try (Stream<String> lFileStream = br.lines().parallel()) {
//                lFileStream.filter(line -> (StringUtils.isNotEmpty(line) && (StringUtils.countMatches(line, String.valueOf(finalSeparator)) + 1) == columnTypes[0].length) && !line.equalsIgnoreCase(header))
//                        .map(line -> {
//                            log.debug((i.get() + 1) + ". " + line);
//                            Table table = null;
//                            try {
//                                table = Table.read().usingOptions(CsvReadOptions.builderFromString(line)
//                                        .tableName(tempTableName)
//                                        .separator(finalSeparator)
//                                        .quoteChar(CSVWriter.DEFAULT_ESCAPE_CHARACTER)
//                                        .header(false)
//                                        .sample(false)
//                                        .columnTypes(columnTypes[0])
//                                        .build());
////                                finalTable[0] = finalTable[0] != null ? finalTable[0].append(table) : table;
//                            } catch (IOException e) {
//                                log.error(e.getMessage(), e);
//                            }
//                            i.getAndIncrement();
//                            return table;
//                        })
//                        .forEachOrdered(table -> {
//                            if (finalTable[0] != null)
//                                finalTable[0].append(table);
//                            else
//                                finalTable[0] = table;
//                        });
//                if (finalTable[0].columnCount() >= 1) {
//                    result[0] = finalTable[0].column(0)
//                            .asStringColumn()
//                            .asList()
//                            .stream()
////                        .map(value ->
////                                !StringUtils.isNullOrEmpty(value.substring((idDsr + "_").length())) ?
////                                        Integer.parseInt(value.substring((idDsr + "_")
////                                                .length())) + 1 : 0)
//
//                            .map(Integer::parseInt)
//                            .collect(Collectors.toSet());
//                }
//            }
//        } catch (IOException e) {
//            log.error(e.getMessage(), e);
//        }
//
//
////            multimediaService.getCSVLoader().createTable(table, tempTableName);
////            multimediaService.getCSVLoader().loadCSV(tempTableName, table);
//
//        return result[0];
//
//    }

    public static void main(String[] args) {



        System.out.println(new Double("0").equals(0.0));
        System.out.println(new Double("0.0") == 0);
        System.out.println(new Double("0.00") == 0);
        System.out.println(new Double("0.01") == 0);
        System.out.println(new Double("1.0") == 0);
        System.out.println(new Double("10") == 0);

        System.out.println("pre");
        System.out.println('|');
        System.out.println("post");

//            String separator = "\t";
            String separator = "\\|";

        List<String> listRecord = new ArrayList<String>();
                listRecord.add("HD|13|");
                listRecord.add("HD|13|20190724|SIAE|spotify|46577|IT|20170701|20170731|EUR|EUR|100000|SIAE_WORK_CODE");
                listRecord.add("ID|ORI|465770000000000||43334215|FAMILYPLAN6|5eRYWTEkuixIKMskzsAC0S|5eRYWTEkuixIKMskzsAC0S|ITZ040100700|T0051097687|00241026300|WWW MIPIACITU|PDS|ST|41|ITSP|M|27|27|11820|10000|NR|A|9175|||0||444621|9200|9167|111458|333163|||0|0|||0|0|||0|0||||");
                listRecord.add("ID|ORI|46577000000000013||95099|FAMILYPLAN6|1btDhQg2IjyF3Vewjy7WYr|1btDhQg2IjyF3Vewjy7WYr|SEYOK1652694||U46577000000000013|\"WEEKEND CHILL | LOUNGLICIOUS\"|PDS|ST|1|ITSP|M|27|27|11820|10000|NR|A|0||10000|0||11820|0|0|0|0|||0|0|||2955|8865|||0|0|||10000|10000");
                listRecord.add("ID|ORI|46577000000000011||93877|FAMILYPLAN6|1dG3TwlyYAgbKRzBUDjxjq|1dG3TwlyYAgbKRzBUDjxjq|||US46577000000000011|\"BEAUTIFUL BACH PIANO | LOUNGLICIOUS\"|PDS|ST|1|ITSP|M|27|27|11820|10000|NR|A|0||10000|0||11820|0|0|0|0|||0|0|||2955|8865|||0|0|||10000|10000");


        listRecord.add("HD\t14\t20190806\tSIAE\tyoutube masterlist\t47412\tDE\t20190601\t20190630\tUSD\tUSD\t1.00000\tSIAE_WORK_CODE\t\tIN\tUnknown\tGOOGLE IRELAND\tOnDemandStream\t0.33\t0.67\tVuoto\n");
        listRecord.add("ID\tORI\t47412000000000001\t\t106001\t\tA269512687352755\tA568456323873339\t\tT9006741022\ta9580734-d029-4448-ad0a-cfc60e041741\tBACK TO EARTH\t3949\tITSP\tM\t1.0\t1.0\t1.0\t1.0\t100.00\tNR\t0.00\t0.00\t0.00\t0.00\t100.00\t0.0\t0.00\t0.00\t0.0\t0.0\t0.0\t0.0\t0.0\t0.0\t1316.20167815685272216797\t2632.79832184314727783203\t0.0\t0.0\t0.00\t0.00\t0.00\t0.00\t100.00\t100.00\t0.00\t0.00\n");
        listRecord.add("ID\tORI\t47412000000000002\t\t105935\t\tA217782069381021\tA277369564293874\tGBCEN1300607\tT9131083039\t17251cc0-7758-429b-b6ef-b2195718b813\tSIGHTS\t3931\tITSP\tM\t1.0\t1.0\t1.0\t1.0\t100.00\tNR\t0.00\t0.00\t0.00\t0.00\t100.00\t0.0\t0.00\t0.00\t0.0\t0.0\t0.0\t0.0\t0.0\t0.0\t1310.20227825641632080078\t2620.79772174358367919922\t0.0\t0.0\t0.00\t0.00\t0.00\t0.00\t100.00\t100.00\t0.00\t0.00\n");

        List<String> listNumRecord = new ArrayList<>();


        int i = 100;

        for (String line : listRecord) {
            try{
                String[] splittedLine = line.split(separator);
                boolean aggiungiRecord = true;
                final String codiceRiga=splittedLine[4];
                //nn escludo dal dsr il non identificato
                try{
                    final String codiceOperaUnid= splittedLine[10];
                    final String codiceOperaOrig= splittedLine[2];
                    if(codiceOperaUnid!= null && codiceOperaUnid.indexOf("U")==0 && codiceOperaUnid.substring(1).equals(codiceOperaOrig))
                        aggiungiRecord=false;
                }catch (Exception e){}

                if(aggiungiRecord){
                    listNumRecord.add(codiceRiga);
                    if(i-- > 0) log.debug("DEBUG: record da scartare:"+ codiceRiga);
                }

            }catch(Exception e){}
        }

        listNumRecord.stream().forEach(System.out::println);

//        listRecord.stream()
//                .map(data -> data.split( separator)[4])
//                .forEach(System.out::println);



    }


}