package com.alkemy.sophia.api.sophiamsinvoice.repository;

import com.alkemy.sophia.api.sophiamsinvoice.entities.Invoice;
import com.alkemy.sophia.api.sophiamsinvoice.entities.SocietaTutela;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface SocietaTutelaRepo extends JpaRepository<SocietaTutela, Integer>{

    SocietaTutela findFirstByCodice(String codice);
}
