//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.alkemy.sophia.api.sophiamsinvoice.conifg;


import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.auth.PropertiesFileCredentialsProvider;
import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressEventType;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.transfer.Download;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.amazonaws.services.s3.transfer.Upload;
import com.amazonaws.services.s3.transfer.Transfer.TransferState;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class S3 {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final Properties configuration;
    private TransferManager transferManager;
    private AmazonS3 s3Client;
    private Aws aws;

    @Inject
    public S3(@Named("configuration") Properties configuration) {
        this.configuration = configuration;
    }

    public synchronized S3 startup() {
        if (null == this.transferManager) {
            String proxyHost = this.configuration.getProperty("http.proxy.host");
            String proxyPort = this.configuration.getProperty("http.proxy.port", "8080");
            ClientConfiguration clientConfiguration = new ClientConfiguration();
            if (!Strings.isNullOrEmpty(proxyHost)) {
                clientConfiguration.setProxyHost(proxyHost);
                clientConfiguration.setProxyPort(Integer.parseInt(proxyPort));
            }

            String region = this.configuration.getProperty("aws.region", "eu-west-1");
            String credentialsFilePath = this.configuration.getProperty("aws.credentials");
            AWSCredentialsProvider credentials = Strings.isNullOrEmpty(credentialsFilePath) ? new ClasspathPropertiesFileCredentialsProvider() : new PropertiesFileCredentialsProvider(credentialsFilePath);
            this.s3Client = (AmazonS3)((AmazonS3ClientBuilder)((AmazonS3ClientBuilder)((AmazonS3ClientBuilder)AmazonS3ClientBuilder.standard().withCredentials((AWSCredentialsProvider)credentials)).withClientConfiguration(clientConfiguration)).withRegion(region)).build();
            this.transferManager = TransferManagerBuilder.standard().withS3Client(this.s3Client).build();
            logger.debug("Ecco le credenziali amazon "+s3Client.getRegionName()+" - "+credentials.getCredentials().getAWSAccessKeyId()+" - "+credentials.getCredentials().getAWSSecretKey());
        }

        return this;
    }

    public synchronized S3 shutdown() {
        if (null != this.transferManager) {
            this.transferManager.shutdownNow(true);
            this.transferManager = null;
        }

        if (null != this.s3Client) {
            this.s3Client.shutdown();
            this.s3Client = null;
        }

        return this;
    }

    public boolean upload(S3.Url url, final File file) {
        int retries = Integer.parseInt(this.configuration.getProperty("s3.retry.put_object", "1"));

        while(retries > 0) {
            try {
                this.logger.debug("uploading file {} to {}", file.getName(), url);
                PutObjectRequest putObjectRequest = new PutObjectRequest(url.bucket, url.key.replace("//", "/"), file);
                putObjectRequest.setGeneralProgressListener(new ProgressListener() {
                    private long accumulator = 0L;
                    private long total = 0L;
                    private long chunk = Math.max(262144L, file.length() / 10L);

                    public void progressChanged(ProgressEvent event) {
                        if (ProgressEventType.REQUEST_BYTE_TRANSFER_EVENT.equals(event.getEventType())) {
                            this.accumulator += event.getBytes();
                            this.total += event.getBytes();
                            if (this.accumulator >= this.chunk) {
                                this.accumulator -= this.chunk;
                                S3.this.logger.debug("transferred {}Kb/{}Kb of file {}", new Object[]{this.total / 1024L, file.length() / 1024L, file.getName()});
                            }
                        } else if (ProgressEventType.TRANSFER_COMPLETED_EVENT.equals(event.getEventType())) {
                            S3.this.logger.debug("transfer of file {} completed", file.getName());
                        }

                    }
                });
                Upload upload = this.transferManager.upload(putObjectRequest);
                upload.waitForCompletion();
                this.logger.debug("file {} successfully uploaded to {}", file.getName(), url);
                return true;
            } catch (Exception var6) {
                this.logger.error("upload", var6);
                --retries;
            }
        }

        this.logger.debug("upload: upload failed {}", file.getName());
        return false;
    }

    public boolean copy(S3.Url fromUrl, S3.Url toUrl) {
        try {
            CopyObjectRequest request = new CopyObjectRequest(fromUrl.bucket, fromUrl.key, toUrl.bucket, toUrl.key);
            return null != this.s3Client.copyObject(request);
        } catch (Exception var4) {
            this.logger.error("copy", var4);
            return false;
        }
    }

    public boolean delete(S3.Url url) {
        int retries = Integer.parseInt(this.configuration.getProperty("s3.retry.delete_object", "1"));

        while(retries > 0) {
            try {
                this.s3Client.deleteObject(url.bucket, url.key);
                return true;
            } catch (Exception var4) {
                this.logger.error("delete", var4);
                --retries;
            }
        }

        return false;
    }

    public boolean download(S3.Url url, File file) {
        for(int retries = Integer.parseInt(this.configuration.getProperty("s3.retry.download_to_file", "1")); retries > 0; --retries) {
            try {
                file.delete();
                Download download = this.transferManager.download(url.bucket, url.key, file);
                download.waitForCompletion();
                if (TransferState.Completed == download.getState() && file.exists()) {
                    return true;
                }
            } catch (Exception var5) {
                this.logger.error("download", var5);
            }
        }

        return false;
    }

    public boolean download(S3.Url url, OutputStream out) {
        try {
            S3Object object = this.s3Client.getObject(new GetObjectRequest(url.bucket, url.key));
            InputStream in = object.getObjectContent();
            Throwable var5 = null;

            try {
                byte[] bytes = new byte[1024];
                boolean var7 = false;

                int count;
                while(-1 != (count = in.read(bytes))) {
                    out.write(bytes, 0, count);
                }
            } catch (Throwable var16) {
                var5 = var16;
                throw var16;
            } finally {
                if (in != null) {
                    if (var5 != null) {
                        try {
                            in.close();
                        } catch (Throwable var15) {
                            var5.addSuppressed(var15);
                        }
                    } else {
                        in.close();
                    }
                }

            }

            return true;
        } catch (Exception var18) {
            this.logger.error("download", var18);
            return false;
        }
    }

    public List<S3ObjectSummary> listObjects(S3.Url url) {
        return this.listObjects(url, false);
    }

    public List<S3ObjectSummary> listObjects(S3.Url url, boolean excludeKey) {
        int retries = Integer.parseInt(this.configuration.getProperty("s3.retry.list_objects", "1"));

        while(retries > 0) {
            try {
                List<S3ObjectSummary> objects = null;
                ListObjectsV2Request request = (new ListObjectsV2Request()).withBucketName(url.bucket).withPrefix(url.key);
                if (excludeKey) {
                    request.withStartAfter(url.key);
                }

                ListObjectsV2Result result;
                do {
                    result = this.s3Client.listObjectsV2(request);
                    List<S3ObjectSummary> summaries = result.getObjectSummaries();
                    if (null != summaries) {
                        if (null == objects) {
                            objects = new ArrayList();
                        }

                        objects.addAll(summaries);
                    }

                    request.setContinuationToken(result.getNextContinuationToken());
                } while(result.isTruncated());

                return objects;
            } catch (Exception var8) {
                this.logger.error("listObjects", var8);
                --retries;
            }
        }

        return null;
    }

    public S3Object getObject(S3.Url url) {
        int retries = Integer.parseInt(this.configuration.getProperty("s3.retry.get_object", "1"));

        while(retries > 0) {
            try {
                return this.s3Client.getObject(new GetObjectRequest(url.bucket, url.key));
            } catch (Exception var4) {
                this.logger.error("getObject", var4);
                --retries;
            }
        }

        return null;
    }

    public boolean doesObjectExist(S3.Url url) {
        try {
            return this.s3Client.doesObjectExist(url.bucket, url.key);
        } catch (Exception var3) {
            this.logger.error("doesObjectExist", var3);
            return false;
        }
    }

    public String getObjectContents(S3.Url url, Charset charset) {
        if (this.doesObjectExist(url)) {
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            try {
                S3Object s3object = this.getObject(url);
                Throwable var5 = null;

                try {
                    InputStream in = s3object.getObjectContent();
                    Throwable var7 = null;

                    try {
                        byte[] bytes = new byte[256];
                        int length = 0;

                        while(-1 != length) {
                            length = in.read(bytes);
                            if (length > 0) {
                                out.write(bytes, 0, length);
                            }
                        }
                    } catch (Throwable var33) {
                        var7 = var33;
                        throw var33;
                    } finally {
                        if (in != null) {
                            if (var7 != null) {
                                try {
                                    in.close();
                                } catch (Throwable var32) {
                                    var7.addSuppressed(var32);
                                }
                            } else {
                                in.close();
                            }
                        }

                    }
                } catch (Throwable var35) {
                    var5 = var35;
                    throw var35;
                } finally {
                    if (s3object != null) {
                        if (var5 != null) {
                            try {
                                s3object.close();
                            } catch (Throwable var31) {
                                var5.addSuppressed(var31);
                            }
                        } else {
                            s3object.close();
                        }
                    }

                }
            } catch (IOException var37) {
                this.logger.error("getObjectContents", var37);
            }

            return new String(out.toByteArray(), charset);
        } else {
            return null;
        }
    }

    public AmazonS3 getClient() {
        return this.s3Client;
    }

    public static class Url {
        public final String bucket;
        public final String key;

        public Url(String url) {
            if (!url.startsWith("s3://")) {
                throw new IllegalArgumentException(String.format("invalid url \"%s\"", url));
            } else {
                this.bucket = url.substring(5, url.indexOf(47, 5));
                this.key = url.substring(1 + url.indexOf(47, 5));
            }
        }

        public Url(String bucket, String key) {
            this.bucket = bucket;
            this.key = key;
        }

        public int hashCode() {
            int result = 31 + (null == this.bucket ? 0 : this.bucket.hashCode());
            return 31 * result + (null == this.key ? 0 : this.key.hashCode());
        }

        public boolean equals(Object object) {
            if (this == object) {
                return true;
            } else if (null == object) {
                return false;
            } else if (this.getClass() != object.getClass()) {
                return false;
            } else {
                S3.Url other = (S3.Url)object;
                if (null == this.bucket) {
                    if (null != other.bucket) {
                        return false;
                    }
                } else if (!this.bucket.equals(other.bucket)) {
                    return false;
                }

                if (null == this.key) {
                    if (null != other.key) {
                        return false;
                    }
                } else if (!this.key.equals(other.key)) {
                    return false;
                }

                return true;
            }
        }

        public String toString() {
            return String.format("s3://%s/%s", this.bucket, this.key);
        }
    }
}
