package com.alkemy.sophia.api.sophiamsinvoice.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity(name = "Invoice")
@Table(name = "INVOICE")
@NamedQueries({ @NamedQuery(name = "Invoice.GetAll", query = "SELECT x FROM Invoice x") })
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Invoice {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_INVOICE", nullable = false)
	private Integer idInvoice;

	@Column(name = "INVOICE_CODE")
	private String invoiceCode;
	
/*	@Column(name = "CLIENT_ID", nullable = false)
	private Integer clientId;*/

	@ManyToOne
	@JoinColumn(name = "CLIENT_ID", referencedColumnName = "ID_ANAG_CLIENT")
	private AnagClient anagClient;

	@Column(name = "CREATION_DATE", nullable = false)
	private Date creationDate;

	@Column(name = "LAST_UPDATE_DATE", nullable = false)
	private Date lastUpdateDate;

	@Column(name = "USER_ID")
	private String userID;

	@Column(name = "TOTAL_INVOICE", nullable = false)
	private BigDecimal total;

	@Column(name = "VAT")
	private Float vat;

	@Column(name = "NOTE")
	private String note;

	@Column(name = "STATUS", nullable = false)
	private String status;

	@Column(name = "PDF_PATH")
	private String pdfPath;

	@Column(name = "DATE_OF_PERTINENCE", nullable = false)
	private Date dateOfPertinence;
	
	@Column(name = "INVOICE_TYPE", nullable = false)
	private String invoiceType;
	
	@Column(name = "REQUEST_ID")
	private String requestID;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "PDF", columnDefinition = "BLOB")
	private byte[] pdf;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "invoice")
	private List<InvoiceItem> invoiceItems = new ArrayList<>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "invoice")
	private List<InvoiceAccantonamento> listInvoiceAccantonamento = new ArrayList<>();

/*
NO NO BRAVI
 */
//	public void addInvoiceItem(InvoiceItem item) {
//		item.setInvoice(this);
//		if(invoiceItems == null){
//			invoiceItems = new ArrayList();
//		}
//		invoiceItems.add(item);
//
//	}


}