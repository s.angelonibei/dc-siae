package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Produces("application/json")
@XmlRootElement 
public class InvoiceDraftDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6994024932898273798L;
	private List<InvoiceItemDto> itemList;
	private String annotation;
	private String idDsp;
	private BigDecimal total;
	private BigDecimal totalValueOrigCurrency;
	private String currency;
	private Date dateOfpertinence;
	private Integer clientId;
	
	public List<InvoiceItemDto> getItemList() {
		return itemList;
	}
	public void setItemList(List<InvoiceItemDto> itemList) {
		this.itemList = itemList;
	}
	public String getAnnotation() {
		return annotation;
	}
	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}
	public String getIdDsp() {
		return idDsp;
	}
	public void setIdDsp(String idDsp) {
		this.idDsp = idDsp;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public Integer getClientId() {
		return clientId;
	}
	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}
	public Date getDateOfpertinence() {
		return dateOfpertinence;
	}
	public void setDateOfpertinence(Date dateOfpertinence) {
		this.dateOfpertinence = dateOfpertinence;
	}
	public BigDecimal getTotalValueOrigCurrency() {
		return totalValueOrigCurrency;
	}
	public void setTotalValueOrigCurrency(BigDecimal totalValueOrigCurrency) {
		this.totalValueOrigCurrency = totalValueOrigCurrency;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	
	
}
