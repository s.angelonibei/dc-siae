package com.alkemy.sophia.api.sophiamsinvoice.repository;

import com.alkemy.sophia.api.sophiamsinvoice.entities.InvoiceItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvoiceItemRepo extends JpaRepository<InvoiceItem, Integer> {

}
