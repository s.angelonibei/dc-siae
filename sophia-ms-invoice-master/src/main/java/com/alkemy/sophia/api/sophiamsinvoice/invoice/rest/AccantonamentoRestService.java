package com.alkemy.sophia.api.sophiamsinvoice.invoice.rest;


import com.alkemy.sophia.api.sophiamsinvoice.invoice.service.AccantonamentoService;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;


@RequestMapping("accantonamenti")
@RestController
@RequiredArgsConstructor
@CommonsLog
public class AccantonamentoRestService {

	private final AccantonamentoService accantonamentoService;

	private final EntityManager entityManager;
	private final Gson gson;


}
