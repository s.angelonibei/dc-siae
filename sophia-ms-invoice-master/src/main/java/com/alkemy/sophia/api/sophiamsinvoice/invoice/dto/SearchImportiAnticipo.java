package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


@Data
@EqualsAndHashCode
@ToString
public class SearchImportiAnticipo {
	private int currentPage;
	private String dsp;
	private String advancePaymentCode;
	private Integer monthFrom;
	private Integer yearFrom;
	private Integer monthTo;
	private Integer yearTo;
	private Boolean showStandBy;

	private Integer idClient;
}
