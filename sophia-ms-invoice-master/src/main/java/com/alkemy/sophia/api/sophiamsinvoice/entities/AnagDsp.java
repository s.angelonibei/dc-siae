package com.alkemy.sophia.api.sophiamsinvoice.entities;

import javax.persistence.*;

import com.google.gson.GsonBuilder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;


@Entity(name="AnagDsp")
@Table(name="ANAG_DSP")
@NamedQueries({@NamedQuery(name="AnagDsp.GetAll", query="SELECT dsp FROM AnagDsp dsp")})
@Data
@EqualsAndHashCode
public class AnagDsp  {

	@Id
	@Column(name="IDDSP", nullable=false)
	private String idDsp;
	
	@Column(name="CODE", nullable=false)
	private String code;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="DESCRIPTION", nullable=false)
	private String description;
	
	@Column(name="FTP_SOURCE_PATH")
	private String ftpSourcePath;


	@OneToMany(fetch = FetchType.LAZY,mappedBy = "anagDsp")
	private List<ClientToDsp> clientToDspList;



	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}
}