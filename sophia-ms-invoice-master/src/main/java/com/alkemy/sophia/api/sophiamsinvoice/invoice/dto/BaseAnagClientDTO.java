package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;

import com.alkemy.sophia.api.sophiamsinvoice.entities.AnagClient;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class BaseAnagClientDTO {
    Integer idAnagClient;
    String companyName;
    String code;
    List<DspDTO> dspList;

    public BaseAnagClientDTO(AnagClient entity,boolean withDsp) {

        this.idAnagClient = entity.getIdAnagClient();
        this.code = entity.getCode();
        this.companyName = entity.getCompanyName();
        dspList = new ArrayList<>();
        if(withDsp){
            entity.getClientToDspList().stream()
                    .filter(dsp -> dsp.getEndDate() == null)
                    .forEach(dsp -> dspList.add(new DspDTO(dsp.getAnagDsp())));
        }


    }
}
