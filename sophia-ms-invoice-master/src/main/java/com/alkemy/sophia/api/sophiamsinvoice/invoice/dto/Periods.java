package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Periods {

    public static Map<Integer, List<Integer>> extractCorrespondingQuarters(int monthFrom, int yearFrom, int monthTo, int yearTo) {
        int years = yearTo - yearFrom;
        Map<Integer, List<Integer>> quartersPerYear = new HashMap<>();
        for (int i = 0; i <= years; i++) {
            int year = yearFrom + i;
            int endMonth = 12;
            int startMonth = 1;
            if (year == yearFrom && years > 0) {
                startMonth = monthFrom;
                endMonth = 12;
            } else if (yearFrom == yearTo) {
                startMonth = monthFrom;
                endMonth = monthTo;
            } else if (year == yearTo && years > 0) {
                endMonth = monthTo;
            }
            List<Integer> quarters = getQuartersInYear(startMonth, endMonth);
            if(!quarters.isEmpty()){

                quartersPerYear.put(year, quarters);
            }
        }
        return quartersPerYear;
    }

    private static List<Integer> getQuartersInYear(int monthFrom, int monthTo) {
        List<Integer> quarters = new ArrayList<>();
        for (int i = monthFrom; i <= monthTo; i++) {
            boolean isStartingQuarter = i % 3 == 1;
            int endOfQuarter = i + 2;
            boolean isQuarterContainedInMonthTo = endOfQuarter <= monthTo;
            if (isStartingQuarter && isQuarterContainedInMonthTo) {
                int quarter = endOfQuarter / 3;
                quarters.add(quarter);
            }

        }
        return quarters;
    }

}
