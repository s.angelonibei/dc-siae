package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;

import lombok.Data;

import java.util.Date;

@Data
public class ClientToDspDTO {


    private Integer id;
    private BaseAnagClientDTO anagClient;
    private DspDTO dspDto;
    private String licence;
    private Date startDate;
    private Date endDate;

}
