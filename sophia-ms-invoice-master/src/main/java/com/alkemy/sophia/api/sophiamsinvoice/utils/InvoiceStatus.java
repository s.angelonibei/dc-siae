package com.alkemy.sophia.api.sophiamsinvoice.utils;

public enum InvoiceStatus {
	
	FATTURATO("FATTURATO"),
	DA_FATTURARE("DA_FATTURARE"),
	BOZZA("BOZZA"),
	FATTURATO_ANTICIPO("FATTURATO_ANTICIPO");
	
	private String text;
	
	InvoiceStatus(String text){
		this.text = text;
	}

	public String getText() {
		return text;
	}
	
	@Override
	public String toString(){
		return text;
	}
	
	public static InvoiceStatus valueOfDescription( String description ){
		for(InvoiceStatus v : values() ){
			if( v.toString().equals( description )){
				return v;
			}
		}
		throw new IllegalArgumentException( "No enum const " + InvoiceStatus.class + "@description." + description );
	}
	
}
