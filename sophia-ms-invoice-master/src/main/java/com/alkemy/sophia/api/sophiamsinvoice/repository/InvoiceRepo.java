package com.alkemy.sophia.api.sophiamsinvoice.repository;

import com.alkemy.sophia.api.sophiamsinvoice.entities.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface InvoiceRepo extends JpaRepository<Invoice, Integer>, QuerydslPredicateExecutor<Invoice> {

}
