package com.alkemy.sophia.api.sophiamsinvoice.entities;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Data
@Entity
@Table(name = "CCID_METADATA_DETAILS" )
public class CcidMetadataDetails {

    @Id
    @Column(name = "ID")
    private Long id;
    @Column(name = "ID_DSR")
    private String idDsr;
    @Column(name = "SOCIETA")
    private String societa;
    @Column(name = "IDENTIFICATO_VALORE")
    private BigDecimal identificatoValore;
    @Column(name = "IDENTIFICATO_UTILIZZAZIONI")
    private Long identificatoUtilizzazioni;
    @Column(name = "PERCENTUALE_IDENTIFICATO_UTILIZZAZIONI")
    private BigDecimal percentualeIdentificatoUtilizzazioni;
    @Column(name = "CLAIM_VALORE")
    private BigDecimal claimValore;
    @Column(name = "CLAIM_UTILIZZAZIONI")
    private Long claimUtilizzazioni;
    @Column(name = "NON_IDENTIFICATO_VALORE")
    private BigDecimal nonIdentificatoValore;
    @Column(name = "NON_IDENTIFICATO_UTILIZZAZIONI")
    private Long nonIdentificatoUtilizzazioni;

}
