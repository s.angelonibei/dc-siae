package com.alkemy.sophia.api.sophiamsinvoice.backclaim.service;

import com.alkemy.sophia.api.sophiamsinvoice.conifg.Aws;
import com.alkemytech.sophia.commons.aws.S3;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.services.s3.transfer.*;
import com.amazonaws.util.IOUtils;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.List;

@Service
@CommonsLog
@Scope("singleton")
public class S3Service {
    private TransferManager transferManager;
    private AmazonS3 s3Client;

    @Autowired
    public S3Service(Aws awsCredentials) {
        shutdown();
        final ClientConfiguration clientConfiguration = new ClientConfiguration();
        log.debug(String.format("startup: region %s", awsCredentials.getRegion()));
        s3Client = AmazonS3ClientBuilder.standard().withCredentials(
                new AWSStaticCredentialsProvider(
                        new BasicAWSCredentials(awsCredentials.getAccessKey(), awsCredentials.getSecretKey()))
        )
                .withClientConfiguration(clientConfiguration).withRegion(awsCredentials.getRegion()).build();
        transferManager = TransferManagerBuilder.standard().withS3Client(s3Client).build();
    }

    private void shutdown() {
        if (null != transferManager) {
            transferManager.shutdownNow(true);
        }
    }

    public boolean copy(S3.Url fromUrl, S3.Url toUrl) {
        try {
            final CopyObjectRequest request = new CopyObjectRequest(fromUrl.bucket,
                    fromUrl.key, toUrl.bucket, toUrl.key);
            return null != s3Client.copyObject(request);
        } catch (Exception e) {
            return false;
        }
    }

    public InputStream download(String bucket, String key) {
        return s3Client.getObject(new GetObjectRequest(bucket, key)).getObjectContent();
    }

    public boolean download(String bucket, String key, File file) {
            try {
                file.delete();
                final Download download = transferManager
                        .download(bucket, key, file);
                download.waitForCompletion();
                if (Transfer.TransferState.Completed == download.getState()
                        && file.exists()) {
                    return true;
                }
            } catch (Exception e) {
            }

        return false;
    }

    public List<S3ObjectSummary> listObjects(String bucketName, String prefix) {
        ObjectListing listing = s3Client.listObjects(bucketName, prefix);
        List<S3ObjectSummary> summaries = listing.getObjectSummaries();

        while (listing.isTruncated()) {
            listing = s3Client.listNextBatchOfObjects(listing);
            summaries.addAll(listing.getObjectSummaries());
        }
        return summaries;
    }

    public List<S3ObjectSummary> listObjects(ListObjectsRequest listObjectsV2Request) {
        ObjectListing listing = s3Client.listObjects(listObjectsV2Request);
        List<S3ObjectSummary> summaries = listing.getObjectSummaries();

        while (listing.isTruncated()) {
            listing = s3Client.listNextBatchOfObjects(listing);
            summaries.addAll(listing.getObjectSummaries());
        }
        return summaries;
    }

    public boolean upload(final String bucket, final String key, final String content) {
//        for (int retries = Integer.parseInt(configuration.getProperty("s3.retry.put_object", "1")); retries > 0; retries--) {
        try {
            log.info(String.format("upload: upload started %s", key));
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(content.getBytes().length);
            final PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, key.replace("//", "/"), new ByteArrayInputStream(content.getBytes()), metadata);
            final Upload upload = transferManager.upload(putObjectRequest);
            upload.waitForCompletion();
            log.info(String.format("upload: upload finished %s", key));
            return true;
        } catch (Exception e) {
            log.error("upload", e);
        }
//        }
        log.error(String.format("upload: upload finished %s", key));
        return false;
    }

    boolean upload(String bucket, String key, final InputStream inputStream) {

        try {
            log.info(String.format("upload: upload started %s", key));
            ObjectMetadata metadata = new ObjectMetadata();
            byte[] bytes = IOUtils.toByteArray(inputStream);
            metadata.setContentLength(bytes.length);
            final PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, key.replace("//", "/"), new ByteArrayInputStream(bytes), metadata);
            final Upload upload = transferManager.upload(putObjectRequest);
            upload.waitForCompletion();
            log.info(String.format("upload: upload started %s", key));
            return true;
        } catch (Exception e) {
            log.error("upload", e);
        }
        return false;
    }

    void delete(String bucketName, String keyName) {
        s3Client.deleteObject(new DeleteObjectRequest(bucketName, keyName));
    }

    public boolean upload(String bucket, String key, File unidentifiedTempFile) {
        try {
            log.info(String.format("upload: upload started %s", key));
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(unidentifiedTempFile.length());
            final Upload upload = transferManager.upload(bucket, key.replace("//", "/"), unidentifiedTempFile);
            upload.waitForCompletion();
            log.info(String.format("upload: upload end %s", key));
            return true;
        } catch (Exception e) {
            log.error("upload", e);
        }
        return false;
    }
}
