package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class InvoiceSearchParameters {
	Integer first;
	Integer last;
	int currentPage;
	AnagClientDTO clientList;
	List<String> statusList;
	List<String> dspList; //to remove
	String invoiceCode;
	Date dateFrom;	
	Date dateTo;

	
}
