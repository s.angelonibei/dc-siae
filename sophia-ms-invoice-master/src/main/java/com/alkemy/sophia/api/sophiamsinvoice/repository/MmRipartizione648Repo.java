package com.alkemy.sophia.api.sophiamsinvoice.repository;

import com.alkemy.sophia.api.sophiamsinvoice.entities.MmRipartizione648;
import com.alkemy.sophia.api.sophiamsinvoice.entities.MmRipartizioneCarico;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MmRipartizione648Repo extends JpaRepository<MmRipartizione648, Long> {


}
