package com.alkemy.sophia.api.sophiamsinvoice.repository;

import com.alkemy.sophia.api.sophiamsinvoice.entities.InvoiceReclamo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReclamoRepo extends JpaRepository<InvoiceReclamo, Integer> {

}
