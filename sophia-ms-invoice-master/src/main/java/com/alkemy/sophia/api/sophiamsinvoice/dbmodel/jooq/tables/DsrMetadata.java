/*
 * This file is generated by jOOQ.
 */
package com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.tables;


import com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.DefaultSchema;
import com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.Indexes;
import com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.Keys;
import com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.tables.records.DsrMetadataRecord;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.9"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class DsrMetadata extends TableImpl<DsrMetadataRecord> {

    private static final long serialVersionUID = -1095921224;

    /**
     * The reference instance of <code>DSR_METADATA</code>
     */
    public static final DsrMetadata DSR_METADATA = new DsrMetadata();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<DsrMetadataRecord> getRecordType() {
        return DsrMetadataRecord.class;
    }

    /**
     * The column <code>DSR_METADATA.IDDSR</code>.
     */
    public final TableField<DsrMetadataRecord, String> IDDSR = createField("IDDSR", org.jooq.impl.SQLDataType.VARCHAR(512).nullable(false), this, "");

    /**
     * The column <code>DSR_METADATA.IDDSP</code>.
     */
    public final TableField<DsrMetadataRecord, String> IDDSP = createField("IDDSP", org.jooq.impl.SQLDataType.VARCHAR(100).nullable(false), this, "");

    /**
     * The column <code>DSR_METADATA.SALES_LINES_NUM</code>.
     */
    public final TableField<DsrMetadataRecord, BigDecimal> SALES_LINES_NUM = createField("SALES_LINES_NUM", org.jooq.impl.SQLDataType.DECIMAL(60, 20), this, "");

    /**
     * The column <code>DSR_METADATA.TOTAL_VALUE</code>.
     */
    public final TableField<DsrMetadataRecord, BigDecimal> TOTAL_VALUE = createField("TOTAL_VALUE", org.jooq.impl.SQLDataType.DECIMAL(60, 20), this, "");

    /**
     * The column <code>DSR_METADATA.SUBSCRIPTIONS_NUM</code>.
     */
    public final TableField<DsrMetadataRecord, Long> SUBSCRIPTIONS_NUM = createField("SUBSCRIPTIONS_NUM", org.jooq.impl.SQLDataType.BIGINT, this, "");

    /**
     * The column <code>DSR_METADATA.CREATION_TIMESTAMP</code>.
     */
    public final TableField<DsrMetadataRecord, String> CREATION_TIMESTAMP = createField("CREATION_TIMESTAMP", org.jooq.impl.SQLDataType.VARCHAR(11), this, "");

    /**
     * The column <code>DSR_METADATA.SERVICE_CODE</code>.
     */
    public final TableField<DsrMetadataRecord, Integer> SERVICE_CODE = createField("SERVICE_CODE", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>DSR_METADATA.PERIOD_TYPE</code>. "month" o "quarter"
     */
    public final TableField<DsrMetadataRecord, String> PERIOD_TYPE = createField("PERIOD_TYPE", org.jooq.impl.SQLDataType.VARCHAR(10), this, "\"month\" o \"quarter\"");

    /**
     * The column <code>DSR_METADATA.PERIOD</code>. se period_type == "month" --&gt; 1 2 3 4 5 6 7 8 9 10 11 12
se period_type == "quarter" --&gt; 1 2 3 4
     */
    public final TableField<DsrMetadataRecord, Integer> PERIOD = createField("PERIOD", org.jooq.impl.SQLDataType.INTEGER, this, "se period_type == \"month\" --> 1 2 3 4 5 6 7 8 9 10 11 12\nse period_type == \"quarter\" --> 1 2 3 4");

    /**
     * The column <code>DSR_METADATA.YEAR</code>.
     */
    public final TableField<DsrMetadataRecord, Integer> YEAR = createField("YEAR", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>DSR_METADATA.COUNTRY</code>.
     */
    public final TableField<DsrMetadataRecord, String> COUNTRY = createField("COUNTRY", org.jooq.impl.SQLDataType.VARCHAR(3), this, "");

    /**
     * The column <code>DSR_METADATA.CURRENCY</code>.
     */
    public final TableField<DsrMetadataRecord, String> CURRENCY = createField("CURRENCY", org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * The column <code>DSR_METADATA.BACKCLAIM</code>. was backclaim requested for this dsr?
     */
    public final TableField<DsrMetadataRecord, Long> BACKCLAIM = createField("BACKCLAIM", org.jooq.impl.SQLDataType.BIGINT.nullable(false).defaultValue(org.jooq.impl.DSL.inline("0", org.jooq.impl.SQLDataType.BIGINT)), this, "was backclaim requested for this dsr?");

    /**
     * The column <code>DSR_METADATA.ID_BACKCLAIM_IN_PROGRESS</code>. 0 -&gt; not in progress, 1 -&gt; in progress, 2 -&gt; waiting for approval
     */
    public final TableField<DsrMetadataRecord, Short> ID_BACKCLAIM_IN_PROGRESS = createField("ID_BACKCLAIM_IN_PROGRESS", org.jooq.impl.SQLDataType.SMALLINT.nullable(false).defaultValue(org.jooq.impl.DSL.inline("0", org.jooq.impl.SQLDataType.SMALLINT)), this, "0 -> not in progress, 1 -> in progress, 2 -> waiting for approval");

    /**
     * The column <code>DSR_METADATA.EXTENDED_DSR_URL</code>.
     */
    public final TableField<DsrMetadataRecord, String> EXTENDED_DSR_URL = createField("EXTENDED_DSR_URL", org.jooq.impl.SQLDataType.VARCHAR(1024), this, "");

    /**
     * The column <code>DSR_METADATA.BC_TYPE</code>.
     */
    public final TableField<DsrMetadataRecord, String> BC_TYPE = createField("BC_TYPE", org.jooq.impl.SQLDataType.VARCHAR(1), this, "");

    /**
     * Create a <code>DSR_METADATA</code> table reference
     */
    public DsrMetadata() {
        this(DSL.name("DSR_METADATA"), null);
    }

    /**
     * Create an aliased <code>DSR_METADATA</code> table reference
     */
    public DsrMetadata(String alias) {
        this(DSL.name(alias), DSR_METADATA);
    }

    /**
     * Create an aliased <code>DSR_METADATA</code> table reference
     */
    public DsrMetadata(Name alias) {
        this(alias, DSR_METADATA);
    }

    private DsrMetadata(Name alias, Table<DsrMetadataRecord> aliased) {
        this(alias, aliased, null);
    }

    private DsrMetadata(Name alias, Table<DsrMetadataRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> DsrMetadata(Table<O> child, ForeignKey<O, DsrMetadataRecord> key) {
        super(child, key, DSR_METADATA);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return DefaultSchema.DEFAULT_SCHEMA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.DSR_METADATA_DSR_METADATA_BACKCLAIM_IDX, Indexes.DSR_METADATA_DSR_METADATA_BACKCLAIM_IN_PROGRESS_IDX, Indexes.DSR_METADATA_FK_DSR_METADATA_ANAG_COUNTRY_IDX, Indexes.DSR_METADATA_FK_DSR_METADATA_COMMERCIAL_OFFER_IDX, Indexes.DSR_METADATA_IX_DSR_METADATA_8055281B0DD3DFFB, Indexes.DSR_METADATA_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<DsrMetadataRecord> getPrimaryKey() {
        return Keys.KEY_DSR_METADATA_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<DsrMetadataRecord>> getKeys() {
        return Arrays.<UniqueKey<DsrMetadataRecord>>asList(Keys.KEY_DSR_METADATA_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<DsrMetadataRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<DsrMetadataRecord, ?>>asList(Keys.FK_DSR_METADATA_COMMERCIAL_OFFER, Keys.DSR_METADATA_FK);
    }

    public CommercialOffers commercialOffers() {
        return new CommercialOffers(this, Keys.FK_DSR_METADATA_COMMERCIAL_OFFER);
    }

    public BackclaimInProgress backclaimInProgress() {
        return new BackclaimInProgress(this, Keys.DSR_METADATA_FK);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DsrMetadata as(String alias) {
        return new DsrMetadata(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DsrMetadata as(Name alias) {
        return new DsrMetadata(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public DsrMetadata rename(String name) {
        return new DsrMetadata(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public DsrMetadata rename(Name name) {
        return new DsrMetadata(name, null);
    }
}
