package com.alkemy.sophia.api.sophiamsinvoice.repository;

import com.alkemy.sophia.api.sophiamsinvoice.entities.CCIDMetadata;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CCIDMetadataRepo extends JpaRepository<CCIDMetadata, Long> {
    List<CCIDMetadata> findAllByIdCCIDMetadataIn(List<Long> idList);
    CCIDMetadata findOneByIdDsr(String idDsr);
}
