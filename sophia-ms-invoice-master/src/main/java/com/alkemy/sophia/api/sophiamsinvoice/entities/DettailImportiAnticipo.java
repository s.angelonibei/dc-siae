package com.alkemy.sophia.api.sophiamsinvoice.entities;

import lombok.Data;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

//@XmlRootElement
//@Entity(name="DettailImportiAnticipo")
@Data
public class DettailImportiAnticipo {

//	@Id
	private Integer idDettailImportiAnticipo;
	private Integer idImportoAnticipo;
	private Date periodoPertinenzaInizio;
	private Date periodoPertinenzaFine;
	private String numeroFattura;
	private BigDecimal importoOriginale;
	private BigDecimal importoUtilizzabile;
//	private String idDsp;
	private Integer idJoinAnticipiCcid;
	private String idCcid;
	private BigDecimal quotaAnticipo;
    private BigInteger idCCIDMetadata;
    private String idDsr;
    private BigDecimal totalValue;
    private String currency;
    private String ccidVersion;
    private Boolean ccidEncoded;
    private Boolean ccidEncodedProvisional;
    private Boolean ccidNotEncoded;
    private Integer invoiceItem;
    private String invoiceStatus;
    private BigDecimal valoreFatturabile;
	
    public DettailImportiAnticipo(){}
	
	public DettailImportiAnticipo(Integer idImportoAnticipo,
                                  Date periodoPertinenzaInizio, Date periodoPertinenzaFine, String numeroFattura, BigDecimal importoOriginale,
                                  BigDecimal importoUtilizzabile,
//								  String idDsp,
								  BigDecimal quotaAnticipo, String idCcid,
                                  String idDsr, BigDecimal valoreFatturabile, BigDecimal totalValue
			) {
		
		this.idImportoAnticipo=idImportoAnticipo;
		this.periodoPertinenzaInizio=periodoPertinenzaInizio;
		this.periodoPertinenzaFine=periodoPertinenzaFine;
		this.numeroFattura=numeroFattura;
		this.importoOriginale=importoOriginale;
		this.importoUtilizzabile=importoUtilizzabile;
//		this.idDsp=idDsp;
		this.quotaAnticipo=quotaAnticipo;
		this.idCcid=idCcid;
		this.idDsr=idDsr;
		this.valoreFatturabile=valoreFatturabile;
		this.totalValue=totalValue;
		
	}	
    


}
