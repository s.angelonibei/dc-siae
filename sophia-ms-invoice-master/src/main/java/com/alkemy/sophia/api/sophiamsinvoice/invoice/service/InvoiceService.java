package com.alkemy.sophia.api.sophiamsinvoice.invoice.service;

import com.alkemy.sophia.api.sophiamsinvoice.entities.*;
import com.alkemy.sophia.api.sophiamsinvoice.invoice.dto.*;
import com.alkemy.sophia.api.sophiamsinvoice.repository.AnagClientRepo;
import com.alkemy.sophia.api.sophiamsinvoice.repository.InvoiceItemRepo;
import com.alkemy.sophia.api.sophiamsinvoice.repository.InvoiceItemToCcidRepo;
import com.alkemy.sophia.api.sophiamsinvoice.repository.InvoiceRepo;
import com.alkemy.sophia.api.sophiamsinvoice.utils.BackClaimType;
import com.alkemytech.sophia.invoice.integration.model.*;
import com.alkemytech.sophia.invoice.integration.service.InvoiceServiceClientInterface;
import com.google.common.primitives.Bytes;
import com.querydsl.core.BooleanBuilder;
import groovy.json.StringEscapeUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.querydsl.QPageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.Tables.*;
import static com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.tables.AnagClient.ANAG_CLIENT;
import static com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.tables.AnagDsp.ANAG_DSP;
import static com.alkemy.sophia.api.sophiamsinvoice.entities.QInvoice.invoice;
import static org.jooq.impl.DSL.*;

@Service
@RequiredArgsConstructor
@CommonsLog
public class InvoiceService {

    private final DSLContext dsl;
    private final EntityManager entityManager;
    private final InvoiceTracer invoceTracer;
    private final InvoiceServiceClientInterface sapPiInvoiceServiceClientImpl;
    private final InvoiceConfiguration invoiceConfiguration;
    private final AnagClientRepo anagClientRepo;
    private final InvoiceItemToCcidRepo invoiceItemToCcidRepo;
    private final InvoiceItemRepo invoiceItemRepo;
    @Value("${invoice.max-rows}")
    private int maxRows;

    private final InvoiceRepo invoiceRepo;

    @Transactional
    public InvoiceDTO newInvoice(InvoiceDraftDto dto) {


        List<Object[]> result;
        List<InvoiceCCIDDTO> dtoList = new ArrayList<>();
        InvoiceDTO invoiceDto = null;
        try {

            AnagClient anagClient = (AnagClient) entityManager
                    .createQuery("SELECT x FROM AnagClient x WHERE x.id =:id").setParameter("id", dto.getClientId())
                    .getSingleResult();
//			entityManager.getTransaction().begin();
            Invoice invoice = new Invoice();
            Date date = new Date();
            invoice.setCreationDate(date);
            invoice.setAnagClient(anagClient);
            invoice.setDateOfPertinence(dto.getDateOfpertinence());
            invoice.setLastUpdateDate(date);
//			invoice.setIdDsp(dto.getIdDsp());
            invoice.setNote(dto.getAnnotation());
            invoice.setTotal(dto.getTotal());
            invoice.setStatus("BOZZA");
            invoice.setInvoiceType("AUTOMATICA");
            int position = 1;

            for (InvoiceItemDto itemDao : dto.getItemList()) {
                InvoiceItem itemEntity = new InvoiceItem();
                InvoiceItemReason description = (InvoiceItemReason) entityManager.createQuery("SELECT x FROM InvoiceItemReason x WHERE x.idItemInvoiceReason =:id").setParameter("id", itemDao.getIdDescription()).getSingleResult();
                itemEntity.setDescription(description);

                itemEntity.setNote(itemDao.getNote());
                itemEntity.setInvoicePosition(position);

                itemEntity.setTotal(itemDao.getTotalValue());
                itemEntity.setTotalOrigCurrency(itemDao.getTotalValueOrig());
                itemEntity.setOrigCurrency(itemDao.getCurrency());
                itemEntity.setInvoice(invoice);
                invoice.getInvoiceItems().add(itemEntity);
                entityManager.persist(invoice);
                entityManager.persist(itemEntity);
                entityManager.flush();

                List<CCIDMetadata> ccids = entityManager.createQuery("SELECT x FROM CCIDMetadata x WHERE x.idCcid IN :idCcids").setParameter("idCcids", itemDao.getCcidList()).getResultList();
                for (CCIDMetadata ccidMedata : ccids) {
                    InvoiceItemToCcid invoiceItemToCcid = new InvoiceItemToCcid();
                    invoiceItemToCcid.setCcidMedata(ccidMedata);
                    invoiceItemToCcid.setInvoiceItem(itemEntity);
                    invoiceItemToCcid.setValore(ccidMedata.getValoreResiduo());
                    entityManager.persist(invoiceItemToCcid);
                    ccidMedata.setQuotaFattura(new BigDecimal("0"));
                    ccidMedata.setValoreResiduo(new BigDecimal(0));
                    entityManager.merge(ccidMedata);
                    entityManager.flush();
                }
                position++;

            }


//			entityManager.getTransaction().commit();

            invoiceDto = new InvoiceDTO();
            invoiceDto.setIdInvoice(invoice.getIdInvoice());

        } catch (Exception e) {
            e.printStackTrace();
            log.error("create invoice", e);
//			if (entityManager.getTransaction().isActive()) {
//				entityManager.getTransaction().rollback();
//			}
        }

        return invoiceDto;
    }


    public List<InvoiceDTO> searchInvoice(InvoiceSearchParameters searchParameters) {

        List<InvoiceDTO> resultSet = new ArrayList<>();
        boolean checkClient = searchParameters.getClientList() != null && searchParameters.getClientList().getIdAnagClient() != null;
        boolean checkDsp = searchParameters.getDspList() != null && !searchParameters.getDspList().isEmpty();
        boolean checkStatus = searchParameters.getStatusList() != null && !searchParameters.getStatusList().isEmpty();
        boolean checkInvoiceCode = searchParameters.getInvoiceCode() != null && !searchParameters.getInvoiceCode().isEmpty();
        boolean checkDateFrom = searchParameters.getDateFrom() != null;
        boolean checkDateTo = searchParameters.getDateTo() != null;

        try {
            BooleanBuilder predicate = new BooleanBuilder();
            if (checkClient)
                predicate.and(invoice.anagClient.idAnagClient.eq(searchParameters.getClientList().getIdAnagClient()));
            if (checkDsp) {
                List<Integer> invoiceItemIds = dsl.selectDistinct(
                        INVOICE_ITEM.ID_ITEM/*,
                        INVOICE_ITEM.ID_INVOICE,
                        INVOICE_ITEM.DESCRIPTION,
                        INVOICE_ITEM.INVOICE_POSITION,
                        INVOICE_ITEM.TOTAL,
                        INVOICE_ITEM.NOTE,
                        INVOICE_ITEM.TOTAL_ORIG_CURRENCY,
                        INVOICE_ITEM.ORIG_CURRENCY*/
                )
                        .from(INVOICE_ITEM)
                        .join(ITEM_INVOICE_REASON).on(INVOICE_ITEM.DESCRIPTION.eq(ITEM_INVOICE_REASON.ID_ITEM_INVOICE_REASON))
                        .join(INVOICE_ITEM_TO_CCID).on(INVOICE_ITEM.ID_ITEM.eq(INVOICE_ITEM_TO_CCID.ID_INVOICE_ITEM))
                        .join(CCID_METADATA).on(INVOICE_ITEM_TO_CCID.ID_CCID_METADATA.eq(CCID_METADATA.ID_CCID_METADATA))
                        .join(DSR_METADATA).on(CCID_METADATA.ID_DSR.eq(DSR_METADATA.IDDSR))
                        .join(ANAG_DSP).on(ANAG_DSP.NAME.eq(DSR_METADATA.IDDSP))
                        .where(ANAG_DSP.IDDSP.in(searchParameters.getDspList()))
                        .fetch().into(Integer.class);

                predicate.and(invoice.invoiceItems.any().idItem.in(invoiceItemIds));
            }
            if (checkStatus)
                predicate.and(invoice.status.in(searchParameters.getStatusList()));
            if (checkInvoiceCode)
                predicate.and(invoice.invoiceCode.in(searchParameters.getInvoiceCode()));
            if (checkDateFrom)
                predicate.and(invoice.creationDate.goe(searchParameters.getDateFrom()));
            if (checkDateTo)
                predicate.and(invoice.creationDate.loe(searchParameters.getDateTo()));
            predicate.and(invoice.invoiceType.ne("ANTICIPO"));

            int size = searchParameters.getLast() - searchParameters.getFirst();
            int page = searchParameters.getLast() / size - 1;
            Page<Invoice> invoices = invoiceRepo.findAll(predicate, new QPageRequest(page, size));


            for (Invoice invoice : invoices.getContent()) {
                resultSet.add(new InvoiceDTO(invoice, invoice.getAnagClient()));
            }


        } catch (Exception e) {
            log.error("search invoice", e);
            return new ArrayList<>();
        }
        return resultSet;
    }


    public InvoiceDTO confirmInvoice(InvoiceDTO dto) {
        return null;
    }


    @Transactional
    public void removeInvoice(Integer idInvoice, boolean withCCIDModification) {
        try {
            Invoice invoice = entityManager.find(Invoice.class, idInvoice);

            for (InvoiceItem invoiceItem : invoice.getInvoiceItems()) {

                for (InvoiceItemToCcid invoiceItemToCcid : invoiceItem.getInvoiceItemToCcid()) {

                    if (withCCIDModification) {
                        CCIDMetadata metadata = invoiceItemToCcid.getCcidMedata();
                        metadata.setInvoiceStatus("DA_FATTURARE");
                        metadata.setInvoiceItem(null);
                        metadata.setValoreResiduo(invoiceItemToCcid.getValore());
                        metadata.setQuotaFattura(new BigDecimal("0"));
                        entityManager.merge(metadata);
                        entityManager.flush();
                    }


                    entityManager.remove(invoiceItemToCcid);
                    entityManager.flush();

                }
                entityManager.refresh(invoiceItem);
                entityManager.remove(invoiceItem);
                entityManager.flush();


            }

            entityManager.createQuery("delete from InvoiceLog x where x.idInvoice = :idInvoice").setParameter("idInvoice", idInvoice).executeUpdate();
            entityManager.flush();

            entityManager.refresh(invoice);
            entityManager.remove(invoice);
            entityManager.flush();

        } catch (Exception e) {
            log.error("delete", e);
            throw (e);
        }

    }


    @Transactional
    public void removeInvoiceBozza(Integer idInvoice) {

        try {
            Invoice invoice = entityManager.find(Invoice.class, idInvoice);

            for (InvoiceItem invoiceItem : invoice.getInvoiceItems()) {

                for (InvoiceItemToCcid invoiceItemToCcid : invoiceItem.getInvoiceItemToCcid()) {
                    CCIDMetadata metadata = invoiceItemToCcid.getCcidMedata();
                    metadata.setInvoiceStatus("DA_FATTURARE");
                    metadata.setInvoiceItem(null);
                    metadata.setValoreResiduo(invoiceItemToCcid.getValore());
                    metadata.setQuotaFattura(new BigDecimal("0"));
                    entityManager.merge(metadata);
                    entityManager.flush();


                    entityManager.remove(invoiceItemToCcid);
                    entityManager.flush();

                }
                entityManager.refresh(invoiceItem);
                entityManager.remove(invoiceItem);
                entityManager.flush();


            }
            entityManager.createQuery("delete from InvoiceLog x where x.idInvoice = :idInvoice").setParameter("idInvoice", idInvoice).executeUpdate();
            entityManager.flush();

            entityManager.refresh(invoice);
            entityManager.remove(invoice);
            entityManager.flush();

        } catch (Exception e) {
            log.error("delete", e);
            throw (e);
        }

    }

//
//    public InvoiceDTO updateInvoice(InvoiceDTO dto) {
//        return null;
//    }


    private String getPeriodRepresentation(String periodType, Integer period) {
        if (periodType.equals("quarter")) {
            return "Q" + period.toString();
        }
        return String.format("%02d", period);
    }

    public List<InvoiceItemReasonDTO> getInvoiceItemReasons() {

        List<InvoiceItemReason> result;
        List<InvoiceItemReasonDTO> dtoList = new ArrayList<>();
        try {
            final Query q = entityManager.createQuery("select x from InvoiceItemReason x");
            result = (List<InvoiceItemReason>) q.getResultList();
            if (result != null && !result.isEmpty()) {
                for (InvoiceItemReason e : result) {
                    dtoList.add(new InvoiceItemReasonDTO(e));
                }
            }

        } catch (Exception e) {
            log.error("list invoice item reason", e);
        }
        return dtoList;
    }


    @Transactional
    public InvoiceResponse callInvoiceService(int invoiceId) {


        List<InvoiceItemReason> resultList;
        InvoiceResponse invoiceResponse = null;

        try {

            String queryString = "select x from InvoiceItemReason x ";
            Query query = entityManager.createQuery(queryString);

            resultList = (List<InvoiceItemReason>) query.getResultList();

            String sqlStringUniqueCodeUpdate = "UPDATE INVOICE_REQUEST_ID SET REQUEST_ID = LAST_INSERT_ID(REQUEST_ID + 1)";
            String sqlStringGetUniqueCode = "SELECT LAST_INSERT_ID()";

            BigInteger requestId;
//			entityManager.getTransaction().begin();
            Query queryUpdate = entityManager.createNativeQuery(sqlStringUniqueCodeUpdate);
            queryUpdate.executeUpdate();
            Query queryGetUniqueCode = entityManager.createNativeQuery(sqlStringGetUniqueCode);
            requestId = (BigInteger) queryGetUniqueCode.getSingleResult();
//			entityManager.getTransaction().commit();

            Map<Integer, InvoiceItemReason> invoiceReasonMap = new HashMap<>();
            for (InvoiceItemReason reason : resultList) {
                invoiceReasonMap.put(reason.getIdItemInvoiceReason(), reason);
            }

            Invoice invoice = entityManager.find(Invoice.class, invoiceId);

            List<InvoiceItem> items = invoice.getInvoiceItems();

            AnagClient anagClient = entityManager.find(AnagClient.class, invoice.getAnagClient().getIdAnagClient());

            if (resultList != null) {

                SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
                Date date = new Date();
                DatiFattura datiFattura = new DatiFattura();
                datiFattura.setIdRichiesta(requestId.toString());
                datiFattura.setCodiceCliente(anagClient.getCode());

                datiFattura.setDataCompetenza(DATE_FORMAT.format(invoice.getDateOfPertinence()));
                datiFattura.setDataDocumento(DATE_FORMAT.format(date));
                datiFattura.setDataAccredito(DATE_FORMAT.format(date));
                datiFattura.setDataPagamento(DATE_FORMAT.format(date));
                datiFattura.setDivisione("DGEN");
                datiFattura.setTipoDocumento("3"); // Il flusso di fatturazione sarà previsto solo per le fatturazione anticipata e fatturazione
                //con acconto per soggetti estero (va gestito come input o dipende unicamente dal cliente estero?)
                //datiFattura.setAttribuzione("MULCL n. ord. d\'acquisto");
                datiFattura.setIncoterms2("C001");
//		        datiFattura.setTestoTestata(invoice.getNote());

                List<Posizione> positions = new ArrayList<>();
                for (InvoiceItem item : items) {

                    Posizione position = new Posizione();
                    position.setCodiceMateriale(item.getDescription().getCode());
                    position.setCodiceIVASAP(anagClient.getVatCode());


                    position.setNumeroPosizione(item.getInvoicePosition().toString());
                    position.setQuantita("1");
                    position.setPrezzoUnitario(item.getTotal().toString());
                    position.setDescrizioneMateriale(item.getDescription().getDescription());
                    positions.add(position);

                }

                datiFattura.setPosizioni(positions);


                InvoiceOrder order = new InvoiceOrder();
                DatiStampa datiStampa = new DatiStampa();

                datiStampa.setAnnotazioni(StringEscapeUtils.escapeJava(invoice.getNote()));

                order.setDatiFattura(datiFattura);
                order.setDatiStampa(datiStampa);


                invoiceResponse = sapPiInvoiceServiceClientImpl.sendInvoiceOrder(order, requestId.toString(),
                        invoiceId);


                if (invoiceResponse != null) {

                    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//					entityManager.getTransaction().begin();
                    invoice.setStatus("RICHIESTA_FATTURA");
                    invoice.setPdfPath(invoiceResponse.getPathFilePdf());
                    invoice.setInvoiceCode(invoiceResponse.getNumeroDocumento());
                    invoice.setLastUpdateDate(new Date());


                    for (InvoiceItem i : invoice.getInvoiceItems()) {

                        // create update
                        CriteriaUpdate<CCIDMetadata> update = cb.createCriteriaUpdate(CCIDMetadata.class);

                        // set the root class
                        Root e = update.from(CCIDMetadata.class);

                        // set update and where clause
                        update.set("invoiceStatus", "RICHIESTA_FATTURA");
                        Expression<String> idItem = e.get("invoiceItem");
                        update.where(cb.equal(idItem, i.getIdItem()));
                        entityManager.createQuery(update).executeUpdate();


                    }
//					entityManager.getTransaction().commit();
                }

            }

        } catch (Exception e) {
            log.error("call invoice service", e);
        }

        return invoiceResponse;

    }


    public List<InvoiceItemDto> getInvoiceItems(Integer invoiceId) {

        List<Object[]> result;
        List<InvoiceItemDto> dtoList = new ArrayList<>();
        try {

            String sqlString = "SELECT \n" +
                    "INVOICE_ITEM.ID_ITEM, \n" +
                    "ITEM_INVOICE_REASON.DESCRIPTION, \n" +
                    "INVOICE_ITEM.TOTAL, \n" +
                    "INVOICE_ITEM_TO_CCID.VALORE,\n" +
                    "INVOICE_ITEM.TOTAL_ORIG_CURRENCY, \n" +
                    "INVOICE_ITEM.ORIG_CURRENCY, \n" +
                    "DSR_METADATA.PERIOD, \n" +
                    "DSR_METADATA.PERIOD_TYPE, \n" +
                    "DSR_METADATA.YEAR, \n" +
                    "ANAG_DSP.NAME, \n" +
                    "(CASE   WHEN DSR_METADATA.PERIOD_TYPE = 'month' THEN concat(ELT(DSR_METADATA.PERIOD, 'Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov','Dic') , ' ', DSR_METADATA.YEAR) \n" +
                    "ELSE concat(ELT(DSR_METADATA.PERIOD, 'Gen - Mar', 'Apr - Giu', 'Lug - Set', 'Ott - Dic') , ' ', DSR_METADATA.YEAR) END) AS PERIOD_STRING, \n" +
                    "ANAG_COUNTRY.NAME as COUNTRY_NAME, \n" +
                    "ANAG_UTILIZATION_TYPE.NAME as UTILIZATION_NAME, \n" +
                    "COMMERCIAL_OFFERS.OFFERING, \n" +
                    "CCID_METADATA.VALORE_RESIDUO , \n" +
                    "CCID_METADATA.CURRENCY\n" +
                    "FROM INVOICE_ITEM_TO_CCID\n" +
                    "\n" +
                    "LEFT JOIN INVOICE_ITEM\n" +
                    "ON INVOICE_ITEM_TO_CCID.ID_INVOICE_ITEM = INVOICE_ITEM.ID_ITEM\n" +
                    "\n" +
                    "LEFT JOIN ITEM_INVOICE_REASON\n" +
                    "ON INVOICE_ITEM.DESCRIPTION = ITEM_INVOICE_REASON.ID_ITEM_INVOICE_REASON\n" +
                    "\n" +
                    "LEFT JOIN CCID_METADATA\n" +
                    "ON INVOICE_ITEM_TO_CCID.ID_CCID_METADATA = CCID_METADATA.ID_CCID_METADATA\n" +
                    "\n" +
                    "LEFT JOIN DSR_METADATA\n" +
                    "ON CCID_METADATA.ID_DSR = DSR_METADATA.IDDSR\n" +
                    "\n" +
                    "LEFT JOIN COMMERCIAL_OFFERS\n" +
                    "ON DSR_METADATA.SERVICE_CODE = COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS\n" +
                    "\n" +
                    "LEFT JOIN ANAG_DSP\n" +
                    "ON DSR_METADATA.IDDSP = ANAG_DSP.IDDSP\n" +
                    "\n" +
                    "LEFT JOIN ANAG_COUNTRY\n" +
                    "ON DSR_METADATA.COUNTRY = ANAG_COUNTRY.ID_COUNTRY\n" +
                    "\n" +
                    "LEFT JOIN ANAG_UTILIZATION_TYPE\n" +
                    "ON COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE = ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE\n" +
                    "\n" +
                    "WHERE INVOICE_ITEM.ID_INVOICE = " + invoiceId.toString();

            final Query q = entityManager.createNativeQuery(sqlString);
            result = (List<Object[]>) q.getResultList();
            if (result != null && !result.isEmpty()) {
                Map<Integer, InvoiceItemDto> itemMap = new HashMap<>();
                for (Object[] element : result) {
                    InvoiceItemDetailDTO itemDetail = new InvoiceItemDetailDTO(element);
                    Integer itemId = (Integer) element[0];
                    Integer period = (Integer) element[6];
                    String periodType = (String) element[7];
                    Integer year = (Integer) element[8];
                    Map<String, BigDecimal> currencyRateMap = new HashMap<>();

                    if (itemDetail.getCurrency().equals("EUR")) {
                        itemDetail.setTotalValueOrigCurrency(itemDetail.getTotalValue());
                    } else {
                        BigDecimal originalValue = itemDetail.getTotalValue();

                        Map<String, String> currencyKeys = getCurrencyKeyMap(itemDetail.getCurrency(), periodType,
                                period, year);
                        updateCurrencyMap(currencyRateMap, currencyKeys, entityManager);

                        BigDecimal eurValue = originalValue.multiply(currencyRateMap.get(currencyKeys.get("key")));
                        itemDetail.setTotalValue(eurValue);
                        itemDetail.setTotalValueOrigCurrency(originalValue);
                    }

                    if (!itemMap.containsKey(itemId)) {
                        InvoiceItemDto invoiceItem = new InvoiceItemDto();
                        invoiceItem.setDescriptionText((String) element[1]);
                        invoiceItem.setTotalValue((BigDecimal) element[14]);
                        invoiceItem.setTotalValueOrig((BigDecimal) element[3]);
                        invoiceItem.setCurrency((String) element[5]);
                        List<InvoiceItemDetailDTO> invoiceDetailList = new ArrayList<>();
                        invoiceDetailList.add(itemDetail);
                        invoiceItem.setInvoiceDetailList(invoiceDetailList);
                        itemMap.put(itemId, invoiceItem);
                        dtoList.add(invoiceItem);
                    } else {
                        itemMap.get(itemId).getInvoiceDetailList().add(itemDetail);
                        itemMap.get(itemId).setTotalValue(itemMap.get(itemId).getTotalValue().add((BigDecimal) element[14]));
                        itemMap.get(itemId).setTotalValueOrig(itemMap.get(itemId).getTotalValueOrig().add((BigDecimal) element[3]));
                    }

                }
            }

        } catch (Exception e) {
            log.error("list invoice item", e);
        }
        return dtoList;
    }

    private void updateCurrencyMap(Map<String, BigDecimal> currencyRateMap, Map<String, String> currencyKeys,
                                   EntityManager em) {
        try {

            BigDecimal rate;

            if (currencyRateMap.containsKey(currencyKeys.get("key"))) {

                rate = currencyRateMap.get(currencyKeys.get("key"));
            } else {

                final CurrencyRate currencyRate = em
                        .createQuery(new StringBuffer().append("select x from CurrencyRate x")
                                .append(" where x.year = :year").append(" and x.month = :month")
                                .append(" and x.srcCurrency = :srcCurrency").append(" and x.dstCurrency = :dstCurrency")
                                .toString(), CurrencyRate.class)
                        .setParameter("year", currencyKeys.get("yearString"))
                        .setParameter("month", currencyKeys.get("month"))
                        .setParameter("srcCurrency", currencyKeys.get("srcCurrency"))
                        .setParameter("dstCurrency", currencyKeys.get("dstCurrency")).getSingleResult();

                if (null != currencyRate) {
                    rate = currencyRate.getRate();

                } else {
                    rate = new BigDecimal(0);
                }
                currencyRateMap.put(currencyKeys.get("key"), currencyRate.getRate());
            }
        } catch (Exception e) {
            log.error("invoice currency map", e);
        }
    }

    private Map<String, String> getCurrencyKeyMap(String currency, String periodType, Integer period, Integer year) {

        final Calendar now = GregorianCalendar.getInstance();
        String month = periodType != null && period != null ? getPeriodRepresentation(periodType, period)
                : String.format("%02d", 1 + now.get(Calendar.MONTH));
        String yearString = year != null ? year.toString() : String.format("%04d", now.get(Calendar.YEAR));
        String srcCurrency = currency;
        String dstCurrency = "EUR";
        String key = yearString + month + srcCurrency + dstCurrency;
        HashMap<String, String> values = new HashMap<>();
        values.put("month", month);
        values.put("yearString", yearString);
        values.put("srcCurrency", srcCurrency);
        values.put("dstCurrency", dstCurrency);
        values.put("key", key);
        return values;
    }


    public AnagClientDTO getAnagClient(String idClient) {

        AnagClientDTO result = null;
        try {
            AnagClient anagClient = anagClientRepo.findById(Integer.parseInt(idClient)).get();
            result = new AnagClientDTO(anagClient, true);

        } catch (Exception e) {
            log.error("client data", e);
        }

        return result;
    }


    public InvoiceDTO getInvoice(Integer invoiceId) {

        InvoiceDTO result = new InvoiceDTO();
        try {

            Invoice invoice = entityManager.find(Invoice.class, invoiceId);
//			AnagDsp anagDsp = (AnagDsp) entityManager.find(AnagDsp.class, invoice.getIdDsp());
            AnagClient anagClient = entityManager.find(AnagClient.class, invoice.getAnagClient().getIdAnagClient());

            String queryString = "select x from InvoiceItemReason x ";
            Query query = entityManager.createQuery(queryString);

            List<InvoiceItemReason> resultList = (List<InvoiceItemReason>) query.getResultList();

            Map<Integer, InvoiceItemReason> invoiceReasonMap = new HashMap<>();
            for (InvoiceItemReason reason : resultList) {
                invoiceReasonMap.put(reason.getIdItemInvoiceReason(), reason);
            }

            if (invoice != null && anagClient != null && !invoiceReasonMap.isEmpty()) {
                result = new InvoiceDTO(invoice, anagClient);

            }

        } catch (Exception e) {
            log.error("client data", e);
        }

        return result;
    }

//	@Transactional(propagation = Propagation.REQUIRES_NEW)
//	public void updateInvoiceRequestId(){
//
//
//
//		Query queryUpdate = entityManager.createNativeQuery(sqlStringUniqueCodeUpdate);
//		queryUpdate.executeUpdate();
//		Query queryGetUniqueCode = entityManager.createNativeQuery(sqlStringGetUniqueCode);
//		requestId = (BigInteger) queryGetUniqueCode.getSingleResult();
//	}



	/*
	TODO: da modificare splittando il metodo in due con REQUIRE_NEW
	 */


    @Transactional
    public InvoiceDTO checkInvoice(Integer invoiceId) {

        InvoiceDTO result = new InvoiceDTO();
        try {

            Invoice invoice = entityManager.find(Invoice.class, invoiceId);

            String sqlStringUniqueCodeUpdate = "UPDATE INVOICE_REQUEST_ID SET REQUEST_ID = LAST_INSERT_ID(REQUEST_ID + 1)";
            String sqlStringGetUniqueCode = "SELECT LAST_INSERT_ID()";
            BigInteger requestId;

//			entityManager.getTransaction().begin();
            Query queryUpdate = entityManager.createNativeQuery(sqlStringUniqueCodeUpdate);
            queryUpdate.executeUpdate();
            Query queryGetUniqueCode = entityManager.createNativeQuery(sqlStringGetUniqueCode);
            requestId = (BigInteger) queryGetUniqueCode.getSingleResult();
//			entityManager.getTransaction().commit();

            String pdfPath = invoice.getPdfPath();
            String pdf = pdfPath != null ? pdfPath.substring(pdfPath.lastIndexOf("\\") + 1) : null;
            List<Byte> pdfByteString = sapPiInvoiceServiceClientImpl.getInvoice(pdf,
                    requestId.toString(), invoiceId);
            if (pdfByteString != null && !pdfByteString.isEmpty()) {

                CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//				entityManager.getTransaction().begin();
                invoice.setStatus("FATTURATO");
                invoice.setPdf(Bytes.toArray(pdfByteString));
                invoice.setLastUpdateDate(new Date());
                for (InvoiceItem i : invoice.getInvoiceItems()) {

                    // create update
                    CriteriaUpdate<CCIDMetadata> update = cb.createCriteriaUpdate(CCIDMetadata.class);

                    // set the root class
                    Root e = update.from(CCIDMetadata.class);

                    // set update and where clause
                    update.set("invoiceStatus", "FATTURATO");
                    Expression<String> itemId = e.get("invoiceItem");
                    update.where(cb.equal(itemId, i.getIdItem().toString()));
                    entityManager.createQuery(update).executeUpdate();


                }
//				entityManager.getTransaction().commit();
            }

//			AnagDsp anagDsp = (AnagDsp) entityManager.find(AnagDsp.class, invoice.getIdDsp());
            AnagClient anagClient = entityManager.find(AnagClient.class, invoice.getAnagClient().getIdAnagClient());

            String queryString = "select x from InvoiceItemReason x ";
            Query query = entityManager.createQuery(queryString);

            List<InvoiceItemReason> resultList = (List<InvoiceItemReason>) query.getResultList();

            Map<Integer, InvoiceItemReason> invoiceReasonMap = new HashMap<>();
            for (InvoiceItemReason reason : resultList) {
                invoiceReasonMap.put(reason.getIdItemInvoiceReason(), reason);
            }

            if (invoice != null && anagClient != null && !invoiceReasonMap.isEmpty()) {
                result = new InvoiceDTO(invoice, anagClient);

            }

        } catch (Exception e) {
            log.error("client data", e);
        }

        return result;
    }


    public byte[] downloadInvoice(Integer invoiceId) {
        Invoice invoice = entityManager.find(Invoice.class, invoiceId);
        if (invoice != null) {
            return invoice.getPdf();
        }
        return null;
    }


    @Transactional
    public boolean updateCCid(UpdateCcidParameter dto) {

        try {
            CCIDMetadata entity = entityManager.getReference(CCIDMetadata.class, new Long(dto.getIdCCIDMetadata()));
            entity.setValoreFatturabile(new BigDecimal(dto.getInvoiceAmaunt()));
            entity.setValoreResiduo(new BigDecimal(dto.getInvoiceAmaunt()));
//			entityManager.getTransaction().begin();
            entityManager.merge(entity);
//			entityManager.getTransaction().commit();
            //return Response.ok( "").build();
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    public InvoiceConfiguration getConfiguration() {
        return invoiceConfiguration;
    }


    public CCIDMetadata getCcidMetadata(UpdateCcidParameter dto) {

        return entityManager.find(CCIDMetadata.class, new Long(dto.getIdCCIDMetadata()));
    }


    public List<InvoiceItemToCcid> getInvoiceItemToCcid(CCIDMetadata ccidMetadata) {
        List<InvoiceItemToCcid> ccids = entityManager.createQuery("Select x from InvoiceItemToCcid x where x.ccidMedata.idCCIDMetadata=:idCCIDMetadata").setParameter("idCCIDMetadata", ccidMetadata.getIdCCIDMetadata()).getResultList();
        return ccids;
    }

    public List<InvoiceCCIDDTO> availableCCID(AvailableCCIDSearchParameters dto) {
        SelectQuery<Record> condition = getCommonAvailableCCID(dto);
        List<InvoiceCCIDDTO> resultList;

        condition.addConditions(DSR_METADATA.IDDSR.notContains(BackClaimType.BC_ORIGINAL.getFileName2()));

        if (!CollectionUtils.isEmpty(dto.getUtilizationList())) {
            condition.addConditions(COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE.in(dto.getUtilizationList()));
        }

        if(dto.getIsBackclaim()!=null && dto.getIsBackclaim().booleanValue() == true)
            condition.addConditions(DSR_METADATA.IDDSR.notContains("_BC"));

        //Cambiato perché sul vecchio codice era con il residuo > 0
//			if(dto.getFatturato()!= null)
//				if(dto.getFatturato())
//					condition.addConditions(CCID_METADATA.INVOICE_STATUS.eq("DA_FATTURARE"));
//				else
//					condition.addConditions(CCID_METADATA.INVOICE_STATUS.eq("FATTURATO"));


        if (dto.getFatturato() != null)
            if (dto.getFatturato())
                condition.addConditions(CCID_METADATA.VALORE_RESIDUO.lessOrEqual(BigDecimal.ZERO));
            else
                condition.addConditions(CCID_METADATA.VALORE_RESIDUO.gt(BigDecimal.ZERO));


        condition = getPeriodContion(dto, condition);

//        Collection<OrderField<?>> listaSort = new ArrayList<>();

        condition = getSortCondition(dto, condition);

        if(dto.getCurrentPage() == null) {
            condition.addLimit(dto.getLast()+1);
            condition.addOffset(dto.getFirst());
        }else {
            condition.addLimit(maxRows + 1);
            condition.addOffset(dto.getCurrentPage() * maxRows);
        }
        resultList = condition.fetchInto(InvoiceCCIDDTO.class);

        for (InvoiceCCIDDTO item : resultList) {
            item.setPeriodStringCustom();
        }
        return resultList;
    }

    public List<InvoiceCCIDDTO> availableCCIDRipatizione(AvailableCCIDSearchParameters dto) {
        SelectQuery<Record> condition =  dsl.select(
                CCID_METADATA.ID_CCID.as("idCCID"),
                CCID_METADATA.ID_DSR,
                CCID_METADATA.TOTAL_VALUE,
                CCID_METADATA.ID_CCID_METADATA.as("idCCIDmetadata"),
                CCID_METADATA.CURRENCY,
                CCID_METADATA.CCID_VERSION,
                CCID_METADATA.CCID_ENCODED,
                CCID_METADATA.CCID_ENCODED_PROVISIONAL,
                CCID_METADATA.CCID_NOT_ENCODED,
                CCID_METADATA.INVOICE_STATUS,
                ANAG_DSP.IDDSP,
                ANAG_DSP.NAME.as("DSP_NAME"),
                COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS,
                COMMERCIAL_OFFERS.OFFERING.as("COMMERCIAL_OFFER"),
                DSR_METADATA.COUNTRY,
                DSR_METADATA.PERIOD_TYPE,
                DSR_METADATA.PERIOD,
                ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE,
                ANAG_UTILIZATION_TYPE.NAME.as("UTILIZATION_TYPE"),
                DSR_METADATA.YEAR,
                CCID_METADATA.VALORE_FATTURABILE.as("INVOICE_AMOUNT"),
                CCID_METADATA.VALORE_RESIDUO,
                ANAG_CLIENT.ID_ANAG_CLIENT,
                ANAG_CLIENT.COMPANY_NAME,
                when(cast(CCID_METADATA.VALORE_FATTURABILE, SQLDataType.DECIMAL(10,2)).minus(cast(CCID_METADATA.VALORE_RESIDUO, SQLDataType.DECIMAL(10,2))).eq(BigDecimal.ZERO), false).otherwise(true).as("ccidParziale"),
                DSR_METADATA.BACKCLAIM,
                DSR_METADATA.ID_BACKCLAIM_IN_PROGRESS,
                BACKCLAIM_IN_PROGRESS.DESCRIPTION,
                INVOICE_ITEM_TO_CCID.PERIODO_RIPARTIZIONE_ID,
                INVOICE.ID_INVOICE,
                coalesce(IMPORTI_ANTICIPO.NUMERO_FATTURA,INVOICE.INVOICE_CODE).as("NUMERO_FATTURA"),
                sum(INVOICE_ITEM_TO_CCID.VALORE).as("VALORE_FATTURA"),
                INVOICE_ITEM_TO_CCID.ID.as("idCcidInvoiceItem")
                )
                .from(DSR_METADATA)
                .join(BACKCLAIM_IN_PROGRESS).on(DSR_METADATA.ID_BACKCLAIM_IN_PROGRESS.eq(BACKCLAIM_IN_PROGRESS.ID_BACKCLAIM_IN_PROGRESS))
                .join(CCID_METADATA).on(DSR_METADATA.IDDSR.eq(CCID_METADATA.ID_DSR))
                .join(COMMERCIAL_OFFERS).on(DSR_METADATA.SERVICE_CODE.eq(COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS))
                .join(ANAG_UTILIZATION_TYPE).on(COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE.eq(ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE))
                .join(ANAG_DSP).on(COMMERCIAL_OFFERS.IDDSP.eq(ANAG_DSP.IDDSP))
                .join(CLIENT_TO_DSP).on(CLIENT_TO_DSP.IDDSP.eq(ANAG_DSP.IDDSP))
                .join(ANAG_CLIENT).on(CLIENT_TO_DSP.ANAG_CLIENT_ID.eq(ANAG_CLIENT.ID_ANAG_CLIENT))
                .join(INVOICE_ITEM_TO_CCID).on(INVOICE_ITEM_TO_CCID.ID_CCID_METADATA.eq(CCID_METADATA.ID_CCID_METADATA))
                .join(INVOICE_ITEM).on(INVOICE_ITEM.ID_ITEM.eq(INVOICE_ITEM_TO_CCID.ID_INVOICE_ITEM))
                .join(INVOICE).on(INVOICE.ID_INVOICE.eq(INVOICE_ITEM.ID_INVOICE))
                .leftJoin(IMPORTI_ANTICIPO).on(IMPORTI_ANTICIPO.ID_INVOICE.eq(INVOICE.ID_INVOICE))
                .groupBy(
                        CCID_METADATA.ID_CCID,
                        CCID_METADATA.ID_DSR,
                        CCID_METADATA.TOTAL_VALUE,
                        CCID_METADATA.ID_CCID_METADATA,
                        CCID_METADATA.CURRENCY,
                        CCID_METADATA.CCID_VERSION,
                        CCID_METADATA.CCID_ENCODED,
                        CCID_METADATA.CCID_ENCODED_PROVISIONAL,
                        CCID_METADATA.CCID_NOT_ENCODED,
                        CCID_METADATA.INVOICE_STATUS,
                        ANAG_DSP.IDDSP,
                        ANAG_DSP.NAME,
                        COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS,
                        COMMERCIAL_OFFERS.OFFERING,
                        DSR_METADATA.COUNTRY,
                        DSR_METADATA.PERIOD_TYPE,
                        DSR_METADATA.PERIOD,
                        ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE,
                        ANAG_UTILIZATION_TYPE.NAME,
                        DSR_METADATA.YEAR,
                        CCID_METADATA.VALORE_FATTURABILE,
                        CCID_METADATA.VALORE_RESIDUO,
                        ANAG_CLIENT.ID_ANAG_CLIENT,
                        ANAG_CLIENT.COMPANY_NAME,
                        when(cast(CCID_METADATA.VALORE_FATTURABILE, SQLDataType.DECIMAL(10,2)).minus(cast(CCID_METADATA.VALORE_RESIDUO, SQLDataType.DECIMAL(10,2))).eq(BigDecimal.ZERO), false).otherwise(true),
                        DSR_METADATA.BACKCLAIM,
                        DSR_METADATA.ID_BACKCLAIM_IN_PROGRESS,
                        BACKCLAIM_IN_PROGRESS.DESCRIPTION,
                        INVOICE_ITEM_TO_CCID.PERIODO_RIPARTIZIONE_ID,
                        INVOICE.ID_INVOICE,
                        coalesce(IMPORTI_ANTICIPO.NUMERO_FATTURA,INVOICE.INVOICE_CODE),
                        INVOICE_ITEM_TO_CCID.ID
                )
                .getQuery();

        if (dto.getClientId() != null)
            condition.addConditions(ANAG_CLIENT.ID_ANAG_CLIENT.eq(dto.getClientId()));

        if (!CollectionUtils.isEmpty(dto.getDspList())) {
            condition.addConditions(ANAG_DSP.IDDSP.in(dto.getDspList()));
        }

        if (!CollectionUtils.isEmpty(dto.getCountryList())) {
            condition.addConditions(DSR_METADATA.COUNTRY.in(dto.getCountryList()));
        }

        if (!CollectionUtils.isEmpty(dto.getOfferList())) {
            condition.addConditions(COMMERCIAL_OFFERS.OFFERING.in(dto.getOfferList()));
        }

        if(dto.getFlagRipartito()!= null)
            if(dto.getFlagRipartito())
                condition.addConditions(INVOICE_ITEM_TO_CCID.PERIODO_RIPARTIZIONE_ID.isNotNull());
            else
                condition.addConditions(INVOICE_ITEM_TO_CCID.PERIODO_RIPARTIZIONE_ID.isNull());




        if (!CollectionUtils.isEmpty(dto.getUtilizationList())) {
            condition.addConditions(COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE.in(dto.getUtilizationList()));
        }

        if(dto.getIsBackclaim()!=null && dto.getIsBackclaim())
            condition.addConditions(DSR_METADATA.IDDSR.notContains("_BC"));

        if (dto.getFatturato() != null)
            if (dto.getFatturato())
                condition.addConditions(CCID_METADATA.VALORE_RESIDUO.lessOrEqual(BigDecimal.ZERO));
            else
                condition.addConditions(CCID_METADATA.VALORE_RESIDUO.gt(BigDecimal.ZERO));


        if (!StringUtils.isEmpty(dto.getInvoiceCode()))
            condition.addConditions(INVOICE.INVOICE_CODE.eq(dto.getInvoiceCode()).or(IMPORTI_ANTICIPO.NUMERO_FATTURA.eq(dto.getInvoiceCode())));


        condition = getPeriodContion(dto, condition);

//        Collection<OrderField<?>> listaSort = new ArrayList<>();

        condition = getSortCondition(dto, condition);

        List<InvoiceCCIDDTO> resultList;
        log.debug("----> dto pagination ", new Throwable(Boolean.toString(dto.getPagination())));
        if (dto.getPagination()) {
            // FIX ROWNUM - CTASK0026560 (CHG0033909 )
            if(dto.getMaxRows() != null){
                maxRows = dto.getMaxRows();
            }

            condition.addLimit(maxRows + 1);
            condition.addOffset(dto.getCurrentPage() * maxRows);
        }

        log.debug("----> dto pagination ",new Throwable(condition.toString()));
        resultList = condition.fetchInto(InvoiceCCIDDTO.class);

        for (InvoiceCCIDDTO item : resultList) {
            item.setPeriodStringCustom();
        }
        return resultList;
    }

    private SelectQuery<Record> getPeriodContion(AvailableCCIDSearchParameters dto, SelectQuery<Record> condition) {
        if (dto.getYearFrom() != null && dto.getMonthFrom() != null && dto.getYearFrom() != 0 && dto.getMonthFrom() != 0) {

            Condition dateConditionMonth =
                    DSR_METADATA.PERIOD_TYPE.eq("month")
                            .and((DSR_METADATA.YEAR.gt(dto.getYearFrom()))
                                    .or(DSR_METADATA.YEAR.greaterOrEqual(dto.getYearFrom())
                                            .and(DSR_METADATA.PERIOD.greaterOrEqual(dto.getMonthFrom()))));
            Condition dateConditionQuorter = DSR_METADATA.PERIOD_TYPE.eq("quarter")
                    .and((DSR_METADATA.YEAR.gt(dto.getYearFrom()))
                            .or(DSR_METADATA.YEAR.greaterOrEqual(dto.getYearFrom())
                                    .and(DSR_METADATA.PERIOD.greaterOrEqual(dto.getMonthFrom() % 3 != 0 ? dto.getMonthFrom() / 3 + 1 : dto.getMonthFrom() / 3))));
            condition.addConditions(dateConditionMonth.or(dateConditionQuorter));
        }

        if (dto.getYearTo() != null && dto.getMonthTo() != null && dto.getYearTo() != 0 && dto.getMonthTo() != 0) {
            Condition dateConditionMonth =
                    DSR_METADATA.PERIOD_TYPE.eq("month")
                            .and((DSR_METADATA.YEAR.lt(dto.getYearTo()))
                                    .or(DSR_METADATA.YEAR.lessOrEqual(dto.getYearTo())
                                            .and(DSR_METADATA.PERIOD.lessOrEqual(dto.getMonthTo()))));
            Condition dateConditionQuorter = DSR_METADATA.PERIOD_TYPE.eq("quarter")
                    .and((DSR_METADATA.YEAR.lt(dto.getYearTo()))
                            .or(DSR_METADATA.YEAR.lessOrEqual(dto.getYearTo())
                                    .and(DSR_METADATA.PERIOD.lessOrEqual(dto.getMonthTo() % 3 != 0 ? dto.getMonthTo() / 3 + 1 : dto.getMonthTo() / 3))));
            condition.addConditions(dateConditionMonth.or(dateConditionQuorter));

        }

        if (dto.getBackclaim() != null)
            if (dto.getBackclaim().intValue() == 0)
                condition.addConditions(DSR_METADATA.BACKCLAIM.eq(0L));
            else
                condition.addConditions(DSR_METADATA.BACKCLAIM.gt(0L));

            return condition;
    }

    public List<InvoiceCCIDDTO> availableCCIDBackclaim(AvailableCCIDSearchParameters dto) {
        SelectQuery<Record> condition = getCommonAvailableCCID(dto);
        List<InvoiceCCIDDTO> resultList;

        if(dto.getIsBackclaim()!=null && dto.getIsBackclaim())
            condition.addConditions(DSR_METADATA.IDDSR.notContains("_BC"));

        //Cambiato perché sul vecchio codice era con il residuo > 0
//			if(dto.getFatturato()!= null)
//				if(dto.getFatturato())
//					condition.addConditions(CCID_METADATA.INVOICE_STATUS.eq("DA_FATTURARE"));
//				else
//					condition.addConditions(CCID_METADATA.INVOICE_STATUS.eq("FATTURATO"));


        getPeriodContion(dto, condition);

        condition.addGroupBy(CCID_METADATA.ID_CCID);

//        Collection<OrderField<?>> listaSort = new ArrayList<>();

        condition = getSortCondition(dto, condition);
        condition.addLimit(maxRows+1);
        condition.addOffset(dto.getCurrentPage()*maxRows);
        resultList = condition.fetchInto(InvoiceCCIDDTO.class);

        for (InvoiceCCIDDTO item : resultList) {
            item.setPeriodStringCustom();
        }
        return resultList;
    }

    private SelectQuery<Record> getSortCondition(AvailableCCIDSearchParameters dto, SelectQuery<Record> condition) {
        if (dto.getSortBy() != null)
            switch (dto.getSortBy()) {


                case idDsr:
                    if (dto.getAsc())
                        condition.addOrderBy(CCID_METADATA.ID_CCID.asc());
                    else
                        condition.addOrderBy(CCID_METADATA.ID_CCID.desc());
                    break;
                case totalValue:
                    if (dto.getAsc())
                        condition.addOrderBy(CCID_METADATA.TOTAL_VALUE.asc());
                    else
                        condition.addOrderBy(CCID_METADATA.TOTAL_VALUE.desc());
                    break;
                case totalValueOrigCurrency:
                    break;
                case idCCIDmetadata:
                    break;
                case invoiceStatus:
                    if (dto.getAsc())
                        condition.addOrderBy(CCID_METADATA.INVOICE_STATUS.asc());
                    else
                        condition.addOrderBy(CCID_METADATA.INVOICE_STATUS.desc());
                    break;
                case dspName:
                    if (dto.getAsc())
                        condition.addOrderBy(ANAG_DSP.NAME.asc());
                    else
                        condition.addOrderBy(ANAG_DSP.NAME.desc());
                    break;
                case utilizationType:
                    if (dto.getAsc())
                        condition.addOrderBy(ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE.asc());
                    else
                        condition.addOrderBy(ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE.desc());
                    break;
                case commercialOffer:
                    if (dto.getAsc())
                        condition.addOrderBy(COMMERCIAL_OFFERS.OFFERING.asc());
                    else
                        condition.addOrderBy(COMMERCIAL_OFFERS.OFFERING.desc());
                    break;
                case country:
                    if (dto.getAsc())
                        condition.addOrderBy(DSR_METADATA.COUNTRY.asc());
                    else
                        condition.addOrderBy(DSR_METADATA.COUNTRY.desc());
                    break;
                case periodString:
                    if (dto.getAsc()) {
                        condition.addOrderBy(DSR_METADATA.YEAR.asc());
                        condition.addOrderBy(DSR_METADATA.PERIOD.asc());
                    } else {
                        condition.addOrderBy(DSR_METADATA.YEAR.desc());
                        condition.addOrderBy(DSR_METADATA.PERIOD.desc());
                    }
                    break;
                case year:
                    break;
                case invoiceAmount:
                    if (dto.getAsc())
                        condition.addOrderBy(CCID_METADATA.VALORE_FATTURABILE.asc());
                    else
                        condition.addOrderBy(CCID_METADATA.VALORE_FATTURABILE.desc());
                    break;
                case valoreResiduo:
                    if (dto.getAsc())
                        condition.addOrderBy(CCID_METADATA.VALORE_RESIDUO.asc());
                    else
                        condition.addOrderBy(CCID_METADATA.VALORE_RESIDUO.desc());
                    break;
                case companyName:
                    if (dto.getAsc())
                        condition.addOrderBy(ANAG_CLIENT.COMPANY_NAME.asc());
                    else
                        condition.addOrderBy(ANAG_CLIENT.COMPANY_NAME.desc());
                    break;
                case ccidParziale:
                    if (dto.getAsc())
                        condition.addOrderBy(when(cast(CCID_METADATA.VALORE_FATTURABILE, SQLDataType.DECIMAL(10,2)).minus(cast(CCID_METADATA.VALORE_RESIDUO, SQLDataType.DECIMAL(10,2))).eq(BigDecimal.ZERO), false).otherwise(true).asc());
                    else
                        condition.addOrderBy(when(cast(CCID_METADATA.VALORE_FATTURABILE, SQLDataType.DECIMAL(10,2)).minus(cast(CCID_METADATA.VALORE_RESIDUO, SQLDataType.DECIMAL(10,2))).eq(BigDecimal.ZERO), false).otherwise(true).desc());
                    break;
                case backclaim:
                    if (dto.getAsc())
                        condition.addOrderBy(DSR_METADATA.BACKCLAIM.asc());
                    else
                        condition.addOrderBy(DSR_METADATA.BACKCLAIM.desc());
                    break;
                case idBackclaimInProgress:
                    if (dto.getAsc())
                        condition.addOrderBy(DSR_METADATA.ID_BACKCLAIM_IN_PROGRESS.asc());
                    else
                        condition.addOrderBy(DSR_METADATA.ID_BACKCLAIM_IN_PROGRESS.desc());
                    break;
                case numeroFattura:
                    if (dto.getAsc())
                        condition.addOrderBy(coalesce(IMPORTI_ANTICIPO.NUMERO_FATTURA,INVOICE.INVOICE_CODE).asc());
                    else
                        condition.addOrderBy(coalesce(IMPORTI_ANTICIPO.NUMERO_FATTURA,INVOICE.INVOICE_CODE).desc());
                    break;
                case valoreFattura:
                    if (dto.getAsc())
                        condition.addOrderBy(sum(INVOICE_ITEM_TO_CCID.VALORE).asc());
                    else
                        condition.addOrderBy(sum(INVOICE_ITEM_TO_CCID.VALORE).desc());
                    break;
                case periodoRipartizioneId:
                    if (dto.getAsc())
                        condition.addOrderBy(INVOICE_ITEM_TO_CCID.PERIODO_RIPARTIZIONE_ID.asc());
                    else
                        condition.addOrderBy(INVOICE_ITEM_TO_CCID.PERIODO_RIPARTIZIONE_ID.desc());
                    break;
            }
        return condition;
    }

    private SelectQuery<Record> getCommonAvailableCCID(AvailableCCIDSearchParameters dto) {

        SelectQuery<Record> condition = dsl.select(
                CCID_METADATA.ID_CCID.as("idCCID"),
                CCID_METADATA.ID_DSR,
                CCID_METADATA.TOTAL_VALUE,
                CCID_METADATA.ID_CCID_METADATA.as("idCCIDmetadata"),
                CCID_METADATA.CURRENCY,
                CCID_METADATA.CCID_VERSION,
                CCID_METADATA.CCID_ENCODED,
                CCID_METADATA.CCID_ENCODED_PROVISIONAL,
                CCID_METADATA.CCID_NOT_ENCODED,
                CCID_METADATA.INVOICE_STATUS,
                ANAG_DSP.IDDSP,
                ANAG_DSP.NAME.as("DSP_NAME"),
                COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS,
                COMMERCIAL_OFFERS.OFFERING.as("COMMERCIAL_OFFER"),
                DSR_METADATA.COUNTRY,
                DSR_METADATA.PERIOD_TYPE,
                DSR_METADATA.PERIOD,
                ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE,
                ANAG_UTILIZATION_TYPE.NAME.as("UTILIZATION_TYPE"),
//				when(DSR_METADATA.PERIOD_TYPE.eq('month')
//					.then(concat((DSR_METADATA.PERIOD, 'Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov','Dic'),' ', DSR_METADATA.YEAR))
//					.else(concat(ELT(DSR_METADATA.PERIOD, 'Gen - Mar', 'Apr - Giu', 'Lug - Set', 'Ott - Dic') , ' ', DSR_METADATA.YEAR).as("PERIOD_STRING"),
                DSR_METADATA.YEAR,
                CCID_METADATA.VALORE_FATTURABILE.as("INVOICE_AMOUNT"),
                CCID_METADATA.VALORE_RESIDUO,
                ANAG_CLIENT.ID_ANAG_CLIENT,
                ANAG_CLIENT.COMPANY_NAME,
                when(cast(CCID_METADATA.VALORE_FATTURABILE, SQLDataType.DECIMAL(10,2)).minus(cast(CCID_METADATA.VALORE_RESIDUO, SQLDataType.DECIMAL(10,2))).eq(BigDecimal.ZERO), false).otherwise(true).as("ccidParziale"),
                DSR_METADATA.BACKCLAIM,
                DSR_METADATA.ID_BACKCLAIM_IN_PROGRESS,
                DSR_METADATA.BC_TYPE,
                BACKCLAIM_IN_PROGRESS.DESCRIPTION)
                .from(DSR_METADATA)
                .join(BACKCLAIM_IN_PROGRESS).on(DSR_METADATA.ID_BACKCLAIM_IN_PROGRESS.eq(BACKCLAIM_IN_PROGRESS.ID_BACKCLAIM_IN_PROGRESS))
                .join(CCID_METADATA).on(DSR_METADATA.IDDSR.eq(CCID_METADATA.ID_DSR))
                .join(COMMERCIAL_OFFERS).on(DSR_METADATA.SERVICE_CODE.eq(COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS))
                .join(ANAG_UTILIZATION_TYPE).on(COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE.eq(ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE))
                .join(ANAG_DSP).on(COMMERCIAL_OFFERS.IDDSP.eq(ANAG_DSP.IDDSP))
                .join(CLIENT_TO_DSP).on(CLIENT_TO_DSP.IDDSP.eq(ANAG_DSP.IDDSP))
                .join(ANAG_CLIENT).on(CLIENT_TO_DSP.ANAG_CLIENT_ID.eq(ANAG_CLIENT.ID_ANAG_CLIENT))
                .getQuery();

        if (dto.getClientId() != null)
            condition.addConditions(ANAG_CLIENT.ID_ANAG_CLIENT.eq(dto.getClientId()));

        if (!CollectionUtils.isEmpty(dto.getDspList())) {
            condition.addConditions(ANAG_DSP.IDDSP.in(dto.getDspList()));
        }

        if (!CollectionUtils.isEmpty(dto.getCountryList())) {
            condition.addConditions(DSR_METADATA.COUNTRY.in(dto.getCountryList()));
        }

        if (!CollectionUtils.isEmpty(dto.getOfferList())) {
            condition.addConditions(COMMERCIAL_OFFERS.OFFERING.in(dto.getOfferList()));
        }
        return condition;
    }


    public List<InvoiceCCIDDTO> availableCCIDReclami(AvailableCCIDSearchParameters dto) {
        List<InvoiceCCIDDTO> resultList = new ArrayList<>();


        try {

            Condition condition = DSL.trueCondition();

/*
            if (dto.getClientId() != null)
                condition = condition.and(INVOICE_RECLAMO.ID_ANAG_CLIENT.eq(dto.getClientId()));
*/
            if (!CollectionUtils.isEmpty(dto.getDspList())) {
                condition = condition.and(INVOICE_RECLAMO.ID_DSP.in(dto.getDspList()));
            }

            if (!CollectionUtils.isEmpty(dto.getCountryList())) {
                condition = condition.and(DSR_METADATA.COUNTRY.in(dto.getCountryList()));
            }

            if (!CollectionUtils.isEmpty(dto.getOfferList())) {
                condition = condition.and(COMMERCIAL_OFFERS.OFFERING.in(dto.getOfferList()));
            }

            if (!CollectionUtils.isEmpty(dto.getUtilizationList())) {
                condition = condition.and(COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE.in(dto.getUtilizationList()));
            }

            //Cambiato perché sul vecchio codice era con il residuo > 0
//			if(dto.getFatturato()!= null)
//				if(dto.getFatturato())
//					condition = condition.and(CCID_METADATA.INVOICE_STATUS.eq("DA_FATTURARE"));
//				else
//					condition = condition.and(CCID_METADATA.INVOICE_STATUS.eq("FATTURATO"));


            if (dto.getFatturato() != null)
                if (dto.getFatturato())
                    condition = condition.and(CCID_METADATA.VALORE_RESIDUO.lessOrEqual(BigDecimal.ZERO));
                else
                    condition = condition.and(CCID_METADATA.VALORE_RESIDUO.gt(BigDecimal.ZERO));

    if (dto.getYearFrom() != null && dto.getMonthFrom() != null && dto.getYearFrom() != 0 && dto.getMonthFrom() != 0) {

                Condition dateConditionMonth =
                        DSR_METADATA.PERIOD_TYPE.eq("month")
                                .and((DSR_METADATA.YEAR.gt(dto.getYearFrom()))
                                        .or(DSR_METADATA.YEAR.greaterOrEqual(dto.getYearFrom())
                                                .and(DSR_METADATA.PERIOD.greaterOrEqual(dto.getMonthFrom()))));
                Condition dateConditionQuorter = DSR_METADATA.PERIOD_TYPE.eq("quarter")
                        .and((DSR_METADATA.YEAR.gt(dto.getYearFrom()))
                                .or(DSR_METADATA.YEAR.greaterOrEqual(dto.getYearFrom())
                                        .and(DSR_METADATA.PERIOD.greaterOrEqual((dto.getMonthFrom() / 3) + 1))));
                condition = condition.and(dateConditionMonth.or(dateConditionQuorter));
            }

            if (dto.getYearTo() != null && dto.getMonthTo() != null && dto.getYearTo() != 0 && dto.getMonthTo() != 0) {
                Condition dateConditionMonth =
                        DSR_METADATA.PERIOD_TYPE.eq("month")
                                .and((DSR_METADATA.YEAR.lt(dto.getYearTo()))
                                        .or(DSR_METADATA.YEAR.lessOrEqual(dto.getYearTo())
                                                .and(DSR_METADATA.PERIOD.lessOrEqual(dto.getMonthTo()))));
                Condition dateConditionQuorter = DSR_METADATA.PERIOD_TYPE.eq("quarter")
                        .and((DSR_METADATA.YEAR.lt(dto.getYearTo()))
                                .or(DSR_METADATA.YEAR.lessOrEqual(dto.getYearTo())
                                        .and(DSR_METADATA.PERIOD.lessOrEqual((dto.getMonthTo() / 3) + 1))));
                condition = condition.and(dateConditionMonth.or(dateConditionQuorter));
            }

            Collection<OrderField<?>> listaSort = new ArrayList<>();

            if (dto.getSortBy() != null)
                switch (dto.getSortBy()) {

                    case idDsr:
                        if (dto.getAsc())
                            listaSort.add(CCID_METADATA.ID_CCID.asc());
                        else
                            listaSort.add(CCID_METADATA.ID_CCID.desc());
                        break;
                    case totalValue:
                        if (dto.getAsc())
                            listaSort.add(CCID_METADATA.TOTAL_VALUE.asc());
                        else
                            listaSort.add(CCID_METADATA.TOTAL_VALUE.desc());
                        break;
                    case invoiceStatus:
                        if (dto.getAsc())
                            listaSort.add(CCID_METADATA.INVOICE_STATUS.asc());
                        else
                            listaSort.add(CCID_METADATA.INVOICE_STATUS.desc());
                        break;
                    case dspName:
                        if (dto.getAsc())
                            listaSort.add(ANAG_DSP.NAME.asc());
                        else
                            listaSort.add(ANAG_DSP.NAME.desc());
                        break;
                    case utilizationType:
                        if (dto.getAsc())
                            listaSort.add(ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE.asc());
                        else
                            listaSort.add(ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE.desc());
                        break;
                    case commercialOffer:
                        if (dto.getAsc())
                            listaSort.add(COMMERCIAL_OFFERS.OFFERING.asc());
                        else
                            listaSort.add(COMMERCIAL_OFFERS.OFFERING.desc());
                        break;
                    case country:
                        if (dto.getAsc())
                            listaSort.add(DSR_METADATA.COUNTRY.asc());
                        else
                            listaSort.add(DSR_METADATA.COUNTRY.desc());
                        break;
                    case periodString:
                        if (dto.getAsc()) {
                            listaSort.add(DSR_METADATA.YEAR.asc());
                            listaSort.add(DSR_METADATA.PERIOD.asc());
                        } else {
                            listaSort.add(DSR_METADATA.YEAR.desc());
                            listaSort.add(DSR_METADATA.PERIOD.desc());
                        }
                        break;
                    case invoiceAmount:
                        if (dto.getAsc())
                            listaSort.add(CCID_METADATA.VALORE_FATTURABILE.asc());
                        else
                            listaSort.add(CCID_METADATA.VALORE_FATTURABILE.desc());
                        break;
                    case valoreResiduo:
                        if (dto.getAsc())
                            listaSort.add(CCID_METADATA.VALORE_RESIDUO.asc());
                        else
                            listaSort.add(CCID_METADATA.VALORE_RESIDUO.desc());
                        break;
                    case companyName:
                        if (dto.getAsc())
                            listaSort.add(ANAG_CLIENT.COMPANY_NAME.asc());
                        else
                            listaSort.add(ANAG_CLIENT.COMPANY_NAME.desc());
                        break;
                    case ccidParziale:
                        if (dto.getAsc())
                            listaSort.add(when(cast(CCID_METADATA.VALORE_FATTURABILE, SQLDataType.DECIMAL(10,2)).minus(cast(CCID_METADATA.VALORE_RESIDUO, SQLDataType.DECIMAL(10,2))).eq(BigDecimal.ZERO), false).otherwise(true).asc());
                        else
                            listaSort.add(when(cast(CCID_METADATA.VALORE_FATTURABILE, SQLDataType.DECIMAL(10,2)).minus(cast(CCID_METADATA.VALORE_RESIDUO, SQLDataType.DECIMAL(10,2))).eq(BigDecimal.ZERO), false).otherwise(true).desc());
                        break;
                }


            SortField<String> asc = CCID_METADATA.ID_CCID.asc();


            OrderField of = CCID_METADATA.ID_DSR.asc();

            resultList = dsl.select(
                    CCID_METADATA.ID_CCID.as("idCCID"),
                    CCID_METADATA.ID_DSR,
                    CCID_METADATA.TOTAL_VALUE,
                    CCID_METADATA.ID_CCID_METADATA.as("idCCIDmetadata"),
                    CCID_METADATA.CURRENCY,
                    CCID_METADATA.CCID_VERSION,
                    CCID_METADATA.CCID_ENCODED,
                    CCID_METADATA.CCID_ENCODED_PROVISIONAL,
                    CCID_METADATA.CCID_NOT_ENCODED,
                    CCID_METADATA.INVOICE_STATUS,
                    ANAG_DSP.IDDSP,
                    ANAG_DSP.NAME.as("DSP_NAME"),
                    COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS,
                    COMMERCIAL_OFFERS.OFFERING.as("COMMERCIAL_OFFER"),
                    DSR_METADATA.COUNTRY,
                    DSR_METADATA.PERIOD_TYPE,
                    DSR_METADATA.PERIOD,
                    ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE,
                    ANAG_UTILIZATION_TYPE.NAME.as("UTILIZATION_TYPE"),
//				when(DSR_METADATA.PERIOD_TYPE.eq('month')
//					.then(concat((DSR_METADATA.PERIOD, 'Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov','Dic'),' ', DSR_METADATA.YEAR))
//					.else(concat(ELT(DSR_METADATA.PERIOD, 'Gen - Mar', 'Apr - Giu', 'Lug - Set', 'Ott - Dic') , ' ', DSR_METADATA.YEAR).as("PERIOD_STRING"),
                    DSR_METADATA.YEAR,
                    CCID_METADATA.VALORE_FATTURABILE.as("INVOICE_AMOUNT"),
                    CCID_METADATA.VALORE_RESIDUO,
                    ANAG_CLIENT.ID_ANAG_CLIENT,
                    ANAG_CLIENT.COMPANY_NAME,
                    when(cast(CCID_METADATA.VALORE_FATTURABILE, SQLDataType.DECIMAL(10,2)).minus(cast(CCID_METADATA.VALORE_RESIDUO, SQLDataType.DECIMAL(10,2))).eq(BigDecimal.ZERO), false).otherwise(true).as("ccidParziale"))           		
                    .from(DSR_METADATA)
                    .join(CCID_METADATA).on(DSR_METADATA.IDDSR.eq(CCID_METADATA.ID_DSR))
                    .join(COMMERCIAL_OFFERS).on(DSR_METADATA.SERVICE_CODE.eq(COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS))
                    .join(ANAG_UTILIZATION_TYPE).on(COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE.eq(ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE))
                    .join(ANAG_DSP).on(COMMERCIAL_OFFERS.IDDSP.eq(ANAG_DSP.IDDSP))
                    .join(CLIENT_TO_DSP).on(CLIENT_TO_DSP.IDDSP.eq(ANAG_DSP.IDDSP))
                    .join(ANAG_CLIENT).on(CLIENT_TO_DSP.ANAG_CLIENT_ID.eq(ANAG_CLIENT.ID_ANAG_CLIENT))
                    .join(INVOICE_RECLAMO).on(INVOICE_RECLAMO.ID_DSR.eq(CCID_METADATA.ID_DSR))
                    .where(condition)
                    .orderBy(listaSort)
                    .limit(dto.getLast()).offset(dto.getFirst())
                    .fetchInto(InvoiceCCIDDTO.class);


            for (InvoiceCCIDDTO item : resultList) {
                item.setPeriodStringCustom();
            }


        } catch (Exception e) {
            // TODO: handle exception
            log.error("invoice available dsr", e);
        }
        return resultList;
    }
}

