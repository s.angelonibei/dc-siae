package com.alkemy.sophia.api.sophiamsinvoice.entities;


import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
@Entity(name = "InvoiceItemReason")
@Table(name = "ITEM_INVOICE_REASON")
//@NamedQueries({@NamedQuery(name = "InvoiceItemReason.GetAll", query = "SELECT x FROM InvoiceItemReason x")})
@Data
@EqualsAndHashCode
public class InvoiceItemReason  {

    @Id
    @Column(name = "ID_ITEM_INVOICE_REASON", nullable = false)
    private Integer idItemInvoiceReason;

    @Column(name = "CODE", nullable = false)
    private String code;
    

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    @Column(name = "CONTO_SAP", nullable = false)
    private String contoSap;



}
