package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode
@ToString
public class AvailableCCIDSearchParameters extends BaseRequestDTO{

    Boolean pagination;
    Integer first;
    Integer last;

    Integer clientId;
    List<String> dspList;
    List<String> countryList;
    List<String> utilizationList;
    List<String>  offerList;
    Integer monthFrom;
    Integer yearFrom;
    Integer monthTo;
    Integer yearTo;
    Boolean fatturato;
    BigDecimal backclaim;
    Short idBackclaimInProgress;

    Boolean isBackclaim;
    Boolean flagRipartito;

    InvoiceCCIDDTO.SORT sortBy;
    Boolean asc;

    String invoiceCode;

}
