package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto.sqs;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class Body {
    private String dsp;
    private String country;
    private String forced;
    private String dspCode;
    private String config;
    private String idDsr;
    private String dsrFileNorm;
    private Map<String, Object> additionalProperties = new HashMap<>();

    protected boolean canEqual(final Object other) {
        return other instanceof Body;
    }

}
