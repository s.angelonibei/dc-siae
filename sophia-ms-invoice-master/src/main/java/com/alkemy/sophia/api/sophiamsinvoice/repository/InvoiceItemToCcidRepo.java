package com.alkemy.sophia.api.sophiamsinvoice.repository;

import com.alkemy.sophia.api.sophiamsinvoice.entities.InvoiceItemToCcid;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvoiceItemToCcidRepo extends JpaRepository<InvoiceItemToCcid, Integer> {

}
