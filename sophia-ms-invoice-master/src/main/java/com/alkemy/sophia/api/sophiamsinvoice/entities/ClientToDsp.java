package com.alkemy.sophia.api.sophiamsinvoice.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;


@Table(name = "CLIENT_TO_DSP")
@Data
@Entity
@EqualsAndHashCode
public class ClientToDsp {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "ANAG_CLIENT_ID")
    private AnagClient anagClient;

    @ManyToOne
    @JoinColumn(name = "IDDSP")
    private AnagDsp anagDsp;

    @Column(name = "LICENCE")
    private String licence;


    @Column(name = "START_DATE", nullable = false)
    private Date startDate;

    @Column(name = "END_DATE")
    private Date endDate;

}
 
