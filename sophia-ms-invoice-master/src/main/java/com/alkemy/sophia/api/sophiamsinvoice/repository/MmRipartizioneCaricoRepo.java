package com.alkemy.sophia.api.sophiamsinvoice.repository;

import com.alkemy.sophia.api.sophiamsinvoice.entities.MmRipartizioneCarico;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MmRipartizioneCaricoRepo extends JpaRepository<MmRipartizioneCarico, Long> {

    MmRipartizioneCarico findFirstByIdRipartizioneSiadaOrderByVersioneDesc(Long idRipartizioneSiada);

    List<MmRipartizioneCarico> findByIdRipartizioneSiadaAndIdNot(Long idRipartizioneSiada, Long id);

}
