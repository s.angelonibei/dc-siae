package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;


import com.alkemy.sophia.api.sophiamsinvoice.entities.MmRipartizioneCarico;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.ToString;

import java.util.Date;
import java.util.List;

@Data
@ToString
public class CaricoRipartizioneDettaglioDTO {
  private Long id;
  private String idRipartizioneSiada;
  private String versione;
  private MmRipartizioneCarico.STATO stato;
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
  private Date dataOraCreazione;
  private int numCcid;
  private Double valoreTotaleRipartizione;

}
