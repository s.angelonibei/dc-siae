package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;

import com.alkemy.sophia.api.sophiamsinvoice.entities.InvoiceItem;
import com.alkemy.sophia.api.sophiamsinvoice.entities.InvoiceItemReason;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Produces("application/json")
@XmlRootElement
public class InvoiceItemDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5820430661097750658L;
	private InvoiceItemReason description;
	private BigDecimal totalValue;
	private BigDecimal totalValueOrig;
	private String currency;
	private String note;
	private List<String> ccidList;
	private Integer idItem;
	private Integer idInvoice;
	private Integer invoicePosition;
	private InvoiceItemReasonDTO invoiceItemReason;
	private List<InvoiceItemDetailDTO> invoiceDetailList;
	private String descriptionText;
	private Integer idDescription;
	public InvoiceItemDto() {

	}

	public InvoiceItemDto(InvoiceItem element) {
		this.description = element.getDescription();
		this.totalValue = element.getTotal();
		this.totalValueOrig = element.getTotalOrigCurrency();
		this.currency = element.getOrigCurrency();
		this.note = element.getNote();
		this.ccidList = null;
		this.idItem = element.getIdItem();
		this.idInvoice = element.getInvoice().getIdInvoice();
		this.invoicePosition = element.getInvoicePosition();
		this.invoiceItemReason = new InvoiceItemReasonDTO(element.getDescription());
		this.invoiceDetailList = null;
		this.descriptionText = element.getDescription().getDescription();
		this.idDescription=element.getDescription().getIdItemInvoiceReason();
	
	}


	public InvoiceItemReason getDescription() {
		return description;
	}

	public void setDescription(InvoiceItemReason description) {
		this.description = description;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public BigDecimal getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(BigDecimal totalValue) {
		this.totalValue = totalValue;
	}

	public List<String> getCcidList() {
		return ccidList;
	}

	public void setCcidList(List<String> ccidList) {
		this.ccidList = ccidList;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public InvoiceItemReasonDTO getInvoiceItemReason() {
		return invoiceItemReason;
	}

	public void setInvoiceItemReason(InvoiceItemReasonDTO invoiceItemReason) {
		this.invoiceItemReason = invoiceItemReason;
	}

	public Integer getIdItem() {
		return idItem;
	}

	public void setIdItem(Integer idItem) {
		this.idItem = idItem;
	}

	public Integer getIdInvoice() {
		return idInvoice;
	}

	public void setIdInvoice(Integer idInvoice) {
		this.idInvoice = idInvoice;
	}

	public Integer getInvoicePosition() {
		return invoicePosition;
	}

	public void setInvoicePosition(Integer invoicePosition) {
		this.invoicePosition = invoicePosition;
	}

	public List<InvoiceItemDetailDTO> getInvoiceDetailList() {
		return invoiceDetailList;
	}

	public void setInvoiceDetailList(List<InvoiceItemDetailDTO> invoiceDetailList) {
		this.invoiceDetailList = invoiceDetailList;
	}

	public String getDescriptionText() {
		return descriptionText;
	}

	public void setDescriptionText(String descriptionText) {
		this.descriptionText = descriptionText;
	}

	public BigDecimal getTotalValueOrig() {
		return totalValueOrig;
	}

	public void setTotalValueOrig(BigDecimal totalValueOrig) {
		this.totalValueOrig = totalValueOrig;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Integer getIdDescription() {
		return idDescription;
	}

	public void setIdDescription(Integer idDescription) {
		this.idDescription = idDescription;
	}

}
