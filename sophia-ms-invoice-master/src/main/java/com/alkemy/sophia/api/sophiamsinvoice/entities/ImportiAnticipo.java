package com.alkemy.sophia.api.sophiamsinvoice.entities;


import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;
import java.util.Date;

@XmlRootElement
@Entity(name="ImportiAnticipo")
@Table(name="IMPORTI_ANTICIPO")
@Data
@EqualsAndHashCode
public class ImportiAnticipo {

    @Id
    @Column(name = "ID_IMPORTO_ANTICIPO", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idImportoAnticipo;
    
    @Column(name = "PERIODO_PERTINENZA_INIZIO", nullable = false)
	private Date periodoPertinenzaInizio;
    
    @Column(name = "PERIODO_PERTINENZA_FINE", nullable = false)
	private Date periodoPertinenzaFine;
    
    @Column(name = "NUMERO_FATTURA", nullable = false)
	private String numeroFattura;
   
    @Column(name = "ID_INVOICE", nullable = false)
	private Integer idInvoice;
    
    @Column(name = "IMPORTO_ORIGINALE", nullable = false)
	private BigDecimal importoOriginale;
    
    @Column(name = "IMPORTO_UTILIZZABILE", nullable = false)
	private BigDecimal importoUtilizzabile;

    @Column(name = "IMPORTO_ACCANTONATO", nullable = false)
    private BigDecimal importoAccantonato;

    @Column(name = "IMPORTO_ACCANTONATO_UTILIZZABILE", nullable = false)
    private BigDecimal importoAccantonatoUtilizzabile;

    @Column(name = "NOTE")
    private String note;

    @Column(name = "PERCENTUALE_ACCANTONAMENTO")
    private BigDecimal percentualeAccantonamento;

    @Column(name = "IMPORTO_UTILIZZATO_RECLAMI")
    private BigDecimal importoUtilizzatoReclami;



}
