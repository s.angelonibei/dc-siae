package com.alkemy.sophia.api.sophiamsinvoice.invoice.service;

import com.alkemy.sophia.api.sophiamsinvoice.invoice.dto.*;
import com.alkemy.sophia.api.sophiamsinvoice.entities.*;
import com.alkemy.sophia.api.sophiamsinvoice.repository.*;
import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.io.CompressionAwareFileOutputStream;
import com.amazonaws.services.s3.AmazonS3URI;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.QuoteMode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.jooq.impl.DSL.field;

@Service
@RequiredArgsConstructor
@CommonsLog
public class ReclamoService {

    private final ReclamoRepo reclamoRepo;
    private final S3 s3;
    private final ReclamoConfigDTO reclamoConfigDTO;


    @Value("${invoice.inputReclamiPath}")
    protected String inputReclamiPath;
    
    @Value("${spring.http.multipart.location}")
    protected String tmpLocation;

    @Value("${invoice.s3.bucket}")
    private String bucketS3;

    //@todo check default value
    @Value("${invoice.outputReclamiBasePath}")
    protected String outputReclamiBasePath;

    @Transactional(propagation = Propagation.SUPPORTS, isolation = Isolation.READ_COMMITTED)
    public List<ReclamoDTO> getListInvoiceReclamo() {
        List<ReclamoDTO> reclamiDTO = new ArrayList<>();
        reclamoRepo.findAll(new Sort(Sort.Direction.DESC, "idInvoiceReclamo")).stream().forEach( entity -> {
              String period = entity.getPeriod().toString();
              reclamiDTO.add(
                new ReclamoDTO(
                  entity.getIdInvoiceReclamo(),
                  entity.getIdDsp(),
                  entity.getIdDsr(),
                  entity.getCountry(),
                  entity.getCommercialOffer(),
                  entity.getDataInserimento(),
                  entity.getDataUltimaModifica(),
                  entity.getUtente(),
                  entity.getPeriod(),
                  (2 == entity.getStato()) ? "OK" : "KO",
                  entity.getPeriodType(),
                  entity.getYear(),
                  ( entity.getPeriodType().contains("MM") )
                    ? Integer.parseInt(period.substring(entity.getPeriodType().indexOf("MM"), period.length()))
                    : null ,
                  entity.getFileLocation(),
                  null
                )
              );
          }
        );
        return reclamiDTO;
    }

    @Transactional
    public void addReclamo(ReclamoDTO reclamoDTO) throws Exception{

        ByteArrayOutputStream baos = getByteArrayOutputStream(reclamoDTO.getFile());
        InputStream inputFile = new ByteArrayInputStream(baos.toByteArray());

        String uuid= UUID.randomUUID().toString();
        String justFileName = reclamoDTO.getFileName().substring(0, reclamoDTO.getFileName().lastIndexOf("."));
        String extension = reclamoDTO.getFileName().substring(reclamoDTO.getFileName().lastIndexOf("."));
        reclamoDTO.setFileName(justFileName + "-" + uuid +extension);


        String inputFilePath = uploadFile(reclamoDTO.getFileName(), inputFile, s3, inputReclamiPath,false);


        log.info("file caricato  su S3 path completo=" + inputReclamiPath + reclamoDTO.getFileName());




        InvoiceReclamo reclamo = new InvoiceReclamo();
        reclamo.setCountry(reclamoDTO.getCountry());

        reclamo.setFileLocation(inputFilePath);
        reclamo.setFileName(reclamoDTO.getFileName());

        reclamo.setDataInserimento(new Date());
        reclamo.setIdDsp(reclamoDTO.getIdDsp());
        reclamo.setPeriod(reclamoDTO.getPeriod());
        reclamo.setPeriodType(reclamoDTO.getPeriodType());
        reclamo.setCommercialOffer(reclamoDTO.getCommercialOffer());
        reclamo.setUtente(reclamoDTO.getUtente());
        reclamo.setYear(reclamoDTO.getYear());
        reclamo.setStato(new Integer(1));

        DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        String dataInserimento = df.format(new Date());
        String fileNameDsr="Report_reclami_bmat_" + reclamo.getCountry() + "_"  + reclamo.getPeriod() + "_" + dataInserimento + "_" + reclamo.getIdDsp()+reclamo.getCommercialOffer();
        reclamo.setIdDsr(fileNameDsr);

        log.info("ID_DSR=" +fileNameDsr);

        reclamoRepo.save(reclamo);
        log.info("Riga Reclamo inserita sul database con ID_INVOICE_RECLAMO=" +reclamo.getIdInvoiceReclamo());


        S3.Url s3url =new S3.Url( inputReclamiPath + reclamoDTO.getFileName());
        File fileOutput = new File(tmpLocation+reclamoDTO.getFileName());
        s3.download(s3url,fileOutput);
        InputStream targetStream = new FileInputStream(fileOutput);

        String outputFilePath = convertSimpleCsvToValidOriginalDsr(
                reclamoDTO,
                targetStream,
                fileNameDsr+ ".csv.gz",
                "utf-8"
        );
        log.info("file dsr creato su path =" +outputFilePath);

        if(outputFilePath==null)
            reclamo.setStato(new Integer(-1));
        else
            reclamo.setStato(new Integer(2));

        reclamoRepo.save(reclamo);



    }


    ///metodi di utilità
    protected ByteArrayOutputStream getByteArrayOutputStream(InputStream file) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len;
        while ((len = file.read(buffer)) > -1 ) {
            baos.write(buffer, 0, len);
        }
        baos.flush();
        return baos;
    }


    private String convertSimpleCsvToValidOriginalDsr(ReclamoDTO reclamoDTO , InputStream inputStream, String fileName, String charset ) {

        int fileMaxRows = 2000;
        BufferedReader bufferedReader;
        try {
            // prepare files
            final File csvFile = new File(tmpLocation+fileName);
            String fileVName = fileName.replaceAll(".csv.gz","_V.csv");
            final File fileV = new File(tmpLocation+fileVName);
            // read CSV configuration
            //@todo effettuare check sul path affinché si aggiunga uno slash finale in caso in configurazione sia assente
            // add period to output path in configuration
            final String outputReclamiPath = this.outputReclamiBasePath + reclamoDTO.getPeriod().toString()+"/";

            // input file reader
            bufferedReader = new BufferedReader(new InputStreamReader( inputStream, charset ));

            // output .csv.gz related  objects
            final OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new CompressionAwareFileOutputStream(csvFile), charset);
            final CSVPrinter csvPrinter = new CSVPrinter(new BufferedWriter(outputStreamWriter),  CSVFormat.newFormat(';')
                    .withQuoteMode(QuoteMode.MINIMAL)
                    .withQuote(reclamoConfigDTO.getTextDelimiter())
                    .withRecordSeparator(reclamoConfigDTO.getRecordSeparator())) ;

            // output _V.csv related objects
            final OutputStreamWriter fileVOSW = new OutputStreamWriter(new FileOutputStream(fileV), charset);
            final CSVPrinter fileVPrinter = new CSVPrinter(new BufferedWriter(fileVOSW),  CSVFormat.newFormat(';')
                    .withQuoteMode(QuoteMode.MINIMAL)
                    .withQuote(reclamoConfigDTO.getTextDelimiter())
                    .withRecordSeparator(reclamoConfigDTO.getRecordSeparator())) ;
            String element, line;
            int rows=0, i=0, totalSales=0;
            while (rows < fileMaxRows && (null != (line = bufferedReader.readLine())) )  {
                //leggiamo la linea e prepariamo array di input e output
                //line = bufferedReader.readLine();
                String[] elements = line.split(reclamoConfigDTO.getFieldSeparator());
                String[] dsrLine= new String[44];
                // mettiamo il valore trovato dal file csv di pochi campi dentro un array (con tanti campi quanti
                // un dsr normale) utilizzando l'indice delle colonne in configurazione.
                i = 0;


                while ( i < elements.length ) {
                    if ( null != reclamoConfigDTO.getColumnsIndexInput().get(i) ) {
                        dsrLine[reclamoConfigDTO.getColumnsIndexOutput().get(reclamoConfigDTO.getColumnsIndexInput().get(i))] = elements[i].trim().replaceAll(", ",",").replaceAll(" ,", ",");
                    }
                    i++;
                }
                dsrLine[reclamoConfigDTO.getColumnsIndexOutput().get("salesCount")] = dsrLine[reclamoConfigDTO.getColumnsIndexOutput().get("salesCount")].replaceAll("[^0-9+]", "");
                dsrLine[reclamoConfigDTO.getColumnsIndexOutput().get("artistRoles")] = dsrLine[reclamoConfigDTO.getColumnsIndexOutput().get("artist")].replaceAll("([a-zA-Z0-9 ]+)","Author");
                dsrLine[ reclamoConfigDTO.getColumnsIndexOutput().get("territory")]= (null != reclamoDTO.getCountry())? reclamoDTO.getCountry(): reclamoConfigDTO.getTerritory();

                dsrLine[ reclamoConfigDTO.getColumnsIndexOutput().get("currency")] = reclamoConfigDTO.getCurrency();
                dsrLine[ reclamoConfigDTO.getColumnsIndexOutput().get("rowNum")] = String.valueOf(rows+1);

                String salesCount = dsrLine[ reclamoConfigDTO.getColumnsIndexOutput().get("salesCount")];
                totalSales += (salesCount.matches("^[0-9]+$")) ? Integer.parseInt(salesCount) : 0 ;
                i=0;
                while (i < dsrLine.length ) {
                    element = dsrLine[i];
                    csvPrinter.print(element);
                    i++;
                }
                csvPrinter.println();
                rows++;
            }
            csvPrinter.close();

            fileVPrinter.print("0.001");
            fileVPrinter.print("0");
            fileVPrinter.print(totalSales);
            fileVPrinter.close();

            // @todo controllare che vada bene
            uploadFile(fileName, new FileInputStream(csvFile), this.s3, outputReclamiPath ,true);
            uploadFile(fileVName,new FileInputStream(fileV), this.s3, outputReclamiPath  ,true);
            csvFile.delete();
            fileV.delete();

        } catch (Exception e) {
            //logger.error("", e);
            e.printStackTrace();
        }
        // @todo controllare che non si voglia ritornare solo il nome del file
        return outputReclamiBasePath + "/" + fileName;
    }

    public String uploadFile(String fileName, InputStream is, S3 s3, String pathFileS3,Boolean cancella) {
        final File file = new File(tmpLocation+fileName);
        try {
            if (saveToFile(is, file)) {
                final AmazonS3URI s3Uri = new AmazonS3URI(pathFileS3 + file.getName());
                boolean uploadSuccessfull = s3.upload(new S3.Url(pathFileS3 + file.getName()),file);
                if (uploadSuccessfull) {
                    return s3Uri.getURI().getPath();
                }
            }
        } catch (Exception e) {
            log.error("Errore Upload File CSV to S3", e);
            throw e;
        } finally {
            if(cancella)
                file.delete();
        }
        return null;
    }



    private boolean saveToFileCompressed(InputStream in, File file) {
        OutputStream out = null;
        try {
            int count = 0;
            final byte[] bytes = new byte[1024];
            out = new CompressionAwareFileOutputStream(file);
            while (-1 != (count = in.read(bytes))) {
                out.write(bytes, 0, count);
            }
            out.close();
            out = null;
            return true;
        } catch (IOException e) {
            return false;
        } finally {
            if (null != out) {
                try {
                    out.close();
                } catch (IOException e) {
                }
            }
        }
    }

    private boolean saveToFile(InputStream in, File file) {
        OutputStream out = null;
        try {
            int count = 0;
            final byte[] bytes = new byte[1024];
            out = new FileOutputStream(file);
            while (-1 != (count = in.read(bytes))) {
                out.write(bytes, 0, count);
            }
            out.close();
            out = null;
            return true;
        } catch (IOException e) {
            return false;
        } finally {
            if (null != out) {
                try {
                    out.close();
                } catch (IOException e) {
                }
            }
        }
    }


    public InputStream  downloadInput(String fileUrl) throws FileNotFoundException{
        String fileUrlHttps = "s3://"+bucketS3+fileUrl;
        S3.Url s3url =new S3.Url(fileUrlHttps);
        final String s3FileName = fileUrlHttps.substring(1 + fileUrlHttps.lastIndexOf('/'));
        log.debug("S3 filename "+s3FileName);
        File fileOutput = new File(tmpLocation+s3FileName);

        s3.download(s3url,fileOutput);
        return new FileInputStream(fileOutput);
    }
}

