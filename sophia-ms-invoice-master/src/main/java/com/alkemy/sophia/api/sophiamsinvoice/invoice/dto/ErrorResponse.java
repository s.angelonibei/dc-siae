package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;

import lombok.Data;

import java.util.List;

@Data
public class ErrorResponse {
	private String errorMessage;
}
