/*
 * This file is generated by jOOQ.
 */
package com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.tables;


import com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.DefaultSchema;
import com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.Indexes;
import com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.Keys;
import com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.tables.records.SocietaTutelaRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.9"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class SocietaTutela extends TableImpl<SocietaTutelaRecord> {

    private static final long serialVersionUID = 368452006;

    /**
     * The reference instance of <code>SOCIETA_TUTELA</code>
     */
    public static final SocietaTutela SOCIETA_TUTELA = new SocietaTutela();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<SocietaTutelaRecord> getRecordType() {
        return SocietaTutelaRecord.class;
    }

    /**
     * The column <code>SOCIETA_TUTELA.ID</code>.
     */
    public final TableField<SocietaTutelaRecord, Integer> ID = createField("ID", org.jooq.impl.SQLDataType.INTEGER.nullable(false).identity(true), this, "");

    /**
     * The column <code>SOCIETA_TUTELA.CODICE</code>.
     */
    public final TableField<SocietaTutelaRecord, String> CODICE = createField("CODICE", org.jooq.impl.SQLDataType.CHAR(20).nullable(false), this, "");

    /**
     * The column <code>SOCIETA_TUTELA.NOMINATIVO</code>.
     */
    public final TableField<SocietaTutelaRecord, String> NOMINATIVO = createField("NOMINATIVO", org.jooq.impl.SQLDataType.CHAR(100).nullable(false), this, "");

    /**
     * The column <code>SOCIETA_TUTELA.HOME_TERRITORY</code>.
     */
    public final TableField<SocietaTutelaRecord, String> HOME_TERRITORY = createField("HOME_TERRITORY", org.jooq.impl.SQLDataType.CHAR(5).nullable(false), this, "");

    /**
     * The column <code>SOCIETA_TUTELA.POSIZIONE_SIAE</code>.
     */
    public final TableField<SocietaTutelaRecord, String> POSIZIONE_SIAE = createField("POSIZIONE_SIAE", org.jooq.impl.SQLDataType.CHAR(50).nullable(false), this, "");

    /**
     * The column <code>SOCIETA_TUTELA.CODICE_SIADA</code>.
     */
    public final TableField<SocietaTutelaRecord, String> CODICE_SIADA = createField("CODICE_SIADA", org.jooq.impl.SQLDataType.CHAR(50).nullable(false), this, "");

    /**
     * The column <code>SOCIETA_TUTELA.TENANT</code>.
     */
    public final TableField<SocietaTutelaRecord, Byte> TENANT = createField("TENANT", org.jooq.impl.SQLDataType.TINYINT.nullable(false), this, "");

    /**
     * Create a <code>SOCIETA_TUTELA</code> table reference
     */
    public SocietaTutela() {
        this(DSL.name("SOCIETA_TUTELA"), null);
    }

    /**
     * Create an aliased <code>SOCIETA_TUTELA</code> table reference
     */
    public SocietaTutela(String alias) {
        this(DSL.name(alias), SOCIETA_TUTELA);
    }

    /**
     * Create an aliased <code>SOCIETA_TUTELA</code> table reference
     */
    public SocietaTutela(Name alias) {
        this(alias, SOCIETA_TUTELA);
    }

    private SocietaTutela(Name alias, Table<SocietaTutelaRecord> aliased) {
        this(alias, aliased, null);
    }

    private SocietaTutela(Name alias, Table<SocietaTutelaRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> SocietaTutela(Table<O> child, ForeignKey<O, SocietaTutelaRecord> key) {
        super(child, key, SOCIETA_TUTELA);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return DefaultSchema.DEFAULT_SCHEMA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.SOCIETA_TUTELA_CODICE, Indexes.SOCIETA_TUTELA_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<SocietaTutelaRecord, Integer> getIdentity() {
        return Keys.IDENTITY_SOCIETA_TUTELA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<SocietaTutelaRecord> getPrimaryKey() {
        return Keys.KEY_SOCIETA_TUTELA_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<SocietaTutelaRecord>> getKeys() {
        return Arrays.<UniqueKey<SocietaTutelaRecord>>asList(Keys.KEY_SOCIETA_TUTELA_PRIMARY, Keys.KEY_SOCIETA_TUTELA_CODICE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SocietaTutela as(String alias) {
        return new SocietaTutela(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SocietaTutela as(Name alias) {
        return new SocietaTutela(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public SocietaTutela rename(String name) {
        return new SocietaTutela(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public SocietaTutela rename(Name name) {
        return new SocietaTutela(name, null);
    }
}
