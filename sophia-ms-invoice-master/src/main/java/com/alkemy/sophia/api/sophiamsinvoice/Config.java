package com.alkemy.sophia.api.sophiamsinvoice;


import java.util.Properties;

import com.amazonaws.services.sqs.AmazonSQSAsync;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.alkemy.sophia.api.sophiamsinvoice.invoice.dto.InvoiceConfiguration;
import com.alkemy.sophia.api.sophiamsinvoice.invoice.dto.ReclamoConfigDTO;
import com.alkemy.sophia.api.sophiamsinvoice.invoice.service.InvoiceTracer;
import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.invoice.integration.service.InvoiceServiceClientInterface;
import com.alkemytech.sophia.invoice.integration.service.SapPiInvoiceServiceClientImpl;

@Configuration
public class Config {
    @Bean
    public InvoiceServiceClientInterface getInvoiceServiceClient(@Autowired InvoiceTracer invoceTracer,
                                                                 @Value("${invoice.esbPath}") String esbPath,
                                                                 @Value("${invoice.https}") String https,
                                                                 @Value("${invoice.certificatePath}") String certificatePath,
                                                                 @Value("${invoice.trustStorePwd}") String trustStorePwd){
        SapPiInvoiceServiceClientImpl sapPiInvoiceServiceClientImpl = new SapPiInvoiceServiceClientImpl();


        com.alkemytech.sophia.invoice.integration.model.Configuration invoiceServiceConfiguration = new com.alkemytech.sophia.invoice.integration.model.Configuration();
        invoiceServiceConfiguration.setEsbPath(esbPath);
        invoiceServiceConfiguration.setHttps(Boolean.parseBoolean(https));
        invoiceServiceConfiguration.setCertificatePath(certificatePath);
        invoiceServiceConfiguration.setTrustStorePwd(trustStorePwd);
        sapPiInvoiceServiceClientImpl.init(invoiceServiceConfiguration, invoceTracer);
        return sapPiInvoiceServiceClientImpl;
    }

    @Bean
    public InvoiceConfiguration getInvoceConfig(){
        return new InvoiceConfiguration();
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Bean
    public S3 s3ServiceNew(@Value("${cloud.aws.credentials.pathConfigFile}") String pathConfigFile,
                           @Value("${cloud.aws.region.static}") String regionStatic
                                  ){
        Properties config = new Properties();
        config.put("aws.region",regionStatic);
        config.put("aws.credentials",pathConfigFile);
        S3 s3 = new S3(config);
        s3.startup();
        return s3;
    }
    @Bean
    public QueueMessagingTemplate queueMessagingTemplate(
            AmazonSQSAsync amazonSQSAsync) {
        return new QueueMessagingTemplate(amazonSQSAsync);
    }

    @Bean
    public ReclamoConfigDTO getReclamoConfigDTO() {
      return new ReclamoConfigDTO();
    }
}
