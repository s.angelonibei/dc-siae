package com.alkemy.sophia.api.sophiamsinvoice.invoice.rest;

import com.alkemy.sophia.api.sophiamsinvoice.invoice.dto.*;
import com.alkemy.sophia.api.sophiamsinvoice.entities.CCIDMetadata;
import com.alkemy.sophia.api.sophiamsinvoice.entities.InvoiceItemToCcid;
import com.alkemy.sophia.api.sophiamsinvoice.invoice.service.InvoiceService;
import com.alkemy.sophia.api.sophiamsinvoice.invoice.service.RipartizioneService;
import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.invoice.integration.model.InvoiceResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RequestMapping("invoice")
@RestController
@CommonsLog
@RequiredArgsConstructor
public class InvoiceRestService {
	private Gson gson = new GsonBuilder()
			.disableHtmlEscaping()
			.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
			.create();
	private final InvoiceService invoiceService;
	private final RipartizioneService ripartizioneService;
	private final S3 s3;
	@Value("${invoice.max-rows}")
	private int maxRows;

	@PostMapping(path = "all/ccid", produces = "application/json")
	public ResponseEntity allCCID(@RequestBody AvailableCCIDSearchParameters dto) {

		if(dto.getFatturato() == null)
			dto.setFatturato(false);

        List<InvoiceCCIDDTO> res;
        List<InvoiceCCIDDTO> dtoList = new ArrayList<>();
        if(dto.getIsBackclaim()!= null && dto.getIsBackclaim()){
            res = invoiceService.availableCCIDBackclaim(dto);
        } else {
            res = invoiceService.availableCCID(dto);
        }
		PagedResult pagedResult = null;
        if(dto.getCurrentPage() == null){
			pagedResult = new PagedResult();
			if (null != res) {
				final int maxrows = dto.getLast() - dto.getFirst();
				final boolean hasNext = res.size() > maxrows;
				for (InvoiceCCIDDTO o : (hasNext ? res.subList(0, maxrows) : res)) {
					dtoList.add(o);
				}

				pagedResult.setRows(dtoList).setMaxrows(maxrows).setFirst(dto.getFirst())
						.setLast(hasNext ? dto.getLast() : dto.getFirst() + res.size()).setHasNext(hasNext).setHasPrev(dto.getFirst() > 0);

			} else {
				pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
			}
		}else{
			pagedResult = new PagedResult(res,getMaxRows(),dto.getCurrentPage());
		}

		return ResponseEntity.ok(gson.toJson(pagedResult));
	}


	@PostMapping(path = "all/ccidReclami", produces = "application/json")
	public ResponseEntity allCCIDReclami(@RequestBody AvailableCCIDSearchParameters dto) {

		if(dto.getFatturato() == null)
			dto.setFatturato(false);
//		List<InvoiceCCIDDTO> res = invoiceService.payableCCID(dto);
		List<InvoiceCCIDDTO> res = invoiceService.availableCCIDReclami(dto);
		final PagedResult pagedResult = new PagedResult();
		if (!res.isEmpty()) {
			int maxrows = ReportPage.NUMBER_RECORD_PER_PAGE;
			boolean hasNext = res.size() >= maxrows;
			pagedResult.setRows(res).setMaxrows(maxrows).setFirst(dto.getFirst())
					.setLast(hasNext ? dto.getLast() : dto.getFirst() + res.size()).setHasNext(hasNext)
					.setHasPrev(dto.getFirst() > 0);
		} else {
			pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
		}
		return ResponseEntity.ok(gson.toJson(pagedResult));
	}

	@PostMapping(path = "all/ccidFattura", produces = "application/json")
	public ResponseEntity allCCIDFattura(@RequestBody AvailableCCIDSearchParameters dto) {
		if(dto.getMaxRows() == null)
			dto.setMaxRows(getMaxRows());
		if(dto.getCurrentPage() == null)
			dto.setCurrentPage(0);

		List<InvoiceCCIDDTO> res = invoiceService.availableCCIDRipatizione(dto);
		Integer maxRows;
		Integer currentPage;

		if (dto.getPagination().equals(false)) { // return all result
			maxRows = res.size();
			currentPage = 0;
		} else {
			maxRows = dto.getMaxRows()!= null ? dto.getMaxRows() : getMaxRows();
			currentPage = dto.getCurrentPage();
		}
		PagedResult pagedResult = new PagedResult(res, maxRows, currentPage);

		return ResponseEntity.ok(gson.toJson(pagedResult));
	}


	@PostMapping(path = "genera-carico-ripartizione", produces = "application/json")
	public ResponseEntity generaCaricoRipartizione(@RequestBody CaricoRipartizioneDTO dto){
		Assert.notEmpty(dto.getCcidList(),"Nessun CCID selezionato");

		for (InvoiceCCIDDTO ccid : dto.getCcidList()) {
			Assert.notNull(ccid.getIdCcidInvoiceItem(),"ID invoice item non valorizzato");
		}


		ripartizioneService.generaCarico(dto);

		return ResponseEntity.ok().build();
	}


	@PostMapping(path = "carico/list", produces = "application/json")
	public ResponseEntity listaCarico(@RequestBody RequestListaCarichiDTO dto){
		if(dto.getMaxRows()== null)
			dto.setMaxRows(getMaxRows());
		if(dto.getCurrentPage()== null)
			dto.setCurrentPage(0);

//
		List<CaricoRipartizioneDettaglioDTO> res = ripartizioneService.listaCarichi(dto);

		PagedResult pagedResult = new PagedResult(res,dto.getMaxRows(),dto.getCurrentPage());

		return ResponseEntity.ok(gson.toJson(pagedResult));

	}

	@GetMapping(path = "carico/{idCarico}", produces = "text/csv")
	public void caricoCsvById(@PathVariable("idCarico") Long idCarico, HttpServletResponse response) {

		//query per prendere i dati

		List<CaricoCsvDTO> result = ripartizioneService.caricoCsv(idCarico);
		if(!CollectionUtils.isEmpty(result)) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			response.addHeader("Content-disposition", "attachment;filename=Carico"+result.get(0).getIdRipartizioneSiada()+ "_"+sdf.format(new Date())+".csv");
			response.setContentType("text/csv");
		}

		try {
			PrintWriter pw = new PrintWriter(response.getOutputStream());
			//for list concatena stringhe con ;

			String separator = " ; ";
			String[] strs = {"ID RIPARTIZIONE SIADA",
					 		 "VERSIONE",
					 		 "DSP",
							 "PERIODO",
							"TERRITORIO",
							"TIPO UTILIZZAZIONE",
							"OFFERTA COMMERCIALE",
							"DSR",
							"VALORE TOTALE CCID",
							"NUMERO FATTURA",
							"DATA FATTURA",
							"TOTALE FATTURA",
							"VALORE PARZIALE",
							"RIPARTITO"
			};

			for (String str : strs) {
				pw.print(str+separator);
			}
			pw.println();

			for (CaricoCsvDTO c : result) {
				pw.println(
						c.getIdRipartizioneSiada()+separator+
						c.getVersione()+separator+
						c.getIddsp()+separator+
						c.getPeriodString()+separator+
						c.getCountry()+separator+
						c.getUtilizationType()+separator+
						c.getCommercialOffer()+separator+
						c.getIdDsr()+separator+
						c.getValoreFatturabile()+separator+
						c.getNumeroFattura()+separator+
						c.getDateOfPertinence()+separator+
						c.getTotalInvoice()+separator+
						c.getValoreFattura()+separator+
						((c.getPeriodoRipartizioneId()== null) ? "NO" : "SI") +separator
				);
			}

			//push della stringa sulla response;

			pw.flush();
		}catch (Exception e){
			log.error("Errore nell'invio del pdf ",e);
			throw new RuntimeException("File non pronto");
		}

	}

    @PostMapping(path = "avvia-ripartizione/{idCarico}", produces = "application/json")
    public ResponseEntity avviaRipartizione(@PathVariable("idCarico") Long idCarico){

	    ripartizioneService.avviaRipartizione(idCarico);

//		ripartizioneService.parametersQuery("SIAE","Report_YouTube_bmat_IT_201801_20180214062650_masterlist");
	    return ResponseEntity.ok().build();
    }

    @GetMapping(path = "ripartizione/all", produces = "application/json")
    public ResponseEntity statisticaRipartizione(){

        return ResponseEntity.ok(ripartizioneService.getAllPeriodi());
    }

    @PostMapping(path = "statistica-ripartizione", produces = "application/json")
    public ResponseEntity statisticaRipartizione(@RequestBody StatisticaRipartizioneRequestDTO dto){
		List<StatisticaRipartizioneDTO> res = ripartizioneService.statisticheRipartizione(dto);
		if (!dto.getPaginationActive()) {
			return ResponseEntity.ok(res);
		}
		PagedResult pagedResult = new PagedResult(res,dto.getMaxRows(),dto.getCurrentPage());

        return ResponseEntity.ok(pagedResult);
    }

	@GetMapping(path = "download648/{id}", produces = "application/octet-stream")
	public void download648(@PathVariable("id") Long id, HttpServletResponse response) {

		Assert.notNull(id,"id 648 non valorizzato");
		S3.Url urlS3 = ripartizioneService.get648UrlS3(id);



		response.addHeader("Content-disposition", "attachment");
		response.setContentType("application/octet-stream");

		try {
			s3.download(urlS3,response.getOutputStream());
			response.flushBuffer();

		}catch (Exception e){
			log.error("Errore nell'invio del 648 ",e);

			throw new RuntimeException("Impossibile scaricare il file 648 per id:"+id);
		}


	}

	@PostMapping(path = "all/updateccid", produces = "application/json")
	public ResponseEntity updateCcid(@RequestBody UpdateCcidParameter dto) {

		// boolean res = invoiceService.updateCCid(dto);
		CCIDMetadata ccidMetadata = invoiceService.getCcidMetadata(dto);

		// if(ccidMetadata != null && ccidMetadata.getTotalValue() != null &&
		// (ccidMetadata.getTotalValue().compareTo( new BigDecimal(
		// dto.getInvoiceAmaunt() ) ) <= 0) ){
		// return Response.status(500).entity("{\"message\":\"Il nuovo Valore
		// fatturabile deve essere minore o uguale al valore corrente\"}").build();
		// }

		List<InvoiceItemToCcid> listJoinAnticipi = invoiceService.getInvoiceItemToCcid(ccidMetadata);
		if (listJoinAnticipi.size() != 0) {
			return ResponseEntity.status(500).body(
					"{\"message\":\"Il valore fatturabile non può essere aggiornato. Il CCID è collegato a uno o piu anticipi\"}");
		}

		if (invoiceService.updateCCid(dto)) {
			return ResponseEntity.ok(gson.toJson(true));
		} else {
			return ResponseEntity.ok(gson.toJson(new ArrayList<InvoiceCCIDDTO>()));
		}
	}


	@PostMapping(path = "all/invoice", produces = "application/json")
	public ResponseEntity getInvoices(@RequestBody InvoiceSearchParameters searchParameters) {

		List<InvoiceDTO> result = invoiceService.searchInvoice(searchParameters);
		final PagedResult pagedResult = new PagedResult(result,maxRows,searchParameters.getCurrentPage());
		return ResponseEntity.ok(gson.toJson(pagedResult));

	}


	@GetMapping(path = "configuration", produces = "application/json")
	public ResponseEntity<InvoiceConfiguration> getConfiguration() {

		try {
			InvoiceConfiguration result = invoiceService.getConfiguration();
			if (result != null) {
				return ResponseEntity.ok(result);
			}
		} catch (Exception e) {
			log.error("invoice configuration", e);
		}
		return ResponseEntity.status(500).build();
	}


	@GetMapping(path = "itemReason/all", produces = "application/json")
	public ResponseEntity<List<InvoiceItemReasonDTO>> getItemReasons() {

		try {
			List<InvoiceItemReasonDTO> result = invoiceService.getInvoiceItemReasons();
			if (result != null) {
				return ResponseEntity.ok(result);
			}
		} catch (Exception e) {
			log.error("list itemReason", e);
		}
		return ResponseEntity.status(500).build();
	}


	@GetMapping(path = "requestInvoice", produces = "application/json")
	public ResponseEntity requestInvoice(@RequestParam( name="invoiceId",required = false) String invoiceId ) {
		if(!StringUtils.hasLength(invoiceId))
			invoiceId="0";
		InvoiceResponse response = invoiceService.callInvoiceService(Integer.parseInt(invoiceId));
		if (response != null) {
			return ResponseEntity.ok(response);
		}

		return ResponseEntity.status(500).build();
	}


	@PostMapping(path = "saveInvoiceDraft", produces = "application/json")
	public ResponseEntity saveInvoiceDraft(@RequestBody InvoiceDraftDto dto) {
		InvoiceDTO result = invoiceService.newInvoice(dto);
		if (result != null) {
			return ResponseEntity.ok(result);
		}

		return ResponseEntity.status(500).build();

	}


	@DeleteMapping(path = "deleteInvoice")
	public ResponseEntity delete(@RequestBody InvoiceDTO invoiceDTO) {
		try {
			invoiceService.removeInvoice(invoiceDTO.getIdInvoice(),true);
			return ResponseEntity.status(200).build();
		} catch (Exception e) {
			return ResponseEntity.status(500).build();
		}
	}


	@DeleteMapping(path = "")
	public ResponseEntity deleteInvoice(@RequestBody InvoiceDTO invoiceDTO) {
		try {
			invoiceService.removeInvoiceBozza(invoiceDTO.getIdInvoice());
			return ResponseEntity.status(200).build();
		} catch (Exception e) {
			return ResponseEntity.status(500).build();
		}
	}

	@GetMapping(path = "all/items", produces = "application/json")
	public ResponseEntity getItems(@RequestParam( name="invoiceId") String invoiceId ) {
		try {
			List<InvoiceItemDto> result = invoiceService.getInvoiceItems(Integer.parseInt(invoiceId));
			if (result != null) {
				return ResponseEntity.ok(result);
			}
		} catch (Exception e) {
			log.error("list invoice items", e);
		}
		return ResponseEntity.status(500).build();
	}


	@GetMapping(path = "clientData", produces = "application/json")
	public ResponseEntity clientData(@RequestParam String client ) {

		if(!StringUtils.hasLength(client))
			client = "";
		try {
			AnagClientDTO result = invoiceService.getAnagClient(client);
			if (result != null) {
				return ResponseEntity.ok(result);
			}
		} catch (Exception e) {
			log.error("clientData", e);
		}
		return ResponseEntity.status(500).build();
	}

	@GetMapping(path = "", produces = "application/json")
	public ResponseEntity getInvoice(@RequestParam Integer invoiceId) {
		try {
			InvoiceDTO result = invoiceService.getInvoice(invoiceId);
			if (result != null) {
				return ResponseEntity.ok(result);
			}
		} catch (Exception e) {
			log.error("getInvoice", e);
		}
		return ResponseEntity.status(500).build();
	}

	@GetMapping(path = "checkInvoice", produces = "application/json")
	public ResponseEntity checkInvoice(@RequestParam Integer invoiceId) {
		try {
			InvoiceDTO result = invoiceService.checkInvoice(invoiceId);
			if (result != null) {
				return ResponseEntity.ok(result);
			}
		} catch (Exception e) {
			log.error("getInvoice", e);
		}
		return ResponseEntity.status(500).build();
	}


	@GetMapping(path = "download/{invoiceId}", produces = "application/octet-stream")
	public void downloadById(@PathVariable("invoiceId") Integer invoiceId, HttpServletResponse response) {
		byte[] blob = invoiceService.downloadInvoice(invoiceId);
		if (blob != null) {
			response.addHeader("Content-disposition", "attachment");
			response.setContentType("application/octet-stream");

			try {
				response.getOutputStream().write(blob);

				response.flushBuffer();
			}catch (Exception e){log.error("Errore nell'invio del pdf ",e);}

		}
	}

	@GetMapping(path = "max-rows")
	@ResponseStatus(HttpStatus.OK)
	public Integer getMaxRows() {
		return maxRows;
	}


	public static void main(String[] args) {

		ArrayList<Integer> list = new ArrayList<Integer>(Arrays.asList(1,7,2,4));
		int n = 3;






	}
}
