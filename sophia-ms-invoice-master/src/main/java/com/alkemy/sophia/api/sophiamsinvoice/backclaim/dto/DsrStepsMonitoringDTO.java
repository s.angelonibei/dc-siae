package com.alkemy.sophia.api.sophiamsinvoice.backclaim.dto;

import lombok.Data;

@Data
public class DsrStepsMonitoringDTO {
    private Integer status;
    private Integer extractStatus;
    private Integer cleanStatus;
    private Integer priceStatus;
    private Integer hypercubeStatus;
    private Integer identifyStatus;
    private Integer uniloadStatus;
    private Integer claimStatus;
}
