package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;

import java.util.List;

public class PagedResult {

	private Object extra;
	private List<?> rows;
	private Integer first;
	private Integer last;
	private Integer maxrows;
	private Boolean hasNext;
	private Boolean hasPrev;
	private Integer currentPage;

	public PagedResult() {
	}

	public PagedResult(List<?> result, Integer maxrows, Integer currentPage) {
		this.maxrows = maxrows;
		if (null != result) {
			boolean hasNext = result.size() > maxrows;
			this.setRows(hasNext ? result.subList(0, maxrows) : result)
					.setMaxrows(maxrows)
					.setHasNext(hasNext)
					.setHasPrev(currentPage > 0)
					.setCurrentPage(currentPage);
		} else {
			this.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
		}
	}

	public Integer getCurrentPage() {
		return currentPage;
	}

	public PagedResult setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
		return this;
	}

	public Object getExtra() {
		return extra;
	}
	public PagedResult setExtra(Object extra) {
		this.extra = extra;
		return this;
	}
	public List<?> getRows() {
		return rows;
	}
	public PagedResult setRows(List<?> rows) {
		this.rows = rows;
		return this;
	}
	public Integer getFirst() {
		return first;
	}
	public PagedResult setFirst(Integer first) {
		this.first = first;
		return this;
	}
	public Integer getLast() {
		return last;
	}
	public PagedResult setLast(Integer last) {
		this.last = last;
		return this;
	}
	public Integer getMaxrows() {
		return maxrows;
	}
	public PagedResult setMaxrows(Integer maxrows) {
		this.maxrows = maxrows;
		return this;
	}
	public Boolean getHasNext() {
		return hasNext;
	}
	public PagedResult setHasNext(Boolean hasNext) {
		this.hasNext = hasNext;
		return this;
	}
	public Boolean getHasPrev() {
		return hasPrev;
	}
	public PagedResult setHasPrev(Boolean hasPrev) {
		this.hasPrev = hasPrev;
		return this;
	}

}
