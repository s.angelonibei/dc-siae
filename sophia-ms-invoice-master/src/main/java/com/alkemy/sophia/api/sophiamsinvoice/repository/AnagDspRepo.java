package com.alkemy.sophia.api.sophiamsinvoice.repository;

import com.alkemy.sophia.api.sophiamsinvoice.entities.AnagDsp;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnagDspRepo extends JpaRepository<AnagDsp, String> {
}
