package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;

import com.alkemy.sophia.api.sophiamsinvoice.entities.DettailImportiAnticipo;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

@Data
@NoArgsConstructor
public class DettailImportiAnticipoDTO {

	private Integer idDettailImportiAnticipo;
	private Integer idImportoAnticipo;
	private Date periodoPertinenzaInizio;
	private Date periodoPertinenzaFine;
	private String numeroFattura;
	private BigDecimal importoOriginale;
	private BigDecimal importoUtilizzabile;
//	private String idDsp;
	private Integer idJoinAnticipiCcid;
	private String idCcid;
	private BigDecimal quotaAnticipo;
    private BigInteger idCCIDMetadata;
    private String idDsr;
    private BigDecimal totalValue;
    private String currency;
    private String ccidVersion;
    private Boolean ccidEncoded;
    private Boolean ccidEncodedProvisional;
    private Boolean ccidNotEncoded;
    private Integer invoiceItem;
    private String invoiceStatus;
    private BigDecimal valoreFatturabile;
    
    
    
	public DettailImportiAnticipoDTO(DettailImportiAnticipo entity) {
		
		init(entity);
		
	}
	

    
	private void init(DettailImportiAnticipo entity) {
		this.idDettailImportiAnticipo= entity.getIdDettailImportiAnticipo();
		this.idImportoAnticipo = entity.getIdImportoAnticipo();
		this.periodoPertinenzaInizio = entity.getPeriodoPertinenzaInizio();
		this.periodoPertinenzaFine = entity.getPeriodoPertinenzaFine();
		this.numeroFattura = entity.getNumeroFattura();
		this.importoOriginale = entity.getImportoOriginale();
		this.importoUtilizzabile = entity.getImportoUtilizzabile();
//		this.idDsp = entity.getIdDsp();
		this.idJoinAnticipiCcid = entity.getIdJoinAnticipiCcid();
		this.idCcid = entity.getIdCcid();
		this.quotaAnticipo = entity.getQuotaAnticipo();
		this.idCCIDMetadata = entity.getIdCCIDMetadata();
		this.idDsr = entity.getIdDsr();
		this.totalValue = entity.getTotalValue();
		this.currency = entity.getCurrency();
		this.ccidVersion = entity.getCcidVersion();
		this.ccidEncoded = entity.getCcidEncoded();
		this.ccidEncodedProvisional = entity.getCcidEncodedProvisional();
		this.ccidNotEncoded = entity.getCcidNotEncoded();
		this.invoiceItem = entity.getInvoiceItem();
		this.invoiceStatus = entity.getInvoiceStatus();
		this.valoreFatturabile = entity.getValoreFatturabile();
		
	}
    
}
