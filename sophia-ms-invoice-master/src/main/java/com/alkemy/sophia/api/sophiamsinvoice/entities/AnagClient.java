package com.alkemy.sophia.api.sophiamsinvoice.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "ANAG_CLIENT")
@Data
@EqualsAndHashCode
public class AnagClient  {

    @Id
    @Column(name = "ID_ANAG_CLIENT", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idAnagClient;

    @Column(name = "CODE", nullable = false)
    private String code;

    @Column(name = "COMPANY_NAME")
    private String companyName;

    @Column(name = "COUNTRY")
    private String country;

    @Column(name = "VAT_CODE")
    private String vatCode;

    @Column(name = "VAT_DESCRIPTION")
    private String vatDescription;

    @Column(name = "FOREIGN_CLIENT")
    private Boolean foreignClient;

    @OneToMany(mappedBy = "anagClient")
    private List<ClientToDsp> clientToDspList;

    @OneToMany
    private List<Invoice> invoices;


}
 
