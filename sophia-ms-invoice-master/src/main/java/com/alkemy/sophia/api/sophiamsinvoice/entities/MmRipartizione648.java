package com.alkemy.sophia.api.sophiamsinvoice.entities;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "MM_RIPARTIZIONE_648")
public class MmRipartizione648 {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "ID_RIPARTIZIONE_SIADA")
    private Integer idRipartizioneSiada;
    @Column(name = "DATA_ORA_CREAZIONE")
    private Timestamp dataOraCreazione;
    @Column(name = "VALORE_LORDO")
    private BigDecimal valoreLordo;
    @Column(name = "VALORE_AGGIO_DEM")
    private BigDecimal valoreAggioDem;
    @Column(name = "VALORE_AGGIO_DRM")
    private BigDecimal valoreAggioDrm;
    @Column(name = "VALORE_TRATTENUTA")
    private BigDecimal valoreTrattenuta;
    @Column(name = "VALORE_NETTO")
    private BigDecimal valoreNetto;
    @Column(name = "PATH_S3_648")
    private String pathS3648;

    @Column(name = "NUMERO_FATTURA")
    private Long  idInvoice;


    @Column(name = "ID_SOCIETA_TUTELA")
    private Integer idSocietaTutela;

    @Column(name = "ID_CCID_METADATA")
    private String idDsr;

    @Transient
    private Timestamp dateOfPertinence;
    @Transient
    private BigDecimal valoreDrm;
    @Transient
    private BigDecimal valoreDem;
    @Transient
    private String society;

    @Transient
    private boolean nuovoFlusso;

}
