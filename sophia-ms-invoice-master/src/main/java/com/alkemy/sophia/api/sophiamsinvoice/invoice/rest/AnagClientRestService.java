package com.alkemy.sophia.api.sophiamsinvoice.invoice.rest;

import com.alkemy.sophia.api.sophiamsinvoice.invoice.dto.AnagClientDTO;
import com.alkemy.sophia.api.sophiamsinvoice.invoice.service.AnagClientService;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;



@RequestMapping("anagClient")
@RestController
@CommonsLog
@RequiredArgsConstructor
public class AnagClientRestService {

	private final AnagClientService anagClientService;

	@GetMapping(path={"clients","clients/{id}"},produces = "application/json")
	public ResponseEntity getAllClient(@PathVariable( name="id",required = false) Integer idClient){
		return ResponseEntity.ok(anagClientService.getAllSimpleClient(idClient));
	}


	@GetMapping(path = "all", produces = "application/json")
	public ResponseEntity all(
			@RequestParam( name="first",required = false) Integer first,
			@RequestParam( name="last",required = false) Integer last,
			@RequestParam( name="clientId",required = false) Integer clientId,
			@RequestParam( name="dsp",required = false) String dsp,
			@RequestParam( name="country",required = false) String country,
			@RequestParam( name="sapCode",required = false) String sapCode) {

		try{
			first = (first != null)? first : 0;
			last = (last != null)? last : 50;

			return ResponseEntity.ok(anagClientService.getFilterClient(first,last,clientId,dsp,country,sapCode));
		} catch (Exception e) {
			log.error("all", e);
		}
		return ResponseEntity.status(500).build();
	}

	@PostMapping(path = "", produces = "application/json")
	public ResponseEntity add(@RequestBody AnagClientDTO dto) {

		try {
			anagClientService.insertClient(dto);
			return ResponseEntity.ok().build();

		} catch (Exception ex) {
			log.error("insertClient error ", ex);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

	}

	@PutMapping(path = "", produces = "application/json")
	@Transactional
	public ResponseEntity update(@RequestBody AnagClientDTO dto) {

		try {
			anagClientService.updateClientToDsp(dto);

			return ResponseEntity.ok().build();

		} catch (Exception e) {
			log.error("update error ", e);
		}
		return ResponseEntity.status(500).build();
	}


	@DeleteMapping(path = "", produces = "application/json")
	public ResponseEntity delete(@RequestParam(name="idClientDsp") int idClientDsp) {

		try {

			anagClientService.removeClientToDsp(idClientDsp);
			return ResponseEntity.ok().build();

		} catch (Exception e) {
			log.error("delete error", e);
		}

		return ResponseEntity.status(500).build();
	}

}
