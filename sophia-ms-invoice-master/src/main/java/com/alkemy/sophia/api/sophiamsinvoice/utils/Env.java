package com.alkemy.sophia.api.sophiamsinvoice.utils;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
public class Env {
    @Value("${default.environment}")
    public String environment;
}
