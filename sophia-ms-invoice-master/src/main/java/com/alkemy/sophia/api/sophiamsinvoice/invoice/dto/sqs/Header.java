package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto.sqs;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class Header {

    private String queue;
    private Long timestamp;
    private String uuid;
    private String sender;
    private Map<String, Object> additionalProperties = new HashMap<>();

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
