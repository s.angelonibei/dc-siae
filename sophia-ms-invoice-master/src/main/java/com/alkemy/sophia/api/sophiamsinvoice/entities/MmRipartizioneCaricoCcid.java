package com.alkemy.sophia.api.sophiamsinvoice.entities;

import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@Table(name = "MM_RIPARTIZIONE_CARICO_CCID")
public class MmRipartizioneCaricoCcid {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    //aggiungere FK
    @ManyToOne
    @JoinColumn(name="ID_RIPARTIZIONE_CARICO", referencedColumnName = "ID")
    private MmRipartizioneCarico ripartizioneCarico;

    @ManyToOne
    @JoinColumn(name="ID_INVOICE_ITEM_TO_CCID", referencedColumnName = "id")
    private InvoiceItemToCcid invoiceItemToCcid;


}
