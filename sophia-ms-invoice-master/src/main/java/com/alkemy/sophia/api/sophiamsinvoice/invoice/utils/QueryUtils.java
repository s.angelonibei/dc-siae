package com.alkemy.sophia.api.sophiamsinvoice.invoice.utils;


import java.sql.Date;
import java.util.List;


public class QueryUtils {

    public static String getValuesCollection(List<String> list) {
        String result = "";
        String valueSeparator = " , ";
        for (String e : list) {
            result = result.concat("\"").concat(e).concat("\"").concat(valueSeparator);
        }
        result = result.substring(0, result.lastIndexOf(valueSeparator));

        return result;
    }

//    public static Field<String> dateFormat(Field<Date> field, String format) {
//        return field("date_format({0}, {1})", SQLDataType.VARCHAR,
//                field,
//                inline(format));    }
}
