package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;


import com.alkemy.sophia.api.sophiamsinvoice.entities.*;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class InvoiceDTO {

    private Integer idInvoice;
    private String invoiceCode;
    private Date creationDate;
    private Date lastUpdateDate;
    private String userID;
    private BigDecimal total;
    private Float vat;
    private String note;
    private String status;
    private String pdfPath;
    private List<InvoiceItemDto> invoiceItems = new ArrayList<InvoiceItemDto>();
    private DspDTO dsp;
    private AnagClientDTO clientData;
    private List<InvoiceAccantonamento> listInvoiceAccantonamento;


    public InvoiceDTO() {

    }

    public InvoiceDTO(Invoice invoice, AnagClient anagClient) {
        this.idInvoice = invoice.getIdInvoice();
        this.invoiceCode = invoice.getInvoiceCode();

        for (InvoiceItem item : invoice.getInvoiceItems()) {
            this.invoiceItems.add(new InvoiceItemDto(item));
        }

        this.note = invoice.getNote();
        this.status = invoice.getStatus();
        this.total = invoice.getTotal();
        this.userID = invoice.getUserID();
        this.vat = invoice.getVat();
        this.creationDate = invoice.getCreationDate();
        this.lastUpdateDate = invoice.getLastUpdateDate();
        this.clientData = new AnagClientDTO(anagClient);
        this.listInvoiceAccantonamento = invoice.getListInvoiceAccantonamento();
    }


}
