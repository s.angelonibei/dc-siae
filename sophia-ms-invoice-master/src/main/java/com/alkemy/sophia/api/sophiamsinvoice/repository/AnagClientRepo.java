package com.alkemy.sophia.api.sophiamsinvoice.repository;

import com.alkemy.sophia.api.sophiamsinvoice.entities.AnagClient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AnagClientRepo extends JpaRepository<AnagClient, Integer> {
    List<AnagClient> findAllByOrderByCompanyName();
    List<AnagClient> findAllByIdAnagClientOrderByCompanyName(Integer idAnag);
}
