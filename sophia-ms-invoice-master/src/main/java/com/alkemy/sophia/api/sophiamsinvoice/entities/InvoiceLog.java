package com.alkemy.sophia.api.sophiamsinvoice.entities;

import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement
@Entity(name = "InvoiceLog")
@Table(name = "INVOICE_LOG")
@NamedQueries({ @NamedQuery(name = "InvoiceLog.GetAll", query = "SELECT x FROM InvoiceLog x") })
@SuppressWarnings("serial")
@EqualsAndHashCode
public class InvoiceLog  {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_INVOICE_LOG", nullable = false, unique = true)
	private Integer idInvoiceLog;

	@Column(name = "REQUEST_ID", nullable = false)
	private String requestId;

	@Column(name = "REQUEST_PARAMS", nullable = false)
	private String requestParams;

	@Column(name = "RESPONSE_PARAMS", nullable = true)
	private String responseParams;

	@Column(name = "START_DATE", nullable = false)
	private Date startDate;

	@Column(name = "DURATION", nullable = true)
	private Long duration;
	
	@Column(name = "ID_INVOICE", nullable = false)
	private Integer idInvoice;
	

	public String getId() {
		return idInvoiceLog.toString();
	}

	public Integer getIdInvoiceLog() {
		return idInvoiceLog;
	}

	public void setIdInvoiceLog(Integer idInvoiceLog) {
		this.idInvoiceLog = idInvoiceLog;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getRequestParams() {
		return requestParams;
	}

	public void setRequestParams(String requestParams) {
		this.requestParams = requestParams;
	}

	public String getResponseParams() {
		return responseParams;
	}

	public void setResponseParams(String responseParams) {
		this.responseParams = responseParams;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public Integer getIdInvoice() {
		return idInvoice;
	}

	public void setIdInvoice(Integer idInvoice) {
		this.idInvoice = idInvoice;
	}

}
