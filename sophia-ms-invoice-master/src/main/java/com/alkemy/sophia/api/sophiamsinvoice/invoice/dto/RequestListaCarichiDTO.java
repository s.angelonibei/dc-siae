package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;


import com.alkemy.sophia.api.sophiamsinvoice.entities.MmRipartizioneCarico;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class RequestListaCarichiDTO extends BaseRequestDTO{
  private int year;
  private int month;
  private MmRipartizioneCarico.STATO stato;
  private String idRipartizioneSiada;

}
