package com.alkemy.sophia.api.sophiamsinvoice.utils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class StringUtils {
    public static String getHash(String originalString) throws NoSuchAlgorithmException {
        return StringUtils.bytesToHex(MessageDigest.getInstance("SHA-256").digest(originalString.getBytes(StandardCharsets.UTF_8)));
    }

    private StringUtils(){}

    private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder();
        for (byte b : hash) {
            String hex = Integer.toHexString(0xff & b);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }
}
