package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;

import com.alkemy.sophia.api.sophiamsinvoice.entities.ImportiAnticipo;
import lombok.Data;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;

@Data
public class ImportiAnticipoDTO {

	private Integer idImportoAnticipo;
	private String periodFrom;
	private String periodTo;
	private String invoiceNumber;
	private BigDecimal originalAmount;
	private BigDecimal amountUsed;
	private String note;
	private BigDecimal percentuale;
	private BigDecimal importoAccantonato;
	private BigDecimal importoAccantonatoUtilizzabile;
	private BigDecimal importoUtilizzatoReclami;
	private BigDecimal ccidSum;
	private Integer idAnagClient;
//	private String idDSP;
	
	public ImportiAnticipoDTO(){
		
	}
	
	public ImportiAnticipoDTO(Object[] obj ){
		configure(obj);
	}
	
	public ImportiAnticipoDTO(ImportiAnticipo entity ){
		init( entity );
	}

	public void configure( Object[] obj ){
		
	}
	
	public void init(ImportiAnticipo entity ){
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM-yyyy");
		
		this.idImportoAnticipo = entity.getIdImportoAnticipo();
		this.periodFrom = dateFormat.format( entity.getPeriodoPertinenzaInizio() );
		this.periodTo = dateFormat.format( entity.getPeriodoPertinenzaFine() );
		this.invoiceNumber = entity.getNumeroFattura();
		this.originalAmount = entity.getImportoOriginale();
		this.amountUsed = entity.getImportoUtilizzabile();
		this.note = entity.getNote();
		this.percentuale = entity.getPercentualeAccantonamento();
		this.importoAccantonato = entity.getImportoAccantonato();
		this.importoAccantonatoUtilizzabile = entity.getImportoAccantonatoUtilizzabile();
//		this.idDSP = entity.getIdDsp();
	
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ImportiAnticipoDTO [idImportoAnticipo=");
		builder.append(idImportoAnticipo);
		builder.append(", periodFrom=");
		builder.append(periodFrom);
		builder.append(", periodTo=");
		builder.append(periodTo);
		builder.append(", invoiceNumber=");
		builder.append(invoiceNumber);
		builder.append(", originalAmount=");
		builder.append(originalAmount);
		builder.append(", amountUsed=");
		builder.append(amountUsed);
//		builder.append(", idDSP=");
//		builder.append(idDSP);
		builder.append("]");
		return builder.toString();
	}
	
}