package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;

import com.alkemy.sophia.api.sophiamsinvoice.entities.AnagClient;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
public class 	AnagClientDTO  extends BaseAnagClientDTO{

	private String idDsp;
	private String dspName;
	private Integer IdClientDsp;

	private String country;
	private String vatCode;
	private String licence;
	private String vatDescription;
	private Boolean foreignClient;
	private Date startDate;
	private Date endDate;

	private Map<String,String> dspLicenceMap ;



	public AnagClientDTO(AnagClient entity) {

		this.idAnagClient = entity.getIdAnagClient();
		this.code = entity.getCode();
//		this.idDsp = entity.getIdDsp();
		this.companyName = entity.getCompanyName();
		this.country = entity.getCountry();
		this.vatCode = entity.getVatCode();
//		this.licence = entity.getLicence();
		this.vatDescription = entity.getVatDescription();
		this.foreignClient = entity.getForeignClient();
//		this.startDate = entity.getStartDate();
//		this.endDate = entity.getEndDate();

	}

	public AnagClientDTO(AnagClient entity,boolean withDspLicence) {

		this.idAnagClient = entity.getIdAnagClient();
		this.code = entity.getCode();
//		this.idDsp = entity.getIdDsp();
		this.companyName = entity.getCompanyName();
		this.country = entity.getCountry();
		this.vatCode = entity.getVatCode();
//		this.licence = entity.getLicence();
		this.vatDescription = entity.getVatDescription();
		this.foreignClient = entity.getForeignClient();
//		this.startDate = entity.getStartDate();
//		this.endDate = entity.getEndDate();

		this.dspList = new ArrayList<>();

		if(withDspLicence){
			dspLicenceMap = new HashMap<String,String>();
			entity.getClientToDspList().forEach(dsp -> dspLicenceMap.put(dsp.getAnagDsp().getCode(),dsp.getLicence()));

			entity.getClientToDspList().stream()
					.filter(dsp -> dsp.getEndDate() == null)
					.forEach(dsp -> this.dspList.add(new DspDTO(dsp.getAnagDsp())));

		}

	}


}
