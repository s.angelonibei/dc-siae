package com.alkemy.sophia.api.sophiamsinvoice.repository;

import com.alkemy.sophia.api.sophiamsinvoice.entities.ClientToDsp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface ClientToDspRepo extends JpaRepository<ClientToDsp, Integer> {

    @Modifying
    @Query("update ClientToDsp dsp set dsp.endDate = CURRENT_DATE where dsp.id = :id")
    void setEndDateToClientToDsp(Integer id);

}
