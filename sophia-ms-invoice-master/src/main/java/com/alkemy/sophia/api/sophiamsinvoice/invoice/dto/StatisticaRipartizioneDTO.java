package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;



import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;

@Data
@RequiredArgsConstructor
public class StatisticaRipartizioneDTO{

    private Long id;
    private Long idRipartizioneSiada;
    private String nominativo;
    private String idDsr;
    private String numeroFattura;
    private BigDecimal valoreLordo;
    private BigDecimal valoreAggioDem;
    private BigDecimal valoreAggioDrm;
    private BigDecimal valoreTrattenuta;
    private BigDecimal valoreNetto;
    private BigDecimal valoreNettoDem;
    private BigDecimal valoreNettoDrm;
    private String dspName;


}
