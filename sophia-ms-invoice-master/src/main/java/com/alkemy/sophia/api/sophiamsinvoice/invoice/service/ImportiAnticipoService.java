package com.alkemy.sophia.api.sophiamsinvoice.invoice.service;

import com.alkemy.sophia.api.sophiamsinvoice.invoice.dto.*;
import com.alkemy.sophia.api.sophiamsinvoice.entities.*;
import com.alkemy.sophia.api.sophiamsinvoice.repository.*;
import com.alkemy.sophia.api.sophiamsinvoice.utils.InvoiceStatus;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.jooq.Condition;
import org.jooq.Converter;
import org.jooq.DSLContext;
import org.jooq.DataType;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.Predicate;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static com.alkemy.sophia.api.sophiamsinvoice.dbmodel.jooq.Tables.*;
import static org.jooq.impl.DSL.field;

@Service
@RequiredArgsConstructor
@CommonsLog
public class ImportiAnticipoService {

    private final DSLContext dsl;
    private final AnagClientRepo anagClientRepo;
    private final AnagDspRepo anagDspRepo;
    private final ClientToDspRepo clientToDspRepo;
    private final CCIDMetadataRepo ccidMetadataRepo;
    private final ImportiAnticipoRepo importiAnticipoRepo;
    private final InvoiceItemRepo invoiceItemRepo;
    private final InvoiceItemToCcidRepo invoiceItemToCcidRepo;
    private final InvoiceRepo invoiceRepo;
    @Value("${invoice.max-rows}")
    private int maxRows;

    //un po brutto
    private final InvoiceService invoiceService;

    public static String CHIUSURA="CHIUSURA";
    public static String RIPROPORZIONAMENTO="RIPROPORZIONAMENTO";
    public static String RECLAMI="RECLAMI";



    public PagedResult getFilteranticipi(SearchImportiAnticipo dataDTO) {

        Condition condition = DSL.trueCondition();

        if(StringUtils.hasLength(dataDTO.getDsp()))
            condition = condition.and(ANAG_DSP.IDDSP.eq(dataDTO.getDsp()));

        if(dataDTO.getIdClient() != null)
            condition = condition.and(ANAG_CLIENT.ID_ANAG_CLIENT.eq(dataDTO.getIdClient()));

        if(dataDTO.getYearFrom()!= null && dataDTO.getMonthFrom() != null && dataDTO.getYearFrom() != 0 && dataDTO.getMonthFrom() != 0){
            LocalDate localDateFrom = LocalDate.of(dataDTO.getYearFrom(), dataDTO.getMonthFrom(), 1);
            condition = condition.and(IMPORTI_ANTICIPO.PERIODO_PERTINENZA_INIZIO.greaterOrEqual(Timestamp.valueOf(localDateFrom.atStartOfDay())));
        }

        if(dataDTO.getYearTo()!= null && dataDTO.getMonthTo() != null && dataDTO.getYearTo() != 0 && dataDTO.getMonthTo() != 0){
            LocalDate localDateTo = LocalDate.of(dataDTO.getYearTo(), dataDTO.getMonthTo(), 1);
            localDateTo = localDateTo.withDayOfMonth(localDateTo.lengthOfMonth());
            condition = condition.and(IMPORTI_ANTICIPO.PERIODO_PERTINENZA_FINE.lessOrEqual(Timestamp.valueOf(localDateTo.atStartOfDay())));
        }

        if(dataDTO.getAdvancePaymentCode() != null)
            condition = condition.and(IMPORTI_ANTICIPO.NUMERO_FATTURA.eq(dataDTO.getAdvancePaymentCode()));

        if(dataDTO.getShowStandBy()!= null)
            if(dataDTO.getShowStandBy())
                condition = condition.and(IMPORTI_ANTICIPO.IMPORTO_UTILIZZABILE.gt(BigDecimal.ZERO));
            else
                condition = condition.and(IMPORTI_ANTICIPO.IMPORTO_UTILIZZABILE.lessOrEqual(BigDecimal.ZERO));

        Converter<Timestamp, String> converter =
                Converter.of(
                        Timestamp.class,
                        String.class,
                        t -> t == null ? null : t.toLocalDateTime().format(DateTimeFormatter.ofPattern("MM-yyyy")),
                        u -> u == null ? null : Timestamp.valueOf(LocalDateTime.parse(u, DateTimeFormatter.ofPattern("MM-yyyy")))
                );

        DataType<String> DATE_TYPE = SQLDataType.TIMESTAMP.asConvertedDataType(converter);

                List<ImportiAnticipoDTO> result = dsl.selectDistinct(
                IMPORTI_ANTICIPO.ID_IMPORTO_ANTICIPO,
                field(IMPORTI_ANTICIPO.PERIODO_PERTINENZA_INIZIO.getName(),DATE_TYPE).as("PERIOD_FROM"),
                field(IMPORTI_ANTICIPO.PERIODO_PERTINENZA_FINE.getName(),DATE_TYPE).as("PERIOD_TO"),
                IMPORTI_ANTICIPO.NUMERO_FATTURA.as("INVOICE_NUMBER"),
                IMPORTI_ANTICIPO.IMPORTO_ORIGINALE.as("ORIGINAL_AMOUNT"),
                IMPORTI_ANTICIPO.IMPORTO_UTILIZZABILE.as("AMOUNT_USED"),
                IMPORTI_ANTICIPO.IMPORTO_ACCANTONATO,
                IMPORTI_ANTICIPO.IMPORTO_ACCANTONATO_UTILIZZABILE,
                IMPORTI_ANTICIPO.PERCENTUALE_ACCANTONAMENTO.as("PERCENTUALE"),
                IMPORTI_ANTICIPO.NOTE
        ).from(IMPORTI_ANTICIPO)
                .join(INVOICE).on(INVOICE.ID_INVOICE.eq(IMPORTI_ANTICIPO.ID_INVOICE))
                .join(ANAG_CLIENT).on(ANAG_CLIENT.ID_ANAG_CLIENT.eq(INVOICE.CLIENT_ID))
                .join(CLIENT_TO_DSP).on(ANAG_CLIENT.ID_ANAG_CLIENT.eq(CLIENT_TO_DSP.ANAG_CLIENT_ID))
                .join(ANAG_DSP).on(CLIENT_TO_DSP.IDDSP.eq(ANAG_DSP.IDDSP))
                .where(condition).limit(maxRows+1)
                        .offset(dataDTO.getCurrentPage()*maxRows).fetch().into(ImportiAnticipoDTO.class);

        final PagedResult pagedResult = new PagedResult(result,maxRows,dataDTO.getCurrentPage());
//        private Integer idImportoAnticipo;
//        private String periodFrom;
//        private String periodTo;
//        private String invoiceNumber;
//        private BigDecimal originalAmount;
//        private BigDecimal amountUsed;
//        private String note;
 //       private BigDecimal percentuale;
  //      private BigDecimal importoAccantonato;
    //    private BigDecimal importoAccantonatoUtilizzabile;

//        final PagedResult pagedResult = new PagedResult();
//        if (null != result) {
//            boolean hasNext = result.size() > maxrows;
//            pagedResult.setRows(hasNext ? result.subList(0, maxrows) : result)
//                    .setMaxrows(maxrows)
//                    .setHasNext(hasNext)
//                    .setHasPrev(dataDTO.getCurrentPage() > 0)
//                    .setCurrentPage(dataDTO.getCurrentPage());
//        } else {
//            pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
//        }

        return pagedResult;
    }


    @Transactional
    public void riproporziona(UsaAnticipoDTO usaAnticipoDTO) throws Exception{

        List<Long> idCCid = new ArrayList<>();

        usaAnticipoDTO.getCcidList().forEach(c -> idCCid.add(c.getIdCCIDmetadata()));

        List<CCIDMetadata> ccidMetadataList = ccidMetadataRepo.findAllByIdCCIDMetadataIn(idCCid);

        //select all ccid in id dto
        BigDecimal totalAmountCcid = new BigDecimal(BigInteger.ZERO);
        for (CCIDMetadata ccidMetadata : ccidMetadataList) {
            //check if residio = 0 for all
            if(ccidMetadata.getValoreResiduo().compareTo(BigDecimal.ZERO) != 0)
                throw new Exception("Il residuo dei ccid selezionati deve essere ZERO");

            //calcola il totale di riproporzionare
            totalAmountCcid = totalAmountCcid.add(ccidMetadata.getValoreFatturabile());


        }

        ImportiAnticipo importiAnticipo = importiAnticipoRepo.findById(usaAnticipoDTO.getIdAdvancePayment().getIdImportoAnticipo()).get();
        Invoice invoice = invoiceRepo.findById(importiAnticipo.getIdInvoice()).get();

        BigDecimal partAmount = importiAnticipo.getImportoUtilizzabile().divide(totalAmountCcid,12, RoundingMode.HALF_EVEN);

        List<InvoiceItemToCcid> itemList = new ArrayList<>();
        for (CCIDMetadata ccidMetadata : ccidMetadataList) {

            BigDecimal valoreDaAggiungere = ccidMetadata.getValoreFatturabile().multiply(partAmount);
            BigDecimal valoreFinale = ccidMetadata.getValoreFatturabile().add(valoreDaAggiungere);

            log.info("riproporziona - idAnticipo:" + importiAnticipo.getIdImportoAnticipo() + " parte utilizzabile:"+importiAnticipo.getImportoUtilizzabile()
                + " CCID:"+ccidMetadata.getIdCcid() + " valore iniziale:"+ccidMetadata.getValoreFatturabile() + " valore da aggiungere:"+ valoreDaAggiungere);
            //il valore
            ccidMetadata.setValoreFatturabile(valoreFinale);
            ccidMetadata.setInvoiceStatus("FATTURATO");
            ccidMetadata.setValoreResiduo(BigDecimal.ZERO);

            InvoiceItemToCcid invoiceItemToCcid = new InvoiceItemToCcid();
            invoiceItemToCcid.setOperazione(RIPROPORZIONAMENTO);
            invoiceItemToCcid.setCcidMedata(ccidMetadata);
            //questa cosa
            for (InvoiceItem item : invoice.getInvoiceItems()) {
                invoiceItemToCcid.setInvoiceItem(item);
            }
            invoiceItemToCcid.setValore(valoreDaAggiungere);
            itemList.add(invoiceItemToCcid);

            ccidMetadataRepo.save(ccidMetadata);


            //devo salvare i valori di dettaglio del ccid
        }

        invoiceItemToCcidRepo.saveAll(itemList);

        importiAnticipo.setImportoUtilizzabile(BigDecimal.ZERO);
        importiAnticipoRepo.save(importiAnticipo);

        invoice.setLastUpdateDate(new Date());
        invoiceRepo.save(invoice);

        //salvo il dettaggio legato al ccid

        //aumento il valore fatturabile
    }

    public UsaAnticipoDTO riproporzionaFake(UsaAnticipoDTO usaAnticipoDTO) throws Exception{

        if(CollectionUtils.isEmpty(usaAnticipoDTO.getCcidList()))
            return usaAnticipoDTO;

        BigDecimal totalAmountCcid = new BigDecimal(BigInteger.ZERO);
        for (CcidInfoDTO ccidMetadata : usaAnticipoDTO.getCcidList()) {
            //check if residio = 0 for all
            if(ccidMetadata.getValoreResiduo().compareTo(BigDecimal.ZERO) != 0)
                throw new Exception("Il residuo dei ccid selezionati deve essere ZERO");

            //calcola il totale di riproporzionare
            totalAmountCcid = totalAmountCcid.add(ccidMetadata.getInvoiceAmount());


        }

        BigDecimal partAmount = usaAnticipoDTO.getIdAdvancePayment().getAmountUsed().divide(totalAmountCcid,12, RoundingMode.HALF_EVEN);

        List<InvoiceItemToCcid> itemList = new ArrayList<>();
        for (CcidInfoDTO ccidMetadata : usaAnticipoDTO.getCcidList()) {

            BigDecimal valoreDaAggiungere = ccidMetadata.getInvoiceAmount().multiply(partAmount);
            BigDecimal valoreFinale = ccidMetadata.getInvoiceAmount().add(valoreDaAggiungere);

            log.info("fake riproporziona - idAnticipo:" + usaAnticipoDTO.getIdAdvancePayment().getIdImportoAnticipo() + " parte utilizzabile:"+usaAnticipoDTO.getIdAdvancePayment().getAmountUsed()
                    + " CCID:"+ccidMetadata.getIdCCID() + " valore iniziale:"+ccidMetadata.getInvoiceAmount() + " valore da aggiungere:"+ valoreDaAggiungere);
            ccidMetadata.setInvoiceAmountNew(valoreDaAggiungere);

        }

        usaAnticipoDTO.getIdAdvancePayment().setCcidSum(totalAmountCcid);

        return usaAnticipoDTO;
    }

    @Transactional
    public void chiudiAnticipo(UsaAnticipoDTO usaAnticipoDTO)  throws Exception{

        List<Long> idCCid = new ArrayList<>();


        usaAnticipoDTO.getCcidList().forEach(c -> idCCid.add(c.getIdCCIDmetadata()));

        List<CCIDMetadata> ccidMetadataList = ccidMetadataRepo.findAllByIdCCIDMetadataIn(idCCid);

        //select all ccid in id dto
        BigDecimal totalRestAmountCcid = new BigDecimal(BigInteger.ZERO);
        for (CCIDMetadata ccidMetadata : ccidMetadataList) {
            //check if residio > 0 for all
            if(ccidMetadata.getValoreResiduo().compareTo(BigDecimal.ZERO) == 0)
                throw new Exception("Il residuo dei ccid selezionati deve essere diverso da ZERO");

            //calcola il totale di riproporzionare
            totalRestAmountCcid = totalRestAmountCcid.add(ccidMetadata.getValoreResiduo());


        }

        ImportiAnticipo importiAnticipo = importiAnticipoRepo.findById(usaAnticipoDTO.getIdAdvancePayment().getIdImportoAnticipo()).get();
        Invoice invoice = invoiceRepo.findById(importiAnticipo.getIdInvoice()).get();

        BigDecimal partAmount = importiAnticipo.getImportoUtilizzabile().divide(totalRestAmountCcid,12, RoundingMode.HALF_EVEN);

        List<InvoiceItemToCcid> itemList = new ArrayList<>();
        for (CCIDMetadata ccidMetadata : ccidMetadataList) {


            BigDecimal valoreResiduoRiproporzionato = ccidMetadata.getValoreResiduo().multiply(partAmount);


            //formula da sottrarre o aggiugnere
            //Z=Z - (X-(X*Y))
            //Z = valore fatturabile
            //X = valore residuo
            //Y = proporzione partAmount
            BigDecimal subToValoreFatturabile = ccidMetadata.getValoreResiduo().subtract(valoreResiduoRiproporzionato);

            ccidMetadata.setValoreFatturabile(ccidMetadata.getValoreFatturabile().subtract(subToValoreFatturabile));
            ccidMetadata.setInvoiceStatus("FATTURATO");
            ccidMetadata.setValoreResiduo(BigDecimal.ZERO);


            InvoiceItemToCcid invoiceItemToCcid = new InvoiceItemToCcid();

            invoiceItemToCcid.setOperazione(CHIUSURA);
            invoiceItemToCcid.setCcidMedata(ccidMetadata);
            //questa cosa
            for (InvoiceItem item : invoice.getInvoiceItems()) {
                invoiceItemToCcid.setInvoiceItem(item);
            }
            invoiceItemToCcid.setValore(valoreResiduoRiproporzionato);
            itemList.add(invoiceItemToCcid);

            ccidMetadataRepo.save(ccidMetadata);


            //devo salvare i valori di dettaglio del ccid
        }

        invoiceItemToCcidRepo.saveAll(itemList);

        importiAnticipo.setImportoUtilizzabile(BigDecimal.ZERO);
        importiAnticipoRepo.save(importiAnticipo);

        invoice.setLastUpdateDate(new Date());
        invoiceRepo.save(invoice);



        //select all ccid in id dto



        //calcola il totale da riproporzionare

            //if maggiore del totale dei ccid aggiungi importo
            //aumento il valore fatturabile

            //else tolgo l'importo dal valore fatturabile
            //diminuisco il valore fatturabile

        //salvo il dettaglio legato al ccid
        //porto il residuo sempre a zerro


    }

    public UsaAnticipoDTO chiudiAnticipoFake(UsaAnticipoDTO usaAnticipoDTO)  throws Exception{

        if(CollectionUtils.isEmpty(usaAnticipoDTO.getCcidList()))
            return usaAnticipoDTO;

        //select all ccid in id dto
        BigDecimal totalRestAmountCcid = new BigDecimal(BigInteger.ZERO);
        for (CcidInfoDTO ccidMetadata : usaAnticipoDTO.getCcidList()) {
            //check if residio > 0 for all
            if(ccidMetadata.getValoreResiduo().compareTo(BigDecimal.ZERO) == 0)
                throw new Exception("Il residuo dei ccid selezionati deve essere diverso da ZERO");

            //calcola il totale di riproporzionare
            totalRestAmountCcid = totalRestAmountCcid.add(ccidMetadata.getValoreResiduo());


        }



        BigDecimal partAmount = usaAnticipoDTO.getIdAdvancePayment().getAmountUsed().divide(totalRestAmountCcid,12, RoundingMode.HALF_EVEN);

        List<InvoiceItemToCcid> itemList = new ArrayList<>();
        for (CcidInfoDTO ccidMetadata : usaAnticipoDTO.getCcidList()) {


            BigDecimal valoreResiduoRiproporzionato = ccidMetadata.getValoreResiduo().multiply(partAmount);


            //formula da sottrarre o aggiugnere
            //Z=Z - (X-(X*Y))
            //Z = valore fatturabile
            //X = valore residuo
            //Y = proporzione partAmount
            BigDecimal subToValoreFatturabile = ccidMetadata.getValoreResiduo().subtract(valoreResiduoRiproporzionato);

            ccidMetadata.setInvoiceAmountNew(valoreResiduoRiproporzionato);



        }
        usaAnticipoDTO.getIdAdvancePayment().setCcidSum(totalRestAmountCcid);

        return usaAnticipoDTO;
    }




    @Transactional
    public void removeAnticipo(ImportiAnticipoDTO anticipoDTO) throws RuntimeException {
        //recupera l'anticipo

        ImportiAnticipo anticipo = importiAnticipoRepo.findById(anticipoDTO.getIdImportoAnticipo()).get();
        importiAnticipoRepo.delete(anticipo);

        Invoice invoice = invoiceRepo.findById(anticipo.getIdInvoice()).get();

        for (InvoiceItem invoiceItem : invoice.getInvoiceItems()) {
            for (InvoiceItemToCcid invoiceItemToCcid : invoiceItem.getInvoiceItemToCcid()) {
                if(invoiceItemToCcid.getOperazione() != null &&
                        (invoiceItemToCcid.getOperazione().equalsIgnoreCase(CHIUSURA) || invoiceItemToCcid.getOperazione().equalsIgnoreCase(RECLAMI)))
                    throw new RuntimeException("Anticipo utilizzato in chiusura o per reclami impossibile eliminare");
            }
        }

        for (InvoiceItem invoiceItem : invoice.getInvoiceItems()) {
            for (InvoiceItemToCcid invoiceItemToCcid : invoiceItem.getInvoiceItemToCcid()) {
                if (invoiceItemToCcid.getOperazione() != null && invoiceItemToCcid.getOperazione().equalsIgnoreCase(RIPROPORZIONAMENTO)) {
                    CCIDMetadata ccidMetadata = invoiceItemToCcid.getCcidMedata();

                    BigDecimal quota = invoiceItemToCcid.getValore();
                    ccidMetadata.setValoreFatturabile(ccidMetadata.getValoreFatturabile().subtract(quota));
                    ccidMetadataRepo.save(ccidMetadata);
                } else if (invoiceItemToCcid.getOperazione() != null && invoiceItemToCcid.getOperazione().equalsIgnoreCase(CHIUSURA)) {
                    throw new RuntimeException("NOOOO");
                } else {

                    CCIDMetadata ccidMetadata = invoiceItemToCcid.getCcidMedata();

                    BigDecimal quota = invoiceItemToCcid.getValore();
                    BigDecimal valoreResiduo = ccidMetadata.getValoreResiduo();
                    ccidMetadata.setValoreResiduo(valoreResiduo.add(quota));
                    ccidMetadata.setInvoiceStatus(InvoiceStatus.DA_FATTURARE.toString());

                    ccidMetadataRepo.save(ccidMetadata);
                }
            }
        }
        invoiceService.removeInvoice(anticipo.getIdInvoice(),false);



    }


    @Transactional
    public void useReclamoPayment(UsaAnticipoDTO dataDTO) throws Exception {

        List<ImportiAnticipo> resultQuery = null;
        List<CCIDMetadata> resultQueryCcidMetadata = new ArrayList<>();
        List<Predicate> predicates = new ArrayList<Predicate>();
        ImportiAnticipoDTO anticipo = dataDTO.getIdAdvancePayment();
        List<Long> idCCid = new ArrayList<>();
        dataDTO.getCcidList().forEach(c -> idCCid.add(c.getIdCCIDmetadata()));


        BigDecimal newUsedVAlue = null;
        BigDecimal ccidResidualValue = null;
        Map<BigInteger, BigDecimal> listValoreFatturabile = new HashMap<BigInteger, BigDecimal>();

        ImportiAnticipo importiAnticipo = dsl.selectFrom(IMPORTI_ANTICIPO).where(IMPORTI_ANTICIPO.ID_IMPORTO_ANTICIPO.eq(anticipo.getIdImportoAnticipo())).fetchOne().into(ImportiAnticipo.class);

        Invoice invoice = dsl.selectFrom(INVOICE).where(INVOICE.ID_INVOICE.eq(importiAnticipo.getIdInvoice())).fetchOne().into(Invoice.class);

        List<InvoiceItem> invoiceItemList =dsl.selectFrom(INVOICE_ITEM).where(INVOICE_ITEM.ID_INVOICE.eq(importiAnticipo.getIdInvoice())).fetch().into(InvoiceItem.class);


        List<CCIDMetadata> ccidMetadataList = ccidMetadataRepo.findAllByIdCCIDMetadataIn(idCCid);


        BigDecimal importoAccantonatoUtilizzabile = importiAnticipo.getImportoAccantonatoUtilizzabile();
        BigDecimal percentuale = importiAnticipo.getPercentualeAccantonamento();
        BigDecimal importoAccantonato = importiAnticipo.getImportoAccantonato();


        if(importoAccantonatoUtilizzabile == null || percentuale == null ||importoAccantonato == null ){
            throw new Exception("nessun accantonamento sulla fattura");
        }

        BigDecimal importoUtilizzatoReclami = importiAnticipo.getImportoUtilizzatoReclami()!= null ? importiAnticipo.getImportoUtilizzatoReclami() : new BigDecimal(0);

        BigDecimal importoAccantonatoUtilizzabileReclami = importoAccantonato.multiply(percentuale).divide(new BigDecimal(100));


        log.info("importoAccantonatoUtilizzabile:" + importoAccantonatoUtilizzabile);
        log.info("importoUtilizzatoReclami:" + importoUtilizzatoReclami);
        log.info("percentuale:" + percentuale);
        log.info("importoAccantonato:" + importoAccantonato);



        BigDecimal importoCcid = new BigDecimal(0);
        for (CCIDMetadata ccidMetadata : ccidMetadataList) {

            importoCcid = importoCcid.add(ccidMetadata.getValoreResiduo()!=null ?  ccidMetadata.getValoreResiduo() : new BigDecimal(0));
        }

        if(importoCcid.add(importoUtilizzatoReclami).compareTo(importoAccantonatoUtilizzabileReclami)>0){
           throw new Exception("Il residuo disponibile per i reclami non è sufficiente");
        }

        log.info("importoCcid:" + importoCcid);
        log.info("importoCcid + importo utilizzato per reclami:" + importoCcid.add(importoUtilizzatoReclami)    );


        importiAnticipo.setImportoAccantonatoUtilizzabile(importoAccantonatoUtilizzabile.subtract(importoCcid));
        importiAnticipo.setImportoUtilizzatoReclami(importoCcid.add(importoUtilizzatoReclami));
        //importiAnticipo.setImportoAccantonatoUtilizzabile(importiAnticipo.getImportoAccantonatoUtilizzabile().subtract(importoCcid));


        List<InvoiceItemToCcid> itemList = new ArrayList<>();
        for (CCIDMetadata ccidMetadata : ccidMetadataList) {

            ccidMetadata.setInvoiceStatus("FATTURATO");
            ccidMetadata.setValoreResiduo(BigDecimal.ZERO);

            InvoiceItemToCcid invoiceItemToCcid = new InvoiceItemToCcid();
            invoiceItemToCcid.setOperazione(RECLAMI);
            invoiceItemToCcid.setCcidMedata(ccidMetadata);

            invoiceItemToCcid.setValore(ccidMetadata.getValoreFatturabile());

            //questa cosa ci piace Marco?
            for (InvoiceItem invoiceItem : invoiceItemList) {
                invoiceItemToCcid.setInvoiceItem(invoiceItem);
            }

            itemList.add(invoiceItemToCcid);
        }
        importiAnticipoRepo.save(importiAnticipo);
        ccidMetadataRepo.saveAll(ccidMetadataList);
        invoiceItemToCcidRepo.saveAll(itemList);
    }
}
