package com.alkemy.sophia.api.sophiamsinvoice.invoice.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.math.BigInteger;


@Data
public class CcidInfoDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Boolean ccidEncoded;
	private Boolean ccidEncodedProvisional;
	private Boolean ccidNotEncoded;
	private String ccidVersion;
	private String commercialOffer;
	private String country;
	private String currency;
	private String dspName;
	private String idCCID;
	private Long idCCIDmetadata;
	private Integer idCommercialOffer;
	private String idDsp;
	private String idDsr;
	private String idUtilizationType;
	private String invoiceStatus;
	private Integer period;
	private String periodString;
	private String periodType;
	private BigDecimal totalValue;
	private BigDecimal totalValueOrigCurrency;
	private String utilizationType;
	private Integer year;
	private BigDecimal invoiceAmount;
	private BigDecimal valoreResiduo;

	private BigDecimal invoiceAmountNew;
	private Boolean  ripartito;


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CcidInfoDTO [ccidEncoded=");
		builder.append(ccidEncoded);
		builder.append(", ccidEncodedProvisional=");
		builder.append(ccidEncodedProvisional);
		builder.append(", ccidNotEncoded=");
		builder.append(ccidNotEncoded);
		builder.append(", ccidVersion=");
		builder.append(ccidVersion);
		builder.append(", commercialOffer=");
		builder.append(commercialOffer);
		builder.append(", country=");
		builder.append(country);
		builder.append(", currency=");
		builder.append(currency);
		builder.append(", dspName=");
		builder.append(dspName);
		builder.append(", idCCID=");
		builder.append(idCCID);
		builder.append(", idCCIDmetadata=");
		builder.append(idCCIDmetadata);
		builder.append(", idCommercialOffer=");
		builder.append(idCommercialOffer);
		builder.append(", idDsp=");
		builder.append(idDsp);
		builder.append(", idDsr=");
		builder.append(idDsr);
		builder.append(", idUtilizationType=");
		builder.append(idUtilizationType);
		builder.append(", invoiceStatus=");
		builder.append(invoiceStatus);
		builder.append(", period=");
		builder.append(period);
		builder.append(", periodString=");
		builder.append(periodString);
		builder.append(", periodType=");
		builder.append(periodType);
		builder.append(", totalValue=");
		builder.append(totalValue);
		builder.append(", totalValueOrigCurrency=");
		builder.append(totalValueOrigCurrency);
		builder.append(", utilizationType=");
		builder.append(utilizationType);
		builder.append(", year=");
		builder.append(year);
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
