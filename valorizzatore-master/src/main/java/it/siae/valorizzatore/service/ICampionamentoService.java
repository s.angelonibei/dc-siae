package it.siae.valorizzatore.service;

import it.siae.valorizzatore.model.CampionamentoConfig;
import it.siae.valorizzatore.model.security.ReportPage;
import it.siae.valorizzatore.model.security.User;

import java.util.List;

/**
 * Created by idilello on 6/21/16.
 */
public interface ICampionamentoService {

    public void insertParametriCampionamento(String inizioPeriodoContabile, String dataInizioLavorazione, String dataEstrazioneLotto,
                                             String bsm1, String bsm2, String bsm3, String bsm4, String bsm5,
                                             String concertini1, String concertini2, String concertini3, String concertini4, String concertini5,
                                             String resto5bsm, String resto5concertini, String resto61bsm, String resto61concertini,User user);

    public ReportPage getCampionamenti(Long contabilitaIniziale, Long contabilitaFinale, Integer page);

    public CampionamentoConfig getCampionamentoConfigById(Long idCampionamentoConfig);

    public ReportPage getStoricoEsecuzioni(Long periodoIniziale, Long periodoFinale, Integer page);

}
