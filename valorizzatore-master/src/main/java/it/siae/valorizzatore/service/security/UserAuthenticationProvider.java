package it.siae.valorizzatore.service.security;

import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.BindingProvider;

import it.siae.valorizzatore.dao.IConfigurationDAO;
import it.siae.valorizzatore.integration.sso.AuthenticationService;
import it.siae.valorizzatore.integration.sso.AuthenticationServiceSoap;
import it.siae.valorizzatore.model.security.*;
import it.siae.valorizzatore.service.IConfigurationService;
import it.siae.valorizzatore.service.ITraceService;
import it.siae.valorizzatore.utility.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import it.siae.valorizzatore.utility.Utility;
import it.siae.valorizzatore.dao.security.IUserDAO;


@Component
public class UserAuthenticationProvider implements AuthenticationProvider, Serializable{
 
    @Autowired
    private IUserDAO userDAO;

    @Autowired
    @Qualifier("configurationService")
    IConfigurationService configurationService;

    @Autowired
    private ITraceService traceService;

    private boolean validatePassword(String password, String userPassword) {
        String encodedPassword= Utility.encodePassword(password);
        boolean correctPassword=encodedPassword.equals(userPassword);
        return correctPassword;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest currentRequest =  attr.getRequest();
        
        Authentication userAuthenticated = null;
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

        String username = "";
        String password = "";
        
        if (currentRequest.getSession().getAttribute("userAuthenticated")==null){

            User user = null;

            String sessionToken = currentRequest.getParameter("SessionID");

            // Autenticazione da SSO Intranet
            if (sessionToken != null){

                username = checkToken(sessionToken);

                traceService.trace("Autenticazione", this.getClass().getName(), "authenticate", "checkToken: "+username, Constants.INFO);

                user = userDAO.getUser(username);

                if (user!=null)
                 user.setSesisonToken(sessionToken);

            } else {
                // Autenticazione da pagina di login applicativa

                username=(String) authentication.getPrincipal();

                password=(String) authentication.getCredentials();

                user = userDAO.getUser(username, Utility.encodePassword(password));

            }

            if (user != null){

                 authorities.add(new SimpleGrantedAuthority("ROLE_VAL"));

                 UserAuthenticatedProfile principal = new UserAuthenticatedProfile(username, password, authorities);

                 userAuthenticated = new UserAuthenticated(principal, password);

                 userAuthenticated.setAuthenticated(true);

                 currentRequest.getSession().setAttribute("userAuthenticated", userAuthenticated);
                 
                 currentRequest.getSession().setAttribute("user", user);

                 traceService.trace("Autenticazione", this.getClass().getName(), "authenticate", "LoggedIn", Constants.INFO);

            }

        } else{

            userAuthenticated = (Authentication) currentRequest.getSession().getAttribute("userAuthenticated");

        }

        return userAuthenticated;

    }


    public String checkToken(String token){

        String hostAddress = null;
        String username = null;

        try {

            hostAddress = InetAddress.getLocalHost().getHostAddress();

            // Se non è in esecuzione su localhost verifica se il token è valido
            //if (InetAddress.getLocalHost().getHostAddress() != null && !InetAddress.getLocalHost().getHostAddress().equals("127.0.0.1") && !InetAddress.getLocalHost().getHostAddress().equals("127.0.1.1")) {

                String ssoEndpointUrl = configurationService.getParameter("SSO_ENDPOINT");
                String applicationCode = configurationService.getParameter("SSO_APPLICATION_CODE");

                AuthenticationService service = new AuthenticationService();
                AuthenticationServiceSoap serviceStub = service.getAuthenticationServiceSoap();

                BindingProvider bp = (BindingProvider)serviceStub;

                bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, ssoEndpointUrl);

                //UniqueLoginResponse response = serviceStub.loginSSO("CRM_Coordinatore_URP","CRM_Coordinatore_URP_pass","127.0.0.1");
                //EsitiAutenticazione esito = response.getEsito();
                //String token = response.getToken();
                //System.out.println("Esito: "+esito.toString());
                //System.out.println("Token: "+token);

                username = serviceStub.checkToken(token, applicationCode, hostAddress);

            //}

        }catch(Exception e){
            e.printStackTrace();

            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);

            traceService.trace("Ricalcolo", this.getClass().getName(), "checkToken", sw.toString(), Constants.ERROR);

        }


        return username;
    }

    @Override
    public boolean supports(Class<?extends Object> authentication) {
        return true;
    }

}
