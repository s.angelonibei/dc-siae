package it.siae.valorizzatore.service;

import it.siae.valorizzatore.dao.ICampionamentoDAO;
import it.siae.valorizzatore.dao.IEsecuzioneDAO;
import it.siae.valorizzatore.model.TracciamentoApplicativo;
import it.siae.valorizzatore.model.security.User;
import it.siae.valorizzatore.utility.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;

@Service("traceService")
public class TraceService implements ITraceService {

    @Autowired
    @Qualifier("esecuzioneDAO")
    IEsecuzioneDAO esecuzioneDAO;

    public void trace(String serviceName, String className, String methodName, String message, String logLevel){

        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest currentRequest =  attr.getRequest();
        User user = (User)currentRequest.getSession().getAttribute("user");

        // Utente Guest
        if (user==null){
            user=new User();

            user.setId(-1L);
            user.setUsername("GUEST");
        }

        String hostName = " ";

        try {

            hostName = InetAddress.getLocalHost().getHostName();

        }catch(UnknownHostException e){
            e.printStackTrace();
        }

        TracciamentoApplicativo tracciamentoApplicativo = new TracciamentoApplicativo();

        tracciamentoApplicativo.setApplication(Constants.APPID);


        tracciamentoApplicativo.setDataInserimento(new Date());
        tracciamentoApplicativo.setUserName(user.getUsername());
        tracciamentoApplicativo.setHostName(hostName);
        tracciamentoApplicativo.setSessionId(currentRequest.getSession().getId());

        tracciamentoApplicativo.setLogLevel(logLevel);
        tracciamentoApplicativo.setServiceName(serviceName);
        tracciamentoApplicativo.setClassName(className);
        tracciamentoApplicativo.setMethodName(methodName);
        tracciamentoApplicativo.setMessage(message);

        esecuzioneDAO.trace(tracciamentoApplicativo);

    }

}
