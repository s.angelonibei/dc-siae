package it.siae.valorizzatore.service.security;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import it.siae.valorizzatore.model.security.User;
import it.siae.valorizzatore.service.IConfigurationService;
import it.siae.valorizzatore.service.ITraceService;
import it.siae.valorizzatore.utility.Constants;
import org.springframework.beans.factory.annotation.Autowired;

public class TimeOutFilter implements Filter {

    @Autowired
    UserAuthenticationProvider userAuthenticationProvider;

    @Autowired
    private IConfigurationService configurationService;


    @Autowired
    private ITraceService traceService;

    @Override
    public void init(FilterConfig cfg) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        try {

            String hostAddress = InetAddress.getLocalHost().getHostAddress();

            String ssoEnabled = configurationService.getParameter("SSO_ENABLED");

            // Se non è in esecuzione su localhost e se SSO è abilitato -> verifica se il token è valido
            if (InetAddress.getLocalHost().getHostAddress() != null &&
                !InetAddress.getLocalHost().getHostAddress().equals("127.0.0.1") &&
                !InetAddress.getLocalHost().getHostAddress().equals("127.0.1.1") &&
                (ssoEnabled == null || ssoEnabled.equals("Y"))) {

                String contextPath = ((HttpServletRequest) request).getContextPath();

                User user = (User)((HttpServletRequest) request).getSession().getAttribute("user");

                if (user == null) {

                    traceService.trace("TimeOutFilter", this.getClass().getName(), "doFilter", "user nullo", Constants.INFO);

                    String homeUrl = configurationService.getParameter("SSO_HOME_URL");

                    ((HttpServletResponse) response).sendRedirect(homeUrl);

                } else {

                    String valid = userAuthenticationProvider.checkToken(user.getSesisonToken());

                    traceService.trace("TimeOutFilter", this.getClass().getName(), "doFilter", "Esito checkToken per "+user.getSesisonToken()+": "+valid, Constants.INFO);

                    if (valid == null || valid.equals("")){

                        String homeUrl = configurationService.getParameter("SSO_HOME_URL");

                        traceService.trace("TimeOutFilter", this.getClass().getName(), "doFilter", "Redirect verso "+homeUrl, Constants.INFO);

                        ((HttpServletResponse) response).sendRedirect(homeUrl);
                    }

                }
            }

        }catch(Exception e){
            e.printStackTrace();

            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);

            traceService.trace("TimeOutFilter", this.getClass().getName(), "doFilter", sw.toString(), Constants.ERROR);
        }

        chain.doFilter(request, response);

    }


    @Override
    public void destroy() {
    }

}