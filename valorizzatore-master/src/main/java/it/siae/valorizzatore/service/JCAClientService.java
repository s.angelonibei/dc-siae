package it.siae.valorizzatore.service;

import it.siae.valorizzatore.dao.IConfigurationDAO;
import it.siae.valorizzatore.dao.IEsecuzioneDAO;
import it.siae.valorizzatore.model.Engine;
import it.siae.valorizzatore.utility.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by idilello on 6/13/16.
 */
@Service("jCAClientService")
public class JCAClientService implements IJCAClientService {

    @Autowired
    @Qualifier("configurationDAO")
    IConfigurationDAO configurationDAO;

    @Autowired
    private ITraceService traceService;

    /**
     * Ritorna:
     *   - ACTIVE se command STATUS e processo attivo
     *   - INACTIVE se command STATUS e processo attivo
     *
     *   - OK se command START_RICALCOLO e esito positivo
     *   - KO se command START_RICALCOLO e esito negativo
     *
     *   - OK se command START_CAMPIONAMENTO e esito positivo
     *   - KO se command START_CAMPIONAMENTO e esito negativo
     *
     *   - 1300-600-100-0-0 (Totali-Elaborati-Sospesi-Errori-Terminato[1]/Attivo [0])  Avanzamenti Elaborazione Ricalcolo/Campionamento/Aggregazione
     *
     * @param command
     * @return
     */
    public List<String> sendMultipleEngineCommand(String command){

        String result = "INACTIVE";
        List<String> resultList = new ArrayList<String>();

        List<Engine> engines = new ArrayList<Engine>();
        Engine engine = null;
        int numeroEngine = Integer.parseInt(configurationDAO.getParameter(Constants.NUMERO_ENGINE));

        // ToDo: Remove
        //numeroEngine = 1;

        for (int i=1; i<=numeroEngine; i++){
            engine = new Engine(configurationDAO.getParameter(Constants.ENGINE_ADDRESS+"_"+i));
            engines.add(engine);
        }

        Socket s = null;
        PrintWriter out = null;
        BufferedReader input = null;

        // Attiva il ricalcolo su tutti gli engine
        for (int i=0; i<numeroEngine; i++){

            try{


                    // ToDo: Remove
                    //s = new Socket("127.0.0.1", 9090);
                    s = new Socket(engines.get(i).getEngineAddress(), engines.get(i).getEnginePort());

                    out = new PrintWriter(s.getOutputStream(), true);
                    out.println(command);

                    input = new BufferedReader(new InputStreamReader(s.getInputStream()));
                    result = input.readLine();

                    resultList.add(i, result);

            }catch(java.net.ConnectException cr){

                    cr.printStackTrace();
                    result = "INACTIVE";

                    resultList.add(i, result);

                    StringWriter sw = new StringWriter();
                    PrintWriter pw = new PrintWriter(sw);
                    cr.printStackTrace(pw);

                    traceService.trace("JCAClientService", this.getClass().getName(), "sendMultipleEngineCommand", "Endpoint: "+engines.get(i).getEngineAddress()+":"+engines.get(i).getEnginePort()+"  Eccezione: "+sw.toString(), Constants.ERROR);

             }catch(java.io.IOException ioe){

                    ioe.printStackTrace();
                    result = "INACTIVE";

                    resultList.add(i, result);

                    StringWriter sw = new StringWriter();
                    PrintWriter pw = new PrintWriter(sw);
                    ioe.printStackTrace(pw);

                    traceService.trace("JCAClientService", this.getClass().getName(), "sendMultipleEngineCommand", sw.toString(), Constants.ERROR);
            }
        }

        return resultList;
    }


    public List<String> sendMultipleEngineCommand2243(String command){

        String result = "INACTIVE";
        List<String> resultList = new ArrayList<String>();

        List<Engine> engines = new ArrayList<Engine>();
        Engine engine = null;
        int numeroEngine = Integer.parseInt(configurationDAO.getParameter(Constants.NUMERO_ENGINE_2243));

        // ToDo: Remove
        //numeroEngine = 1;

        for (int i=1; i<=numeroEngine; i++){
            engine = new Engine(configurationDAO.getParameter(Constants.ENGINE_ADDRESS_2243+"_"+i));
            engines.add(engine);
        }

        Socket s = null;
        PrintWriter out = null;
        BufferedReader input = null;

        // Attiva il ricalcolo su tutti gli engine
        for (int i=0; i<numeroEngine; i++){

            try{


                // ToDo: Remove
                //s = new Socket("127.0.0.1", 9090);
                s = new Socket(engines.get(i).getEngineAddress(), engines.get(i).getEnginePort());

                out = new PrintWriter(s.getOutputStream(), true);
                out.println(command);

                input = new BufferedReader(new InputStreamReader(s.getInputStream()));
                result = input.readLine();

                resultList.add(i, result);

            }catch(java.net.ConnectException cr){

                cr.printStackTrace();
                result = "INACTIVE";

                resultList.add(i, result);

                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                cr.printStackTrace(pw);

                traceService.trace("JCAClientService2243", this.getClass().getName(), "sendMultipleEngineCommand2243", "Endpoint: "+engines.get(i).getEngineAddress()+":"+engines.get(i).getEnginePort()+"  Eccezione: "+sw.toString(), Constants.ERROR);

            }catch(java.io.IOException ioe){

                ioe.printStackTrace();
                result = "INACTIVE";

                resultList.add(i, result);

                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                ioe.printStackTrace(pw);

                traceService.trace("JCAClientService2243", this.getClass().getName(), "sendMultipleEngineCommand2243", sw.toString(), Constants.ERROR);
            }
        }

        return resultList;
    }


    public String sendCommand(String command){

        String serverAddress = "127.0.0.1";
        int serverPort = 9090;

        String clientListener = configurationDAO.getParameter(Constants.PROCESSO1_LISTENER);
        String[] hostPort = clientListener.split(":");
        serverAddress=hostPort[0];
        serverPort=Integer.parseInt(hostPort[1]);

        String result = "INACTIVE";

        // ToDo: Remove
        //serverAddress="127.0.0.1";

        try{

            Socket s = new Socket(serverAddress, serverPort);

            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            out.println(command);

            BufferedReader input = new BufferedReader(new InputStreamReader(s.getInputStream()));
            result = input.readLine();

            //System.out.println("JCAClientService: "+result);

        }catch(java.net.ConnectException cr){
            cr.printStackTrace();
            result = "INACTIVE";

            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            cr.printStackTrace(pw);

            traceService.trace("JCAClientService", this.getClass().getName(), "sendCommand", sw.toString(), Constants.ERROR);


        }catch(java.io.IOException ioe){
            ioe.printStackTrace();
            result = "INACTIVE";

            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ioe.printStackTrace(pw);

            traceService.trace("JCAClientService", this.getClass().getName(), "sendCommand", sw.toString(), Constants.ERROR);
        }

        return result;
    }

    public static void main(String[] args){

        String result = "INACTIVE";

        Socket s = null;
        PrintWriter out = null;
        BufferedReader input = null;

        try{

            // srvtst-l029v.net.siae:9090
            // srvtst-l029v.net.siae:9091
            // srvtst-l053v.net.siae:9092
            // srvtst-l053v.net.siae:9093

            s = new Socket("127.0.0.1", 9090);

            // STATUS_2243
            // DEACTIVATE_2243
            // RICALCOLO_START_2243 123
            // INFORMAZIONI_ELABORAZIONE_2243
            // AGGREGAZIONE_START_2243
            // INFORMAZIONI_ELABORAZIONE_AGGREGAZIONE_2243

            out = new PrintWriter(s.getOutputStream(), true);
            out.println("RICALCOLO_START_2243 123");

            input = new BufferedReader(new InputStreamReader(s.getInputStream()));
            result = input.readLine();

            System.out.println("Result: "+result);

        }catch(java.net.ConnectException cr){

            cr.printStackTrace();
            result = "INACTIVE";

            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            cr.printStackTrace(pw);

        }catch(java.io.IOException ioe){

            ioe.printStackTrace();
            result = "INACTIVE";


            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ioe.printStackTrace(pw);

        }

    }

}
