package it.siae.valorizzatore.service;

import it.siae.valorizzatore.dao.ICampionamentoDAO;
import it.siae.valorizzatore.dao.IProgrammaMusicaleDAO;
import it.siae.valorizzatore.model.*;
import it.siae.valorizzatore.model.security.AggregatoVoce;
import it.siae.valorizzatore.model.security.ReportPage;
import it.siae.valorizzatore.utility.Constants;
import it.siae.valorizzatore.utility.ReportCache;
import it.siae.valorizzatore.utility.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ForkJoinPoolFactoryBean;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ForkJoinPool;

@Service("programmaMusicaleService")
public class ProgrammaMusicaleService implements IProgrammaMusicaleService{

    @Autowired
    @Qualifier("programmaMusicaleDAO")
    IProgrammaMusicaleDAO programmaMusicaleDAO;

    @Autowired
    @Qualifier("campionamentoDAO")
    ICampionamentoDAO campionamentoDAO;

    @Autowired
    private ForkJoinPool forkJoinPool;


    public ReportPage getFlussoGuida(Long contabilitaIniziale, Long contabilitaFinale, Integer page){
        return campionamentoDAO.getFlussoGuida(contabilitaIniziale, contabilitaFinale, page);
    }

    public ReportPage getFlussoRientrati(Long contabilitaIniziale, Long contabilitaFinale, Integer page){
        return campionamentoDAO.getFlussoRientrati(contabilitaIniziale, contabilitaFinale, page);
    }

    public List<String> getTipoProgrammi(){
        return programmaMusicaleDAO.getTipoProgrammi();
    }



    public ReportPage saveAndDownloadFlussoGuida(Long contabilitaIniziale, Long contabilitaFinale){

        System.out.println(" Acquisiszione flusso guida (Inizio): "+new Date());
        ReportPage reportPage = campionamentoDAO.getFlussoGuida(contabilitaIniziale, contabilitaFinale, -1);
        System.out.println(" Acquisiszione flusso guida (End): "+new Date());

        List<Object> listaPMFlussoGuida = (List<Object>)reportPage.getRecords();

        UUID id = UUID.randomUUID();
        ArrayList<UUID> ids = new ArrayList<UUID>();
        ids.add(id);

        System.out.println(" Inoltro task parallelo (Inizio): "+new Date());
        ParallelAction task = new ParallelAction(ids, programmaMusicaleDAO, listaPMFlussoGuida, 0, 0);
        //forkJoinPool.invoke(task);
        forkJoinPool.submit(task);
        System.out.println(" Inoltro task parallelo (End): "+new Date());

        /*
        List<Object> listaPMFlussoGuida = (List<Object>)reportPage.getRecords();
        ProgrammaMusicale programmaMusicale;
        MovimentazioneLogisticaPM movimentazioneLogisticaPM;
        for (int i=0; i<listaPMFlussoGuida.size(); i++){

            programmaMusicale = (ProgrammaMusicale)listaPMFlussoGuida.get(i);
            movimentazioneLogisticaPM=new MovimentazioneLogisticaPM();

            movimentazioneLogisticaPM.setFlagAttivo('Y');
            movimentazioneLogisticaPM.setFlagEsclusoInvioNeed('N');
            movimentazioneLogisticaPM.setFlagEsclusoRicalcolo('N');
            movimentazioneLogisticaPM.setStato(Constants.INVIATO_A_NEED);
            movimentazioneLogisticaPM.setIdProgrammaMusicale(programmaMusicale.getId());

            programmaMusicaleDAO.insertMovimentazioneLogisticaPM(movimentazioneLogisticaPM);
        }
        */

        return reportPage;
    }

    public int storeFileContant(String filePathName){

        BufferedReader br = null;
        int cont=0;

        try {

            String sCurrentLine;

            br = new BufferedReader(new FileReader(filePathName));

            String[] elements;
            Long numeroProgramma;
            Long totaleCedole;
            Double totaleDurata;
            ProgrammaMusicale programmaMusicale;



            while ((sCurrentLine = br.readLine()) != null) {
                System.out.println(sCurrentLine);

                if (sCurrentLine!=null){
                    elements = sCurrentLine.split(",");

                    if (elements[0]!=null){

                        if (Utility.isNumber(elements[0])){

                            numeroProgramma=elements[0]!=null?Long.parseLong(elements[0]):0L;
                            totaleCedole=elements[1]!=null?Long.parseLong(elements[1]):0L;
                            totaleDurata=elements[2]!=null?Double.parseDouble(elements[2]):0L;

                            programmaMusicale=new ProgrammaMusicale();

                            programmaMusicale.setNumeroProgrammaMusicale(numeroProgramma);
                            programmaMusicale.setTotaleDurataCedole(totaleDurata);
                            programmaMusicale.setTotaleCedole(totaleCedole);

                            programmaMusicaleDAO.updatePMRientrati(programmaMusicale);
                            cont++;
                        }



                    }

                }

            }

        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        } finally {
            try {
                if (br != null)br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
                return -1;
            }
        }

        return cont;

    }

    public ReportPage getTracciamentoPM(Date dataIniziale, Date dataFinale, Long numeroPM, Integer page){

        List<String> parameters = new ArrayList<String>();

        if (dataIniziale!=null)
            parameters.add(dataIniziale.toString());
        else
            parameters.add(null);
        if (dataFinale!=null)
            parameters.add(dataFinale.toString());
        else
            parameters.add(null);
        if (numeroPM!=null)
            parameters.add(numeroPM.toString());
        else
            parameters.add(null);

        String key = ReportCache.generateKey(ReportCache.REPORT_TRACCIAMENTO_PM, parameters);

        ReportPage reportPage = ReportCache.getReport(key);

        // Verifica presenza del Report in Cache.
        if (reportPage != null){

            // Se il report è già presente in cache, non esegue la query per prendere la pagina interessata, ma usa i dati
            // già presenti
            reportPage = ReportCache.setCurrentReportPage(reportPage, page);

        }else{

            ReportCache.clearReports();

            // Se il report non è presente in cache, esegue la query per acquisire i dati del report e lo restituisce, dopo
            // aver aggiornato la cache
            reportPage = programmaMusicaleDAO.getTracciamentoPM(dataIniziale, dataFinale, numeroPM, page);

            ReportCache.putReport(ReportCache.REPORT_TRACCIAMENTO_PM, key, reportPage);
        }

        return reportPage;


    }

    public ReportPage getListaEventiPagati(Long inizioPeriofoContabile, Long finePeriofoContabile, Long evento, String fattura, String reversale, String voceIncasso,
                                           Date dataInizioEvento, String oraInizioEvento, String seprag, String tipoDocumento, String locale, String codiceBA,
                                           String presenzaMovimenti, Integer page) {

        List<String> parameters = new ArrayList<String>();
        parameters.add(inizioPeriofoContabile.toString());
        parameters.add(finePeriofoContabile.toString());
        if (evento!=null)
          parameters.add(evento.toString());
        else
          parameters.add(null);
        parameters.add(fattura);
        parameters.add(reversale);
        parameters.add(voceIncasso);
        if (evento!=null && dataInizioEvento!=null)
         parameters.add(dataInizioEvento.toString());
        else
         parameters.add(null);
        parameters.add(oraInizioEvento);
        parameters.add(seprag);
        parameters.add(tipoDocumento);
        parameters.add(locale);
        parameters.add(codiceBA);
        parameters.add(presenzaMovimenti);
        String key = ReportCache.generateKey(ReportCache.REPORT_LISTA_EVENTI, parameters);

        ReportPage reportPage = ReportCache.getReport(key);

        // Verifica presenza del Report in Cache.
        if (reportPage != null){

            // Se il report è già presente in cache, non esegue la query per prendere la pagina interessata, ma usa i dati
            // già presenti
            reportPage = ReportCache.setCurrentReportPage(reportPage, page);

        }else{

            ReportCache.clearReports();

            // Se il report non è presente in cache, esegue la query per acquisire i dati del report e lo restituisce, dopo
            // aver aggiornato la cache
            reportPage = programmaMusicaleDAO.getListaEventiPagati(inizioPeriofoContabile, finePeriofoContabile, evento, fattura, reversale, voceIncasso,
                         dataInizioEvento, oraInizioEvento, seprag, tipoDocumento, locale, codiceBA, presenzaMovimenti, page);

            ReportCache.putReport(ReportCache.REPORT_LISTA_EVENTI, key, reportPage);
        }

        return reportPage;

    }

    private Map<String, List<ImportoRicalcolato>> normalizeImportiPMAssegnatiPerVoce(List<ImportoRicalcolato> listaImportiPMAssegnati){

        Map<String, List<ImportoRicalcolato>> mapImporti = new HashMap<String, List<ImportoRicalcolato>>();
        List<ImportoRicalcolato> listaImporti;
        String voceIncasso;
        for (ImportoRicalcolato importo: listaImportiPMAssegnati){
            voceIncasso = importo.getVoceIncasso();

            if (mapImporti.get(voceIncasso)==null) {
                listaImporti = new ArrayList<ImportoRicalcolato>();
                listaImporti.add(importo);
                mapImporti.put(voceIncasso, listaImporti);
            }else{
                listaImporti = mapImporti.get(voceIncasso);
                listaImporti.add(importo);
                mapImporti.put(voceIncasso, listaImporti);
            }
        }
        return mapImporti;
    }

    private Map<String, ImportoRicalcolato> normalizeImportiTotaliEventoPerVoce(List<ImportoRicalcolato> listaImportiPMAssegnati){

        Map<String, ImportoRicalcolato> mapImporti = new HashMap<String, ImportoRicalcolato>();
        String voceIncasso;
        for (ImportoRicalcolato importo: listaImportiPMAssegnati){
            voceIncasso = importo.getVoceIncasso();

            if (mapImporti.get(voceIncasso)==null) {
                mapImporti.put(voceIncasso, importo);
            }
        }
        return mapImporti;
    }


    // 33618319 non si vede dettaglio (EventoId = 41673648)
    // 33838289 si vede dettaglio
    public List<EventiPagati> getEvento(Long eventoId){

        List<EventiPagati> listaEventiPagati = programmaMusicaleDAO.getEvento(eventoId);

        // Caso in cui ci sono movimentazioni su PM, ma non l'evento pagato da SUN
        if (listaEventiPagati==null || listaEventiPagati.size()==0){

            EventiPagati eventoPagato = new EventiPagati();

            eventoPagato.setIdEvento(eventoId);

            Manifestazione manifestazione = null;
            MovimentoContabile movimentoContabile = null;

            List<MovimentoContabile> movimentazioni = programmaMusicaleDAO.getMovimentazioniPerEvento(eventoId);

            List<Manifestazione> manifestazioni = programmaMusicaleDAO.getManifestazioni(movimentazioni);

            if (manifestazioni!=null && manifestazioni.size()>0) {

                manifestazione = manifestazioni.get(0);

                movimentoContabile = movimentazioni.get(0);

                eventoPagato.setManifestazione(manifestazione);

                eventoPagato.setDataInizioEvento(manifestazione.getDataInizioEvento());

                eventoPagato.setDenominazioneLocale(manifestazione.getDenominazioneLocale());

                eventoPagato.setCodiceBaLocale(manifestazione.getCodiceLocale());

                //eventoPagato.setContabilita(movimentoContabile.getContabilita());

            }

            eventoPagato.setMovimentazioniPM(movimentazioni);
            eventoPagato.setVoceIncasso(movimentazioni.get(0).getVoceIncasso());
            eventoPagato.setNumeroFattura(movimentazioni.get(0).getNumeroFattura().toString());
            Cartella cartella = programmaMusicaleDAO.getCartellaById(movimentazioni.get(0).getIdCartella());
            eventoPagato.setSeprag(cartella.getSeprag());

            listaEventiPagati.add(eventoPagato);
        }


        if (listaEventiPagati!=null && listaEventiPagati.size()>0){

            Map<String, EventiPagati> attesiPerEvento = new HashMap<String, EventiPagati>();
            Map<String, MovimentoContabile> attesiPerMovimento = new HashMap<String, MovimentoContabile>();

            Map<String, List<String>> rientrati = new HashMap<String, List<String>>();
            Map<String, List<String>> rientratiSpalla = new HashMap<String, List<String>>();

            // Lista degli importi ricalcolati
            List<ImportoRicalcolato> listaImportiPMAssegnati = programmaMusicaleDAO.getImportiPMAssegnatiPerEvento(eventoId);

            // Importi presenti in Eventi Pagati
            List<ImportoRicalcolato> listaImportiTotali = programmaMusicaleDAO.getImportiTotaliPerEvento(eventoId);

            Map<String, List<ImportoRicalcolato>> importiPMAssegnati = normalizeImportiPMAssegnatiPerVoce(listaImportiPMAssegnati);
            Map<String, ImportoRicalcolato> importiTotali = normalizeImportiTotaliEventoPerVoce(listaImportiTotali);

            // Aquisisce l'ultimo movimento contabile dell'Evento/Voce Incasso (EventiPagati più aggiornati)
            String voceIncasso;
            EventiPagati eventoSelezionato;
            MovimentoContabile movimentoSelezionato;
            for (EventiPagati evento: listaEventiPagati){
                voceIncasso = evento.getVoceIncasso();

                if (attesiPerEvento.get(voceIncasso)==null)
                    attesiPerEvento.put(voceIncasso, evento);
                else{
                    eventoSelezionato = attesiPerEvento.get(voceIncasso);
                    // Entry più recente per cui ha le informazioni degli attesi per Evento/Voce più aggiornate
                    if (evento.getId().longValue()>eventoSelezionato.getId().longValue())
                        attesiPerEvento.put(voceIncasso, evento);
                }
            }

            // Aquisisce l'ultimo movimento contabile dell'Evento/Voce Incasso (movimento158 più aggiornato)

            Long count=0L;

            List<MovimentoContabile> movimentazioni = listaEventiPagati.get(0).getMovimentazioniPM();
            List<String> pmRientrati = null;
            for (MovimentoContabile movimento: movimentazioni){

                voceIncasso = movimento.getVoceIncasso();

                if (attesiPerMovimento.get(voceIncasso)==null)
                    attesiPerMovimento.put(voceIncasso, movimento);
                else{
                    movimentoSelezionato = attesiPerMovimento.get(voceIncasso);
                    // Entry più recente per cui ha le informazioni degli attesi per Evento/Voce più aggiornate
                    if (movimento.getId().longValue()>movimentoSelezionato.getId().longValue())
                        attesiPerMovimento.put(voceIncasso, movimento);
                }


                // Trattasi di gruppo principale o spalla ()
                if (voceIncasso.equals(Constants.VOCE_2244) || voceIncasso.equals(Constants.VOCE_2243)){

                    if (movimento.getFlagGruppoPrincipale()=='Y' || movimento.getFlagGruppoPrincipale()=='1'){
                        if (rientrati.get(voceIncasso)==null) {
                            pmRientrati=new ArrayList<String>();
                            pmRientrati.add(movimento.getNumProgrammaMusicale());
                            rientrati.put(voceIncasso, pmRientrati);
                        }else{
                            // A parità di evento/voce incasso ci posono essere più fatture sui PM e quindi i PM potrbbero ripetersi nella movimentazione
                            pmRientrati = rientrati.get(voceIncasso);
                            if (!pmRientrati.contains(movimento.getNumProgrammaMusicale())) {
                                pmRientrati.add(movimento.getNumProgrammaMusicale());
                                rientrati.put(voceIncasso, pmRientrati);
                            }
                        }
                    }else{
                        // Trattasi di gruppo Spalla
                        if (rientratiSpalla.get(voceIncasso)==null) {
                            pmRientrati=new ArrayList<String>();
                            pmRientrati.add(movimento.getNumProgrammaMusicale());
                            rientratiSpalla.put(voceIncasso, pmRientrati);
                        }else{
                            pmRientrati = rientratiSpalla.get(voceIncasso);
                            if (!pmRientrati.contains(movimento.getNumProgrammaMusicale())) {
                                pmRientrati.add(movimento.getNumProgrammaMusicale());
                                rientratiSpalla.put(voceIncasso, pmRientrati);
                            }
                        }
                    }
                    /*
                    if (movimento.getFlagGruppoPrincipale()=='Y' || movimento.getFlagGruppoPrincipale()=='1'){
                        if (rientrati.get(voceIncasso+"_"+movimento.getNumProgrammaMusicale())==null)
                            rientrati.put(voceIncasso+"_"+movimento.getNumProgrammaMusicale(), 1L);
                        else{
                            // A parità di evento/voce incasso ci posono essere più fatture sui PM e quindi i PM potrbbero ripetersi nella movimentazione
                            count = rientrati.get(voceIncasso+"_"+movimento.getNumProgrammaMusicale());
                            count++;
                            rientrati.put(voceIncasso+"_"+movimento.getNumProgrammaMusicale(), count);
                        }
                    }else{
                        // Trattasi di gruppo Spalla
                        if (rientratiSpalla.get(voceIncasso+"_"+movimento.getNumProgrammaMusicale())==null)
                            rientratiSpalla.put(voceIncasso+"_"+movimento.getNumProgrammaMusicale(), 1L);
                        else{
                            count = rientratiSpalla.get(voceIncasso+"_"+movimento.getNumProgrammaMusicale());
                            count++;
                            rientratiSpalla.put(voceIncasso+"_"+movimento.getNumProgrammaMusicale(), count);
                        }
                    }
                   */
                }else{

                    if (rientrati.get(voceIncasso)==null) {
                        pmRientrati=new ArrayList<String>();
                        pmRientrati.add(movimento.getNumProgrammaMusicale());
                        rientrati.put(voceIncasso, pmRientrati);
                    }else{
                        pmRientrati = rientrati.get(voceIncasso);
                        if (!pmRientrati.contains(movimento.getNumProgrammaMusicale())) {
                            pmRientrati.add(movimento.getNumProgrammaMusicale());
                            rientrati.put(voceIncasso, pmRientrati);
                        }
                    }

                }

            }

            DettaglioPM dettaglioPM;
            List<DettaglioPM> listaPMAttesi = new ArrayList<DettaglioPM>();
            Iterator listaVociIncasso = attesiPerEvento.keySet().iterator();
            while (listaVociIncasso.hasNext()){
                voceIncasso = (String)listaVociIncasso.next();

                dettaglioPM = new DettaglioPM();
                dettaglioPM.setIdEvento(eventoId);
                dettaglioPM.setVoceIncasso(voceIncasso);

                // I previsti dei Movimenti sono più aggiornati rispetto a quelli degli eventi
                if (attesiPerMovimento.get(voceIncasso)!=null){
                    movimentoSelezionato = attesiPerMovimento.get(voceIncasso);
                    dettaglioPM.setPmPrevisti(movimentoSelezionato.getNumeroPmPrevisti().longValue());
                    dettaglioPM.setPmPrevistiSpalla(movimentoSelezionato.getNumeroPmPrevistiSpalla().longValue());
                }else{
                    eventoSelezionato =  attesiPerEvento.get(voceIncasso);
                    dettaglioPM.setPmPrevisti(eventoSelezionato.getNumeroPmTotaliAttesi().longValue());
                    dettaglioPM.setPmPrevistiSpalla(eventoSelezionato.getNumeroPmTotaliAttesiSpalla().longValue());
                }

                if (rientrati.get(voceIncasso)!=null){
                    dettaglioPM.setPmRientrati(new Long(((List<String>)rientrati.get(voceIncasso)).size()));
                }else{
                    dettaglioPM.setPmRientrati(new Long(0));
                }

                if (rientratiSpalla.get(voceIncasso)!=null){
                    dettaglioPM.setPmRientratiSpalla(new Long(((List<String>)rientratiSpalla.get(voceIncasso)).size()));
                }else{
                    dettaglioPM.setPmRientratiSpalla(new Long(0));
                }

                if (importiPMAssegnati.get(voceIncasso)!=null){
                    dettaglioPM.setImportiRicalcolati(importiPMAssegnati.get(voceIncasso));
                }
                if (importiTotali.get(voceIncasso)!=null){
                    dettaglioPM.setImportoTotale(importiTotali.get(voceIncasso).getImporto());
                }

                listaPMAttesi.add(dettaglioPM);
            }

            for (EventiPagati evento:listaEventiPagati){
                evento.setDettaglioPM(listaPMAttesi);
            }

        }

        return listaEventiPagati;
    }

    public ReportPage getListaMovimentazioni(Long inizioPeriodoContabile, Long finePeriodoContabile, Long evento, String fattura, Long reversale, String voceIncasso,
                                             Date dataInizioEvento,Date dataFineEvento, String seprag, String tipoDocumento, String locale, String organizzatore,
                                             String tipoSupporto, String tipoProgramma, String gruppoPrincipale, String titoloOpera, String numeroPM, String permesso,
                                             Integer page) {



        List<String> parameters = new ArrayList<String>();
        parameters.add(inizioPeriodoContabile.toString());
        parameters.add(finePeriodoContabile.toString());
        parameters.add(voceIncasso);
        parameters.add(fattura);
        if (dataInizioEvento!=null)
          parameters.add(dataInizioEvento.toString());
        else
          parameters.add(null);
        if (dataFineEvento!=null)
          parameters.add(dataFineEvento.toString());
        else
          parameters.add(null);

        parameters.add(seprag);
        parameters.add(tipoDocumento);
        parameters.add(locale);
        parameters.add(organizzatore);
        parameters.add(tipoSupporto);
        parameters.add(tipoProgramma);
        parameters.add(gruppoPrincipale);
        parameters.add(titoloOpera);
        parameters.add(numeroPM);
        parameters.add(permesso);

        if (evento!=null)
          parameters.add(evento.toString());
        else
          parameters.add(null);

        if (reversale!=null)
          parameters.add(reversale.toString());
        else
          parameters.add(null);

        if (evento!=null)
          parameters.add(evento.toString());
        else
          parameters.add(null);

        String key = ReportCache.generateKey(ReportCache.REPORT_LISTA_MOVIMENTI, parameters);

        ReportPage reportPage = ReportCache.getReport(key);

        // Verifica presenza del Report in Cache.
        if (reportPage != null){

            // Se il report è già presente in cache, non esegue la query per prendere la pagina interessata, ma usa i dati
            // già presenti
            reportPage = ReportCache.setCurrentReportPage(reportPage, page);

        }else{

            ReportCache.clearReports();

            // Se il report non è presente in cache, esegue la query per acquisire i dati del report e lo restituisce, dopo
            // aver aggiornato la cache
            reportPage = programmaMusicaleDAO.getListaMovimentazioni(inizioPeriodoContabile, finePeriodoContabile, evento, fattura, reversale, voceIncasso,
                                                dataInizioEvento, dataFineEvento, seprag, tipoDocumento, locale, organizzatore,
                                                tipoSupporto, tipoProgramma, gruppoPrincipale, titoloOpera, numeroPM, permesso, page);

            ReportCache.putReport(ReportCache.REPORT_LISTA_MOVIMENTI, key, reportPage);
        }

        return reportPage;


    }


    public ReportPage getListaAggregatiSiada(Long inizioPeriodoContabile, Long finePeriodoContabile, String voceIncasso,
                                             String tipoSupporto, String tipoProgramma, Long numeroPM, Integer page){

        List<String> parameters = new ArrayList<String>();
        parameters.add(inizioPeriodoContabile.toString());
        parameters.add(finePeriodoContabile.toString());
        parameters.add(voceIncasso);
        parameters.add(tipoSupporto);
        parameters.add(tipoProgramma);
        if (numeroPM!=null)
         parameters.add(numeroPM.toString());
        else
         parameters.add(null);

        String key = ReportCache.generateKey(ReportCache.REPORT_VERIFICA_PM_SIADA, parameters);

        ReportPage reportPage = ReportCache.getReport(key);

        // Verifica presenza del Report in Cache.
        if (reportPage != null){

            // Se il report è già presente in cache, non esegue la query per prendere la pagina interessata, ma usa i dati
            // già presenti
            reportPage = ReportCache.setCurrentReportPage(reportPage, page);

        }else{

            ReportCache.clearReports();

            // Se il report non è presente in cache, esegue la query per acquisire i dati del report e lo restituisce, dopo
            // aver aggiornato la cache
            reportPage = programmaMusicaleDAO.getListaAggregatiSiada(inizioPeriodoContabile, finePeriodoContabile, voceIncasso,tipoSupporto, tipoProgramma, numeroPM, page);

            ReportCache.putReport(ReportCache.REPORT_VERIFICA_PM_SIADA, key, reportPage);
        }

        return reportPage;


    }



    public ReportPage getListaAggregatiSiadaSMC(Long inizioPeriodoContabile, Long finePeriodoContabile, String voceIncasso,
                                             String tipoSupporto, String tipoProgramma, Long numeroPM, Integer page){

        List<String> parameters = new ArrayList<String>();
        parameters.add(inizioPeriodoContabile.toString());
        parameters.add(finePeriodoContabile.toString());
        parameters.add(voceIncasso);
        parameters.add(tipoSupporto);
        parameters.add(tipoProgramma);
        if (numeroPM!=null)
            parameters.add(numeroPM.toString());
        else
            parameters.add(null);

        String key = ReportCache.generateKey(ReportCache.REPORT_VERIFICA_PM_SIADA_SMC, parameters);

        ReportPage reportPage = ReportCache.getReport(key);

        // Verifica presenza del Report in Cache.
        if (reportPage != null){

            // Se il report è già presente in cache, non esegue la query per prendere la pagina interessata, ma usa i dati
            // già presenti
            reportPage = ReportCache.setCurrentReportPage(reportPage, page);

        }else{

            ReportCache.clearReports();

            // Se il report non è presente in cache, esegue la query per acquisire i dati del report e lo restituisce, dopo
            // aver aggiornato la cache
            reportPage = programmaMusicaleDAO.getListaAggregatiSiadaSMC(inizioPeriodoContabile, finePeriodoContabile, voceIncasso,tipoSupporto, tipoProgramma, numeroPM, page);

            ReportCache.putReport(ReportCache.REPORT_VERIFICA_PM_SIADA_SMC, key, reportPage);
        }

        return reportPage;


    }


    public ReportPage getListaAggregatiEventi(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale){

        List<String> parameters = new ArrayList<String>();
        parameters.add(inizioPeriodoContabile.toString());
        parameters.add(finePeriodoContabile.toString());
        if (idPuntoTerritoriale!=null)
            parameters.add(idPuntoTerritoriale.toString());
        else
            parameters.add(null);

        String key = ReportCache.generateKey(ReportCache.REPORT_AGGREGATO_INCASSO, parameters);

        ReportPage reportPage = ReportCache.getReport(key);

        // Verifica presenza del Report in Cache.
        if (reportPage != null){

            int page = -1;

            // Se il report è già presente in cache, non esegue la query per prendere la pagina interessata, ma usa i dati
            // già presenti
            reportPage = ReportCache.setCurrentReportPage(reportPage, page);

        }else{

            ReportCache.clearReports();

            // Se il report non è presente in cache, esegue le query per acquisire i dati del report e lo restituisce, dopo
            // aver aggiornato la cache
            List<AggregatoVoce> listaAggregati = new ArrayList<AggregatoVoce>();

            reportPage = new ReportPage(listaAggregati, listaAggregati.size(), 1);

            List<AggregatoVoce> listaDocumentiEvento = programmaMusicaleDAO.getListaDocumentiEventi(inizioPeriodoContabile,finePeriodoContabile,idPuntoTerritoriale);

            if (listaDocumentiEvento != null && listaDocumentiEvento.size() > 0) {

                List<AggregatoVoce> listaEventi = programmaMusicaleDAO.getListaEventi(inizioPeriodoContabile,finePeriodoContabile,idPuntoTerritoriale);

                AggregatoVoce aggregatoVoce = null;
                HashMap<String, AggregatoVoce> aggregatiEventiMap = new HashMap<String, AggregatoVoce>();

                for (int i = 0; i < listaDocumentiEvento.size(); i++) {

                    if (aggregatiEventiMap.get(listaDocumentiEvento.get(i).getVoceIncasso())==null) {
                        aggregatoVoce = new AggregatoVoce();
                        aggregatoVoce.setVoceIncasso(listaDocumentiEvento.get(i).getVoceIncasso());

                        for (int j=0; j<listaEventi.size(); j++){
                            if (listaEventi.get(j).getVoceIncasso().equalsIgnoreCase(listaDocumentiEvento.get(i).getVoceIncasso())){
                                aggregatoVoce.setNumeroEventi(listaEventi.get(j).getNumeroEventi());
                                break;
                            }
                        }
                        aggregatiEventiMap.put(listaDocumentiEvento.get(i).getVoceIncasso(), aggregatoVoce);

                    } else {
                        aggregatoVoce = aggregatiEventiMap.get(listaDocumentiEvento.get(i).getVoceIncasso());
                    }

                    if (listaDocumentiEvento.get(i).getTipoDocumento()!=null && listaDocumentiEvento.get(i).getTipoDocumento().equalsIgnoreCase(Constants.DOCUMENTO_ENTRATE)){
                        aggregatoVoce.setNumeroDocumentiEntrata(listaDocumentiEvento.get(i).getNumeroDocumenti());
                        aggregatoVoce.setImportoEntrate(listaDocumentiEvento.get(i).getImporto());
                    }else{
                        aggregatoVoce.setNumeroDocumentiUscita(listaDocumentiEvento.get(i).getNumeroDocumenti());
                        aggregatoVoce.setImportoUscite(listaDocumentiEvento.get(i).getImporto());
                    }

                }

                if (aggregatiEventiMap.size()>0) {
                    listaAggregati.addAll(aggregatiEventiMap.values());
                }

                reportPage = new ReportPage(listaAggregati, listaAggregati, listaAggregati.size(), 1);

            }


            ReportCache.putReport(ReportCache.REPORT_AGGREGATO_INCASSO, key, reportPage);
        }

        return reportPage;



    }


    private List<ReportPage> getListaAggregatiMovimentiInternal(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale, String correntiArretrati){

        List<ReportPage> listaReports = new ArrayList<ReportPage>();

        // Inizializza i reports da mostrare in pagina con Report vuoti
        ReportPage emptyReport = new ReportPage(new ArrayList<AggregatoVoce>(), 0, 1);
        listaReports.add(0,emptyReport);
        listaReports.add(1,emptyReport);
        listaReports.add(2,emptyReport);
        listaReports.add(3,emptyReport);

        // ------ Gestione PM Correnti --------
        if (correntiArretrati.equalsIgnoreCase("T") || correntiArretrati.equalsIgnoreCase("C")) {

            // Importo Netto per voce incasso PM Correnti
            List<AggregatoVoce> listaPMImportoNetto = programmaMusicaleDAO.getImportoNettiPMCorrenti(inizioPeriodoContabile, finePeriodoContabile, idPuntoTerritoriale);

            // Importo Programmato effettivo per Voce Incasso
            List<AggregatoVoce> listaPMImportoProgrammato = programmaMusicaleDAO.getImportoProgrammatoCorrenti(inizioPeriodoContabile, finePeriodoContabile, idPuntoTerritoriale);

            if (listaPMImportoNetto != null && listaPMImportoNetto.size() > 0) {

                // Aggiunge all'importo Netto gli altri importi calcolati o presi tramite altre query in modo fafornire un unico oggetto popolato da mostrare in pagina
                AggregatoVoce singolaVoce = null;
                for (int i = 0; i < listaPMImportoNetto.size(); i++) {
                    singolaVoce = listaPMImportoNetto.get(i);
                    for (int j = 0; j < listaPMImportoProgrammato.size(); j++) {
                        if (singolaVoce.getVoceIncasso().equals(listaPMImportoProgrammato.get(j).getVoceIncasso())) {
                            singolaVoce.setImportoProgrammato(listaPMImportoProgrammato.get(j).getImportoProgrammato());
                            // ToDo: inserire con il gestionali gli importi corretti per Sospesi e Annullati
                            singolaVoce.setImporto566(Utility.round(singolaVoce.getImporto() - singolaVoce.getImportoProgrammato() - singolaVoce.getImportoAnnullati() - singolaVoce.getImportoSospesi(), 7));
                            break;
                        }
                    }
                }

                ReportPage reportImportiPMCorrenti = new ReportPage(listaPMImportoNetto, listaPMImportoNetto.size(), 1);

                listaReports.add(0, reportImportiPMCorrenti);
            }


            // Numero PM e Numero Cedole per Voce Incasso
            List<AggregatoVoce> listaNumeroPMCorrenti = programmaMusicaleDAO.getNumeroPMCorrenti(inizioPeriodoContabile, finePeriodoContabile, idPuntoTerritoriale);

            List<AggregatoVoce> listaNumeroCedoleCorrenti = programmaMusicaleDAO.getNumeroCedoleCorrenti(inizioPeriodoContabile, finePeriodoContabile, idPuntoTerritoriale);

            if (listaNumeroPMCorrenti != null && listaNumeroPMCorrenti.size() > 0) {

                // Aggiunge al numero di PM il numero di Cedole
                AggregatoVoce singolaVoce = null;
                for (int i = 0; i < listaNumeroPMCorrenti.size(); i++) {
                    singolaVoce = listaNumeroPMCorrenti.get(i);
                    for (int j = 0; j < listaNumeroCedoleCorrenti.size(); j++) {
                        if (singolaVoce.getVoceIncasso().equals(listaNumeroCedoleCorrenti.get(j).getVoceIncasso())) {
                            singolaVoce.setNumeroCedole(listaNumeroCedoleCorrenti.get(j).getNumeroCedole());
                            break;
                        }
                    }
                }

                ReportPage reportImportiCedolePM = new ReportPage(listaNumeroPMCorrenti, listaNumeroPMCorrenti.size(), 1);

                listaReports.add(1, reportImportiCedolePM);

            }

        }


        // ------ Gstione PM Arretrati --------
        if (correntiArretrati.equalsIgnoreCase("T") || correntiArretrati.equalsIgnoreCase("A")) {

            List<AggregatoVoce> listaPMImportoNettoArretrati = programmaMusicaleDAO.getImportoNettiPMArretrati(inizioPeriodoContabile, finePeriodoContabile, idPuntoTerritoriale);

            if (listaPMImportoNettoArretrati != null && listaPMImportoNettoArretrati.size() > 0) {

                ReportPage reportImportoNettoArretrati = new ReportPage(listaPMImportoNettoArretrati, listaPMImportoNettoArretrati.size(), 1);

                listaReports.add(2, reportImportoNettoArretrati);
            }

            List<AggregatoVoce> listaNumeroPMArretrati = programmaMusicaleDAO.getNumeroPMArretrati(inizioPeriodoContabile, finePeriodoContabile, idPuntoTerritoriale);

            List<AggregatoVoce> listaNumeroCedoleArretrati = programmaMusicaleDAO.getNumeroCedoleArretrati(inizioPeriodoContabile, finePeriodoContabile, idPuntoTerritoriale);


            if (listaNumeroPMArretrati != null && listaNumeroPMArretrati.size() > 0) {

                // Aggiunge al numero di PM il numero di Cedole
                AggregatoVoce singolaVoce = null;
                for (int i = 0; i < listaNumeroPMArretrati.size(); i++) {
                    singolaVoce = listaNumeroPMArretrati.get(i);
                    for (int j = 0; j < listaNumeroCedoleArretrati.size(); j++) {
                        if (singolaVoce.getVoceIncasso().equals(listaNumeroCedoleArretrati.get(j).getVoceIncasso())) {
                            singolaVoce.setNumeroCedole(listaNumeroCedoleArretrati.get(j).getNumeroCedole());
                            break;
                        }
                    }
                }

                ReportPage reportImportiCedolePMArretrati = new ReportPage(listaNumeroPMArretrati, listaNumeroPMArretrati.size(), 1);

                listaReports.add(3, reportImportiCedolePMArretrati);
            }
        }


        return listaReports;

    }

    public List<ReportPage> getListaAggregatiMovimenti(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale, Integer page, String correntiArretrati){

        List<String> parameters = new ArrayList<String>();

        parameters.add(inizioPeriodoContabile.toString());
        parameters.add(finePeriodoContabile.toString());
        if (idPuntoTerritoriale!=null)
            parameters.add(idPuntoTerritoriale.toString());
        else
            parameters.add(null);
        parameters.add(correntiArretrati);

        String key = ReportCache.generateKey(ReportCache.REPORT_AGGREGATO_MOVIMENTI, parameters);

        List<ReportPage> reportsPage = ReportCache.getReportList(key);

        // Verifica presenza del Report in Cache
        if (reportsPage == null){

            ReportCache.clearReports();

            reportsPage = getListaAggregatiMovimentiInternal(inizioPeriodoContabile, finePeriodoContabile, idPuntoTerritoriale, correntiArretrati);

            ReportCache.putReportList(ReportCache.REPORT_AGGREGATO_MOVIMENTI,key, reportsPage);

        }

        return reportsPage;

    }


    public List<EventiPagati> getDettagliMovimento(Long movimentoId){

        MovimentoContabile movimentoContabile = programmaMusicaleDAO.getMovimentoContabile(movimentoId);

        List<EventiPagati> listaEventiPagati = getEvento(movimentoContabile.getIdEvento());

        return listaEventiPagati;
    }

    public ProgrammaMusicale getDettaglioPM(Long idProgrammaMusicale, Long idMovimento){

        ProgrammaMusicale programmaMusicale = programmaMusicaleDAO.getProgrammaMusicaleById(idProgrammaMusicale);

        MovimentoContabile movimentoSelezionato = programmaMusicaleDAO.getMovimentoContabile(idMovimento);

        programmaMusicale.setVoceIncasso(movimentoSelezionato.getVoceIncasso());

        List<Utilizzazione> listaOpere = programmaMusicaleDAO.getUtilizzazioni(idProgrammaMusicale);
        List<MovimentoContabile> listaMovimenti = programmaMusicaleDAO.getMovimentiContabile(idProgrammaMusicale);
        List<Manifestazione> eventi = programmaMusicaleDAO.getManifestazioni(listaMovimenti);

        MovimentoContabile movimento=null;
        for (int i=0; i<listaMovimenti.size(); i++){
            movimento = listaMovimenti.get(i);
            for (int j=0; j<eventi.size(); j++){
                if (movimento.getIdEvento().longValue()==eventi.get(j).getId().longValue()){
                    movimento.setEvento(eventi.get(j));
                    break;
                }
            }
        }

        DirettoreEsecuzione direttoreEsecuzione = programmaMusicaleDAO.getDirettoreEsecuzione(idProgrammaMusicale);
        Cartella cartella = programmaMusicaleDAO.getCartellaById(movimentoSelezionato.getIdCartella());

        programmaMusicale.setOpere(listaOpere);
        programmaMusicale.setMovimenti(listaMovimenti);
        programmaMusicale.setEventi(eventi);
        programmaMusicale.setDirettoreEsecuzione(direttoreEsecuzione);
        programmaMusicale.setCartella(cartella);
        programmaMusicale.setDataRientroPM(movimentoSelezionato.getDataRientroPMStr());

        // Inizializzo gli importi aggregati ricalcolati
        ImportoPM importo = new ImportoPM();

        Double importoSUN = programmaMusicaleDAO.getImportoPMSUNAggregato(idProgrammaMusicale);
        importo.setImportoAggregatoSUN(importoSUN);

        Double importoTotaleRicalcolato = programmaMusicaleDAO.getImportoPMSIADAAggregato(idProgrammaMusicale);

        if (importoTotaleRicalcolato == null){
           List<ImportoRicalcolato> importiRicalcolati = programmaMusicaleDAO.getImportiPMRicalcolati(idProgrammaMusicale);
            importo.setImportiRicalcolati(importiRicalcolati);
        }else{
            importo.setImportoTotaleRicalcolato(importoTotaleRicalcolato);
        }
        programmaMusicale.setImporto(importo);

        return programmaMusicale;
    }


    public ProgrammaMusicale getDettaglioPM(Long idProgrammaMusicale){

        ProgrammaMusicale programmaMusicale = programmaMusicaleDAO.getProgrammaMusicaleById(idProgrammaMusicale);

        List<Utilizzazione> listaOpere = programmaMusicaleDAO.getUtilizzazioni(idProgrammaMusicale);
        List<MovimentoContabile> listaMovimenti = programmaMusicaleDAO.getMovimentiContabile(idProgrammaMusicale);
        List<Manifestazione> eventi = programmaMusicaleDAO.getManifestazioni(listaMovimenti);

        programmaMusicale.setVoceIncasso(listaMovimenti.get(0).getVoceIncasso());

        MovimentoContabile movimento=null;
        for (int i=0; i<listaMovimenti.size(); i++){
            movimento = listaMovimenti.get(i);
            for (int j=0; j<eventi.size(); j++){
                if (movimento.getIdEvento().longValue()==eventi.get(j).getId().longValue()){
                    movimento.setEvento(eventi.get(j));
                    break;
                }
            }
        }

        DirettoreEsecuzione direttoreEsecuzione = programmaMusicaleDAO.getDirettoreEsecuzione(idProgrammaMusicale);
        Cartella cartella = programmaMusicaleDAO.getCartellaById(listaMovimenti.get(0).getIdCartella());

        programmaMusicale.setOpere(listaOpere);
        programmaMusicale.setMovimenti(listaMovimenti);
        programmaMusicale.setEventi(eventi);
        programmaMusicale.setDirettoreEsecuzione(direttoreEsecuzione);
        programmaMusicale.setCartella(cartella);
        programmaMusicale.setDataRientroPM(listaMovimenti.get(0).getDataRientroPMStr());

        // Inizializzo gli importi aggregati ricalcolati
        ImportoPM importo = new ImportoPM();

        // Calcolo degli importi delle singole cedole
        if (programmaMusicale.getMovimenti().get(0).getImportiRicalcolati()!=null && programmaMusicale.getMovimenti().get(0).getImportiRicalcolati().size()>0){

            // Distingue le tipologie di ricalcolo in base al tipo di voce di incasso
            Double importoUnitario=0.0D;
            Map<String, TrattamentoPM> mappaVociIncasso = (Map<String, TrattamentoPM>)Constants.CONFIGURATIONS.get("vociIncasso");

            TrattamentoPM tipoVoceIncasso = mappaVociIncasso.get(programmaMusicale.getVoceIncasso());

            ImportoRicalcolato importoRicalcolato = programmaMusicale.getMovimenti().get(0).getImportiRicalcolati().get(0);

            if (importoRicalcolato.getImportoSingolaCedola()!=null)
                importoUnitario=importoRicalcolato.getImportoSingolaCedola();

            if (tipoVoceIncasso.getTipo().equalsIgnoreCase("A") || tipoVoceIncasso.getTipo().equalsIgnoreCase("B") || tipoVoceIncasso.getTipo().equalsIgnoreCase("D")) {
                // Calcolo ProQuota
                for (Utilizzazione cedola : listaOpere){
                    cedola.setImportoCedola(importoUnitario);
                }
            } else {
                // Ricalcolo Pro Durata
                for (Utilizzazione cedola : listaOpere){
                    cedola.setImportoCedola(importoUnitario*cedola.getDurata());
                }
            }
        }

        Double importoSUN = programmaMusicaleDAO.getImportoPMSUNAggregato(idProgrammaMusicale);
        importo.setImportoAggregatoSUN(importoSUN);

        Double importoTotaleRicalcolato = programmaMusicaleDAO.getImportoPMSIADAAggregato(idProgrammaMusicale);

        if (importoTotaleRicalcolato == null){
            List<ImportoRicalcolato> importiRicalcolati = programmaMusicaleDAO.getImportiPMRicalcolati(idProgrammaMusicale);
            importo.setImportiRicalcolati(importiRicalcolati);
        }else{
            importo.setImportoTotaleRicalcolato(importoTotaleRicalcolato);
        }
        programmaMusicale.setImporto(importo);

        return programmaMusicale;
    }


    public Long getMovimentiContabili(Long periodoContabileInizio, Long periodoContabileFine, String tipologiaPm){
       return programmaMusicaleDAO.getMovimentiContabili(periodoContabileInizio, periodoContabileFine, tipologiaPm);
    }

    public Integer updateContabilitaOriginale(Integer nuovaContabilita, List<Object> listaMovimenti){
        return programmaMusicaleDAO.updateContabilitaOriginale(nuovaContabilita, listaMovimenti);
    }

}
