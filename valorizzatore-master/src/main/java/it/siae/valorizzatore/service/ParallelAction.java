package it.siae.valorizzatore.service;

import it.siae.valorizzatore.dao.IProgrammaMusicaleDAO;
import it.siae.valorizzatore.model.MovimentazioneLogisticaPM;
import it.siae.valorizzatore.model.ProgrammaMusicale;
import it.siae.valorizzatore.utility.Constants;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.RecursiveAction;

/**
 * Created by idilello on 10/26/16.
 */
public class ParallelAction extends RecursiveAction{

    private static final long serialVersionUID = 1L;
    private static final int THRESHOLD = 100;

    private ArrayList<UUID> ids;
    private int fromIndex;
    private int toIndex;

    private IProgrammaMusicaleDAO programmaMusicaleDAO ;
    List<Object> listaPMFlussoGuida;

    public ParallelAction(ArrayList<UUID> ids, IProgrammaMusicaleDAO programmaMusicaleDAO, List<Object> listaPMFlussoGuida, int fromIndex, int toIndex) {
        this.ids = ids;
        this.fromIndex = fromIndex;
        this.toIndex = toIndex;
        this.programmaMusicaleDAO = programmaMusicaleDAO;
        this.listaPMFlussoGuida = listaPMFlussoGuida;
    }

    private boolean computationSetIsSamllEnough() {
        return (toIndex - fromIndex) < THRESHOLD;
    }


    @Override
    protected void compute() {

        if (computationSetIsSamllEnough()) {
            computeDirectly();
        } else {
            int leftToIndex = fromIndex + (toIndex - fromIndex) / 2;
            ParallelAction leftPartitioner = new ParallelAction(ids, programmaMusicaleDAO, listaPMFlussoGuida, fromIndex, leftToIndex);
            ParallelAction rightPartitioner = new ParallelAction(ids, programmaMusicaleDAO, listaPMFlussoGuida, leftToIndex + 1, toIndex);

            invokeAll(leftPartitioner, rightPartitioner);
        }
    }


    private void computeDirectly() {

        final List<UUID> subList = ids.subList(fromIndex, toIndex);

        ProgrammaMusicale programmaMusicale = null;
        MovimentazioneLogisticaPM movimentazioneLogisticaPM = null;

        System.out.println("----> ComputeDirectly: "+new Date()+": Begin");

        for (int i=0; i<listaPMFlussoGuida.size(); i++){

            programmaMusicale = (ProgrammaMusicale)listaPMFlussoGuida.get(i);

            movimentazioneLogisticaPM=new MovimentazioneLogisticaPM();

            movimentazioneLogisticaPM.setFlagAttivo('Y');
            movimentazioneLogisticaPM.setFlagEsclusoInvioNeed('N');
            movimentazioneLogisticaPM.setFlagEsclusoRicalcolo('N');
            movimentazioneLogisticaPM.setStato(Constants.INVIATO_A_NEED);
            movimentazioneLogisticaPM.setIdProgrammaMusicale(programmaMusicale.getId());

            modifyTheData(movimentazioneLogisticaPM);

        }
        System.out.println("----> ComputeDirectly: "+new Date()+": End");
    }

    private void modifyTheData(MovimentazioneLogisticaPM movimentazioneLogisticaPM) {
        programmaMusicaleDAO.insertMovimentazioneLogisticaPM(movimentazioneLogisticaPM);
    }
}
