package it.siae.valorizzatore.service;

import it.siae.valorizzatore.dao.IConfigurationDAO;

import it.siae.valorizzatore.dao.IEsecuzioneDAO;;
import it.siae.valorizzatore.dao.IProgrammaMusicaleDAO;
import it.siae.valorizzatore.model.*;
import it.siae.valorizzatore.model.security.ReportPage;
import it.siae.valorizzatore.utility.Constants;
import it.siae.valorizzatore.utility.HttpUtil;
import it.siae.valorizzatore.utility.ReportCache;
import it.siae.valorizzatore.utility.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.io.*;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by idilello on 6/6/16.
 */
@Service("consoleService")
public class ConsoleRicalcoloService implements IConsoleRicalcoloService {


    @Autowired
    @Qualifier("esecuzioneDAO")
    IEsecuzioneDAO esecuzioneDAO;

    @Autowired
    @Qualifier("configurationDAO")
    IConfigurationDAO configurationDAO;

    @Autowired
    @Qualifier("programmaMusicaleDAO")
    IProgrammaMusicaleDAO programmaMusicaleDAO;

    @Autowired
    @Qualifier("jCAClientService")
    IJCAClientService jCAClientService;


    @Autowired
    private ITraceService traceService;

    public Long[] getPeriodoAAAAMM(String parameterName){

        Long[] result = new Long[2];

        String value = configurationDAO.getParameter(parameterName);

        Long longValue = Long.parseLong(value);

        result[0] = Long.parseLong(Long.toString(longValue).substring(0, 4));
        result[1] = Long.parseLong(Long.toString(longValue).substring(4, 6));

        return result;
    }


    public List<EsecuzioneRicalcolo> getEsecuzioni(){
        return esecuzioneDAO.getEsecuzioni();
    }

    //  -----------------------------------------------------------
    //  ------------------------ Start Engine----------------------
    //  -----------------------------------------------------------

    public String startProcess(){

        String esito = "ACTIVE";

        try {

            /*
             * Legge il numero di Engine configurati
             * Per ogni Engine i:
             *         - Legge l' indirizzo (ENGINE_ADDRESS_i)
             *         - se è configurato per il server stesso prende la porta e lo fa partire, passanodo porta e indice
             * Invoca il servizio remoto per far partire gli engine sull'altro server (se configurati)
             */
            Engine engine = null;

            int numeroEngine = Integer.parseInt(configurationDAO.getParameter(Constants.NUMERO_ENGINE));

            String localHostName = InetAddress.getLocalHost().getHostName();

            for (int i=1; i<=numeroEngine; i++){

                engine = new Engine(configurationDAO.getParameter(Constants.ENGINE_ADDRESS+"_"+i));

                if (localHostName.equalsIgnoreCase(engine.getEngineAddress())) {

                    esito = startLocalProcess(engine.getEnginePort(), i);

                    if (esito.equals("ACTIVE")) {
                        traceService.trace("Ricalcolo", this.getClass().getName(), "startProcess", "Started engine " + i + ":" + engine.getEngineAddress() + ":" + engine.getEnginePort(), Constants.INFO);
                    }

                }else{
                    // Acquisisce il nome logico del serve remoto sul quale invocare la URL di start dell'Engine
                    String logicalAddress = configurationDAO.getLogicalServerAddress(engine.getEngineAddress());

                    esito = startRemoteProcess(logicalAddress, i);
                }
            }

        }catch(Exception e){

            e.printStackTrace();
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);

            traceService.trace("Ricalcolo", this.getClass().getName(), "startProcess", sw.toString(), Constants.ERROR);
        }

        return esito;
    }


    public String startProcess2243(){

        String esito = "ACTIVE";

        try {

            /*
             * Legge il numero di Engine configurati
             * Per ogni Engine i:
             *         - Legge l' indirizzo (ENGINE_ADDRESS_i)
             *         - se è configurato per il server stesso prende la porta e lo fa partire, passanodo porta e indice
             * Invoca il servizio remoto per far partire gli engine sull'altro server (se configurati)
             */
            Engine engine = null;

            int numeroEngine = Integer.parseInt(configurationDAO.getParameter(Constants.NUMERO_ENGINE_2243));

            String localHostName = InetAddress.getLocalHost().getHostName();

            //traceService.trace("Ricalcolo2243", this.getClass().getName(), "startProcess2243", "Numero engine " + numeroEngine , Constants.INFO);

            for (int i=1; i<=numeroEngine; i++){

                engine = new Engine(configurationDAO.getParameter(Constants.ENGINE_ADDRESS_2243+"_"+i));

                if (localHostName.equalsIgnoreCase(engine.getEngineAddress())) {

                    esito = startLocalProcess(engine.getEnginePort(), i);

                    //traceService.trace("Ricalcolo2243", this.getClass().getName(), "startProcess2243", "Local execution - esito: "+esito , Constants.INFO);


                    if (esito.equals("ACTIVE")) {
                        traceService.trace("Ricalcolo2243", this.getClass().getName(), "startProcess2243", "Started engine " + i + ":" + engine.getEngineAddress() + ":" + engine.getEnginePort(), Constants.INFO);
                    }

                }else{
                    // Acquisisce il nome logico del serve remoto sul quale invocare la URL di start dell'Engine
                    String logicalAddress = configurationDAO.getLogicalServerAddress(engine.getEngineAddress());

                    esito = startRemoteProcess2243(logicalAddress, i);
                }
            }

        }catch(Exception e){

            e.printStackTrace();
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);

            traceService.trace("Ricalcolo2243", this.getClass().getName(), "startProcess", sw.toString(), Constants.ERROR);
        }

        return esito;
    }



    public String startLocalProcessFromRemote(int engineIndex){

        String esito = "ACTIVE";

        try {

            String localHostName = InetAddress.getLocalHost().getHostName();

            Engine engine = new Engine(configurationDAO.getParameter(Constants.ENGINE_ADDRESS+"_"+engineIndex));

            //if (localHostName.equalsIgnoreCase(engine.getEngineAddress())) {

            esito = startLocalProcess(engine.getEnginePort(), engineIndex);

            if (esito.equals("ACTIVE")) {
              traceService.trace("Ricalcolo", this.getClass().getName(), "startProcess", "Started engine " + engineIndex + ":" + engine.getEngineAddress() + ":" + engine.getEnginePort(), Constants.INFO);
            }

            //}

        }catch(Exception e){

            e.printStackTrace();
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);

            traceService.trace("Ricalcolo", this.getClass().getName(), "startProcess", sw.toString(), Constants.ERROR);
        }

        return esito;
    }

    public String startLocalProcessFromRemote2243(int engineIndex){

        String esito = "ACTIVE";

        try {

            String localHostName = InetAddress.getLocalHost().getHostName();

            Engine engine = new Engine(configurationDAO.getParameter(Constants.ENGINE_ADDRESS_2243+"_"+engineIndex));

            //if (localHostName.equalsIgnoreCase(engine.getEngineAddress())) {

            esito = startLocalProcess(engine.getEnginePort(), engineIndex);

            if (esito.equals("ACTIVE")) {
                traceService.trace("Ricalcolo2243", this.getClass().getName(), "startProcess", "Started engine " + engineIndex + ":" + engine.getEngineAddress() + ":" + engine.getEnginePort(), Constants.INFO);
            }

            //}

        }catch(Exception e){

            e.printStackTrace();
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);

            traceService.trace("Ricalcolo2243", this.getClass().getName(), "startProcess", sw.toString(), Constants.ERROR);
        }

        return esito;
    }

    public String startLocalProcess(int enginePort, int engineIndex){

        String esito = "ACTIVE";

        try {

            String pathStartCommand=configurationDAO.getParameter(Constants.START_COMMAND);

            String[] command = {"/bin/bash", pathStartCommand, (new Integer(enginePort)).toString(), (new Integer(engineIndex)).toString()};
            ProcessBuilder p = new ProcessBuilder(command);
            Process p2 = p.start();

            //BufferedReader br = new BufferedReader(new InputStreamReader(p2.getInputStream()));
            //String line;
            //System.out.println("Output of running " + command + " is: ");
            //while ((line = br.readLine()) != null) {
            //    System.out.println(line);
            //}

        }catch (Exception e) {
            try {
                String localHostName = InetAddress.getLocalHost().getHostName();
                esito = "Local host: "+localHostName+": "+e.getMessage();
            }catch(Exception e1){}

            e.printStackTrace();
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);

            traceService.trace("Ricalcolo", this.getClass().getName(), "startLocalProcess", sw.toString(), Constants.ERROR);
        }

        return esito;

    }

    public String startRemoteProcess(String address, int engineIndex) {

        String esito = "ACTIVE";

        try {

            String remoteStartEngineUrl = configurationDAO.getParameter(Constants.START_ENGINE_URL);

            remoteStartEngineUrl = remoteStartEngineUrl.replace("SERVER",address)+"?key="+engineIndex;

            String result = HttpUtil.sendGet(remoteStartEngineUrl);

        }catch(Exception e){
            e.printStackTrace();
            try {
                String localHostName = InetAddress.getLocalHost().getHostName();
                esito = "Remote host "+localHostName+": "+e.getMessage();
            }catch(Exception e1){}

            e.printStackTrace();
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);

            traceService.trace("Ricalcolo", this.getClass().getName(), "startRemoteProcess", sw.toString(), Constants.ERROR);
        }

        return esito;
    }

    public String startRemoteProcess2243(String address, int engineIndex) {

        String esito = "ACTIVE";

        try {

            String remoteStartEngineUrl = configurationDAO.getParameter(Constants.START_ENGINE_URL_2243);

            remoteStartEngineUrl = remoteStartEngineUrl.replace("SERVER",address)+"?key="+engineIndex;

            String result = HttpUtil.sendGet(remoteStartEngineUrl);

        }catch(Exception e){
            e.printStackTrace();
            try {
                String localHostName = InetAddress.getLocalHost().getHostName();
                esito = "Remote host "+localHostName+": "+e.getMessage();
            }catch(Exception e1){}

            e.printStackTrace();
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);

            traceService.trace("Ricalcolo2243", this.getClass().getName(), "startRemoteProcess", sw.toString(), Constants.ERROR);
        }

        return esito;
    }
    //  -----------------------------------------------------------
    //  ------------------------ Verifiche ------------------------
    //  -----------------------------------------------------------

    public String checkStatus(){

        String esitoTrace="";
        String esitoAggregato = "ACTIVE";

        List<String> esito = jCAClientService.sendMultipleEngineCommand(Constants.STATUS);

        // Ritorna: ACTIVE/INACTIVE  ACTIVE/INACTIVE
        for (int i=0; i<esito.size(); i++){

            if (esito.get(i) != null && !esito.get(i).equalsIgnoreCase("ACTIVE")) {
                esitoAggregato = esito.get(i);
            }
            esitoTrace=esitoTrace+esito.get(i)+" ";
        }

        traceService.trace("Ricalcolo", this.getClass().getName(), "checkStatus", "Comando "+Constants.STATUS+". Esito: "+esitoTrace, Constants.INFO);

        return esitoAggregato;
    }

    public String checkStatus2243(){

        String esitoTrace="";
        String esitoAggregato = "ACTIVE";

        List<String> esito = jCAClientService.sendMultipleEngineCommand2243(Constants.STATUS_2243);

        // Ritorna: ACTIVE/INACTIVE  ACTIVE/INACTIVE
        for (int i=0; i<esito.size(); i++){

            if (esito.get(i) != null && !esito.get(i).equalsIgnoreCase("ACTIVE")) {
                esitoAggregato = esito.get(i);
            }
            esitoTrace=esitoTrace+esito.get(i)+" ";
        }

        traceService.trace("Ricalcolo2243", this.getClass().getName(), "checkStatus", "Comando "+Constants.STATUS_2243+". Esito: "+esitoTrace, Constants.INFO);

        return esitoAggregato;
    }

    public String deactivate(){

        String esitoTrace="";
        String esitoAggregato = null;

        List<String> esito = jCAClientService.sendMultipleEngineCommand(Constants.DEACTIVATE);

        // Ritorna: OK/KO OK/KO
        for (int i=0; i<esito.size(); i++){
            esitoAggregato=esito.get(i);
            esitoTrace=esitoTrace+esitoAggregato+" ";
        }

        traceService.trace("Ricalcolo", this.getClass().getName(), "deactivate", "Comando "+Constants.DEACTIVATE+". Esito: "+esitoTrace, Constants.INFO);


        return esitoAggregato;
    }

    public String deactivate2243(){

        String esitoTrace="";
        String esitoAggregato = null;

        List<String> esito = jCAClientService.sendMultipleEngineCommand2243(Constants.DEACTIVATE_2243);

        // Ritorna: OK/KO OK/KO
        for (int i=0; i<esito.size(); i++){
            esitoAggregato=esito.get(i);
            esitoTrace=esitoTrace+esitoAggregato+" ";
        }

        traceService.trace("Ricalcolo2243", this.getClass().getName(), "deactivate", "Comando "+Constants.DEACTIVATE_2243+". Esito: "+esitoTrace, Constants.INFO);


        return esitoAggregato;
    }

    //  -----------------------------------------------------------
    //  ------------------------ Ricalcolo ------------------------
    //  -----------------------------------------------------------

    public String startRicalcolo(){

        String esitoTrace="";
        String esitoAggregato = null;

        // Suddivide il numero di PM da ricalcolare in base al numero di Engine configurato
        int numeroEngine = Integer.parseInt(configurationDAO.getParameter(Constants.NUMERO_ENGINE));
        Long dataInizioRicalcolo = Long.parseLong(configurationDAO.getParameter("INIZIO_PERIODO_RICALCOLO"));
        Long dataFineRicalcolo = Long.parseLong(configurationDAO.getParameter("FINE_PERIODO_RICALCOLO"));
        String tipologiaPM = configurationDAO.getParameter("TIPOLOGIA_PM");

        if (tipologiaPM!=null && tipologiaPM.equalsIgnoreCase("Tutte"))
            tipologiaPM=null;

        Long numeroMovimenti = programmaMusicaleDAO.getMovimentiContabili(dataInizioRicalcolo, dataFineRicalcolo, tipologiaPM);
        String[] intervalliEngine = new String[numeroEngine];
        long min=0;
        long max=0;
        long r = numeroMovimenti / numeroEngine;

        for (int i=0; i<numeroEngine; i++){
            min = (r*i)+1;
            max = (r*(i+1));
            // Assegna eventuali movimenti residue all'ultimo engine
            if (i==(numeroEngine-1)){
                max=numeroMovimenti;
            }
            intervalliEngine[i]=min+"-"+max;

        }

        // Salva in configurazione la nuova suddivizione dei movimenti da ricalcolare
        for (int i=0; i<numeroEngine; i++){
            Configurazione interval = configurationDAO.getParameterObject(Constants.ENGINE_INTERVAL+"_"+(i+1));
            interval.setParameterValue(intervalliEngine[i]);
            configurationDAO.insertParameterObject(interval);

        }

        Long elaborationId = esecuzioneDAO.getNewEsecuzioneRicalcoloId();

        String newCommand = Constants.START_RICALCOLO+" "+elaborationId;

        // Ritorna: OK/KO OK/KO
        List<String> esito = jCAClientService.sendMultipleEngineCommand(newCommand);

        for (int i=0; i<esito.size(); i++){
            esitoAggregato=esito.get(i);
            esitoTrace=esitoTrace+esitoAggregato+" ";
        }

        traceService.trace("Ricalcolo", this.getClass().getName(), "startRicalcolo", "Comando "+Constants.START_RICALCOLO+" per elaborazione: "+elaborationId+". Esito: "+esitoTrace, Constants.INFO);

        return esitoAggregato;
    }


    public String startRicalcolo2243(){

        String esitoTrace="";
        String esitoAggregato = null;

        // Suddivide il numero di PM da ricalcolare in base al numero di Engine configurato
        int numeroEngine = Integer.parseInt(configurationDAO.getParameter(Constants.NUMERO_ENGINE_2243));
        Long dataInizioRicalcolo = Long.parseLong(configurationDAO.getParameter("INIZIO_PERIODO_RICALCOLO_2243"));
        Long dataFineRicalcolo = Long.parseLong(configurationDAO.getParameter("FINE_PERIODO_RICALCOLO_2243"));
        String tipologiaPM = configurationDAO.getParameter("TIPOLOGIA_PM_2243");

        if (tipologiaPM!=null && tipologiaPM.equalsIgnoreCase("Tutte"))
          tipologiaPM=null;

        Long numeroMovimenti = programmaMusicaleDAO.getMovimentiContabili2243(dataInizioRicalcolo, dataFineRicalcolo, tipologiaPM);
        String[] intervalliEngine = new String[numeroEngine];
        long min=0;
        long max=0;
        long r = numeroMovimenti / numeroEngine;

        for (int i=0; i<numeroEngine; i++){
            min = (r*i)+1;
            max = (r*(i+1));
            // Assegna eventuali movimenti residue all'ultimo engine
            if (i==(numeroEngine-1)){
                max=numeroMovimenti;
            }
            intervalliEngine[i]=min+"-"+max;

        }

        // Salva in configurazione la nuova suddivizione dei movimenti da ricalcolare
        for (int i=0; i<numeroEngine; i++){
            Configurazione interval = configurationDAO.getParameterObject(Constants.ENGINE_INTERVAL_2243+"_"+(i+1));
            interval.setParameterValue(intervalliEngine[i]);
            configurationDAO.insertParameterObject(interval);

        }

        Long elaborationId = esecuzioneDAO.getNewEsecuzioneRicalcoloId();

        String newCommand = Constants.START_RICALCOLO_2243+" "+elaborationId;

        // Ritorna: OK/KO OK/KO
        List<String> esito = jCAClientService.sendMultipleEngineCommand2243(newCommand);

        for (int i=0; i<esito.size(); i++){
            esitoAggregato=esito.get(i);
            esitoTrace=esitoTrace+esitoAggregato+" ";
        }

        traceService.trace("Ricalcolo2243", this.getClass().getName(), "startRicalcolo", "Comando "+Constants.START_RICALCOLO_2243+" per elaborazione: "+elaborationId+". Esito: "+esitoTrace, Constants.INFO);

        return esitoAggregato;
    }


    public InformazioniPM getAvanzamentoElaborazione(){

        String esitoAggregato = null;
        InformazioniPM informazioniPM = new  InformazioniPM();

        // Formato esito: 1300-600-100-0 (Totali, Elaborati, Sospesi, Errori, Attivo/Terminato)
        List<String> esito = jCAClientService.sendMultipleEngineCommand(Constants.INFORMAZIONI_ELABORAZIONE);

        boolean isTerminated = true;

        //esito = "1300-600-100-0";
        for (int i=0; i<esito.size(); i++) {

            esitoAggregato=esito.get(i);

            String[] info = esitoAggregato.split("-");

            if (info.length == 5 && Long.parseLong(info[1]) >= 0L) {

                informazioniPM.setError("0");

                informazioniPM.setTotalePM(informazioniPM.getTotalePM()+Long.parseLong(info[0]));
                informazioniPM.setTotaleElaborati(informazioniPM.getTotaleElaborati()+Long.parseLong(info[1]));
                informazioniPM.setTotaleSospesi(informazioniPM.getTotaleSospesi()+Long.parseLong(info[2]));
                informazioniPM.setTotaleErrori(informazioniPM.getTotaleErrori()+Long.parseLong(info[3]));

                // Se almeno uno degli Engine è in esecuzione, considero l'intero blocco in esecuzione
                if (Long.parseLong(info[4])==0L){
                    isTerminated = false;
                }

            } else {

                informazioniPM.setError("-1");
                informazioniPM.setTotalePM(0L);
                informazioniPM.setTotaleElaborati(0L);
                informazioniPM.setTotaleSospesi(0L);
                informazioniPM.setTotaleErrori(0L);
                informazioniPM.setPercentuale(100L);

                // 1 - Sospeso, 0 - Attivo
                informazioniPM.setSuspende(1L);

            }
        }

        if (informazioniPM.getTotalePM() != 0) {
            informazioniPM.setPercentuale((informazioniPM.getTotaleElaborati() * 100) / informazioniPM.getTotalePM());
        }else {
            informazioniPM.setPercentuale(0L);
        }

        // 0 - Attivo, 1 - Terminato
        if (isTerminated)
            informazioniPM.setTerminate(1L);
        else
            informazioniPM.setTerminate(0L);

        return informazioniPM;

    }

    public InformazioniPM getAvanzamentoElaborazione2243(){

        String esitoAggregato = null;
        InformazioniPM informazioniPM = new  InformazioniPM();

        // Formato esito: 1300-600-100-0 (Totali, Elaborati, Sospesi, Errori, Attivo/Terminato)
        List<String> esito = jCAClientService.sendMultipleEngineCommand2243(Constants.INFORMAZIONI_ELABORAZIONE_2243);

        boolean isTerminated = true;

        //esito = "1300-600-100-0";
        for (int i=0; i<esito.size(); i++) {

            esitoAggregato=esito.get(i);

            String[] info = esitoAggregato.split("-");

            if (info.length == 5 && Long.parseLong(info[1]) >= 0L) {

                informazioniPM.setError("0");

                informazioniPM.setTotalePM(informazioniPM.getTotalePM()+Long.parseLong(info[0]));
                informazioniPM.setTotaleElaborati(informazioniPM.getTotaleElaborati()+Long.parseLong(info[1]));
                informazioniPM.setTotaleSospesi(informazioniPM.getTotaleSospesi()+Long.parseLong(info[2]));
                informazioniPM.setTotaleErrori(informazioniPM.getTotaleErrori()+Long.parseLong(info[3]));

                // Se almeno uno degli Engine è in esecuzione, considero l'intero blocco in esecuzione
                if (Long.parseLong(info[4])==0L){
                    isTerminated = false;
                }

            } else {

                informazioniPM.setError("-1");
                informazioniPM.setTotalePM(0L);
                informazioniPM.setTotaleElaborati(0L);
                informazioniPM.setTotaleSospesi(0L);
                informazioniPM.setTotaleErrori(0L);
                informazioniPM.setPercentuale(100L);

                // 1 - Sospeso, 0 - Attivo
                informazioniPM.setSuspende(1L);

            }
        }

        if (informazioniPM.getTotalePM() != 0) {
            informazioniPM.setPercentuale((informazioniPM.getTotaleElaborati() * 100) / informazioniPM.getTotalePM());
        }else {
            informazioniPM.setPercentuale(0L);
        }

        // 0 - Attivo, 1 - Terminato
        if (isTerminated)
            informazioniPM.setTerminate(1L);
        else
            informazioniPM.setTerminate(0L);

        return informazioniPM;

    }


    //  ---------------------------------------------------------------
    //  ------------------------ Campionamento ------------------------
    //  ---------------------------------------------------------------

    public String startCampionamento(){

        String esito = jCAClientService.sendCommand(Constants.START_CAMPIONAMENTO);

        traceService.trace("Campionamento", this.getClass().getName(), "startCampionamento", "Comando "+Constants.START_CAMPIONAMENTO+". Esito: "+esito, Constants.INFO);

        return esito;
    }

    public InformazioniPM getAvanzamentoElaborazioneCampionamento(){

        InformazioniPM informazioniPM = new  InformazioniPM();

        // Formato esito: 1300-600-0-0 (Totali, Elaborati, Sospesi, Errori)
        String esito = jCAClientService.sendCommand(Constants.INFORMAZIONI_ELABORAZIONE_CAMPIONAMENTO);

        //esito = "1300-600-100-0";

        String[] info = esito.split("-");

        if (info.length==5 && Long.parseLong(info[1])>=0L){
            informazioniPM.setTotalePM(Long.parseLong(info[0]));
            informazioniPM.setTotaleElaborati(Long.parseLong(info[1]));
            informazioniPM.setTotaleSospesi(Long.parseLong(info[2]));
            informazioniPM.setTotaleErrori(Long.parseLong(info[3]));
            // 1 - Sospeso, 0 - Attivo
            informazioniPM.setTerminate(Long.parseLong(info[4]));

            if (informazioniPM.getTotalePM()!=0)
                informazioniPM.setPercentuale((informazioniPM.getTotaleElaborati()*100)/ informazioniPM.getTotalePM());
            else
                informazioniPM.setPercentuale(0L);
            informazioniPM.setError("0");
        }else{
            informazioniPM.setError("-1");
            informazioniPM.setTotalePM(0L);
            informazioniPM.setTotaleElaborati(0L);
            informazioniPM.setTotaleSospesi(0L);
            informazioniPM.setTotaleErrori(0L);
            informazioniPM.setPercentuale(100L);
            // 1 - Sospeso, 0 - Attivo
            informazioniPM.setSuspende(1L);
        }

        return informazioniPM;

    }

    //  ---------------------------------------------------------------
    //  ------------------------ Aggregazione -------------------------
    //  ---------------------------------------------------------------

    public String startAggregazioneSiadaOld(){

        String esito = jCAClientService.sendCommand(Constants.START_AGGREGAZIONE);

        traceService.trace("Aggregazione Siada", this.getClass().getName(), "startAggregazione", "Comando "+Constants.START_AGGREGAZIONE+". Esito: "+esito, Constants.INFO);

        return esito;
    }

    public String startAggregazioneSiada(Long elaborazioneSIADAId){

        String esitoTrace="";
        String esitoAggregato = null;

        // Suddivide il numero di PM da aggregare in base al numero di Engine configurato
        int numeroEngine = Integer.parseInt(configurationDAO.getParameter(Constants.NUMERO_ENGINE));

        EsecuzioneRicalcolo esecuzione = esecuzioneDAO.findEsecuzioneById(elaborazioneSIADAId);

        Long numeroAggregazioni = esecuzioneDAO.getNumeroMovimentazioniAggregate(esecuzione);

        esecuzioneDAO.deleteMovimentoSiada();

        String[] intervalliEngine = new String[numeroEngine];
        long min=0;
        long max=0;
        long r = numeroAggregazioni / numeroEngine;

        for (int i=0; i<numeroEngine; i++){
            min = (r*i)+1;
            max = (r*(i+1));
            // Assegna eventuali movimenti residue all'ultimo engine
            if (i==(numeroEngine-1)){
                max=numeroAggregazioni;
            }
            intervalliEngine[i]=min+"-"+max;

        }

        // Salva in configurazione la nuova suddivizione degli aggregati da produrre
        for (int i=0; i<numeroEngine; i++){
            Configurazione interval = configurationDAO.getParameterObject(Constants.AGGREGATE_INTERVAL+"_"+(i+1));
            interval.setParameterValue(intervalliEngine[i]);
            configurationDAO.insertParameterObject(interval);

        }

        String newCommand = Constants.START_AGGREGAZIONE+" "+elaborazioneSIADAId;

        // Ritorna: OK/KO OK/KO
        List<String> esito = jCAClientService.sendMultipleEngineCommand(newCommand);

        for (int i=0; i<esito.size(); i++){
            esitoAggregato=esito.get(i);
            esitoTrace=esitoTrace+esitoAggregato+" ";
        }

        traceService.trace("Aggregazione Siada", this.getClass().getName(), "startAggregazione", "Comando "+Constants.START_AGGREGAZIONE+" per elaborazione: "+elaborazioneSIADAId+". Esito: "+esitoTrace, Constants.INFO);

        return esitoAggregato;

    }


    public String startAggregazioneSiada2243(Long elaborazioneSIADAId){

        String esitoTrace="";
        String esitoAggregato = null;

        // Suddivide il numero di PM da aggregare in base al numero di Engine configurato
        int numeroEngine = Integer.parseInt(configurationDAO.getParameter(Constants.NUMERO_ENGINE_2243));

        EsecuzioneRicalcolo esecuzione = esecuzioneDAO.findEsecuzioneById(elaborazioneSIADAId);

        Long numeroAggregazioni = esecuzioneDAO.getNumeroMovimentazioniAggregate2243(esecuzione);

//        esecuzioneDAO.deleteMovimentoSiada2243();

        String[] intervalliEngine = new String[numeroEngine];
        long min=0;
        long max=0;
        long r = numeroAggregazioni / numeroEngine;

        for (int i=0; i<numeroEngine; i++){
            min = (r*i)+1;
            max = (r*(i+1));
            // Assegna eventuali movimenti residue all'ultimo engine
            if (i==(numeroEngine-1)){
                max=numeroAggregazioni;
            }
            intervalliEngine[i]=min+"-"+max;

        }

        // Salva in configurazione la nuova suddivizione degli aggregati da produrre
        for (int i=0; i<numeroEngine; i++){
            Configurazione interval = configurationDAO.getParameterObject(Constants.AGGREGATE_INTERVAL_2243+"_"+(i+1));
            interval.setParameterValue(intervalliEngine[i]);
            configurationDAO.insertParameterObject(interval);
        }

        String newCommand = Constants.START_AGGREGAZIONE_2243+" "+elaborazioneSIADAId;

        // Ritorna: OK/KO OK/KO
        List<String> esito = jCAClientService.sendMultipleEngineCommand2243(newCommand);

        for (int i=0; i<esito.size(); i++){
            esitoAggregato=esito.get(i);
            esitoTrace=esitoTrace+esitoAggregato+" ";
        }

        traceService.trace("Aggregazione Siada2243", this.getClass().getName(), "startAggregazione2243", "Comando "+Constants.START_AGGREGAZIONE_2243+" per elaborazione: "+elaborazioneSIADAId+". Esito: "+esitoTrace, Constants.INFO);

        return esitoAggregato;

    }

    public InformazioniPM getAvanzamentoAggregazione(){

        String esitoAggregato = null;
        InformazioniPM informazioniPM = new  InformazioniPM();

        // Formato esito: 1300-600-0-0 (Totali, Elaborati, Sospesi, Errori)
        //String esito = jCAClientService.sendCommand(Constants.INFORMAZIONI_ELABORAZIONE_AGGREGAZIONE);
        List<String> esito = jCAClientService.sendMultipleEngineCommand(Constants.INFORMAZIONI_ELABORAZIONE_AGGREGAZIONE);

        //esito = "1300-600-100-0";

        boolean isTerminated = true;

        //esito = "1300-600-100-0";
        for (int i=0; i<esito.size(); i++) {

            esitoAggregato = esito.get(i);

            String[] info = esitoAggregato.split("-");

            if (info.length == 5 && Long.parseLong(info[1]) >= 0L) {
                informazioniPM.setTotalePM(informazioniPM.getTotalePM()+Long.parseLong(info[0]));
                informazioniPM.setTotaleElaborati(informazioniPM.getTotaleElaborati()+Long.parseLong(info[1]));
                informazioniPM.setTotaleSospesi(informazioniPM.getTotaleSospesi()+Long.parseLong(info[2]));
                informazioniPM.setTotaleErrori(informazioniPM.getTotaleErrori()+Long.parseLong(info[3]));
                // 1 - Sospeso, 0 - Attivo
                informazioniPM.setTerminate(Long.parseLong(info[4]));

                if (informazioniPM.getTotalePM() != 0)
                    informazioniPM.setPercentuale((informazioniPM.getTotaleElaborati() * 100) / informazioniPM.getTotalePM());
                else
                    informazioniPM.setPercentuale(0L);

                informazioniPM.setError("0");

            } else {
                informazioniPM.setError("-1");
                informazioniPM.setTotalePM(0L);
                informazioniPM.setTotaleElaborati(0L);
                informazioniPM.setTotaleSospesi(0L);
                informazioniPM.setTotaleErrori(0L);
                informazioniPM.setPercentuale(100L);
                // 1 - Sospeso, 0 - Attivo
                informazioniPM.setSuspende(1L);
            }

        }

        return informazioniPM;

    }

    public InformazioniPM getAvanzamentoAggregazione2243(){

        String esitoAggregato = null;
        InformazioniPM informazioniPM = new  InformazioniPM();

        // Formato esito: 1300-600-0-0 (Totali, Elaborati, Sospesi, Errori)
        //String esito = jCAClientService.sendCommand(Constants.INFORMAZIONI_ELABORAZIONE_AGGREGAZIONE);
        List<String> esito = jCAClientService.sendMultipleEngineCommand2243(Constants.INFORMAZIONI_ELABORAZIONE_AGGREGAZIONE_2243);

        //esito = "1300-600-100-0";

        boolean isTerminated = true;

        System.out.println("-----> getAvanzamentoAggregazione2243.size "+esito.size());

        //esito = "1300-600-100-0";
        for (int i=0; i<esito.size(); i++) {

            esitoAggregato = esito.get(i);

            System.out.println("-----> getAvanzamentoAggregazione2243.esito_aggregato("+i+") "+esitoAggregato);

            String[] info = esitoAggregato.split("-");

            if (info.length == 5 && Long.parseLong(info[1]) >= 0L) {
                informazioniPM.setTotalePM(informazioniPM.getTotalePM()+Long.parseLong(info[0]));
                informazioniPM.setTotaleElaborati(informazioniPM.getTotaleElaborati()+Long.parseLong(info[1]));
                informazioniPM.setTotaleSospesi(informazioniPM.getTotaleSospesi()+Long.parseLong(info[2]));
                informazioniPM.setTotaleErrori(informazioniPM.getTotaleErrori()+Long.parseLong(info[3]));
                // 1 - Sospeso, 0 - Attivo
                informazioniPM.setTerminate(Long.parseLong(info[4]));

                if (informazioniPM.getTotalePM() != 0)
                    informazioniPM.setPercentuale((informazioniPM.getTotaleElaborati() * 100) / informazioniPM.getTotalePM());
                else
                    informazioniPM.setPercentuale(0L);

                informazioniPM.setError("0");

            } else {
                informazioniPM.setError("-1");
                informazioniPM.setTotalePM(0L);
                informazioniPM.setTotaleElaborati(0L);
                informazioniPM.setTotaleSospesi(0L);
                informazioniPM.setTotaleErrori(0L);
                informazioniPM.setPercentuale(100L);
                // 1 - Sospeso, 0 - Attivo
                informazioniPM.setSuspende(1L);
            }

        }

        System.out.println("-----> getAvanzamentoAggregazione2243.informazioniPM "+informazioniPM);

        return informazioniPM;

    }


    public int setDataCongelamentoEsecuzione(Long elaborazioneSIADAId){

        int result = 0;

        result = esecuzioneDAO.setDataCongelamentoEsecuzione(elaborazioneSIADAId);

        if (result == 0){
            traceService.trace("Ricalcolo", this.getClass().getName(), "setDataCongelamentoEsecuzione", "Fissata data congelamento per ID Esecuzione: "+elaborazioneSIADAId, Constants.INFO);
        } else {
            traceService.trace("Ricalcolo", this.getClass().getName(), "setDataCongelamentoEsecuzione", "Problemi nel fissare la data di congelamento. Errore: "+result, Constants.ERROR);
        }

        return result;
    }

    public EsecuzioneRicalcolo getEsecuzioneRicalcolo(Long elaborazioneSIADAId){

        return esecuzioneDAO.findEsecuzioneById(elaborazioneSIADAId);

    }

    public ReportPage getListaPM(Long idEsecuzioneRicalcolo, Long idEvento, String numeroFattura, String voceIncasso, String numeroProgrammaMusicale,
                                 String inizioPeriodoContabile, String finePeriodoContabile, Integer page, String tipoSospensione){

        List<String> parameters = new ArrayList<String>();
        String reportName = null;

        if (idEsecuzioneRicalcolo!=null)
            parameters.add(idEsecuzioneRicalcolo.toString());
        else
            parameters.add(null);
        if (idEvento!=null)
            parameters.add(idEvento.toString());
        else
            parameters.add(null);

        parameters.add(numeroFattura);
        parameters.add(voceIncasso);
        parameters.add(numeroProgrammaMusicale);
        parameters.add(inizioPeriodoContabile);
        parameters.add(finePeriodoContabile);
        parameters.add(tipoSospensione);

        if (tipoSospensione!=null && tipoSospensione.equalsIgnoreCase(Constants.SOSPESI)){
            reportName = ReportCache.REPORT_IMPORTI_SOSPESI;
        } else
        if (tipoSospensione!=null && tipoSospensione.equalsIgnoreCase(Constants.ERRORE)){
            reportName = ReportCache.REPORT_IMPORTI_ERRORE;
        }else{
            reportName = ReportCache.REPORT_IMPORTI_RICALCOLATI;
        }

        String key = ReportCache.generateKey(reportName, parameters);

        ReportPage reportPage = ReportCache.getReport(key);

        // Verifica presenza del Report in Cache.
        if (reportPage != null){

            // Se il report è già presente in cache, non esegue la query per prendere la pagina interessata, ma usa i dati
            // già presenti
            reportPage = ReportCache.setCurrentReportPage(reportPage, page);

        }else{

            // Se il report non è presente in cache, esegue la query per acquisire i dati del report e lo restituisce, dopo
            // aver aggiornato la cache
            // 1. RICALCOLATI
            // 2. SOSPESI
            // 3. ERRORE
            if (tipoSospensione!=null && tipoSospensione.equalsIgnoreCase(Constants.SOSPESI)){
                reportPage = esecuzioneDAO.getListaPMSospesi(idEsecuzioneRicalcolo, idEvento, numeroFattura, voceIncasso, numeroProgrammaMusicale, inizioPeriodoContabile, finePeriodoContabile, page, Constants.SOSPESI);
            } else
            if (tipoSospensione!=null && tipoSospensione.equalsIgnoreCase(Constants.ERRORE)){
                reportPage = esecuzioneDAO.getListaPMSospesi(idEsecuzioneRicalcolo, idEvento, numeroFattura, voceIncasso, numeroProgrammaMusicale, inizioPeriodoContabile, finePeriodoContabile, page, Constants.ERRORE);
            }else{
                reportPage = esecuzioneDAO.getListaPM(idEsecuzioneRicalcolo, idEvento, numeroFattura, voceIncasso, numeroProgrammaMusicale, inizioPeriodoContabile, finePeriodoContabile, page);
            }

            ReportCache.putReport(reportName, key, reportPage);
        }

        traceService.trace("Ricalcolo", this.getClass().getName(), "getListaPM", "Visualizzazione lista PM: "+tipoSospensione, Constants.INFO);

        return reportPage;

    }

}
