package it.siae.valorizzatore.service;

import it.siae.valorizzatore.dao.IConfigurationDAO;
import it.siae.valorizzatore.model.Circoscrizione;
import it.siae.valorizzatore.model.Configurazione;
import it.siae.valorizzatore.utility.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("configurationService")
public class ConfigurationService implements IConfigurationService{

    @Autowired
    @Qualifier("configurationDAO")
    IConfigurationDAO configurationDAO;

    @Autowired
    private ITraceService traceService;

    public String getParameter(String parameterName){
        return configurationDAO.getParameter(parameterName);
    }


    public void insertParameter(String parameterName, String parameterValue){
         configurationDAO.insertParameter(parameterName, parameterValue);
    }

    public Configurazione getParameterObject(String parameterName){
        return configurationDAO.getParameterObject(parameterName);
    }

    public void insertParameterObject(Configurazione configurazione){
        configurationDAO.insertParameterObject(configurazione);
    }

    public List<Circoscrizione> getCircoscrizioni(){
        return configurationDAO.getCircoscrizioni();
    }

    public void updateConfigurazione(String annoFinePeriodoRipartizione, String meseFinePeriodoRipartizione,
                                     String annoInizioPeriodoContabile, String meseInizioPeriodoContabile,
                                     String annoFinePeriodoContabile, String meseFinePeriodoContabile,
                                     String tipologiaPM, String campionaDigitali,
                                     String annoFinePeriodoRipartizione2243, String meseFinePeriodoRipartizione2243,
                                     String annoInizioPeriodoContabile2243, String meseInizioPeriodoContabile2243,
                                     String annoFinePeriodoContabile2243, String meseFinePeriodoContabile2243,
                                     String tipologiaPM2243){

        if (meseFinePeriodoRipartizione!=null && meseFinePeriodoRipartizione.length()==1)
            meseFinePeriodoRipartizione="0"+meseFinePeriodoRipartizione;
        if (meseInizioPeriodoContabile!=null && meseInizioPeriodoContabile.length()==1)
            meseInizioPeriodoContabile="0"+meseInizioPeriodoContabile;
        if (meseFinePeriodoContabile!=null && meseFinePeriodoContabile.length()==1)
            meseFinePeriodoContabile="0"+meseFinePeriodoContabile;

        if (meseFinePeriodoRipartizione2243!=null && meseFinePeriodoRipartizione2243.length()==1)
            meseFinePeriodoRipartizione2243="0"+meseFinePeriodoRipartizione2243;
        if (meseInizioPeriodoContabile2243!=null && meseInizioPeriodoContabile2243.length()==1)
            meseInizioPeriodoContabile2243="0"+meseInizioPeriodoContabile2243;
        if (meseFinePeriodoContabile2243!=null && meseFinePeriodoContabile2243.length()==1)
            meseFinePeriodoContabile2243="0"+meseFinePeriodoContabile2243;

        if (annoInizioPeriodoContabile!=null && meseInizioPeriodoContabile!=null)
         configurationDAO.insertParameter(Constants.INIZIO_PERIODO_RICALCOLO, annoInizioPeriodoContabile+meseInizioPeriodoContabile);
        if (annoFinePeriodoContabile!=null && meseFinePeriodoContabile!=null)
         configurationDAO.insertParameter(Constants.FINE_PERIODO_RICALCOLO, annoFinePeriodoContabile+meseFinePeriodoContabile);
        if (annoFinePeriodoRipartizione!=null && meseFinePeriodoRipartizione!=null)
         configurationDAO.insertParameter(Constants.FINE_PERIODO_RIPARTIZIONE, annoFinePeriodoRipartizione+meseFinePeriodoRipartizione);
        if (tipologiaPM!=null)
         configurationDAO.insertParameter(Constants.TIPOLOGIA_PM, tipologiaPM);
        if (campionaDigitali!=null)
         configurationDAO.insertParameter(Constants.CAMPIONAMENTO_DIGITALI, campionaDigitali);

        if (annoInizioPeriodoContabile2243!=null && meseInizioPeriodoContabile2243!=null)
         configurationDAO.insertParameter(Constants.INIZIO_PERIODO_RICALCOLO_2243, annoInizioPeriodoContabile2243+meseInizioPeriodoContabile2243);
        if (annoFinePeriodoContabile2243!=null && meseFinePeriodoContabile2243!=null)
         configurationDAO.insertParameter(Constants.FINE_PERIODO_RICALCOLO_2243, annoFinePeriodoContabile2243+meseFinePeriodoContabile2243);
        if (annoFinePeriodoRipartizione2243!=null && meseFinePeriodoRipartizione2243!=null)
         configurationDAO.insertParameter(Constants.FINE_PERIODO_RIPARTIZIONE_2243, annoFinePeriodoRipartizione2243+meseFinePeriodoRipartizione2243);
        if (tipologiaPM2243!=null)
         configurationDAO.insertParameter(Constants.TIPOLOGIA_PM_2243, tipologiaPM2243);

        traceService.trace("Ricalcolo", this.getClass().getName(), "updateConfigurazione", "Aggiornata configurazione", Constants.INFO);
    }

}
