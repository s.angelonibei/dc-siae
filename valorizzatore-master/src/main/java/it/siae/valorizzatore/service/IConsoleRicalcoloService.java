package it.siae.valorizzatore.service;

import it.siae.valorizzatore.model.EsecuzioneRicalcolo;
import it.siae.valorizzatore.model.InformazioniPM;
import it.siae.valorizzatore.model.MovimentoContabile;
import it.siae.valorizzatore.model.security.ReportPage;

import java.util.List;

/**
 * Created by idilello on 6/6/16.
 */
public interface IConsoleRicalcoloService {

    public Long[] getPeriodoAAAAMM(String parameterName);

    public List<EsecuzioneRicalcolo> getEsecuzioni();

    public String checkStatus();

    public String checkStatus2243();

    public String deactivate();

    public String deactivate2243();

    public String startLocalProcess(int enginePort, int engineIndex);

    public String startLocalProcessFromRemote(int engineIndex);

    public String startLocalProcessFromRemote2243(int engineIndex);

    public String startProcess();

    public String startProcess2243();

    public InformazioniPM getAvanzamentoElaborazione();

    public InformazioniPM getAvanzamentoElaborazione2243();

    public String startRicalcolo();

    public String startRicalcolo2243();

    public ReportPage getListaPM(Long idEsecuzioneRicalcolo, Long idEvento, String numeroFattura, String voceIncasso, String numeroProgrammaMusicale, String inizioPeriodoContabile, String finePeriodoContabile, Integer page, String tipoSospensione);

    public String startCampionamento();

    public String startAggregazioneSiada(Long elaborazioneSIADAId);

    public String startAggregazioneSiada2243(Long elaborazioneSIADAId);

    public InformazioniPM getAvanzamentoAggregazione();

    public InformazioniPM getAvanzamentoAggregazione2243();

    public InformazioniPM getAvanzamentoElaborazioneCampionamento();

    public int setDataCongelamentoEsecuzione(Long elaborazioneSIADAId);

    public EsecuzioneRicalcolo getEsecuzioneRicalcolo(Long elaborazioneSIADAId);

}
