package it.siae.valorizzatore.service;

import it.siae.valorizzatore.dao.ICampionamentoDAO;
import it.siae.valorizzatore.dao.IConfigurationDAO;
import it.siae.valorizzatore.dao.IEsecuzioneDAO;
import it.siae.valorizzatore.model.CampionamentoConfig;
import it.siae.valorizzatore.model.EsecuzioneRicalcolo;
import it.siae.valorizzatore.model.InformazioniPM;
import it.siae.valorizzatore.model.TracciamentoApplicativo;
import it.siae.valorizzatore.model.security.ReportPage;
import it.siae.valorizzatore.model.security.User;
import it.siae.valorizzatore.utility.Constants;
import it.siae.valorizzatore.utility.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

;

/**
 * Created by idilello on 6/6/16.
 */
@Service("campionamentoService")
public class CampionamentoService implements ICampionamentoService {

    @Autowired
    @Qualifier("campionamentoDAO")
    ICampionamentoDAO campionamentoDAO;

    public void insertParametriCampionamento(String inizioPeriodoContabile, String dataInizioLavorazione, String dataEstrazioneLotto,
                                             String bsm1, String bsm2, String bsm3, String bsm4, String bsm5,
                                             String concertini1, String concertini2, String concertini3, String concertini4, String concertini5,
                                             String resto5bsm, String resto5concertini, String resto61bsm, String resto61concertini,User user){

        CampionamentoConfig campionamentoConfig = new CampionamentoConfig();

        campionamentoConfig.setContabilita(Long.parseLong(inizioPeriodoContabile));
        campionamentoConfig.setDataEstrazioniLotto(Utility.stringDate(dataEstrazioneLotto, Utility.OracleFormat_D));
        campionamentoConfig.setDataInizioLavorazione(Utility.stringDate(dataInizioLavorazione, Utility.OracleFormat_D));
        campionamentoConfig.setEstrazioniRoma(bsm1+";"+bsm2+";"+bsm3+";"+bsm4+";"+bsm5);
        campionamentoConfig.setEstrazioniMilano(concertini1+";"+concertini2+";"+concertini3+";"+concertini4+";"+concertini5);

        campionamentoConfig.setRestoRoma1(Long.parseLong(resto5bsm));         // Resto Modulo 5
        campionamentoConfig.setRestoRoma2(Long.parseLong(resto61bsm));         // Resto Modulo 61

        campionamentoConfig.setRestoMilano1(Long.parseLong(resto5concertini));       // Resto Modulo 5
        campionamentoConfig.setRestoMilano2(Long.parseLong(resto61concertini));       // Resto Modulo 61

        campionamentoConfig.setUtenteUltimaModifica(user.getUsername());

        campionamentoDAO.insertParametriCampionamento(campionamentoConfig);

    }

    public ReportPage getCampionamenti(Long contabilitaIniziale, Long contabilitaFinale, Integer page){
        return campionamentoDAO.getCampionamenti(contabilitaIniziale, contabilitaFinale, page);
    }

    public CampionamentoConfig getCampionamentoConfigById(Long idCampionamentoConfig){
           return campionamentoDAO.getCampionamentoConfigById(idCampionamentoConfig);
    }

    public ReportPage getStoricoEsecuzioni(Long periodoIniziale, Long periodoFinale, Integer page){

        TracciamentoApplicativo tracciamento = null;

        int recordCount=0;
        long dateDiff = -1;
        long currentDateDiff = -1;

        // Parametri dell'esecuzione
        List<TracciamentoApplicativo> esecuzioniEngine = campionamentoDAO.getEsecuzioniCampionamentoEngine(periodoIniziale, periodoFinale);

        // Utenza dell'esecuzione
        List<TracciamentoApplicativo> esecuzioniStart = campionamentoDAO.getEsecuzioniCampionamentoStart(periodoIniziale, periodoFinale);

        // Cerca gli start equivalenti per acquisire l'utente che ha lanciato il motore
        for (int i=0; i<esecuzioniEngine.size(); i++){
            dateDiff = -1;
            tracciamento = null;

            for (int j=0; j<esecuzioniStart.size(); j++){

                currentDateDiff = Math.abs(esecuzioniEngine.get(i).getDataInserimento().getTime() - esecuzioniStart.get(j).getDataInserimento().getTime());

                if (currentDateDiff < dateDiff || dateDiff<0){
                    dateDiff = currentDateDiff;
                    tracciamento = esecuzioniStart.get(j);
                }
            }

            if (tracciamento != null){
                esecuzioniEngine.get(i).setUserName(tracciamento.getUserName());
            }
        }


        ReportPage reportPage = new ReportPage(esecuzioniEngine, esecuzioniEngine.size(), page);

        if (esecuzioniEngine != null && esecuzioniEngine.size() > 0) {

            recordCount = esecuzioniEngine.size();

            int startRecord = (page-1) * ReportPage.NUMBER_RECORD_PER_PAGE;
            int endRecord = 0;
            if (recordCount>ReportPage.NUMBER_RECORD_PER_PAGE) {
                endRecord = startRecord + ReportPage.NUMBER_RECORD_PER_PAGE;
                if (endRecord > recordCount)
                    endRecord = recordCount -1;
            }else {
                endRecord = recordCount;
            }

            reportPage = new ReportPage(esecuzioniEngine, recordCount, page);

        }

        return reportPage;
    }


}
