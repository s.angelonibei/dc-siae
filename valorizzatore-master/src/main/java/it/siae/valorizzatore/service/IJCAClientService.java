package it.siae.valorizzatore.service;

import java.util.List;

/**
 * Created by idilello on 6/13/16.
 */
public interface IJCAClientService {

    public String sendCommand(String command);

    public List<String> sendMultipleEngineCommand(String command);

    public List<String> sendMultipleEngineCommand2243(String command);

}
