package it.siae.valorizzatore.service;

import it.siae.valorizzatore.model.EventiPagati;
import it.siae.valorizzatore.model.MovimentoContabile;
import it.siae.valorizzatore.model.ProgrammaMusicale;
import it.siae.valorizzatore.model.security.ReportPage;

import java.util.Date;
import java.util.List;

/**
 * Created by idilello on 7/11/16.
 */
public interface IProgrammaMusicaleService {

    public List<String> getTipoProgrammi();

    public ReportPage getFlussoGuida(Long contabilitaIniziale, Long contabilitaFinale, Integer page);

    public ReportPage getFlussoRientrati(Long contabilitaIniziale, Long contabilitaFinale, Integer page);

    public ReportPage saveAndDownloadFlussoGuida(Long contabilitaIniziale, Long contabilitaFinale);

    public int storeFileContant(String filePathName);

    public ReportPage getTracciamentoPM(Date dataIniziale, Date dataFinale, Long numeroPM, Integer page);

    public ReportPage getListaEventiPagati(Long inizioPeriofoContabile, Long finePeriofoContabile, Long evento, String fattura, String reversale, String voceIncasso,
                                           Date dataInizioEvento, String oraInizioEvento, String seprag, String tipoDocumento, String locale, String codiceBA, String presenzaMovimenti, Integer page);

    public List<EventiPagati> getEvento(Long eventoId);

    public ReportPage getListaMovimentazioni(Long inizioPeriodoContabile, Long finePeriodoContabile, Long evento, String fattura, Long reversale, String voceIncasso,
                                             Date dataInizioEvento,Date dataFineEvento, String seprag, String tipoDocumento, String locale, String organizzatore,
                                             String tipoSupporto, String tipoProgramma, String gruppoPrincipale, String titoloOpera, String numeroPM, String permesso,
                                             Integer page);

    public ReportPage getListaAggregatiSiada(Long inizioPeriodoContabile, Long finePeriodoContabile, String voceIncasso, String tipoSupporto, String tipoProgramma, Long numeroPM, Integer page);

    public ReportPage getListaAggregatiSiadaSMC(Long inizioPeriodoContabile, Long finePeriodoContabile, String voceIncasso, String tipoSupporto, String tipoProgramma, Long numeroPM, Integer page);

    public ReportPage getListaAggregatiEventi(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale);

    public List<ReportPage> getListaAggregatiMovimenti(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale, Integer page, String correntiArretrati);

    public List<EventiPagati> getDettagliMovimento(Long movimentoId);

    public ProgrammaMusicale getDettaglioPM(Long idProgrammaMusicale, Long idMovimento);

    public ProgrammaMusicale getDettaglioPM(Long idProgrammaMusicale);

    public Integer updateContabilitaOriginale(Integer nuovaContabilita, List<Object> listaMovimenti);
}
