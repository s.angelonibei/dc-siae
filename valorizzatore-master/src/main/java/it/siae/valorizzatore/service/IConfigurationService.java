package it.siae.valorizzatore.service;

import it.siae.valorizzatore.model.Circoscrizione;
import it.siae.valorizzatore.model.Configurazione;

import java.util.List;

/**
 * Created by idilello on 7/27/16.
 */
public interface IConfigurationService {

    public String getParameter(String parameterName);

    public void insertParameter(String parameterName, String parameterValue);

    public Configurazione getParameterObject(String parameterName);

    public void insertParameterObject(Configurazione configurazione);

    public List<Circoscrizione> getCircoscrizioni();

    public void updateConfigurazione(String annoFinePeriodoRipartizione, String meseFinePeriodoRipartizione,
                                     String annoInizioPeriodoContabile, String meseInizioPeriodoContabile,
                                     String annoFinePeriodoContabile, String meseFinePeriodoContabile,
                                     String tipologiaPM, String campionaDigitali,
                                     String annoFinePeriodoRipartizione2243, String meseFinePeriodoRipartizione2243,
                                     String annoInizioPeriodoContabile2243, String meseInizioPeriodoContabile2243,
                                     String annoFinePeriodoContabile2243, String meseFinePeriodoContabile2243,
                                     String tipologiaPM2243);
}
