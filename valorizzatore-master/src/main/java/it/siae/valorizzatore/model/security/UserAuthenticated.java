package it.siae.valorizzatore.model.security;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;


public class UserAuthenticated extends AbstractAuthenticationToken {

    private static final long serialVersionUID = 1L;

    private UserDetails principal;
    private String credentials;


    public UserAuthenticated(UserDetails principal, String password) {

        super(principal.getAuthorities());
        this.principal=principal;
        this.credentials=password;
    }

    @Override
    public Object getCredentials() {

        return credentials;
    }

    @Override
    public Object getPrincipal() {

        return principal;
    }



}