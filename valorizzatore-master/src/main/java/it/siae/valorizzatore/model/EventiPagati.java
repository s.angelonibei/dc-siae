package it.siae.valorizzatore.model;

import it.siae.valorizzatore.utility.Utility;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by idilello on 6/7/16.
 */
@Entity
@Table(name = "EVENTI_PAGATI")
public class EventiPagati implements Serializable {

    private Long id;
    private String seprag;
    private Integer contabilita;
    private Integer contabilitaOrg;
    private String reversale;
    private String numeroFattura;
    private String voceIncasso;
    private String tipoDocumentoContabile;
    private String denominazioneLocale;
    private String codiceBaLocale;
    private String codiceSpeiLocale;
    private Date dataInizioEvento;
    private String oraInizioEvento;
    private Long numeroPmTotaliAttesi;
    private Long numeroPmTotaliAttesiSpalla;
    private Double importDem=0D;;
    private Date dataIns;
    private Long idEvento;

    private Manifestazione manifestazione;
    private List<MovimentoContabile> movimentazioniPM = new ArrayList<MovimentoContabile>();
    private List<DettaglioPM> dettaglioPM;

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="EVENTI_PAGATI_SEQ")
    @Column(name = "ID_EVENTO_PAGATO", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Column(name = "SEPRAG")
    public String getSeprag() {
        return seprag;
    }

    public void setSeprag(String seprag) {
        this.seprag = seprag;
    }

    @Column(name = "CONTABILITA")
    public Integer getContabilita() {
        return contabilita;
    }

    public void setContabilita(Integer contabilita) {
        this.contabilita = contabilita;
    }

    @Column(name = "CONTABILITA_ORIG")
    public Integer getContabilitaOrg() {
        return contabilitaOrg;
    }

    public void setContabilitaOrg(Integer contabilitaOrg) {
        this.contabilitaOrg = contabilitaOrg;
    }


    @Column(name = "NUMERO_REVERSALE")
    public String getReversale() {
        return reversale;
    }

    public void setReversale(String reversale) {
        this.reversale = reversale;
    }


    @Column(name = "NUMERO_FATTURA")
    public String getNumeroFattura() {
        return numeroFattura;
    }

    public void setNumeroFattura(String numeroFattura) {
        this.numeroFattura = numeroFattura;
    }

    @Column(name = "VOCE_INCASSO")
    public String getVoceIncasso() {
        return voceIncasso;
    }

    public void setVoceIncasso(String voceIncasso) {
        this.voceIncasso = voceIncasso;
    }

    @Column(name = "TIPO_DOCUMENTO_CONTABILE")
    public String getTipoDocumentoContabile() {
        return tipoDocumentoContabile;
    }

    public void setTipoDocumentoContabile(String tipoDocumentoContabile) {
        this.tipoDocumentoContabile = tipoDocumentoContabile;
    }


    @Column(name = "DENOMINAZIONE_LOCALE")
    public String getDenominazioneLocale() {
        return denominazioneLocale;
    }

    public void setDenominazioneLocale(String denominazioneLocale) {
        this.denominazioneLocale = denominazioneLocale;
    }

    @Column(name = "CODICE_BA_LOCALE")
    public String getCodiceBaLocale() {
        return codiceBaLocale;
    }

    public void setCodiceBaLocale(String codiceBaLocale) {
        this.codiceBaLocale = codiceBaLocale;
    }

    @Column(name = "CODICE_SPEI_LOCALE")
    public String getCodiceSpeiLocale() {
        return codiceSpeiLocale;
    }

    public void setCodiceSpeiLocale(String codiceSpeiLocale) {
        this.codiceSpeiLocale = codiceSpeiLocale;
    }


    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_INIZIO_EVENTO")
    public Date getDataInizioEvento() {
        return dataInizioEvento;
    }

    public void setDataInizioEvento(Date dataInizioEvento) {
        this.dataInizioEvento = dataInizioEvento;
    }

    @Column(name = "ORA_INIZIO_EVENTO")
    public String getOraInizioEvento() {
        return oraInizioEvento;
    }

    public void setOraInizioEvento(String oraInizioEvento) {
        this.oraInizioEvento = oraInizioEvento;
    }

    @Column(name = "NUMERO_PM_ATTESI")
    public Long getNumeroPmTotaliAttesi() {
        if (numeroPmTotaliAttesi==null)
            numeroPmTotaliAttesi=1L;
        return numeroPmTotaliAttesi;
    }

    public void setNumeroPmTotaliAttesi(Long numeroPmTotaliAttesi) {
        this.numeroPmTotaliAttesi = numeroPmTotaliAttesi;
    }

    @Column(name = "IMPORTO_DEM")
    public Double getImportDem() {
        if (importDem != null)
           return Utility.round(importDem,7);
        else
           return importDem;
    }

    public void setImportDem(Double importDem) {
        this.importDem = importDem;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_INS")
    public Date getDataIns() {
        return dataIns;
    }

    public void setDataIns(Date dataIns) {
        this.dataIns = dataIns;
    }

    @Column(name = "ID_EVENTO")
    public Long getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(Long idEvento) {
        this.idEvento = idEvento;
    }


    @Column(name = "NUMERO_PM_ATTESI_SPALLA")
    public Long getNumeroPmTotaliAttesiSpalla() {
        if (numeroPmTotaliAttesiSpalla==null)
            numeroPmTotaliAttesiSpalla=1L;
        return numeroPmTotaliAttesiSpalla;
    }

    public void setNumeroPmTotaliAttesiSpalla(Long numeroPmTotaliAttesiSpalla) {
        this.numeroPmTotaliAttesiSpalla = numeroPmTotaliAttesiSpalla;
    }

    @Transient
    public String getDataInizioEventoStr(){
        return Utility.dateString(dataInizioEvento, Utility.OracleFormat_D);
    }

    @Transient
    public Manifestazione getManifestazione() {
        return manifestazione;
    }

    public void setManifestazione(Manifestazione manifestazione) {
        this.manifestazione = manifestazione;
    }

    @Transient
    public List<MovimentoContabile> getMovimentazioniPM() {
        return movimentazioniPM;
    }

    public void setMovimentazioniPM(List<MovimentoContabile> movimentazioniPM) {
        this.movimentazioniPM = movimentazioniPM;
    }

    @Transient
    public List<DettaglioPM> getDettaglioPM() {
        return dettaglioPM;
    }

    public void setDettaglioPM(List<DettaglioPM> dettaglioPM) {
        this.dettaglioPM = dettaglioPM;
    }

    @Transient
    public String getImportoDemFormatted() {
        return Utility.displayFormatted(importDem);
    }

    @Override
    public String toString() {
        return "EventiPagati{" +
                "id=" + id +
                ", seprag='" + seprag + '\'' +
                ", contabilita=" + contabilita +
                ", contabilitaOrg=" + contabilitaOrg +
                ", reversale='" + reversale + '\'' +
                ", numeroFattura='" + numeroFattura + '\'' +
                ", voceIncasso='" + voceIncasso + '\'' +
                ", tipoDocumentoContabile='" + tipoDocumentoContabile + '\'' +
                ", denominazioneLocale='" + denominazioneLocale + '\'' +
                ", codiceBaLocale='" + codiceBaLocale + '\'' +
                ", codiceSpeiLocale='" + codiceSpeiLocale + '\'' +
                ", dataInizioEvento=" + dataInizioEvento +
                ", oraInizioEvento='" + oraInizioEvento + '\'' +
                ", numeroPmTotaliAttesi=" + numeroPmTotaliAttesi +
                ", numeroPmTotaliAttesiSpalla=" + numeroPmTotaliAttesiSpalla +
                ", importDem=" + importDem +
                ", dataIns=" + dataIns +
                ", idEvento=" + idEvento +
                ", manifestazione=" + manifestazione +
                ", movimentazioniPM=" + movimentazioniPM +
                ", dettaglioPM=" + dettaglioPM.toString() +
                '}';
    }
}
