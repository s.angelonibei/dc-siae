package it.siae.valorizzatore.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "MOVIMENTAZIONE_LOGISTICA_PM")
public class MovimentazioneLogisticaPM implements Serializable{

    private Long id;
    private Long idProgrammaMusicale;
    private String stato;
    private Character flagEsclusoInvioNeed;
    private Character flagEsclusoRicalcolo;
    private Character flagAttivo;


    @Id
    @SequenceGenerator(name="MOV_LOGISTICA_PM",sequenceName = "MOV_LOGISTICA_PM_SEQ",allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="MOV_LOGISTICA_PM")
    @Column(name = "ID_MOVIMENTAZIONE_LOGISTICA", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ID_PROGRAMMA_MUSICALE")
    public Long getIdProgrammaMusicale() {

        return idProgrammaMusicale;
    }

    public void setIdProgrammaMusicale(Long idProgrammaMusicale) {

        this.idProgrammaMusicale = idProgrammaMusicale;
    }

    @Column(name = "STATO")
    public String getStato() {
        return stato;
    }

    public void setStato(String stato) {
        this.stato = stato;
    }

    @Column(name = "FLAG_ESCLUSO_INVIO_NEED")
    public Character getFlagEsclusoInvioNeed() {
        return flagEsclusoInvioNeed;
    }

    public void setFlagEsclusoInvioNeed(Character flagEsclusoInvioNeed) {
        this.flagEsclusoInvioNeed = flagEsclusoInvioNeed;
    }

    @Column(name = "FLAG_ESCLUSO_RICALCOLO")
    public Character getFlagEsclusoRicalcolo() {
        return flagEsclusoRicalcolo;
    }

    public void setFlagEsclusoRicalcolo(Character flagEsclusoRicalcolo) {
        this.flagEsclusoRicalcolo = flagEsclusoRicalcolo;
    }

    @Column(name = "FLAG_ATTIVO")
    public Character getFlagAttivo() {
        return flagAttivo;
    }

    public void setFlagAttivo(Character flagAttivo) {
        this.flagAttivo = flagAttivo;
    }
}
