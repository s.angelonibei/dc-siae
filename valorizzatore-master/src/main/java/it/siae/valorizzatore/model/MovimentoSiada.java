package it.siae.valorizzatore.model;

import it.siae.valorizzatore.utility.Utility;

import javax.persistence.*;
import java.util.Comparator;
import java.util.Date;

@Entity
@Table(name = "MOVIMENTI_SIADA")
public class MovimentoSiada implements Comparable{

    private Long id;
    private Long idEvento;
    private Long idProgrammaMusicale;
    private Date dataInizioEvento;
    private Date dataFineEvento;
    private String voceIncasso;
    private String voceIncassoSiada;
    private Long numeroFattura;
    private Double importo;
    private Double importoSingolaCedola;
    private Integer meseContabile;
    private Integer meseContabileOrig;
    private Integer meseInoltroSiada;
    private Date dataIns;
    private Long numeroProgrammaMusicale;
    private String dataInizioEventoStr;
    private String dataFineEventoStr;
    private Date dataElaborazione;


    @Id
    @SequenceGenerator(name="MOVIMENTI_SIADA",sequenceName = "MOVIMENTI_SIADA_SEQ",allocationSize=1)
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="MOVIMENTI_SIADA")
    @Column(name = "ID", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Column(name = "ID_EVENTO")
    public Long getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(Long idEvento) {
        this.idEvento = idEvento;
    }

    @Column(name = "ID_PROGRAMMA_MUSICALE")
    public Long getIdProgrammaMusicale() {
        return idProgrammaMusicale;
    }

    public void setIdProgrammaMusicale(Long idProgrammaMusicale) {
        this.idProgrammaMusicale = idProgrammaMusicale;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_INIZIO_EVENTO")
    public Date getDataInizioEvento() {
        return dataInizioEvento;
    }

    public void setDataInizioEvento(Date dataInizioEvento) {
        this.dataInizioEvento = dataInizioEvento;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_FINE_EVENTO")
    public Date getDataFineEvento() {
        return dataFineEvento;
    }

    public void setDataFineEvento(Date dataFineEvento) {
        this.dataFineEvento = dataFineEvento;
    }


    @Column(name = "CODICE_VOCE_INCASSO")
    public String getVoceIncasso() {
        return voceIncasso;
    }

    public void setVoceIncasso(String voceIncasso) {
        this.voceIncasso = voceIncasso;
    }


    @Column(name = "CODICE_VOCE_INCASSO_SIADA")
    public String getVoceIncassoSiada() {
        return voceIncassoSiada;
    }

    public void setVoceIncassoSiada(String voceIncassoSiada) {
        this.voceIncassoSiada = voceIncassoSiada;
    }

    @Column(name = "NUMERO_FATTURA")
    public Long getNumeroFattura() {
        return numeroFattura;
    }

    public void setNumeroFattura(Long numeroFattura) {
        this.numeroFattura = numeroFattura;
    }

    @Column(name = "IMPORTO")
    public Double getImporto() {
        return importo;
    }

    public void setImporto(Double importo) {
        this.importo = importo;
    }


    @Column(name = "MESE_CONTABILE")
    public Integer getMeseContabile() {
        return meseContabile;
    }

    public void setMeseContabile(Integer meseContabile) {
        this.meseContabile = meseContabile;
    }

    @Column(name = "MESE_CONTABILE_ORIG")
    public Integer getMeseContabileOrig() {
        return meseContabileOrig;
    }

    public void setMeseContabileOrig(Integer meseContabileOrig) {
        this.meseContabileOrig = meseContabileOrig;
    }

    @Column(name = "MESE_INOLTRO_SIADA")
    public Integer getMeseInoltroSiada() {
        return meseInoltroSiada;
    }

    public void setMeseInoltroSiada(Integer meseInoltroSiada) {
        this.meseInoltroSiada = meseInoltroSiada;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_INS")
    public Date getDataIns() {
        return dataIns;
    }

    public void setDataIns(Date dataIns) {
        this.dataIns = dataIns;
    }


    @Column(name = "IMPORTO_SINGOLA_CEDOLA")
    public Double getImportoSingolaCedola() {
        return importoSingolaCedola;
    }

    public void setImportoSingolaCedola(Double importoSingolaCedola) {
        this.importoSingolaCedola = importoSingolaCedola;
    }


    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_ELABORAZIONE")
    public Date getDataElaborazione() {
        return dataElaborazione;
    }


    public void setDataElaborazione(Date dataElaborazione) {
        this.dataElaborazione = dataElaborazione;
    }

    @Transient
    public Long getNumeroProgrammaMusicale() {
        return numeroProgrammaMusicale;
    }

    public void setNumeroProgrammaMusicale(Long numeroProgrammaMusicale) {
        this.numeroProgrammaMusicale = numeroProgrammaMusicale;
    }

    @Transient
    public String getDataInizioEventoStr() {
        dataInizioEventoStr=Utility.dateString(dataInizioEvento, Utility.OracleFormat_D);
        return dataInizioEventoStr;
    }

    public void setDataInizioEventoStr(String dataInizioEventoStr) {
        this.dataInizioEventoStr = dataInizioEventoStr;
    }


    public int compareTo(Object o){
        MovimentoSiada m = (MovimentoSiada)o;
        if (this.getVoceIncasso().hashCode()>m.getVoceIncasso().hashCode())
            return 1;
        else
            return -1;
    }

    @Transient
    public String getDataFineEventoStr() {
        dataFineEventoStr=Utility.dateString(dataFineEvento, Utility.OracleFormat_D);
        return dataFineEventoStr;
    }

    public void setDataFineEventoStr(String dataFineEventoStr) {
        this.dataFineEventoStr = dataFineEventoStr;
    }


    @Transient
    public String getImportoFormatted() {
        return Utility.displayFormatted(importo);
    }

    @Transient
    public String getImportoSingolaCedolaFormatted() {
        return Utility.displayFormatted(importoSingolaCedola);
    }

    @Transient
    public String getDataElaborazioneStr(){
        return Utility.dateString(dataElaborazione,Utility.OracleFormat_D);
    }

}
