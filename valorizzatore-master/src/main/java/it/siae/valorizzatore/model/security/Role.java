package it.siae.valorizzatore.model.security;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "ROLE")
public class Role implements Serializable {

    public static final String ROLE_ADMIN="ROLE_ADMIN";

    private Long id;
    private String roleName;
    private String description;

    //@SequenceGenerator(
    //       name="ADM_ROLE_SEQ_GENERATOR",
    //        sequenceName="ADM_ROLE_SEQ",
    //        allocationSize = 1,
    //        initialValue = 1
    //)

    @Id
    //@GeneratedValue(strategy= GenerationType.SEQUENCE, generator="ADM_ROLE_SEQ_GENERATOR")
    @Column(name = "ID", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ROLE_NAME")
    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Column(name = "DESCRIPTION")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}