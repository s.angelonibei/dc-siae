package it.siae.valorizzatore.model;

import it.siae.valorizzatore.utility.Utility;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "MANIFESTAZIONE")
public class Manifestazione  implements Serializable {

    private Long id;
    private String codiceLocale;
    private String codiceComune;
    private String denominazioneLocale;
    private String denominazioneLocalita;
    private Date dataInizioEvento;
    private Date dataFineEvento;
    private String oraInizioEvento;
    private String oraFineEvento;
    private Long totaleMinuti;
    private String siglaProvinciaLocale;
    private String codiceSapOrganizzatore;
    private Long numeroFatture;

    @Id
    @Column(name = "ID_EVENTO", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "CODICE_LOCALE")
    public String getCodiceLocale() {
        return codiceLocale;
    }

    public void setCodiceLocale(String codiceLocale) {
        this.codiceLocale = codiceLocale;
    }

    @Column(name = "CODICE_COMUNE")
    public String getCodiceComune() {
        return codiceComune;
    }

    public void setCodiceComune(String codiceComune) {
        this.codiceComune = codiceComune;
    }


    @Column(name = "DENOMINAZIONE_LOCALE")
    public String getDenominazioneLocale() {
        return denominazioneLocale;
    }

    public void setDenominazioneLocale(String denominazioneLocale) {
        this.denominazioneLocale = denominazioneLocale;
    }

    @Column(name = "DENOMINAZIONE_LOCALITA")
    public String getDenominazioneLocalita() {
        return denominazioneLocalita;
    }

    public void setDenominazioneLocalita(String denominazioneLocalita) {
        this.denominazioneLocalita = denominazioneLocalita;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_INIZIO_EVENTO")
    public Date getDataInizioEvento() {
        return dataInizioEvento;
    }

    public void setDataInizioEvento(Date dataInizioEvento) {
        this.dataInizioEvento = dataInizioEvento;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_FINE_EVENTO")
    public Date getDataFineEvento() {
        return dataFineEvento;
    }

    public void setDataFineEvento(Date dataFineEvento) {
        this.dataFineEvento = dataFineEvento;
    }



    @Column(name = "ORA_INIZIO")
    public String getOraInizioEvento() {
        return oraInizioEvento;
    }

    public void setOraInizioEvento(String oraInizioEvento) {
        this.oraInizioEvento = oraInizioEvento;
    }

    @Column(name = "ORA_FINE")
    public String getOraFineEvento() {
        return oraFineEvento;
    }

    public void setOraFineEvento(String oraFineEvento) {
        this.oraFineEvento = oraFineEvento;
    }

    @Column(name = "TOTALE_MINUTI")
    public Long getTotaleMinuti() {
        return totaleMinuti;
    }

    public void setTotaleMinuti(Long totaleMinuti) {
        this.totaleMinuti = totaleMinuti;
    }


    @Column(name = "SIGLA_PROVINCIA_LOCALE")
    public String getSiglaProvinciaLocale() {
        return siglaProvinciaLocale;
    }

    public void setSiglaProvinciaLocale(String siglaProvinciaLocale) {
        this.siglaProvinciaLocale = siglaProvinciaLocale;
    }

    @Column(name = "CODICE_SAP_ORGANIZZATORE")
    public String getCodiceSapOrganizzatore() {
        return codiceSapOrganizzatore;
    }

    public void setCodiceSapOrganizzatore(String codiceSapOrganizzatore) {
        this.codiceSapOrganizzatore = codiceSapOrganizzatore;
    }

    @Column(name = "NUMERO_FATTURE")
    public Long getNumeroFatture() {
        return numeroFatture;
    }

    public void setNumeroFatture(Long numeroFatture) {
        this.numeroFatture = numeroFatture;
    }

    @Transient
    public String getDataInizioEventoStr() {
      return Utility.dateString(dataInizioEvento, Utility.OracleFormat_D);
    }

    @Transient
    public String getDataFineEventoStr() {
        return Utility.dateString(dataFineEvento, Utility.OracleFormat_D);
    }
}
