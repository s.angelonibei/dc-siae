package it.siae.valorizzatore.model.view;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by idilello on 11/25/16.
 */
public class BreadCrumbList {

    private List<BreadCrumb> breadCrumbs = new ArrayList<BreadCrumb>();

    public BreadCrumbList(){
        this.breadCrumbs = new ArrayList<BreadCrumb>();
    }

    public void addBreadCrumb(String label, String url){
        BreadCrumb breadCrumb = new BreadCrumb(label, url);
        boolean find = false;
        for (int i=0; i<breadCrumbs.size(); i++){
            if (breadCrumbs.get(i).getLabel()!=null && breadCrumbs.get(i).getLabel().equalsIgnoreCase(label)){
                find = true;
                break;
            }

        }
        if (!find) {
            breadCrumbs.add(breadCrumb);
        }
    }

    public List<BreadCrumb> getBreadCrumbs(){
        return  breadCrumbs;
    }

}
