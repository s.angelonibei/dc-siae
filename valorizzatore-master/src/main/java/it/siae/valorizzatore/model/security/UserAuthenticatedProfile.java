package it.siae.valorizzatore.model.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.io.Serializable;
import java.util.*;

public class UserAuthenticatedProfile extends User implements Serializable{

    public UserAuthenticatedProfile(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    void initAcl(){
		/*
		userAcl=new HashSet<String>();
		channelAcl=new HashSet<String>();
		channelAcl.add("84:2");
		channelAcl.add("84:0");
		*/

    }

    public boolean  isAllowed(Map regole){
        return true;

    }

    public boolean isProfiledFor(String controllo_certificato) {
        return true;
    }
}