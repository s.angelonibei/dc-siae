package it.siae.valorizzatore.model;

import it.siae.valorizzatore.utility.Utility;

import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by idilello on 7/15/16.
 */
public class DettaglioPM implements Serializable{

    private Long idEvento;
    private String voceIncasso;
    private Long pmPrevisti=1L;
    private Long pmPrevistiSpalla=1L;
    private Long pmRientrati=0L;
    private Long pmRientratiSpalla=0L;
    private List<ImportoRicalcolato> importiRicalcolati;
    private Double importoTotale=0D;;

    private Double importoAssegnato=0D;
    private Double importoDisponibile=0D;

    public Long getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(Long idEvento) {
        this.idEvento = idEvento;
    }

    public String getVoceIncasso() {
        return voceIncasso;
    }

    public void setVoceIncasso(String voceIncasso) {
        this.voceIncasso = voceIncasso;
    }

    public Long getPmPrevisti() {
        return pmPrevisti;
    }

    public void setPmPrevisti(Long pmPrevisti) {
        this.pmPrevisti = pmPrevisti;
    }

    public Long getPmPrevistiSpalla() {
        return pmPrevistiSpalla;
    }

    public void setPmPrevistiSpalla(Long pmPrevistiSpalla) {
        this.pmPrevistiSpalla = pmPrevistiSpalla;
    }

    public Long getPmRientrati() {
        return pmRientrati;
    }

    public void setPmRientrati(Long pmRientrati) {
        this.pmRientrati = pmRientrati;
    }

    public Long getPmRientratiSpalla() {
        return pmRientratiSpalla;
    }

    public void setPmRientratiSpalla(Long pmRientratiSpalla) {
        this.pmRientratiSpalla = pmRientratiSpalla;
    }

    public List<ImportoRicalcolato> getImportiRicalcolati() {
        return importiRicalcolati;
    }

    public void setImportiRicalcolati(List<ImportoRicalcolato> importiRicalcolati) {
        this.importiRicalcolati = importiRicalcolati;
    }

    public Double getImportoTotale() {
        if (importoTotale==null)
            importoTotale=0D;

        return importoTotale;
    }

    public void setImportoTotale(Double importoTotale) {
        this.importoTotale = importoTotale;
    }

    public Double getImportoAssegnato() {
        if (importiRicalcolati!=null && importiRicalcolati.size()>0)
          importoAssegnato=importiRicalcolati.get(0).getImporto();
        else
            importoAssegnato=0D;

        return importoAssegnato;
    }

    public void setImportoAssegnato(Double importoAssegnato) {
        this.importoAssegnato = importoAssegnato;
    }

    public Double getImportoDisponibile() {
        importoDisponibile = getImportoTotale() - getImportoAssegnato();
        return importoDisponibile;
    }

    public void setImportoDisponibile(Double importoDisponibile) {

        this.importoDisponibile = importoDisponibile;
    }

    public String getImportoTotaleFormatted() {
        return Utility.displayFormatted(importoTotale);
    }

    public String getImportoAssegnatoFormatted() {
        return Utility.displayFormatted(importoAssegnato);
    }

    public String getImportoDisponibileFormatted() {
        return Utility.displayFormatted(importoDisponibile);
    }

    @Override
    public String toString() {
        return "DettaglioPM{" +
                "idEvento=" + idEvento +
                ", voceIncasso='" + voceIncasso + '\'' +
                ", pmPrevisti=" + pmPrevisti +
                ", pmPrevistiSpalla=" + pmPrevistiSpalla +
                ", pmRientrati=" + pmRientrati +
                ", pmRientratiSpalla=" + pmRientratiSpalla +
                ", importiRicalcolati=" + importiRicalcolati +
                ", importoTotale=" + importoTotale +
                ", importoAssegnato=" + importoAssegnato +
                ", importoDisponibile=" + importoDisponibile +
                '}';
    }
}
