package it.siae.valorizzatore.model;

import it.siae.valorizzatore.utility.Utility;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "UTILIZZAZIONE")
public class Utilizzazione implements Serializable {

    private Long id;
    private Long idProgrammaMusicale;
    private Long numeroProgrammaMusicale;
    private String compositore;
    private String titoloComposizione;
    private String durataInferiore60sec;
    private Double durata;
    private String codiceOpera;
    private Double importoCedola;


    @Id
    @SequenceGenerator(name="UTILIZZAZIONE_GEN",sequenceName = "UTILIZZAZIONE_SEQ",allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="UTILIZZAZIONE_GEN")
    @Column(name = "ID_UTILIZZAZIONE", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Column(name = "ID_PROGRAMMA_MUSICALE")
    public Long getIdProgrammaMusicale() {
        return idProgrammaMusicale;
    }

    public void setIdProgrammaMusicale(Long idProgrammaMusicale) {
        this.idProgrammaMusicale = idProgrammaMusicale;
    }

    @Column(name = "NUMERO_PROGRAMMA_MUSICALE")
    public Long getNumeroProgrammaMusicale() {
        return numeroProgrammaMusicale;
    }

    public void setNumeroProgrammaMusicale(Long numeroProgrammaMusicale) {
        this.numeroProgrammaMusicale = numeroProgrammaMusicale;
    }

    @Column(name = "COMPOSITORE")
    public String getCompositore() {
        return compositore;
    }

    public void setCompositore(String compositore) {
        this.compositore = compositore;
    }

    @Column(name = "TITOLO_COMPOSIZIONE")
    public String getTitoloComposizione() {
        return titoloComposizione;
    }

    public void setTitoloComposizione(String titoloComposizione) {
        this.titoloComposizione = titoloComposizione;
    }

    @Column(name = "DURATA_INFERIORE_60SEC")
    public String getDurataInferiore60sec() {
        return durataInferiore60sec;
    }

    public void setDurataInferiore60sec(String durataInferiore60sec) {
        this.durataInferiore60sec = durataInferiore60sec;
    }

    @Column(name = "DURATA")
    public Double getDurata() {
        return durata;
    }

    public void setDurata(Double durata) {
        this.durata = durata;
    }

    @Column(name = "CODICE_OPERA")
    public String getCodiceOpera() {
        return codiceOpera;
    }

    public void setCodiceOpera(String codiceOpera) {
        this.codiceOpera = codiceOpera;
    }

    @Transient
    public Double getImportoCedola() {
        return importoCedola;
    }

    public void setImportoCedola(Double importoCedola) {
        this.importoCedola = importoCedola;
    }

    @Transient
    public String getImportoCedolaFormatted() {
        return Utility.displayFormatted(importoCedola);
    }

}
