package it.siae.valorizzatore.model;

import it.siae.valorizzatore.utility.Utility;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by idilello on 6/8/16.
 */
@Entity
@Table(name = "IMPORTO_RICALCOLATO")
public class ImportoRicalcolato implements Serializable {

    private Long id;
    private Long idMovimentoContabile;
    private Long idEvento;
    private Long numeroFattura;
    private String voceIncasso;
    private Double importo=0D;;
    private Double importoSingolaCedola=0D;;
    private Long idEsecuzioneRicalcolo;
    private Date dataOraInserimento;

    private Long idProgrammaMusicale;

    @Id
    @SequenceGenerator(name="IMPORTO_RIC",sequenceName = "IMPORTO_RIC_SEQ",allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IMPORTO_RIC")
    @Column(name = "ID_IMPORTO_RICALCOLATO", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Column(name = "ID_MOVIMENTO_CONTABILE")
    public Long getIdMovimentoContabile() {
        return idMovimentoContabile;
    }

    public void setIdMovimentoContabile(Long idMovimentoContabile) {
        this.idMovimentoContabile = idMovimentoContabile;
    }

    @Column(name = "ID_EVENTO")
    public Long getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(Long idEvento) {
        this.idEvento = idEvento;
    }



    @Column(name = "NUMERO_FATTURA")
    public Long getNumeroFattura() {
        return numeroFattura;
    }

    public void setNumeroFattura(Long numeroFattura) {
        this.numeroFattura = numeroFattura;
    }


    @Column(name = "VOCE_INCASSO")
    public String getVoceIncasso() {
        return voceIncasso;
    }

    public void setVoceIncasso(String voceIncasso) {
        this.voceIncasso = voceIncasso;
    }

    @Column(name = "IMPORTO")
    public Double getImporto() {
        return importo;
    }

    public void setImporto(Double importo) {
        this.importo = importo;
    }

    @Column(name = "IMPORTO_SINGOLA_CEDOLA")
    public Double getImportoSingolaCedola() {
        return importoSingolaCedola;
    }

    public void setImportoSingolaCedola(Double importoSingolaCedola) {
        this.importoSingolaCedola = importoSingolaCedola;
    }

    @Column(name = "ID_ESECUZIONE_RICALCOLO")
    public Long getIdEsecuzioneRicalcolo() {
        return idEsecuzioneRicalcolo;
    }

    public void setIdEsecuzioneRicalcolo(Long idEsecuzioneRicalcolo) {
        this.idEsecuzioneRicalcolo = idEsecuzioneRicalcolo;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_ORA_INSERIMENTO")
    public Date getDataOraInserimento() {
        return dataOraInserimento;
    }

    public void setDataOraInserimento(Date dataOraInserimento) {
        this.dataOraInserimento = dataOraInserimento;
    }

    @Transient
    public String getDataOraInserimentoStr() {
        return Utility.dateString(dataOraInserimento, Utility.OracleFormat_D);
    }

    @Transient
    public Long getIdProgrammaMusicale() {
        return idProgrammaMusicale;
    }

    public void setIdProgrammaMusicale(Long idProgrammaMusicale) {
        this.idProgrammaMusicale = idProgrammaMusicale;
    }

    @Transient
    public String getImportoFormatted() {
        return Utility.displayFormatted(importo);
    }

    @Transient
    public String getImportoSingolaCedolaFormatted() {
        return Utility.displayFormatted(importoSingolaCedola);
    }

}
