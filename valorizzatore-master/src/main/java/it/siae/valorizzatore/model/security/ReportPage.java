package it.siae.valorizzatore.model.security;

import it.siae.valorizzatore.model.*;
import it.siae.valorizzatore.utility.Constants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by idilello on 6/21/16.
 */
public class ReportPage implements Serializable{

    public static final int NUMBER_RECORD_PER_PAGE = 400;

    private int currentPage;
    private int totRecords;
    private List<Long> pages;
    private List allRecords;
    private List records;
    private String fileName;

    public ReportPage(List recordList, int totRecords, int currentPage){

        if (recordList!=null && recordList.size()>0) {

            Object type = recordList.get(0);

             if (type instanceof MovimentoContabile) {
                 this.records = new ArrayList<MovimentoContabile>();
                 this.allRecords = new ArrayList<MovimentoContabile>();
             }else
             if (type instanceof CampionamentoConfig) {
                 this.records = new ArrayList<CampionamentoConfig>();
                 this.allRecords = new ArrayList<CampionamentoConfig>();
             }else
             if (type instanceof ProgrammaMusicale) {
                 this.records = new ArrayList<ProgrammaMusicale>();
                 this.allRecords = new ArrayList<ProgrammaMusicale>();
             } else
             if (type instanceof EventiPagati) {
                 this.records = new ArrayList<EventiPagati>();
                 this.allRecords = new ArrayList<EventiPagati>();
             } else
             if (type instanceof MovimentoSiada) {
                 this.records = new ArrayList<MovimentoSiada>();
                 this.allRecords = new ArrayList<MovimentoSiada>();
             } else
             if (type instanceof AggregatoVoce) {
                 this.records = new ArrayList<AggregatoVoce>();
                 this.allRecords = new ArrayList<AggregatoVoce>();
             } else
             if (type instanceof TracciamentoApplicativo) {
                 this.records = new ArrayList<TracciamentoApplicativo>();
                 this.allRecords = new ArrayList<TracciamentoApplicativo>();
             }

             this.records.addAll(recordList);
             this.totRecords = totRecords;
             this.currentPage = currentPage;

             pages = new ArrayList<Long>();

             int nPages = 0;
             if (currentPage != -1)
                 nPages = (this.totRecords / NUMBER_RECORD_PER_PAGE) +1;
             else
                 nPages = 1;

             for (int i=0; i<nPages; i++){
                 pages.add(new Long(i+1));
             }


       }else{
         this.records = new ArrayList();
         this.totRecords = 0;
       }

    }

    public ReportPage(List allRecordsList, List recordList, int totRecords, int currentPage){

        if (recordList!=null && recordList.size()>0) {

            Object type = recordList.get(0);

            if (type instanceof MovimentoContabile) {
                this.records = new ArrayList<MovimentoContabile>();
                this.allRecords = new ArrayList<MovimentoContabile>();
            }else
            if (type instanceof CampionamentoConfig) {
                this.records = new ArrayList<CampionamentoConfig>();
                this.allRecords = new ArrayList<CampionamentoConfig>();
            }else
            if (type instanceof ProgrammaMusicale) {
                this.records = new ArrayList<ProgrammaMusicale>();
                this.allRecords = new ArrayList<ProgrammaMusicale>();
            } else
            if (type instanceof EventiPagati) {
                this.records = new ArrayList<EventiPagati>();
                this.allRecords = new ArrayList<EventiPagati>();
            } else
            if (type instanceof MovimentoSiada) {
                this.records = new ArrayList<MovimentoSiada>();
                this.allRecords = new ArrayList<MovimentoSiada>();
            } else
            if (type instanceof AggregatoVoce) {
                this.records = new ArrayList<AggregatoVoce>();
                this.allRecords = new ArrayList<AggregatoVoce>();
            } else
            if (type instanceof TracciamentoApplicativo) {
                this.records = new ArrayList<TracciamentoApplicativo>();
                this.allRecords = new ArrayList<TracciamentoApplicativo>();
            } else
            if (type instanceof MovimentoSiada2243) {
                this.records = new ArrayList<MovimentoSiada2243>();
                this.allRecords = new ArrayList<MovimentoSiada2243>();
            }

            this.records.addAll(recordList);
            this.allRecords.addAll(allRecordsList);
            this.totRecords = totRecords;
            this.currentPage = currentPage;

            pages = new ArrayList<Long>();

            int nPages = 0;
            if (currentPage != -1)
                nPages = (this.totRecords / NUMBER_RECORD_PER_PAGE) +1;
            else
                nPages = 1;

            for (int i=0; i<nPages; i++){
                pages.add(new Long(i+1));
            }


        }else{
            this.records = new ArrayList();
            this.totRecords = 0;
        }

    }


    public int getTotRecords() {
        return totRecords;
    }

    public void setTotRecords(int totRecords) {
        this.totRecords = totRecords;
    }

    public List<Object> getRecords() {
        return records;
    }

    public List<Object> getAllRecords() {
        return allRecords;
    }


    public void setRecords(List records, int currentPage) {

        if (records!=null && records.size()>0) {

            Object type = records.get(0);

            if (type instanceof MovimentoContabile) {
                this.records = new ArrayList<MovimentoContabile>();
            }else
            if (type instanceof CampionamentoConfig) {
                this.records = new ArrayList<CampionamentoConfig>();
            }else
            if (type instanceof ProgrammaMusicale) {
                this.records = new ArrayList<ProgrammaMusicale>();
            } else
            if (type instanceof EventiPagati) {
                this.records = new ArrayList<EventiPagati>();
            } else
            if (type instanceof MovimentoSiada) {
                this.records = new ArrayList<MovimentoSiada>();
            } else
            if (type instanceof AggregatoVoce) {
                this.records = new ArrayList<AggregatoVoce>();
            } else
            if (type instanceof TracciamentoApplicativo) {
                this.records = new ArrayList<TracciamentoApplicativo>();
            }

            this.records.addAll(records);
            this.currentPage = currentPage;


        }else{
            this.records = new ArrayList();
            this.totRecords = 0;
        }
    }

    public void initRecordsInPage(int page) {

        int startRecord = 0;
        int endRecord = ReportPage.NUMBER_RECORD_PER_PAGE-1;
        int recordCount = allRecords.size();

        if (page != -1) {

            startRecord = (page - 1) * ReportPage.NUMBER_RECORD_PER_PAGE;
            endRecord = 0;
            if (recordCount > ReportPage.NUMBER_RECORD_PER_PAGE) {
                endRecord = startRecord + ReportPage.NUMBER_RECORD_PER_PAGE;
                if (endRecord > recordCount)
                    endRecord = recordCount - 1;
            } else {
                endRecord = recordCount;
            }

        }else{
            if (allRecords!=null && recordCount>0 && recordCount<=ReportPage.NUMBER_RECORD_PER_PAGE)
               endRecord = allRecords.size();
        }

        this.records.clear();

        for (int i = startRecord; i < endRecord; i++) {
            this.records.add(allRecords.get(i));
        }
    }



    public List<Long> getPages() {
        return pages;
    }

    public void setPages(List<Long> pages) {
        this.pages = pages;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
