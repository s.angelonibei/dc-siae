package it.siae.valorizzatore.model.security;

import it.siae.valorizzatore.model.security.Role;
import it.siae.valorizzatore.utility.Utility;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.servlet.http.HttpServletRequest;

@Entity
@Table(name = "USERS")
public class User implements Serializable {

    private Long id;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private Character flagEnabled;   
    private Date dataIn;
    private List<Role> roles;
    private List<Profile> profiles;
    private String sesisonToken;

    //@SequenceGenerator(
    //        name="ADM_USR_ADMIN_SEQ_GENERATOR",
    //        sequenceName="ADM_USR_ADMIN_SEQ",
    //        allocationSize = 1,
    //        initialValue = 1
    //)
    @Id
    //@GeneratedValue(strategy= GenerationType.SEQUENCE, generator="ADM_USR_ADMIN_SEQ_GENERATOR")
    @Column(name = "ID", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "USERNAME",nullable = false)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "PASSWORD",nullable = false)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "FIRST_NAME")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "LAST_NAME")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Column(name = "ACTIVE")
    public Character getFlagEnabled() {
        return flagEnabled;
    }

    public void setFlagEnabled(Character flagEnabled) {
        this.flagEnabled = flagEnabled;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_INS")
    public Date getDataIn() {
        return dataIn;
    }

    public void setDataIn(Date dataIn) {
        this.dataIn = dataIn;
    }

    @OneToMany
    @JoinTable(
            name="USER_ROLE",
           joinColumns={ @JoinColumn(name="ID_USER", referencedColumnName="ID") },
           inverseJoinColumns={ @JoinColumn(name="ID_ROLE", referencedColumnName="ID", unique=true) }
    )
     public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @Transient
    public boolean isAdmin(){
        for (Role role : roles){
            if (role.getRoleName().equalsIgnoreCase(Role.ROLE_ADMIN))
             return true;
             }
        return false;
    }

    @Transient
    public String getDataCreazione(){
       return Utility.dateString(dataIn, Utility.OracleFormat_D);
    }


    @Transient
    public List<Profile> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<Profile> profiles) {
        this.profiles = profiles;
    }

    @Transient
    public boolean hasProfile(String profile){

        for (Profile userProfile: profiles)
            if (userProfile.getProfileName().equalsIgnoreCase(profile))
                return true;

        return false;
    }

    @Transient
    public String getSesisonToken() {
        return sesisonToken;
    }

    @Transient
    public void setSesisonToken(String sesisonToken) {
        this.sesisonToken = sesisonToken;
    }
}