package it.siae.valorizzatore.utility;

import groovy.json.JsonBuilder;

import java.io.*;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import static javax.script.ScriptEngine.FILENAME;

public class Utility {

    public static final String OracleFormat_DT="dd/MM/yyyy HH:mm:ss";
    public static final String OracleFormat_D="dd/MM/yyyy";
    public static final String Format_YYYYMM="yyyyMM";
    public static final String Format_YYYYMMdd="yyyyMMdd";
    public static final String[] mesi={"Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"};

    public static String getDescrizioneContabilita(Long annoMese){

        String descrizione="";

        String annoMeseStr = annoMese.toString();

        String anno=annoMeseStr.substring(0,4);
        String mese=annoMeseStr.substring(4,6);

        descrizione = mesi[Integer.parseInt(mese)-1]+" "+anno;

        return descrizione;

    }

    public static Long normalizePeriodoContabile(String mesePeriodo, String annoPeriodo){

        Long periodoContabile;

        if (mesePeriodo.length()==1)
            return Long.parseLong(annoPeriodo+"0"+mesePeriodo);
        else
            return Long.parseLong(annoPeriodo+mesePeriodo);
    }

    public static String encodePassword(String password){

        String passwordEncoded=password;

        if (password!=null){

            byte[] defaultBytes = passwordEncoded.getBytes();

            try{
                MessageDigest algorithm = MessageDigest.getInstance("MD5");
                algorithm.reset();
                algorithm.update(defaultBytes);
                byte messageDigest[] = algorithm.digest();

                StringBuffer hexString = new StringBuffer();
                for (int i=0;i<messageDigest.length;i++) {
                    hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
                }

                passwordEncoded=hexString.toString();

            }catch(NoSuchAlgorithmException nsae){
                nsae.printStackTrace();
            }

        }

        return passwordEncoded;

    }

    public static final String jsonNOK(String msg) {
        HashMap<String, String> ret = new HashMap<String, String>();
        String msgRet = msg != null ? msg : "Errore Generico";
        ret.put("status", "KO");
        ret.put("reason", msgRet);
        JsonBuilder builder = new JsonBuilder(ret);
        return builder.toString();
    }

    public static String dateString(Date data, String outputFormat)
    {
        if (data != null) {

            SimpleDateFormat formatter = new SimpleDateFormat(outputFormat);

            return formatter.format(data);
        }
        return null;
    }

    public static Date stringDate(String data, String inputFormat)
    {
        Date ret = null;

        if ( (data != null) && (!data.equals("")) ){

            SimpleDateFormat formatter = new SimpleDateFormat(inputFormat);

            try{
                ret = formatter.parse(data);
            }
            catch(Exception e)
            {
                ret= null;
            }
        }

        return ret;
    }

    public static boolean isNumber(String s){
        try {
            double d = Double.parseDouble(s);
            return true;
        } catch(NumberFormatException nfe) {
            return false;
        }
    }

    public static double round(double value, int places){

        if (places < 0) throw new IllegalArgumentException();

        long factor = (long)Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double)tmp / factor;

    }

    public static String sendCommand(String command){

        String serverAddress = "127.0.0.1";
        int serverPort = 9090;

        String clientListener = "srvtst-l029v.net.siae:9090";


        String[] hostPort = clientListener.split(":");
        serverAddress=hostPort[0];
        serverPort=Integer.parseInt(hostPort[1]);

        String result = "INACTIVE";

        System.out.println("Host: "+serverAddress);
        System.out.println("Port: "+serverPort);
        try{

            Socket s = new Socket(serverAddress, serverPort);

            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            out.println(command);

            BufferedReader input = new BufferedReader(new InputStreamReader(s.getInputStream()));
            result = input.readLine();

            System.out.println("JCAClientService: "+result);

        }catch(java.net.ConnectException cr){
            cr.printStackTrace();
            result = "INACTIVE";
        }catch(java.io.IOException ioe){
            ioe.printStackTrace();
            result = "INACTIVE";
        }

        return result;
    }


    public static void eleboraFile(){

        BufferedReader br = null;
        FileReader fr = null;

        BufferedReader br2 = null;
        FileReader fr2 = null;
        PrintWriter writer = null;

        String FILENAME_ORDINI = "/home/idilello/Downloads/Ordini_30032017_new.csv";
        String FILENAME_LEGAMI = "/home/idilello/Downloads/Legami_30032017_new.csv";
        String FILENAME_OUT = "/home/idilello/Downloads/OrdiniBonifica_v2.csv";

        try {

            fr = new FileReader(FILENAME_ORDINI);
            br = new BufferedReader(fr);

            fr2 = new FileReader(FILENAME_LEGAMI);
            br2 = new BufferedReader(fr2);

            writer = new PrintWriter(FILENAME_OUT, "UTF-8");

            String sCurrentLine;
            String sCurrentLine2;

            br = new BufferedReader(new FileReader(FILENAME_ORDINI));


            String[] rigaOrdini;
            String[] rigaLegami;

            int i=0;
            while ((sCurrentLine = br.readLine()) != null) {
                rigaOrdini = sCurrentLine.split(";");
                //System.out.println("rigaOrdini: "+rigaOrdini[0]+" "+rigaOrdini[1]+" "+rigaOrdini[2]+" "+rigaOrdini[3]+" "+rigaOrdini[4]);

                br2 = new BufferedReader(new FileReader(FILENAME_LEGAMI));
                while ((sCurrentLine2 = br2.readLine()) != null) {
                    rigaLegami = sCurrentLine2.split(";");
                    //System.out.println("rigaLEgami: "+rigaLegami[0]+" "+rigaLegami[1]+" "+rigaLegami[2]);

                    if (rigaOrdini[1].equals(rigaLegami[1])) {
                        i++;
                        //System.out.println(rigaOrdini[1] + ";" + rigaOrdini[2] + ";" + rigaLegami[2]);
                        writer.println(rigaOrdini[5] + ";" + rigaOrdini[2].replace(".","/") + ";" + rigaLegami[2]);
                    }
                }
                if (br2 != null)
                    br2.close();

                if (fr2 != null)
                    fr2.close();

            }


        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            try {

                if (br != null)
                    br.close();

                if (fr != null)
                    fr.close();

                if (br2 != null)
                    br2.close();

                if (fr2 != null)
                    fr2.close();

                writer.close();

            } catch (IOException ex) {

                ex.printStackTrace();

            }

        }


    }

    public static String displayFormatted(Double currencyAmount) {

        if (currencyAmount!=null) {
            Locale currentLocale = new Locale("it", "IT");
            Locale.setDefault(currentLocale);
            Currency currentCurrency = Currency.getInstance(currentLocale);
            NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(currentLocale);

            String formattedNumber = currencyFormatter.format(currencyAmount);

            formattedNumber = formattedNumber.replace("€", "&euro;");

            return formattedNumber;

        }else
            return "";
    }

    public static void displayCurrency() {

        Double currencyAmount = new Double(9876543.2168465454956734);

        Locale currentLocale = new Locale("it", "IT");
        Locale.setDefault(currentLocale);
        Currency currentCurrency = Currency.getInstance(currentLocale);
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(currentLocale);

        String formattedNumber = currencyFormatter.format(currencyAmount);

        formattedNumber=formattedNumber.replace("€","&euro;");

        System.out.println(formattedNumber);
    }

    public static void main(String[] args){

        displayCurrency();

    }
    
}