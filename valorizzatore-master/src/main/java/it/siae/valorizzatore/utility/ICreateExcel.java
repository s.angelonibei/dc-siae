package it.siae.valorizzatore.utility;

import java.io.OutputStream;

/**
 * Created by cimo on 24/10/17.
 */
public interface ICreateExcel {

    public void exportToExcel(String idReport, OutputStream outputStream);
    public void exportToExcelDetail(String idReport, String detailType, Long detailObjectId, OutputStream outputStream);
}
