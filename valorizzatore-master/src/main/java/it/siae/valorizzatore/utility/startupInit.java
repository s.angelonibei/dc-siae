package it.siae.valorizzatore.utility;

import it.siae.valorizzatore.dao.IConfigurationDAO;
import it.siae.valorizzatore.model.Circoscrizione;
import it.siae.valorizzatore.model.Configurazione;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by idilello on 11/14/16.
 */
@Component
public class startupInit implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    @Qualifier("configurationDAO")
    IConfigurationDAO configurationDAO;

    @Override
    public void onApplicationEvent(final ContextRefreshedEvent event) {
        configurationDAO.initCache();
    }
}
