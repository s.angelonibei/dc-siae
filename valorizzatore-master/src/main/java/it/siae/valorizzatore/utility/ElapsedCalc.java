package it.siae.valorizzatore.utility;

import java.io.Serializable;


public class ElapsedCalc implements Serializable{
    protected long startTime;
    protected long endTime;
    protected long ellapsed;

    public ElapsedCalc() {
        startTime = System.currentTimeMillis();
    }

    public long stop() {
        endTime = System.currentTimeMillis();
        ellapsed = endTime - startTime;
        return ellapsed;
    }

    public long getSeconds() {
        return ellapsed / 1000l;
    }

    public float getMinutes() {
        return ellapsed / 60000.0f;
    }

    public long getMilliseconds() {
        return ellapsed;
    }

    public long getEndTime() {
        return endTime;
    }

}
