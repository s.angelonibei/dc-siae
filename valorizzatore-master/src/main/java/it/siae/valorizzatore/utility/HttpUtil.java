package it.siae.valorizzatore.utility;

import org.apache.commons.lang.StringUtils;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 * User: giampiero
 * Date: 18/09/13
 * Time: 18:23
 * To change this template use File | Settings | File Templates.
 */
public class HttpUtil {

	private static final String USER_AGENT = "Mozilla/5.0";

	static String ipString[]={
		"X-Forwarded-For",
		"x-forwarded-for",
		"Proxy-Client-IP",
		"WL-Proxy-Client-IP",
		"HTTP_CLIENT_IP",
		"HTTP_X_FORWARDED_FOR"
	};

	static String accessType[][]={
	  {"10.8.215","windConnect"},
      {"0:","intranet"},
	  {"10.","intranet"},
	  {"127.","intranet"},
	  {"172.21","dealerStation"}
	};



	public static String access(String ip){
		if(StringUtils.isEmpty(ip)) return "";
    	for(String[] access:accessType){
			if(ip.startsWith(access[0]))
				return access[1];
		}
		return "extranet";
	}

	public static String getClientIpAddr(HttpServletRequest request) {
		String ip=null;
		for(String s:ipString){
			String tmp = request.getHeader(s);
			if(StringUtils.isNotEmpty(tmp) && ! "unknown".equalsIgnoreCase(tmp)) {
				ip=tmp;
				break;
			}
		}

		if (StringUtils.isEmpty(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}


	public static String sendGet(String url) throws Exception {

		//String url = "http://www.google.com/search?q=mkyong";

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("User-Agent", USER_AGENT);

		int responseCode = con.getResponseCode();
		//System.out.println("\nSending 'GET' request to URL : " + url);
		//System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		//System.out.println();

		return response.toString();

	}

	public static String sendPost(String url, String urlParameters) throws Exception {

		//String url = "https://selfsolve.apple.com/wcResults.do";
		//String urlParameters = "sn=C02G8416DRJM&cn=&locale=&caller=&num=12345";

		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();

		BufferedReader in = new BufferedReader(
				new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		return response.toString();

	}
}
