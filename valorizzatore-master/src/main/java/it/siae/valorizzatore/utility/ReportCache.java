package it.siae.valorizzatore.utility;

import it.siae.valorizzatore.model.security.ReportPage;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * Created by cimo on 20/10/17.
 */
public class ReportCache {

    public static final String REPORT_LISTA_EVENTI = "Lista_Eventi";
    public static final String REPORT_LISTA_MOVIMENTI = "Lista_Movimenti";
    public static final String REPORT_AGGREGATO_INCASSO = "Aggregato_Incasso";
    public static final String REPORT_AGGREGATO_MOVIMENTI = "Aggregato_Movimenti";
    public static final String REPORT_IMPORTI_RICALCOLATI = "Lista_Importi_RICALCOLATI";
    public static final String REPORT_IMPORTI_SOSPESI = "Lista_Importi_SOSPESI";
    public static final String REPORT_IMPORTI_ERRORE = "Lista_Importi_ERRORE";
    public static final String REPORT_TRACCIAMENTO_PM = "Tracciamento_PM";
    public static final String REPORT_VERIFICA_PM_SIADA = "Verifica_PM_SIADA";
    public static final String REPORT_VERIFICA_PM_SIADA_SMC = "Verifica_PM_SIADA_SMC";

    public static final String REPORT_DETTAGLIO_MOVIMENTO = "Dettaglio_Movimenti";
    public static final String REPORT_DETTAGLIO_PM = "Dettaglio_PM";


    public static String generateKey(String reportId, List<String> parameters) {

        String reportKey = reportId;

        if (parameters != null && parameters.size() > 0) {

            for (String p : parameters) {

                reportKey = reportKey + "_" + p;
            }
        }

        return reportKey;

    }

    public static void putReport(String name, String key, ReportPage report) {

        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = attr.getRequest();
        HttpSession session = attr.getRequest().getSession();
        Map<String, List<ReportPage>> reports = (Map<String, List<ReportPage>>) session.getAttribute("REPORT_CACHE");

        if (reports == null) {

            reports = new HashMap<String, List<ReportPage>>();

        } else {
            // Elimina dalla cache eventuali report simili precedenti,in modo da ottimizzare lo spazio occupato
            Iterator keys = reports.keySet().iterator();
            String currentKey = null;
            while (keys.hasNext()) {
                currentKey = (String) keys.next();
                if (currentKey.startsWith(key)) {
                    reports.remove(currentKey);
                }
            }
        }

        List<ReportPage> reportsPage = new ArrayList<ReportPage>();
        reportsPage.add(report);
        reports.put(key, reportsPage);

        session.setAttribute("REPORT_CACHE", reports);

    }

    public static ReportPage getReport(String key) {

        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = attr.getRequest();
        HttpSession session = attr.getRequest().getSession();

        Map<String, List<ReportPage>> reports = (Map<String, List<ReportPage>>) session.getAttribute("REPORT_CACHE");

        List<ReportPage> reportsPage = null;

        if (reports != null) {
            reportsPage = reports.get(key);
            if (reportsPage != null && reportsPage.size() > 0)
                return reportsPage.get(0);
            else
                return null;
        } else {
            return null;
        }

    }

    public static List<ReportPage> getReportList(String key) {

        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = attr.getRequest();
        HttpSession session = attr.getRequest().getSession();

        Map<String, List<ReportPage>> reports = (Map<String, List<ReportPage>>) session.getAttribute("REPORT_CACHE");

        if (reports != null) {
            return reports.get(key);
        } else {
            return null;
        }

    }


    public static void putReportList(String name, String key, List<ReportPage> report) {

        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = attr.getRequest();
        HttpSession session = attr.getRequest().getSession();
        Map<String, List<ReportPage>> reports = (Map<String, List<ReportPage>>) session.getAttribute("REPORT_CACHE");

        if (reports == null) {

            reports = new HashMap<String, List<ReportPage>>();

        } else {
            // Elimina dalla cache eventuali report simili precedenti,in modo da ottimizzare lo spazio occupato
            Iterator keys = reports.keySet().iterator();
            String currentKey = null;
            while (keys.hasNext()) {
                currentKey = (String) keys.next();
                if (currentKey.startsWith(key)) {
                    reports.remove(currentKey);
                }
            }
        }

        reports.put(key, report);

        session.setAttribute("REPORT_CACHE", reports);

    }

    public static ReportPage getReportForExport(String key) {

        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        ReportPage reportPage = null;
        HttpServletRequest request = attr.getRequest();
        HttpSession session = attr.getRequest().getSession();

        Map<String, List<ReportPage>> reports = (Map<String, List<ReportPage>>) session.getAttribute("REPORT_CACHE");

        if (reports != null) {
            List<ReportPage> reportsPage = null;
            Iterator keys = reports.keySet().iterator();
            String currentKey = null;
            while (keys.hasNext()) {
                currentKey = (String) keys.next();
                if (currentKey.startsWith(key)) {
                    reportsPage = reports.get(currentKey);
                    reportPage = reportsPage.get(0);
                    //break;
                }
            }
        }

        return reportPage;
    }

    public static List<ReportPage> getReportListForExport(String key) {

        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        List<ReportPage> reportPage = null;
        HttpServletRequest request = attr.getRequest();
        HttpSession session = attr.getRequest().getSession();

        Map<String, List<ReportPage>> reports = (Map<String, List<ReportPage>>) session.getAttribute("REPORT_CACHE");

        List<ReportPage> reportsPage = null;
        if (reports != null) {

            Iterator keys = reports.keySet().iterator();
            String currentKey = null;
            while (keys.hasNext()) {
                currentKey = (String) keys.next();
                if (currentKey.startsWith(key)) {
                    reportsPage = reports.get(currentKey);
                    break;
                }
            }
        }

        return reportsPage;
    }

    public static void clearReport(String key) {

        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = attr.getRequest();
        HttpSession session = attr.getRequest().getSession();
        Map<String, ReportPage> reports = (Map<String, ReportPage>) session.getAttribute("REPORT_CACHE");

        if (reports != null)
            reports.remove(key);


    }

    public static void clearReports() {

        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = attr.getRequest();
        HttpSession session = attr.getRequest().getSession();
        Map<String, ReportPage> reports = (Map<String, ReportPage>) session.getAttribute("REPORT_CACHE");

        if (reports != null)
            reports.clear();

    }

    public static ReportPage setCurrentReportPage(ReportPage reportPage, int page) {

        int recordCount = reportPage.getAllRecords().size();
        int startRecord = 0;
        int endRecord = recordCount;

        if (page != -1) {

            startRecord = (page - 1) * ReportPage.NUMBER_RECORD_PER_PAGE;
            endRecord = 0;
            if (recordCount > ReportPage.NUMBER_RECORD_PER_PAGE) {
                endRecord = startRecord + ReportPage.NUMBER_RECORD_PER_PAGE;
                if (endRecord > recordCount)
                    endRecord = recordCount - 1;
            } else {
                endRecord = recordCount;
            }

        }

//        EventiPagati eventoPagato = null;
//        List<EventiPagati> listaEventi = new ArrayList<EventiPagati>();
//        for (int i = startRecord; i < endRecord; i++) {
//            eventoPagato = (EventiPagati)reportPage.getAllRecords().get(i);
//            listaEventi.add(eventoPagato);
//        }
//        reportPage.setRecords(listaEventi, page);

        Object object = null;
        List<Object> objectsList = new ArrayList<Object>();

        for (int i = startRecord; i < endRecord; i++) {

            object = reportPage.getAllRecords().get(i);

            objectsList.add(object);
        }

        reportPage.setRecords(objectsList, page);

        return reportPage;

    }

}
