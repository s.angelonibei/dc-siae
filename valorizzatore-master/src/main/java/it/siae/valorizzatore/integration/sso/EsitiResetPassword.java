
package it.siae.valorizzatore.integration.sso;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EsitiResetPassword.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EsitiResetPassword">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="OK"/>
 *     &lt;enumeration value="AccountInesistente"/>
 *     &lt;enumeration value="RispostaSegretaAssente"/>
 *     &lt;enumeration value="RispostaSegretaErrata"/>
 *     &lt;enumeration value="EmailAssente"/>
 *     &lt;enumeration value="InvioEmailErrato"/>
 *     &lt;enumeration value="ErroreGenerico"/>
 *     &lt;enumeration value="InputNonValido"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EsitiResetPassword")
@XmlEnum
public enum EsitiResetPassword {

    OK("OK"),
    @XmlEnumValue("AccountInesistente")
    ACCOUNT_INESISTENTE("AccountInesistente"),
    @XmlEnumValue("RispostaSegretaAssente")
    RISPOSTA_SEGRETA_ASSENTE("RispostaSegretaAssente"),
    @XmlEnumValue("RispostaSegretaErrata")
    RISPOSTA_SEGRETA_ERRATA("RispostaSegretaErrata"),
    @XmlEnumValue("EmailAssente")
    EMAIL_ASSENTE("EmailAssente"),
    @XmlEnumValue("InvioEmailErrato")
    INVIO_EMAIL_ERRATO("InvioEmailErrato"),
    @XmlEnumValue("ErroreGenerico")
    ERRORE_GENERICO("ErroreGenerico"),
    @XmlEnumValue("InputNonValido")
    INPUT_NON_VALIDO("InputNonValido");
    private final String value;

    EsitiResetPassword(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EsitiResetPassword fromValue(String v) {
        for (EsitiResetPassword c: EsitiResetPassword.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
