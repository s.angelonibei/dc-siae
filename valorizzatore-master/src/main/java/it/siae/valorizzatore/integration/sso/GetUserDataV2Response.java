
package it.siae.valorizzatore.integration.sso;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetUserData_v2Result" type="{http://aas.siae.it/}UtenteBackOffice_v2" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getUserDataV2Result"
})
@XmlRootElement(name = "GetUserData_v2Response")
public class GetUserDataV2Response {

    @XmlElement(name = "GetUserData_v2Result")
    protected UtenteBackOfficeV2 getUserDataV2Result;

    /**
     * Gets the value of the getUserDataV2Result property.
     * 
     * @return
     *     possible object is
     *     {@link UtenteBackOfficeV2 }
     *     
     */
    public UtenteBackOfficeV2 getGetUserDataV2Result() {
        return getUserDataV2Result;
    }

    /**
     * Sets the value of the getUserDataV2Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link UtenteBackOfficeV2 }
     *     
     */
    public void setGetUserDataV2Result(UtenteBackOfficeV2 value) {
        this.getUserDataV2Result = value;
    }

}
