
package it.siae.valorizzatore.integration.sso;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResetPasswordResult" type="{http://aas.siae.it/}EsitiResetPassword"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "resetPasswordResult"
})
@XmlRootElement(name = "ResetPasswordResponse")
public class ResetPasswordResponse {

    @XmlElement(name = "ResetPasswordResult", required = true)
    protected EsitiResetPassword resetPasswordResult;

    /**
     * Gets the value of the resetPasswordResult property.
     * 
     * @return
     *     possible object is
     *     {@link EsitiResetPassword }
     *     
     */
    public EsitiResetPassword getResetPasswordResult() {
        return resetPasswordResult;
    }

    /**
     * Sets the value of the resetPasswordResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link EsitiResetPassword }
     *     
     */
    public void setResetPasswordResult(EsitiResetPassword value) {
        this.resetPasswordResult = value;
    }

}
