
package it.siae.valorizzatore.integration.sso;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UtenteBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UtenteBase">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UserName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TokenID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Denominazione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatoPassword" type="{http://aas.siae.it/}EsitiAutenticazione"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UtenteBase", propOrder = {
    "userName",
    "tokenID",
    "denominazione",
    "email",
    "statoPassword"
})
@XmlSeeAlso({
    UtenteBackOffice.class,
    UtenteFrontOffice.class
})
public abstract class UtenteBase {

    @XmlElement(name = "UserName")
    protected String userName;
    @XmlElement(name = "TokenID")
    protected String tokenID;
    @XmlElement(name = "Denominazione")
    protected String denominazione;
    @XmlElement(name = "Email")
    protected String email;
    @XmlElement(name = "StatoPassword", required = true)
    protected EsitiAutenticazione statoPassword;

    /**
     * Gets the value of the userName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the userName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Gets the value of the tokenID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTokenID() {
        return tokenID;
    }

    /**
     * Sets the value of the tokenID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTokenID(String value) {
        this.tokenID = value;
    }

    /**
     * Gets the value of the denominazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDenominazione() {
        return denominazione;
    }

    /**
     * Sets the value of the denominazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDenominazione(String value) {
        this.denominazione = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the statoPassword property.
     * 
     * @return
     *     possible object is
     *     {@link EsitiAutenticazione }
     *     
     */
    public EsitiAutenticazione getStatoPassword() {
        return statoPassword;
    }

    /**
     * Sets the value of the statoPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link EsitiAutenticazione }
     *     
     */
    public void setStatoPassword(EsitiAutenticazione value) {
        this.statoPassword = value;
    }

}
