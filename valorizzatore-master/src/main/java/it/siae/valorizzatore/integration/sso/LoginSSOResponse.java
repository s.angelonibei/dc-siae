
package it.siae.valorizzatore.integration.sso;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LoginSSOResult" type="{http://aas.siae.it/}UniqueLoginResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "loginSSOResult"
})
@XmlRootElement(name = "LoginSSOResponse")
public class LoginSSOResponse {

    @XmlElement(name = "LoginSSOResult")
    protected UniqueLoginResponse loginSSOResult;

    /**
     * Gets the value of the loginSSOResult property.
     * 
     * @return
     *     possible object is
     *     {@link UniqueLoginResponse }
     *     
     */
    public UniqueLoginResponse getLoginSSOResult() {
        return loginSSOResult;
    }

    /**
     * Sets the value of the loginSSOResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link UniqueLoginResponse }
     *     
     */
    public void setLoginSSOResult(UniqueLoginResponse value) {
        this.loginSSOResult = value;
    }

}
