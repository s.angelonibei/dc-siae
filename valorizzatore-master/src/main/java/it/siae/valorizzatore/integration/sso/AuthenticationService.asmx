<?xml version="1.0" encoding="utf-8"?>
<wsdl:definitions xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:tns="http://aas.siae.it/" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tm="http://microsoft.com/wsdl/mime/textMatching/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" targetNamespace="http://aas.siae.it/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/">
  <wsdl:types>
    <s:schema elementFormDefault="qualified" targetNamespace="http://aas.siae.it/">
      <s:element name="ResetPassword">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="userName" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="ip" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="operatore" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="securityPassword" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="ResetPasswordResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="ResetPasswordResult" type="tns:EsitiResetPassword" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:simpleType name="EsitiResetPassword">
        <s:restriction base="s:string">
          <s:enumeration value="OK" />
          <s:enumeration value="AccountInesistente" />
          <s:enumeration value="RispostaSegretaAssente" />
          <s:enumeration value="RispostaSegretaErrata" />
          <s:enumeration value="EmailAssente" />
          <s:enumeration value="InvioEmailErrato" />
          <s:enumeration value="ErroreGenerico" />
          <s:enumeration value="InputNonValido" />
        </s:restriction>
      </s:simpleType>
      <s:element name="ChangePassword">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="userName" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="oldPassword" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="newPassword" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="ip" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="operatore" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="securityPassword" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="ChangePasswordResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="ChangePasswordResult" type="tns:EsitiCambioPassword" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:simpleType name="EsitiCambioPassword">
        <s:restriction base="s:string">
          <s:enumeration value="OK" />
          <s:enumeration value="VecchiaPasswordErrata" />
          <s:enumeration value="NuovaPasswordNonAmmissibile" />
          <s:enumeration value="AccountBloccato" />
          <s:enumeration value="AccountTemporaneamenteBloccato" />
          <s:enumeration value="AccountInesistente" />
          <s:enumeration value="InputNonValido" />
          <s:enumeration value="ErroreGenerico" />
          <s:enumeration value="NuovaPasswordNonConfermata" />
        </s:restriction>
      </s:simpleType>
      <s:element name="Login">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="userName" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="password" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="LoginResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="LoginResult" type="tns:EsitiAutenticazione" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:simpleType name="EsitiAutenticazione">
        <s:restriction base="s:string">
          <s:enumeration value="OK" />
          <s:enumeration value="PasswordErrata" />
          <s:enumeration value="PasswordScaduta" />
          <s:enumeration value="AccountBloccato" />
          <s:enumeration value="AccountTemporaneamenteBloccato" />
          <s:enumeration value="AccountInesistente" />
          <s:enumeration value="ErroreGenerico" />
        </s:restriction>
      </s:simpleType>
      <s:element name="GetUserData">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="userName" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetUserDataResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetUserDataResult" type="tns:UtenteBackOffice" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="UtenteBackOffice">
        <s:complexContent mixed="false">
          <s:extension base="tns:UtenteBase">
            <s:sequence>
              <s:element minOccurs="0" maxOccurs="1" name="PuntoPeriferico" type="tns:PuntoPeriferico" />
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType name="UtenteBase" abstract="true">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="1" name="UserName" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="TokenID" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Denominazione" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Email" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="StatoPassword" type="tns:EsitiAutenticazione" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="UtenteFrontOffice">
        <s:complexContent mixed="false">
          <s:extension base="tns:UtenteBase">
            <s:sequence>
              <s:element minOccurs="1" maxOccurs="1" name="IdPersona" type="s:int" />
              <s:element minOccurs="1" maxOccurs="1" name="IdParent" type="s:int" />
              <s:element minOccurs="0" maxOccurs="1" name="DenominazioneParent" type="s:string" />
              <s:element minOccurs="1" maxOccurs="1" name="Grado" type="tns:GradoPersona" />
              <s:element minOccurs="1" maxOccurs="1" name="Attivo" type="s:boolean" />
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:simpleType name="GradoPersona">
        <s:restriction base="s:string">
          <s:enumeration value="Forte" />
          <s:enumeration value="Debole" />
        </s:restriction>
      </s:simpleType>
      <s:complexType name="PuntoPeriferico">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="Tipo" type="tns:TipoPuntoPeriferico" />
          <s:element minOccurs="0" maxOccurs="1" name="CodiceSeprag" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Descrizione" type="s:string" />
        </s:sequence>
      </s:complexType>
      <s:simpleType name="TipoPuntoPeriferico">
        <s:restriction base="s:string">
          <s:enumeration value="DG" />
          <s:enumeration value="Mandataria" />
          <s:enumeration value="Filiale" />
          <s:enumeration value="Agenzia" />
          <s:enumeration value="Sede" />
          <s:enumeration value="Presidio" />
        </s:restriction>
      </s:simpleType>
      <s:element name="GetUserData_v2">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="userName" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetUserData_v2Response">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetUserData_v2Result" type="tns:UtenteBackOffice_v2" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="UtenteBackOffice_v2">
        <s:complexContent mixed="false">
          <s:extension base="tns:UtenteBackOffice">
            <s:sequence>
              <s:element minOccurs="0" maxOccurs="1" name="Nome" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="Cognome" type="s:string" />
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:element name="GetUserGroups">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="userName" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="appCode" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetUserGroupsResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetUserGroupsResult" type="tns:ArrayOfString" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="ArrayOfString">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" name="string" nillable="true" type="s:string" />
        </s:sequence>
      </s:complexType>
      <s:element name="GetGroupMembers">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="applicationGroup" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="subApplicationGroup" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetGroupMembersResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetGroupMembersResult" type="tns:ArrayOfString" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetSecurityLevel">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="applicationGroup" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="applicationSubGroup" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetSecurityLevelResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="GetSecurityLevelResult" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetLdapPasswordExpirationTime">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="userName" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetLdapPasswordExpirationTimeResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="GetLdapPasswordExpirationTimeResult" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="LoginSSO">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="userName" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="password" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="ip" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="LoginSSOResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="LoginSSOResult" type="tns:UniqueLoginResponse" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="UniqueLoginResponse">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="1" name="Token" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="Esito" type="tns:EsitiAutenticazione" />
        </s:sequence>
      </s:complexType>
      <s:element name="ExpireToken">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="authenticationToken" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="ip" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="ExpireTokenResponse">
        <s:complexType />
      </s:element>
      <s:element name="RefreshToken">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="authenticationToken" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="ip" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="RefreshTokenResponse">
        <s:complexType />
      </s:element>
      <s:element name="CheckToken">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="authenticationToken" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="applicationCode" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="ip" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="CheckTokenResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="CheckTokenResult" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
    </s:schema>
  </wsdl:types>
  <wsdl:message name="ResetPasswordSoapIn">
    <wsdl:part name="parameters" element="tns:ResetPassword" />
  </wsdl:message>
  <wsdl:message name="ResetPasswordSoapOut">
    <wsdl:part name="parameters" element="tns:ResetPasswordResponse" />
  </wsdl:message>
  <wsdl:message name="ChangePasswordSoapIn">
    <wsdl:part name="parameters" element="tns:ChangePassword" />
  </wsdl:message>
  <wsdl:message name="ChangePasswordSoapOut">
    <wsdl:part name="parameters" element="tns:ChangePasswordResponse" />
  </wsdl:message>
  <wsdl:message name="LoginSoapIn">
    <wsdl:part name="parameters" element="tns:Login" />
  </wsdl:message>
  <wsdl:message name="LoginSoapOut">
    <wsdl:part name="parameters" element="tns:LoginResponse" />
  </wsdl:message>
  <wsdl:message name="GetUserDataSoapIn">
    <wsdl:part name="parameters" element="tns:GetUserData" />
  </wsdl:message>
  <wsdl:message name="GetUserDataSoapOut">
    <wsdl:part name="parameters" element="tns:GetUserDataResponse" />
  </wsdl:message>
  <wsdl:message name="GetUserData_v2SoapIn">
    <wsdl:part name="parameters" element="tns:GetUserData_v2" />
  </wsdl:message>
  <wsdl:message name="GetUserData_v2SoapOut">
    <wsdl:part name="parameters" element="tns:GetUserData_v2Response" />
  </wsdl:message>
  <wsdl:message name="GetUserGroupsSoapIn">
    <wsdl:part name="parameters" element="tns:GetUserGroups" />
  </wsdl:message>
  <wsdl:message name="GetUserGroupsSoapOut">
    <wsdl:part name="parameters" element="tns:GetUserGroupsResponse" />
  </wsdl:message>
  <wsdl:message name="GetGroupMembersSoapIn">
    <wsdl:part name="parameters" element="tns:GetGroupMembers" />
  </wsdl:message>
  <wsdl:message name="GetGroupMembersSoapOut">
    <wsdl:part name="parameters" element="tns:GetGroupMembersResponse" />
  </wsdl:message>
  <wsdl:message name="GetSecurityLevelSoapIn">
    <wsdl:part name="parameters" element="tns:GetSecurityLevel" />
  </wsdl:message>
  <wsdl:message name="GetSecurityLevelSoapOut">
    <wsdl:part name="parameters" element="tns:GetSecurityLevelResponse" />
  </wsdl:message>
  <wsdl:message name="GetLdapPasswordExpirationTimeSoapIn">
    <wsdl:part name="parameters" element="tns:GetLdapPasswordExpirationTime" />
  </wsdl:message>
  <wsdl:message name="GetLdapPasswordExpirationTimeSoapOut">
    <wsdl:part name="parameters" element="tns:GetLdapPasswordExpirationTimeResponse" />
  </wsdl:message>
  <wsdl:message name="LoginSSOSoapIn">
    <wsdl:part name="parameters" element="tns:LoginSSO" />
  </wsdl:message>
  <wsdl:message name="LoginSSOSoapOut">
    <wsdl:part name="parameters" element="tns:LoginSSOResponse" />
  </wsdl:message>
  <wsdl:message name="ExpireTokenSoapIn">
    <wsdl:part name="parameters" element="tns:ExpireToken" />
  </wsdl:message>
  <wsdl:message name="ExpireTokenSoapOut">
    <wsdl:part name="parameters" element="tns:ExpireTokenResponse" />
  </wsdl:message>
  <wsdl:message name="RefreshTokenSoapIn">
    <wsdl:part name="parameters" element="tns:RefreshToken" />
  </wsdl:message>
  <wsdl:message name="RefreshTokenSoapOut">
    <wsdl:part name="parameters" element="tns:RefreshTokenResponse" />
  </wsdl:message>
  <wsdl:message name="CheckTokenSoapIn">
    <wsdl:part name="parameters" element="tns:CheckToken" />
  </wsdl:message>
  <wsdl:message name="CheckTokenSoapOut">
    <wsdl:part name="parameters" element="tns:CheckTokenResponse" />
  </wsdl:message>
  <wsdl:portType name="AuthenticationServiceSoap">
    <wsdl:operation name="ResetPassword">
      <wsdl:input message="tns:ResetPasswordSoapIn" />
      <wsdl:output message="tns:ResetPasswordSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="ChangePassword">
      <wsdl:input message="tns:ChangePasswordSoapIn" />
      <wsdl:output message="tns:ChangePasswordSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="Login">
      <wsdl:input message="tns:LoginSoapIn" />
      <wsdl:output message="tns:LoginSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetUserData">
      <wsdl:input message="tns:GetUserDataSoapIn" />
      <wsdl:output message="tns:GetUserDataSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetUserData_v2">
      <wsdl:input message="tns:GetUserData_v2SoapIn" />
      <wsdl:output message="tns:GetUserData_v2SoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetUserGroups">
      <wsdl:input message="tns:GetUserGroupsSoapIn" />
      <wsdl:output message="tns:GetUserGroupsSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetGroupMembers">
      <wsdl:input message="tns:GetGroupMembersSoapIn" />
      <wsdl:output message="tns:GetGroupMembersSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetSecurityLevel">
      <wsdl:input message="tns:GetSecurityLevelSoapIn" />
      <wsdl:output message="tns:GetSecurityLevelSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetLdapPasswordExpirationTime">
      <wsdl:input message="tns:GetLdapPasswordExpirationTimeSoapIn" />
      <wsdl:output message="tns:GetLdapPasswordExpirationTimeSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="LoginSSO">
      <wsdl:input message="tns:LoginSSOSoapIn" />
      <wsdl:output message="tns:LoginSSOSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="ExpireToken">
      <wsdl:input message="tns:ExpireTokenSoapIn" />
      <wsdl:output message="tns:ExpireTokenSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="RefreshToken">
      <wsdl:input message="tns:RefreshTokenSoapIn" />
      <wsdl:output message="tns:RefreshTokenSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="CheckToken">
      <wsdl:input message="tns:CheckTokenSoapIn" />
      <wsdl:output message="tns:CheckTokenSoapOut" />
    </wsdl:operation>
  </wsdl:portType>
  <wsdl:binding name="AuthenticationServiceSoap" type="tns:AuthenticationServiceSoap">
    <soap:binding transport="http://schemas.xmlsoap.org/soap/http" />
    <wsdl:operation name="ResetPassword">
      <soap:operation soapAction="http://aas.siae.it/ResetPassword" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ChangePassword">
      <soap:operation soapAction="http://aas.siae.it/ChangePassword" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="Login">
      <soap:operation soapAction="http://aas.siae.it/Login" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetUserData">
      <soap:operation soapAction="http://aas.siae.it/GetUserData" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetUserData_v2">
      <soap:operation soapAction="http://aas.siae.it/GetUserData_v2" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetUserGroups">
      <soap:operation soapAction="http://aas.siae.it/GetUserGroups" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetGroupMembers">
      <soap:operation soapAction="http://aas.siae.it/GetGroupMembers" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSecurityLevel">
      <soap:operation soapAction="http://aas.siae.it/GetSecurityLevel" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetLdapPasswordExpirationTime">
      <soap:operation soapAction="http://aas.siae.it/GetLdapPasswordExpirationTime" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="LoginSSO">
      <soap:operation soapAction="http://aas.siae.it/LoginSSO" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ExpireToken">
      <soap:operation soapAction="http://aas.siae.it/ExpireToken" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="RefreshToken">
      <soap:operation soapAction="http://aas.siae.it/RefreshToken" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CheckToken">
      <soap:operation soapAction="http://aas.siae.it/CheckToken" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:binding name="AuthenticationServiceSoap12" type="tns:AuthenticationServiceSoap">
    <soap12:binding transport="http://schemas.xmlsoap.org/soap/http" />
    <wsdl:operation name="ResetPassword">
      <soap12:operation soapAction="http://aas.siae.it/ResetPassword" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ChangePassword">
      <soap12:operation soapAction="http://aas.siae.it/ChangePassword" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="Login">
      <soap12:operation soapAction="http://aas.siae.it/Login" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetUserData">
      <soap12:operation soapAction="http://aas.siae.it/GetUserData" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetUserData_v2">
      <soap12:operation soapAction="http://aas.siae.it/GetUserData_v2" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetUserGroups">
      <soap12:operation soapAction="http://aas.siae.it/GetUserGroups" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetGroupMembers">
      <soap12:operation soapAction="http://aas.siae.it/GetGroupMembers" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSecurityLevel">
      <soap12:operation soapAction="http://aas.siae.it/GetSecurityLevel" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetLdapPasswordExpirationTime">
      <soap12:operation soapAction="http://aas.siae.it/GetLdapPasswordExpirationTime" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="LoginSSO">
      <soap12:operation soapAction="http://aas.siae.it/LoginSSO" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ExpireToken">
      <soap12:operation soapAction="http://aas.siae.it/ExpireToken" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="RefreshToken">
      <soap12:operation soapAction="http://aas.siae.it/RefreshToken" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CheckToken">
      <soap12:operation soapAction="http://aas.siae.it/CheckToken" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:service name="AuthenticationService">
    <wsdl:port name="AuthenticationServiceSoap" binding="tns:AuthenticationServiceSoap">
      <soap:address location="http://aasws.sviluppo.siae/AuthenticationService.asmx" />
    </wsdl:port>
    <wsdl:port name="AuthenticationServiceSoap12" binding="tns:AuthenticationServiceSoap12">
      <soap12:address location="http://aasws.sviluppo.siae/AuthenticationService.asmx" />
    </wsdl:port>
  </wsdl:service>
</wsdl:definitions>