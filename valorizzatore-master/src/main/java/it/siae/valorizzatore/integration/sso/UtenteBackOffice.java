
package it.siae.valorizzatore.integration.sso;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UtenteBackOffice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UtenteBackOffice">
 *   &lt;complexContent>
 *     &lt;extension base="{http://aas.siae.it/}UtenteBase">
 *       &lt;sequence>
 *         &lt;element name="PuntoPeriferico" type="{http://aas.siae.it/}PuntoPeriferico" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UtenteBackOffice", propOrder = {
    "puntoPeriferico"
})
@XmlSeeAlso({
    UtenteBackOfficeV2 .class
})
public class UtenteBackOffice
    extends UtenteBase
{

    @XmlElement(name = "PuntoPeriferico")
    protected PuntoPeriferico puntoPeriferico;

    /**
     * Gets the value of the puntoPeriferico property.
     * 
     * @return
     *     possible object is
     *     {@link PuntoPeriferico }
     *     
     */
    public PuntoPeriferico getPuntoPeriferico() {
        return puntoPeriferico;
    }

    /**
     * Sets the value of the puntoPeriferico property.
     * 
     * @param value
     *     allowed object is
     *     {@link PuntoPeriferico }
     *     
     */
    public void setPuntoPeriferico(PuntoPeriferico value) {
        this.puntoPeriferico = value;
    }

}
