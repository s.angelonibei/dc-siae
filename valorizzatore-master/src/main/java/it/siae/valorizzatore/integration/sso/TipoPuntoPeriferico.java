
package it.siae.valorizzatore.integration.sso;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TipoPuntoPeriferico.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TipoPuntoPeriferico">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DG"/>
 *     &lt;enumeration value="Mandataria"/>
 *     &lt;enumeration value="Filiale"/>
 *     &lt;enumeration value="Agenzia"/>
 *     &lt;enumeration value="Sede"/>
 *     &lt;enumeration value="Presidio"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TipoPuntoPeriferico")
@XmlEnum
public enum TipoPuntoPeriferico {

    DG("DG"),
    @XmlEnumValue("Mandataria")
    MANDATARIA("Mandataria"),
    @XmlEnumValue("Filiale")
    FILIALE("Filiale"),
    @XmlEnumValue("Agenzia")
    AGENZIA("Agenzia"),
    @XmlEnumValue("Sede")
    SEDE("Sede"),
    @XmlEnumValue("Presidio")
    PRESIDIO("Presidio");
    private final String value;

    TipoPuntoPeriferico(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TipoPuntoPeriferico fromValue(String v) {
        for (TipoPuntoPeriferico c: TipoPuntoPeriferico.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
