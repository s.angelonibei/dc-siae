
package it.siae.valorizzatore.integration.sso;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PuntoPeriferico complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PuntoPeriferico">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Tipo" type="{http://aas.siae.it/}TipoPuntoPeriferico"/>
 *         &lt;element name="CodiceSeprag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Descrizione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PuntoPeriferico", propOrder = {
    "tipo",
    "codiceSeprag",
    "descrizione"
})
public class PuntoPeriferico {

    @XmlElement(name = "Tipo", required = true)
    protected TipoPuntoPeriferico tipo;
    @XmlElement(name = "CodiceSeprag")
    protected String codiceSeprag;
    @XmlElement(name = "Descrizione")
    protected String descrizione;

    /**
     * Gets the value of the tipo property.
     * 
     * @return
     *     possible object is
     *     {@link TipoPuntoPeriferico }
     *     
     */
    public TipoPuntoPeriferico getTipo() {
        return tipo;
    }

    /**
     * Sets the value of the tipo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoPuntoPeriferico }
     *     
     */
    public void setTipo(TipoPuntoPeriferico value) {
        this.tipo = value;
    }

    /**
     * Gets the value of the codiceSeprag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceSeprag() {
        return codiceSeprag;
    }

    /**
     * Sets the value of the codiceSeprag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceSeprag(String value) {
        this.codiceSeprag = value;
    }

    /**
     * Gets the value of the descrizione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Sets the value of the descrizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

}
