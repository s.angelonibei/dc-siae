
package it.siae.valorizzatore.integration.sso;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EsitiAutenticazione.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EsitiAutenticazione">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="OK"/>
 *     &lt;enumeration value="PasswordErrata"/>
 *     &lt;enumeration value="PasswordScaduta"/>
 *     &lt;enumeration value="AccountBloccato"/>
 *     &lt;enumeration value="AccountTemporaneamenteBloccato"/>
 *     &lt;enumeration value="AccountInesistente"/>
 *     &lt;enumeration value="ErroreGenerico"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EsitiAutenticazione")
@XmlEnum
public enum EsitiAutenticazione {

    OK("OK"),
    @XmlEnumValue("PasswordErrata")
    PASSWORD_ERRATA("PasswordErrata"),
    @XmlEnumValue("PasswordScaduta")
    PASSWORD_SCADUTA("PasswordScaduta"),
    @XmlEnumValue("AccountBloccato")
    ACCOUNT_BLOCCATO("AccountBloccato"),
    @XmlEnumValue("AccountTemporaneamenteBloccato")
    ACCOUNT_TEMPORANEAMENTE_BLOCCATO("AccountTemporaneamenteBloccato"),
    @XmlEnumValue("AccountInesistente")
    ACCOUNT_INESISTENTE("AccountInesistente"),
    @XmlEnumValue("ErroreGenerico")
    ERRORE_GENERICO("ErroreGenerico");
    private final String value;

    EsitiAutenticazione(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EsitiAutenticazione fromValue(String v) {
        for (EsitiAutenticazione c: EsitiAutenticazione.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
