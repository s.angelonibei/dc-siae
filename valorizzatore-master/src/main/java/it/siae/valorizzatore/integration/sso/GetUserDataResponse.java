
package it.siae.valorizzatore.integration.sso;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetUserDataResult" type="{http://aas.siae.it/}UtenteBackOffice" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getUserDataResult"
})
@XmlRootElement(name = "GetUserDataResponse")
public class GetUserDataResponse {

    @XmlElement(name = "GetUserDataResult")
    protected UtenteBackOffice getUserDataResult;

    /**
     * Gets the value of the getUserDataResult property.
     * 
     * @return
     *     possible object is
     *     {@link UtenteBackOffice }
     *     
     */
    public UtenteBackOffice getGetUserDataResult() {
        return getUserDataResult;
    }

    /**
     * Sets the value of the getUserDataResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link UtenteBackOffice }
     *     
     */
    public void setGetUserDataResult(UtenteBackOffice value) {
        this.getUserDataResult = value;
    }

}
