
package it.siae.valorizzatore.integration.sso;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UtenteFrontOffice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UtenteFrontOffice">
 *   &lt;complexContent>
 *     &lt;extension base="{http://aas.siae.it/}UtenteBase">
 *       &lt;sequence>
 *         &lt;element name="IdPersona" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IdParent" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DenominazioneParent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Grado" type="{http://aas.siae.it/}GradoPersona"/>
 *         &lt;element name="Attivo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UtenteFrontOffice", propOrder = {
    "idPersona",
    "idParent",
    "denominazioneParent",
    "grado",
    "attivo"
})
public class UtenteFrontOffice
    extends UtenteBase
{

    @XmlElement(name = "IdPersona")
    protected int idPersona;
    @XmlElement(name = "IdParent")
    protected int idParent;
    @XmlElement(name = "DenominazioneParent")
    protected String denominazioneParent;
    @XmlElement(name = "Grado", required = true)
    protected GradoPersona grado;
    @XmlElement(name = "Attivo")
    protected boolean attivo;

    /**
     * Gets the value of the idPersona property.
     * 
     */
    public int getIdPersona() {
        return idPersona;
    }

    /**
     * Sets the value of the idPersona property.
     * 
     */
    public void setIdPersona(int value) {
        this.idPersona = value;
    }

    /**
     * Gets the value of the idParent property.
     * 
     */
    public int getIdParent() {
        return idParent;
    }

    /**
     * Sets the value of the idParent property.
     * 
     */
    public void setIdParent(int value) {
        this.idParent = value;
    }

    /**
     * Gets the value of the denominazioneParent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDenominazioneParent() {
        return denominazioneParent;
    }

    /**
     * Sets the value of the denominazioneParent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDenominazioneParent(String value) {
        this.denominazioneParent = value;
    }

    /**
     * Gets the value of the grado property.
     * 
     * @return
     *     possible object is
     *     {@link GradoPersona }
     *     
     */
    public GradoPersona getGrado() {
        return grado;
    }

    /**
     * Sets the value of the grado property.
     * 
     * @param value
     *     allowed object is
     *     {@link GradoPersona }
     *     
     */
    public void setGrado(GradoPersona value) {
        this.grado = value;
    }

    /**
     * Gets the value of the attivo property.
     * 
     */
    public boolean isAttivo() {
        return attivo;
    }

    /**
     * Sets the value of the attivo property.
     * 
     */
    public void setAttivo(boolean value) {
        this.attivo = value;
    }

}
