
package it.siae.valorizzatore.integration.sso;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GradoPersona.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="GradoPersona">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Forte"/>
 *     &lt;enumeration value="Debole"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "GradoPersona")
@XmlEnum
public enum GradoPersona {

    @XmlEnumValue("Forte")
    FORTE("Forte"),
    @XmlEnumValue("Debole")
    DEBOLE("Debole");
    private final String value;

    GradoPersona(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GradoPersona fromValue(String v) {
        for (GradoPersona c: GradoPersona.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
