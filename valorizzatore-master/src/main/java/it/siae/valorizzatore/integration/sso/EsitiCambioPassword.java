
package it.siae.valorizzatore.integration.sso;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EsitiCambioPassword.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EsitiCambioPassword">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="OK"/>
 *     &lt;enumeration value="VecchiaPasswordErrata"/>
 *     &lt;enumeration value="NuovaPasswordNonAmmissibile"/>
 *     &lt;enumeration value="AccountBloccato"/>
 *     &lt;enumeration value="AccountTemporaneamenteBloccato"/>
 *     &lt;enumeration value="AccountInesistente"/>
 *     &lt;enumeration value="InputNonValido"/>
 *     &lt;enumeration value="ErroreGenerico"/>
 *     &lt;enumeration value="NuovaPasswordNonConfermata"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EsitiCambioPassword")
@XmlEnum
public enum EsitiCambioPassword {

    OK("OK"),
    @XmlEnumValue("VecchiaPasswordErrata")
    VECCHIA_PASSWORD_ERRATA("VecchiaPasswordErrata"),
    @XmlEnumValue("NuovaPasswordNonAmmissibile")
    NUOVA_PASSWORD_NON_AMMISSIBILE("NuovaPasswordNonAmmissibile"),
    @XmlEnumValue("AccountBloccato")
    ACCOUNT_BLOCCATO("AccountBloccato"),
    @XmlEnumValue("AccountTemporaneamenteBloccato")
    ACCOUNT_TEMPORANEAMENTE_BLOCCATO("AccountTemporaneamenteBloccato"),
    @XmlEnumValue("AccountInesistente")
    ACCOUNT_INESISTENTE("AccountInesistente"),
    @XmlEnumValue("InputNonValido")
    INPUT_NON_VALIDO("InputNonValido"),
    @XmlEnumValue("ErroreGenerico")
    ERRORE_GENERICO("ErroreGenerico"),
    @XmlEnumValue("NuovaPasswordNonConfermata")
    NUOVA_PASSWORD_NON_CONFERMATA("NuovaPasswordNonConfermata");
    private final String value;

    EsitiCambioPassword(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EsitiCambioPassword fromValue(String v) {
        for (EsitiCambioPassword c: EsitiCambioPassword.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
