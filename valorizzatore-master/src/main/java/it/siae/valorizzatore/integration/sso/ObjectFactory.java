
package it.siae.valorizzatore.integration.sso;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the it.siae.valorizzatore.integration package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: it.siae.valorizzatore.integration
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LoginSSO }
     * 
     */
    public LoginSSO createLoginSSO() {
        return new LoginSSO();
    }

    /**
     * Create an instance of {@link ChangePassword }
     * 
     */
    public ChangePassword createChangePassword() {
        return new ChangePassword();
    }

    /**
     * Create an instance of {@link Login }
     * 
     */
    public Login createLogin() {
        return new Login();
    }

    /**
     * Create an instance of {@link GetUserGroupsResponse }
     * 
     */
    public GetUserGroupsResponse createGetUserGroupsResponse() {
        return new GetUserGroupsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfString }
     * 
     */
    public ArrayOfString createArrayOfString() {
        return new ArrayOfString();
    }

    /**
     * Create an instance of {@link GetSecurityLevel }
     * 
     */
    public GetSecurityLevel createGetSecurityLevel() {
        return new GetSecurityLevel();
    }

    /**
     * Create an instance of {@link ExpireToken }
     * 
     */
    public ExpireToken createExpireToken() {
        return new ExpireToken();
    }

    /**
     * Create an instance of {@link GetUserGroups }
     * 
     */
    public GetUserGroups createGetUserGroups() {
        return new GetUserGroups();
    }

    /**
     * Create an instance of {@link GetLdapPasswordExpirationTime }
     * 
     */
    public GetLdapPasswordExpirationTime createGetLdapPasswordExpirationTime() {
        return new GetLdapPasswordExpirationTime();
    }

    /**
     * Create an instance of {@link GetGroupMembersResponse }
     * 
     */
    public GetGroupMembersResponse createGetGroupMembersResponse() {
        return new GetGroupMembersResponse();
    }

    /**
     * Create an instance of {@link GetUserData }
     * 
     */
    public GetUserData createGetUserData() {
        return new GetUserData();
    }

    /**
     * Create an instance of {@link RefreshToken }
     * 
     */
    public RefreshToken createRefreshToken() {
        return new RefreshToken();
    }

    /**
     * Create an instance of {@link GetLdapPasswordExpirationTimeResponse }
     * 
     */
    public GetLdapPasswordExpirationTimeResponse createGetLdapPasswordExpirationTimeResponse() {
        return new GetLdapPasswordExpirationTimeResponse();
    }

    /**
     * Create an instance of {@link GetUserDataV2Response }
     * 
     */
    public GetUserDataV2Response createGetUserDataV2Response() {
        return new GetUserDataV2Response();
    }

    /**
     * Create an instance of {@link UtenteBackOfficeV2 }
     * 
     */
    public UtenteBackOfficeV2 createUtenteBackOfficeV2() {
        return new UtenteBackOfficeV2();
    }

    /**
     * Create an instance of {@link GetUserDataV2 }
     * 
     */
    public GetUserDataV2 createGetUserDataV2() {
        return new GetUserDataV2();
    }

    /**
     * Create an instance of {@link ExpireTokenResponse }
     * 
     */
    public ExpireTokenResponse createExpireTokenResponse() {
        return new ExpireTokenResponse();
    }

    /**
     * Create an instance of {@link LoginSSOResponse }
     * 
     */
    public LoginSSOResponse createLoginSSOResponse() {
        return new LoginSSOResponse();
    }

    /**
     * Create an instance of {@link UniqueLoginResponse }
     * 
     */
    public UniqueLoginResponse createUniqueLoginResponse() {
        return new UniqueLoginResponse();
    }

    /**
     * Create an instance of {@link CheckToken }
     * 
     */
    public CheckToken createCheckToken() {
        return new CheckToken();
    }

    /**
     * Create an instance of {@link ResetPassword }
     * 
     */
    public ResetPassword createResetPassword() {
        return new ResetPassword();
    }

    /**
     * Create an instance of {@link GetUserDataResponse }
     * 
     */
    public GetUserDataResponse createGetUserDataResponse() {
        return new GetUserDataResponse();
    }

    /**
     * Create an instance of {@link UtenteBackOffice }
     * 
     */
    public UtenteBackOffice createUtenteBackOffice() {
        return new UtenteBackOffice();
    }

    /**
     * Create an instance of {@link RefreshTokenResponse }
     * 
     */
    public RefreshTokenResponse createRefreshTokenResponse() {
        return new RefreshTokenResponse();
    }

    /**
     * Create an instance of {@link GetSecurityLevelResponse }
     * 
     */
    public GetSecurityLevelResponse createGetSecurityLevelResponse() {
        return new GetSecurityLevelResponse();
    }

    /**
     * Create an instance of {@link ResetPasswordResponse }
     * 
     */
    public ResetPasswordResponse createResetPasswordResponse() {
        return new ResetPasswordResponse();
    }

    /**
     * Create an instance of {@link CheckTokenResponse }
     * 
     */
    public CheckTokenResponse createCheckTokenResponse() {
        return new CheckTokenResponse();
    }

    /**
     * Create an instance of {@link LoginResponse }
     * 
     */
    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    /**
     * Create an instance of {@link GetGroupMembers }
     * 
     */
    public GetGroupMembers createGetGroupMembers() {
        return new GetGroupMembers();
    }

    /**
     * Create an instance of {@link ChangePasswordResponse }
     * 
     */
    public ChangePasswordResponse createChangePasswordResponse() {
        return new ChangePasswordResponse();
    }

    /**
     * Create an instance of {@link UtenteFrontOffice }
     * 
     */
    public UtenteFrontOffice createUtenteFrontOffice() {
        return new UtenteFrontOffice();
    }

    /**
     * Create an instance of {@link PuntoPeriferico }
     * 
     */
    public PuntoPeriferico createPuntoPeriferico() {
        return new PuntoPeriferico();
    }

}
