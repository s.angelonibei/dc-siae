package it.siae.valorizzatore.controller;

import groovy.json.JsonBuilder;
import it.siae.valorizzatore.model.Circoscrizione;
import it.siae.valorizzatore.model.EsecuzioneRicalcolo;
import it.siae.valorizzatore.model.EventiPagati;
import it.siae.valorizzatore.model.ProgrammaMusicale;
import it.siae.valorizzatore.model.security.ReportPage;
import it.siae.valorizzatore.model.view.BreadCrumb;
import it.siae.valorizzatore.model.view.BreadCrumbList;
import it.siae.valorizzatore.service.*;
import it.siae.valorizzatore.utility.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.print.DocFlavor;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/secure")
public class ConsoleCruscottiController {


    @Autowired
    private ICampionamentoService campionamentoService;

    @Autowired
    private IProgrammaMusicaleService programmaMusicaleService;

    @Autowired
    private IConsoleRicalcoloService consoleRicalcoloService;

    @Autowired
    private IConfigurationService configurationService;


    @Autowired
    private ITraceService traceService;


    @Autowired
    private ICreateExcel createExcel;

    /**
     * Ricalcolo Controllo
     *
     * @param model
     * @return
     */
    @RequestMapping(value="/ricalcoloControllo",  method= RequestMethod.GET)
    public String ricalcoloControllo(Model model) {

        traceService.trace("Cruscotti", this.getClass().getName(), "/ricalcoloControllo", "Accesso verifica di un ricalcolo avvenuto", Constants.CLICK);

        List<EsecuzioneRicalcolo> esecuzioni = consoleRicalcoloService.getEsecuzioni();

        String currentYear = Utility.dateString(new Date(), "yyyy");
        String currentMonth = Utility.dateString(new Date(), "MM");
        String finePeriodoRicalcolo = configurationService.getParameter("FINE_PERIODO_RIPARTIZIONE");

        Long annoFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(0,4));
        Long meseFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(4,6));

        if (meseFinePeriodoRicalcolo==12) {
            model.addAttribute("meseInizioPeriodoContabile", "01");
            model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo+1);
        }else {
            if (meseFinePeriodoRicalcolo<9)
                model.addAttribute("meseInizioPeriodoContabile", "0"+meseFinePeriodoRicalcolo);
            else
                model.addAttribute("meseInizioPeriodoContabile", meseFinePeriodoRicalcolo);

            model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo);
        }


        if (esecuzioni!=null && esecuzioni.size()>0)
            model.addAttribute("elabSelected", esecuzioni.get(0).getId());
        else
            model.addAttribute("elabSelected", -1);

        model.addAttribute("meseFinePeriodoContabile", currentMonth);
        model.addAttribute("annoFinePeriodoContabile", currentYear);

        model.addAttribute("tipoSospensione", "RICALCOLATI");
        model.addAttribute("esecuzioni", esecuzioni);

        ReportCache.clearReports();

        return "ricalcoloControllo";
    }


    @RequestMapping(value="/ricalcoloControlloView",  method= RequestMethod.GET)
    public String ricalcoloControlloView(@RequestParam(value="elaborazioneSIADAId", required = false) Long elaborazioneSIADAId,
                                         @RequestParam(value="idEsecuzione") Long idEsecuzione,
                                         @RequestParam(value="evento") Long evento,
                                         @RequestParam(value="fattura") String fattura,
                                         @RequestParam(value="voceIncasso") String voceIncasso,
                                         @RequestParam(value="numeroPM") String numeroPM,
                                         @RequestParam(value="meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
                                         @RequestParam(value="annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
                                         @RequestParam(value="meseFinePeriodoContabile") String meseFinePeriodoContabile,
                                         @RequestParam(value="annoFinePeriodoContabile") String annoFinePeriodoContabile,
                                         @RequestParam(value="tipoSospensione") String tipoSospensione,
                                         @RequestParam(value="page") Integer page,
                                         Model model) {

        traceService.trace("Cruscotti", this.getClass().getName(), "/ricalcoloControlloView", "Visualizzazione report di ricalcolo avvenuto", Constants.CLICK);


        // Setta l'elaborazione arrivata come elaborazione da inviare a SIADA
        if (elaborazioneSIADAId!=null) {

            int result = 0;
            boolean is2243Execution = false;

            EsecuzioneRicalcolo selectedEsecuzione = consoleRicalcoloService.getEsecuzioneRicalcolo(elaborazioneSIADAId);

            String statoEngine = null;

            if (selectedEsecuzione.getVoceIncasso() != null && selectedEsecuzione.getVoceIncasso().equalsIgnoreCase(Constants.VOCE_2243)) {
                is2243Execution = true;
            }

            if (is2243Execution){
                statoEngine = consoleRicalcoloService.checkStatus2243();
            }else {
                statoEngine = consoleRicalcoloService.checkStatus();
            }

            if (statoEngine!=null && statoEngine.equals("ACTIVE")){

                result = consoleRicalcoloService.setDataCongelamentoEsecuzione(elaborazioneSIADAId);

                if (result != 0 && result == -1) {

                    model.addAttribute("message", "Si sono verificati problemi durante l'operazione");

                }else{

                    String status = null;

                    if (is2243Execution) {

                        status = consoleRicalcoloService.startAggregazioneSiada2243(elaborazioneSIADAId);

                        return "aggregazioneEsecuzioneSMC";

                    }else{

                        status = consoleRicalcoloService.startAggregazioneSiada(elaborazioneSIADAId);

                        return "aggregazioneEsecuzione";
                    }


                }

            } else {

                model.addAttribute("message", "Attenzione. Verificare che il server sia attivo dal menu Ricalcolo - Esecuzioni");
            }


        }

        if (page==null)
            page=1;

        if (meseInizioPeriodoContabile.length()==1)
            meseInizioPeriodoContabile="0"+meseInizioPeriodoContabile;
        if (meseFinePeriodoContabile.length()==1)
            meseFinePeriodoContabile="0"+meseFinePeriodoContabile;

        String inizioPeriofoContabile=annoInizioPeriodoContabile+meseInizioPeriodoContabile;
        String finePeriofoContabile=annoFinePeriodoContabile+meseFinePeriodoContabile;


        ReportPage resultPage = consoleRicalcoloService.getListaPM(idEsecuzione, evento, fattura, voceIncasso, numeroPM, inizioPeriofoContabile, finePeriofoContabile, page, tipoSospensione);

        List<EsecuzioneRicalcolo> esecuzioni = consoleRicalcoloService.getEsecuzioni();

        EsecuzioneRicalcolo esecuzioneCorrente = null;
        for (int i=0; i<esecuzioni.size(); i++){
            if (esecuzioni.get(i).getId().longValue()==idEsecuzione.longValue()){
                esecuzioneCorrente=esecuzioni.get(i);
                break;
            }
        }

        model.addAttribute("evento", evento);
        model.addAttribute("fattura", fattura);
        model.addAttribute("voceIncasso", voceIncasso);
        model.addAttribute("numeroPM", numeroPM);

        model.addAttribute("elabSelected", idEsecuzione);
        model.addAttribute("resultPage", resultPage);
        model.addAttribute("esecuzioni", esecuzioni);
        model.addAttribute("esecuzioneCorrente", esecuzioneCorrente);

        model.addAttribute("meseInizioPeriodoContabile", meseInizioPeriodoContabile);
        model.addAttribute("annoInizioPeriodoContabile", annoInizioPeriodoContabile);
        model.addAttribute("meseFinePeriodoContabile", meseFinePeriodoContabile);
        model.addAttribute("annoFinePeriodoContabile", annoFinePeriodoContabile);
        model.addAttribute("tipoSospensione", tipoSospensione);

        return "ricalcoloControllo";
    }



    /**
     * Tracciamento PM
     *
     * @param model
     * @return
     */
    @RequestMapping(value="/tracciamentoPM",  method= RequestMethod.GET)
    public String tracciamentoPM(Model model, HttpServletRequest request) {

        traceService.trace("Cruscotti", this.getClass().getName(), "/tracciamentoPM", "Accesso tracciamento PM", Constants.CLICK);

        // Gestione breadCrumbs in pagina: inizio
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        request = attr.getRequest();
        HttpSession session = attr.getRequest().getSession();
        BreadCrumbList breadCrumbList = new BreadCrumbList();
        String url = request.getContextPath() + request.getServletPath();
        if (request.getQueryString() != null){
            url = url + "?" + request.getQueryString();
        }
        breadCrumbList.addBreadCrumb("Tracciamento PM", url);
        session.setAttribute("breadCrumbList", breadCrumbList);
        // Gestione breadCrumbs in pagina: fine

        session.setAttribute("resultPage", null);
        model.addAttribute("resultPage");

        ReportCache.clearReports();

        return "cruscottiTracciamentoPM";
    }


    @RequestMapping(value="/tracciamentoPMLista",  method= RequestMethod.GET)
    public String tracciamentoPMLista(@RequestParam(value="dataInizio") String dataInizioPeriodo,
                                      @RequestParam(value="dataFine") String dataFinePeriodo,
                                      @RequestParam(value="numeroPM") String numeroPM,
                                      @RequestParam(value="page") Integer page,
                                      Model model) {

        traceService.trace("Cruscotti", this.getClass().getName(), "/tracciamentoPMLista", "Lista tracciamento PM", Constants.CLICK);


        Long numeroProgrammaMusicale=null;
        if (numeroPM != null && !numeroPM.equalsIgnoreCase(""))
            numeroProgrammaMusicale = Long.parseLong(numeroPM);

        ReportPage resultPage = programmaMusicaleService.getTracciamentoPM(Utility.stringDate(dataInizioPeriodo, Utility.OracleFormat_D), Utility.stringDate(dataFinePeriodo, Utility.OracleFormat_D), numeroProgrammaMusicale, page);

        model.addAttribute("dataInizio", dataInizioPeriodo);
        model.addAttribute("dataFine", dataFinePeriodo);
        model.addAttribute("numeroPM", numeroPM);

        model.addAttribute("resultPage", resultPage);

        return "cruscottiTracciamentoPM";
    }


    /**
     * Lista PM Sospesi
     *
     *
     * @return
     */
    @RequestMapping(value="/sospesiPM",  method= RequestMethod.GET)
    public String sospesiPM(Model model) {

        traceService.trace("Cruscotti", this.getClass().getName(), "/sospesiPM", "Accesso lista PM sospesi", Constants.CLICK);

        List<EsecuzioneRicalcolo> esecuzioni = consoleRicalcoloService.getEsecuzioni();

        String currentYear = Utility.dateString(new Date(), "yyyy");
        String currentMonth = Utility.dateString(new Date(), "MM");
        String finePeriodoRicalcolo = configurationService.getParameter("FINE_PERIODO_RIPARTIZIONE");

        Long annoFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(0,4));
        Long meseFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(4,6));

        if (meseFinePeriodoRicalcolo==12) {
            model.addAttribute("meseInizioPeriodoContabile", "01");
            model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo+1);
        }else {
            if (meseFinePeriodoRicalcolo<9)
                model.addAttribute("meseInizioPeriodoContabile", "0"+meseFinePeriodoRicalcolo);
            else
                model.addAttribute("meseInizioPeriodoContabile", meseFinePeriodoRicalcolo);

            model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo);
        }


        if (esecuzioni!=null && esecuzioni.size()>0)
            model.addAttribute("elabSelected", esecuzioni.get(0).getId());
        else
            model.addAttribute("elabSelected", -1);

        model.addAttribute("meseFinePeriodoContabile", currentMonth);
        model.addAttribute("annoFinePeriodoContabile", currentYear);

        model.addAttribute("tipoSospensione", "RICALCOLATI");
        model.addAttribute("esecuzioni", esecuzioni);

        return "cruscottiSospesi";
    }

    @RequestMapping(value="/sospesiPMList",  method= RequestMethod.GET)
    public String sospesiPMList(@RequestParam(value="idEsecuzione") Long idEsecuzione,
                                 @RequestParam(value="meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
                                 @RequestParam(value="annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
                                 @RequestParam(value="meseFinePeriodoContabile") String meseFinePeriodoContabile,
                                 @RequestParam(value="annoFinePeriodoContabile") String annoFinePeriodoContabile,
                                 @RequestParam(value="tipoSospensione") String tipoSospensione,
                                 @RequestParam(value="page") Integer page,
                                 Model model) {

        traceService.trace("Cruscotti", this.getClass().getName(), "/sospesiPMList", "Visualizzazione lista PM sospesi", Constants.CLICK);

        if (page==null)
            page=1;

        if (meseInizioPeriodoContabile.length()==1)
            meseInizioPeriodoContabile="0"+meseInizioPeriodoContabile;
        if (meseFinePeriodoContabile.length()==1)
            meseFinePeriodoContabile="0"+meseFinePeriodoContabile;

        String inizioPeriofoContabile=annoInizioPeriodoContabile+meseInizioPeriodoContabile;
        String finePeriofoContabile=annoFinePeriodoContabile+meseFinePeriodoContabile;


        ReportPage resultPage = consoleRicalcoloService.getListaPM(idEsecuzione, null, null, null, null, inizioPeriofoContabile, finePeriofoContabile, page, tipoSospensione);

        List<EsecuzioneRicalcolo> esecuzioni = consoleRicalcoloService.getEsecuzioni();

        EsecuzioneRicalcolo esecuzioneCorrente = null;
        for (int i=0; i<esecuzioni.size(); i++){
            if (esecuzioni.get(i).getId().longValue()==idEsecuzione.longValue()){
                esecuzioneCorrente=esecuzioni.get(i);
                break;
            }
        }

        model.addAttribute("elabSelected", idEsecuzione);
        model.addAttribute("resultPage", resultPage);
        model.addAttribute("esecuzioni", esecuzioni);
        model.addAttribute("esecuzioneCorrente", esecuzioneCorrente);

        model.addAttribute("meseInizioPeriodoContabile", meseInizioPeriodoContabile);
        model.addAttribute("annoInizioPeriodoContabile", annoInizioPeriodoContabile);
        model.addAttribute("meseFinePeriodoContabile", meseFinePeriodoContabile);
        model.addAttribute("annoFinePeriodoContabile", annoFinePeriodoContabile);
        model.addAttribute("tipoSospensione", tipoSospensione);

        return "cruscottiSospesi";
    }


    @RequestMapping(value="/eventiPagati",  method= RequestMethod.GET)
    public String eventiPagati(Model model) {

        traceService.trace("Cruscotti", this.getClass().getName(), "/eventiPagati", "Accesso lista Eventi Pagati", Constants.CLICK);

        // Gestione breadCrumbs in pagina: inizio
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = attr.getRequest();
        HttpSession session = attr.getRequest().getSession();
        BreadCrumbList breadCrumbList = new BreadCrumbList();
        String url = request.getContextPath() + request.getServletPath();
        if (request.getQueryString() != null){
            url = url + "?" + request.getQueryString();
        }
        breadCrumbList.addBreadCrumb("Visualizzazione Eventi", url);
        session.setAttribute("breadCrumbList", breadCrumbList);
        // Gestione breadCrumbs in pagina: fine


        String currentYear = Utility.dateString(new Date(), "yyyy");
        String currentMonth = Utility.dateString(new Date(), "MM");
        String finePeriodoRicalcolo = configurationService.getParameter("FINE_PERIODO_RIPARTIZIONE");

        Long annoFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(0,4));
        Long meseFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(4,6));

        if (meseFinePeriodoRicalcolo==12) {
            model.addAttribute("meseInizioPeriodoContabile", "01");
            model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo+1);
        }else {
            if (meseFinePeriodoRicalcolo<9)
                model.addAttribute("meseInizioPeriodoContabile", "0"+meseFinePeriodoRicalcolo);
            else
                model.addAttribute("meseInizioPeriodoContabile", meseFinePeriodoRicalcolo);

            model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo);
        }

        model.addAttribute("meseFinePeriodoContabile", currentMonth);
        model.addAttribute("annoFinePeriodoContabile", currentYear);

        session.setAttribute("resultPage", null);
        model.addAttribute("resultPage");

        ReportCache.clearReports();

        return "cruscottiEventiPagati";
    }


    @RequestMapping(value="/eventiPagatiList",  method= RequestMethod.GET)
    public String eventiPagatiList(@RequestParam(value="meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
                                   @RequestParam(value="annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
                                   @RequestParam(value="meseFinePeriodoContabile") String meseFinePeriodoContabile,
                                   @RequestParam(value="annoFinePeriodoContabile") String annoFinePeriodoContabile,
                                   @RequestParam(value="evento") Long evento,
                                   @RequestParam(value="fattura") String fattura,
                                   @RequestParam(value="reversale") String reversale,
                                   @RequestParam(value="voceIncasso") String voceIncasso,
                                   @RequestParam(value="dataInizioEvento") String dataInizioEvento,
                                   @RequestParam(value="oraInizioEvento") String oraInizioEvento,
                                   @RequestParam(value="seprag") String seprag,
                                   @RequestParam(value="tipoDocumento") String tipoDocumento,
                                   @RequestParam(value="locale") String locale,
                                   @RequestParam(value="codiceBA") String codiceBA,
                                   @RequestParam(value="presenzaMovimenti") String presenzaMovimenti,
                                   @RequestParam(value="page") Integer page,
                                   Model model) {

        traceService.trace("Cruscotti", this.getClass().getName(), "/eventiPagatiList", "Visualizzazione lista Eventi Pagati", Constants.CLICK);


        // Gestione breadCrumbs in pagina: inizio
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = attr.getRequest();
        HttpSession session = attr.getRequest().getSession();
        BreadCrumbList breadCrumbList = (BreadCrumbList)session.getAttribute("breadCrumbList");
        String url = request.getContextPath() + request.getServletPath();
        if (request.getQueryString() != null){
            url = url + "?" + request.getQueryString();
        }
        breadCrumbList.addBreadCrumb("Lista Eventi Pagati", url);
        session.setAttribute("breadCrumbList", breadCrumbList);
        // Gestione breadCrumbs in pagina: fine

        if (page==null)
            page=1;

        if (evento!=null && evento.equals(""))
            evento=null;
        if (fattura!=null && fattura.equals(""))
            fattura=null;
        if (reversale!=null && reversale.equals(""))
            reversale=null;
        if (voceIncasso!=null && voceIncasso.equals(""))
            voceIncasso=null;
        if (dataInizioEvento!=null && dataInizioEvento.equals(""))
            dataInizioEvento=null;
        if (oraInizioEvento!=null && oraInizioEvento.equals(""))
            oraInizioEvento=null;
        if (seprag!=null && seprag.equals(""))
            seprag=null;
        if (tipoDocumento!=null && tipoDocumento.equals(""))
            tipoDocumento=null;
        if (locale!=null && locale.equals(""))
            locale=null;
        if (codiceBA!=null && codiceBA.equals(""))
            codiceBA=null;

        if (meseInizioPeriodoContabile.length()==1)
            meseInizioPeriodoContabile="0"+meseInizioPeriodoContabile;
        if (meseFinePeriodoContabile.length()==1)
            meseFinePeriodoContabile="0"+meseFinePeriodoContabile;

        String inizioPeriofoContabile=annoInizioPeriodoContabile+meseInizioPeriodoContabile;
        String finePeriofoContabile=annoFinePeriodoContabile+meseFinePeriodoContabile;

        Date dataEvento = null;
        if (dataInizioEvento!=null)
            dataEvento=Utility.stringDate(dataInizioEvento, Utility.OracleFormat_D);

        ReportPage resultPage = programmaMusicaleService.getListaEventiPagati(Long.parseLong(inizioPeriofoContabile), Long.parseLong(finePeriofoContabile), evento, fattura, reversale, voceIncasso, dataEvento, oraInizioEvento, seprag, tipoDocumento, locale, codiceBA, presenzaMovimenti, page);
        model.addAttribute("resultPage", resultPage);

        model.addAttribute("meseInizioPeriodoContabile", meseInizioPeriodoContabile);
        model.addAttribute("annoInizioPeriodoContabile", annoInizioPeriodoContabile);
        model.addAttribute("meseFinePeriodoContabile", meseFinePeriodoContabile);
        model.addAttribute("annoFinePeriodoContabile", annoFinePeriodoContabile);

        model.addAttribute("evento", evento);
        model.addAttribute("fattura", fattura);
        model.addAttribute("reversale", reversale);
        model.addAttribute("voceIncasso", voceIncasso);
        model.addAttribute("dataInizioEvento", dataInizioEvento);
        model.addAttribute("oraInizioEvento", oraInizioEvento);
        model.addAttribute("seprag", seprag);
        model.addAttribute("tipoDocumento", tipoDocumento);
        model.addAttribute("locale", locale);
        model.addAttribute("codiceBA", codiceBA);
        model.addAttribute("presenzaMovimenti", presenzaMovimenti);

        return "cruscottiEventiPagati";
    }


    @RequestMapping(value="/eventoDetail",  method= RequestMethod.GET)
    public String eventoDetail(@RequestParam(value="evento") Long evento,
                               Model model) {

        traceService.trace("Cruscotti", this.getClass().getName(), "/eventoDetail", "Visualizzazione dettaglio Eventi Pagati", Constants.CLICK);

        // Gestione breadCrumbs in pagina: inizio
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = attr.getRequest();
        HttpSession session = attr.getRequest().getSession();
        BreadCrumbList breadCrumbList = (BreadCrumbList)session.getAttribute("breadCrumbList");
        String url = request.getContextPath() + request.getServletPath();
        if (request.getQueryString() != null){
            url = url + "?" + request.getQueryString();
        }
        breadCrumbList.addBreadCrumb("Dettaglio Evento", url);
        session.setAttribute("breadCrumbList", breadCrumbList);
        // Gestione breadCrumbs in pagina: fine


        List<EventiPagati> eventoPagato = programmaMusicaleService.getEvento(evento);

        model.addAttribute("evento", eventoPagato);

        return "cruscottiEventiPagatiDettaglio";
    }




    @RequestMapping(value="/movimenti",  method= RequestMethod.GET)
    public String movimenti(Model model) {

        traceService.trace("Cruscotti", this.getClass().getName(), "/movimenti", "Accesso visualizzazione movimenti PM", Constants.CLICK);

        // Gestione breadCrumbs in pagina: inizio
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = attr.getRequest();
        HttpSession session = attr.getRequest().getSession();
        BreadCrumbList breadCrumbList = new BreadCrumbList();
        String url = request.getContextPath() + request.getServletPath();
        if (request.getQueryString() != null){
            url = url + "?" + request.getQueryString();
        }
        breadCrumbList.addBreadCrumb("Visualizzazione Movimenti", url);
        session.setAttribute("breadCrumbList", breadCrumbList);
        // Gestione breadCrumbs in pagina: fine

        String currentYear = Utility.dateString(new Date(), "yyyy");
        String currentMonth = Utility.dateString(new Date(), "MM");
        String finePeriodoRicalcolo = configurationService.getParameter("FINE_PERIODO_RIPARTIZIONE");
        List<String> tipoProgrammi = programmaMusicaleService.getTipoProgrammi();

        Long annoFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(0,4));
        Long meseFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(4,6));

        if (meseFinePeriodoRicalcolo==12) {
            model.addAttribute("meseInizioPeriodoContabile", "01");
            model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo+1);
        }else {
            if (meseFinePeriodoRicalcolo<9)
                model.addAttribute("meseInizioPeriodoContabile", "0"+meseFinePeriodoRicalcolo);
            else
                model.addAttribute("meseInizioPeriodoContabile", meseFinePeriodoRicalcolo);

            model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo);
        }

        model.addAttribute("meseFinePeriodoContabile", currentMonth);
        model.addAttribute("annoFinePeriodoContabile", currentYear);

        model.addAttribute("tipoProgrammi", tipoProgrammi);

        session.setAttribute("resultPage", null);
        model.addAttribute("resultPage");

        ReportCache.clearReports();

        return "cruscottiMovimenti";
    }


    @RequestMapping(value="/movimentiList",  method= RequestMethod.GET)
    public String movimentiList(@RequestParam(value="meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
                                @RequestParam(value="annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
                                @RequestParam(value="meseFinePeriodoContabile") String meseFinePeriodoContabile,
                                @RequestParam(value="annoFinePeriodoContabile") String annoFinePeriodoContabile,
                                @RequestParam(value="evento") Long evento,
                                @RequestParam(value="fattura") String fattura,
                                @RequestParam(value="reversale") Long reversale,
                                @RequestParam(value="voceIncasso") String voceIncasso,
                                @RequestParam(value="dataInizioEvento") String dataInizioEvento,
                                @RequestParam(value="dataFineEvento") String dataFineEvento,
                                @RequestParam(value="seprag") String seprag,
                                @RequestParam(value="tipoDocumento") String tipoDocumento,
                                @RequestParam(value="locale") String locale,
                                @RequestParam(value="organizzatore") String organizzatore,
                                @RequestParam(value="tipoSupporto") String tipoSupporto,
                                @RequestParam(value="tipoProgramma") String tipoProgramma,
                                @RequestParam(value="gruppoPrincipale") String gruppoPrincipale,
                                @RequestParam(value="titoloOpera") String titoloOpera,
                                @RequestParam(value="numeroPM") String numeroPM,
                                @RequestParam(value="permesso") String permesso,

                                @RequestParam(value="page") Integer page,
                                Model model) {

        traceService.trace("Cruscotti", this.getClass().getName(), "/movimentiList", "Lista movimenti PM", Constants.CLICK);

        // Gestione breadCrumbs in pagina: inizio
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = attr.getRequest();
        HttpSession session = attr.getRequest().getSession();
        BreadCrumbList breadCrumbList = (BreadCrumbList)session.getAttribute("breadCrumbList");
        String url = request.getContextPath() + request.getServletPath();
        if (request.getQueryString() != null){
            url = url + "?" + request.getQueryString();
        }
        breadCrumbList.addBreadCrumb("Lista Movimenti", url);
        session.setAttribute("breadCrumbList", breadCrumbList);
        // Gestione breadCrumbs in pagina: fine

        if (page==null)
            page=1;

        if (evento!=null && evento.equals(""))
            evento=null;
        if (fattura!=null && fattura.equals(""))
            fattura=null;
        if (reversale!=null && reversale.equals(""))
            reversale=null;
        if (voceIncasso!=null && voceIncasso.equals(""))
            voceIncasso=null;
        if (dataInizioEvento!=null && dataInizioEvento.equals(""))
            dataInizioEvento=null;
        if (dataFineEvento!=null && dataFineEvento.equals(""))
            dataFineEvento=null;
        if (seprag!=null && seprag.equals(""))
            seprag=null;
        if (tipoDocumento!=null && tipoDocumento.equals(""))
            tipoDocumento=null;
        if (locale!=null && locale.equals(""))
            locale=null;
        if (organizzatore!=null && organizzatore.equals(""))
            organizzatore=null;
        if (tipoSupporto!=null && tipoSupporto.equals(""))
            tipoSupporto=null;
        if (tipoProgramma!=null && tipoProgramma.equals(""))
            tipoProgramma=null;
        if (gruppoPrincipale!=null && gruppoPrincipale.equals(""))
            gruppoPrincipale=null;
        if (titoloOpera!=null && titoloOpera.equals(""))
            titoloOpera=null;
        if (numeroPM!=null && numeroPM.equals(""))
            numeroPM=null;
        if (permesso!=null && permesso.equals(""))
            permesso=null;

        if (meseInizioPeriodoContabile.length()==1)
            meseInizioPeriodoContabile="0"+meseInizioPeriodoContabile;
        if (meseFinePeriodoContabile.length()==1)
            meseFinePeriodoContabile="0"+meseFinePeriodoContabile;

        String inizioPeriodoContabile=annoInizioPeriodoContabile+meseInizioPeriodoContabile;
        String finePeriodoContabile=annoFinePeriodoContabile+meseFinePeriodoContabile;

        Date dataEventoInizio = null;
        if (dataInizioEvento!=null)
            dataEventoInizio=Utility.stringDate(dataInizioEvento, Utility.OracleFormat_D);
        Date dataEventoFine = null;
        if (dataFineEvento!=null)
            dataEventoFine=Utility.stringDate(dataFineEvento, Utility.OracleFormat_D);

        ReportPage resultPage = programmaMusicaleService.getListaMovimentazioni(Long.parseLong(inizioPeriodoContabile), Long.parseLong(finePeriodoContabile), evento, fattura, reversale, voceIncasso,
                dataEventoInizio, dataEventoFine, seprag, tipoDocumento, locale, organizzatore,
                tipoSupporto, tipoProgramma, gruppoPrincipale, titoloOpera, numeroPM, permesso, page);

        model.addAttribute("resultPage", resultPage);

        List<String> tipoProgrammi = programmaMusicaleService.getTipoProgrammi();

        model.addAttribute("meseInizioPeriodoContabile", meseInizioPeriodoContabile);
        model.addAttribute("annoInizioPeriodoContabile", annoInizioPeriodoContabile);
        model.addAttribute("meseFinePeriodoContabile", meseFinePeriodoContabile);
        model.addAttribute("annoFinePeriodoContabile", annoFinePeriodoContabile);

        model.addAttribute("evento", evento);
        model.addAttribute("fattura", fattura);
        model.addAttribute("reversale", reversale);
        model.addAttribute("voceIncasso", voceIncasso);
        model.addAttribute("dataInizioEvento", dataInizioEvento);
        model.addAttribute("dataFineEvento", dataFineEvento);
        model.addAttribute("seprag", seprag);
        model.addAttribute("tipoDocumento", tipoDocumento);
        model.addAttribute("locale", locale);
        model.addAttribute("organizzatore", organizzatore);

        model.addAttribute("tipoSupporto", tipoSupporto);
        model.addAttribute("tipoProgramma", tipoProgramma);
        model.addAttribute("gruppoPrincipale", gruppoPrincipale);
        model.addAttribute("titoloOpera", titoloOpera);
        model.addAttribute("numeroPM", numeroPM);
        model.addAttribute("permesso", permesso);

        model.addAttribute("tipoProgrammi", tipoProgrammi);

        return "cruscottiMovimenti";
    }


    @RequestMapping(value="/movimentiDetail",  method= RequestMethod.GET)
    public String movimentiDetail(@RequestParam(value="movimento") Long movimento,
                                  Model model) {

        traceService.trace("Cruscotti", this.getClass().getName(), "/movimentiDetail", "Visualizzazione dettaglio movimento", Constants.CLICK);

        // Gestione breadCrumbs in pagina: inizio
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = attr.getRequest();
        HttpSession session = attr.getRequest().getSession();
        BreadCrumbList breadCrumbList = (BreadCrumbList)session.getAttribute("breadCrumbList");
        String url = request.getContextPath() + request.getServletPath();
        if (request.getQueryString() != null){
            url = url + "?" + request.getQueryString();
        }
        breadCrumbList.addBreadCrumb("Dettaglio Movimento", url);
        session.setAttribute("breadCrumbList", breadCrumbList);
        // Gestione breadCrumbs in pagina: fine

        List<EventiPagati> eventoPagato = programmaMusicaleService.getDettagliMovimento(movimento);

        model.addAttribute("evento", eventoPagato);
        model.addAttribute("movimento", movimento);

        return "cruscottiMovimentiDettaglio";
    }


    @RequestMapping(value="/pmDetail",  method= RequestMethod.GET)
    public String pmDetail(@RequestParam(value="idProgrammaMusicale")Long idProgrammaMusicale,
                           @RequestParam(value="idMovimento", required = false) Long idMovimento,
                           Model model) {

        traceService.trace("Cruscotti", this.getClass().getName(), "/pmDetail", "Visualizzazione dettaglio PM", Constants.CLICK);

        // Gestione breadCrumbs in pagina: inizio
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = attr.getRequest();
        HttpSession session = attr.getRequest().getSession();

        BreadCrumbList breadCrumbList = (BreadCrumbList)session.getAttribute("breadCrumbList");
        String url = request.getContextPath() + request.getServletPath();
        if (request.getQueryString() != null){
            url = url + "?" + request.getQueryString();
        }
        breadCrumbList.addBreadCrumb("Dettaglio Programma Musicale", url);
        session.setAttribute("breadCrumbList", breadCrumbList);
        // Gestione breadCrumbs in pagina: fine

        ProgrammaMusicale programmaMusicale = null;

        if (idMovimento != null)
          programmaMusicale = programmaMusicaleService.getDettaglioPM(idProgrammaMusicale, idMovimento);
        else
          programmaMusicale = programmaMusicaleService.getDettaglioPM(idProgrammaMusicale);

        model.addAttribute("programmaMusicale", programmaMusicale);

        return "cruscottiProgrammaMusicaleDettaglio";
    }



    @RequestMapping(value="/aggregazioniIncassi",  method= RequestMethod.GET)
    public String aggregazioniIncassi(Model model) {

        traceService.trace("Cruscotti", this.getClass().getName(), "/aggregazioniIncassi", "Accesso report aggregazioni Incassi", Constants.CLICK);

        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = attr.getRequest();
        HttpSession session = attr.getRequest().getSession();

        List<Circoscrizione> circoscrizioni = configurationService.getCircoscrizioni();

        model.addAttribute("circoscrizioni", circoscrizioni);

        session.setAttribute("resultPage", null);
        model.addAttribute("resultPage");

        ReportCache.clearReports();

        return "cruscottoAggregatiIncassi";
    }

    @RequestMapping(value="/aggregazioniIncassiList",  method= RequestMethod.GET)
    public String aggregazioniIncassiList(@RequestParam(value="meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
                                @RequestParam(value="annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
                                @RequestParam(value="meseFinePeriodoContabile") String meseFinePeriodoContabile,
                                @RequestParam(value="annoFinePeriodoContabile") String annoFinePeriodoContabile,
                                @RequestParam(value="idPuntoTerritoriale") String idPuntoTerritoriale,
                                @RequestParam(value="page") Integer page,
                                Model model) {

        traceService.trace("Cruscotti", this.getClass().getName(), "/aggregazioniIncassiList", "Aggregazione incassi", Constants.CLICK);

        String seprag=null;

        if (idPuntoTerritoriale!=null && "".equals(idPuntoTerritoriale))
            idPuntoTerritoriale=null;

        if (idPuntoTerritoriale!=null && idPuntoTerritoriale.length()<7){
            seprag="0"+seprag;
        } else {
            seprag = idPuntoTerritoriale;
        }

        if (meseInizioPeriodoContabile.length()==1)
            meseInizioPeriodoContabile="0"+meseInizioPeriodoContabile;
        if (meseFinePeriodoContabile.length()==1)
            meseFinePeriodoContabile="0"+meseFinePeriodoContabile;

        String inizioPeriodoContabile=annoInizioPeriodoContabile+meseInizioPeriodoContabile;
        String finePeriodoContabile=annoFinePeriodoContabile+meseFinePeriodoContabile;

        ReportPage resultPage = programmaMusicaleService.getListaAggregatiEventi(Long.parseLong(inizioPeriodoContabile), Long.parseLong(finePeriodoContabile), seprag);

        List<Circoscrizione> circoscrizioni = configurationService.getCircoscrizioni();

        model.addAttribute("resultPage", resultPage);

        model.addAttribute("meseInizioPeriodoContabile", meseInizioPeriodoContabile);
        model.addAttribute("annoInizioPeriodoContabile", annoInizioPeriodoContabile);
        model.addAttribute("meseFinePeriodoContabile", meseFinePeriodoContabile);
        model.addAttribute("annoFinePeriodoContabile", annoFinePeriodoContabile);
        model.addAttribute("elabSelected", idPuntoTerritoriale);
        model.addAttribute("circoscrizioni", circoscrizioni);

        return "cruscottoAggregatiIncassi";
    }

    @RequestMapping(value="/aggregazioniMovimenti",  method= RequestMethod.GET)
    public String aggregazioniMovimenti(Model model) {

        traceService.trace("Cruscotti", this.getClass().getName(), "/aggregazioniMovimenti", "Accesso report aggregazioni Movimenti", Constants.CLICK);

        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = attr.getRequest();
        HttpSession session = attr.getRequest().getSession();

        List<Circoscrizione> circoscrizioni = configurationService.getCircoscrizioni();

        model.addAttribute("circoscrizioni", circoscrizioni);

        session.setAttribute("resultPage", null);
        model.addAttribute("resultPage");

        ReportCache.clearReports();

        return "cruscottoAggregatiMovimenti";
    }

    @RequestMapping(value="/aggregazioniMovimentiList",  method= RequestMethod.GET)
    public String aggregazioniMovimentiList(@RequestParam(value="meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
                                          @RequestParam(value="annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
                                          @RequestParam(value="meseFinePeriodoContabile") String meseFinePeriodoContabile,
                                          @RequestParam(value="annoFinePeriodoContabile") String annoFinePeriodoContabile,
                                          @RequestParam(value="idPuntoTerritoriale") String idPuntoTerritoriale,
                                          @RequestParam(value="correntiArretrati") String correntiArretrati,
                                          @RequestParam(value="page") Integer page,
                                          Model model) {

        traceService.trace("Cruscotti", this.getClass().getName(), "/aggregazioniMovimentiList", "Aggregazione incassi", Constants.CLICK);

        String seprag=null;

        if (idPuntoTerritoriale!=null && "".equals(idPuntoTerritoriale))
            idPuntoTerritoriale=null;

        if (idPuntoTerritoriale!=null && idPuntoTerritoriale.length()<7){
            seprag="0"+seprag;
        } else {
            seprag = idPuntoTerritoriale;
        }


        if (page==null)
            page=1;

        if (meseInizioPeriodoContabile.length()==1)
            meseInizioPeriodoContabile="0"+meseInizioPeriodoContabile;
        if (meseFinePeriodoContabile.length()==1)
            meseFinePeriodoContabile="0"+meseFinePeriodoContabile;

        String inizioPeriodoContabile=annoInizioPeriodoContabile+meseInizioPeriodoContabile;
        String finePeriodoContabile=annoFinePeriodoContabile+meseFinePeriodoContabile;


        List<ReportPage> reports = programmaMusicaleService.getListaAggregatiMovimenti(Long.parseLong(inizioPeriodoContabile), Long.parseLong(finePeriodoContabile), seprag, page, correntiArretrati);

        List<Circoscrizione> circoscrizioni = configurationService.getCircoscrizioni();

        model.addAttribute("listaImportiCorrenti", reports.get(0));
        model.addAttribute("listaCedolePMCorrenti", reports.get(1));

        model.addAttribute("listaImportiArretrati", reports.get(2));
        model.addAttribute("listaCedolePMArretrati", reports.get(3));

        model.addAttribute("meseInizioPeriodoContabile", meseInizioPeriodoContabile);
        model.addAttribute("annoInizioPeriodoContabile", annoInizioPeriodoContabile);
        model.addAttribute("meseFinePeriodoContabile", meseFinePeriodoContabile);
        model.addAttribute("annoFinePeriodoContabile", annoFinePeriodoContabile);
        model.addAttribute("elabSelected", idPuntoTerritoriale);
        model.addAttribute("circoscrizioni", circoscrizioni);
        model.addAttribute("correntiArretrati", correntiArretrati);

        return "cruscottoAggregatiMovimenti";
    }


    @RequestMapping(value="/aggregazioniSiada",  method= RequestMethod.GET)
    public String aggregazioniSiada(Model model) {

        traceService.trace("Cruscotti", this.getClass().getName(), "/aggregazioniSiada", "Accesso visualizzazione aggregazioniSiada", Constants.CLICK);

        // Gestione breadCrumbs in pagina: inizio
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = attr.getRequest();
        HttpSession session = attr.getRequest().getSession();
        BreadCrumbList breadCrumbList = new BreadCrumbList();
        String url = request.getContextPath() + request.getServletPath();
        if (request.getQueryString() != null){
            url = url + "?" + request.getQueryString();
        }
        breadCrumbList.addBreadCrumb("Tracciamento PM", url);
        session.setAttribute("breadCrumbList", breadCrumbList);
        // Gestione breadCrumbs in pagina: fine

        String currentYear = Utility.dateString(new Date(), "yyyy");
        String currentMonth = Utility.dateString(new Date(), "MM");
        String finePeriodoRicalcolo = configurationService.getParameter("FINE_PERIODO_RIPARTIZIONE");
        List<String> tipoProgrammi = programmaMusicaleService.getTipoProgrammi();

        Long annoFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(0,4));
        Long meseFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(4,6));

        if (meseFinePeriodoRicalcolo==12) {
            model.addAttribute("meseInizioPeriodoContabile", "01");
            model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo+1);
        }else {
            if (meseFinePeriodoRicalcolo<9)
                model.addAttribute("meseInizioPeriodoContabile", "0"+meseFinePeriodoRicalcolo);
            else
                model.addAttribute("meseInizioPeriodoContabile", meseFinePeriodoRicalcolo);

            model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo);
        }

        model.addAttribute("meseFinePeriodoContabile", currentMonth);
        model.addAttribute("annoFinePeriodoContabile", currentYear);

        model.addAttribute("tipoProgrammi", tipoProgrammi);
        session.setAttribute("resultPage", null);
        model.addAttribute("resultPage");

        ReportCache.clearReports();

        return "cruscottoAggregatiSiada";
    }


    @RequestMapping(value="/aggregatiList",  method= RequestMethod.GET)
    public String aggregatiList(@RequestParam(value="meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
                                @RequestParam(value="annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
                                @RequestParam(value="meseFinePeriodoContabile") String meseFinePeriodoContabile,
                                @RequestParam(value="annoFinePeriodoContabile") String annoFinePeriodoContabile,
                                @RequestParam(value="voceIncasso") String voceIncasso,
                                @RequestParam(value="tipoSupporto") String tipoSupporto,
                                @RequestParam(value="tipoProgramma") String tipoProgramma,
                                @RequestParam(value="numeroPM") String numeroPM,
                                @RequestParam(value="page") Integer page,
                                Model model) {

        traceService.trace("Cruscotti", this.getClass().getName(), "/aggregatiList", "Lista movimenti PM", Constants.CLICK);

        Long numPM = null;

        if (page==null)
            page=1;

        if (voceIncasso!=null && voceIncasso.equals(""))
            voceIncasso=null;
        if (tipoSupporto!=null && tipoSupporto.equals(""))
            tipoSupporto=null;
        if (tipoProgramma!=null && tipoProgramma.equals(""))
            tipoProgramma=null;
        if (numeroPM!=null && numeroPM.equals(""))
            numeroPM=null;
        if (meseInizioPeriodoContabile.length()==1)
            meseInizioPeriodoContabile="0"+meseInizioPeriodoContabile;
        if (meseFinePeriodoContabile.length()==1)
            meseFinePeriodoContabile="0"+meseFinePeriodoContabile;

        String inizioPeriodoContabile=annoInizioPeriodoContabile+meseInizioPeriodoContabile;
        String finePeriodoContabile=annoFinePeriodoContabile+meseFinePeriodoContabile;

        if (numeroPM!=null)
            numPM = Long.parseLong(numeroPM);

        ReportPage resultPage = programmaMusicaleService.getListaAggregatiSiada(Long.parseLong(inizioPeriodoContabile), Long.parseLong(finePeriodoContabile), voceIncasso, tipoSupporto, tipoProgramma, numPM, page);

        model.addAttribute("resultPage", resultPage);

        List<String> tipoProgrammi = programmaMusicaleService.getTipoProgrammi();

        model.addAttribute("meseInizioPeriodoContabile", meseInizioPeriodoContabile);
        model.addAttribute("annoInizioPeriodoContabile", annoInizioPeriodoContabile);
        model.addAttribute("meseFinePeriodoContabile", meseFinePeriodoContabile);
        model.addAttribute("annoFinePeriodoContabile", annoFinePeriodoContabile);
        model.addAttribute("voceIncasso", voceIncasso);
        model.addAttribute("tipoSupporto", tipoSupporto);
        model.addAttribute("tipoProgramma", tipoProgramma);
        model.addAttribute("numeroPM", numeroPM);
        model.addAttribute("tipoProgrammi", tipoProgrammi);

        return "cruscottoAggregatiSiada";
    }

    @RequestMapping(value="/aggregazioniSiadaSMC",  method= RequestMethod.GET)
    public String aggregazioniSiadaSMC(Model model) {

        traceService.trace("Cruscotti", this.getClass().getName(), "/aggregazioniSiadaSMC", "Accesso visualizzazione aggregazioniSiadaSMC", Constants.CLICK);

        // Gestione breadCrumbs in pagina: inizio
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = attr.getRequest();
        HttpSession session = attr.getRequest().getSession();
        BreadCrumbList breadCrumbList = new BreadCrumbList();
        String url = request.getContextPath() + request.getServletPath();
        if (request.getQueryString() != null){
            url = url + "?" + request.getQueryString();
        }
        breadCrumbList.addBreadCrumb("Tracciamento PM", url);
        session.setAttribute("breadCrumbList", breadCrumbList);
        // Gestione breadCrumbs in pagina: fine

        String currentYear = Utility.dateString(new Date(), "yyyy");
        String currentMonth = Utility.dateString(new Date(), "MM");
        String finePeriodoRicalcolo = configurationService.getParameter("FINE_PERIODO_RIPARTIZIONE");
        List<String> tipoProgrammi = programmaMusicaleService.getTipoProgrammi();

        Long annoFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(0,4));
        Long meseFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(4,6));

        if (meseFinePeriodoRicalcolo==12) {
            model.addAttribute("meseInizioPeriodoContabile", "01");
            model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo+1);
        }else {
            if (meseFinePeriodoRicalcolo<9)
                model.addAttribute("meseInizioPeriodoContabile", "0"+meseFinePeriodoRicalcolo);
            else
                model.addAttribute("meseInizioPeriodoContabile", meseFinePeriodoRicalcolo);

            model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo);
        }

        model.addAttribute("meseFinePeriodoContabile", currentMonth);
        model.addAttribute("annoFinePeriodoContabile", currentYear);

        model.addAttribute("tipoProgrammi", tipoProgrammi);
        session.setAttribute("resultPage", null);
        model.addAttribute("resultPage");

        ReportCache.clearReports();

        return "cruscottoAggregatiSiadaSMC";
    }

    @RequestMapping(value="/aggregatiListSMC",  method= RequestMethod.GET)
    public String aggregatiListSMC(@RequestParam(value="meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
                                @RequestParam(value="annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
                                @RequestParam(value="meseFinePeriodoContabile") String meseFinePeriodoContabile,
                                @RequestParam(value="annoFinePeriodoContabile") String annoFinePeriodoContabile,
                                @RequestParam(value="voceIncasso") String voceIncasso,
                                @RequestParam(value="tipoSupporto") String tipoSupporto,
                                @RequestParam(value="tipoProgramma") String tipoProgramma,
                                @RequestParam(value="numeroPM") String numeroPM,
                                @RequestParam(value="page") Integer page,
                                Model model) {

        traceService.trace("Cruscotti", this.getClass().getName(), "/aggregatiList", "Lista movimenti PM", Constants.CLICK);

        Long numPM = null;

        if (page==null)
            page=1;

        if (voceIncasso!=null && voceIncasso.equals(""))
            voceIncasso=null;
        if (tipoSupporto!=null && tipoSupporto.equals(""))
            tipoSupporto=null;
        if (tipoProgramma!=null && tipoProgramma.equals(""))
            tipoProgramma=null;
        if (numeroPM!=null && numeroPM.equals(""))
            numeroPM=null;
        if (meseInizioPeriodoContabile.length()==1)
            meseInizioPeriodoContabile="0"+meseInizioPeriodoContabile;
        if (meseFinePeriodoContabile.length()==1)
            meseFinePeriodoContabile="0"+meseFinePeriodoContabile;

        String inizioPeriodoContabile=annoInizioPeriodoContabile+meseInizioPeriodoContabile;
        String finePeriodoContabile=annoFinePeriodoContabile+meseFinePeriodoContabile;

        if (numeroPM!=null)
            numPM = Long.parseLong(numeroPM);

        ReportPage resultPage = programmaMusicaleService.getListaAggregatiSiadaSMC(Long.parseLong(inizioPeriodoContabile), Long.parseLong(finePeriodoContabile), voceIncasso, tipoSupporto, tipoProgramma, numPM, page);

        model.addAttribute("resultPage", resultPage);

        List<String> tipoProgrammi = programmaMusicaleService.getTipoProgrammi();

        model.addAttribute("meseInizioPeriodoContabile", meseInizioPeriodoContabile);
        model.addAttribute("annoInizioPeriodoContabile", annoInizioPeriodoContabile);
        model.addAttribute("meseFinePeriodoContabile", meseFinePeriodoContabile);
        model.addAttribute("annoFinePeriodoContabile", annoFinePeriodoContabile);
        model.addAttribute("voceIncasso", voceIncasso);
        model.addAttribute("tipoSupporto", tipoSupporto);
        model.addAttribute("tipoProgramma", tipoProgramma);
        model.addAttribute("numeroPM", numeroPM);
        model.addAttribute("tipoProgrammi", tipoProgrammi);

        return "cruscottoAggregatiSiadaSMC";
    }


    @RequestMapping(value="/exportReport",  method= RequestMethod.GET)
    public String exportReport(@RequestParam(value="idReport", required = true)String idReport,
                               @RequestParam(value="detailType", required = false)String detailType,
                               @RequestParam(value="idDetail", required = false)String idDetail,
                               Model model) throws IOException {

        traceService.trace("exportReport", this.getClass().getName(), "/exportReport", "Esportazione "+idReport+" in excel", Constants.CLICK);

        // Gestione breadCrumbs in pagina: inizio
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletResponse response = attr.getResponse();

        response.setContentType("application/ms-excel");
        //response.setContentLength(outArray.length);
        response.setHeader("Expires:", "0"); // eliminates browser caching
        response.setHeader("Content-Disposition", "attachment; filename="+idReport+".xls");

        if (detailType==null && idDetail==null) {

            // Esporta liste di oggetti
            createExcel.exportToExcel(idReport, response.getOutputStream());

        }else
        if (detailType!=null && idDetail!=null) {

            createExcel.exportToExcelDetail(idReport, detailType, new Long(idDetail), response.getOutputStream());

        }

        return null;
    }



    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest req, Exception exception) {

        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", exception);
        mav.addObject("url", req.getRequestURL());
        mav.setViewName("error");
        exception.printStackTrace();

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exception.printStackTrace(pw);

        traceService.trace("Cruscotti", this.getClass().getName(), req.getRequestURL().toString(), sw.toString(), Constants.ERROR);

        return mav;
    }




        private String toJson(Object obj){
        String ris = null;
        try {
            return new JsonBuilder(obj).toString();
        } catch (Exception e) {
            e.printStackTrace();
            ris = Utility.jsonNOK(e.getMessage());
        }
        return ris;
    }

}
