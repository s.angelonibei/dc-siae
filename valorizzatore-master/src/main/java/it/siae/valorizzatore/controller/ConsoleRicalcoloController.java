package it.siae.valorizzatore.controller;

import groovy.json.JsonBuilder;
import it.siae.valorizzatore.model.CampionamentoConfig;
import it.siae.valorizzatore.model.EsecuzioneRicalcolo;
import it.siae.valorizzatore.model.InformazioniPM;
import it.siae.valorizzatore.model.MovimentoContabile;
import it.siae.valorizzatore.model.security.ReportPage;
import it.siae.valorizzatore.model.security.User;
import it.siae.valorizzatore.model.view.BreadCrumbList;
import it.siae.valorizzatore.service.*;
import it.siae.valorizzatore.utility.Constants;
import it.siae.valorizzatore.utility.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by idilello on 6/11/16.
 */
@Controller
@RequestMapping("/secure")
public class ConsoleRicalcoloController {

    @Autowired
    private IConsoleRicalcoloService consoleRicalcoloService;

    @Autowired
    private IConfigurationService configurationService;

    @Autowired
    private ICampionamentoService campionamentoService;

    @Autowired
    private ITraceService traceService;

    @Autowired
    private IProgrammaMusicaleService programmaMusicaleService;

    @RequestMapping(value="/home")
    public String homePage() {

        traceService.trace("Navigazione", this.getClass().getName(), "/home", "Accesso home", Constants.CLICK);

        return "home";
    }

    @RequestMapping(value="/userData")
    public String userData(Model model) {

        traceService.trace("Navigazione", this.getClass().getName(), "/userData", "Accesso dati utente", Constants.CLICK);

        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest currentRequest =  attr.getRequest();

        User user = (User)currentRequest.getSession().getAttribute("user");

        model.addAttribute("user", user);

        return "userData";
    }


    @RequestMapping(value="/ricalcoloConfigurazione",  method= RequestMethod.GET)
    public String ricalcoloConfigurazione(Model model) {

        traceService.trace("Ricalcolo", this.getClass().getName(), "/ricalcoloConfigurazione", "Accesso configurazione", Constants.CLICK);

        Long[] inizioPeriodoRicalcolo = consoleRicalcoloService.getPeriodoAAAAMM(Constants.INIZIO_PERIODO_RICALCOLO);
        Long[] finePeriodoRicalcolo = consoleRicalcoloService.getPeriodoAAAAMM(Constants.FINE_PERIODO_RICALCOLO);
        Long[] finePeriodoRipartizione = consoleRicalcoloService.getPeriodoAAAAMM(Constants.FINE_PERIODO_RIPARTIZIONE);
        String tipologiaPM = configurationService.getParameter(Constants.TIPOLOGIA_PM);
        String campionaDigitali = configurationService.getParameter(Constants.CAMPIONAMENTO_DIGITALI);

        Long[] inizioPeriodoRicalcolo2243 = consoleRicalcoloService.getPeriodoAAAAMM(Constants.INIZIO_PERIODO_RICALCOLO_2243);
        Long[] finePeriodoRicalcolo2243 = consoleRicalcoloService.getPeriodoAAAAMM(Constants.FINE_PERIODO_RICALCOLO_2243);
        Long[] finePeriodoRipartizione2243 = consoleRicalcoloService.getPeriodoAAAAMM(Constants.FINE_PERIODO_RIPARTIZIONE_2243);
        String tipologiaPM2243 = configurationService.getParameter(Constants.TIPOLOGIA_PM_2243);

        model.addAttribute("inizioPeriodoRicalcoloAnno", inizioPeriodoRicalcolo[0]);
        model.addAttribute("inizioPeriodoRicalcoloMese", inizioPeriodoRicalcolo[1]);
        model.addAttribute("finePeriodoRicalcoloAnno", finePeriodoRicalcolo[0]);
        model.addAttribute("finePeriodoRicalcoloMese", finePeriodoRicalcolo[1]);
        model.addAttribute("finePeriodoRipartizioneAnno", finePeriodoRipartizione[0]);
        model.addAttribute("finePeriodoRipartizioneMese", finePeriodoRipartizione[1]);
        model.addAttribute("tipologiaPM", tipologiaPM);
        model.addAttribute("campionaDigitali", campionaDigitali);

        model.addAttribute("inizioPeriodoRicalcoloAnno2243", inizioPeriodoRicalcolo2243[0]);
        model.addAttribute("inizioPeriodoRicalcoloMese2243", inizioPeriodoRicalcolo2243[1]);
        model.addAttribute("finePeriodoRicalcoloAnno2243", finePeriodoRicalcolo2243[0]);
        model.addAttribute("finePeriodoRicalcoloMese2243", finePeriodoRicalcolo2243[1]);
        model.addAttribute("finePeriodoRipartizioneAnno2243", finePeriodoRipartizione2243[0]);
        model.addAttribute("finePeriodoRipartizioneMese2243", finePeriodoRipartizione2243[1]);
        model.addAttribute("tipologiaPM2243", tipologiaPM2243);


        return "ricalcoloConfigurazione";
    }

    @RequestMapping(value="/ricalcoloConfigurazioneSave",  method= RequestMethod.POST)
    public String ricalcoloConfigurazioneSave(@RequestParam(value="annoFinePeriodoRipartizione", required = false) String annoFinePeriodoRipartizione,
                                              @RequestParam(value="meseFinePeriodoRipartizione", required = false) String meseFinePeriodoRipartizione,
                                              @RequestParam(value="annoInizioPeriodoContabile", required = false) String annoInizioPeriodoContabile,
                                              @RequestParam(value="meseInizioPeriodoContabile", required = false) String meseInizioPeriodoContabile,
                                              @RequestParam(value="annoFinePeriodoContabile", required = false) String annoFinePeriodoContabile,
                                              @RequestParam(value="meseFinePeriodoContabile", required = false) String meseFinePeriodoContabile,
                                              @RequestParam(value="tipologiaPM", required = false) String tipologiaPM,
                                              @RequestParam(value="campionaDigitali", required = false) String campionaDigitali,

                                              @RequestParam(value="annoFinePeriodoRipartizione2243", required = false) String annoFinePeriodoRipartizione2243,
                                              @RequestParam(value="meseFinePeriodoRipartizione2243", required = false) String meseFinePeriodoRipartizione2243,
                                              @RequestParam(value="annoInizioPeriodoContabile2243", required = false) String annoInizioPeriodoContabile2243,
                                              @RequestParam(value="meseInizioPeriodoContabile2243", required = false) String meseInizioPeriodoContabile2243,
                                              @RequestParam(value="annoFinePeriodoContabile2243", required = false) String annoFinePeriodoContabile2243,
                                              @RequestParam(value="meseFinePeriodoContabile2243", required = false) String meseFinePeriodoContabile2243,
                                              @RequestParam(value="tipologiaPM2243", required = false) String tipologiaPM2243,
                                              Model model) {

        traceService.trace("Ricalcolo", this.getClass().getName(), "/ricalcoloConfigurazioneSave", "Salvataggio configurazione", Constants.CLICK);

        configurationService.updateConfigurazione(annoFinePeriodoRipartizione, meseFinePeriodoRipartizione,
                                                     annoInizioPeriodoContabile, meseInizioPeriodoContabile,
                                                     annoFinePeriodoContabile, meseFinePeriodoContabile,
                                                     tipologiaPM, campionaDigitali,

                                                     annoFinePeriodoRipartizione2243, meseFinePeriodoRipartizione2243,
                                                     annoInizioPeriodoContabile2243, meseInizioPeriodoContabile2243,
                                                     annoFinePeriodoContabile2243, meseFinePeriodoContabile2243,
                                                     tipologiaPM2243 );

        model.addAttribute("inizioPeriodoRicalcoloAnno", annoInizioPeriodoContabile);
        model.addAttribute("inizioPeriodoRicalcoloMese", meseInizioPeriodoContabile);
        model.addAttribute("finePeriodoRicalcoloAnno", annoFinePeriodoContabile);
        model.addAttribute("finePeriodoRicalcoloMese", meseFinePeriodoContabile);
        model.addAttribute("finePeriodoRipartizioneAnno", annoFinePeriodoRipartizione);
        model.addAttribute("finePeriodoRipartizioneMese", meseFinePeriodoRipartizione);
        model.addAttribute("tipologiaPM", tipologiaPM);
        model.addAttribute("campionaDigitali", campionaDigitali);

        model.addAttribute("inizioPeriodoRicalcoloAnno2243", annoInizioPeriodoContabile2243);
        model.addAttribute("inizioPeriodoRicalcoloMese2243", meseInizioPeriodoContabile2243);
        model.addAttribute("finePeriodoRicalcoloAnno2243", annoFinePeriodoContabile2243);
        model.addAttribute("finePeriodoRicalcoloMese2243", meseFinePeriodoContabile2243);
        model.addAttribute("finePeriodoRipartizioneAnno2243", annoFinePeriodoRipartizione2243);
        model.addAttribute("finePeriodoRipartizioneMese2243", meseFinePeriodoRipartizione2243);
        model.addAttribute("tipologiaPM2243", tipologiaPM2243);

        return "ricalcoloConfigurazione";

    }

    @RequestMapping(value="/ricalcoloEsecuzione",  method= RequestMethod.GET)
    public String ricalcoloEsecuzione(Model model) {

        traceService.trace("Ricalcolo", this.getClass().getName(), "/ricalcoloEsecuzione", "Accesso esecuzione ricalcolo", Constants.CLICK);

        return "ricalcoloEsecuzione";
    }




    @RequestMapping(value = "/sendCommand", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> sendCommand(@RequestParam(value="info") String info,
                                              @RequestParam(value="periodoContabile",required = false) String periodoContabile,
                                              Model model) {

        String response="";

        try{

            // Acquisisce lo stato del processo di backend (ACTIVE/INACTIVE)
            if (info!=null && info.equalsIgnoreCase(Constants.STATUS)){

                String status = consoleRicalcoloService.checkStatus();

                response = toJson(status);
            } else
            if (info!=null && info.equalsIgnoreCase(Constants.ACTIVATE)){

                traceService.trace("Ricalcolo", this.getClass().getName(), "/sendCommand", "Inviato comando: "+Constants.ACTIVATE, Constants.CLICK);

                // Esegue lo script sheel per far partire il processo remoto
                String status = consoleRicalcoloService.startProcess();

                response = toJson(status);

            } else
            if (info!=null && info.equalsIgnoreCase(Constants.DEACTIVATE)){

                traceService.trace("Ricalcolo", this.getClass().getName(), "/sendCommand", "Inviato comando: "+Constants.DEACTIVATE, Constants.CLICK);

                String status = consoleRicalcoloService.deactivate();

                response = toJson(status);
            }  else
            if (info!=null && info.equalsIgnoreCase(Constants.START_RICALCOLO)){

                traceService.trace("Ricalcolo", this.getClass().getName(), "/sendCommand", "Inviato comando: "+Constants.START_RICALCOLO, Constants.CLICK);

                String status = consoleRicalcoloService.startRicalcolo();

                response = toJson(status);
            } else
            if (info!=null && info.equalsIgnoreCase(Constants.START_CAMPIONAMENTO)){

                traceService.trace("Ricalcolo", this.getClass().getName(), "/sendCommand", "Inviato comando: "+Constants.START_CAMPIONAMENTO, Constants.CLICK);

                String status;
                if (periodoContabile!=null && !periodoContabile.equalsIgnoreCase("")) {

                    CampionamentoConfig parametriConfigurazione = campionamentoService.getCampionamentoConfigById(Long.parseLong(periodoContabile));

                    configurationService.insertParameter("INIZIO_PERIODO_CAMPIONAMENTO", parametriConfigurazione.getContabilita().toString());
                    configurationService.insertParameter("FINE_PERIODO_CAMPIONAMENTO", parametriConfigurazione.getContabilita().toString());

                    status = consoleRicalcoloService.startCampionamento();

                }else{
                    status = "KO";
                }
                response = toJson(status);
            }else

            // Comandi per Valorizzatore 2243
            if (info!=null && info.equalsIgnoreCase(Constants.STATUS_2243)){

                String status = consoleRicalcoloService.checkStatus2243(); // 2243

                response = toJson(status);
            }else
            if (info!=null && info.equalsIgnoreCase(Constants.ACTIVATE_2243)){

                traceService.trace("Ricalcolo2243", this.getClass().getName(), "/sendCommand", "Inviato comando: "+Constants.ACTIVATE_2243, Constants.CLICK);

                // Esegue lo script sheel per far partire il processo remoto
                String status = consoleRicalcoloService.startProcess2243();

                response = toJson(status);

            } else
            if (info!=null && info.equalsIgnoreCase(Constants.DEACTIVATE_2243)){

                traceService.trace("Ricalcolo2243", this.getClass().getName(), "/sendCommand", "Inviato comando: "+Constants.DEACTIVATE_2243, Constants.CLICK);

                String status = consoleRicalcoloService.deactivate2243();

                response = toJson(status);
            }  else
            if (info!=null && info.equalsIgnoreCase(Constants.START_RICALCOLO_2243)){

                traceService.trace("Ricalcolo2243", this.getClass().getName(), "/sendCommand", "Inviato comando: "+Constants.START_RICALCOLO_2243, Constants.CLICK);

                String status = consoleRicalcoloService.startRicalcolo2243();

                response = toJson(status);
            }


        }catch (Exception e){
            response=Utility.jsonNOK(e.getMessage());

            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);

            traceService.trace("Ricalcolo", this.getClass().getName(), "/sendCommand", sw.toString(), Constants.ERROR);

        }

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }


    @RequestMapping(value = "/getAvanzamentoElaborazione", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getAvanzamentoElaborazione(Model model) {

        String response="";

        try{

            InformazioniPM informazioni = new InformazioniPM();

            informazioni = consoleRicalcoloService.getAvanzamentoElaborazione();

            response = toJson(informazioni);

        }catch (Exception e){

            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);

            traceService.trace("Ricalcolo", this.getClass().getName(), "/getAvanzamentoElaborazione", sw.toString(), Constants.ERROR);
        }

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/getAvanzamentoElaborazione2243", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getAvanzamentoElaborazione2243(Model model) {

        String response="";

        try{

            InformazioniPM informazioni = new InformazioniPM();

            informazioni = consoleRicalcoloService.getAvanzamentoElaborazione2243();

            response = toJson(informazioni);


        }catch (Exception e){

            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);

            traceService.trace("Ricalcolo2243", this.getClass().getName(), "/getAvanzamentoElaborazione2243", sw.toString(), Constants.ERROR);
        }

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }


    @RequestMapping(value = "/getAvanzamentoAggregazione", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getAvanzamentoAggregazione(Model model) {

        String response="";

        try{

            InformazioniPM informazioni = new InformazioniPM();

            informazioni = consoleRicalcoloService.getAvanzamentoAggregazione();

            response = toJson(informazioni);

        }catch (Exception e){

            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);

            traceService.trace("Ricalcolo", this.getClass().getName(), "/getAvanzamentoAggregazione", sw.toString(), Constants.ERROR);
        }

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/getAvanzamentoAggregazione2243", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getAvanzamentoAggregazione2243(Model model) {

        String response="";

        try{

            InformazioniPM informazioni = new InformazioniPM();

            informazioni = consoleRicalcoloService.getAvanzamentoAggregazione2243();

            response = toJson(informazioni);

        }catch (Exception e){

            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);

            traceService.trace("Ricalcolo2243", this.getClass().getName(), "/getAvanzamentoAggregazione2243", sw.toString(), Constants.ERROR);
        }

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }


    @RequestMapping(value="/modificaContabilita",  method= RequestMethod.GET)
    public String modificaContabilita(Model model) {

        traceService.trace("Ricalcolo", this.getClass().getName(), "/modificaContabilita", "Accesso visualizzazione movimenti PM", Constants.CLICK);

        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = attr.getRequest();
        HttpSession session = attr.getRequest().getSession();
        session.setAttribute("modificaContabilitaList", null);

        String currentYear = Utility.dateString(new Date(), "yyyy");
        String currentMonth = Utility.dateString(new Date(), "MM");
        String finePeriodoRicalcolo = configurationService.getParameter("FINE_PERIODO_RIPARTIZIONE");
        List<String> tipoProgrammi = programmaMusicaleService.getTipoProgrammi();

        Long annoFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(0,4));
        Long meseFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(4,6));

        if (meseFinePeriodoRicalcolo==12) {
            model.addAttribute("meseInizioPeriodoContabile", "01");
            model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo+1);
        }else {
            if (meseFinePeriodoRicalcolo<9)
                model.addAttribute("meseInizioPeriodoContabile", "0"+meseFinePeriodoRicalcolo);
            else
                model.addAttribute("meseInizioPeriodoContabile", meseFinePeriodoRicalcolo);

            model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo);
        }

        model.addAttribute("meseFinePeriodoContabile", currentMonth);
        model.addAttribute("annoFinePeriodoContabile", currentYear);

        model.addAttribute("tipoProgrammi", tipoProgrammi);
        model.addAttribute("resultPage", null);
        session.setAttribute("resultPage", null);

        return "ricalcoloModificaContabilita";
    }

    @RequestMapping(value="/ricalcoloModificaContabilitaList",  method= RequestMethod.GET)
    public String ricalcoloModificaContabilitaList(@RequestParam(value="meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
                                @RequestParam(value="annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
                                @RequestParam(value="meseFinePeriodoContabile") String meseFinePeriodoContabile,
                                @RequestParam(value="annoFinePeriodoContabile") String annoFinePeriodoContabile,
                                @RequestParam(value="evento") Long evento,
                                @RequestParam(value="fattura") String fattura,
                                @RequestParam(value="reversale") Long reversale,
                                @RequestParam(value="voceIncasso") String voceIncasso,
                                @RequestParam(value="dataInizioEvento") String dataInizioEvento,
                                @RequestParam(value="dataFineEvento") String dataFineEvento,
                                @RequestParam(value="seprag") String seprag,
                                @RequestParam(value="tipoDocumento") String tipoDocumento,
                                @RequestParam(value="locale") String locale,
                                @RequestParam(value="organizzatore") String organizzatore,
                                @RequestParam(value="tipoSupporto") String tipoSupporto,
                                @RequestParam(value="tipoProgramma") String tipoProgramma,
                                @RequestParam(value="gruppoPrincipale") String gruppoPrincipale,
                                @RequestParam(value="titoloOpera") String titoloOpera,
                                @RequestParam(value="numeroPM") String numeroPM,
                                @RequestParam(value="permesso") String permesso,
                                @RequestParam(value="page") Integer page,
                                Model model) {

        traceService.trace("Ricalcolo", this.getClass().getName(), "/ricalcoloModificaContabilitaList", "Lista movimenti PM", Constants.CLICK);

        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = attr.getRequest();
        HttpSession session = attr.getRequest().getSession();

        if (page==null)
            page=1;

        if (evento!=null && evento.equals(""))
            evento=null;
        if (fattura!=null && fattura.equals(""))
            fattura=null;
        if (reversale!=null && reversale.equals(""))
            reversale=null;
        if (voceIncasso!=null && voceIncasso.equals(""))
            voceIncasso=null;
        if (dataInizioEvento!=null && dataInizioEvento.equals(""))
            dataInizioEvento=null;
        if (dataFineEvento!=null && dataFineEvento.equals(""))
            dataFineEvento=null;
        if (seprag!=null && seprag.equals(""))
            seprag=null;
        if (tipoDocumento!=null && tipoDocumento.equals(""))
            tipoDocumento=null;
        if (locale!=null && locale.equals(""))
            locale=null;
        if (organizzatore!=null && organizzatore.equals(""))
            organizzatore=null;
        if (tipoSupporto!=null && tipoSupporto.equals(""))
            tipoSupporto=null;
        if (tipoProgramma!=null && tipoProgramma.equals(""))
            tipoProgramma=null;
        if (gruppoPrincipale!=null && gruppoPrincipale.equals(""))
            gruppoPrincipale=null;
        if (titoloOpera!=null && titoloOpera.equals(""))
            titoloOpera=null;
        if (numeroPM!=null && numeroPM.equals(""))
            numeroPM=null;
        if (permesso!=null && permesso.equals(""))
            permesso=null;

        if (meseInizioPeriodoContabile.length()==1)
            meseInizioPeriodoContabile="0"+meseInizioPeriodoContabile;
        if (meseFinePeriodoContabile.length()==1)
            meseFinePeriodoContabile="0"+meseFinePeriodoContabile;

        String inizioPeriodoContabile=annoInizioPeriodoContabile+meseInizioPeriodoContabile;
        String finePeriodoContabile=annoFinePeriodoContabile+meseFinePeriodoContabile;

        Date dataEventoInizio = null;
        if (dataInizioEvento!=null)
            dataEventoInizio=Utility.stringDate(dataInizioEvento, Utility.OracleFormat_D);
        Date dataEventoFine = null;
        if (dataFineEvento!=null)
            dataEventoFine=Utility.stringDate(dataFineEvento, Utility.OracleFormat_D);


        // Gestione risultati reports: Inizio
        ReportPage resultPage = null;

        resultPage =  (ReportPage)session.getAttribute("resultPage");

        if (resultPage == null) {
            resultPage = programmaMusicaleService.getListaMovimentazioni(Long.parseLong(inizioPeriodoContabile), Long.parseLong(finePeriodoContabile), evento, fattura, reversale, voceIncasso,
                    dataEventoInizio, dataEventoFine, seprag, tipoDocumento, locale, organizzatore,
                    tipoSupporto, tipoProgramma, gruppoPrincipale, titoloOpera, numeroPM, permesso, -1);
        }

        resultPage.initRecordsInPage(page);

        session.setAttribute("resultPage", resultPage);
        model.addAttribute("resultPage", resultPage);
        // Gestione risultati reports: Fine


        List<String> tipoProgrammi = programmaMusicaleService.getTipoProgrammi();

        model.addAttribute("meseInizioPeriodoContabile", meseInizioPeriodoContabile);
        model.addAttribute("annoInizioPeriodoContabile", annoInizioPeriodoContabile);
        model.addAttribute("meseFinePeriodoContabile", meseFinePeriodoContabile);
        model.addAttribute("annoFinePeriodoContabile", annoFinePeriodoContabile);

        model.addAttribute("evento", evento);
        model.addAttribute("fattura", fattura);
        model.addAttribute("reversale", reversale);
        model.addAttribute("voceIncasso", voceIncasso);
        model.addAttribute("dataInizioEvento", dataInizioEvento);
        model.addAttribute("dataFineEvento", dataFineEvento);
        model.addAttribute("seprag", seprag);
        model.addAttribute("tipoDocumento", tipoDocumento);
        model.addAttribute("locale", locale);
        model.addAttribute("organizzatore", organizzatore);

        model.addAttribute("tipoSupporto", tipoSupporto);
        model.addAttribute("tipoProgramma", tipoProgramma);
        model.addAttribute("gruppoPrincipale", gruppoPrincipale);
        model.addAttribute("titoloOpera", titoloOpera);
        model.addAttribute("numeroPM", numeroPM);
        model.addAttribute("permesso", permesso);

        model.addAttribute("tipoProgrammi", tipoProgrammi);
        model.addAttribute("tipoOperazione","updateContabilita");

        return "ricalcoloModificaContabilita";
    }


    @RequestMapping(value="/ricalcoloModificaContabilitaUpdate",  method= RequestMethod.GET)
    public String ricalcoloModificaContabilitaUpdate(@RequestParam(value="meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
                                @RequestParam(value="annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
                                @RequestParam(value="meseFinePeriodoContabile") String meseFinePeriodoContabile,
                                @RequestParam(value="annoFinePeriodoContabile") String annoFinePeriodoContabile,
                                @RequestParam(value="evento") Long evento,
                                @RequestParam(value="fattura") String fattura,
                                @RequestParam(value="reversale") Long reversale,
                                @RequestParam(value="voceIncasso") String voceIncasso,
                                @RequestParam(value="dataInizioEvento") String dataInizioEvento,
                                @RequestParam(value="dataFineEvento") String dataFineEvento,
                                @RequestParam(value="seprag") String seprag,
                                @RequestParam(value="tipoDocumento") String tipoDocumento,
                                @RequestParam(value="locale") String locale,
                                @RequestParam(value="organizzatore") String organizzatore,
                                @RequestParam(value="tipoSupporto") String tipoSupporto,
                                @RequestParam(value="tipoProgramma") String tipoProgramma,
                                @RequestParam(value="gruppoPrincipale") String gruppoPrincipale,
                                @RequestParam(value="titoloOpera") String titoloOpera,
                                @RequestParam(value="numeroPM") String numeroPM,
                                @RequestParam(value="permesso") String permesso,
                                @RequestParam(value="nuovaContabilitaInserita") String nuovaContabilita,
                                @RequestParam(value="page") Integer page,
                                Model model) {

        traceService.trace("Ricalcolo", this.getClass().getName(), "/ricalcoloModificaContabilitaUpdate", "Lista movimenti PM update", Constants.CLICK);

        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = attr.getRequest();
        HttpSession session = attr.getRequest().getSession();

        if (page==null)
            page=1;

        if (evento!=null && evento.equals(""))
            evento=null;
        if (fattura!=null && fattura.equals(""))
            fattura=null;
        if (reversale!=null && reversale.equals(""))
            reversale=null;
        if (voceIncasso!=null && voceIncasso.equals(""))
            voceIncasso=null;
        if (dataInizioEvento!=null && dataInizioEvento.equals(""))
            dataInizioEvento=null;
        if (dataFineEvento!=null && dataFineEvento.equals(""))
            dataFineEvento=null;
        if (seprag!=null && seprag.equals(""))
            seprag=null;
        if (tipoDocumento!=null && tipoDocumento.equals(""))
            tipoDocumento=null;
        if (locale!=null && locale.equals(""))
            locale=null;
        if (organizzatore!=null && organizzatore.equals(""))
            organizzatore=null;
        if (tipoSupporto!=null && tipoSupporto.equals(""))
            tipoSupporto=null;
        if (tipoProgramma!=null && tipoProgramma.equals(""))
            tipoProgramma=null;
        if (gruppoPrincipale!=null && gruppoPrincipale.equals(""))
            gruppoPrincipale=null;
        if (titoloOpera!=null && titoloOpera.equals(""))
            titoloOpera=null;
        if (numeroPM!=null && numeroPM.equals(""))
            numeroPM=null;
        if (permesso!=null && permesso.equals(""))
            permesso=null;

        if (meseInizioPeriodoContabile.length()==1)
            meseInizioPeriodoContabile="0"+meseInizioPeriodoContabile;
        if (meseFinePeriodoContabile.length()==1)
            meseFinePeriodoContabile="0"+meseFinePeriodoContabile;

        String inizioPeriodoContabile=annoInizioPeriodoContabile+meseInizioPeriodoContabile;
        String finePeriodoContabile=annoFinePeriodoContabile+meseFinePeriodoContabile;

        Date dataEventoInizio = null;
        if (dataInizioEvento!=null)
            dataEventoInizio=Utility.stringDate(dataInizioEvento, Utility.OracleFormat_D);
        Date dataEventoFine = null;
        if (dataFineEvento!=null)
            dataEventoFine=Utility.stringDate(dataFineEvento, Utility.OracleFormat_D);



        // Gestione risultati reports: Inizio
        Integer esito = -1;
        ReportPage resultPage  = (ReportPage)session.getAttribute("resultPage");
        List<Object> listaMovimenti = new ArrayList<Object>();
        if (resultPage != null) {
            listaMovimenti = resultPage.getAllRecords();
            esito = programmaMusicaleService.updateContabilitaOriginale(Integer.parseInt(nuovaContabilita), listaMovimenti);
        }
        // Gestione risultati reports: Fine

        String message = "";

        if (esito==0) {
            MovimentoContabile movimentoContabile = null;
            if (nuovaContabilita != null) {
                for (int i = 0; i < listaMovimenti.size(); i++) {
                    movimentoContabile = (MovimentoContabile) listaMovimenti.get(i);
                    movimentoContabile.setContabilitaOrg(movimentoContabile.getContabilita());
                    movimentoContabile.setContabilita(new Integer(nuovaContabilita));
                }
            }

            message="Contabilita' aggiornata correttamente";

        }  else {

            message="Non e' stato possibile aggiornare la contabilita' causa problemi interazione con base dati. Riprovare piu' tardi.";

        }

        model.addAttribute("message", message);

        //resultPage.setRecords(listaMovimenti);

        model.addAttribute("resultPage", null);

        List<String> tipoProgrammi = programmaMusicaleService.getTipoProgrammi();

        model.addAttribute("meseInizioPeriodoContabile", meseInizioPeriodoContabile);
        model.addAttribute("annoInizioPeriodoContabile", annoInizioPeriodoContabile);
        model.addAttribute("meseFinePeriodoContabile", meseFinePeriodoContabile);
        model.addAttribute("annoFinePeriodoContabile", annoFinePeriodoContabile);

        model.addAttribute("evento", evento);
        model.addAttribute("fattura", fattura);
        model.addAttribute("reversale", reversale);
        model.addAttribute("voceIncasso", voceIncasso);
        model.addAttribute("dataInizioEvento", dataInizioEvento);
        model.addAttribute("dataFineEvento", dataFineEvento);
        model.addAttribute("seprag", seprag);
        model.addAttribute("tipoDocumento", tipoDocumento);
        model.addAttribute("locale", locale);
        model.addAttribute("organizzatore", organizzatore);

        model.addAttribute("tipoSupporto", tipoSupporto);
        model.addAttribute("tipoProgramma", tipoProgramma);
        model.addAttribute("gruppoPrincipale", gruppoPrincipale);
        model.addAttribute("titoloOpera", titoloOpera);
        model.addAttribute("numeroPM", numeroPM);
        model.addAttribute("permesso", permesso);

        model.addAttribute("tipoProgrammi", tipoProgrammi);
        model.addAttribute("tipoOperazione","updateContabilita");

        model.addAttribute("nuovaContabilita",nuovaContabilita);

        return "ricalcoloModificaContabilita";
    }


    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest req, Exception exception) {

        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", exception);
        mav.addObject("url", req.getRequestURL());
        mav.setViewName("error");
        exception.printStackTrace();


        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exception.printStackTrace(pw);

        traceService.trace("Ricalcolo", this.getClass().getName(), req.getRequestURL().toString(), sw.toString(), Constants.ERROR);


        return mav;
    }

    private String toJson(Object obj){
        String ris = null;
        try {
            return new JsonBuilder(obj).toString();
        } catch (Exception e) {
            e.printStackTrace();
            ris = Utility.jsonNOK(e.getMessage());
        }
        return ris;
    }


}

