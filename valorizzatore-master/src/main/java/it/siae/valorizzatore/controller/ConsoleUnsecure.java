package it.siae.valorizzatore.controller;

import groovy.json.JsonBuilder;
import it.siae.valorizzatore.model.Configurazione;
import it.siae.valorizzatore.service.IConfigurationService;
import it.siae.valorizzatore.service.IConsoleRicalcoloService;
import it.siae.valorizzatore.service.ITraceService;
import it.siae.valorizzatore.service.security.UserAuthenticationProvider;
import it.siae.valorizzatore.utility.Constants;
import it.siae.valorizzatore.utility.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

/**
 * Created by idilello on 7/19/16.
 */
@Controller
public class ConsoleUnsecure {

    @Autowired
    UserAuthenticationProvider userAuthenticationProvider;

    @Autowired
    IConsoleRicalcoloService consoleRicalcoloService;

    @Autowired
    private IConfigurationService configurationService;

    @Autowired
    private ITraceService traceService;

    @RequestMapping(value = "/guest/startEngine", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> startEngine(@RequestParam(value="key", required = true) String key,
                                              Model model) {

        traceService.trace("StartEngine", this.getClass().getName(), "/startEngine", "Remote start for engine: "+key, Constants.CLICK);

        String response="OK";

        try{

            //String result = consoleRicalcoloService.startProcess();
            String result = consoleRicalcoloService.startLocalProcessFromRemote(Integer.parseInt(key));

            response = toJson(result);

        }catch (Exception e){
            response = "KO";
        }

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/guest/startEngine2243", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> startEngine2243(@RequestParam(value="key", required = true) String key,
                                                  Model model) {

        traceService.trace("StartEngine2243", this.getClass().getName(), "/startEngine", "Remote start for engine: "+key, Constants.CLICK);

        String response="OK";

        try{

            //String result = consoleRicalcoloService.startProcess();
            String result = consoleRicalcoloService.startLocalProcessFromRemote2243(Integer.parseInt(key));

            response = toJson(result);

        }catch (Exception e){
            response = "KO";
        }

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @RequestMapping(value="/main",  method= RequestMethod.GET)
    public String autoLogin(@RequestParam(value="SessionID", required = false) String SessionID, Model model) {

        String resultPage=null;

        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request =  attr.getRequest();

        if (SessionID != null) {

            traceService.trace("Authenticate", this.getClass().getName(), "/Login", "Accesso tramite token SSO: "+SessionID, Constants.CLICK);

            UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(SessionID, SessionID);

            // Authenticate the user
            Authentication authentication = userAuthenticationProvider.authenticate(authRequest);
            SecurityContext securityContext = SecurityContextHolder.getContext();
            securityContext.setAuthentication(authentication);

            // Create a new session and add the security context.
            HttpSession session = request.getSession(true);
            session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);

        }

        if (request.getSession().getAttribute("userAuthenticated")!=null) {

            traceService.trace("Authenticate", this.getClass().getName(), "/Login", "Autenticato con token SSO", Constants.CLICK);

            resultPage="home";

        }else{

            traceService.trace("Authenticate", this.getClass().getName(), "/Login", "Tentativa accesso tramite token SSO vuoto", Constants.CLICK);

            String applicationCode = configurationService.getParameter("SSO_APPLICATION_CODE");
            String homeUrl = configurationService.getParameter("SSO_HOME_URL");

            resultPage="redirect:"+homeUrl+applicationCode;
        }

        return resultPage;
    }

    @RequestMapping(value="/ssoEnable",  method= RequestMethod.GET)
    public ResponseEntity<String> configureSSO(@RequestParam(value="sso", required = false) String sso, Model model) {

        String response="OK";

        try{

            Configurazione parametroSSO = configurationService.getParameterObject("SSO_ENABLED");

            if (parametroSSO==null){
                parametroSSO = new  Configurazione();
                parametroSSO.setParameterName("SSO_ENABLED");
                parametroSSO.setFlagAttivo('Y');
                parametroSSO.setUtenteUltimaModifica("admin");
                parametroSSO.setDataOraUltimaModifica(new Date());

            }

            parametroSSO.setParameterValue(sso);
            configurationService.insertParameterObject(parametroSSO);


        }catch (Exception e){

            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);

            response="KO";
            traceService.trace("Configurazione", this.getClass().getName(), "/ssoEnable", sw.toString(), Constants.ERROR);
        }

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

        @RequestMapping(value="/logout",  method= RequestMethod.POST)
    public String logout(Model model) {

        traceService.trace("Authenticate", this.getClass().getName(), "/logout", "Logout", Constants.CLICK);

        String resultPage=null;

        /*
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request =  attr.getRequest();

        SecurityContext securityContext = SecurityContextHolder.getContext();

        // Create a new session and add the security context.
        HttpSession session = request.getSession(true);
        session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
       */

        //Authentication auth = SecurityContextHolder.getContext().getAuthentication(); // concern you

        SecurityContextHolder.clearContext();

        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request =  attr.getRequest();

        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }

        //String applicationCode = configurationService.getParameter("SSO_APPLICATION_CODE");
        String homeUrl = configurationService.getParameter("SSO_HOME_URL");

        //model.addAttribute("homeIntranet", homeUrl);

        resultPage="redirect:"+homeUrl;


        return resultPage;
    }

    @RequestMapping(value="/guest/deactivateEngine",  method= RequestMethod.GET)
    public String deactivateEngine() {

        String result = consoleRicalcoloService.deactivate();

        return result;
    }

        @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest req, Exception exception) {

        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", exception);
        mav.addObject("url", req.getRequestURL());
        mav.setViewName("error");
        exception.printStackTrace();

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exception.printStackTrace(pw);

        traceService.trace("Ricalcolo", this.getClass().getName(), req.getRequestURL().toString(), sw.toString(), Constants.ERROR);



        return mav;
    }

    private String toJson(Object obj){
        String ris = null;
        try {
            return new JsonBuilder(obj).toString();
        } catch (Exception e) {
            e.printStackTrace();
            ris = Utility.jsonNOK(e.getMessage());
        }
        return ris;
    }
}
