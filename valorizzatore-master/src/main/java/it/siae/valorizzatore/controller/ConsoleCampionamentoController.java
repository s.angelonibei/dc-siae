package it.siae.valorizzatore.controller;

import groovy.json.JsonBuilder;
import it.siae.valorizzatore.model.CampionamentoConfig;
import it.siae.valorizzatore.model.EsecuzioneRicalcolo;
import it.siae.valorizzatore.model.InformazioniPM;
import it.siae.valorizzatore.model.ProgrammaMusicale;
import it.siae.valorizzatore.model.security.ReportPage;
import it.siae.valorizzatore.model.security.User;
import it.siae.valorizzatore.service.*;
import it.siae.valorizzatore.utility.Constants;
import it.siae.valorizzatore.utility.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.Date;
import java.util.List;

/**
 * Created by idilello on 6/11/16.
 */
@Controller
@RequestMapping("/secure")
public class ConsoleCampionamentoController {

    @Autowired
    private ICampionamentoService campionamentoService;

    @Autowired
    private IProgrammaMusicaleService programmaMusicaleService;

    @Autowired
    private IConsoleRicalcoloService consoleRicalcoloService;

    @Autowired
    private IConfigurationService configurationService;

    @Autowired
    private ITraceService traceService;

    @Autowired
    private ServletContext servletContext;

    @RequestMapping(value="/campionamentoConfigurazione",  method= RequestMethod.GET)
    public String campionamentoConfigurazione(Model model) {

        traceService.trace("Campionamento", this.getClass().getName(), "/campionamentoConfigurazione", "Accesso menu Configurazione", Constants.CLICK);

        return "campionamentoConfigurazione";
    }


    @RequestMapping(value="/campionamentoConfigurazioneSave",  method= RequestMethod.POST)
    public String campionamentoConfigurazioneSave(@RequestParam(value="mesePeriodoContabilita") String mesePeriodoContabilita,
                                                  @RequestParam(value="annoPeriodoContabilita") String annoPeriodoContabilita,
                                                  @RequestParam(value="dataInizioLavorazione") String dataInizioLavorazione,
                                                  @RequestParam(value="dataEstrazioneLotto") String dataEstrazioneLotto,

                                                  @RequestParam(value="bsm1") String bsm1,
                                                  @RequestParam(value="bsm2") String bsm2,
                                                  @RequestParam(value="bsm3") String bsm3,
                                                  @RequestParam(value="bsm4") String bsm4,
                                                  @RequestParam(value="bsm5") String bsm5,

                                                  @RequestParam(value="concertini1") String concertini1,
                                                  @RequestParam(value="concertini2") String concertini2,
                                                  @RequestParam(value="concertini3") String concertini3,
                                                  @RequestParam(value="concertini4") String concertini4,
                                                  @RequestParam(value="concertini5") String concertini5,

                                                  @RequestParam(value="resto5bsm") String resto5bsm,
                                                  @RequestParam(value="resto5concertini") String resto5concertini,
                                                  @RequestParam(value="resto61bsm") String resto61bsm,
                                                  @RequestParam(value="resto61concertini") String resto61concertini,

                                                  Model model) {

        traceService.trace("Campionamento", this.getClass().getName(), "/campionamentoConfigurazioneSave", "Salvataggio configurazione", Constants.CLICK);

        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest currentRequest =  attr.getRequest();
        User user = (User)currentRequest.getSession().getAttribute("user");

        String inizioPeriodoContabile;

        if (mesePeriodoContabilita.length()==1)
            inizioPeriodoContabile = annoPeriodoContabilita+"0"+mesePeriodoContabilita;
        else
            inizioPeriodoContabile = annoPeriodoContabilita+mesePeriodoContabilita;

        // ToDo: ripetere tutti i controlli di congruità dei dati datti in Javascript anche sul backend

        campionamentoService.insertParametriCampionamento(inizioPeriodoContabile, dataInizioLavorazione, dataEstrazioneLotto,
                                                             bsm1, bsm2, bsm3, bsm4, bsm5,
                                                             concertini1, concertini2, concertini3, concertini4, concertini5,
                                                             resto5bsm, resto5concertini, resto61bsm, resto61concertini, user);



        String message = "Configurazione eseguita";

        model.addAttribute("dataInizioLavorazione", dataInizioLavorazione);
        model.addAttribute("dataEstrazioneLotto", dataEstrazioneLotto);
        model.addAttribute("mesePeriodoContabilita", mesePeriodoContabilita);
        model.addAttribute("annoPeriodoContabilita", annoPeriodoContabilita);

        model.addAttribute("bsm1", bsm1);
        model.addAttribute("bsm2", bsm2);
        model.addAttribute("bsm3", bsm3);
        model.addAttribute("bsm4", bsm4);
        model.addAttribute("bsm5", bsm5);

        model.addAttribute("concertini1", concertini1);
        model.addAttribute("concertini2", concertini2);
        model.addAttribute("concertini3", concertini3);
        model.addAttribute("concertini4", concertini4);
        model.addAttribute("concertini5", concertini5);

        model.addAttribute("resto5bsm", resto5bsm);
        model.addAttribute("resto5concertini", resto5concertini);
        model.addAttribute("resto61bsm", resto61bsm);
        model.addAttribute("resto61concertini", resto61concertini);

        model.addAttribute("message", message);

        return "campionamentoConfigurazione";
    }


    @RequestMapping(value="/campionamentoStorico",  method= RequestMethod.GET)
    public String campionamentoStorico(Model model) {

        traceService.trace("Campionamento", this.getClass().getName(), "/campionamentoStorico", "Accesso visualizzazione parametri storici di campionamento", Constants.CLICK);

        return "campionamentoStorico";
    }



    @RequestMapping(value="/campionamentoStoricoLista",  method= RequestMethod.GET)
    public String campionamentoStoricoLista(@RequestParam(value="annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
                                            @RequestParam(value="meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
                                            @RequestParam(value="annoFinePeriodoContabile") String annoFinePeriodoContabile,
                                            @RequestParam(value="meseFinePeriodoContabile") String meseFinePeriodoContabile,
                                            @RequestParam(value="page") Integer page,
                                            Model model) {

        traceService.trace("Campionamento", this.getClass().getName(), "/campionamentoStoricoLista", "Visualizzazione parametri storici di campionamento", Constants.CLICK);

        String message = null;
        String inizioPeriodoContabile;
        String finePeriodoContabile;

        if (annoInizioPeriodoContabile==null || meseInizioPeriodoContabile==null || annoFinePeriodoContabile==null || meseFinePeriodoContabile==null){
            message = "E' necessario selezionare un criterio di ricerca";
            model.addAttribute("message", message);
        } else {

            if (meseInizioPeriodoContabile.length() == 1)
                inizioPeriodoContabile = annoInizioPeriodoContabile + "0" + meseInizioPeriodoContabile;
            else
                inizioPeriodoContabile = annoInizioPeriodoContabile + meseInizioPeriodoContabile;

            if (meseFinePeriodoContabile.length() == 1)
                finePeriodoContabile = annoFinePeriodoContabile + "0" + meseFinePeriodoContabile;
            else
                finePeriodoContabile = annoFinePeriodoContabile + meseFinePeriodoContabile;


            ReportPage resultPage = campionamentoService.getCampionamenti(Long.parseLong(inizioPeriodoContabile), Long.parseLong(finePeriodoContabile), page);

            model.addAttribute("annoInizioPeriodoContabile", annoInizioPeriodoContabile);
            model.addAttribute("meseInizioPeriodoContabile", meseInizioPeriodoContabile);
            model.addAttribute("annoFinePeriodoContabile", annoFinePeriodoContabile);
            model.addAttribute("meseFinePeriodoContabile", meseFinePeriodoContabile);

            model.addAttribute("resultPage", resultPage);
        }



        return "campionamentoStorico";
    }

    // ---------

    @RequestMapping(value="/campionamentoStoricoEsecuzioni",  method= RequestMethod.GET)
    public String campionamentoStoricoEsecuzioni(Model model) {

        traceService.trace("Campionamento", this.getClass().getName(), "/campionamentoStoricoEsecuzioni", "Accesso visualizzazione storico esecuzioni", Constants.CLICK);

        return "campionamentoStoricoEsecuzioni";
    }



    @RequestMapping(value="/campionamentoStoricoEsecuzioniLista",  method= RequestMethod.GET)
    public String campionamentoStoricoEsecuzioniLista(@RequestParam(value="annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
                                            @RequestParam(value="meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
                                            @RequestParam(value="annoFinePeriodoContabile") String annoFinePeriodoContabile,
                                            @RequestParam(value="meseFinePeriodoContabile") String meseFinePeriodoContabile,
                                            @RequestParam(value="page") Integer page,
                                            Model model) {

        traceService.trace("Campionamento", this.getClass().getName(), "/campionamentoStoricoEsecuzioniLista", "Visualizzazione storico esecuzioni", Constants.CLICK);

        String message=null;
        String inizioPeriodoContabile;
        String finePeriodoContabile;

        if (annoInizioPeriodoContabile==null || meseInizioPeriodoContabile==null || annoFinePeriodoContabile==null || meseFinePeriodoContabile==null){
            message = "E' necessario selezionare un criterio di ricerca";
            model.addAttribute("message", message);
        } else {

            if (meseInizioPeriodoContabile.length()==1)
                inizioPeriodoContabile = annoInizioPeriodoContabile+"0"+meseInizioPeriodoContabile;
            else
                inizioPeriodoContabile = annoInizioPeriodoContabile+meseInizioPeriodoContabile;

            if (meseFinePeriodoContabile.length()==1)
                finePeriodoContabile = annoFinePeriodoContabile+"0"+meseFinePeriodoContabile;
            else
                finePeriodoContabile = annoFinePeriodoContabile+meseFinePeriodoContabile;


            ReportPage resultPage = campionamentoService.getStoricoEsecuzioni(Long.parseLong(inizioPeriodoContabile), Long.parseLong(finePeriodoContabile),page);

            model.addAttribute("annoInizioPeriodoContabile", annoInizioPeriodoContabile);
            model.addAttribute("meseInizioPeriodoContabile", meseInizioPeriodoContabile);
            model.addAttribute("annoFinePeriodoContabile", annoFinePeriodoContabile);
            model.addAttribute("meseFinePeriodoContabile", meseFinePeriodoContabile);

            model.addAttribute("resultPage", resultPage);
        }

        return "campionamentoStoricoEsecuzioni";
    }

    // ---------


    @RequestMapping(value="/campionamentoEsecuzione",  method= RequestMethod.GET)
    public String ricalcoloEsecuzione(Model model) {

        traceService.trace("Campionamento", this.getClass().getName(), "/campionamentoEsecuzione", "Accesso esecuzione algoritmo di campionamento", Constants.CLICK);

        String endDate = Utility.dateString(new Date(), Utility.Format_YYYYMM);
        String startDate = Utility.dateString(Utility.stringDate("01/01/2016", Utility.OracleFormat_D), Utility.Format_YYYYMM);

        ReportPage resultPage = campionamentoService.getCampionamenti(Long.parseLong(startDate), Long.parseLong(endDate),1);

        model.addAttribute("resultPage", resultPage);


        return "campionamentoEsecuzione";
    }

    @RequestMapping(value = "/getAvanzamentoElaborazioneCampionamento", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getAvanzamentoElaborazioneCampionamento(Model model) {

        String response="";

        try{

            InformazioniPM informazioni = new InformazioniPM();

            informazioni = consoleRicalcoloService.getAvanzamentoElaborazioneCampionamento();

            response = toJson(informazioni);

        }catch (Exception e){}

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }


    @RequestMapping(value="/campionamentoFlussoGuida",  method= RequestMethod.GET)
    public String campionamentoFlussoGuida(Model model) {

        traceService.trace("Campionamento", this.getClass().getName(), "/campionamentoFlussoGuida", "Accesso esportazione flusso guida", Constants.CLICK);

        String currentYear = Utility.dateString(new Date(), "yyyy");
        String currentMonth = Utility.dateString(new Date(), "MM");
        String finePeriodoRicalcolo = configurationService.getParameter("FINE_PERIODO_RIPARTIZIONE");

        Long annoFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(0,4));
        Long meseFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(4,6));

        if (meseFinePeriodoRicalcolo==12) {
            model.addAttribute("meseInizioPeriodoContabile", "01");
            model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo+1);
        }else {
            if (meseFinePeriodoRicalcolo<9)
                model.addAttribute("meseInizioPeriodoContabile", "0"+meseFinePeriodoRicalcolo);
            else
                model.addAttribute("meseInizioPeriodoContabile", meseFinePeriodoRicalcolo);

            model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo);
        }

        model.addAttribute("meseFinePeriodoContabile", currentMonth);
        model.addAttribute("annoFinePeriodoContabile", currentYear);

        return "campionamentoFlussoGuida";
    }

    @RequestMapping(value="/viewFlussoGuida",  method= RequestMethod.POST)
    public String viewFlussoGuida(ModelMap model,
                                  @RequestParam(value="meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
                                  @RequestParam(value="annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
                                  @RequestParam(value="meseFinePeriodoContabile") String meseFinePeriodoContabile,
                                  @RequestParam(value="annoFinePeriodoContabile") String annoFinePeriodoContabile,
                                  @RequestParam(value="page") Integer page,
                                  HttpServletRequest request,
                                  HttpServletResponse response) throws IOException{

        traceService.trace("Campionamento", this.getClass().getName(), "/viewFlussoGuida", "Visualizzazione flusso guida", Constants.CLICK);

        Long inizioPeriodoContabile = Utility.normalizePeriodoContabile(meseInizioPeriodoContabile, annoInizioPeriodoContabile);
        Long finePeriodoContabile = Utility.normalizePeriodoContabile(meseFinePeriodoContabile, annoFinePeriodoContabile);

        String downloadPath = servletContext.getRealPath("/") + "/download/";

        ReportPage resultPage = programmaMusicaleService.getFlussoGuida(inizioPeriodoContabile, finePeriodoContabile, page);

        downloadPath = servletContext.getRealPath("/") + "/download/"+resultPage.getFileName();

        model.addAttribute("downloadPath", downloadPath);
        model.addAttribute("resultPage", resultPage);

        model.addAttribute("annoInizioPeriodoContabile", annoInizioPeriodoContabile);
        model.addAttribute("meseInizioPeriodoContabile", meseInizioPeriodoContabile);

        model.addAttribute("meseFinePeriodoContabile", meseFinePeriodoContabile);
        model.addAttribute("annoFinePeriodoContabile", annoFinePeriodoContabile);


        return "campionamentoFlussoGuida";
    }


    @RequestMapping(value="/downloadCSVFlussoGuida",method=RequestMethod.POST)
    public ModelAndView downloadCSVFlussoGuida(ModelMap model,
                                               @RequestParam(value="meseInizioPeriodoContabileSel", required = false) String meseInizioPeriodoContabile,
                                               @RequestParam(value="annoInizioPeriodoContabileSel", required = false) String annoInizioPeriodoContabile,
                                               @RequestParam(value="meseFinePeriodoContabileSel", required = false) String meseFinePeriodoContabile,
                                               @RequestParam(value="annoFinePeriodoContabileSel", required = false) String annoFinePeriodoContabile,
                                               HttpServletRequest request, HttpServletResponse response) throws IOException{

        traceService.trace("Campionamento", this.getClass().getName(), "/downloadCSVFlussoGuida", "Download flusso guida", Constants.CLICK);

        String fileName = "FlussoGuida_"+Utility.dateString(new Date(), Utility.OracleFormat_DT)+".csv";

        response.setContentType("application/csv");
        response.setHeader("content-disposition","attachment;filename ="+fileName);

        Long inizioPeriodoContabile = Utility.normalizePeriodoContabile(meseInizioPeriodoContabile, annoInizioPeriodoContabile);
        Long finePeriodoContabile = Utility.normalizePeriodoContabile(meseFinePeriodoContabile, annoFinePeriodoContabile);

        ServletOutputStream writer = response.getOutputStream();

        ReportPage resultPage = programmaMusicaleService.saveAndDownloadFlussoGuida(inizioPeriodoContabile, finePeriodoContabile);

        int numRecords = resultPage.getTotRecords();

        writer.print("NUMERO PM,VOCE INCASSO, DA CAMPIONARE,NUMERO PM RIFERIMENTO");

        ProgrammaMusicale p;
        String pmRiferimento;
        Character flagDaCampionare;
        for(int i=0;i<numRecords;i++){

            p=(ProgrammaMusicale)resultPage.getRecords().get(i);
            pmRiferimento = p.getPmRiferimento()!=null?p.getPmRiferimento():" ";
            flagDaCampionare = p.getFlagDaCampionare()!=null?p.getFlagDaCampionare():' ';

            writer.print(p.getNumeroProgrammaMusicale()+","+p.getVoceIncasso()+","+flagDaCampionare+","+pmRiferimento);
            writer.println();
        }

        writer.flush();
        writer.close();

        return null;
    }



    @RequestMapping(value="/campionamentoVerificaRientrati",  method= RequestMethod.GET)
    public String campionamentoVerificaRientrati(Model model) {

        traceService.trace("Campionamento", this.getClass().getName(), "/campionamentoVerificaRientrati", "Accesso verifica PM rientrati", Constants.CLICK);

        String currentYear = Utility.dateString(new Date(), "yyyy");
        String currentMonth = Utility.dateString(new Date(), "MM");
        String finePeriodoRicalcolo = configurationService.getParameter("FINE_PERIODO_RIPARTIZIONE");

        Long annoFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(0,4));
        Long meseFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(4,6));

        if (meseFinePeriodoRicalcolo==12) {
            model.addAttribute("meseInizioPeriodoContabile", "01");
            model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo+1);
        }else {
            if (meseFinePeriodoRicalcolo<9)
                model.addAttribute("meseInizioPeriodoContabile", "0"+meseFinePeriodoRicalcolo);
            else
                model.addAttribute("meseInizioPeriodoContabile", meseFinePeriodoRicalcolo);

            model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo);
        }

        model.addAttribute("meseFinePeriodoContabile", currentMonth);
        model.addAttribute("annoFinePeriodoContabile", currentYear);

        return "campionamentoFlussoRientrati";
    }

    @RequestMapping(value="/viewFlussoRientrati",  method= RequestMethod.POST)
    public String viewFlussoRientrati(ModelMap model,
                                  @RequestParam(value="meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
                                  @RequestParam(value="annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
                                  @RequestParam(value="meseFinePeriodoContabile") String meseFinePeriodoContabile,
                                  @RequestParam(value="annoFinePeriodoContabile") String annoFinePeriodoContabile,
                                  @RequestParam(value="page") Integer page,
                                  HttpServletRequest request,
                                  HttpServletResponse response) throws IOException{

        traceService.trace("Campionamento", this.getClass().getName(), "/viewFlussoRientrati", "Visualizzazione flusso PM rientrati", Constants.CLICK);

        Long inizioPeriodoContabile = Utility.normalizePeriodoContabile(meseInizioPeriodoContabile, annoInizioPeriodoContabile);
        Long finePeriodoContabile = Utility.normalizePeriodoContabile(meseFinePeriodoContabile, annoFinePeriodoContabile);

        ReportPage resultPage = programmaMusicaleService.getFlussoRientrati(inizioPeriodoContabile, finePeriodoContabile, page);

        model.addAttribute("resultPage", resultPage);

        model.addAttribute("annoInizioPeriodoContabile", annoInizioPeriodoContabile);
        model.addAttribute("meseInizioPeriodoContabile", meseInizioPeriodoContabile);

        model.addAttribute("meseFinePeriodoContabile", meseFinePeriodoContabile);
        model.addAttribute("annoFinePeriodoContabile", annoFinePeriodoContabile);

        return "campionamentoFlussoRientrati";
    }

    @RequestMapping(value="/fileupload", method=RequestMethod.POST)
    public String processUpload(ModelMap model,
                                @RequestParam MultipartFile file) throws IOException {

        traceService.trace("Campionamento", this.getClass().getName(), "/fileupload", "Upload flusso PM rientrati", Constants.CLICK);

        String fileName = file.getOriginalFilename();

        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();

                File dir = new File("tmpFiles");
                if (!dir.exists())
                    dir.mkdirs();

                // Create the file on server
                File serverFile = new File(dir.getAbsolutePath() + File.separator + fileName);

                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                stream.write(bytes);
                stream.close();

                int numPMcaricati = programmaMusicaleService.storeFileContant(dir.getAbsolutePath() + File.separator + fileName);

                if (numPMcaricati<0)
                    model.addAttribute("message", "Problemi durante il caricamento del file. Verificare il formato");
                else
                    model.addAttribute("message", "Acquisiti correttamente "+numPMcaricati+" programmi musicali presenti nel file");

            } catch (Exception e) {
                e.printStackTrace();
                model.addAttribute("message", "Errore durante il caricamento del file");
            }
        } else {
            model.addAttribute("message", "File di dimensioni pari a 0");
        }

        return "campionamentoFlussoRientrati";
    }



    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest req, Exception exception) {

        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", exception);
        mav.addObject("url", req.getRequestURL());
        mav.setViewName("error");
        exception.printStackTrace();

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exception.printStackTrace(pw);

        traceService.trace("Campionamento", this.getClass().getName(), req.getRequestURL().toString(), sw.toString(), Constants.ERROR);

        return mav;
    }

    private String toJson(Object obj){
        String ris = null;
        try {
            return new JsonBuilder(obj).toString();
        } catch (Exception e) {
            e.printStackTrace();
            ris = Utility.jsonNOK(e.getMessage());
        }
        return ris;
    }


}

