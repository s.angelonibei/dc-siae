package it.siae.valorizzatore.dao;

import it.siae.valorizzatore.model.EsecuzioneRicalcolo;
import it.siae.valorizzatore.model.MovimentoContabile;
import it.siae.valorizzatore.model.TracciamentoApplicativo;
import it.siae.valorizzatore.model.security.ReportPage;

import java.util.List;

/**
 * Created by idilello on 6/14/16.
 */
public interface IEsecuzioneDAO {

    public List<EsecuzioneRicalcolo> getEsecuzioni();

    public ReportPage getListaPM(Long idEsecuzioneRicalcolo, Long idEvento, String numeroFattura, String voceIncasso, String numeroProgrammaMusicale, String inizioPeriodoContabile, String finePeriodoContabile, Integer page);

    public ReportPage getListaPMSospesi(Long idEsecuzioneRicalcolo, Long idEvento, String numeroFattura, String voceIncasso, String numeroProgrammaMusicale, String inizioPeriodoContabile, String finePeriodoContabile, Integer page,String type);

    public EsecuzioneRicalcolo findEsecuzioneByContabilitaSiada(Long contabilitaSiada);

    public EsecuzioneRicalcolo findEsecuzioneById(Long idEsecuzioneRicalcolo);

    public int setDataCongelamentoEsecuzione(Long elaborazioneSIADAId);

    public Long getNumeroMovimentazioniAggregate(EsecuzioneRicalcolo esecuzione);

    public Long getNumeroMovimentazioniAggregate2243(EsecuzioneRicalcolo esecuzione);

    public void trace(TracciamentoApplicativo tracciamentoApplicativo);

    public void deleteMovimentoSiada();

    public void deleteMovimentoSiada2243();

    public Long getNewEsecuzioneRicalcoloId();
}
