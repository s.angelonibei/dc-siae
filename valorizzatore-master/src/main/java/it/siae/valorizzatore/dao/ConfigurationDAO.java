package it.siae.valorizzatore.dao;

import it.siae.valorizzatore.model.Circoscrizione;
import it.siae.valorizzatore.model.Configurazione;
import it.siae.valorizzatore.model.TrattamentoPM;
import it.siae.valorizzatore.utility.Constants;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by idilello on 6/9/16.
 */
//@Component
@Repository("configurationDAO")
public class ConfigurationDAO implements IConfigurationDAO{

    @PersistenceContext(unitName = "VALORIZZATORE")
    private EntityManager entityManager;


    public void initCache() {

        List<Circoscrizione> circoscrizioni = getCircoscrizioni();
        Constants.CONFIGURATIONS.put("circoscrizioni", circoscrizioni);

        List<TrattamentoPM> listaVociIncasso = getTrattamentoPM();
        Map<String, TrattamentoPM> mappaVociIncasso = new HashMap<String, TrattamentoPM>();
        for (TrattamentoPM voceIncasso : listaVociIncasso){
            mappaVociIncasso.put(voceIncasso.getVoceIncasso(), voceIncasso);
        }
        Constants.CONFIGURATIONS.put("vociIncasso", mappaVociIncasso);


        List<Configurazione> configurazioni = getAllCachedParameter();
        for (int i=0; i<configurazioni.size(); i++){

            Constants.CONFIGURATIONS.put(configurazioni.get(i).getParameterName(), configurazioni.get(i));
        }

    }

    public List<TrattamentoPM> getTrattamentoPM(){

        String query = "select t from TrattamentoPM t where t.tipo is not null";

        List<TrattamentoPM> results = (List<TrattamentoPM>)entityManager.createQuery(query)
                .getResultList();
        return results;

    }

    public String getLogicalServerAddress(String phisycalAddress){

        String logicalAddress = null;
        String mapLogicalPhisycal = null;

        if (Constants.CONFIGURATIONS.get("CLUSTER") != null){

            Configurazione configurazione = (Configurazione)Constants.CONFIGURATIONS.get("CLUSTER");

            mapLogicalPhisycal = configurazione.getParameterValue();

        } else {

            mapLogicalPhisycal = getParameter("CLUSTER");

        }

        // AR147-127.0.1.1:7001,localhost-127.0.0.1:7001
        if (mapLogicalPhisycal != null){

           String[] addresses = mapLogicalPhisycal.split(",");
           String address = null;

            for (int i=0; i<addresses.length; i++){
              address =  addresses[i].trim();
              if (address.startsWith(phisycalAddress)){
                  String[] splittedAddress = address.split("!");
                  logicalAddress = splittedAddress[1];
                  break;
              }
           }
        }

        return logicalAddress;
    }

    public List<Circoscrizione> getCircoscrizioni(){

        if (Constants.CONFIGURATIONS.get("circoscrizioni") !=null){

            return (List<Circoscrizione>)Constants.CONFIGURATIONS.get("circoscrizioni");

        }else {

            String query = "select c from Circoscrizione c where c.id<>'9999999' order by c.denominazione asc";

            List<Circoscrizione> results = (List<Circoscrizione>) entityManager.createQuery(query)
                    .getResultList();

            return results;
        }

    }

    public List<Configurazione> getAllCachedParameter(){

        String query = "select c from Configurazione c where c.flagAttivo='Y' and c.flagCache='Y' ";

        List<Configurazione> results = (List<Configurazione>)entityManager.createQuery(query)
                .getResultList();

        return results;

    }

    public String getParameter(String parameterName){

        if (Constants.CONFIGURATIONS.get(parameterName) != null){

            Configurazione parametro = (Configurazione)Constants.CONFIGURATIONS.get(parameterName);

           return parametro.getParameterValue();

         }else {

            String query = "select c from Configurazione c where c.parameterName=:parameterName and c.flagAttivo='Y'";

            List<Configurazione> results = (List<Configurazione>) entityManager.createQuery(query)
                    .setParameter("parameterName", parameterName)
                    .getResultList();

            if (results != null && results.size() > 0)
                return results.get(0).getParameterValue();
            else
                return null;
        }

    }


    @Transactional(propagation = Propagation.REQUIRED)
    public void insertParameter(String parameterName, String parameterValue){

        Configurazione configurazione;


        try {
            String query = "select c from Configurazione c where c.parameterName=:parameterName and c.flagAttivo='Y'";

            List<Configurazione> results = (List<Configurazione>)entityManager.createQuery(query)
                    .setParameter("parameterName",parameterName)
                    .getResultList();

            if (results!=null && results.size()>0) {

                configurazione = results.get(0);

                configurazione.setParameterValue(parameterValue);
                configurazione.setDataOraUltimaModifica(new Date());

                entityManager.persist(configurazione);
                entityManager.flush();

            }

         }catch(Exception e){
                e.printStackTrace();
         }

    }

    public Configurazione getParameterObject(String parameterName){

        if (Constants.CONFIGURATIONS.get(parameterName) != null){

            Configurazione parametro = (Configurazione)Constants.CONFIGURATIONS.get(parameterName);

            return parametro;

        }else {

            String query = "select c from Configurazione c where c.parameterName=:parameterName and c.flagAttivo='Y'";

            List<Configurazione> results = (List<Configurazione>) entityManager.createQuery(query)
                    .setParameter("parameterName", parameterName)
                    .getResultList();

            if (results != null && results.size() > 0)
                return results.get(0);
            else
                return null;
        }

    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void insertParameterObject(Configurazione configurazione){

        try {

            Configurazione conf = getParameterObject(configurazione.getParameterName());

            if (conf!=null){
                entityManager.merge(configurazione);
            }else{
                entityManager.persist(configurazione);
            }

            entityManager.flush();

        }catch(Exception e){
            e.printStackTrace();
        }
    }



}
