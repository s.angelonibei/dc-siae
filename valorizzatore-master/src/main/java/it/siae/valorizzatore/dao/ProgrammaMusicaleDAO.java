package it.siae.valorizzatore.dao;

import it.siae.valorizzatore.model.*;
import it.siae.valorizzatore.model.security.AggregatoVoce;
import it.siae.valorizzatore.model.security.ReportPage;
import it.siae.valorizzatore.utility.Constants;
import it.siae.valorizzatore.utility.Utility;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.*;
import java.util.jar.Manifest;

@Repository("programmaMusicaleDAO")
public class ProgrammaMusicaleDAO implements IProgrammaMusicaleDAO{


    @PersistenceContext(unitName = "VALORIZZATORE")
    private EntityManager entityManager;


    @Transactional(propagation = Propagation.REQUIRED)
    public void insertMovimentazioneLogisticaPM(MovimentazioneLogisticaPM movimentazioneLogisticaPM){

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setName("rootTransaction");
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        try {
            entityManager.persist(movimentazioneLogisticaPM);
            entityManager.flush();

        }catch(Exception e){
        e.printStackTrace();
        }

    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void updatePMRientrati(ProgrammaMusicale programmaMusicale){


        try {

            List<ProgrammaMusicale> programmiRientrati = getProgrammaMusicale(programmaMusicale.getNumeroProgrammaMusicale());
            MovimentazioneLogisticaPM ultimaMovimentazioneLogisticaPM;
            MovimentazioneLogisticaPM nuovaMovimentazioneLogisticaPM;
            for (ProgrammaMusicale programma : programmiRientrati) {

                programma.setTotaleCedole(programmaMusicale.getTotaleCedole());
                programma.setTotaleDurataCedole(programmaMusicale.getTotaleDurataCedole());
                entityManager.merge(programma);

                ultimaMovimentazioneLogisticaPM = getUltimaMovimentazionePM(programma.getId());
                if (ultimaMovimentazioneLogisticaPM!=null){
                    ultimaMovimentazioneLogisticaPM.setFlagAttivo('N');
                    entityManager.merge(ultimaMovimentazioneLogisticaPM);
                }

                nuovaMovimentazioneLogisticaPM = new MovimentazioneLogisticaPM();
                nuovaMovimentazioneLogisticaPM.setFlagAttivo('Y');
                nuovaMovimentazioneLogisticaPM.setIdProgrammaMusicale(ultimaMovimentazioneLogisticaPM.getIdProgrammaMusicale());
                nuovaMovimentazioneLogisticaPM.setStato(Constants.RITORNATO_DA_NEED);
                nuovaMovimentazioneLogisticaPM.setFlagEsclusoRicalcolo(ultimaMovimentazioneLogisticaPM.getFlagEsclusoRicalcolo());
                nuovaMovimentazioneLogisticaPM.setFlagEsclusoInvioNeed(ultimaMovimentazioneLogisticaPM.getFlagEsclusoInvioNeed());
                entityManager.persist(nuovaMovimentazioneLogisticaPM);

                entityManager.flush();

            }



        }catch(Exception e){
            e.printStackTrace();
        }

    }


    public List<ProgrammaMusicale> getProgrammaMusicale(Long numeroProgrammaMusicale){

        List<ProgrammaMusicale> results = new ArrayList<ProgrammaMusicale>();

        String query = "select c from ProgrammaMusicale c where c.numeroProgrammaMusicale=:numeroProgrammaMusicale";

        results = (List<ProgrammaMusicale>)entityManager.createQuery(query)
                                          .setParameter("numeroProgrammaMusicale", numeroProgrammaMusicale)
                                          .getResultList();

        if (results==null)
            results = new ArrayList<ProgrammaMusicale>();

        return results;

    }


    public MovimentazioneLogisticaPM getUltimaMovimentazionePM(Long idProgrammaMusicale){

        List<MovimentazioneLogisticaPM> results = new ArrayList<MovimentazioneLogisticaPM>();

        String query = "select m from MovimentazioneLogisticaPM m where m.idProgrammaMusicale=:idProgrammaMusicale and m.flagAttivo=:flagAttivo";

        results = (List<MovimentazioneLogisticaPM>)entityManager.createQuery(query)
                .setParameter("idProgrammaMusicale", idProgrammaMusicale)
                .setParameter("flagAttivo", 'Y')
                .getResultList();

        if (results!=null && results.size()>0)
           return results.get(0);
        else
           return null;

    }


    public ReportPage getTracciamentoPM(Date dataIniziale, Date dataFinale, Long numeroPM, Integer page){

        String query = null;

        List<ProgrammaMusicale> listaPM = new ArrayList<ProgrammaMusicale>();
        List<ProgrammaMusicale> listaAllPM = new ArrayList<ProgrammaMusicale>();

        ReportPage reportPage = new ReportPage(listaPM, listaPM.size(), page);

        int recordCount=0;

        query = "select t.ID_PM, t.NUMERO_PM, t.DATA, t.STATO "+
                "from "+
                "(select distinct(id_programma_musicale) AS ID_PM, to_number(numero_pm) AS NUMERO_PM, trunc(min(DATA_INS)) AS DATA, 'ENTRATO_VALORIZZATORE' as STATO "+
                "        from movimento_contabile_158  "+
                "        where trunc(DATA_INS)>=trunc(?) and trunc(DATA_INS)<=trunc(?) "+   // and rownum<100
                "        group by id_programma_musicale, numero_pm "+
                "        union "+
                "        select m.id_programma_musicale AS ID_PM, p.numero_programma_musicale AS NUMERO_PM, trunc(m.DATA_INS) AS DATA, m.STATO "+
                "        from MOVIMENTAZIONE_LOGISTICA_PM m, PROGRAMMA_MUSICALE p "+
                "        where m.id_programma_musicale=p.id_programma_musicale  "+
                "        and trunc(m.DATA_INS)>=trunc(?) and trunc(m.DATA_INS)<=trunc(?) "+
                "        union "+
                "        select m.ID_PROGRAMMA_MUSICALE AS ID_PM, to_number(numero_pm) AS NUMERO_PM,trunc(i.DATA_ORA_INSERIMENTO) AS DATA, 'IMPORTO_RICALCOLATO' AS STATO "+
                "        from importo_ricalcolato i, movimento_contabile_158 m  "+
                "        where i.ID_MOVIMENTO_CONTABILE=m.ID_MOVIMENTO_CONTABILE_158  "+
                "        and trunc(i.DATA_ORA_INSERIMENTO)>=trunc(?) and trunc(i.DATA_ORA_INSERIMENTO)<=trunc(?) "+
                " ) t ";

        List<ProgrammaMusicale> programmaMusicale = null;

        if (numeroPM != null && !numeroPM.equals("")) {
            programmaMusicale = getProgrammaMusicale(numeroPM);
            if (programmaMusicale != null && programmaMusicale.size()>0)
              query = query + "where t.ID_PM=? ";
            else{
                reportPage = new ReportPage(listaPM, 0, page);
                return reportPage;
            }

        }

        query = query + "order by t.ID_PM,t.DATA asc ";

        if (dataIniziale == null)
          dataIniziale= Utility.stringDate("01/01/2016",Utility.OracleFormat_D);
        if (dataFinale == null)
            dataFinale= new Date();

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, dataIniziale);
        q.setParameter(2, dataFinale);
        q.setParameter(3, dataIniziale);
        q.setParameter(4, dataFinale);
        q.setParameter(5, dataIniziale);
        q.setParameter(6, dataFinale);

        if (programmaMusicale != null && programmaMusicale.size()>0)
         q.setParameter(7, programmaMusicale.get(0).getId());

        List<Object[]> results = (List<Object[]>)q.getResultList();

        if (results != null && results.size() > 0) {

            recordCount = results.size();

            int startRecord = 0;
            int endRecord = recordCount;

            if (page != -1) {

                startRecord = (page - 1) * ReportPage.NUMBER_RECORD_PER_PAGE;
                endRecord = 0;
                if (recordCount > ReportPage.NUMBER_RECORD_PER_PAGE) {
                    endRecord = startRecord + ReportPage.NUMBER_RECORD_PER_PAGE;
                    if (endRecord > recordCount)
                        endRecord = recordCount - 1;
                } else {
                    endRecord = recordCount;
                }

            }

            ProgrammaMusicale programma = null;

            for (int i = 0; i < recordCount; i++) {

                programma = new ProgrammaMusicale();
                if (results.get(i)[0]!=null)
                 programma.setId(((BigDecimal)results.get(i)[0]).longValue());
                if (results.get(i)[1]!=null)
                 programma.setNumeroProgrammaMusicale(((BigDecimal)results.get(i)[1]).longValue());
                if (results.get(i)[2]!=null)
                 programma.setDataPassaggioStato(Utility.dateString((Date)results.get(i)[2],Utility.OracleFormat_D));
                programma.setStato((String)results.get(i)[3]);


                if (i>=startRecord && i<endRecord)
                  listaPM.add(programma);

                listaAllPM.add(programma);
            }

            //reportPage = new ReportPage(listaPM, recordCount, page);
            reportPage = new ReportPage(listaAllPM, listaPM, recordCount, page);

        }

        return reportPage ;

    }

    public EventiPagati getEventoPagato(Long idEvento, String voceIncasso, String numeroFattura){

        EventiPagati eventoPagato = null;

        String query = "select e from EventiPagati e "+
                       "where e.idEvento=:idEvento "+
                        "and e.numeroFattura=:numeroFattura "+
                        "and e.voceIncasso=:voceIncasso";

        Query q = entityManager.createQuery(query);


        q.setParameter("idEvento", idEvento);
        q.setParameter("numeroFattura", numeroFattura);
        q.setParameter("voceIncasso", voceIncasso);

        List<EventiPagati> results = (List<EventiPagati>)q.getResultList();

        if (results != null && results.size() > 0) {
            eventoPagato = results.get(0);
        }

        return eventoPagato;

    }

    public ReportPage getListaEventiPagati(Long inizioPeriodoContabile, Long finePeriodoContabile, Long evento, String fattura, String reversale, String voceIncasso,
                                           Date dataInizioEvento, String oraInizioEvento, String seprag, String tipoDocumento, String locale, String codiceBA, String presenzaMovimenti, Integer page) {

        String query = "";

        List<EventiPagati> listaEventi = new ArrayList<EventiPagati>();

        ReportPage reportPage = new ReportPage(listaEventi, listaEventi.size(), page);

        int recordCount=0;

        if (presenzaMovimenti != null && !presenzaMovimenti.equalsIgnoreCase("T")) {

            query = "select distinct e from EventiPagati e, MovimentoContabile m ";

        }else {
            query = "select e from EventiPagati e ";
        }

        query = query + "where e.contabilita>=:inizioPeriodoContabile and e.contabilita<=:finePeriodoContabile "+
                        "and (:idEvento is null or e.idEvento=:idEvento) "+
                        "and (:numeroFattura is null or e.numeroFattura=:numeroFattura) "+
                        "and (:reversale is null or e.reversale=:reversale) "+
                        "and (:voceIncasso is null or e.voceIncasso=:voceIncasso) "+
                        "and (:dataInizioEvento is null or e.dataInizioEvento=:dataInizioEvento) "+
                        "and (:oraInizioEvento is null or e.oraInizioEvento=:oraInizioEvento) "+
                        "and (:seprag is null or e.seprag=:seprag) "+
                        "and (:tipoDocumento is null or e.tipoDocumentoContabile=:tipoDocumento) ";

        if (presenzaMovimenti != null && !presenzaMovimenti.equalsIgnoreCase("T")) {
            query = query + "and m.idEvento=e.idEvento ";
            query = query + "and m.numeroFattura=e.numeroFattura ";
            query = query + "and m.voceIncasso=e.voceIncasso ";
        }

        if (locale !=null) {
            query = query + "and (:denominazioneLocale is null or upper(e.denominazioneLocale) like upper(:denominazioneLocale)) ";
        }

        if (codiceBA != null) {
            query = query + "and (:codiceBaLocale is null or e.codiceBaLocale=:codiceBaLocale) ";
        }

        query = query + "order by e.idEvento, e.numeroFattura, e.voceIncasso asc ";

        Query q = entityManager.createQuery(query);

        q.setParameter("inizioPeriodoContabile", inizioPeriodoContabile);
        q.setParameter("finePeriodoContabile", finePeriodoContabile);

        q.setParameter("idEvento", evento);
        q.setParameter("numeroFattura", fattura);
        q.setParameter("reversale", reversale);
        q.setParameter("voceIncasso", voceIncasso);
        q.setParameter("dataInizioEvento", dataInizioEvento);
        q.setParameter("oraInizioEvento", oraInizioEvento);
        q.setParameter("seprag",seprag );
        q.setParameter("tipoDocumento", tipoDocumento);
        if (locale !=null)
         q.setParameter("denominazioneLocale","%"+locale+"%");
        if (codiceBA != null)
         q.setParameter("codiceBaLocale", codiceBA);

        List<EventiPagati> listaAllEventi = (List<EventiPagati>)q.getResultList();

        if (listaAllEventi != null && listaAllEventi.size() > 0) {

            recordCount = listaAllEventi.size();

            int startRecord = 0;
            int endRecord = recordCount;

            if (page != -1) {

                startRecord = (page - 1) * ReportPage.NUMBER_RECORD_PER_PAGE;
                endRecord = 0;
                if (recordCount > ReportPage.NUMBER_RECORD_PER_PAGE) {
                    endRecord = startRecord + ReportPage.NUMBER_RECORD_PER_PAGE;
                    if (endRecord > recordCount)
                        endRecord = recordCount - 1;
                } else {
                    endRecord = recordCount;
                }

            }

            EventiPagati eventoPagato = null;

            for (int i = startRecord; i < endRecord; i++) {

                eventoPagato = (EventiPagati)listaAllEventi.get(i);

                listaEventi.add(eventoPagato);
            }

            //reportPage = new ReportPage(listaEventi, recordCount, page);
            reportPage = new ReportPage(listaAllEventi, listaEventi, recordCount, page);

        }

        return reportPage;
    }

    public List<ImportoRicalcolato> getImportiRicalcolatiPerMovimentazione(Long idMovimento158){

        String query = "select i " +
                "from ImportoRicalcolato i " +
                "where i.idMovimentoContabile=:idMovimento158 "+
                "order by i.dataOraInserimento desc";

        Query q = entityManager.createQuery(query).setParameter("idMovimento158", idMovimento158);

        List<ImportoRicalcolato> results = (List<ImportoRicalcolato>)q.getResultList();

        if (results != null && results.size()>0)
            return results;
        else
            results = new ArrayList<ImportoRicalcolato>();

        return results;

    }


    public List<MovimentoContabile> getMovimentazioniPerEvento(Long eventoId){

        String query = "select m " +
                       "from MovimentoContabile m " +
                       "where m.idEvento=:eventoId "+
                       "order by m.contabilita, m.voceIncasso, m.numeroFattura desc";

        Query q = entityManager.createQuery(query).setParameter("eventoId",eventoId);

        List<MovimentoContabile> results = (List<MovimentoContabile>)q.getResultList();

        if (results != null && results.size()>0) {
            List<ImportoRicalcolato> importiRicalcolati;
            for (int i=0; i<results.size(); i++){
                importiRicalcolati = getImportiRicalcolatiPerMovimentazione(results.get(i).getId());
                results.get(i).setImportiRicalcolati(importiRicalcolati);
            }
            return results;
        }else
            results = new ArrayList<MovimentoContabile>();

        return results;

    }

    public List<EventiPagati> getEvento(Long eventoId) {

        String query = "select e " +
                       "from EventiPagati e " +
                       "where e.idEvento=:eventoId "+
                       "order by e.contabilita, e.voceIncasso, e.numeroFattura desc";

        Query q = entityManager.createQuery(query).setParameter("eventoId",eventoId);

        List<EventiPagati> results = (List<EventiPagati>)q.getResultList();

        Manifestazione manifestazione = entityManager.find(Manifestazione.class, eventoId);

        List<MovimentoContabile> movimentazioniPM = getMovimentazioniPerEvento(eventoId);

        if (results!=null && results.size()>0 && manifestazione!=null){
             for (int i=0; i<results.size(); i++){
                 results.get(i).setManifestazione(manifestazione);
                 results.get(i).setMovimentazioniPM(movimentazioniPM);
             }
        }

        return results;

    }

    public List<ImportoRicalcolato> getImportiPMAssegnatiPerEvento(Long idEvento){

        List<ImportoRicalcolato> importiPMAssegnati = new ArrayList<ImportoRicalcolato>();
        ImportoRicalcolato importoPMAssegnati;

        // Importo di un evento già assegnato a PM rientrati, suddiviso per Voce Incasso
        String query = "select codice_voce_incasso,id_esecuzione_ricalcolo,data_inserimento,importo "+
                       "from "+
                       "(select m.codice_voce_incasso, i.id_esecuzione_ricalcolo, trunc(i.data_ora_inserimento) AS data_inserimento, sum(CASE WHEN tipo_documento_contabile='501' THEN i.importo ELSE -i.importo END) AS importo "+
                       "from importo_ricalcolato i, movimento_contabile_158 m "+
                       "where i.id_movimento_contabile=m.id_movimento_contabile_158 "+
                       "and m.id_evento=? "+
                       "group by m.codice_voce_incasso, i.id_esecuzione_ricalcolo, trunc(i.data_ora_inserimento)) "+
                       "order by id_esecuzione_ricalcolo desc ";

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, idEvento);

        List<Object[]> results = (List<Object[]>)q.getResultList();

        if (results != null && results.size() > 0) {

            for (int i = 0; i < results.size(); i++) {

                importoPMAssegnati = new ImportoRicalcolato();
                importoPMAssegnati.setVoceIncasso((String)results.get(i)[0]);
                importoPMAssegnati.setIdEsecuzioneRicalcolo(((BigDecimal)results.get(i)[1]).longValue());
                importoPMAssegnati.setDataOraInserimento((Date)results.get(i)[2]);
                importoPMAssegnati.setIdEvento(idEvento);
                importoPMAssegnati.setImporto(((BigDecimal)results.get(i)[3]).doubleValue());

                importiPMAssegnati.add(importoPMAssegnati);
            }
        }

        return importiPMAssegnati;
    }


    public List<ImportoRicalcolato> getImportiTotaliPerEvento(Long idEvento){

        List<ImportoRicalcolato> importiPMAssegnati = new ArrayList<ImportoRicalcolato>();
        ImportoRicalcolato importoPMAssegnati;

        // Importo di un evento totale per Voce Incasso
        String query = "select voce_incasso, sum(CASE WHEN tipo_documento_contabile='501' THEN importo_dem ELSE -importo_dem END) AS IMPORTO_DEM " +
                "        from eventi_pagati " +
                "        where id_evento=? " +
                "        group by voce_incasso";

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, idEvento);

        List<Object[]> results = (List<Object[]>)q.getResultList();

        if (results != null && results.size() > 0) {

            for (int i = 0; i < results.size(); i++) {

                importoPMAssegnati = new ImportoRicalcolato();
                importoPMAssegnati.setVoceIncasso((String)results.get(i)[0]);
                importoPMAssegnati.setImporto(((BigDecimal)results.get(i)[1]).doubleValue());

                importiPMAssegnati.add(importoPMAssegnati);
            }
        }

        return importiPMAssegnati;
    }


    public List<String> getTipoProgrammi(){

        List<String> tipoPMList=null;

        String query = "select distinct(p.tipoPm)" +
                "        from ProgrammaMusicale p " +
                "        order by p.tipoPm asc";

        Query q = entityManager.createQuery(query);

        tipoPMList = q.getResultList();

        return tipoPMList;

    }

    public MovimentoContabile getMovimentoContabile(Long idMovimento){

       return entityManager.find(MovimentoContabile.class, idMovimento);

    }

    public List<MovimentoContabile> getMovimentiContabile(Long idProgrammaMusicale){

        List<MovimentoContabile> listaMovimenti=null;

        String query = "select m " +
                "        from MovimentoContabile m " +
                "        where m.idProgrammaMusicale=:idProgrammaMusicale";

        Query q = entityManager.createQuery(query);

        q.setParameter("idProgrammaMusicale",idProgrammaMusicale);

        listaMovimenti = q.getResultList();

        if (listaMovimenti!=null && listaMovimenti.size()>0){
            List<ImportoRicalcolato> importiRicalcolati;
            for (int i=0; i<listaMovimenti.size(); i++){
                importiRicalcolati = getImportiRicalcolatiPerMovimentazione(listaMovimenti.get(i).getId());
                listaMovimenti.get(i).setImportiRicalcolati(importiRicalcolati);
            }
            return listaMovimenti;
        }else{
            return new ArrayList<MovimentoContabile>();
        }

    }

    public Manifestazione getManifestazione(Long idEvento){
         return entityManager.find(Manifestazione.class, idEvento);
    }

    public List<Manifestazione> getManifestazioni(List<MovimentoContabile> listaMovimenti){

        List<Manifestazione> listaEventi=new ArrayList<Manifestazione>();
        Map<String,String> eventiKey=new HashMap<String,String>();
        Manifestazione evento = null;
        if (listaMovimenti!=null) {
            for (MovimentoContabile movimento : listaMovimenti) {
                evento = getManifestazione(movimento.getIdEvento());
                if (eventiKey.get(evento.getId().toString())==null){
                    listaEventi.add(evento);
                    eventiKey.put(evento.getId().toString(), evento.getId().toString());
                }

            }
        }

        return listaEventi;

    }


    public ProgrammaMusicale getProgrammaMusicaleById(Long idProgrammaMusicale){

        ProgrammaMusicale programmaMusicale = entityManager.find(ProgrammaMusicale.class, idProgrammaMusicale);

        return programmaMusicale;

    }



    public List<Utilizzazione> getUtilizzazioni(Long idProgrammaMusicale){

        List<Utilizzazione> listaOpere=null;

        String query = "select u " +
                "        from Utilizzazione u " +
                "        where u.idProgrammaMusicale=:idProgrammaMusicale";

        Query q = entityManager.createQuery(query);

        q.setParameter("idProgrammaMusicale",idProgrammaMusicale);

        listaOpere = q.getResultList();

        if (listaOpere!=null && listaOpere.size()>0){
            return listaOpere;
        }else{
            return new ArrayList<Utilizzazione>();
        }

    }

    public DirettoreEsecuzione getDirettoreEsecuzione(Long idProgrammaMusicale){

        List<DirettoreEsecuzione> listaDirettoriEsecuzione=null;

        String query = "select d " +
                "        from DirettoreEsecuzione d " +
                "        where d.idProgrammaMusicale=:idProgrammaMusicale";

        Query q = entityManager.createQuery(query);

        q.setParameter("idProgrammaMusicale",idProgrammaMusicale);

        listaDirettoriEsecuzione = q.getResultList();

        if (listaDirettoriEsecuzione!=null && listaDirettoriEsecuzione.size()>0){
            return (DirettoreEsecuzione)listaDirettoriEsecuzione.get(0);
        }else{
            return null;
        }

    }

    public Cartella getCartellaById(Long idCartella){

        Cartella cartella = entityManager.find(Cartella.class, idCartella);

        String seprag = cartella.getSeprag();

        String query =  "select denominazione "+
                        "from circoscrizione "+
                        "where cod_seprag=?";

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, seprag);

        List<Object> results = (List<Object>)q.getResultList();

        if (results != null && results.size() > 0) {

            String denomminazione = (String)results.get(0);

            cartella.setDescrizionePuntoTerritoriale(denomminazione);
        }

        return cartella;

    }


    public ReportPage getListaMovimentazioni(Long inizioPeriodoContabile, Long finePeriodoContabile, Long evento, String fattura, Long reversale, String voceIncasso,
                                             Date dataInizioEvento,Date dataFineEvento, String seprag, String tipoDocumento, String locale, String organizzatore,
                                             String tipoSupporto, String tipoProgramma, String gruppoPrincipale, String titoloOpera, String numeroPM, String permesso,
                                             Integer page) {

        Character flagGruppoPrincipale=null;
        // Arriva come Y o N
        if (gruppoPrincipale != null && gruppoPrincipale.equals('Y'))
            flagGruppoPrincipale = '1';  // Gruppo Principale
        else
        if (gruppoPrincipale != null && gruppoPrincipale.equals('N'))
            flagGruppoPrincipale = '0';  // Gruppo Spalla

        String query = null;

        List<MovimentoContabile> listaEventi = new ArrayList<MovimentoContabile>();
        List<MovimentoContabile> listaAllEventi = new ArrayList<MovimentoContabile>();

        ReportPage reportPage = new ReportPage(listaEventi, listaEventi.size(), page);

        int recordCount=0;


        query = "select e.contabilita, e.voceIncasso, e.numeroFattura, e.reversale, e.idProgrammaMusicale, e.numProgrammaMusicale, e.tipoDocumentoContabile, e.numeroPermesso, e.importoTotDem, f.dataInizioEvento, f.dataFineEvento, f.oraInizioEvento, f.oraFineEvento, e.id, e.contabilitaOrg "+
                "from MovimentoContabile e, Manifestazione f ";

        if (seprag !=null)
            query = query + ", Cartella c ";

        if (tipoSupporto !=null || tipoProgramma !=null)
            query = query + ", ProgrammaMusicale p ";

        if (titoloOpera !=null)
            query = query + ", Utilizzazione u ";

        //if (dataInizioEvento !=null || dataFineEvento !=null || locale !=null || organizzatore !=null)
        //    query = query + ", Manifestazione f ";

        query = query + "where e.contabilita>=:inizioPeriodoContabile and e.contabilita<=:finePeriodoContabile "+
                        "and f.id = e.idEvento "+
                        "and (:idEvento is null or e.idEvento=:idEvento) "+
                        "and (:numeroFattura is null or e.numeroFattura=:numeroFattura) "+
                        "and (:reversale is null or e.reversale=:reversale) "+
                        "and (:voceIncasso is null or e.voceIncasso=:voceIncasso) "+
                        "and (:numeroPermesso is null or e.numeroPermesso=:numeroPermesso) "+
                        "and (:tipoDocumentoContabile is null or e.tipoDocumentoContabile=:tipoDocumentoContabile) "+
                        "and (:numProgrammaMusicale is null or e.numProgrammaMusicale=:numProgrammaMusicale) "+
                        "and (:flagGruppoPrincipale is null or e.flagGruppoPrincipale=:flagGruppoPrincipale) ";

        if (seprag !=null){
            query = query + "and c.id = e.idCartella and c.codiceSede=:codiceSede and c.codiceProvincia=:codiceProvincia and c.unitaTerritoriale=:unitaTerritoriale ";
        }

        if (tipoSupporto !=null || tipoProgramma !=null){
            query = query + "and p.id = e.idProgrammaMusicale "+
                            "and (:tipoSupporto is null or p.tipologiaPm=:tipoSupporto) "+
                            "and (:tipoProgramma is null or p.tipoPm=:tipoProgramma) ";
        }

        if (titoloOpera !=null){
            query = query + "and u.idProgrammaMusicale = e.idProgrammaMusicale "+
                            "and (:titoloOpera is null or upper(u.titoloComposizione) like upper(:titoloOpera))";
        }

        if (dataInizioEvento !=null || dataFineEvento !=null || locale !=null || organizzatore !=null) {
            query = query + "and (:dataInizioEvento is null or f.dataInizioEvento=:dataInizioEvento) " +
                            "and (:dataFineEvento is null or f.dataFineEvento=:dataFineEvento) " +
                            "and (:locale is null or upper(f.denominazioneLocale) like upper(:locale))" +
                            "and (:organizzatore is null or f.codiceSapOrganizzatore=:organizzatore) ";
        }

        query = query + "order by e.idEvento, e.numeroFattura, e.voceIncasso asc ";

        Query q = entityManager.createQuery(query);

        q.setParameter("inizioPeriodoContabile", inizioPeriodoContabile);
        q.setParameter("finePeriodoContabile", finePeriodoContabile);
        q.setParameter("idEvento", evento);
        if (fattura != null)
         q.setParameter("numeroFattura", Long.parseLong(fattura));
        else
         q.setParameter("numeroFattura", fattura);
        q.setParameter("reversale", reversale);
        q.setParameter("voceIncasso", voceIncasso);
        q.setParameter("numeroPermesso", permesso);
        q.setParameter("tipoDocumentoContabile", tipoDocumento);
        q.setParameter("numProgrammaMusicale", numeroPM);
        q.setParameter("flagGruppoPrincipale", flagGruppoPrincipale);

        if (seprag !=null){
            // 1205801
            // 12=codice sede
            // 058=codice provincia
            // 01=unita territoriale
            String sede = seprag;
            String provincia = seprag;
            String unitaTerritoriale = seprag;

            if (seprag.length()==7) {
                sede = seprag.substring(0, 2);
                provincia = seprag.substring(2, 5);
                unitaTerritoriale = seprag.substring(5, seprag.length());
            }

            q.setParameter("codiceSede", sede);
            q.setParameter("codiceProvincia", provincia);
            q.setParameter("unitaTerritoriale", unitaTerritoriale);

        }

        if (tipoSupporto !=null || tipoProgramma !=null){
            q.setParameter("tipoSupporto",tipoSupporto );
            q.setParameter("tipoProgramma",tipoProgramma );
        }

        if (titoloOpera !=null)
            q.setParameter("titoloOpera","%"+titoloOpera+"%");

        if (dataInizioEvento !=null || dataFineEvento !=null || locale !=null || organizzatore !=null) {
            q.setParameter("locale","%"+locale+"%");
            q.setParameter("organizzatore","%"+organizzatore+"%");
            q.setParameter("dataInizioEvento", dataInizioEvento);
            q.setParameter("dataFineEvento", dataFineEvento);
        }

        List<Object[]> results = (List<Object[]>)q.getResultList();

        if (results != null && results.size() > 0) {

            recordCount = results.size();

            int startRecord = 0;
            int endRecord = recordCount;

            if (page != -1) {

                startRecord = (page - 1) * ReportPage.NUMBER_RECORD_PER_PAGE;
                endRecord = 0;
                if (recordCount > ReportPage.NUMBER_RECORD_PER_PAGE) {
                    endRecord = startRecord + ReportPage.NUMBER_RECORD_PER_PAGE;
                    if (endRecord > recordCount)
                        endRecord = recordCount - 1;
                } else {
                    endRecord = recordCount;
                }

            }

            MovimentoContabile movimentoContabile = null;

            for (int i = 0; i < results.size(); i++) {

                //   e.contabilita, e.voceIncasso, e.numeroFattura, e.reversale,
                //   e.idProgrammaMusicale, e.numProgrammaMusicale, e.tipoDocumentoContabile, e.numeroPermesso,
                //   e.importoTotDem, f.dataInizioEvento, f.dataFineEvento, f.oraInizioEvento,
                //   f.oraFineEvento
                movimentoContabile = new MovimentoContabile();

                movimentoContabile.setContabilita(((Integer)results.get(i)[0]));
                movimentoContabile.setVoceIncasso(((String)results.get(i)[1]));
                movimentoContabile.setNumeroFattura(((Long)results.get(i)[2]));
                movimentoContabile.setReversale(((Long)results.get(i)[3]));
                movimentoContabile.setIdProgrammaMusicale(((Long)results.get(i)[4]));
                movimentoContabile.setNumProgrammaMusicale((String)results.get(i)[5]);
                movimentoContabile.setTipoDocumentoContabile(((String)results.get(i)[6]));
                movimentoContabile.setNumeroPermesso(((String)results.get(i)[7]));
                movimentoContabile.setImportoTotDem((Double)results.get(i)[8]);
                movimentoContabile.setId((Long)results.get(i)[13]);
                movimentoContabile.setContabilitaOrg(((Integer)results.get(i)[14]));

                Manifestazione manifestazione = new Manifestazione();
                manifestazione.setDataInizioEvento(((Date)results.get(i)[9]));
                manifestazione.setDataFineEvento(((Date)results.get(i)[10]));
                manifestazione.setOraInizioEvento(((String)results.get(i)[11]));
                manifestazione.setOraFineEvento(((String)results.get(i)[12]));

                movimentoContabile.setEvento(manifestazione);

                if (i>=startRecord && i<endRecord)
                 listaEventi.add(movimentoContabile);

                listaAllEventi.add(movimentoContabile);
            }

            //reportPage = new ReportPage(listaEventi, recordCount, page);
            reportPage = new ReportPage(listaAllEventi, listaEventi, recordCount, page);

        }

        return reportPage;
    }



    public ReportPage getListaAggregatiSiada(Long inizioPeriodoContabile, Long finePeriodoContabile, String voceIncasso, String tipoSupporto, String tipoProgramma, Long numeroPM, Integer page){

        String query = null;

        List<MovimentoSiada> listaAggregati = new ArrayList<MovimentoSiada>();
        List<MovimentoSiada> listaAllAggregati = new ArrayList<MovimentoSiada>();

        ReportPage reportPage = new ReportPage(listaAggregati, listaAggregati.size(), page);

        int recordCount=0;

        query = "select e.id, e.idEvento, e.idProgrammaMusicale, e.dataInizioEvento, e.dataFineEvento, e.voceIncasso, e.numeroFattura,e.importo, e.meseContabile, e.meseInoltroSiada, p.numeroProgrammaMusicale, e.meseContabileOrig, e.dataElaborazione, e.voceIncassoSiada "+
                "from MovimentoSiada e, ProgrammaMusicale p ";

        query = query + "where e.meseContabile>=:inizioPeriodoContabile and e.meseContabile<=:finePeriodoContabile "+
                         "and (:voceIncasso is null or e.voceIncasso=:voceIncasso) "+
                         "and p.id = e.idProgrammaMusicale "+
                         "and (:tipoSupporto is null or p.tipologiaPm=:tipoSupporto) "+
                         "and (:numProgrammaMusicale is null or p.numeroProgrammaMusicale=:numProgrammaMusicale) "+
                         "and (:tipoProgramma is null or p.tipoPm=:tipoProgramma) ";

        query = query + "order by e.idProgrammaMusicale, e.voceIncasso asc ";

        Query q = entityManager.createQuery(query);

        q.setParameter("inizioPeriodoContabile", inizioPeriodoContabile);
        q.setParameter("finePeriodoContabile", finePeriodoContabile);
        q.setParameter("voceIncasso", voceIncasso);
        q.setParameter("tipoSupporto",tipoSupporto );
        q.setParameter("tipoProgramma",tipoProgramma );
        q.setParameter("numProgrammaMusicale", numeroPM);


        List<Object[]> results = (List<Object[]>)q.getResultList();

        if (results != null && results.size() > 0) {

            recordCount = results.size();

            int startRecord = 0;
            int endRecord = recordCount;

            if (page != -1) {

                startRecord = (page - 1) * ReportPage.NUMBER_RECORD_PER_PAGE;
                endRecord = 0;
                if (recordCount > ReportPage.NUMBER_RECORD_PER_PAGE) {
                    endRecord = startRecord + ReportPage.NUMBER_RECORD_PER_PAGE;
                    if (endRecord > recordCount)
                        endRecord = recordCount - 1;
                } else {
                    endRecord = recordCount;
                }

            }

            MovimentoSiada aggregatoSiada = null;

            for (int i = 0; i < recordCount; i++) {

                // e.id, e.idEvento, e.idProgrammaMusicale, e.dataInizioEvento, e.dataFineEvento, e.voceIncasso, e.numeroFattura,e.importo, e.meseContabile, e.meseInoltroSiada, p.numeroProgrammaMusicale
                aggregatoSiada = new MovimentoSiada();
                aggregatoSiada.setId((Long)results.get(i)[0]);
                aggregatoSiada.setIdEvento((Long)results.get(i)[1]);
                aggregatoSiada.setIdProgrammaMusicale((Long)results.get(i)[2]);
                aggregatoSiada.setDataInizioEvento((Date)results.get(i)[3]);
                aggregatoSiada.setDataFineEvento((Date)results.get(i)[4]);
                aggregatoSiada.setVoceIncasso((String)results.get(i)[5]);
                aggregatoSiada.setNumeroFattura((Long)results.get(i)[6]);
                aggregatoSiada.setImporto((Double)results.get(i)[7]);
                aggregatoSiada.setMeseContabile((Integer)results.get(i)[8]);
                aggregatoSiada.setMeseInoltroSiada((Integer)results.get(i)[9]);
                aggregatoSiada.setNumeroProgrammaMusicale((Long)results.get(i)[10]);
                aggregatoSiada.setMeseContabileOrig((Integer)results.get(i)[11]);
                aggregatoSiada.setDataElaborazione((Date)results.get(i)[12]);
                aggregatoSiada.setVoceIncassoSiada((String)results.get(i)[13]);
                //aggregatoSiada = (MovimentoSiada)results.get(i);

                if (i >= startRecord && i<endRecord)
                 listaAggregati.add(aggregatoSiada);

                listaAllAggregati.add(aggregatoSiada);
            }

            // Ordina la lista aggregati per voce di incasso
            reportPage = new ReportPage(listaAllAggregati, listaAggregati, recordCount, page);

        }

        return reportPage;

    }


    public ReportPage getListaAggregatiSiadaSMC(Long inizioPeriodoContabile, Long finePeriodoContabile, String voceIncasso, String tipoSupporto, String tipoProgramma, Long numeroPM, Integer page){

        String query = null;

        List<MovimentoSiada2243> listaAggregati = new ArrayList<MovimentoSiada2243>();
        List<MovimentoSiada2243> listaAllAggregati = new ArrayList<MovimentoSiada2243>();

        ReportPage reportPage = new ReportPage(listaAggregati, listaAggregati.size(), page);

        int recordCount=0;

        query = "select e.id, e.idEvento, e.idProgrammaMusicale, e.dataInizioEvento, e.dataFineEvento, e.voceIncasso, e.numeroFattura,e.importo, e.meseContabile, e.meseInoltroSiada, p.numeroProgrammaMusicale, e.meseContabileOrig, e.dataElaborazione, e.voceIncassoSiada "+
                "from MovimentoSiada2243 e, ProgrammaMusicale p ";

        query = query + "where e.meseContabile>=:inizioPeriodoContabile and e.meseContabile<=:finePeriodoContabile "+
                "and (:voceIncasso is null or e.voceIncasso=:voceIncasso) "+
                "and p.id = e.idProgrammaMusicale "+
                "and (:tipoSupporto is null or p.tipologiaPm=:tipoSupporto) "+
                "and (:numProgrammaMusicale is null or p.numeroProgrammaMusicale=:numProgrammaMusicale) "+
                "and (:tipoProgramma is null or p.tipoPm=:tipoProgramma) ";

        query = query + "order by e.idProgrammaMusicale, e.voceIncasso asc ";

        Query q = entityManager.createQuery(query);

        q.setParameter("inizioPeriodoContabile", inizioPeriodoContabile);
        q.setParameter("finePeriodoContabile", finePeriodoContabile);
        q.setParameter("voceIncasso", voceIncasso);
        q.setParameter("tipoSupporto",tipoSupporto );
        q.setParameter("tipoProgramma",tipoProgramma );
        q.setParameter("numProgrammaMusicale", numeroPM);


        List<Object[]> results = (List<Object[]>)q.getResultList();

        if (results != null && results.size() > 0) {

            recordCount = results.size();

            int startRecord = 0;
            int endRecord = recordCount;

            if (page != -1) {

                startRecord = (page - 1) * ReportPage.NUMBER_RECORD_PER_PAGE;
                endRecord = 0;
                if (recordCount > ReportPage.NUMBER_RECORD_PER_PAGE) {
                    endRecord = startRecord + ReportPage.NUMBER_RECORD_PER_PAGE;
                    if (endRecord > recordCount)
                        endRecord = recordCount - 1;
                } else {
                    endRecord = recordCount;
                }

            }

            MovimentoSiada2243 aggregatoSiada = null;

            for (int i = 0; i < recordCount; i++) {

                // e.id, e.idEvento, e.idProgrammaMusicale, e.dataInizioEvento, e.dataFineEvento, e.voceIncasso, e.numeroFattura,e.importo, e.meseContabile, e.meseInoltroSiada, p.numeroProgrammaMusicale
                aggregatoSiada = new MovimentoSiada2243();
                aggregatoSiada.setId((Long)results.get(i)[0]);
                aggregatoSiada.setIdEvento((Long)results.get(i)[1]);
                aggregatoSiada.setIdProgrammaMusicale((Long)results.get(i)[2]);
                aggregatoSiada.setDataInizioEvento((Date)results.get(i)[3]);
                aggregatoSiada.setDataFineEvento((Date)results.get(i)[4]);
                aggregatoSiada.setVoceIncasso((String)results.get(i)[5]);
                aggregatoSiada.setNumeroFattura((Long)results.get(i)[6]);
                aggregatoSiada.setImporto((Double)results.get(i)[7]);
                aggregatoSiada.setMeseContabile((Integer)results.get(i)[8]);
                aggregatoSiada.setMeseInoltroSiada((Integer)results.get(i)[9]);
                aggregatoSiada.setNumeroProgrammaMusicale((Long)results.get(i)[10]);
                aggregatoSiada.setMeseContabileOrig((Integer)results.get(i)[11]);
                aggregatoSiada.setDataElaborazione((Date)results.get(i)[12]);
                aggregatoSiada.setVoceIncassoSiada((String)results.get(i)[13]);
                //aggregatoSiada = (MovimentoSiada)results.get(i);

                if (i >= startRecord && i<endRecord)
                    listaAggregati.add(aggregatoSiada);

                listaAllAggregati.add(aggregatoSiada);
            }

            // Ordina la lista aggregati per voce di incasso
            reportPage = new ReportPage(listaAllAggregati, listaAggregati, recordCount, page);

        }

        return reportPage;

    }




    public List<AggregatoVoce> getListaDocumentiEventi(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale){

        String query = null;

        List<AggregatoVoce> listaAggregati = new ArrayList<AggregatoVoce>();

        query = "select voce_incasso, tipo_documento_contabile AS TIPO_DOCUMENTO, count(tipo_documento_contabile) AS NUMERO_DOCUMENTI, sum(decode(tipo_documento_contabile,'501',importo_dem,-importo_dem)) as IMPORTO, count(distinct(id_evento)) as NUMERO_EVENTI "+
                "from eventi_pagati "+
                "where CONTABILITA>=? and contabilita<=? ";

        if (idPuntoTerritoriale!=null)
            query = query + "and seprag=? ";

        //query = query + "group by voce_incasso, tipo_documento_contabile ";

        query = query + "group by voce_incasso, tipo_documento_contabile "+
                "order by tipo_documento_contabile desc ";

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, inizioPeriodoContabile);
        q.setParameter(2, finePeriodoContabile);

        if (idPuntoTerritoriale!=null)
            q.setParameter(3, idPuntoTerritoriale);

        List<Object[]> results = (List<Object[]>)q.getResultList();


        if (results != null && results.size() > 0) {

            AggregatoVoce aggregatoVoce = null;

            for (int i = 0; i < results.size(); i++) {

                aggregatoVoce = new AggregatoVoce();
                aggregatoVoce.setVoceIncasso((String)results.get(i)[0]);
                aggregatoVoce.setTipoDocumento((String)results.get(i)[1]);
                aggregatoVoce.setNumeroDocumenti(((BigDecimal)results.get(i)[2]).longValue());
                aggregatoVoce.setImporto(((BigDecimal)results.get(i)[3]).doubleValue());
                aggregatoVoce.setNumeroEventi(((BigDecimal)results.get(i)[4]).longValue());

                listaAggregati.add(aggregatoVoce);
            }

        }

        return listaAggregati;

    }

    public List<AggregatoVoce> getListaEventi(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale){

        String query = null;

        List<AggregatoVoce> listaAggregati = new ArrayList<AggregatoVoce>();

        query = "select voce_incasso, count(distinct(id_evento)) as NUMERO_EVENTI "+
                "from eventi_pagati "+
                "where CONTABILITA>=? and contabilita<=? ";

        if (idPuntoTerritoriale!=null)
            query = query + "and seprag=? ";

        query = query + "group by voce_incasso";

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, inizioPeriodoContabile);
        q.setParameter(2, finePeriodoContabile);

        if (idPuntoTerritoriale!=null)
            q.setParameter(3, idPuntoTerritoriale);

        List<Object[]> results = (List<Object[]>)q.getResultList();

        if (results != null && results.size() > 0) {

            AggregatoVoce aggregatoVoce = null;

            for (int i = 0; i < results.size(); i++) {

                aggregatoVoce = new AggregatoVoce();
                aggregatoVoce.setVoceIncasso((String)results.get(i)[0]);
                aggregatoVoce.setNumeroEventi(((BigDecimal)results.get(i)[1]).longValue());

                listaAggregati.add(aggregatoVoce);
            }

        }

        return listaAggregati;

    }

    public List<AggregatoVoce>  getImportoNettiPMCorrenti(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale){

        // PM Correnti: 501-201 (la sottrazione degli assegnati a DG è automatica perchè la join tra le due tabelle per il periodo di riferimento non fornisce nulla)
        //         null                  501
        //         Liquidazione          501
        //         Storno                221
        String query = null;

        List<AggregatoVoce> listaAggregati = new ArrayList<AggregatoVoce>();

        if (idPuntoTerritoriale != null){

            query = "select p.voce_incasso, sum(decode(m.tipo_documento_contabile,'501',p.importo_dem,-p.importo_dem)) as IMPORTO " +
                    "from movimento_contabile_158 m, eventi_pagati p, cartella c " +
                    "where m.codice_voce_incasso=p.voce_incasso " +
                    "and m.id_evento=p.id_evento " +
                    "and m.ID_CARTELLA=c.ID_CARTELLA "+
                    "and m.numero_fattura=p.numero_fattura " +
                    "and m.contabilita>=? and m.contabilita<=? " +
                    "and p.contabilita>=? and p.contabilita<=? "+
                    "and (c.CODICE_SEDE||c.CODICE_PROVINCIA||c.UNITA_TERRITORIALE=?) "+
                    "group by p.voce_incasso";

        } else {

            query = "select p.voce_incasso, sum(decode(m.tipo_documento_contabile,'501',p.importo_dem,-p.importo_dem)) as IMPORTO " +
                    "from movimento_contabile_158 m, eventi_pagati p " +
                    "where m.codice_voce_incasso=p.voce_incasso " +
                    "and m.id_evento=p.id_evento " +
                    "and m.numero_fattura=p.numero_fattura " +
                    "and m.contabilita>=? and m.contabilita<=? " +
                    "and p.contabilita>=? and p.contabilita<=? "+
                    "group by p.voce_incasso";
        }


        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, inizioPeriodoContabile);
        q.setParameter(2, finePeriodoContabile);
        q.setParameter(3, inizioPeriodoContabile);
        q.setParameter(4, finePeriodoContabile);

        if (idPuntoTerritoriale!=null)
          q.setParameter(5, idPuntoTerritoriale);

        List<Object[]> results = (List<Object[]>)q.getResultList();

        if (results != null && results.size() > 0) {

            AggregatoVoce aggregatoVoce = null;

            for (int i = 0; i < results.size(); i++) {

                aggregatoVoce = new AggregatoVoce();
                aggregatoVoce.setVoceIncasso((String)results.get(i)[0]);
                aggregatoVoce.setImporto(((BigDecimal)results.get(i)[1]).doubleValue());

                listaAggregati.add(aggregatoVoce);
            }

        }

        return listaAggregati;

    }

    // Importo Programmato effettivo per Voce Incasso
    public List<AggregatoVoce> getImportoProgrammatoCorrenti(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale){


        String query = null;

        List<AggregatoVoce> listaAggregati = new ArrayList<AggregatoVoce>();

        if (idPuntoTerritoriale != null){

            query = "select m.codice_voce_incasso, sum(importo) as IMPORTO "+
                    "from movimenti_siada m, movimento_contabile_158 mo, cartella c "+
                    "where m.id_programma_musicale=mo.id_programma_musicale "+
                    "and mo.ID_CARTELLA=c.ID_CARTELLA "+
                    "and m.mese_contabile>=? and m.mese_contabile<=? "+
                    "and (c.CODICE_SEDE||c.CODICE_PROVINCIA||c.UNITA_TERRITORIALE=?) "+
                    "group by m.codice_voce_incasso";
        } else {

            query = "select m.codice_voce_incasso, sum(importo) as IMPORTO "+
                    "from movimenti_siada m, movimento_contabile_158 mo "+
                    "where m.id_programma_musicale=mo.id_programma_musicale "+
                    "and m.mese_contabile>=? and m.mese_contabile<=? "+
                    "group by m.codice_voce_incasso";
        }


        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, inizioPeriodoContabile);
        q.setParameter(2, finePeriodoContabile);

        if (idPuntoTerritoriale!=null)
            q.setParameter(3, idPuntoTerritoriale);

        List<Object[]> results = (List<Object[]>)q.getResultList();

        if (results != null && results.size() > 0) {

            AggregatoVoce aggregatoVoce = null;

            for (int i = 0; i < results.size(); i++) {

                aggregatoVoce = new AggregatoVoce();
                aggregatoVoce.setVoceIncasso((String)results.get(i)[0]);
                aggregatoVoce.setImportoProgrammato(((BigDecimal)results.get(i)[1]).doubleValue());

                listaAggregati.add(aggregatoVoce);
            }

        }

        return listaAggregati;

    }


    // Numero PM e Numero Cedole per Voce Incasso
    public List<AggregatoVoce> getNumeroPMCorrenti(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale){

        String query = null;

        List<AggregatoVoce> listaAggregati = new ArrayList<AggregatoVoce>();

        if (idPuntoTerritoriale != null){

            query = "select m.codice_voce_incasso, count(distinct(p.NUMERO_PROGRAMMA_MUSICALE)), sum(m.importo) "+
                    "from movimenti_siada m, movimento_contabile_158 mo, programma_musicale p, cartella c "+
                    "where m.ID_PROGRAMMA_MUSICALE=p.ID_PROGRAMMA_MUSICALE "+
                    "and m.ID_PROGRAMMA_MUSICALE=mo.ID_PROGRAMMA_MUSICALE "+
                    "and mo.ID_CARTELLA=c.ID_CARTELLA "+
                    "and m.mese_contabile>=? and m.mese_contabile<=? "+
                    "and (c.CODICE_SEDE||c.CODICE_PROVINCIA||c.UNITA_TERRITORIALE=?) "+
                    "group by m.codice_voce_incasso ";

        } else {

            query = "select m.codice_voce_incasso, count(distinct(p.NUMERO_PROGRAMMA_MUSICALE)), sum(m.importo) "+
                    "from movimenti_siada m, programma_musicale p "+
                    "where m.ID_PROGRAMMA_MUSICALE=p.ID_PROGRAMMA_MUSICALE "+
                    "and m.mese_contabile>=? and m.mese_contabile<=? "+
                    "group by m.codice_voce_incasso ";
        }


        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, inizioPeriodoContabile);
        q.setParameter(2, finePeriodoContabile);

        if (idPuntoTerritoriale!=null)
            q.setParameter(3, idPuntoTerritoriale);

        List<Object[]> results = (List<Object[]>)q.getResultList();

        if (results != null && results.size() > 0) {

            AggregatoVoce aggregatoVoce = null;

            for (int i = 0; i < results.size(); i++) {

                aggregatoVoce = new AggregatoVoce();
                aggregatoVoce.setVoceIncasso((String)results.get(i)[0]);
                aggregatoVoce.setNumeroPM(((BigDecimal)results.get(i)[1]).longValue());
                aggregatoVoce.setImporto(((BigDecimal)results.get(i)[2]).doubleValue());

                listaAggregati.add(aggregatoVoce);
            }

        }

        return listaAggregati;

    }

    // Numero PM e Numero Cedole per Voce Incasso
    public List<AggregatoVoce> getNumeroCedoleCorrenti(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale){

        String query = null;

        List<AggregatoVoce> listaAggregati = new ArrayList<AggregatoVoce>();

        if (idPuntoTerritoriale != null){

            query = "select m.codice_voce_incasso, count(distinct(p.NUMERO_PROGRAMMA_MUSICALE)), count(u.TITOLO_COMPOSIZIONE) "+
                    "from movimenti_siada m, movimento_contabile_158 mo, programma_musicale p, utilizzazione u, cartella c "+
                    "where m.ID_PROGRAMMA_MUSICALE=p.ID_PROGRAMMA_MUSICALE "+
                    "and m.ID_PROGRAMMA_MUSICALE=mo.ID_PROGRAMMA_MUSICALE "+
                    "and p.ID_PROGRAMMA_MUSICALE=u.ID_PROGRAMMA_MUSICALE "+
                    "and mo.ID_CARTELLA=c.ID_CARTELLA "+
                    "and m.mese_contabile>=? and m.mese_contabile<=? "+
                    "and (c.CODICE_SEDE||c.CODICE_PROVINCIA||c.UNITA_TERRITORIALE=?) "+
                    "group by m.codice_voce_incasso ";

        } else {

            query = "select m.codice_voce_incasso, count(distinct(p.NUMERO_PROGRAMMA_MUSICALE)), count(u.TITOLO_COMPOSIZIONE) "+
                    "from movimenti_siada m, programma_musicale p, utilizzazione u "+
                    "where m.ID_PROGRAMMA_MUSICALE=p.ID_PROGRAMMA_MUSICALE "+
                    "and p.ID_PROGRAMMA_MUSICALE=u.ID_PROGRAMMA_MUSICALE "+
                    "and m.mese_contabile>=? and m.mese_contabile<=? "+
                    "group by m.codice_voce_incasso ";
        }


        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, inizioPeriodoContabile);
        q.setParameter(2, finePeriodoContabile);

        if (idPuntoTerritoriale!=null)
            q.setParameter(3, idPuntoTerritoriale);

        List<Object[]> results = (List<Object[]>)q.getResultList();

        if (results != null && results.size() > 0) {

            AggregatoVoce aggregatoVoce = null;

            for (int i = 0; i < results.size(); i++) {

                aggregatoVoce = new AggregatoVoce();
                aggregatoVoce.setVoceIncasso((String)results.get(i)[0]);
                aggregatoVoce.setNumeroPM(((BigDecimal)results.get(i)[1]).longValue());
                aggregatoVoce.setNumeroCedole(((BigDecimal)results.get(i)[2]).longValue());

                listaAggregati.add(aggregatoVoce);
            }

        }

        return listaAggregati;

    }


    public List<AggregatoVoce>  getImportoNettiPMArretrati(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale){

        String query = null;

        List<AggregatoVoce> listaAggregati = new ArrayList<AggregatoVoce>();

        if (idPuntoTerritoriale != null){

            query = "select codice_voce_incasso, sum(importo) "+
                    "from movimenti_siada s "+
                    "where id_programma_musicale in ( "+
                    "    select m.id_programma_musicale "+
                    "    from eventi_pagati p, movimento_contabile_158 m, cartella c "+
                    "    where m.codice_voce_incasso=p.voce_incasso "+
                    "    and m.id_evento=p.id_evento "+
                    "    and m.numero_fattura=p.numero_fattura "+
                    "    and m.contabilita>=? and m.contabilita<=? "+
                    "    and p.contabilita<? "+
                    "    and m.ID_CARTELLA=c.ID_CARTELLA "+
                    "    and (c.CODICE_SEDE||c.CODICE_PROVINCIA||c.UNITA_TERRITORIALE=?) "+
                    ") "+
                    "group by codice_voce_incasso ";
        } else {

            query = "select codice_voce_incasso, sum(importo) "+
                    "from movimenti_siada s "+
                    "where id_programma_musicale in ( "+
                    "    select m.id_programma_musicale "+
                    "    from eventi_pagati p, movimento_contabile_158 m "+
                    "    where m.codice_voce_incasso=p.voce_incasso "+
                    "    and m.id_evento=p.id_evento "+
                    "    and m.numero_fattura=p.numero_fattura "+
                    "    and m.contabilita>=? and m.contabilita<=? "+
                    "    and p.contabilita<? "+
                    ") "+
                    "group by codice_voce_incasso ";
        }

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, inizioPeriodoContabile);
        q.setParameter(2, finePeriodoContabile);
        q.setParameter(3, inizioPeriodoContabile);

        if (idPuntoTerritoriale!=null)
            q.setParameter(4, idPuntoTerritoriale);

        List<Object[]> results = (List<Object[]>)q.getResultList();

        if (results != null && results.size() > 0) {

            AggregatoVoce aggregatoVoce = null;

            for (int i = 0; i < results.size(); i++) {

                aggregatoVoce = new AggregatoVoce();
                aggregatoVoce.setVoceIncasso((String)results.get(i)[0]);
                aggregatoVoce.setImportoProgrammato(((BigDecimal)results.get(i)[1]).doubleValue());

                listaAggregati.add(aggregatoVoce);
            }

        }

        return listaAggregati;

    }


    public List<AggregatoVoce> getNumeroPMArretrati(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale){

        String query = null;

        List<AggregatoVoce> listaAggregati = new ArrayList<AggregatoVoce>();

        if (idPuntoTerritoriale != null){

            query = "select m.codice_voce_incasso, count(distinct(p.NUMERO_PROGRAMMA_MUSICALE)), sum(m.importo) "+
                    "from movimenti_siada m, movimento_contabile_158 mo, programma_musicale p, cartella c, "+
                    " ( "+
                    "        select m.id_programma_musicale "+
                    "        from eventi_pagati p, movimento_contabile_158 m "+
                    "        where m.codice_voce_incasso=p.voce_incasso "+
                    "        and m.id_evento=p.id_evento "+
                    "        and m.numero_fattura=p.numero_fattura "+
                    "        and m.contabilita>=? and m.contabilita<=? "+
                    "        and p.contabilita<? "+
                    ") a  "+
                    "where m.ID_PROGRAMMA_MUSICALE=p.ID_PROGRAMMA_MUSICALE "+
                    "and p.ID_PROGRAMMA_MUSICALE=a.ID_PROGRAMMA_MUSICALE "+
                    "and m.ID_PROGRAMMA_MUSICALE=mo.ID_PROGRAMMA_MUSICALE "+
                    "and mo.ID_CARTELLA=c.ID_CARTELLA "+
                    "and m.mese_contabile>=? and m.mese_contabile<=? "+
                    "and (c.CODICE_SEDE||c.CODICE_PROVINCIA||c.UNITA_TERRITORIALE=?) "+
                    "group by m.codice_voce_incasso ";

        } else {

            query = "select m.codice_voce_incasso, count(distinct(p.NUMERO_PROGRAMMA_MUSICALE)), sum(m.importo) "+
                    "from movimenti_siada m, programma_musicale p, "+
                    "( "+
                    " select m.id_programma_musicale  "+
                    " from eventi_pagati p, movimento_contabile_158 m "+
                    " where m.codice_voce_incasso=p.voce_incasso  "+
                    " and m.id_evento=p.id_evento "+
                    " and m.numero_fattura=p.numero_fattura "+
                    " and m.contabilita>=? and m.contabilita<=? "+
                    " and p.contabilita<? ) a "+
                    "where m.ID_PROGRAMMA_MUSICALE=p.ID_PROGRAMMA_MUSICALE "+
                    "and p.ID_PROGRAMMA_MUSICALE=a.ID_PROGRAMMA_MUSICALE "+
                    "and m.mese_contabile>=? and m.mese_contabile<=? "+
                    "group by m.codice_voce_incasso ";
        }


        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, inizioPeriodoContabile);
        q.setParameter(2, finePeriodoContabile);
        q.setParameter(3, inizioPeriodoContabile);
        q.setParameter(4, inizioPeriodoContabile);
        q.setParameter(5, finePeriodoContabile);

        if (idPuntoTerritoriale!=null)
          q.setParameter(6, idPuntoTerritoriale);

        List<Object[]> results = (List<Object[]>)q.getResultList();

        if (results != null && results.size() > 0) {

            AggregatoVoce aggregatoVoce = null;

            for (int i = 0; i < results.size(); i++) {

                aggregatoVoce = new AggregatoVoce();
                aggregatoVoce.setVoceIncasso((String)results.get(i)[0]);
                aggregatoVoce.setNumeroPM(((BigDecimal)results.get(i)[1]).longValue());
                aggregatoVoce.setImporto(((BigDecimal)results.get(i)[2]).doubleValue());

                listaAggregati.add(aggregatoVoce);
            }

        }

        return listaAggregati;

    }


    public List<AggregatoVoce> getNumeroCedoleArretrati(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale){

        String query = null;

        List<AggregatoVoce> listaAggregati = new ArrayList<AggregatoVoce>();

        if (idPuntoTerritoriale != null){

            query = "select m.codice_voce_incasso, count(distinct(p.NUMERO_PROGRAMMA_MUSICALE)), count(u.TITOLO_COMPOSIZIONE) "+
                    "from movimenti_siada m, movimento_contabile_158 mo, programma_musicale p, utilizzazione u, cartella c, "+
                    " ( "+
                    "        select m.id_programma_musicale "+
                    "        from eventi_pagati p, movimento_contabile_158 m "+
                    "        where m.codice_voce_incasso=p.voce_incasso "+
                    "        and m.id_evento=p.id_evento "+
                    "        and m.numero_fattura=p.numero_fattura "+
                    "        and m.contabilita>=? and m.contabilita<=? "+
                    "        and p.contabilita<? "+
                    ") a  "+
                    "where m.ID_PROGRAMMA_MUSICALE=p.ID_PROGRAMMA_MUSICALE "+
                    "and p.ID_PROGRAMMA_MUSICALE=u.ID_PROGRAMMA_MUSICALE "+
                    "and p.ID_PROGRAMMA_MUSICALE=a.ID_PROGRAMMA_MUSICALE "+
                    "and m.ID_PROGRAMMA_MUSICALE=mo.ID_PROGRAMMA_MUSICALE "+
                    "and mo.ID_CARTELLA=c.ID_CARTELLA "+
                    "and m.mese_contabile>=? and m.mese_contabile<=? "+
                    "and (c.CODICE_SEDE||c.CODICE_PROVINCIA||c.UNITA_TERRITORIALE=?) "+
                    "group by m.codice_voce_incasso ";

        } else {

            query = "select m.codice_voce_incasso, count(distinct(p.NUMERO_PROGRAMMA_MUSICALE)), count(u.TITOLO_COMPOSIZIONE)"+
                    "from movimenti_siada m, programma_musicale p, utilizzazione u, "+
                    "( "+
                    " select m.id_programma_musicale  "+
                    " from eventi_pagati p, movimento_contabile_158 m "+
                    " where m.codice_voce_incasso=p.voce_incasso  "+
                    " and m.id_evento=p.id_evento "+
                    " and m.numero_fattura=p.numero_fattura "+
                    " and m.contabilita>=? and m.contabilita<=? "+
                    " and p.contabilita<? ) a "+
                    "where m.ID_PROGRAMMA_MUSICALE=p.ID_PROGRAMMA_MUSICALE "+
                    "and p.ID_PROGRAMMA_MUSICALE=u.ID_PROGRAMMA_MUSICALE "+
                    "and p.ID_PROGRAMMA_MUSICALE=a.ID_PROGRAMMA_MUSICALE "+
                    "and m.mese_contabile>=? and m.mese_contabile<=? "+
                    "group by m.codice_voce_incasso ";
        }


        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, inizioPeriodoContabile);
        q.setParameter(2, finePeriodoContabile);
        q.setParameter(3, inizioPeriodoContabile);
        q.setParameter(4, inizioPeriodoContabile);
        q.setParameter(5, finePeriodoContabile);

        if (idPuntoTerritoriale!=null)
            q.setParameter(6, idPuntoTerritoriale);

        List<Object[]> results = (List<Object[]>)q.getResultList();

        if (results != null && results.size() > 0) {

            AggregatoVoce aggregatoVoce = null;

            for (int i = 0; i < results.size(); i++) {

                aggregatoVoce = new AggregatoVoce();
                aggregatoVoce.setVoceIncasso((String)results.get(i)[0]);
                aggregatoVoce.setNumeroPM(((BigDecimal)results.get(i)[1]).longValue());
                aggregatoVoce.setNumeroCedole(((BigDecimal)results.get(i)[2]).longValue());

                listaAggregati.add(aggregatoVoce);
            }

        }

        return listaAggregati;

    }


    public Double getImportoPMSUNAggregato(Long idProgrammaMusicale){

        Double importo = 0D;
        String query = null;

        // Importo SUN totale
        query = "select sum(decode(m.tipo_documento_contabile,'501',m.importo_dem_totale,-m.importo_dem_totale)) as IMPORTO "+
                "from MOVIMENTO_CONTABILE_158 m "+
                "where ID_PROGRAMMA_MUSICALE=? ";

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, idProgrammaMusicale);

        List<Object> results = (List<Object>)q.getResultList();

        if (results != null && results.size() > 0) {

            importo = ((BigDecimal)results.get(0)).doubleValue();
        }

        return importo;

    }


    public Double getImportoPMSIADAAggregato(Long idProgrammaMusicale){

        Double importo = null;
        String query = null;

        // Importo aggregato e inviato a SIADA oppure l'ultimo ricalcolato
        query = "select IMPORTO "+
                "from movimenti_siada "+
                "where ID_PROGRAMMA_MUSICALE=? ";

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, idProgrammaMusicale);

        List<Object> results = (List<Object>)q.getResultList();

        if (results != null && results.size() > 0) {

            importo = ((BigDecimal)results.get(0)).doubleValue();
        }

        return importo;

    }

    public List<ImportoRicalcolato> getImportiPMRicalcolati(Long idProgrammaMusicale){

        String query = null;

        List<ImportoRicalcolato> listaImportiRicalcolati = new ArrayList<ImportoRicalcolato>();

        // Importo ricalcolato (da considerare se non esiste ancora l'aggregato)
        query = "select ID_ESECUZIONE_RICALCOLO, ID_PROGRAMMA_MUSICALE, IMPORTO "+
                "from ( "+
                //"select i.ID_ESECUZIONE_RICALCOLO, m.ID_PROGRAMMA_MUSICALE, SUM(i.IMPORTO) as IMPORTO "+
                "select i.ID_ESECUZIONE_RICALCOLO, m.ID_PROGRAMMA_MUSICALE, "+
                "sum(decode(m.TIPO_DOCUMENTO_CONTABILE,'501', i.IMPORTO, '221',-i.IMPORTO)) as IMPORTO "+
                "from IMPORTO_RICALCOLATO i, MOVIMENTO_CONTABILE_158 m  "+
                "where i.ID_MOVIMENTO_CONTABILE=m.ID_MOVIMENTO_CONTABILE_158 "+
                "and m.ID_PROGRAMMA_MUSICALE=? "+
                "group by i.ID_ESECUZIONE_RICALCOLO, m.ID_PROGRAMMA_MUSICALE "+
                "union "+
                //"select i.ID_ESECUZIONE_RICALCOLO, m.ID_PROGRAMMA_MUSICALE, SUM(m.IMPORTO_DEM_TOTALE) as IMPORTO "+
                "select i.ID_ESECUZIONE_RICALCOLO, m.ID_PROGRAMMA_MUSICALE, "+
                "sum(decode(m.TIPO_DOCUMENTO_CONTABILE,'501', m.IMPORTO_DEM_TOTALE, '221',-m.IMPORTO_DEM_TOTALE)) as IMPORTO "+
                "from SOSPESI_RICALCOLO i, MOVIMENTO_CONTABILE_158 m "+
                "where i.ID_MOVIMENTO_CONTABILE=m.ID_MOVIMENTO_CONTABILE_158 "+
                "and m.ID_PROGRAMMA_MUSICALE=? "+
                "group by i.ID_ESECUZIONE_RICALCOLO, m.ID_PROGRAMMA_MUSICALE "+
                ") "+
                "order by ID_ESECUZIONE_RICALCOLO desc ";

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, idProgrammaMusicale);
        q.setParameter(2, idProgrammaMusicale);

        List<Object[]> results = (List<Object[]>)q.getResultList();

        if (results != null && results.size() > 0) {

            ImportoRicalcolato importoRicalcolato = null;

            for (int i = 0; i < results.size(); i++) {

                importoRicalcolato = new ImportoRicalcolato();
                importoRicalcolato.setIdEsecuzioneRicalcolo(((BigDecimal)results.get(i)[0]).longValue());
                importoRicalcolato.setIdProgrammaMusicale(((BigDecimal)results.get(i)[1]).longValue());
                importoRicalcolato.setImporto(((BigDecimal)results.get(i)[2]).doubleValue());

                listaImportiRicalcolati.add(importoRicalcolato);
            }

        }

        return listaImportiRicalcolati;

    }

    public Long getMovimentiContabili(Long periodoContabileInizio, Long periodoContabileFine, String tipologiaPm){

        String query = "select count(m) from MovimentoContabile m, ProgrammaMusicale p "+
                "where m.idProgrammaMusicale=p.id and m.contabilita>=:inizioPeriodoContabile "+
                "and m.contabilita<=:finePeriodoContabile "+
                "and (:tipologiaPm is null or p.tipologiaPm=:tipologiaPm) "+
                "and m.voceIncasso<>:voceIncasso "+
                "order by m.idEvento, m.numeroFattura, m.voceIncasso, m.idProgrammaMusicale asc";

        Long results = (Long)entityManager.createQuery(query)
                .setParameter("inizioPeriodoContabile",periodoContabileInizio)
                .setParameter("finePeriodoContabile",periodoContabileFine)
                .setParameter("tipologiaPm",tipologiaPm)
                .setParameter("voceIncasso","2243")
                .getSingleResult();


        return results;

    }


    public Long getMovimentiContabili2243(Long periodoContabileInizio, Long periodoContabileFine, String tipologiaPm){

        String query = "select count(m) from MovimentoContabile m, ProgrammaMusicale p "+
                "where m.idProgrammaMusicale=p.id and m.contabilita>=:inizioPeriodoContabile "+
                "and m.contabilita<=:finePeriodoContabile "+
                "and (:tipologiaPm is null or p.tipologiaPm=:tipologiaPm) "+
                "and m.voceIncasso=:voceIncasso "+
                "order by m.idEvento, m.numeroFattura, m.voceIncasso, m.idProgrammaMusicale asc";

        Long results = (Long)entityManager.createQuery(query)
                .setParameter("inizioPeriodoContabile",periodoContabileInizio)
                .setParameter("finePeriodoContabile",periodoContabileFine)
                .setParameter("tipologiaPm",tipologiaPm)
                .setParameter("voceIncasso","2243")
                .getSingleResult();


        return results;

    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Integer updateContabilitaOriginale(Integer nuovaContabilita, List<Object> listaMovimenti){

        Integer result = 0;

        try{
            MovimentoContabile movimentoContabile=null;
            if (nuovaContabilita != null) {
                for (int i = 0; i < listaMovimenti.size(); i++) {
                    movimentoContabile = (MovimentoContabile) listaMovimenti.get(i);

                    // Aggiorna contabilita dei movimenti
                    movimentoContabile = entityManager.find(MovimentoContabile.class, movimentoContabile.getId());
                    movimentoContabile.setContabilitaOrg(movimentoContabile.getContabilita());
                    movimentoContabile.setContabilita(new Integer(nuovaContabilita));
                    entityManager.merge(movimentoContabile);

                    // Aggiorna contabilita cartella
                    Cartella cartella = entityManager.find(Cartella.class, movimentoContabile.getIdCartella());
                    cartella.setContabilitaOrg(cartella.getContabilita());
                    cartella.setContabilita(nuovaContabilita);
                    entityManager.merge(cartella);

                    // Aggiorna contabilita eventi pagati
                    EventiPagati importoPagato = getEventoPagato(movimentoContabile.getIdEvento(), movimentoContabile.getVoceIncasso(), movimentoContabile.getNumeroFattura().toString());
                    if (importoPagato != null) {
                        importoPagato.setContabilitaOrg(importoPagato.getContabilita());
                        importoPagato.setContabilita(nuovaContabilita);
                        entityManager.merge(importoPagato);
                    }

                    entityManager.flush();

                }
            }
        } catch(Exception e){
            e.printStackTrace();
            result = -1;
        }

        return result;

    }

}
