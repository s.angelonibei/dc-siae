package it.siae.valorizzatore.dao;

import it.siae.valorizzatore.model.Circoscrizione;
import it.siae.valorizzatore.model.Configurazione;
import it.siae.valorizzatore.model.TrattamentoPM;

import java.util.List;

/**
 * Created by idilello on 6/9/16.
 */
public interface IConfigurationDAO {

    public void initCache();

    public List<TrattamentoPM> getTrattamentoPM();

    public String getLogicalServerAddress(String phisycalAddress);

    public String getParameter(String parameterName);

    public List<Configurazione> getAllCachedParameter();

    public void insertParameter(String parameterName, String parameterValue);

    public Configurazione getParameterObject(String parameterName);

    public void insertParameterObject(Configurazione configurazione);

    public List<Circoscrizione> getCircoscrizioni();
}
