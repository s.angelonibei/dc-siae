package it.siae.valorizzatore.dao.security;

import it.siae.valorizzatore.model.security.Profile;
import it.siae.valorizzatore.model.security.Role;
import it.siae.valorizzatore.model.security.User;

import java.util.List;

public interface IUserDAO {

    public User getUser(String username, String password);

    public User getUser(String username);

    public List<Profile> getUserProfiles(User user);
}
