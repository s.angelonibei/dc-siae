package it.siae.valorizzatore.dao.security;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import it.siae.valorizzatore.model.security.Profile;
import it.siae.valorizzatore.model.security.Role;
import it.siae.valorizzatore.model.security.User;

import org.springframework.stereotype.Repository;

@Repository
public class UserDAO implements IUserDAO {
    
    @PersistenceContext(unitName = "VALORIZZATORE")
    private EntityManager entityManager;

    public User getUser(String username, String password){

        User user = null;

        String query = "select u from User u where u.username=:username and u.password=:password and u.flagEnabled=:active";

        List<User> results = (List<User>)entityManager.createQuery(query)
                .setParameter("username", username)
                .setParameter("password", password)
                .setParameter("active", 'Y')
                .getResultList();

        if (results!=null && results.size()>0) {
            user = results.get(0);
            user.setProfiles(getUserProfiles(user));

            return user;
        }

        return null;
    }

    public User getUser(String username){

        User user = null;

        String query = "select u from User u where UPPER(u.username)=UPPER(:username) and u.flagEnabled=:active";

        List<User> results = (List<User>)entityManager.createQuery(query)
                .setParameter("username", username)
                .setParameter("active", 'Y')
                .getResultList();

        if (results!=null && results.size()>0) {
            user = results.get(0);
            user.setProfiles(getUserProfiles(user));

            return user;
        }

        return null;

    }


    public List<Profile> getUserProfiles(User user){

        String query = "select p "+
                       "from Role r, UserRole ur, RoleProfile rp, Profile p "+
                       "where r.id=ur.roleId and ur.roleId=rp.idRole and rp.idProfile=p.id " +
                       "and ur.userId=:userId ";

        List<Profile> profiles = (List<Profile>)entityManager.createQuery(query)
                .setParameter("userId", user.getId())
                .getResultList();

        if (profiles!=null && profiles.size()>0) {
            return profiles;
        }

        return null;
    }
    
}