package it.siae.valorizzatore.dao;

import it.siae.valorizzatore.model.*;
import it.siae.valorizzatore.model.security.ReportPage;
import it.siae.valorizzatore.utility.Constants;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by idilello on 6/9/16.
 */
@Repository("esecuzioneDAO")
public class EsecuzioneDAO implements IEsecuzioneDAO{

    @PersistenceContext(unitName = "VALORIZZATORE")
    private EntityManager entityManager;

    public List<EsecuzioneRicalcolo> getEsecuzioni(){

        //String query = "select e from EsecuzioneRicalcolo e where e.esitoEsecuzione=:esitoEsecuzione order by e.dataOraInizioEsecuzione desc";
        String query = "select e from EsecuzioneRicalcolo e order by e.dataOraInizioEsecuzione desc";


        List<EsecuzioneRicalcolo> results = (List<EsecuzioneRicalcolo>)entityManager.createQuery(query)
                                                    //.setParameter("esitoEsecuzione","0")
                                                    .getResultList();

        // Ritorna solo le ultime 20 elaborazioni eseguite per controllo
        if (results!=null && results.size()>20) {
            List<EsecuzioneRicalcolo> newResult=new ArrayList<EsecuzioneRicalcolo>();
            for (int i = 0; i < 20; i++) {
                newResult.add(results.get(i));
            }
            return newResult;
        }

        return results;
    }

    public EsecuzioneRicalcolo findEsecuzioneByContabilitaSiada(Long contabilitaSiada){

        String query = "select e from EsecuzioneRicalcolo e where e.contabilitaSiada=:contabilitaSiada";

        List<EsecuzioneRicalcolo> results = (List<EsecuzioneRicalcolo>)entityManager.createQuery(query)
                                            .setParameter("contabilitaSiada",contabilitaSiada)
                                            .getResultList();

        // Ritorna solo le ultime 20 elaborazioni eseguite per controllo
        if (results!=null && results.size()>0) {
            return results.get(0);
        } else
           return null;
    }

    public EsecuzioneRicalcolo findEsecuzioneById(Long idEsecuzioneRicalcolo){

        String query = "select e from EsecuzioneRicalcolo e where e.id=:idEsecuzioneRicalcolo";

        List<EsecuzioneRicalcolo> results = (List<EsecuzioneRicalcolo>)entityManager.createQuery(query)
                .setParameter("idEsecuzioneRicalcolo",idEsecuzioneRicalcolo)
                .getResultList();

        // Ritorna solo le ultime 20 elaborazioni eseguite per controllo
        if (results!=null && results.size()>0) {
            return results.get(0);
        } else
            return null;

    }

    @Transactional(propagation = Propagation.REQUIRED)
    public int setDataCongelamentoEsecuzione(Long elaborazioneSIADAId){

        int result = 0;

        try {
            EsecuzioneRicalcolo esecuzione = entityManager.find(EsecuzioneRicalcolo.class, elaborazioneSIADAId);

            if (esecuzione != null){
                esecuzione.setDataOraCongelamento(new Date());
                esecuzione.setDataOraTrasmissioneSIADA(new Date());
                esecuzione.setContabilitaSiada(esecuzione.getContabilitaFine());

                entityManager.persist(esecuzione);
                entityManager.flush();

            } else {
                result = -2;
            }

        }catch(Exception e){
            e.printStackTrace();
            result = -1;
        }

        return result;

    }

    public ReportPage getListaPMSospesi(Long idEsecuzioneRicalcolo, Long idEvento, String numeroFattura, String voceIncasso, String numeroProgrammaMusicale, String inizioPeriodoContabile, String finePeriodoContabile, Integer page, String type) {

        String query = null;

        List<MovimentoContabile> listaPM = new ArrayList<MovimentoContabile>();
        List<MovimentoContabile> listaallPM = new ArrayList<MovimentoContabile>();


        ReportPage reportPage = new ReportPage(listaPM, listaPM.size(), page);

        int recordCount=0;

        query = "select m.idEvento, m.voceIncasso, m.numeroFattura, m.numProgrammaMusicale, m.tipologiaMovimento, m.tipoDocumentoContabile, m.numeroPmPrevisti, m.importoTotDem, i.motivazioneSospensione, p.totaleCedole, p.totaleDurataCedole " +
                "from SospesiRicalcolo i, MovimentoContabile m, ProgrammaMusicale p " +
                "where m.idProgrammaMusicale=p.id "+
                "and m.idProgrammaMusicale=i.idProgrammaMusicale "+
                "and i.voceIncasso=m.voceIncasso "+
                "and i.numeroFattura=m.numeroFattura "+
                "and i.idEvento=m.idEvento "+
                "and i.idProgrammaMusicale=p.id " +
                "and i.idEsecuzioneRicalcolo=:idEsecuzioneRicalcolo " +
                "and m.contabilita>=:inizioPeriodoContabile and m.contabilita<=:finePeriodoContabile ";

        if (type!=null && !type.equalsIgnoreCase(Constants.ERRORE))
            query = query + "and i.motivazioneSospensione not like :errorMessage ";
        else
            query = query + "and i.motivazioneSospensione like :errorMessage ";

        if (idEvento != null)
            query = query + "and m.idEvento=:idEvento ";
        if (numeroFattura != null && !numeroFattura.equalsIgnoreCase(""))
            query = query + "and m.numeroFattura=:numeroFattura ";
        if (voceIncasso != null && !voceIncasso.equalsIgnoreCase(""))
            query = query + "and m.voceIncasso=:voceIncasso ";
        if (numeroProgrammaMusicale != null && !numeroProgrammaMusicale.equalsIgnoreCase(""))
            query = query + "and m.numProgrammaMusicale=:numeroProgrammaMusicale ";

        query = query + "order by m.idEvento, m.numeroFattura, m.voceIncasso desc";

        Query q = entityManager.createQuery(query);

        // Parametri obbligatori
        q.setParameter("idEsecuzioneRicalcolo", idEsecuzioneRicalcolo);
        q.setParameter("inizioPeriodoContabile", Integer.parseInt(inizioPeriodoContabile));
        q.setParameter("finePeriodoContabile", Integer.parseInt(finePeriodoContabile));
        q.setParameter("errorMessage", "ERRORE_ELABORAZIONE%");

        // Parametri facoltativi
        if (idEvento != null)
            q.setParameter("idEvento", idEvento);
        if (numeroFattura != null && !numeroFattura.equalsIgnoreCase(""))
            q.setParameter("numeroFattura", numeroFattura);
        if (voceIncasso != null && !voceIncasso.equalsIgnoreCase(""))
            q.setParameter("voceIncasso", voceIncasso);
        if (numeroProgrammaMusicale != null && !numeroProgrammaMusicale.equalsIgnoreCase(""))
            q.setParameter("numeroProgrammaMusicale", numeroProgrammaMusicale);

        System.out.println("-----> Inizio: "+System.currentTimeMillis());

        List<Object[]> results = (List<Object[]>) q.getResultList();

        System.out.println("-----> Fine: "+System.currentTimeMillis());

        if (results != null && results.size() > 0) {

            recordCount = results.size();

            int startRecord = (page-1) * ReportPage.NUMBER_RECORD_PER_PAGE;
            int endRecord = 0;
            if (recordCount>ReportPage.NUMBER_RECORD_PER_PAGE) {
                endRecord = startRecord + ReportPage.NUMBER_RECORD_PER_PAGE;
                if (endRecord > recordCount)
                    endRecord = recordCount -1;
            }else {
                endRecord = recordCount;
            }

            MovimentoContabile movimento = null;
            Double importo=0D;
            for (int i = 0; i < recordCount; i++) {
                movimento = new MovimentoContabile();

                movimento.setIdEvento((Long) results.get(i)[0]);
                movimento.setVoceIncasso((String) results.get(i)[1]);
                movimento.setNumeroFattura((Long) results.get(i)[2]);
                movimento.setNumProgrammaMusicale((String) results.get(i)[3]);
                movimento.setTipologiaMovimento((String) results.get(i)[4]);
                movimento.setTipoDocumentoContabile((String) results.get(i)[5]);
                movimento.setNumeroPmPrevisti((Integer) results.get(i)[6]);
                movimento.setImportoTotDem((Double) results.get(i)[7]);
                movimento.setMotivazioneSospensione((String) results.get(i)[8]);
                movimento.setTotaleCedole((Long) results.get(i)[9]);
                movimento.setTotaleDurataCedole((Double) results.get(i)[10]);
                importo = getImportoEvento(Integer.parseInt(inizioPeriodoContabile), Integer.parseInt(finePeriodoContabile),movimento.getIdEvento(), movimento.getNumeroFattura(), movimento.getVoceIncasso());
                movimento.setImportoEvento(importo);

                if (i>=startRecord && i<endRecord)
                 listaPM.add(movimento);

                listaallPM.add(movimento);
            }

            reportPage = new ReportPage(listaallPM, listaPM, recordCount, page);

        }

        return reportPage ;

    }


    private Double getImportoEvento(Integer inizioPeriodoContabile, Integer finePeriodoContabile, Long idEvento, Long numeroFattura, String voceIncasso){

        String query = "select sum(decode(m.TIPO_DOCUMENTO_CONTABILE,'501', m.IMPORTO_DEM, '221',-m.IMPORTO_DEM)) "+
                       "from Eventi_Pagati m "+
                       "where m.contabilita>=? and m.contabilita<=? "+
                       "and m.ID_EVENTO=? "+
                       "and m.numero_fattura=? "+
                       "and m.voce_incasso=? ";

        List<BigDecimal> importo = (List<BigDecimal>)entityManager.createNativeQuery(query)
                .setParameter(1,new Integer(inizioPeriodoContabile))
                .setParameter(2, new Integer(finePeriodoContabile))
                .setParameter(3, idEvento)
                .setParameter(4, numeroFattura)
                .setParameter(5, voceIncasso)
                .getResultList();

        if (importo != null && importo.size()>0)
          return importo.get(0)!=null?((BigDecimal)importo.get(0)).doubleValue():0D;
        else
            return 0D;

    }


    public ReportPage getListaPM(Long idEsecuzioneRicalcolo, Long idEvento, String numeroFattura, String voceIncasso, String numeroProgrammaMusicale, String inizioPeriodoContabile, String finePeriodoContabile, Integer page){

        String query = null;

        List<MovimentoContabile> listaPM = new ArrayList<MovimentoContabile>();
        List<MovimentoContabile> listaAllPM = new ArrayList<MovimentoContabile>();

        ReportPage reportPage = new ReportPage(listaPM, listaPM.size(), page);

        int recordCount=0;

        query = "select m.idEvento, m.voceIncasso, m.numeroFattura, m.numProgrammaMusicale, m.tipologiaMovimento, m.tipoDocumentoContabile, m.numeroPmPrevisti, m.importoTotDem, i.importo, p.totaleCedole, p.totaleDurataCedole, i.importoSingolaCedola " +
                "from ImportoRicalcolato i, MovimentoContabile m, ProgrammaMusicale p " +
                "where m.id=i.idMovimentoContabile and m.idProgrammaMusicale=p.id " +
                "and i.idEsecuzioneRicalcolo=:idEsecuzioneRicalcolo " +
                "and m.contabilita>=:inizioPeriodoContabile and m.contabilita<=:finePeriodoContabile ";

        if (idEvento != null)
         query = query + "and m.idEvento=:idEvento ";
        if (numeroFattura != null && !numeroFattura.equalsIgnoreCase(""))
         query = query + "and m.numeroFattura=:numeroFattura ";
        if (voceIncasso != null && !voceIncasso.equalsIgnoreCase(""))
         query = query + "and m.voceIncasso=:voceIncasso ";
        if (numeroProgrammaMusicale != null && !numeroProgrammaMusicale.equalsIgnoreCase(""))
         query = query + "and m.numProgrammaMusicale=:numeroProgrammaMusicale ";

         query = query + "order by m.idEvento, m.numeroFattura, m.voceIncasso desc";

         Query q = entityManager.createQuery(query);

         // Parametri obbligatori
         q.setParameter("idEsecuzioneRicalcolo", idEsecuzioneRicalcolo);
         q.setParameter("inizioPeriodoContabile", Integer.parseInt(inizioPeriodoContabile));
         q.setParameter("finePeriodoContabile", Integer.parseInt(finePeriodoContabile));

         // Parametri facoltativi
         if (idEvento != null)
            q.setParameter("idEvento", idEvento);
         if (numeroFattura != null && !numeroFattura.equalsIgnoreCase(""))
            q.setParameter("numeroFattura", numeroFattura);
         if (voceIncasso != null && !voceIncasso.equalsIgnoreCase(""))
            q.setParameter("voceIncasso", voceIncasso);
         if (numeroProgrammaMusicale != null && !numeroProgrammaMusicale.equalsIgnoreCase(""))
            q.setParameter("numeroProgrammaMusicale", numeroProgrammaMusicale);

         //q.setFirstResult((page-1) * ReportPage.NUMBER_RECORD_PER_PAGE);
         //q.setMaxResults(ReportPage.NUMBER_RECORD_PER_PAGE);

         List<Object[]> results = (List<Object[]>) q.getResultList();

         if (results != null && results.size() > 0) {

            recordCount = results.size();

            int startRecord = (page-1) * ReportPage.NUMBER_RECORD_PER_PAGE;
            int endRecord = 0;
            if (recordCount>ReportPage.NUMBER_RECORD_PER_PAGE) {
                endRecord = startRecord + ReportPage.NUMBER_RECORD_PER_PAGE;
                if (endRecord > recordCount)
                    endRecord = recordCount -1;
            }else {
                endRecord = recordCount;
            }

            MovimentoContabile movimento = null;
            Double importo=0D;
            for (int i = 0; i < recordCount; i++) {
                    movimento = new MovimentoContabile();
                    movimento.setIdEvento((Long) results.get(i)[0]);
                    movimento.setVoceIncasso((String) results.get(i)[1]);
                    movimento.setNumeroFattura((Long) results.get(i)[2]);
                    movimento.setNumProgrammaMusicale((String) results.get(i)[3]);
                    movimento.setTipologiaMovimento((String) results.get(i)[4]);
                    movimento.setTipoDocumentoContabile((String) results.get(i)[5]);
                    movimento.setNumeroPmPrevisti((Integer) results.get(i)[6]);
                    movimento.setImportoTotDem((Double) results.get(i)[7]);
                    movimento.setImportoRicalcolato((Double) results.get(i)[8]);
                    movimento.setTotaleCedole((Long) results.get(i)[9]);
                    movimento.setTotaleDurataCedole((Double) results.get(i)[10]);
                    if (results.get(i)[11]!=null)
                        movimento.setImportoRicalcolatoSingolaCedola((Double) results.get(i)[11]);
                    importo = getImportoEvento(Integer.parseInt(inizioPeriodoContabile), Integer.parseInt(finePeriodoContabile),movimento.getIdEvento(), movimento.getNumeroFattura(), movimento.getVoceIncasso());
                    movimento.setImportoEvento(importo);

                   if (i>=startRecord && i< endRecord)
                    listaPM.add(movimento);

                listaAllPM.add(movimento);
            }

            //reportPage = new ReportPage(listaPM, recordCount, page);
            reportPage = new ReportPage(listaAllPM, listaPM, recordCount, page);

         }

        return reportPage ;

    }


    public Long getNumeroMovimentazioniAggregate(EsecuzioneRicalcolo esecuzione){

        List<MovimentoSiada> movimentiSiada = new ArrayList<MovimentoSiada>();

        String query=
                "select count(*) "+
                "from ( "+
                    //"-- Use case 1 PM (totale 15 Use Case) "+
                    "select m.ID_PROGRAMMA_MUSICALE, max(m.CONTABILITA) as CONTABILITA, min(e.ID_EVENTO) as ID_EVENTO, min(e.DATA_INIZIO_EVENTO) as DATA_INIZIO_EVENTO, max(e.DATA_FINE_EVENTO) as DATA_FINE_EVENTO, "+
                    "m.CODICE_VOCE_INCASSO, max(m.NUMERO_FATTURA) as NUMERO_FATTURA, "+
                    "sum(decode(m.TIPO_DOCUMENTO_CONTABILE,'501', m.IMPORTO_DEM_TOTALE, '221',-m.IMPORTO_DEM_TOTALE)) as IMPORTO  "+
                    "from movimento_contabile_158 m, MANIFESTAZIONE e, esecuzione_ricalcolo r, sospesi_ricalcolo s "+
                    "where m.ID_EVENTO=e.ID_EVENTO "+
                    "and s.id_esecuzione_ricalcolo=r.id_esecuzione_ricalcolo "+
                    "and s.motivazione_sospensione='EVENTO_VOCE_UNICO_PM_ATTESO' "+
                    "and s.id_movimento_contabile=m.id_movimento_contabile_158  "+
                    "and m.CONTABILITA>=? and m.CONTABILITA<=? "+
                    "and r.id_esecuzione_ricalcolo=? "+
                    "and m.CODICE_VOCE_INCASSO<>? "+
                    "group by m.ID_PROGRAMMA_MUSICALE, m.CODICE_VOCE_INCASSO "+
                    "union  "+
                    //"-- Use case q PM (totale 16 Use Case) "+
                    "select m.ID_PROGRAMMA_MUSICALE, max(m.CONTABILITA) as CONTABILITA, min(m.ID_EVENTO) as ID_EVENTO, min(e.DATA_INIZIO_EVENTO) as DATA_INIZIO_EVENTO, max(e.DATA_FINE_EVENTO) as DATA_FINE_EVENTO, "+
                    "m.CODICE_VOCE_INCASSO, max(m.NUMERO_FATTURA) as NUMERO_FATTURA, "+
                    "sum(decode(m.TIPO_DOCUMENTO_CONTABILE,'501', i.IMPORTO, '221',-i.IMPORTO)) as IMPORTO "+
                    "from movimento_contabile_158 m, MANIFESTAZIONE e, esecuzione_ricalcolo r, importo_ricalcolato i "+
                    "where m.ID_EVENTO=e.ID_EVENTO "+
                    "and i.id_esecuzione_ricalcolo=r.id_esecuzione_ricalcolo "+
                    "and i.id_movimento_contabile=m.id_movimento_contabile_158 "+
                    "and m.CONTABILITA>=? and m.CONTABILITA<=? "+
                    "and r.id_esecuzione_ricalcolo=? "+
                    "and m.CODICE_VOCE_INCASSO<>? "+
                    "group by m.CODICE_VOCE_INCASSO, m.ID_PROGRAMMA_MUSICALE "+
                ") ";
                //"order by ID_PROGRAMMA_MUSICALE,CODICE_VOCE_INCASSO asc ";

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, esecuzione.getContabilitaInizio());
        q.setParameter(2, esecuzione.getContabilitaFine());
        q.setParameter(3, esecuzione.getId());
        q.setParameter(4, Constants.VOCE_2243);
        q.setParameter(5, esecuzione.getContabilitaInizio());
        q.setParameter(6, esecuzione.getContabilitaFine());
        q.setParameter(7, esecuzione.getId());
        q.setParameter(8, Constants.VOCE_2243);

        BigDecimal results = (BigDecimal) q.getSingleResult();

        return results.longValue();
    }

    public Long getNumeroMovimentazioniAggregate2243(EsecuzioneRicalcolo esecuzione){

        List<MovimentoSiada> movimentiSiada = new ArrayList<MovimentoSiada>();

        String query=
                "select count(*) "+
                        "from ( "+
                        //"-- Use case 1 PM (totale 15 Use Case) "+
                        "select m.ID_PROGRAMMA_MUSICALE, max(m.CONTABILITA) as CONTABILITA, min(e.ID_EVENTO) as ID_EVENTO, min(e.DATA_INIZIO_EVENTO) as DATA_INIZIO_EVENTO, max(e.DATA_FINE_EVENTO) as DATA_FINE_EVENTO, "+
                        "m.CODICE_VOCE_INCASSO, max(m.NUMERO_FATTURA) as NUMERO_FATTURA, "+
                        "sum(decode(m.TIPO_DOCUMENTO_CONTABILE,'501', m.IMPORTO_DEM_TOTALE, '221',-m.IMPORTO_DEM_TOTALE)) as IMPORTO  "+
                        "from movimento_contabile_158 m, MANIFESTAZIONE e, esecuzione_ricalcolo r, sospesi_ricalcolo s "+
                        "where m.ID_EVENTO=e.ID_EVENTO "+
                        "and s.id_esecuzione_ricalcolo=r.id_esecuzione_ricalcolo "+
                        "and s.motivazione_sospensione='EVENTO_VOCE_UNICO_PM_ATTESO' "+
                        "and s.id_movimento_contabile=m.id_movimento_contabile_158  "+
                        "and m.CONTABILITA>=? and m.CONTABILITA<=? "+
                        "and r.id_esecuzione_ricalcolo=? "+
                        "and m.CODICE_VOCE_INCASSO=? "+
                        "group by m.ID_PROGRAMMA_MUSICALE, m.CODICE_VOCE_INCASSO "+
                        "union  "+
                        //"-- Use case q PM (totale 16 Use Case) "+
                        "select m.ID_PROGRAMMA_MUSICALE, max(m.CONTABILITA) as CONTABILITA, min(m.ID_EVENTO) as ID_EVENTO, min(e.DATA_INIZIO_EVENTO) as DATA_INIZIO_EVENTO, max(e.DATA_FINE_EVENTO) as DATA_FINE_EVENTO, "+
                        "m.CODICE_VOCE_INCASSO, max(m.NUMERO_FATTURA) as NUMERO_FATTURA, "+
                        "sum(decode(m.TIPO_DOCUMENTO_CONTABILE,'501', i.IMPORTO, '221',-i.IMPORTO)) as IMPORTO "+
                        "from movimento_contabile_158 m, MANIFESTAZIONE e, esecuzione_ricalcolo r, importo_ricalcolato i "+
                        "where m.ID_EVENTO=e.ID_EVENTO "+
                        "and i.id_esecuzione_ricalcolo=r.id_esecuzione_ricalcolo "+
                        "and i.id_movimento_contabile=m.id_movimento_contabile_158 "+
                        "and m.CONTABILITA>=? and m.CONTABILITA<=? "+
                        "and r.id_esecuzione_ricalcolo=? "+
                        "and m.CODICE_VOCE_INCASSO=? "+
                        "group by m.CODICE_VOCE_INCASSO, m.ID_PROGRAMMA_MUSICALE "+
                        ") ";
        //"order by ID_PROGRAMMA_MUSICALE,CODICE_VOCE_INCASSO asc ";

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, esecuzione.getContabilitaInizio());
        q.setParameter(2, esecuzione.getContabilitaFine());
        q.setParameter(3, esecuzione.getId());
        q.setParameter(4, Constants.VOCE_2243);
        q.setParameter(5, esecuzione.getContabilitaInizio());
        q.setParameter(6, esecuzione.getContabilitaFine());
        q.setParameter(7, esecuzione.getId());
        q.setParameter(8, Constants.VOCE_2243);

        BigDecimal results = (BigDecimal) q.getSingleResult();

        return results.longValue();
    }


    @Transactional(propagation= Propagation.REQUIRED)
    public void deleteMovimentoSiada(){

        String query = "delete from movimenti_siada where data_elaborazione is null";

        Query q = entityManager.createNativeQuery(query);

        int result = q.executeUpdate();

        entityManager.flush();

    }

    @Transactional(propagation= Propagation.REQUIRED)
    public void deleteMovimentoSiada2243(){

        String query = "delete from movimenti_siada_2243 where data_elaborazione is null";

        Query q = entityManager.createNativeQuery(query);

        int result = q.executeUpdate();

        entityManager.flush();

    }


    @Transactional(propagation = Propagation.REQUIRED)
    public void trace(TracciamentoApplicativo tracciamentoApplicativo){


        try {

            entityManager.persist(tracciamentoApplicativo);
            entityManager.flush();

        }catch(Exception e){
            e.printStackTrace();
        }

    }


    public Long getNewEsecuzioneRicalcoloId(){

        String query = "select ESEC_RICALCOLO_SEQ.nextval "+
                "from dual ";

        BigDecimal importo = (BigDecimal)entityManager.createNativeQuery(query)
                .getSingleResult();

        return importo.longValue();

    }

}
