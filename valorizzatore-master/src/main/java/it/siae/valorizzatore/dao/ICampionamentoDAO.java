package it.siae.valorizzatore.dao;

import it.siae.valorizzatore.model.CampionamentoConfig;
import it.siae.valorizzatore.model.TracciamentoApplicativo;
import it.siae.valorizzatore.model.security.ReportPage;

import java.util.List;

/**
 * Created by idilello on 6/21/16.
 */
public interface ICampionamentoDAO {

    public CampionamentoConfig getCampionamentoConfig(Long contabilita);

    public void insertParametriCampionamento(CampionamentoConfig campionamentoConfig);

    public ReportPage getCampionamenti(Long contabilitaIniziale, Long contabilitaFinale, Integer page);

    public CampionamentoConfig getCampionamentoConfigById(Long idCampionamentoConfig);

    public ReportPage getFlussoGuida(Long contabilitaIniziale, Long contabilitaFinale, Integer page);

    public ReportPage getFlussoRientrati(Long contabilitaIniziale, Long contabilitaFinale, Integer page);

    public ReportPage getStoricoEsecuzioni(Long periodoIniziale, Long periodoFinale, Integer page);

    public List<TracciamentoApplicativo> getEsecuzioniCampionamentoEngine(Long periodoIniziale, Long periodoFinale);

    public List<TracciamentoApplicativo> getEsecuzioniCampionamentoStart(Long periodoIniziale, Long periodoFinale);
}
