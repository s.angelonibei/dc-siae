package it.siae.valorizzatore.dao;

import it.siae.valorizzatore.model.*;
import it.siae.valorizzatore.model.security.AggregatoVoce;
import it.siae.valorizzatore.model.security.ReportPage;

import java.util.Date;
import java.util.List;

/**
 * Created by idilello on 7/11/16.
 */
public interface IProgrammaMusicaleDAO {

    public void insertMovimentazioneLogisticaPM(MovimentazioneLogisticaPM movimentazioneLogisticaPM);

    public List<ProgrammaMusicale> getProgrammaMusicale(Long numeroProgrammaMusicale);

    public void  updatePMRientrati(ProgrammaMusicale programmaMusicale);

    public MovimentazioneLogisticaPM getUltimaMovimentazionePM(Long idProgrammaMusicale);

    public ReportPage getTracciamentoPM(Date dataIniziale, Date dataFinale, Long numeroPM, Integer page);

    public ReportPage getListaEventiPagati(Long inizioPeriofoContabile, Long finePeriofoContabile, Long evento, String fattura, String reversale, String voceIncasso,
                                           Date dataInizioEvento, String oraInizioEvento, String seprag, String tipoDocumento, String locale, String codiceBA, String presenzaMovimenti, Integer page);

    public List<EventiPagati> getEvento(Long eventoId);

    public List<MovimentoContabile> getMovimentazioniPerEvento(Long eventoId);

    public List<ImportoRicalcolato> getImportiPMAssegnatiPerEvento(Long idEvento);

    public List<ImportoRicalcolato> getImportiTotaliPerEvento(Long idEvento);

    public List<ImportoRicalcolato> getImportiRicalcolatiPerMovimentazione(Long idMovimento158);

    public List<String> getTipoProgrammi();

    public ReportPage getListaMovimentazioni(Long inizioPeriodoContabile, Long finePeriodoContabile, Long evento, String fattura, Long reversale, String voceIncasso,
                                             Date dataInizioEvento,Date dataFineEvento, String seprag, String tipoDocumento, String locale, String organizzatore,
                                             String tipoSupporto, String tipoProgramma, String gruppoPrincipale, String titoloOpera, String numeroPM, String permesso,
                                             Integer page);

    public List<AggregatoVoce> getListaDocumentiEventi(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale);

    public List<AggregatoVoce> getListaEventi(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale);

    public MovimentoContabile getMovimentoContabile(Long idMovimento);

    public List<MovimentoContabile> getMovimentiContabile(Long idProgrammaMusicale);

    public ProgrammaMusicale getProgrammaMusicaleById(Long idProgrammaMusicale);

    public Manifestazione getManifestazione(Long idEvento);

    public List<Utilizzazione> getUtilizzazioni(Long idProgrammaMusicale);

    public DirettoreEsecuzione getDirettoreEsecuzione(Long idProgrammaMusicale);

    public Cartella getCartellaById(Long idCartella);

    public List<Manifestazione> getManifestazioni(List<MovimentoContabile> listaMovimenti);

    public ReportPage getListaAggregatiSiada(Long inizioPeriodoContabile, Long finePeriodoContabile, String voceIncasso, String tipoSupporto, String tipoProgramma, Long numeroPM, Integer page);

    public ReportPage getListaAggregatiSiadaSMC(Long inizioPeriodoContabile, Long finePeriodoContabile, String voceIncasso, String tipoSupporto, String tipoProgramma, Long numeroPM, Integer page);

    public List<AggregatoVoce>  getImportoNettiPMCorrenti(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale);

    public List<AggregatoVoce> getImportoProgrammatoCorrenti(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale);

    public List<AggregatoVoce> getNumeroPMCorrenti(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale);

    public List<AggregatoVoce> getNumeroCedoleCorrenti(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale);

    public List<AggregatoVoce>  getImportoNettiPMArretrati(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale);

    public List<AggregatoVoce> getNumeroPMArretrati(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale);

    public List<AggregatoVoce> getNumeroCedoleArretrati(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale);

    public Double getImportoPMSUNAggregato(Long idProgrammaMusicale);

    public Double getImportoPMSIADAAggregato(Long idProgrammaMusicale);

    public List<ImportoRicalcolato> getImportiPMRicalcolati(Long idProgrammaMusicale);

    public Long getMovimentiContabili(Long periodoContabileInizio, Long periodoContabileFine, String tipologiaPm);

    public Long getMovimentiContabili2243(Long periodoContabileInizio, Long periodoContabileFine, String tipologiaPm);

    public Integer updateContabilitaOriginale(Integer nuovaContabilita, List<Object> listaMovimenti);


}
