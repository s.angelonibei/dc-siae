package it.siae.valorizzatore.dao;

import it.siae.valorizzatore.model.*;
import it.siae.valorizzatore.model.security.ReportPage;
import it.siae.valorizzatore.utility.Constants;
import it.siae.valorizzatore.utility.Utility;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by idilello on 6/9/16.
 */
@Repository("campionamentoDAO")
public class CampionamentoDAO implements ICampionamentoDAO{

    @PersistenceContext(unitName = "VALORIZZATORE")
    private EntityManager entityManager;


     public CampionamentoConfig getCampionamentoConfig(Long contabilita){

         String query = "select c from CampionamentoConfig c where c.contabilita=:contabilita and c.flagAttivo='Y'";

         List<CampionamentoConfig> results = (List<CampionamentoConfig>)entityManager.createQuery(query)
                 .setParameter("contabilita",contabilita)
                 .getResultList();

         if (results!=null && results.size()>0)
             return results.get(0);
         else
             return null;

     }

    public CampionamentoConfig getCampionamentoConfigById(Long idCampionamentoConfig){

        String query = "select c from CampionamentoConfig c where c.id=:idCampionamentoConfig and c.flagAttivo='Y'";

        List<CampionamentoConfig> results = (List<CampionamentoConfig>)entityManager.createQuery(query)
                .setParameter("idCampionamentoConfig",idCampionamentoConfig)
                .getResultList();

        if (results!=null && results.size()>0)
            return results.get(0);
        else
            return null;

    }


    @Transactional(propagation = Propagation.REQUIRED)
    public void insertParametriCampionamento(CampionamentoConfig campionamentoConfig){

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setName("rootTransaction");
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        CampionamentoConfig configurazionePrecedeente = getCampionamentoConfig(campionamentoConfig.getContabilita());

        if (configurazionePrecedeente!=null){
            configurazionePrecedeente.setFlagAttivo('N');
            entityManager.persist(configurazionePrecedeente);
            entityManager.flush();
        }

        campionamentoConfig.setDataOraUltimaModifica(new Date());
        campionamentoConfig.setFlagAttivo('Y');

        entityManager.persist(campionamentoConfig);
        entityManager.flush();

    }


    public ReportPage getFlussoGuida(Long contabilitaIniziale, Long contabilitaFinale, Integer page){
         return getFlussoPM(contabilitaIniziale, contabilitaFinale, null, page);
    }

    public ReportPage getFlussoRientrati(Long contabilitaIniziale, Long contabilitaFinale, Integer page){
        return getFlussoPM(contabilitaIniziale, contabilitaFinale, Constants.INVIATO_A_NEED, page);
    }



    private ReportPage getFlussoPM(Long contabilitaIniziale, Long contabilitaFinale, String statoPM, Integer page){

        String query = null;

        List<ProgrammaMusicale> listaPM = new ArrayList<ProgrammaMusicale>();

        ReportPage reportPage = new ReportPage(listaPM, listaPM.size(), page);

        int recordCount=0;

        query = "select p.numero_Programma_Musicale,p.flag_Da_Campionare,p.pm_Riferimento,p.codice_voce_Incasso,p.id_Programma_Musicale, p.RISULTATO_CALCOLO_RESTO,p.codice_campionamento, l.stato "+
                "from "+
                " (select distinct(p.numero_Programma_Musicale), p.flag_Da_Campionare,p.pm_Riferimento,m.codice_voce_Incasso,p.id_Programma_Musicale,p.RISULTATO_CALCOLO_RESTO,p.codice_campionamento "+
                "  from Programma_Musicale p, Movimento_Contabile_158 m "+
                "  where m.contabilita>=? and m.contabilita<=? "+
                "  and m.id_Programma_Musicale=p.id_Programma_Musicale and p.tipologia_Pm=? "+
                " ) p left join MOVIMENTAZIONE_LOGISTICA_PM l on p.id_Programma_Musicale=l.id_Programma_Musicale "+
                " and l.flag_attivo=? and l.flag_escluso_invio_need=?";


        Query q = entityManager.createNativeQuery(query);


        List<Object[]> results = (List<Object[]>)q
                            .setParameter(1,contabilitaIniziale)
                            .setParameter(2,contabilitaFinale)
                            .setParameter(3,Constants.PM_CARTACEO)
                            .setParameter(4,'Y')
                            .setParameter(5,'N')
                            .getResultList();

        if (results != null && results.size() > 0) {

            recordCount = results.size();

            int startRecord = 0;
            int endRecord = recordCount;

            if (page != -1) {

                startRecord = (page - 1) * ReportPage.NUMBER_RECORD_PER_PAGE;
                endRecord = 0;
                if (recordCount > ReportPage.NUMBER_RECORD_PER_PAGE) {
                    endRecord = startRecord + ReportPage.NUMBER_RECORD_PER_PAGE;
                    if (endRecord > recordCount)
                        endRecord = recordCount - 1;
                } else {
                    endRecord = recordCount;
                }

            }

            ProgrammaMusicale programma = null;
            String posizioneLogisticaPM = null;
            Character flag=null;
            for (int i = startRecord; i < endRecord; i++) {

                posizioneLogisticaPM = null;

                programma = new ProgrammaMusicale();

                programma.setNumeroProgrammaMusicale(((BigDecimal)results.get(i)[0]).longValue());
                if (results.get(i)[1]!=null) {
                    flag = ((String)results.get(i)[1]).charAt(0);
                    programma.setFlagDaCampionare(flag);
                }
                if (results.get(i)[2]!=null)
                programma.setPmRiferimento((String)results.get(i)[2]);
                programma.setVoceIncasso((String) results.get(i)[3]);
                programma.setId(((BigDecimal) results.get(i)[4]).longValue());
                if (results.get(i)[5]!=null)
                    programma.setRisultatoCalcoloResto(((BigDecimal) results.get(i)[5]).longValue());
                if (results.get(i)[6]!=null)
                    programma.setCodiceCampionamento(((BigDecimal) results.get(i)[6]).longValue());
                if (results.get(i)[7]!=null)
                    posizioneLogisticaPM = (String) results.get(i)[7];

                // Il PM non ha nessuno stato per cui può essere inviato a NEED
                if ( (statoPM==null && posizioneLogisticaPM==null) ||
                     (statoPM!=null && posizioneLogisticaPM!=null && posizioneLogisticaPM.equalsIgnoreCase(statoPM)))
                  listaPM.add(programma);
            }

            //recordCount = listaPM.size();

            reportPage = new ReportPage(listaPM, recordCount, page);

        }

        return reportPage ;

    }

    public ReportPage getCampionamenti(Long contabilitaIniziale, Long contabilitaFinale, Integer page){

        String query = null;

        List<CampionamentoConfig> results = new ArrayList<CampionamentoConfig>();

        ReportPage reportPage = new ReportPage(results, results.size(), page);

        int recordCount=0;

        query = "select c from CampionamentoConfig c where c.contabilita>=:contabilitaIniziale and c.contabilita<=:contabilitaFinale order by c.contabilita, c.flagAttivo desc";

        Query q = entityManager.createQuery(query);

        results = (List<CampionamentoConfig>)q
                .setParameter("contabilitaIniziale",contabilitaIniziale)
                .setParameter("contabilitaFinale",contabilitaFinale)
                .getResultList();

        if (results != null && results.size() > 0) {

            recordCount = results.size();

            int startRecord = (page-1) * ReportPage.NUMBER_RECORD_PER_PAGE;
            int endRecord = 0;
            if (recordCount>ReportPage.NUMBER_RECORD_PER_PAGE) {
                endRecord = startRecord + ReportPage.NUMBER_RECORD_PER_PAGE;
                if (endRecord > recordCount)
                    endRecord = recordCount -1;
            }else {
                endRecord = recordCount;
            }

            reportPage = new ReportPage(results, recordCount, page);

        }

        return reportPage ;

    }


    public ReportPage getStoricoEsecuzioni(Long periodoIniziale, Long periodoFinale, Integer page){

        String query = null;
        String serviceName="Campionamento";
        String methodName="/campionamentoEsecuzione";

        String periodoInizialeStr = periodoIniziale.toString()+"01";
        String periodoFinaleStr = periodoFinale.toString()+"01";

        Date initialData = Utility.stringDate(periodoInizialeStr,Utility.Format_YYYYMMdd);
        Date finalData = Utility.stringDate(periodoFinaleStr,Utility.Format_YYYYMMdd);


        List<TracciamentoApplicativo> results = new ArrayList<TracciamentoApplicativo>();

        ReportPage reportPage = new ReportPage(results, results.size(), page);

        int recordCount=0;

        query = "select c from TracciamentoApplicativo c "+
                "where c.serviceName=:serviceName and c.methodName=:methodName "+
                "and c.dataInserimento>=:periodoIniziale and c.dataInserimento<=:periodoFinale "+
                "order by c.dataInserimento desc";

        Query q = entityManager.createQuery(query);

        results = (List<TracciamentoApplicativo>)q
                .setParameter("serviceName",serviceName)
                .setParameter("methodName",methodName)
                .setParameter("periodoIniziale",initialData)
                .setParameter("periodoFinale",finalData)
                .getResultList();

        if (results != null && results.size() > 0) {

            recordCount = results.size();

            int startRecord = (page-1) * ReportPage.NUMBER_RECORD_PER_PAGE;
            int endRecord = 0;
            if (recordCount>ReportPage.NUMBER_RECORD_PER_PAGE) {
                endRecord = startRecord + ReportPage.NUMBER_RECORD_PER_PAGE;
                if (endRecord > recordCount)
                    endRecord = recordCount -1;
            }else {
                endRecord = recordCount;
            }

            reportPage = new ReportPage(results, recordCount, page);

        }

        return reportPage ;


    }


    public List<TracciamentoApplicativo> getEsecuzioniCampionamentoEngine(Long periodoIniziale, Long periodoFinale){

        String query = null;
        String serviceName = "CampionamentoInstance";
        String methodName = "startCampionamento";
        String message = "Periodo contabile da:";

        String periodoInizialeStr = periodoIniziale.toString()+"01";
        String periodoFinaleStr = periodoFinale.toString()+"01";

        Date initialData = Utility.stringDate(periodoInizialeStr,Utility.Format_YYYYMMdd);
        Date finalData = Utility.stringDate(periodoFinaleStr,Utility.Format_YYYYMMdd);

        List<TracciamentoApplicativo> results = new ArrayList<TracciamentoApplicativo>();

        query = "select c from TracciamentoApplicativo c "+
                "where c.serviceName=:serviceName and c.methodName=:methodName "+
                "and c.dataInserimento>=:periodoIniziale and c.dataInserimento<=:periodoFinale "+
                "and c.message like :message "+
                "order by c.dataInserimento desc";

        Query q = entityManager.createQuery(query);

        results = (List<TracciamentoApplicativo>)q
                .setParameter("serviceName",serviceName)
                .setParameter("methodName",methodName)
                .setParameter("periodoIniziale",initialData)
                .setParameter("periodoFinale",finalData)
                .setParameter("message", "%"+message+"%")
                .getResultList();

        return results;

    }


    public List<TracciamentoApplicativo> getEsecuzioniCampionamentoStart(Long periodoIniziale, Long periodoFinale){

        String query = null;
        String serviceName="Campionamento";
        String methodName="/campionamentoEsecuzione";

        String periodoInizialeStr = periodoIniziale.toString()+"01";
        String periodoFinaleStr = periodoFinale.toString()+"01";

        Date initialData = Utility.stringDate(periodoInizialeStr,Utility.Format_YYYYMMdd);
        Date finalData = Utility.stringDate(periodoFinaleStr,Utility.Format_YYYYMMdd);

        List<TracciamentoApplicativo> results = new ArrayList<TracciamentoApplicativo>();


        query = "select c from TracciamentoApplicativo c "+
                "where c.serviceName=:serviceName and c.methodName=:methodName "+
                "and c.dataInserimento>=:periodoIniziale and c.dataInserimento<=:periodoFinale "+
                "order by c.dataInserimento desc";

        Query q = entityManager.createQuery(query);

        results = (List<TracciamentoApplicativo>)q
                .setParameter("serviceName",serviceName)
                .setParameter("methodName",methodName)
                .setParameter("periodoIniziale",initialData)
                .setParameter("periodoFinale",finalData)
                .getResultList();

        return results;

    }

}
