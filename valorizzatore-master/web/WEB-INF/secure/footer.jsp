<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!-- INIZIO BARRA FOOTER -->
<br/><br/>
<input id="activityReminder" class="hide noprint" type="hidden" value="60">
  <footer class="noprint"><div class="vtFooter"><p>Powered by SIAE - &#169 2016&nbsp;
    <a href="http://www.siae.it/" target="_blank">siae.it</a>
  </p></div>

  </footer>
<!-- FINE BARRA FOOTER -->


</div>
</div>
</div>

    <script type="text/javascript" src="${contextPath}/js/jquery-1.10.2.js"></script>
    <!--<script type="text/javascript" src="${contextPath}/js/jquery.blockUI.js"></script>-->
    <!--<script type="text/javascript" src="${contextPath}/js/chosen.jquery.min.js"></script>-->
	<script type="text/javascript" src="${contextPath}/js/select2.min.js"></script>
	<!--script type="text/javascript" src="${contextPath}/js/jquery-ui-1.8.16.custom.min.js"></script-->
    <script type="text/javascript" src="${contextPath}/js/jquery-ui.js"></script>
    <!--<script type="text/javascript" src="${contextPath}/js/jquery.class.min.js"></script>-->
	<!--<script type="text/javascript" src="${contextPath}/js/jquery.pjax.js"></script> -->
	<script type="text/javascript" src="${contextPath}/js/jstorage.min.js"></script>
	<!--<script type="text/javascript" src="${contextPath}/js/jquery.autosize-min.js"></script> -->
	<script type="text/javascript" src="${contextPath}/js/slimScroll.min.js"></script>
	<!--<script type="text/javascript" src="${contextPath}/js/jquery.pnotify.min.js"></script> -->
	<!--<script type="text/javascript" src="${contextPath}/js/jquery.hoverIntent.minified.js"></script>-->
	<script type="text/javascript" src="${contextPath}/js/bootstrap-alert.js"></script>
	<script type="text/javascript" src="${contextPath}/js/bootstrap-tooltip.js"></script>
	<script type="text/javascript" src="${contextPath}/js/bootstrap-tab.js"></script>
	<script type="text/javascript" src="${contextPath}/js/bootstrap-collapse.js"></script>
	<script type="text/javascript" src="${contextPath}/js/bootstrap-modal.js"></script>
	<script type="text/javascript" src="${contextPath}/js/bootstrap-dropdown.js"></script>
	<script type="text/javascript" src="${contextPath}/js/bootstrap-popover.js"></script>
	<script type="text/javascript" src="${contextPath}/js/bootbox.min.js"></script>
	<!--<script type="text/javascript" src="${contextPath}/js/jquery.additions.js"></script> -->
    <!--<script type="text/javascript" src="${contextPath}/js/app.js"></script> -->
<!--<script type="text/javascript" src="${contextPath}/js/helper.js"></script> -->
	<script type="text/javascript" src="${contextPath}/js/Connector.js"></script>
<!--<script type="text/javascript" src="${contextPath}/js/ProgressIndicator.js"></script> -->
	<!--<script type="text/javascript" src="${contextPath}/js/jquery.validationEngine.js"></script>-->
	<script type="text/javascript" src="${contextPath}/js/guiders-1.2.6.js"></script>
	<!--<script type="text/javascript" src="${contextPath}/js/jquery.ba-outside-events.min.js"></script> -->
	<!--<script type="text/javascript" src="${contextPath}/js/jquery.placeholder.js"></script> -->

    <!--<script type="text/javascript" src="${contextPath}/js/datepicker.js"></script> -->
    <!--<script type="text/javascript" src="${contextPath}/js/date.js"></script> -->
    <!--<script type="text/javascript" src="${contextPath}/js/bootstrap-datepicker.js"></script>-->
    <!--<script type="text/javascript" src="${contextPath}/js/bootstrap-datepicker.it.js"></script>-->

    <!--<script type="text/javascript" src="${contextPath}/js/jquery.timepicker.min.js"></script>-->
<!--<script type="text/javascript" src="${contextPath}/js/Header.js"></script> -->
<!--<script type="text/javascript" src="${contextPath}/js/Edit.js"></script> -->
<!--<script type="text/javascript" src="${contextPath}/js/Edit(1).js"></script> -->
<!--<script type="text/javascript" src="${contextPath}/js/Popup.js"></script> -->
<!--<script type="text/javascript" src="${contextPath}/js/Field.js"></script> -->
<!--<script type="text/javascript" src="${contextPath}/js/BaseValidator.js"></script> -->
<!--<script type="text/javascript" src="${contextPath}/js/FieldValidator.js"></script> -->
    <!--<script type="text/javascript" src="${contextPath}/js/jquery_windowmsg.js"></script> -->
<!--<script type="text/javascript" src="${contextPath}/js/BasicSearch.js"></script> -->
<!--<script type="text/javascript" src="${contextPath}/js/AdvanceFilter.js"></script> -->
<!--<script type="text/javascript" src="${contextPath}/js/SearchAdvanceFilter.js"></script> -->
<!--<script type="text/javascript" src="${contextPath}/js/AdvanceSearch.js"></script> -->
    <!--<script type="text/javascript" src="${contextPath}/js/jquery.validationEngine-it.js"></script>-->
<!--<script type="text/javascript" src="${contextPath}/js/ExtensionStore.js"></script> -->
<!--<script type="text/javascript" src="${contextPath}/js/VGSHideAdsActions.js"></script> -->
<!--<script type="text/javascript" src="${contextPath}/js/Vtiger.js"></script> -->

	<!-- Added in the end since it should be after less file loaded -->
	<script type="text/javascript" src="${contextPath}/js/less.min.js"></script>


	<script type="text/javascript">
		$(document).ready(function() {

			var msg="";
			var elements = document.getElementsByTagName("INPUT");
			for (var i = 0; i < elements.length; i++) {
				elements[i].oninvalid = function(e) {
					e.target.setCustomValidity("");

					switch(e.target.id){
						case "password" :
						msg="Password errata";
						case "username" :
						msg="La Username non puo' essere vuota";
						default :
						msg="Campo obbligatorio";break;
					}

					if (!e.target.validity.valid) {
						e.target.setCustomValidity(msg);
					}
				};
				elements[i].oninput = function(e) {
					e.target.setCustomValidity("");
				};

		} })
	</script>


</div>
