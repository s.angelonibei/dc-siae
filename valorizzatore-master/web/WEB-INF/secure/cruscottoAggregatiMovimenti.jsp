<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>

 <jsp:include page="header.jsp"/>

<body data-language="it_it">

<div id="page">

    <!-- container which holds data temporarly for pjax calls -->
    <div id="pjaxContainer" class="hide noprint"></div>

    <!-- INIZIO INTESTAZIONE-->
    <jsp:include page="menu_bar.jsp"/>
    <!-- FINE INTESTAZIONE-->

    <!-- INIZIO BARRA DI RICERCA -->
    <jsp:include page="search_bar_cruscottiAggregatiMovimenti.jsp"/>
    <!-- FINE BARRA DI RICERCA -->

    <script type="text/javascript">

    </script>

    <!-- BODY -->
    <form class="form-horizontal recordEditView" id="ListaAggregatiViewId" name="ListaAggregatiView" method="get" action="${contextPath}/secure/movimentiDetail">

        <!-- INIZIO SEZIONE VIEW DATI -->

        <!-- Report(1): Inizio Lista importi PM Correnti-->
        <c:if test="${listaImportiCorrenti!=null and empty listaImportiCorrenti.records and (correntiArretrati=='T' or correntiArretrati=='C')}">
            <div class="listViewEntriesDiv">
                    <br/><br/>
                    <b>Nessun Programma Musicale corrente per il periodo selezionato</b>
                    <br/><br/><br/>
            </div>
        </c:if>

        <c:if test="${listaImportiCorrenti.records.size() > 0}">

            <div class="listViewEntriesDiv">
                <div style="width: 1308px;">

                    <table class="table table-bordered listViewEntriesTable">
                        <thead>
                        <tr class="listViewHeaders">
                            <th nowrap="" valign="center" align="center" colspan="6">
                                <a class="listViewHeaderValues" >Programmi Musicali Correnti</a>
                            </th>
                        </tr>
                        <tr class="listViewHeaders">
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Voce<br/>Incasso</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Importo<br/>Netto Incassi</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Importo<br/>Non Programmato</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Importo<br/>Sospesi</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Importo<br/>Annullati</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Importo<br/>Programmato</a>
                            </th>
                        </tr>
                        </thead>

                        <tbody>

                        <c:forEach items="${listaImportiCorrenti.records}" var="m">
                            <tr id="row_${m.voceIncasso}_${m.importo}" class="listViewEntries">
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.voceIncasso}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.importoFormatted}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.importo566Formatted}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.importoSospesiFormatted}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.importoAnnullatiFormatted}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.importoProgrammatoFormatted}</td>
                            </tr>
                        </c:forEach>

                        </tbody>
                    </table>
         </c:if>
         <!-- Report(1): Fine Lista importi PM Correnti-->

         <br/>

         <!-- Report(2): Inizio Numero Cedole PM Correnti-->
         <c:if test="${listaCedolePMCorrenti.records.size() > 0}">

                    <div class="listViewEntriesDiv">
                        <div style="width: 1308px;">

                            <table class="table table-bordered listViewEntriesTable">
                                <thead>
                                <tr class="listViewHeaders">
                                    <th nowrap="" valign="center" align="center">
                                        <a class="listViewHeaderValues" href="javascript:void(0);">Voce<br/>Incasso</a>
                                    </th>
                                    <th nowrap="" valign="center" align="center">
                                        <a class="listViewHeaderValues" href="javascript:void(0);">Numero<br/>Programmi Musicali</a>
                                    </th>
                                    <th nowrap="" valign="center" align="center">
                                        <a class="listViewHeaderValues" href="javascript:void(0);">Numero<br/>Cedole</a>
                                    </th>
                                    <th nowrap="" valign="center" align="center">
                                        <a class="listViewHeaderValues" href="javascript:void(0);">Importo<br/>Programmato</a>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <c:forEach items="${listaCedolePMCorrenti.records}" var="m">
                                    <tr id="row_${m.voceIncasso}_${m.importoFormatted}" class="listViewEntries">
                                        <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.voceIncasso}</td>
                                        <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.numeroPM}</td>
                                        <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.numeroCedole}</td>
                                        <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.importoFormatted}</td>
                                    </tr>
                                </c:forEach>

                                </tbody>
                            </table>
          </c:if>
          <!-- Report(2): Fine Numero Cedole PM Correnti-->

          <br/>

          <!-- Sezione Arretrati -->

            <!-- Report(1): Inizio Lista importi PM Arretrati-->
            <c:if test="${listaImportiArretrati!=null and empty listaImportiArretrati.records and (correntiArretrati=='T' or correntiArretrati=='A')}">
                    <div class="listViewEntriesDiv">
                        <br/><br/>
                        <b>Nessun Programma Musicale arretrato per il periodo selezionato</b>
                        <br/><br/><br/>
                    </div>
            </c:if>

            <c:if test="${listaImportiArretrati.records.size() > 0}">

                            <div class="listViewEntriesDiv">
                                <div style="width: 1308px;">

                                    <table class="table table-bordered listViewEntriesTable">
                                        <thead>
                                        <tr class="listViewHeaders">
                                            <th nowrap="" valign="center" align="center" colspan="6">
                                                <a class="listViewHeaderValues" >Programmi Musicali Arretrati</a>
                                            </th>
                                        </tr>
                                        <tr class="listViewHeaders">
                                            <th nowrap="" valign="center" align="center">
                                                <a class="listViewHeaderValues" href="javascript:void(0);">Voce<br/>Incasso</a>
                                            </th>
                                            <th nowrap="" valign="center" align="center">
                                                <a class="listViewHeaderValues" href="javascript:void(0);">Importo<br/>Sospesi</a>
                                            </th>
                                            <th nowrap="" valign="center" align="center">
                                                <a class="listViewHeaderValues" href="javascript:void(0);">Importo<br/>Annullati</a>
                                            </th>
                                            <th nowrap="" valign="center" align="center">
                                                <a class="listViewHeaderValues" href="javascript:void(0);">Importo<br/>Programmato</a>
                                            </th>
                                        </tr>
                                        </thead>

                                        <tbody>

                                        <c:forEach items="${listaImportiArretrati.records}" var="m">
                                            <tr id="row_${m.voceIncasso}_${m.importo}" class="listViewEntries">
                                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.voceIncasso}</td>
                                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.importoSospesiFormatted}</td>
                                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.importoAnnullatiFormatted}</td>
                                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.importoProgrammatoFormatted}</td>
                                            </tr>
                                        </c:forEach>

                                        </tbody>
                                    </table>
                    </c:if>
                    <!-- Report(1): Fine Lista importi PM Correnti-->

                                    <br/>

                    <!-- Report(2): Inizio Numero Cedole PM Arretrati-->
                    <c:if test="${listaCedolePMArretrati.records.size() > 0}">

                                    <div class="listViewEntriesDiv">
                                        <div style="width: 1308px;">

                                            <table class="table table-bordered listViewEntriesTable">
                                                <thead>
                                                <tr class="listViewHeaders">
                                                    <th nowrap="" valign="center" align="center">
                                                        <a class="listViewHeaderValues" href="javascript:void(0);">Voce<br/>Incasso</a>
                                                    </th>
                                                    <th nowrap="" valign="center" align="center">
                                                        <a class="listViewHeaderValues" href="javascript:void(0);">Numero<br/>Programmi Musicali</a>
                                                    </th>
                                                    <th nowrap="" valign="center" align="center">
                                                        <a class="listViewHeaderValues" href="javascript:void(0);">Numero<br/>Cedole</a>
                                                    </th>
                                                    <th nowrap="" valign="center" align="center">
                                                        <a class="listViewHeaderValues" href="javascript:void(0);">Importo<br/>Programmato</a>
                                                    </th>
                                                </tr>
                                                </thead>

                                                <tbody>

                                                <c:forEach items="${listaCedolePMArretrati.records}" var="m">
                                                    <tr id="row_${m.voceIncasso}_${m.importo}" class="listViewEntries">
                                                        <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.voceIncasso}</td>
                                                        <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.numeroPM}</td>
                                                        <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.numeroCedole}</td>
                                                        <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.importoFormatted}</td>
                                                    </tr>
                                                </c:forEach>

                                                </tbody>
                                            </table>
           </c:if>
         <!-- FINE SEZIONE VIEW DATI -->


        <c:if test="${(listaImportiCorrenti!=null and not empty listaImportiCorrenti.records and listaImportiCorrenti.records.size() > 0) or
                      (listaCedolePMCorrenti!=null and not empty listaCedolePMCorrenti.records and listaCedolePMCorrenti.records.size() > 0) or
                      (listaImportiArretrati!=null and not empty listaImportiArretrati.records and listaImportiArretrati.records.size() > 0) or
                      (listaCedolePMArretrati!=null and not empty listaCedolePMArretrati.records and listaCedolePMArretrati.records.size() > 0)
        }">
            <!-- INIZIO TASTI FONDO PAGINA -->
            <br/><br/>
            <div class="row-fluid">
                <div class="pull-right">
                    <!--
                    <button class="btn btn-success" type="submit">
                        <strong>Salva</strong>
                    </button>
                    <a class="cancelLink" type="reset" onclick="javascript:window.history.back();">Annulla</a>
                    -->
                    <a href="${contextPath}/secure/exportReport?idReport=Aggregato_Movimenti">
                        <img src="${contextPath}/images/excel.jpg" alt="Esporta in formato excel" width="35" height="38"/>
                    </a>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- FINE TASTI FONDO PAGINA -->
        </c:if>

    </form>

    <!-- INIZIO FOOTER -->
    <jsp:include page="footer.jsp"/>
    <!-- FINE FOOTER -->

</body>
</html>