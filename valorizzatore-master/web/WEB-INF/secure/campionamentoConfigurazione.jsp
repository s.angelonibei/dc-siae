<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>

 <jsp:include page="header.jsp"/>

<body data-language="it_it">

<div id="page">

    <!-- container which holds data temporarly for pjax calls -->
    <div id="pjaxContainer" class="hide noprint"></div>

    <!-- INIZIO INTESTAZIONE-->
    <jsp:include page="menu_bar.jsp"/>
    <!-- FINE INTESTAZIONE-->

    <!-- INIZIO BARRA DI RICERCA -->
    <jsp:include page="search_bar.jsp"/>
    <!-- FINE BARRA DI RICERCA -->

    <!-- BODY -->
    <form class="form-horizontal recordEditView" id="CampionamentoFormId" name="CampionamentoForm" method="post" action="${contextPath}/secure/campionamentoConfigurazioneSave">

        <!-- INIZIO SEZIONE DATI -->

        <table border="0" >
            <thead>
            <tr>
                <th class="blockHeader" colspan="4">CONTABILITA'</th>
                <th class="blockHeader">INIZIO<br/>LAVORAZIONE</th>
                <th class="blockHeader">ESTRAZIONE<br/>LOTTO</th>
                <th class="blockHeader"></th>
                <th class="blockHeader"></th>
                <th class="blockHeader">RESTO<br/>MODULO 5</th>
                <th class="blockHeader">RESTO<br/>MODULO 61</th>
            </tr>
            </thead>
            <tr>
                <td colspan="4">
                    <select style="width: 110px" class="js-basic-single-select" id="mesePeriodoContabilitaId" name="mesePeriodoContabilita" required="true">
                        <option value="">-- Mese --</option>
                        <option value="1">Gennaio</option>
                        <option value="2">Febbraio</option>
                        <option value="3">Marzo</option>
                        <option value="4">Aprile</option>
                        <option value="5">Maggio</option>
                        <option value="6">Giugno</option>
                        <option value="7">Luglio</option>
                        <option value="8">Agosto</option>
                        <option value="9">Settembre</option>
                        <option value="10">Ottobre</option>
                        <option value="11">Novembre</option>
                        <option value="12">Dicembre</option>
                    </select>

                    <select style="width: 100px;" class="js-basic-single-select" id="annoPeriodoContabilitaId" name="annoPeriodoContabilita" required="true">
                        <option value="">-- Anno --</option>
                        <option value="2015">2015</option>
                        <option value="2016">2016</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                    </select>

                </td>
                <td valign="center">
                    <input type="text" style="width: 100px;"  value="${dataInizioLavorazione}" name="dataInizioLavorazione" id="dataInizioLavorazioneId" required="true">
                </td>
                <td valign="center">
                    <input type="text" style="width: 100px;" value="${dataEstrazioneLotto}" name="dataEstrazioneLotto" id="dataEstrazioneLottoId" required="true">
                </td>
                <td valign="center">

                    <table border="0">
                        <tr>
                            <td height="20"></td>
                            <td height="20"></td>
                        </tr>
                    </table>

                    <table border="1" style="border-color: blue;">
                        <tr style="border-color: blue;">
                            <td style="border-color: blue;" height="25" align="center" valign="center">BSM</td>
                            <td style="border-color: blue;" height="25" align="center" valign="center">ROMA</td>
                        </tr>
                        <tr style="border-color: blue;">
                            <td style="border-color: blue;" height="25" align="center" valign="center">CONCERTINI</td>
                            <td style="border-color: blue;" height="25" align="center" valign="center">MILANO</td>
                        </tr>
                    </table>
                </td>

                <td>

                    <table border="1" style="border-color: blue;">
                        <tr>
                            <td align="center" style="width: 59px;">1</td>
                            <td align="center" style="width: 59px;">2</td>
                            <td align="center" style="width: 59px;">3</td>
                            <td align="center" style="width: 59px;">4</td>
                            <td align="center" style="width: 59px;">5</td>
                        </tr>
                    </table>
                    <table border="0">
                        <tr>
                            <td><input style="width: 50px;" type="text" value="${bsm1}" name="bsm1" id="bsm1Id" required="true"/></td>
                            <td><input style="width: 50px;" type="text" value="${bsm2}" name="bsm2" id="bsm2Id" required="true"/></td>
                            <td><input style="width: 50px;" type="text" value="${bsm3}" name="bsm3" id="bsm3Id" required="true"/></td>
                            <td><input style="width: 50px;" type="text" value="${bsm4}" name="bsm4" id="bsm4Id" required="true"/></td>
                            <td><input style="width: 50px;" type="text" value="${bsm5}" name="bsm5" id="bsm5Id" required="true"/></td>
                        </tr>
                        <tr>
                            <td><input style="width: 50px;" type="text" value="${concertini1}" name="concertini1" id="concertini1Id" required="true"/></td>
                            <td><input style="width: 50px;" type="text" value="${concertini2}" name="concertini2" id="concertini2Id" required="true"/></td>
                            <td><input style="width: 50px;" type="text" value="${concertini3}" name="concertini3" id="concertini3Id" required="true"/></td>
                            <td><input style="width: 50px;" type="text" value="${concertini4}" name="concertini4" id="concertini4Id" required="true"/></td>
                            <td><input style="width: 50px;" type="text" value="${concertini5}" name="concertini5" id="concertini5Id" required="true"/></td>
                        </tr>
                    </table>

                </td>

                <td align="center" valign="center">
                    <table border="0">
                        <tr>
                            <td height="20"></td>
                            <td height="20"></td>
                        </tr>
                    </table>
                    <table border="0">
                        <tr>
                            <td align="center" valign="center"><input style="width: 50px;" type="text" readonly value="${resto5bsm}" name="resto5bsm" id="resto5bsmId" required="true"/></td>
                        </tr>
                        <tr>
                            <td align="center" valign="center"><input style="width: 50px;" type="text" readonly value="${resto5concertini}" name="resto5concertini" id="resto5concertiniId" required="true"/></td>
                        </tr>
                    </table>
                </td>

                <td align="center" valign="center">
                    <table border="0">
                        <tr>
                            <td height="20"></td>
                            <td height="20"></td>
                        </tr>
                    </table>
                    <table border="0">
                        <tr>
                            <td align="center" valign="center"><input style="width: 50px;" type="text" readonly value="${resto61bsm}" name="resto61bsm" id="resto61bsmId" required="true"/></td>
                        </tr>
                        <tr>
                            <td align="center" valign="center"><input style="width: 50px;" type="text" readonly value="${resto61concertini}" name="resto61concertini" id="resto61concertiniId" required="true"/></td>
                        </tr>
                    </table>
                </td>

            </tr>

        </table>

        <!-- FINE SEZIONE DATI -->
        <br/>
        <center><span style="color: red">${message}</span></center>

        <!-- INIZIO TASTI FONDO PAGINA -->
        <br/><br/>

        <div class="row-fluid">
            <div class="pull-right">
                <button class="btn btn-success" type="submit">
                    <strong>Salva</strong>
                </button>
            </div>
            <div class="clearfix"></div>
        </div>

        <!-- FINE TASTI FONDO PAGINA -->

    </form>

    <script type="text/javascript">

               var noIntMessage = "Attenzione!\nOgni valore deve essere un intero compreso tra 1 e 90";
               var duplicateValueMessage = "Attenzione !\nNon e' possibile inserire valori duplicati.";
               var emptyMessageBsm = "Attenzione!\nInserire tutti e 5 i valori necessari per la configurazione BSM.";
               var emptyMessageConcertini = "Attenzione!\nInserire tutti e 5 i valori necessari per la configurazione Concertini.";

			   $("#bsm1Id").on("change", function() {
			        if (!isCorrectInt($(this).val())){
			            alert(noIntMessage);
			        }else{
			          startCalcoloRestoBsm();
			        }
    		   });

			   $("#bsm2Id").on("change", function() {
			        if (!isCorrectInt($(this).val())){
			            alert(noIntMessage);
			        }else{
			          startCalcoloRestoBsm();
			        }
    		   });
			   $("#bsm3Id").on("change", function() {
			        if (!isCorrectInt($(this).val())){
			            alert(noIntMessage);
			        }else{
			          startCalcoloRestoBsm();
			        }
    		   });
			   $("#bsm4Id").on("change", function() {
			        if (!isCorrectInt($(this).val())){
			            alert(noIntMessage);
			        }else{
			          startCalcoloRestoBsm();
			        }
    		   });
			   $("#bsm5Id").on("change", function() {
			        if (!isCorrectInt($(this).val())){
			            alert(noIntMessage);
			        }else{
			          startCalcoloRestoBsm();
			        }
    		   });


			   $("#concertini1Id").on("change", function() {
			        if (!isCorrectInt($(this).val())){
			            alert(noIntMessage);
			        }else{
			          startCalcoloRestoConcertini();
			        }
    		   });

			   $("#concertini2Id").on("change", function() {
			        if (!isCorrectInt($(this).val())){
			            alert(noIntMessage);
			        }else{
			          startCalcoloRestoConcertini();
			        }
    		   });
			   $("#concertini3Id").on("change", function() {
			        if (!isCorrectInt($(this).val())){
			            alert(noIntMessage);
			        }else{
			          startCalcoloRestoConcertini();
			        }
    		   });
			   $("#concertini4Id").on("change", function() {
			        if (!isCorrectInt($(this).val())){
			            alert(noIntMessage);
			        }else{
			          startCalcoloRestoConcertini();
			        }
    		   });
			   $("#concertini5Id").on("change", function() {
			        if (!isCorrectInt($(this).val())){
			            alert(noIntMessage);
			        }else{
			          startCalcoloRestoConcertini();
			        }
    		   });

               function startCalcoloRestoBsm(){

			         if ($("#bsm1Id").val()!='' && $("#bsm2Id").val()!='' && $("#bsm3Id").val()!='' && $("#bsm4Id").val()!='' && $("#bsm5Id").val()!=''){

                         var canGo = true;

                         if ($("#bsm1Id").val()==$("#bsm2Id").val() || $("#bsm1Id").val()==$("#bsm3Id").val() || $("#bsm1Id").val()==$("#bsm4Id").val() || $("#bsm1Id").val()==$("#bsm5Id").val() ||
                             $("#bsm2Id").val()==$("#bsm3Id").val() || $("#bsm2Id").val()==$("#bsm4Id").val() || $("#bsm2Id").val()==$("#bsm5Id").val() ||
                             $("#bsm3Id").val()==$("#bsm4Id").val() || $("#bsm3Id").val()==$("#bsm5Id").val() ||
                             $("#bsm4Id").val()==$("#bsm5Id").val()
                         ) {
                             alert(duplicateValueMessage);
                             canGo = false;
                         }

                         if (canGo==true){
                             var x = $("#bsm1Id").val();
                             var r;
                             var p;

                             if (x>=0 && x<=18) r = 0;
                             else if (x>=19 && x<=36) r = 1;
                             else if (x>=37 && x<=54) r = 2;
                             else if (x>=55 && x<=72) r = 3;
                             else if (x>=73 && x<=90) r = 4;
                             $("#resto5bsmId").val(r);

                             p = $("#bsm1Id").val()*$("#bsm2Id").val()*$("#bsm3Id").val()*$("#bsm4Id").val()*$("#bsm5Id").val();
                             r = p % 61;
                             $("#resto61bsmId").val(r);
                         }

			         }
               }

               function startCalcoloRestoConcertini(){

			         if ($("#concertini1Id").val()!='' && $("#concertini2Id").val()!='' && $("#concertini3Id").val()!='' && $("#concertini4Id").val()!='' && $("#concertini5Id").val()!=''){

                         var canGo = true;

                         if ($("#concertini1Id").val()==$("#concertini2Id").val() || $("#concertini1Id").val()==$("#concertini3Id").val() || $("#concertini1Id").val()==$("#concertini4Id").val() || $("#concertini1Id").val()==$("#concertini5Id").val() ||
                             $("#concertini2Id").val()==$("#concertini3Id").val() || $("#concertini2Id").val()==$("#concertini4Id").val() || $("#concertini2Id").val()==$("#concertini5Id").val() ||
                             $("#concertini3Id").val()==$("#concertini4Id").val() || $("#concertini3Id").val()==$("#concertini5Id").val() ||
                             $("#concertini4Id").val()==$("#concertini5Id").val()
                         ) {
                             alert(duplicateValueMessage);
                             canGo = false;
                         }

                         if (canGo==true){
                             var x = $("#concertini1Id").val();
                             var r;
                             var p;

                             if (x>=0 && x<=18) r = 0;
                             else if (x>=19 && x<=36) r = 1;
                             else if (x>=37 && x<=54) r = 2;
                             else if (x>=55 && x<=72) r = 3;
                             else if (x>=73 && x<=90) r = 4;
                             $("#resto5concertiniId").val(r);

                             p = $("#concertini1Id").val()*$("#concertini2Id").val()*$("#concertini3Id").val()*$("#concertini4Id").val()*$("#concertini5Id").val();
                             r = p % 61;
                             $("#resto61concertiniId").val(r);
                         }
			         }
               }

               function isCorrectInt(value) {
  					var isValidNumber =  !isNaN(value) && parseInt(Number(value)) == value && !isNaN(parseInt(value, 10));
                    if (isValidNumber){
                       if (value<0 || value>100)
                          isValidNumber = false;
                    }

  					return isValidNumber;
               }

            $(function() {
               $( "#dataInizioLavorazioneId" ).datepicker();
               $( "#dataEstrazioneLottoId" ).datepicker();

               $("#mesePeriodoContabilitaId").val("${mesePeriodoContabilita}");
               $("#annoPeriodoContabilitaId").val("${annoPeriodoContabilita}");

            });

    </script>


    <!-- INIZIO FOOTER -->
    <jsp:include page="footer.jsp"/>
    <!-- FINE FOOTER -->

</body>
</html>