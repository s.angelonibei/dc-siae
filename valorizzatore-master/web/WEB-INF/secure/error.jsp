<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>

<jsp:include page="header.jsp"/>

<body data-language="it_it">

<div id="page">

    <!-- container which holds data temporarly for pjax calls -->
    <div id="pjaxContainer" class="hide noprint"></div>

    <!-- INIZIO INTESTAZIONE-->
    <jsp:include page="menu_bar.jsp"/>
    <!-- FINE INTESTAZIONE-->

    <!-- INIZIO BARRA DI RICERCA -->
    <jsp:include page="search_bar.jsp"/>
    <!-- FINE BARRA DI RICERCA -->

    <!-- BODY -->
    <form class="form-horizontal recordEditView" id="errorFormId" name="errorForm" method="post" action="#">

        <div class="contents-topscroll noprint">
            <div class="topscroll-div" style="width: 1308px; height: 70px;">
            </div>
        </div>

        <div class="listViewEntriesDiv contents-bottomscroll">
            <div class="bottomscroll-div" style="width: 1308px;">
                <br/><br/>
                <b>Si &eacute; verificato un errore:</b><br/>
                ${exception}
                <br/><br/><br/>
            </div>
        </div>

    </form>

    <!-- INIZIO FOOTER -->
    <jsp:include page="footer.jsp"/>
    <!-- FINE FOOTER -->

</body>
</html>