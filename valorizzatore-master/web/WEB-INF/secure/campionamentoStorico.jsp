<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>

 <jsp:include page="header.jsp"/>

<body data-language="it_it">


<div id="page">

    <!-- container which holds data temporarly for pjax calls -->
    <div id="pjaxContainer" class="hide noprint"></div>

    <!-- INIZIO INTESTAZIONE-->
    <jsp:include page="menu_bar.jsp"/>
    <!-- FINE INTESTAZIONE-->

    <!-- INIZIO BARRA DI RICERCA -->
    <jsp:include page="search_bar_campionamentoStorico.jsp"/>
    <!-- FINE BARRA DI RICERCA -->


    <!-- BODY -->
    <form class="form-horizontal recordEditView" id="EditView" name="EditView" method="post" action="">

        <!-- INIZIO SEZIONE VIEW DATI -->

        <c:if test="${resultPage!=null and empty resultPage.records}">

            <div class="contents-topscroll noprint">
                <div class="topscroll-div" style="width: 1308px; height: 70px;">

                </div>
            </div>

            <div class="listViewEntriesDiv">
                <div class="bottomscroll-div" style="width: 1308px;">
                    <b>Nessun dato ritornato</b>
                    <br/><br/><br/>
                </div>
            </div>
        </c:if>

            <c:if test="${resultPage.records.size() > 0}">

            <div class="listViewEntriesDiv">

                    <br/>
                    Numero configurazioni: <b>${resultPage.records.size()}</b> di <b>${resultPage.totRecords}</b>
                    <br/>
                    <center>

                        <c:if test="${resultPage.pages.size() > 1}">
                            <c:if test="${resultPage.currentPage > 1}">
                              <a href="javascript:callPage(${resultPage.currentPage-1});">< Prec</a>
                            </c:if>

                            <c:forEach items="${resultPage.pages}" var="p">
                                <c:choose>
                                    <c:when test="${resultPage.currentPage != p}">
                                        <a href="javascript:callPage(${p});">${p} </a>
                                    </c:when>
                                    <c:otherwise>
                                        <a style="color: #09b6eb;">${p} </a>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>

                            <c:if test="${resultPage.currentPage < resultPage.pages.size()}">
                               <a href="javascript:callPage(${resultPage.currentPage+1});"> > Suc </a>
                            </c:if>
                        </c:if>

                    </center>
                    <br/><br/>
                    <table class="table table-bordered">
                        <thead>
                        <tr class="listViewHeaders">
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues">Contabilita'</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues">Inizio<br/>Lavorazione</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues">Estrazione<br/>Lotto</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues">Resto Modulo 5</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues">Resto Modulo 61</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues">Attiva</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues">Ultima modifica</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues">Utente</a>
                            </th>
                        </tr>
                        </thead>

                        <tbody>

                        <c:forEach items="${resultPage.records}" var="m">
                            <tr id="row_${m.id}" class="listViewEntries">
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.descrizioneContabilita}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.dataInizioLavorazioneStr}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.dataEstrazioniLottoStr}</td>

                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                                    <table border="1" style="border-color: blue;">
                                        <tr style="border-color: blue;">
                                            <td style="border-color: blue;" height="25" align="center" valign="center">BSM</td>
                                            <td style="border-color: blue;" height="25" align="center" valign="center">ROMA</td>
                                        </tr>
                                        <tr style="border: thin blue">
                                            <td style="border-color: blue;" height="25" align="center" valign="center">CONCERTINI</td>
                                            <td style="border-color: blue;" height="25" align="center" valign="center">MILANO</td>
                                        </tr>
                                    </table>
                                </td>

                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">

                                    <table>
                                        <tr>
                                            <td height="25" align="center" valign="center">${m.restoRoma1}</td>
                                        </tr>
                                        <tr>
                                            <td height="25" align="center" valign="center">${m.restoRoma2}</td>
                                        </tr>
                                    </table>

                                </td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">

                                    <table>
                                        <tr>
                                            <td height="25" align="center" valign="center">${m.restoMilano1}</td>
                                        </tr>
                                        <tr>
                                            <td height="25" align="center" valign="center">${m.restoMilano2}</td>
                                        </tr>
                                    </table>

                                </td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.flagAttivo}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.dataOraUltimaModificaStr}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.utenteUltimaModifica}</td>

                            </tr>
                        </c:forEach>

                        </tbody>
                    </table>
                    </c:if>


                <!-- FINE SEZIONE VIEW DATI -->


        <!-- INIZIO TASTI FONDO PAGINA -->
        <br/>
        <center><span style="color: red">${message}</span></center>
        <br/>
        <!--
        <div class="row-fluid">
            <div class="pull-right">
                <button class="btn btn-success" type="submit">
                    <strong>Salva</strong>
                </button>
                <a class="cancelLink" type="reset" onclick="javascript:window.history.back();">Annulla</a>
            </div>
            <div class="clearfix"></div>
        </div>
        -->
        <!-- FINE TASTI FONDO PAGINA -->

    </form>

    <!-- INIZIO FOOTER -->
    <jsp:include page="footer.jsp"/>
    <!-- FINE FOOTER -->

</body>
</html>