<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>

 <jsp:include page="header.jsp"/>

<body data-language="it_it">

<div id="page">

    <!-- container which holds data temporarly for pjax calls -->
    <div id="pjaxContainer" class="hide noprint"></div>

    <!-- INIZIO INTESTAZIONE-->
    <jsp:include page="menu_bar.jsp"/>
    <!-- FINE INTESTAZIONE-->

    <!-- INIZIO BARRA DI RICERCA -->
    <jsp:include page="search_bar_cruscottiMovimenti.jsp"/>
    <!-- FINE BARRA DI RICERCA -->

    <script type="text/javascript">

       function openPMDetail(idProgrammaMusicale){
           document.forms.ListaMovimentiViewId.elements.idProgrammaMusicale.value=idProgrammaMusicale;
           document.forms.ListaMovimentiViewId.action='${contextPath}/secure/pmDetail';
           document.forms.ListaMovimentiViewId.submit();
        }

    </script>

    <!-- BODY -->
    <form class="form-horizontal recordEditView" id="ListaMovimentiViewId" name="ListaMovimentiView" method="get" action="${contextPath}/secure/movimentiDetail">

        <!-- INIZIO SEZIONE VIEW DATI -->

        <c:if test="${resultPage!=null and empty resultPage.records and elabSelected != -1}">
            <div class="listViewEntriesDiv">
                    <br/><br/>
                    <b>Nessun dato ritornato</b>
                    <br/><br/><br/>
            </div>
        </c:if>

            <c:if test="${resultPage.records.size() > 0}">

            <div class="listViewEntriesDiv">
                <div>
                    <br/>
                    Numero movimenti contabili: <b>${resultPage.records.size()}</b> di <b>${resultPage.totRecords}</b>
                    <br/>
                    <center>

                        <c:if test="${resultPage.pages.size() > 1}">
                            <c:if test="${resultPage.currentPage > 1}">
                              <a href="javascript:callPage(${resultPage.currentPage-1});">< Prec</a>
                            </c:if>

                            <c:forEach items="${resultPage.pages}" var="p">
                                <c:choose>
                                    <c:when test="${resultPage.currentPage != p}">
                                        <a href="javascript:callPage(${p});">${p} </a>
                                    </c:when>
                                    <c:otherwise>
                                        <a style="color: #09b6eb;">${p} </a>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>

                            <c:if test="${resultPage.currentPage < resultPage.pages.size()}">
                               <a href="javascript:callPage(${resultPage.currentPage+1});"> > Suc </a>
                            </c:if>
                        </c:if>

                    </center>
                    <br/><br/>
                    <table class="table table-bordered listViewEntriesTable">
                        <thead>
                        <tr class="listViewHeaders">
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Contabilit&agrave;</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Voce<br/>Incasso</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Evento</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues"  href="javascript:void(0);">Fattura</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues"  href="javascript:void(0);">Reversale</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Tipo<br/>Documento</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Numero<br/>Programma Musicale</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Importo<br/>SUN</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Permesso</a>
                            </th>

                        </tr>
                        </thead>

                        <tbody>

                        <c:forEach items="${resultPage.records}" var="m">
                            <tr id="row_${m.id}" class="listViewEntries">
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string"><a href="javascript:doSubmit(${m.id});"><u>${m.contabilita}</u></a></td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.voceIncasso}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.evento.dataInizioEventoStr} ${m.evento.oraInizioEvento}-${m.evento.oraFineEvento}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.numeroFattura}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.reversale}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                                    <c:if test="${m.tipoDocumentoContabile =='501'}">
                                        Mod. 501 (Entrata)
                                    </c:if>
                                    <c:if test="${m.tipoDocumentoContabile =='221'}">
                                        Mod. 221 (Uscita)
                                    </c:if>
                                </td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                                    <a href="javascript:openPMDetail('${m.idProgrammaMusicale}')"><u>${m.numProgrammaMusicale}</u></a>
                                </td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.importoTotDemFormatted}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.numeroPermesso}</td>
                            </tr>
                        </c:forEach>

                        </tbody>
                    </table>
                    </c:if>

                  <input type="hidden" name="idProgrammaMusicale" id="idProgrammaMusicaleId" value=""/>
                  <input type="hidden" name="movimento" name="movimentoId" value=""/>
                <!-- FINE SEZIONE VIEW DATI -->

                    <c:if test="${resultPage!=null and not empty resultPage.records and resultPage.records.size() > 0}">
                        <!-- INIZIO TASTI FONDO PAGINA -->
                        <br/><br/>

                        <div class="row-fluid">
                            <div class="pull-right">
                                <!--
                                <button class="btn btn-success" type="submit">
                                    <strong>Salva</strong>
                                </button>
                                <a class="cancelLink" type="reset" onclick="javascript:window.history.back();">Annulla</a>
                                -->
                                <a href="${contextPath}/secure/exportReport?idReport=Lista_Movimenti">
                                    <img src="${contextPath}/images/excel.jpg" alt="Esporta in formato excel" width="35" height="38"/>
                                </a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <!-- FINE TASTI FONDO PAGINA -->
                    </c:if>

    </form>

    <!-- INIZIO FOOTER -->
    <jsp:include page="footer.jsp"/>
    <!-- FINE FOOTER -->

</body>
</html>