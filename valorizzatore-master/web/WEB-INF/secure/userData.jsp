<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>

 <jsp:include page="header.jsp"/>

<body data-language="it_it">

<div id="js_strings" class="hide noprint"></div>

<div id="page">

    <!-- container which holds data temporarly for pjax calls -->
    <div id="pjaxContainer" class="hide noprint"></div>

    <!-- INIZIO INTESTAZIONE-->
    <jsp:include page="menu_bar.jsp"/>
    <!-- FINE INTESTAZIONE-->

    <!-- INIZIO BARRA DI RICERCA -->
    <jsp:include page="search_bar.jsp"/>
    <!-- FINE BARRA DI RICERCA -->

    <!-- BODY -->
    <form class="form-horizontal recordEditView" id="EditView" name="EditView" method="post" action="" enctype="multipart/form-data">

        <div class="contentHeader row-fluid">
            <h3 class="span8 textOverflowEllipsis" title="Modifica Organizzatore CIRCOLO DELLA VELA">Valorizzatore Programmi Musicale</h3>
              <!-- INIZIO TASTI INIZIO PAGINA -->
              <!--
              <span class="pull-right">
               <button class="btn btn-success" type="submit">
                <strong>Salva</strong>
               </button>
               <a class="cancelLink" type="reset" onclick="javascript:window.history.back();">Annulla</a>
             </span>
             -->
             <!-- FINE TASTI INIZIO PAGINA -->
        </div>


        <!-- INIZIO SEZIONE DATI -->
        <table class="table table-bordered blockContainer showInlineTable equalSplit">
            <thead>
            <tr>
                <th class="blockHeader" colspan="4">Dati utente</th>
            </tr>
            </thead>
            <tbody style="display: table-row-group;">

            <tr>
                <td class="fieldLabel medium">
                    <label class="muted pull-right marginRight10px">
                        <label class="muted pull-right marginRight10px">Nome</label>
                    </label>
                </td>
                <td class="fieldValue medium">
                    <div class="row-fluid">
                        <span class="span10">
                            <div class="input-append row-fluid">
                                <div class="span12 row-fluid date">
                                      ${user.firstName}
                                </div>
                            </div>
                        </span>
                    </div>
                </td>
                <td class="fieldLabel medium">
                    <label class="muted pull-right marginRight10px">Cognome</label>
                </td>
                <td class="fieldValue medium">
                    <div class="row-fluid">
                        <span class="span10">
                            <div class="input-append row-fluid">
                                <div class="span12 row-fluid date">
                                    ${user.lastName}
                                </div>
                            </div>
                        </span>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="fieldLabel medium">
                    <label class="muted pull-right marginRight10px">
                        <label class="muted pull-right marginRight10px">Username</label>
                    </label>
                </td>
                <td class="fieldValue medium">
                    <div class="row-fluid">
                        <span class="span10">
                            <div class="input-append row-fluid">
                                <div class="span12 row-fluid date">
                                    ${user.username}
                                </div>
                            </div>
                        </span>
                    </div>
                </td>
                <td class="fieldLabel medium">
                    <label class="muted pull-right marginRight10px">Data creazione</label>
                </td>
                <td class="fieldValue medium">
                    <div class="row-fluid">
                        <span class="span10">
                            <div class="input-append row-fluid">
                                <div class="span12 row-fluid date">
                                    ${user.dataCreazione}
                                </div>
                            </div>
                        </span>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="fieldLabel medium">
                    <label class="muted pull-right marginRight10px">
                        <label class="muted pull-right marginRight10px">Ruolo</label>
                    </label>
                </td>
                <td class="fieldValue medium">
                    <div class="row-fluid">
                        <span class="span10">
                            <div class="input-append row-fluid">
                                <div class="span12 row-fluid date">
                                    <c:forEach items="${user.roles}" var="r">
                                        ${r.description}&nbsp;
                                    </c:forEach>
                                </div>
                            </div>
                        </span>
                    </div>
                </td>
                <td class="fieldLabel medium">
                    <label class="muted pull-right marginRight10px">Profili</label>
                </td>
                <td class="fieldValue medium">
                    <div class="row-fluid">
                        <span class="span10">
                            <div class="input-append row-fluid">
                                <div class="span12 row-fluid date">
                                  <c:forEach items="${user.profiles}" var="r">
                                      ${r.description}&nbsp;
                                  </c:forEach>
                                </div>
                            </div>
                        </span>
                    </div>
                </td>
            </tr>

            </tbody>
        </table>



        <!-- FINE SEZIONE DATI -->



        <!-- INIZIO TASTI FONDO PAGINA -->
        <br/><br/>
        <!--
        <div class="row-fluid">
            <div class="pull-right">
                <button class="btn btn-success" type="submit">
                    <strong>Salva</strong>
                </button>
                <a class="cancelLink" type="reset" onclick="javascript:window.history.back();">Annulla</a>
            </div>
            <div class="clearfix"></div>
        </div>
        -->
        <!-- FINE TASTI FONDO PAGINA -->

    </form>

    <script type="text/javascript">
$(document).ready(function() {
  $(".js-basic-single-select").select2();
});
</script>

    <!-- INIZIO FOOTER -->
    <jsp:include page="footer.jsp"/>
    <!-- FINE FOOTER -->

</body>
</html>