<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>

 <jsp:include page="header.jsp"/>

 <body data-language="it_it">

 <div id="page">

     <!-- container which holds data temporarly for pjax calls -->
     <div id="pjaxContainer" class="hide noprint"></div>

     <!-- INIZIO INTESTAZIONE-->
     <jsp:include page="menu_bar.jsp"/>
     <!-- FINE INTESTAZIONE-->

     <!-- INIZIO BARRA DI RICERCA -->
     <jsp:include page="search_bar_cruscottiTracciamentoPM.jsp"/>
     <!-- FINE BARRA DI RICERCA -->

     <script type="text/javascript">

       function openPMDetail(idProgrammaMusicale){
           document.forms.ViewFormId.elements.idProgrammaMusicale.value=idProgrammaMusicale;
           document.forms.ViewFormId.action='${contextPath}/secure/pmDetail';
           document.forms.ViewFormId.submit();
        }

    </script>

     <!-- BODY -->
     <form class="form-horizontal recordEditView" id="ViewFormId" name="ViewForm" method="get"  action="">

         <!-- INIZIO SEZIONE VIEW DATI -->

         <c:if test="${resultPage!=null and empty resultPage.records}">

             <div class="contents-topscroll noprint">
                 <div class="topscroll-div" style="width: 1308px; height: 1px;">
                 </div>
             </div>

             <div class="listViewEntriesDiv contents-bottomscroll">
                 <div class="bottomscroll-div" style="width: 1308px;">
                     <br/><br/>
                     <b>Nessun dato ritornato</b>
                     <br/><br/><br/>
                 </div>
             </div>
         </c:if>

         <c:if test="${resultPage.records.size() > 0}">

         <div class="listViewEntriesDiv contents-bottomscroll">
             <div class="bottomscroll-div" style="width: 1308px;">
                 <br/><br/>
                 Numero Programmi Musicali: <b>${resultPage.records.size()}</b> di <b>${resultPage.totRecords}</b>
                 <br/>
                 <center>

                     <c:if test="${resultPage.pages.size() > 1}">
                         <c:if test="${resultPage.currentPage > 1}">
                             <a href="javascript:callPage(${resultPage.currentPage-1});">< Prec</a>
                         </c:if>

                         <c:forEach items="${resultPage.pages}" var="p">
                             <c:choose>
                                 <c:when test="${resultPage.currentPage != p}">
                                     <a href="javascript:callPage(${p});">${p} </a>
                                 </c:when>
                                 <c:otherwise>
                                     <a style="color: #09b6eb;">${p} </a>
                                 </c:otherwise>
                             </c:choose>
                         </c:forEach>

                         <c:if test="${resultPage.currentPage < resultPage.pages.size()}">
                             <a href="javascript:callPage(${resultPage.currentPage+1});"> > Suc </a>
                         </c:if>
                     </c:if>

                 </center>
                 <br/><br/>
                 <table class="table table-bordered listViewEntriesTable">
                     <thead>
                     <tr class="listViewHeaders">
                         <th nowrap="" valign="center" align="center">
                             <a class="listViewHeaderValues" href="javascript:void(0);">Numero<br/>Programma</a>
                         </th>
                         <th nowrap="" valign="center" align="center">
                             <a class="listViewHeaderValues" href="javascript:void(0);">Data</a>
                         </th>
                         <th nowrap="" valign="center" align="center">
                             <a class="listViewHeaderValues" href="javascript:void(0);">Stato</a>
                         </th>
                     </tr>
                     </thead>

                     <tbody>

                     <c:forEach items="${resultPage.records}" var="p">
                         <tr id="row_${m.id}" class="listViewEntries">

                             <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                                    <a href="javascript:openPMDetail('${p.id}')"><u>${p.numeroProgrammaMusicale}</u></a>
                             </td>
                             <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${p.dataPassaggioStato}</td>
                             <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${p.stato}</td>
                         </tr>
                     </c:forEach>

                     </tbody>
                 </table>
                 </c:if>

                 <!-- FINE SEZIONE VIEW DATI -->

                 <div class="contentHeader row-fluid">
                     <!--h3 class="span8 textOverflowEllipsis">Valorizzatore Programmi Musicale</h3-->
                     <!-- INIZIO TASTI INIZIO PAGINA
                     <span class="pull-right">
                      <button class="btn btn-success" type="submit">
                       <strong>Cerca</strong>
                      </button>
                    </span>
                    -->
                     <!-- FINE TASTI INIZIO PAGINA -->
                 </div>

                 <!-- INIZIO TASTI FONDO PAGINA -->
                 <br/>
                 <br/>
                 <center><span style="color: red">${message}</span></center>

                 <input type="hidden" name="idProgrammaMusicale" id="idProgrammaMusicaleId" value=""/>



                 <c:if test="${resultPage!=null and not empty resultPage.records and resultPage.records.size() > 0}">
                 <!-- INIZIO TASTI FONDO PAGINA -->
                 <br/><br/>
                 <div class="row-fluid">
                     <div class="pull-right">
                         <a href="${contextPath}/secure/exportReport?idReport=Tracciamento_PM">
                             <img src="${contextPath}/images/excel.jpg" alt="Esporta in formato excel" width="35" height="38"/>
                         </a>
                     </div>
                     <div class="clearfix"></div>
                 </div>
                 <!-- FINE TASTI FONDO PAGINA -->
                 </c:if>


     </form>

     <!-- INIZIO FOOTER -->
     <jsp:include page="footer.jsp"/>
     <!-- FINE FOOTER -->

 </div>
 </body>
</html>