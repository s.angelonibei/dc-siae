<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>

 <jsp:include page="header.jsp"/>

<body data-language="it_it">

<div id="page">

    <!-- container which holds data temporarly for pjax calls -->
    <div id="pjaxContainer" class="hide noprint"></div>

    <!-- INIZIO INTESTAZIONE-->
    <jsp:include page="menu_bar.jsp"/>
    <!-- FINE INTESTAZIONE-->

    <!-- INIZIO BARRA DI RICERCA -->
    <jsp:include page="search_bar_ricalcoloModificaContabilita.jsp"/>
    <!-- FINE BARRA DI RICERCA -->

    <script type="text/javascript">

       function openPMDetail(idProgrammaMusicale){
           document.forms.ListaMovimentiViewId.elements.idProgrammaMusicale.value=idProgrammaMusicale;
           document.forms.ListaMovimentiViewId.action='${contextPath}/secure/pmDetail';
           document.forms.ListaMovimentiViewId.submit();
        }

    </script>

    <!-- BODY
    <form class="form-horizontal recordEditView" id="ricalcoloModificaContabilitaId" name="ricalcoloModificaContabilita" method="get" action="${contextPath}/secure/ricalcoloModificaContabilitaUpdate">
     -->
        <!-- INIZIO SEZIONE VIEW DATI -->

        <c:if test="${resultPage!=null and empty resultPage.records and elabSelected != -1}">
            <div class="listViewEntriesDiv">
                    <br/><br/>
                    <b>Nessun dato ritornato</b>
                    <br/><br/><br/>
            </div>
        </c:if>

            <c:if test="${resultPage.records.size() > 0}">

            <div class="listViewEntriesDiv">
                <div>
                    <br/>
                    Numero movimenti contabili: <b>${resultPage.records.size()}</b> di <b>${resultPage.totRecords}</b>
                    <br/>
                    <center>

                        <c:if test="${resultPage.pages.size() > 1}">
                            <c:if test="${resultPage.currentPage > 1}">
                              <a href="javascript:callPage(${resultPage.currentPage-1});">< Prec</a>
                            </c:if>

                            <c:forEach items="${resultPage.pages}" var="p">
                                <c:choose>
                                    <c:when test="${resultPage.currentPage != p}">
                                        <a href="javascript:callPage(${p});">${p} </a>
                                    </c:when>
                                    <c:otherwise>
                                        <a style="color: #09b6eb;">${p} </a>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>

                            <c:if test="${resultPage.currentPage < resultPage.pages.size()}">
                               <a href="javascript:callPage(${resultPage.currentPage+1});"> > Suc </a>
                            </c:if>
                        </c:if>

                    </center>

                    <br/>
                    <c:if test="${resultPage.records.size() > 1}">
                        <div>
                             <span class="pull-right">
                               <button class="btn btn-success" onclick="javascript:callUpdate()">
                                <strong>Modifica</strong>
                               </button>
                             </span>
                        </div>
                    </c:if>

                    <input type="text" style="width: 250px;" class="" value="${nuovaContabilita}" name="nuovaContabilita" id="nuovaContabilitaId" placeholder="Nuova contabilita' (AAAAMM)" results="10">&nbsp;&nbsp;

                    <br/>
                    <center><span style="color: red">${message}</span></center>
                    <br/>
                    <table class="table table-bordered listViewEntriesTable">
                        <thead>
                        <tr class="listViewHeaders">
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Contabilit&agrave;</a>  </br>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Contabilit&agrave;<br/>Originale</a>  </br>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Voce<br/>Incasso</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Evento</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues"  href="javascript:void(0);">Fattura</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues"  href="javascript:void(0);">Reversale</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Tipo<br/>Documento</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Numero<br/>Programma Musicale</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Importo<br/>SUN</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Permesso</a>
                            </th>

                        </tr>
                        </thead>

                        <tbody>

                        <c:forEach items="${resultPage.records}" var="m">
                            <tr id="row_${m.id}" class="listViewEntries">
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.contabilita}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.contabilitaOrg}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.voceIncasso}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.evento.dataInizioEventoStr} ${m.evento.oraInizioEvento}-${m.evento.oraFineEvento}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.numeroFattura}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.reversale}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                                    <c:if test="${m.tipoDocumentoContabile =='501'}">
                                        Mod. 501 (Entrata)
                                    </c:if>
                                    <c:if test="${m.tipoDocumentoContabile =='221'}">
                                        Mod. 221 (Uscita)
                                    </c:if>
                                </td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                                    ${m.idProgrammaMusicale}
                                </td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.importoTotDem}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.numeroPermesso}</td>
                            </tr>
                        </c:forEach>

                        </tbody>
                    </table>
                    </c:if>

                  <input type="hidden" name="idProgrammaMusicale" id="idProgrammaMusicaleId" value=""/>
                  <input type="hidden" name="movimento" name="movimentoId" value=""/>
                <!-- FINE SEZIONE VIEW DATI -->


        <!-- INIZIO TASTI FONDO PAGINA -->
        <br/>
                    <br/>
                    <c:if test="${resultPage.records.size() > 1}">
                        <div>
                              <span class="pull-right">
                               <button class="btn btn-success" onclick="javascript:callUpdate()">
                                <strong>Modifica</strong>
                               </button>
                             </span>
                        </div>
                    </c:if>

                    <br/>

                    <!-- </form>   -->

                   <!-- INIZIO FOOTER -->
    <jsp:include page="footer.jsp"/>
    <!-- FINE FOOTER -->

</body>
</html>