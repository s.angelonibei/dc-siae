<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>

 <jsp:include page="header.jsp"/>

<body data-language="it_it">

<div id="js_strings" class="hide noprint"></div>
<input type="hidden" id="start_day" value="Sunday">
<input type="hidden" id="row_type" value="medium">
<input type="hidden" id="current_user_id" value="1">

<div id="page">

    <!-- container which holds data temporarly for pjax calls -->
    <div id="pjaxContainer" class="hide noprint"></div>

    <!-- INIZIO INTESTAZIONE-->
    <jsp:include page="menu_bar.jsp"/>
    <!-- FINE INTESTAZIONE-->

    <!-- INIZIO BARRA DI RICERCA -->
    <jsp:include page="search_bar.jsp"/>
    <!-- FINE BARRA DI RICERCA -->

    <!-- BODY -->
    <script>

    // Gestione Progress Bar Valorizzatore - Start
    $(function() {

        var progressbar = $( "#progressbar" ),
          progressLabel = $( ".progress-label" ),
          executeButton = $( "#executeButton" )
            .button()
            .on( "click", function() {

              $( this ).button( "option", {
                disabled: true,
                label: "Esecuzione in corso..."
              });

              var jqxhr = $.get( "${contextPath}/secure/sendCommand?info=START_RICALCOLO", function( value ) {
                   progressLabel.text( "Loading..." );

                   $("#elaborazioneValue #totValue").text("0");
                   $("#elaborazioneValue #elabValue").text("0");
                   $("#elaborazioneValue #sospValue").text("0");
                   $("#elaborazioneValue #errValue").text("0");

                   setTimeout( progress, 2000 );

                   $("#suspendDiv").show();
                   $("#resumeDiv").hide();

              });
            });

        progressbar.progressbar({
          value: false,
          change: function() {
            progressLabel.text( progressbar.progressbar( "value" ) + "%" );
          },
          complete: function() {
            progressLabel.text( "Complete!" );

            $( "#executeButton" ).button( "option", {
                disabled: false,
                label: "Inizia esecuzione"
            });

          }
        });

        function progress() {

                var jqxhr = $.get( "${contextPath}/secure/getAvanzamentoElaborazione", function( value ) {

                      var val = progressbar.progressbar( "value" ) || 0;

                      $("#progressbar").progressbar( "value", value.percentuale);

                      $("#elaborazioneValue #totValue").text(value.totalePM);
                      $("#elaborazioneValue #elabValue").text(value.totaleElaborati);
                      $("#elaborazioneValue #sospValue").text(value.totaleSospesi);
                      $("#elaborazioneValue #errValue").text(value.totaleErrori);

                      if ( val < 100) {
                        setTimeout( progress, 2000 );     // Refresh ogni 2 sec
                      }else{
                            $( "#executeButton" ).button( "option", {
                                disabled: false,
                                label: "Inizia esecuzione"
                            });
                      }
                });
        }

  });
    // Gestione Progress Bar Valorizzatore - End

    // Gestione Progress Bar Valorizzatore SMC - Start
    $(function() {

        var progressbar = $( "#progressbar2243" ),
                progressLabel = $( ".progress-label2243" ),
                executeButton = $( "#executeButton2243" )
                        .button()
                        .on( "click", function() {

                            $( this ).button( "option", {
                                disabled: true,
                                label: "Esecuzione in corso..."
                            });

                            var jqxhr = $.get( "${contextPath}/secure/sendCommand?info=RICALCOLO_START_2243", function( value ) {
                                progressLabel.text( "Loading..." );

                                $("#elaborazioneValue2243 #totValue2243").text("0");
                                $("#elaborazioneValue2243 #elabValue2243").text("0");
                                $("#elaborazioneValue2243 #sospValue2243").text("0");
                                $("#elaborazioneValue2243 #errValue2243").text("0");

                                setTimeout( progressSMC, 2000 );

                                $("#suspendDiv2243").show();
                                $("#resumeDiv2243").hide();

                            });
                        });

        progressbar.progressbar({
            value: false,
            change: function() {
                progressLabel.text( progressbar.progressbar( "value" ) + "%" );
            },
            complete: function() {
                progressLabel.text( "Complete!" );

                $( "#executeButton2243" ).button( "option", {
                    disabled: false,
                    label: "Inizia esecuzione"
                });

            }
        });

        function progressSMC() {

            var jqxhr = $.get( "${contextPath}/secure/getAvanzamentoElaborazione2243", function( value ) {

                var val = progressbar.progressbar( "value" ) || 0;

                $("#progressbar2243").progressbar( "value", value.percentuale);

                $("#elaborazioneValue2243 #totValue2243").text(value.totalePM);
                $("#elaborazioneValue2243 #elabValue2243").text(value.totaleElaborati);
                $("#elaborazioneValue2243 #sospValue2243").text(value.totaleSospesi);
                $("#elaborazioneValue2243 #errValue2243").text(value.totaleErrori);

                if ( val < 100) {
                    setTimeout( progressSMC, 2000 );     // Refresh ogni 2 sec
                }else{
                    $( "#executeButton2243" ).button( "option", {
                        disabled: false,
                        label: "Inizia esecuzione"
                    });
                }
            });
        }

    });
    // Gestione Progress Bar Valorizzatore SMC - End

    // Gestione Azioni Valorizzatore - Start
    function callActivateDeactivate(status){

        var request = $.ajax({
             url: "${contextPath}/secure/sendCommand?info="+status,
             type: "GET"
        });

        request.done(function(value) {

          value=new String(value);

          if (value == "ACTIVE"){

            $("#activeArrow").show();
            $("#inactiveArrow").hide();

            $("#activateDiv").hide();
            $("#inactiveDiv").show();

            $("#statoProcessoDiv #statoProcesso").text("Attivo");

            $("#executeButton").button("option", {
             disabled: false
            });

            $("#elaborazioneLabel").show();
            $("#elaborazioneValue").show();

             var jqxhr = $.get( "${contextPath}/secure/getAvanzamentoElaborazione", function( value ) {

               $("#elaborazioneValue #totValue").text(value.totalePM);
               $("#elaborazioneValue #elabValue").text(value.totaleElaborati);
               $("#elaborazioneValue #sospValue").text(value.totaleSospesi);
               $("#elaborazioneValue #errValue").text(value.totaleErrori);

            });


          } else
          if (value == "INACTIVE"){
            $("#activeArrow").hide();
            $("#inactiveArrow").show();

            $("#activateDiv").show();
            $("#inactiveDiv").hide();

            $("#statoProcessoDiv #statoProcesso").text("Inattivo");

            $("#executeButton").button("option", {
             disabled: true
            });

            $("#elaborazioneLabel").hide();
            $("#elaborazioneValue").hide();

          }else{
              alert("Error: "+value);
          }

        });

        request.fail(function(jqXHR, textStatus) {
           alert( "Request failed: " + textStatus );
        });
   }

    function callSuspendeResume(status){

        //suspendDiv      resumeDiv
        //suspendButton   resumeButton
        //SUSPENDE        RESUME

        var request = $.ajax({
             url: "${contextPath}/secure/sendCommand?info="+status,
             type: "GET"
        });

        request.done(function(value) {

          if (value == "SUSPENDED"){

            $("#suspendDiv").hide();
            $("#resumeDiv").show();

          } else
          if (value == "RESUMED"){

            $("#suspendDiv").show();
             $("#resumeDiv").hide();

          }else{
              alert(value);
          }

        });

        request.fail(function(jqXHR, textStatus) {
           alert( "Request failed: " + textStatus );
        });
   }
    // Gestione Azioni Valorizzatore - End

    // Gestione Azioni Valorizzatore - Start
    function callActivateDeactivate2243(status){

        var request = $.ajax({
            url: "${contextPath}/secure/sendCommand?info="+status,
            type: "GET"
        });

        request.done(function(value) {

            value=new String(value);

            if (value == "ACTIVE"){

                $("#activeArrow2243").show();
                $("#inactiveArrow2243").hide();

                $("#activateDiv2243").hide();
                $("#inactiveDiv2243").show();

                $("#statoProcessoDiv2243 #statoProcesso2243").text("Attivo");

                $("#executeButton2243").button("option", {
                    disabled: false
                });

                $("#elaborazioneLabel2243").show();
                $("#elaborazioneValue2243").show();

                var jqxhr = $.get( "${contextPath}/secure/getAvanzamentoElaborazione2243", function( value ) {

                    $("#elaborazioneValue2243 #totValue2243").text(value.totalePM);
                    $("#elaborazioneValue2243 #elabValue2243").text(value.totaleElaborati);
                    $("#elaborazioneValue2243 #sospValue2243").text(value.totaleSospesi);
                    $("#elaborazioneValue2243 #errValue2243").text(value.totaleErrori);

                });


            } else
            if (value == "INACTIVE"){
                $("#activeArrow2243").hide();
                $("#inactiveArrow2243").show();

                $("#activateDiv2243").show();
                $("#inactiveDiv2243").hide();

                $("#statoProcessoDiv2243 #statoProcesso2243").text("Inattivo");

                $("#executeButton2243").button("option", {
                    disabled: true
                });

                $("#elaborazioneLabel2243").hide();
                $("#elaborazioneValue2243").hide();

            }else{
                alert("Error: "+value);
            }

        });

        request.fail(function(jqXHR, textStatus) {
            alert( "Request failed: " + textStatus );
        });
    }
    // Gestione Azioni Valorizzatore - End

  </script>

    <form class="form-horizontal recordEditView" id="RicalcoloFormId" name="RicalcoloForm" method="post" action="${contextPath}/secure/ricalcoloEsecuzioneSave">

    </form>

        <!-- VALORIZZATORE  -->
        <c:if test="${user.hasProfile('P_RICALCOLO_ALL')}">
            <table class="table table-bordered blockContainer showInlineTable equalSplit">
                <thead>
                <tr>
                    <th class="blockHeader" colspan="4">Controllo esecuzione</th>
                </tr>
                </thead>
                <tbody style="display: table-row-group;">

                <tr>
                    <td class="fieldLabel medium" style="vertical-align: middle">
                        <label class="muted pull-right marginRight10px">
                            <!-- Contenuto -->
                            <div id="activateDiv" style="display:none" align="center">
                                <button id="startButton" onclick="callActivateDeactivate('ACTIVATE');">Start</button>
                            </div>
                            <div id="inactiveDiv" style="display:none" align="center">
                                <button id="stopButton" onclick="callActivateDeactivate('DEACTIVATE');">Stop</button>
                            </div>
                        </label>
                    </td>
                    <td class="fieldValue medium" style="vertical-align: middle">
                        <div class="row-fluid">
                            <span class="span10">
                                <div class="input-append row-fluid">
                                    <div class="span12 row-fluid date">
                                        <!-- Contenuto -->
                                        <div id="activeArrow" style="display:none" align="center">
                                           <img src="${contextPath}/images/green-arrow.png" height="30" width="30" alt="Processo attivo" title="Stato processo"/>
                                        </div>
                                        <div id="inactiveArrow" style="display:none" align="center">
                                            <img src="${contextPath}/images/red-arrow.png" height="30" width="30" alt="Processo attivo" title="Stato processo"/>
                                        </div>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </td>

                    <td class="fieldLabel medium" style="vertical-align: middle">
                       <label class="muted pull-right marginRight10px">Stato processo</label>
                    </td>
                    <td class="fieldValue medium" style="vertical-align: middle">
                        <div class="row-fluid">
                            <span class="span10">
                                <div class="input-append row-fluid">
                                    <div class="span12 row-fluid date">
                                        <!-- Contenuto -->
                                        <div id="statoProcessoDiv" class="row-fluid">
                                           <b><span id="statoProcesso" class="span10 pull-left"></span></b>
                                        </div>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="fieldLabel medium" style="vertical-align: middle">
                        <label class="muted pull-right marginRight10px">
                            <!-- Contenuto -->
                            <button id="executeButton">Inizia esecuzione</button>
                        </label>
                    </td>
                    <td class="fieldValue medium" style="vertical-align: middle">
                        <div class="row-fluid">
                            <span class="span10">
                                <div class="input-append row-fluid">
                                    <div class="span12 row-fluid date">
                                        <!-- Contenuto -->
                                            <div id="progressbar"><div class="progress-label"></div></div>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </td>
                    <td class="fieldLabel medium" style="vertical-align: middle">
                      <div id="elaborazioneLabel" style="display:none">
                          <table>
                              <tr><td><label class="muted pull-right marginRight10px">Numero PM totali</label></td></tr>
                              <tr><td><label class="muted pull-right marginRight10px">Numero PM elaborati</label></td></tr>
                              <tr><td><label class="muted pull-right marginRight10px">Numero PM sospesi</label></td></tr>
                              <tr><td><label class="muted pull-right marginRight10px">Numero PM in errore</label></td></tr>
                           </table>
                      </div>
                    </td>
                    <td class="fieldValue medium" style="vertical-align: middle">
                        <div id="elaborazioneValue" style="display:none" class="row-fluid">
                            <table width="100%">
                                <tr><td><span id="totValue" class="span10 pull-left"></span></td></tr>
                                <tr><td><span id="elabValue" class="span10 pull-left"></span></td></tr>
                                <tr><td><span id="sospValue" class="span10 pull-left"></span></td></tr>
                                <tr><td><span id="errValue" class="span10 pull-left"></span></td></tr>
                            </table>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
            <br/><br/><br/>
            <!-- VALORIZZATORE  -->
        </c:if>

        <!-- VALORIZZATORE SMC -->
        <c:if test="${user.hasProfile('P_RICALCOLO_SMC')}">
            <table class="table table-bordered blockContainer showInlineTable equalSplit">
        <thead>
        <tr>
            <th class="blockHeader" colspan="4">Controllo esecuzione SMC</th>
        </tr>
        </thead>
        <tbody style="display: table-row-group;">

        <tr>
            <td class="fieldLabel medium" style="vertical-align: middle">
                <label class="muted pull-right marginRight10px">
                    <!-- Contenuto -->
                    <div id="activateDiv2243" style="display:none" align="center">
                        <button id="startButton2243" onclick="callActivateDeactivate2243('ACTIVATE_2243');">Start</button>
                    </div>
                    <div id="inactiveDiv2243" style="display:none" align="center">
                        <button id="stopButton2243" onclick="callActivateDeactivate2243('DEACTIVATE_2243');">Stop</button>
                    </div>
                </label>
            </td>
            <td class="fieldValue medium" style="vertical-align: middle">
                <div class="row-fluid">
                        <span class="span10">
                            <div class="input-append row-fluid">
                                <div class="span12 row-fluid date">
                                    <!-- Contenuto -->
                                    <div id="activeArrow2243" style="display:none" align="center">
                                       <img src="${contextPath}/images/green-arrow.png" height="30" width="30" alt="Processo attivo" title="Stato processo"/>
                                    </div>
                                    <div id="inactiveArrow2243" style="display:none" align="center">
                                        <img src="${contextPath}/images/red-arrow.png" height="30" width="30" alt="Processo attivo" title="Stato processo"/>
                                    </div>
                                </div>
                            </div>
                        </span>
                </div>
            </td>

            <td class="fieldLabel medium" style="vertical-align: middle">
                <label class="muted pull-right marginRight10px">Stato processo</label>
            </td>
            <td class="fieldValue medium" style="vertical-align: middle">
                <div class="row-fluid">
                        <span class="span10">
                            <div class="input-append row-fluid">
                                <div class="span12 row-fluid date">
                                    <!-- Contenuto -->
                                    <div id="statoProcessoDiv2243" class="row-fluid">
                                       <b><span id="statoProcesso2243" class="span10 pull-left"></span></b>
                                    </div>
                                </div>
                            </div>
                        </span>
                </div>
            </td>
        </tr>

        <tr>
            <td class="fieldLabel medium" style="vertical-align: middle">
                <label class="muted pull-right marginRight10px">
                    <!-- Contenuto -->
                    <button id="executeButton2243">Inizia esecuzione</button>
                </label>
            </td>
            <td class="fieldValue medium" style="vertical-align: middle">
                <div class="row-fluid">
                        <span class="span10">
                            <div class="input-append row-fluid">
                                <div class="span12 row-fluid date">
                                    <!-- Contenuto -->
                                        <div id="progressbar2243"><div class="progress-label2243"></div></div>
                                </div>
                            </div>
                        </span>
                </div>
            </td>
            <td class="fieldLabel medium" style="vertical-align: middle">
                <div id="elaborazioneLabel2243" style="display:none">
                    <table>
                         <tr><td><label class="muted pull-right marginRight10px">Numero PM totali</label></td></tr>
                         <tr><td><label class="muted pull-right marginRight10px">Numero PM elaborati</label></td></tr>
                         <tr><td><label class="muted pull-right marginRight10px">Numero PM sospesi</label></td></tr>
                         <tr><td><label class="muted pull-right marginRight10px">Numero PM in errore</label></td></tr>
                     </table>
                </div>
            </td>
            <td class="fieldLabel medium" style="vertical-align: middle">
                <div id="elaborazioneValue2243" style="display:none" class="row-fluid">
                    <table width="100%">
                        <tr><td><span id="totValue2243" class="span10 pull-left"></span></td></tr>
                        <tr><td><span id="elabValue2243" class="span10 pull-left"></span></td></tr>
                        <tr><td><span id="sospValue2243" class="span10 pull-left"></span></td></tr>
                        <tr><td><span id="errValue2243" class="span10 pull-left"></span></td></tr>
                    </table>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
        </c:if>
        <!-- VALORIZZATORE SMC -->

    <script>


    $(document).ready(function(){

        // Gestione stato Valorizzatore - Start
        var request = $.ajax({
             url: "${contextPath}/secure/sendCommand?info=STATUS",
             type: "GET"
        });

        request.done(function(value) {

          if (value == "ACTIVE"){

            $("#activeArrow").show();
            $("#inactiveArrow").hide();

            $("#activateDiv").hide();
            $("#inactiveDiv").show();

            $("#statoProcessoDiv #statoProcesso").text("Attivo");

            $("#executeButton").attr("disabled", false);

            $("#executeButton").button("option", {
             disabled: false
            });

            $("#elaborazioneLabel").show();
            $("#elaborazioneValue").show();

            var jqxhr = $.get( "${contextPath}/secure/getAvanzamentoElaborazione", function( value ) {

               $("#elaborazioneValue #totValue").text(value.totalePM);
               $("#elaborazioneValue #elabValue").text(value.totaleElaborati);
               $("#elaborazioneValue #sospValue").text(value.totaleSospesi);
               $("#elaborazioneValue #errValue").text(value.totaleErrori);

                //if (value.totalePM>0 && value.totaleElaborati>0){
                //    $('#executeButton').click();
                //}
            });

          } else
          if (value == "INACTIVE"){

            $("#activeArrow").hide();
            $("#inactiveArrow").show();

            $("#activateDiv").show();
            $("#inactiveDiv").hide();

            $("#statoProcessoDiv #statoProcesso").text("Inattivo");

            $("#executeButton").button("option", {
             disabled: true
            });

            $("#elaborazioneLabel").hide();
            $("#elaborazioneValue").hide();

          }else{
              alert(value);
          }
        });
        request.fail(function(jqXHR, textStatus) {
           alert( "Request Valorizzatore failed: " + textStatus );
        });
        // Gestione stato Valorizzatore - End

        // Gestione stato Valorizzatore SMC - Start
        var requestSMC = $.ajax({
            url: "${contextPath}/secure/sendCommand?info=STATUS_2243",
            type: "GET"
        });

        requestSMC.done(function(value) {

            if (value == "ACTIVE"){

                $("#activeArrow2243").show();
                $("#inactiveArrow2243").hide();

                $("#activateDiv2243").hide();
                $("#inactiveDiv2243").show();

                $("#statoProcessoDiv2243 #statoProcesso2243").text("Attivo");

                $("#executeButton2243").attr("disabled", false);

                $("#executeButton2243").button("option", {
                    disabled: false
                });

                $("#elaborazioneLabel2243").show();
                $("#elaborazioneValue2243").show();

                var jqxhr = $.get( "${contextPath}/secure/getAvanzamentoElaborazione2243", function( value ) {

                    $("#elaborazioneValue2243 #totValue2243").text(value.totalePM);
                    $("#elaborazioneValue2243 #elabValue2243").text(value.totaleElaborati);
                    $("#elaborazioneValue2243 #sospValue2243").text(value.totaleSospesi);
                    $("#elaborazioneValue2243 #errValue2243").text(value.totaleErrori);

                    //if (value.totalePM>0 && value.totaleElaborati>0){
                    //    $('#executeButton').click();
                    //}
                });

            } else
            if (value == "INACTIVE"){

                $("#activeArrow2243").hide();
                $("#inactiveArrow2243").show();

                $("#activateDiv2243").show();
                $("#inactiveDiv2243").hide();

                $("#statoProcessoDiv2243 #statoProcesso2243").text("Inattivo");

                $("#executeButton2243").button("option", {
                    disabled: true
                });

                $("#elaborazioneLabel2243").hide();
                $("#elaborazioneValue2243").hide();

            }else{
                alert(value);
            }
        });
        requestSMC.fail(function(jqXHR, textStatus) {
            alert( "Request Valorizzatore SMC failed: " + textStatus );
        });
        // Gestione stato Valorizzatore SMC - End

    });


    </script>

        <!-- INIZIO TASTI FONDO PAGINA -->
        <br/><br/>

    <!-- INIZIO FOOTER -->
    <jsp:include page="footer.jsp"/>
    <!-- FINE FOOTER -->

</body>
</html>