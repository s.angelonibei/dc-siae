<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>

 <jsp:include page="header.jsp"/>

 <body data-language="it_it">

 <div id="page">

     <!-- container which holds data temporarly for pjax calls -->
     <div id="pjaxContainer" class="hide noprint"></div>

     <!-- INIZIO INTESTAZIONE-->
     <jsp:include page="menu_bar.jsp"/>
     <!-- FINE INTESTAZIONE-->

     <!-- INIZIO BARRA DI RICERCA -->
     <jsp:include page="search_bar_flussoGuida.jsp"/>
     <!-- FINE BARRA DI RICERCA -->


     <!-- BODY -->
     <form class="form-horizontal recordEditView" id="FlussoGuidaFormId" name="FlussoGuidaForm" method="post"
           action="${contextPath}/secure/downloadCSVFlussoGuida">

         <!-- INIZIO SEZIONE VIEW DATI -->

         <c:if test="${resultPage!=null and empty resultPage.records}">

             <div class="contents-topscroll noprint">
                 <div class="topscroll-div" style="width: 1308px; height: 70px;">
                 </div>
             </div>

             <div class="listViewEntriesDiv">
                 <div class="bottomscroll-div" style="width: 1308px;">
                     <br/><br/>
                     <b>Nessun dato ritornato</b>
                     <br/><br/><br/>
                 </div>
             </div>
         </c:if>

         <c:if test="${resultPage.records.size() > 0}">
         <div class="contents-topscroll noprint">
             <div class="topscroll-div" style="width: 1308px; height: 70px;"></div>
         </div>

         <div class="listViewEntriesDiv">

                 <br/><br/>
                 Numero Programmi Musicali: <b>${resultPage.records.size()}</b> di <b>${resultPage.totRecords}</b>
                 <br/>
                 <center>

                     <c:if test="${resultPage.pages.size() > 1}">
                         <c:if test="${resultPage.currentPage > 1}">
                             <a href="javascript:callPage(${resultPage.currentPage-1});">< Prec</a>
                         </c:if>

                         <c:forEach items="${resultPage.pages}" var="p">
                             <c:choose>
                                 <c:when test="${resultPage.currentPage != p}">
                                     <a href="javascript:callPage(${p});">${p} </a>
                                 </c:when>
                                 <c:otherwise>
                                     <a style="color: #09b6eb;">${p} </a>
                                 </c:otherwise>
                             </c:choose>
                         </c:forEach>

                         <c:if test="${resultPage.currentPage < resultPage.pages.size()}">
                             <a href="javascript:callPage(${resultPage.currentPage+1});"> > Suc </a>
                         </c:if>
                     </c:if>

                 </center>
                 <br/><br/>
                 <table class="table table-bordered listViewEntriesTable">
                     <thead>
                     <tr class="listViewHeaders">
                         <th nowrap="" valign="center" align="center">
                             <a class="listViewHeaderValues" href="javascript:void(0);">Numero<br/>Programma</a>
                         </th>
                         <th nowrap="" valign="center" align="center">
                             <a class="listViewHeaderValues" href="javascript:void(0);">Voce<br/>Incasso</a>
                         </th>
                         <th nowrap="" valign="center" align="center">
                             <a class="listViewHeaderValues" href="javascript:void(0);">Codice<br/>Campionamento</a>
                         </th>
                         <th nowrap="" valign="center" align="center">
                             <a class="listViewHeaderValues" href="javascript:void(0);">Risultato <br/>Calcolo resto</a>
                         </th>
                         <th nowrap="" valign="center" align="center">
                             <a class="listViewHeaderValues" href="javascript:void(0);">Flag da Campionare</a>
                         </th>
                         <th nowrap="" valign="center" align="center">
                             <a class="listViewHeaderValues" href="javascript:void(0);">Numero Programma<br/>Riferimento</a>
                         </th>
                     </tr>
                     </thead>

                     <tbody>

                     <c:forEach items="${resultPage.records}" var="p">
                         <tr id="row_${m.id}" class="listViewEntries">
                             <td class="listViewEntryValue medium" nowrap=""
                                 data-field-type="string">${p.numeroProgrammaMusicale}</td>
                             <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${p.voceIncasso}</td>

                             <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${p.codiceCampionamento}</td>
                             <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                                 <c:if test="${p.risultatoCalcoloResto != null}">
                                     ${p.risultatoCalcoloResto}
                                 </c:if>
                                 <c:if test="${p.risultatoCalcoloResto == null}">
                                     --
                                 </c:if>
                             </td>

                             <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                                 <c:if test="${p.flagDaCampionare != null}">
                                     ${p.flagDaCampionare}
                                 </c:if>
                                 <c:if test="${p.flagDaCampionare == null}">
                                     --
                                 </c:if>
                             </td>
                             <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                                 <c:if test="${p.pmRiferimento != null}">
                                     ${p.pmRiferimento}
                                 </c:if>
                                 <c:if test="${p.pmRiferimento == null}">
                                     --
                                 </c:if>
                             </td>
                         </tr>
                     </c:forEach>

                     </tbody>
                 </table>
                 </c:if>


                 <!-- FINE SEZIONE VIEW DATI -->

                 <!-- INIZIO TASTI FONDO PAGINA -->
                 <br/><br/>

                 <c:if test="${resultPage!=null and not empty resultPage.records}">
                 <div class="row-fluid">

                     <div class="pull-right">
                         <button name="download" id="downloadId" class="btn btn-success" type="submit">
                             <strong>Download csv</strong>
                         </button>
                     </div>

                     <div class="clearfix"></div>
                 </div>
                 </c:if>


                 <!-- FINE TASTI FONDO PAGINA -->
                 <input type="hidden" name="annoInizioPeriodoContabileSel" id="annoInizioPeriodoContabileSelId"
                        value="${annoInizioPeriodoContabile}"/>
                 <input type="hidden" name="meseInizioPeriodoContabileSel" id="meseInizioPeriodoContabileSelId"
                        value="${meseInizioPeriodoContabile}"/>
                 <input type="hidden" name="annoFinePeriodoContabileSel" id="annoFinePeriodoContabileSelId"
                        value="${annoFinePeriodoContabile}"/>
                 <input type="hidden" name="meseFinePeriodoContabileSel" id="meseFinePeriodoContabileSelId"
                        value="${meseFinePeriodoContabile}"/>
     </form>


     <script type="text/javascript">

     $(document).ready( function() {

         $("#annoInizioPeriodoContabileId").val(${annoInizioPeriodoContabile});
         $("#meseInizioPeriodoContabileId").val(${meseInizioPeriodoContabile});
         $("#annoFinePeriodoContabileId").val(${annoFinePeriodoContabile});
         $("#meseFinePeriodoContabileId").val(${meseFinePeriodoContabile});

        $('#cercaId').click(function(e){

          document.getElementById("searchFormId").action="${contextPath}/secure/viewFlussoGuida";
          document.getElementById("searchFormId").submit();

        });

        $('#downloadId').click(function(e){

         var okAnswer = confirm("Attenzione!\nProseguendo con il download tutti i Programmi Musicali saranno etichettati come inviati a Need per l'acquisizione.\nSi vuole proseguire ?");

          if (okAnswer==true){
              document.getElementById("searchFormId").action="${contextPath}/secure/downloadCSVFlussoGuida";
              document.getElementById("searchFormId").submit();
          }else{
             return false;
          }



        });

     });



     </script>

     <!-- INIZIO FOOTER -->
     <jsp:include page="footer.jsp"/>
     <!-- FINE FOOTER -->

 </div>
 </body>
</html>