<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<div class="navbar commonActionsContainer noprint">
    <div class="actionsContainer row-fluid" style="height: 120px;">
        <div class="span2">
    <span class="companyLogo">
      <img src="${contextPath}/images/siae_logo_siaetrasparente.png" title="siae_logo_siaetrasparente.png" alt="siae_logo_siaetrasparente.png">&nbsp;</span>
        </div>
        <div class="span10">
            <div class="row-fluid">

                <form class="form-horizontal recordEditView" id="searchFormId" name="searchForm" method="get" action="${contextPath}/secure/aggregatiListSMC">

                    <div class="searchElement span14">

                        <div class="select-search">
                            <a class="globalSearch_module_All">Contabilita' da:</a>
                            <select style="width: 110px" class="js-basic-single-select" id="meseInizioPeriodoContabileId" name="meseInizioPeriodoContabile" required="true">
                                <option value="" class="globalSearch_module_All" selected>- Mese -</option>
                                <option value="1" class="globalSearch_module_All" >Gennaio</option>
                                <option value="2" class="globalSearch_module_All" >Febbraio</option>
                                <option value="3" class="globalSearch_module_All" >Marzo</option>
                                <option value="4" class="globalSearch_module_All" >Aprile</option>
                                <option value="5" class="globalSearch_module_All" >Maggio</option>
                                <option value="6" class="globalSearch_module_All" >Giugno</option>
                                <option value="7" class="globalSearch_module_All" >Luglio</option>
                                <option value="8" class="globalSearch_module_All" >Agosto</option>
                                <option value="9" class="globalSearch_module_All" >Settembre</option>
                                <option value="10" class="globalSearch_module_All" >Ottobre</option>
                                <option value="11" class="globalSearch_module_All" >Novembre</option>
                                <option value="12" class="globalSearch_module_All" >Dicembre</option>
                            </select>
                            <select style="width: 80px" class="js-basic-single-select" id="annoInizioPeriodoContabileId" name="annoInizioPeriodoContabile" required="true">
                                <option value="" class="globalSearch_module_All" selected>- Anno -</option>
                                <option value="2015" class="globalSearch_module_All" >2015</option>
                                <option value="2016" class="globalSearch_module_All" >2016</option>
                                <option value="2017" class="globalSearch_module_All" >2017</option>
                                <option value="2018" class="globalSearch_module_All" >2018</option>
                                <option value="2019" class="globalSearch_module_All" >2019</option>
                                <option value="2020" class="globalSearch_module_All" >2020</option>
                                <option value="2021" class="globalSearch_module_All" >2021</option>
                                <option value="2022" class="globalSearch_module_All" >2022</option>
                                <option value="2023" class="globalSearch_module_All" >2023</option>
                                <option value="2024" class="globalSearch_module_All" >2024</option>
                                <option value="2025" class="globalSearch_module_All" >2025</option>
                                <option value="2026" class="globalSearch_module_All" >2026</option>
                                <option value="2027" class="globalSearch_module_All" >2027</option>
                                <option value="2028" class="globalSearch_module_All" >2028</option>
                                <option value="2029" class="globalSearch_module_All" >2029</option>
                                <option value="2030" class="globalSearch_module_All" >2030</option>
                            </select>
                            <a class="globalSearch_module_All">a:</a>
                            <select style="width: 110px" class="js-basic-single-select" id="meseFinePeriodoContabileId" name="meseFinePeriodoContabile" required="true">
                                <option value="" class="globalSearch_module_All" selected>- Mese -</option>
                                <option value="1" class="globalSearch_module_All" >Gennaio</option>
                                <option value="2" class="globalSearch_module_All" >Febbraio</option>
                                <option value="3" class="globalSearch_module_All" >Marzo</option>
                                <option value="4" class="globalSearch_module_All" >Aprile</option>
                                <option value="5" class="globalSearch_module_All" >Maggio</option>
                                <option value="6" class="globalSearch_module_All" >Giugno</option>
                                <option value="7" class="globalSearch_module_All" >Luglio</option>
                                <option value="8" class="globalSearch_module_All" >Agosto</option>
                                <option value="9" class="globalSearch_module_All" >Settembre</option>
                                <option value="10" class="globalSearch_module_All" >Ottobre</option>
                                <option value="11" class="globalSearch_module_All" >Novembre</option>
                                <option value="12" class="globalSearch_module_All" >Dicembre</option>
                            </select>
                            <select style="width: 80px" class="js-basic-single-select" id="annoFinePeriodoContabileId" name="annoFinePeriodoContabile" required="true">
                                <option value="" class="globalSearch_module_All" selected>- Anno -</option>
                                <option value="2015" class="globalSearch_module_All" >2015</option>
                                <option value="2016" class="globalSearch_module_All" >2016</option>
                                <option value="2017" class="globalSearch_module_All" >2017</option>
                                <option value="2018" class="globalSearch_module_All" >2018</option>
                                <option value="2019" class="globalSearch_module_All" >2019</option>
                                <option value="2020" class="globalSearch_module_All" >2020</option>
                                <option value="2021" class="globalSearch_module_All" >2021</option>
                                <option value="2022" class="globalSearch_module_All" >2022</option>
                                <option value="2023" class="globalSearch_module_All" >2023</option>
                                <option value="2024" class="globalSearch_module_All" >2024</option>
                                <option value="2025" class="globalSearch_module_All" >2025</option>
                                <option value="2026" class="globalSearch_module_All" >2026</option>
                                <option value="2027" class="globalSearch_module_All" >2027</option>
                                <option value="2028" class="globalSearch_module_All" >2028</option>
                                <option value="2029" class="globalSearch_module_All" >2029</option>
                                <option value="2030" class="globalSearch_module_All" >2030</option>
                            </select>

                        </div>

                        <br/><br/>

                        <div class="select-search">
                            <select style="width: 120px" class="js-basic-single-select" id="tipoSupportoId" name="tipoSupporto">
                                <option value="" class="globalSearch_module_All" selected>- Supporto -</option>
                                <option value="DIGITALE" class="globalSearch_module_All" >Digitale</option>
                                <option value="CARTACEO" class="globalSearch_module_All" >Carteceo</option>
                            </select>
                        </div>

                        <div class="select-search">
                            <select class="chzn-select chzn-done" name="tipoProgramma" id="tipoProgrammaId" style="width: 160px; display: block;">
                                <option value="" class="globalSearch_module_All" selected>- Tipo Programmi -</option>
                                <c:forEach items="${tipoProgrammi}" var="t">
                                    <option value="${t}" class="globalSearch_module_All">
                                            ${t}
                                    </option>
                                </c:forEach>
                            </select>
                        </div>

                        <div class="input-append searchBar">
                            <input type="text" readonly="true" style="width: 100px" class="" value="2243" name="voceIncasso" id="voceIncasso" placeholder="Voce Incasso" results="5">&nbsp;&nbsp;
                        </div>

                        <div class="input-append searchBar">
                            <input type="text" style="width: 150px;" class="" value="${numeroPM}" name="numeroPM" id="numeroPMId" placeholder="Numero Programma" results="10">&nbsp;&nbsp;
                        </div>

                        <br/><br/>

                    </div>

                    <input type="hidden" id="pageId" name="page" value="1"/>

                    <div>
                          <span class="pull-right">
                           <button class="btn btn-success" type="submit">
                            <strong>Cerca</strong>
                           </button>
                         </span>
                    </div>

                </form>

                <script type="text/javascript">

                    $(function() {
                       $("#annoInizioPeriodoContabileId").val(${annoInizioPeriodoContabile});
                       $("#meseInizioPeriodoContabileId").val(${meseInizioPeriodoContabile});
                       $("#annoFinePeriodoContabileId").val(${annoFinePeriodoContabile});
                       $("#meseFinePeriodoContabileId").val(${meseFinePeriodoContabile});

                    });

                    function callPage(page){
                        $("#pageId").val(page);
                        document.getElementById("searchFormId").submit();
                    }
                    document.getElementById('tipoSupportoId').value = '${tipoSupporto}';
                    document.getElementById('tipoProgrammaId').value = '${tipoProgramma}';

                    function doSubmit(movimento){
                        document.forms.ListaMovimentiViewId.elements.movimento.value=movimento;
                        document.forms.ListaMovimentiViewId.submit();
                    }

                </script>

            </div></div></div></div>
</div>

<div class="bodyContents" style="min-height: 488px;">
    <div class="mainContainer row-fluid" style="min-height: 1355px;">
        <div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 1355px;">
            <div class="container-fluid editViewContainer">

