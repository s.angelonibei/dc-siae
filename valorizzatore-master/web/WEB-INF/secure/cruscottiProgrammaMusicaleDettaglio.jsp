<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>

 <jsp:include page="header.jsp"/>

<body data-language="it_it">

<div id="page">

    <!-- container which holds data temporarly for pjax calls -->
    <div id="pjaxContainer" class="hide noprint"></div>

    <!-- INIZIO INTESTAZIONE-->
    <jsp:include page="menu_bar.jsp"/>
    <!-- FINE INTESTAZIONE-->

    <!-- INIZIO BARRA DI RICERCA -->
    <jsp:include page="search_bar.jsp"/>
    <!-- FINE BARRA DI RICERCA -->

    <script type="text/javascript">
       function toggle_visibility(id) {
           var e = document.getElementById(id);
           if(e.style.display == 'block')
              e.style.display = 'none';
           else
              e.style.display = 'block';
       }
    </script>

    <!-- BODY -->
    <form class="form-horizontal recordEditView" id="EventoDettaglioFormId" name="EventoDettaglioForm" method="post" action="">

      <c:if test="${programmaMusicale != null}">

        <!-- INIZIO SEZIONE DATI -->

         <!-- Sezione Programma Musicale -->
         <table class="table table-bordered blockContainer showInlineTable equalSplit">
            <thead>
            <tr>
                <th class="blockHeader" colspan="4">Riepilogo Programma Musicale</th>
            </tr>
            </thead>
            <tbody style="display: table-row-group;">

                <tr>
                    <!-- blocco 1 -->
                    <td class="fieldLabel medium">
                        <label class="muted pull-right marginRight10px">
                            <label class="muted pull-right marginRight10px">Numero Programma</label>
                        </label>
                    </td>
                    <td class="fieldValue medium">
                        <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                                ${programmaMusicale.numeroProgrammaMusicale}
                        </div></div></span></div>
                    </td>
                    <!-- blocco 1 -->
                    <!-- blocco 2 -->
                    <td class="fieldLabel medium">
                        <label class="muted pull-right marginRight10px">
                           Voce Incasso
                        </label>
                    </td>
                    <td class="fieldValue medium">
                        <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                                ${programmaMusicale.voceIncasso}
                        </div></div></span></div>
                    </td>
                    <!-- blocco 2 -->
                </tr>

                <tr>
                    <td class="fieldLabel medium">
                        <label class="muted pull-right marginRight10px">
                            Tipo supporto
                        </label>
                    </td>
                    <td class="fieldValue medium">
                        <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                                ${programmaMusicale.tipologiaPm}
                        </div></div></span></div>
                    </td>

                    <!-- blocco 1 -->
                    <td class="fieldLabel medium">
                        <label class="muted pull-right marginRight10px">
                            <label class="muted pull-right marginRight10px">Tipo</label>
                        </label>
                    </td>
                    <td class="fieldValue medium">
                        <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                                ${programmaMusicale.tipoPm}
                        </div></div></span></div>
                    </td>
                    <!-- blocco 1 -->

                </tr>

                <tr>
                    <!-- blocco 1 -->
                    <td class="fieldLabel medium">
                        <label class="muted pull-right marginRight10px">
                            <label class="muted pull-right marginRight10px">Flag campionamento</label>
                        </label>
                    </td>
                    <td class="fieldValue medium">
                        <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                                ${programmaMusicale.flagDaCampionare}
                        </div></div></span></div>
                    </td>
                    <!-- blocco 1 -->
                    <!-- blocco 2 -->
                    <td class="fieldLabel medium">
                        <label class="muted pull-right marginRight10px">
                            Codice campionamento
                        </label>
                    </td>
                    <td class="fieldValue medium">
                        <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                                ${programmaMusicale.codiceCampionamento}
                        </div></div></span></div>
                    </td>
                    <!-- blocco 2 -->

                </tr>

                <tr>
                    <!-- blocco 1 -->
                    <td class="fieldLabel medium">
                        <label class="muted pull-right marginRight10px">
                            Risultato calcolo campionamento
                        </label>
                    </td>
                    <td class="fieldValue medium">
                        <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                                ${programmaMusicale.risultatoCalcoloResto}
                        </div></div></span></div>
                    </td>
                    <!-- blocco 1 -->
                    <!-- blocco 2 -->
                    <td class="fieldLabel medium">
                        <label class="muted pull-right marginRight10px">
                            Totale durata cedole
                        </label>
                    </td>
                    <td class="fieldValue medium">
                        <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                                ${programmaMusicale.totaleDurataCedole}
                        </div></div></span></div>
                    </td>
                    <!-- blocco 2 -->
                </tr>

                <tr>
                    <!-- blocco 1 -->
                    <td class="fieldLabel medium">
                        <label class="muted pull-right marginRight10px">
                            <label class="muted pull-right marginRight10px">Totale cedole</label>
                        </label>
                    </td>
                    <td class="fieldValue medium">
                        <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                                ${programmaMusicale.totaleCedole}
                        </div></div></span></div>
                    </td>
                    <!-- blocco 1 -->
                    <!-- blocco 2 -->
                    <td class="fieldLabel medium">
                        <label class="muted pull-right marginRight10px">
                            <label class="muted pull-right marginRight10px">Programma foglio segue di:</label>
                        </label>
                    </td>
                    <td class="fieldValue medium">
                        <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                                ${programmaMusicale.pmRiferimento}
                        </div></div></span></div>
                    </td>
                    <!-- blocco 2 -->
                </tr>

                <tr>
                    <!-- blocco 1 -->
                    <td class="fieldLabel medium">
                        <label class="muted pull-right marginRight10px">
                            Data rientro
                        </label>
                    </td>
                    <td class="fieldValue medium">
                        <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                                ${programmaMusicale.dataRientroPM}
                        </div></div></span></div>
                    </td>
                    <!-- blocco 1 -->
                    <!-- blocco 2 -->
                    <td class="fieldLabel medium">
                        <label class="muted pull-right marginRight10px">
                            Direttore Esecuzione
                        </label>
                    </td>
                    <td class="fieldValue medium">
                        <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                                ${programmaMusicale.direttoreEsecuzione.direttoreEsecuzione}
                        </div></div></span></div>
                    </td>
                    <!-- blocco 2 -->
                </tr>

            </tbody>
        </table>
        <!-- Sezione Programma Musicale -->
         <br/>

          <!-- Sezione Eventi -->
          <table class="table table-bordered blockContainer showInlineTable equalSplit">
              <thead>
              <tr>
                  <th class="blockHeader" colspan="4">Riepilogo Evento</th>
              </tr>
              </thead>
              <tbody style="display: table-row-group;">
              <tr>
                  <!-- blocco 1 -->
                  <td class="fieldLabel medium">
                      <label class="muted pull-right marginRight10px">
                          <label class="muted pull-right marginRight10px">Seprag punto territoriale</label>
                      </label>
                  </td>
                  <td class="fieldValue medium">
                      <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                              ${programmaMusicale.cartella.seprag} - ${programmaMusicale.cartella.descrizionePuntoTerritoriale}
                      </div></div></span></div>
                  </td>
                  <!-- blocco 1 -->
                  <!-- blocco 2 -->
                  <td class="fieldLabel medium">
                      <label class="muted pull-right marginRight10px">
                          Locale
                      </label>
                  </td>
                  <td class="fieldValue medium">
                      <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                              ${programmaMusicale.eventi.get(0).denominazioneLocale}
                      </div></div></span></div>
                  </td>
                  <!-- blocco 2 -->
              </tr>
              <tr>
                  <!-- blocco 1 -->
                  <td class="fieldLabel medium">
                      <label class="muted pull-right marginRight10px">
                          <label class="muted pull-right marginRight10px">Codice locale</label>
                      </label>
                  </td>
                  <td class="fieldValue medium">
                      <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                              ${programmaMusicale.eventi.get(0).codiceLocale}
                      </div></div></span></div>
                  </td>
                  <!-- blocco 1 -->
                  <!-- blocco 2 -->
                  <td class="fieldLabel medium">
                      <label class="muted pull-right marginRight10px">
                          Localit&aacute;
                      </label>
                  </td>
                  <td class="fieldValue medium">
                      <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                              ${programmaMusicale.eventi.get(0).denominazioneLocalita}
                      </div></div></span></div>
                  </td>
                  <!-- blocco 2 -->
              </tr>

              <tr>
                  <!-- blocco 1 -->
                  <td class="fieldLabel medium">
                      <label class="muted pull-right marginRight10px">
                          <label class="muted pull-right marginRight10px">Provincia</label>
                      </label>
                  </td>
                  <td class="fieldValue medium">
                      <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                              ${programmaMusicale.eventi.get(0).siglaProvinciaLocale}
                      </div></div></span></div>
                  </td>
                  <!-- blocco 1 -->
                  <!-- blocco 2 -->
                  <td class="fieldLabel medium">
                      <label class="muted pull-right marginRight10px">

                      </label>
                  </td>
                  <td class="fieldValue medium">
                      <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">

                      </div></div></span></div>
                  </td>
                  <!-- blocco 2 -->
              </tr>

              </tbody>
          </table>

         <br/>

          <table class="table table-bordered blockContainer showInlineTable equalSplit">

              <tbody style="display: table-row-group;">

              <c:forEach items="${programmaMusicale.eventi}" var="e">

                  <tr>
                      <!-- blocco 1 -->
                      <td class="fieldLabel medium">
                          <label class="muted pull-right marginRight10px">
                              <label class="muted pull-right marginRight10px">Data inizio evento</label>
                          </label>
                      </td>
                      <td class="fieldValue medium">
                          <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                                ${e.dataInizioEventoStr}
                        </div></div></span></div>
                      </td>
                      <!-- blocco 1 -->
                      <!-- blocco 2 -->
                      <td class="fieldLabel medium">
                          <label class="muted pull-right marginRight10px">Data fine evento</label>
                      </td>
                      <td class="fieldValue medium">
                          <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                                  ${e.dataFineEventoStr}
                          </div></div></span></div>
                      </td>
                      <!-- blocco 2 -->
                  </tr>


                  <tr>
                      <!-- blocco 1 -->
                      <td class="fieldLabel medium">
                          <label class="muted pull-right marginRight10px">
                              <label class="muted pull-right marginRight10px">Ora inizio evento</label>
                          </label>
                      </td>
                      <td class="fieldValue medium">
                          <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                               ${e.oraInizioEvento}
                        </div></div></span></div>
                      </td>
                      <!-- blocco 1 -->
                      <!-- blocco 2 -->
                      <td class="fieldLabel medium">
                          <label class="muted pull-right marginRight10px">
                              Ora fine evento
                          </label>
                      </td>
                      <td class="fieldValue medium">
                          <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                                  ${e.oraFineEvento}
                          </div></div></span></div>
                      </td>
                      <!-- blocco 2 -->
                  </tr>
                  <tr>
                      <!-- blocco 1 -->
                      <td class="fieldLabel medium">
                          <label class="muted pull-right marginRight10px">
                              <label class="muted pull-right marginRight10px"></label>
                          </label>
                      </td>
                      <td class="fieldValue medium">
                          <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">

                          </div></div></span></div>
                      </td>
                      <!-- blocco 1 -->
                      <!-- blocco 2 -->
                      <td class="fieldLabel medium">
                          <label class="muted pull-right marginRight10px">

                          </label>
                      </td>
                      <td class="fieldValue medium">
                          <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">

                          </div></div></span></div>
                      </td>
                      <!-- blocco 2 -->
                  </tr>
              </c:forEach>

              </tbody>
          </table>
          <!-- Sezione Eventi -->

         <br/>


            <!-- Sezione Movimenti Programma Musicale -->
            <table class="table table-bordered blockContainer showInlineTable equalSplit">
                <thead>
                <tr>
                    <th class="blockHeader" colspan="4">Riepilogo Movimenti</th>
                </tr>
                </thead>
            </table>
            <table class="table table-bordered listViewEntriesTable">
                <thead>
                <tr class="listViewHeaders">
                    <th nowrap="" valign="center" align="center">
                        <a class="listViewHeaderValuesSoft">Periodo<br/>rientro</a>
                    </th>
                    <th nowrap="" valign="center" align="center">
                        <a class="listViewHeaderValuesSoft">Evento</a>
                    </th>
                    <th nowrap="" valign="center" align="center">
                        <a class="listViewHeaderValuesSoft">Voce incasso</a>
                    </th>
                    <th nowrap="" valign="center" align="center">
                        <a class="listViewHeaderValuesSoft">Numero<br/>fattura</a>
                    </th>
                    <th nowrap="" valign="center" align="center">
                        <a class="listViewHeaderValuesSoft">Numero<br/>reversale</a>
                    </th>
                    <th nowrap="" valign="center" align="center">
                        <a class="listViewHeaderValuesSoft">Data<br/>reversale</a>
                    </th>
                    <th nowrap="" valign="center" align="center">
                        <a class="listViewHeaderValuesSoft">Tipo<br/>Movimento</a>
                    </th>
                    <th nowrap="" valign="center" align="center">
                        <a class="listViewHeaderValuesSoft">Numero<br/>PM</a>
                    </th>
                    <th nowrap="" valign="center" align="center">
                        <a class="listViewHeaderValuesSoft">Importo<br/>SUN</a>
                    </th>
                    <th nowrap="" valign="center" align="center">
                        <a class="listViewHeaderValuesSoft">Importo<br/>Ricalcolato</a>
                    </th>
                    <th nowrap="" valign="center" align="center">
                        <a class="listViewHeaderValuesSoft">Importo<br/>Manuale</a>
                    </th>
                </tr>
                </thead>

                <tbody>

                    <c:forEach items="${programmaMusicale.movimenti}" var="m">
                        <tr class="listViewEntries">
                            <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.contabilita}</td>
                            <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                                    ${m.evento.dataInizioEventoStr}  ${m.evento.oraInizioEvento}
                            </td>

                            <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.voceIncasso}</td>
                            <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.numeroFattura}</td>
                            <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.reversale}</td>
                            <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.dataReversaleStr}</td>
                            <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.tipologiaMovimento}</td>
                            <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                                 <c:if test="${(m.voceIncasso =='2244' or m.voceIncasso =='2243') and m.flagGruppoPrincipale=='1'}">
                                     ${m.numProgrammaMusicale} (Principale)
                                 </c:if>
                                 <c:if test="${(m.voceIncasso =='2244' or m.voceIncasso =='2243') and m.flagGruppoPrincipale=='0'}">
                                     ${m.numProgrammaMusicale} (Spalla)
                                 </c:if>
                                <c:if test="${(m.voceIncasso !='2244' and m.voceIncasso !='2243')}">
                                    ${m.numProgrammaMusicale}
                                </c:if>
                            </td>

                            <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                                <c:if test="${m.importiRicalcolati == null or empty m.importiRicalcolati}">
                                    ${m.importoTotDemFormatted}
                                </c:if>
                                <c:if test="${m.importiRicalcolati != null and not empty m.importiRicalcolati}">
                                    <strike>${m.importoTotDemFormatted}</strike>
                                </c:if>
                            </td>

                            <td class="listViewEntryValue medium" nowrap="" data-field-type="string">

                                <c:if test="${m.importiRicalcolati != null and not empty m.importiRicalcolati}">
                                    <a href="javascript:toggle_visibility('importiDiv_${m.id}')"><u>${m.importiRicalcolati.get(0).importoFormatted}</u></a>
                                    <div id="importiDiv_${m.id}" style="display:none; background:#F2F2F2; border-style: groove; width: 420px;">
                                        <c:forEach items="${m.importiRicalcolati}" var="i">

                                            <c:if test="${i.importoSingolaCedola != null}">
                                              &#8226; Elab. ${i.idEsecuzioneRicalcolo} del ${i.dataOraInserimentoStr}: ${i.importoFormatted} Un. &euro; ${i.importoSingolaCedola}<br/>
                                            </c:if>

                                            <c:if test="${i.importoSingolaCedola == null}">
                                                &#8226; Elab. ${i.idEsecuzioneRicalcolo} del ${i.dataOraInserimentoStr}: ${i.importoFormatted}<br/>
                                            </c:if>

                                        </c:forEach>
                                    </div>
                                </c:if>

                                <c:if test="${m.importiRicalcolati == null or empty m.importiRicalcolati}">
                                    0.0 &#8364;
                                </c:if>

                            </td>

                            <td class="listViewEntryValue medium" nowrap="" data-field-type="string">

                                <c:if test="${m.importoManuale != null}">
                                    ${m.importoManualeFormatted}
                                </c:if>
                                <c:if test="${m.importoManuale == null}">
                                    0.0 &#8364;
                                </c:if>
                            </td>
                        </tr>
                    </c:forEach>

                    <tr class="listViewEntries">
                        <td class="listViewEntryValue medium" nowrap="" data-field-type="string"> <b>Totale</b></td>
                        <td class="listViewEntryValue medium" nowrap="" data-field-type="string" colspan="7"/>
                        <td class="listViewEntryValue medium" nowrap="" data-field-type="string"> <b>${programmaMusicale.importo.importoAggregatoSUNFormatted}</b></td>
                        <td class="listViewEntryValue medium" nowrap="" data-field-type="string">

                            <c:if test="${programmaMusicale.importo.importoTotaleRicalcolato != null}">
                                <b> ${programmaMusicale.importo.importoTotaleRicalcolato} &#8364; </b>
                            </c:if>

                            <c:if test="${programmaMusicale.importo.importiRicalcolati != null and not empty programmaMusicale.importo.importiRicalcolati}">

                               <a href="javascript:toggle_visibility('importiDivTot')"><u>${programmaMusicale.importo.importiRicalcolati.get(0).importoFormatted}</u></a>
                               <div id="importiDivTot" style="display:none; background:#F2F2F2; border-style: groove; width: 250px;">
                                   <b>
                                       <c:forEach items="${programmaMusicale.importo.importiRicalcolati}" var="i">
                                             &#8226; Elabor. ${i.idEsecuzioneRicalcolo}: ${i.importoFormatted}<br/>
                                       </c:forEach>
                                   </b>
                               </div>

                            </c:if>

                            <c:if test="${programmaMusicale.importo.importoTotaleRicalcolato == null and (programmaMusicale.importo.importiRicalcolati == null or programmaMusicale.importo.importiRicalcolati.size()==0)}">
                               <b>0.0 &#8364;  </b>
                            </c:if>

                        </td>
                        <td class="listViewEntryValue medium" nowrap="" data-field-type="string">

                        </td>

                    </tr>

                </tbody>
            </table>
            <!-- Sezione Movimenti Programma Musicale -->


          <br/>

          <!-- Sezione Opere Programma Musicale -->
          <table class="table table-bordered blockContainer showInlineTable equalSplit">
              <thead>
              <tr>
                  <th class="blockHeader" colspan="4">Lista Opere</th>
              </tr>
              </thead>
          </table>
          <table class="table table-bordered listViewEntriesTable">
              <thead>
              <tr class="listViewHeaders">
                  <th nowrap="" valign="center" align="center">
                      <a class="listViewHeaderValuesSoft">Codice</a>
                  </th>
                  <th nowrap="" valign="center" align="center">
                      <a class="listViewHeaderValuesSoft">Titolo</a>
                  </th>
                  <th nowrap="" valign="center" align="center">
                      <a class="listViewHeaderValuesSoft">Compositore</a>
                  </th>
                  <th nowrap="" valign="center" align="center">
                      <a class="listViewHeaderValuesSoft">Durata</a>
                  </th>
                  <th nowrap="" valign="center" align="center">
                      <a class="listViewHeaderValuesSoft">Durata meno<br/>30 sec</a>
                  </th>
                  <th nowrap="" valign="center" align="center">
                      <a class="listViewHeaderValuesSoft">Importo<br/>Singola Cedola</a>
                  </th>
              </tr>
              </thead>

              <tbody>

              <c:forEach items="${programmaMusicale.opere}" var="o">
                  <tr class="listViewEntries">
                      <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${o.codiceOpera}</td>
                      <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${o.titoloComposizione}</td>
                      <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${o.compositore}</td>
                      <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${o.durata}</td>
                      <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${o.durataInferiore60sec}</td>
                      <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                          <c:if test="${o.importoCedola != null}">
                             &euro; ${o.importoCedola}
                          </c:if>
                      </td>
                  </tr>
              </c:forEach>

              </tbody>
          </table>
          <!-- Sezione Opere Programma Musicale -->

        </c:if>

        <!-- FINE SEZIONE DATI -->

        <!-- INIZIO TASTI FONDO PAGINA -->
        <br/><br/>

        <div class="row-fluid">
            <div class="pull-right">
                <!--
                <button class="btn btn-success" type="submit">
                    <strong>Salva</strong>
                </button>
                <a class="cancelLink" type="reset" onclick="javascript:window.history.back();">Annulla</a>
                -->
                <a href="${contextPath}/secure/exportReport?idReport=Dettaglio_PM&detailType=PM&idDetail=${programmaMusicale.id}">
                    <img src="${contextPath}/images/excel.jpg" alt="Esporta in formato excel" width="35" height="38"/>
                </a>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- FINE TASTI FONDO PAGINA -->


    </form>


    <!-- INIZIO FOOTER -->
    <jsp:include page="footer.jsp"/>
    <!-- FINE FOOTER -->

</body>
</html>