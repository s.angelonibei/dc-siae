<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>

 <jsp:include page="header.jsp"/>

<body data-language="it_it">

<div id="page">

    <!-- container which holds data temporarly for pjax calls -->
    <div id="pjaxContainer" class="hide noprint"></div>

    <!-- INIZIO INTESTAZIONE-->
    <jsp:include page="menu_bar.jsp"/>
    <!-- FINE INTESTAZIONE-->

    <!-- INIZIO BARRA DI RICERCA -->
    <jsp:include page="search_bar.jsp"/>
    <!-- FINE BARRA DI RICERCA -->

    <!-- BODY -->
    <form class="form-horizontal recordEditView" id="RicalcoloFormId" name="RicalcoloForm" method="post" action="${contextPath}/secure/ricalcoloConfigurazioneSave">

        <!-- INIZIO SEZIONE DATI VALORIZZATORE -->
        <c:if test="${user.hasProfile('P_CONFIGURAZIONE_ALL')}">
            <table class="table table-bordered blockContainer showInlineTable equalSplit">
                <thead>
                <tr>
                    <th class="blockHeader" colspan="4">Configurazione</th>
                </tr>
                </thead>
                <tbody style="display: table-row-group;">

                    <tr>
                        <td class="fieldLabel medium">
                            <label class="muted pull-right marginRight10px">
                                <label class="muted pull-right marginRight10px">Fine ultimo periodo di ripatizione</label>
                            </label>
                        </td>
                        <td class="fieldValue medium">
                            <div class="row-fluid">
                            <span class="span10">
                                <div class="input-append row-fluid">
                                    <div class="span12 row-fluid date">

                                        <select style="width: 110px" class="js-basic-single-select" id="meseFinePeriodoRipartizioneId" name="meseFinePeriodoRipartizione">
                                            <option value="1">Gennaio</option>
                                            <option value="2">Febbraio</option>
                                            <option value="3">Marzo</option>
                                            <option value="4">Aprile</option>
                                            <option value="5">Maggio</option>
                                            <option value="6">Giugno</option>
                                            <option value="7">Luglio</option>
                                            <option value="8">Agosto</option>
                                            <option value="9">Settembre</option>
                                            <option value="10">Ottobre</option>
                                            <option value="11">Novembre</option>
                                            <option value="12">Dicembre</option>
                                        </select>

                                        <select style="width: 80px" class="js-basic-single-select" id="annoFinePeriodoRipartizioneId" name="annoFinePeriodoRipartizione">
                                            <option value="2015">2015</option>
                                            <option value="2016">2016</option>
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                            <option value="2022">2022</option>
                                            <option value="2023">2023</option>
                                            <option value="2024">2024</option>
                                            <option value="2025">2025</option>
                                            <option value="2026">2026</option>
                                            <option value="2027">2027</option>
                                            <option value="2028">2028</option>
                                            <option value="2029">2029</option>
                                            <option value="2030">2030</option>
                                        </select>

                                    </div>
                                </div>
                            </span>
                            </div>
                        </td>

                        <td class="fieldLabel medium">
                            <!--label class="muted pull-right marginRight10px">Nome</label-->
                        </td>
                        <td class="fieldValue medium">
                            <!--div class="row-fluid">
                                <span class="span10">
                                 <input id="Accounts_editView_fieldName_cf_2113" type="text" class="input-large" name="cf_2113" value="MARIO">
                                </span>
                            </div-->
                        </td>
                    </tr>


                    <tr>
                        <td class="fieldLabel medium">
                            <label class="muted pull-right marginRight10px">
                                <label class="muted pull-right marginRight10px">Inizio periodo contabile</label>
                            </label>
                        </td>
                        <td class="fieldValue medium">
                            <div class="row-fluid">
                            <span class="span10">
                                <div class="input-append row-fluid">
                                    <div class="span12 row-fluid date">

                                        <select style="width: 110px" class="js-basic-single-select" id="meseInizioPeriodoContabileId" name="meseInizioPeriodoContabile">
                                            <option value="1">Gennaio</option>
                                            <option value="2">Febbraio</option>
                                            <option value="3">Marzo</option>
                                            <option value="4">Aprile</option>
                                            <option value="5">Maggio</option>
                                            <option value="6">Giugno</option>
                                            <option value="7">Luglio</option>
                                            <option value="8">Agosto</option>
                                            <option value="9">Settembre</option>
                                            <option value="10">Ottobre</option>
                                            <option value="11">Novembre</option>
                                            <option value="12">Dicembre</option>
                                        </select>

                                        <select style="width: 80px" class="js-basic-single-select" id="annoInizioPeriodoContabileId" name="annoInizioPeriodoContabile">
                                            <option value="2015">2015</option>
                                            <option value="2016">2016</option>
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                            <option value="2022">2022</option>
                                            <option value="2023">2023</option>
                                            <option value="2024">2024</option>
                                            <option value="2025">2025</option>
                                            <option value="2026">2026</option>
                                            <option value="2027">2027</option>
                                            <option value="2028">2028</option>
                                            <option value="2029">2029</option>
                                            <option value="2030">2030</option>
                                        </select>

                                    </div>
                                </div>
                            </span>
                            </div>
                        </td>

                        <td class="fieldLabel medium">
                            <label class="muted pull-right marginRight10px">Fine periodo contabile</label>
                        </td>
                        <td class="fieldValue medium">
                            <div class="row-fluid">
                            <span class="span10">
                                <div class="input-append row-fluid">
                                    <div class="span12 row-fluid date">
                                        <select style="width: 110px" class="js-basic-single-select" id="meseFinePeriodoContabileId" name="meseFinePeriodoContabile">
                                            <option value="1">Gennaio</option>
                                            <option value="2">Febbraio</option>
                                            <option value="3">Marzo</option>
                                            <option value="4">Aprile</option>
                                            <option value="5">Maggio</option>
                                            <option value="6">Giugno</option>
                                            <option value="7">Luglio</option>
                                            <option value="8">Agosto</option>
                                            <option value="9">Settembre</option>
                                            <option value="10">Ottobre</option>
                                            <option value="11">Novembre</option>
                                            <option value="12">Dicembre</option>
                                        </select>

                                        <select style="width: 80px" class="js-basic-single-select" id="annoFinePeriodoContabileId" name="annoFinePeriodoContabile">
                                            <option value="2015">2015</option>
                                            <option value="2016">2016</option>
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                            <option value="2022">2022</option>
                                            <option value="2023">2023</option>
                                            <option value="2024">2024</option>
                                            <option value="2025">2025</option>
                                            <option value="2026">2026</option>
                                            <option value="2027">2027</option>
                                            <option value="2028">2028</option>
                                            <option value="2029">2029</option>
                                            <option value="2030">2030</option>
                                        </select>

                                    </div>
                                </div>
                            </span>
                            </div>
                        </td>
                    </tr>


                    <tr>
                        <td class="fieldLabel medium">
                            <label class="muted pull-right marginRight10px">
                                <label class="muted pull-right marginRight10px">Tipo di Programmi Musicali</label>
                            </label>
                        </td>
                        <td class="fieldValue medium">
                            <div class="row-fluid">
                            <span class="span10">
                                <div class="input-append row-fluid">
                                    <div class="span12 row-fluid date">
                                        <select style="width: 150px" class="js-basic-single-select" id="tipologiaPMId" name="tipologiaPM">
                                            <option value="CARTACEO">Cartacei</option>
                                            <option value="DIGITALE">Digitali</option>
                                            <option value="Tutte">Digitali, Cartacei</option>
                                        </select>
                                    </div>
                                </div>
                            </span>
                            </div>
                        </td>

                        <td class="fieldLabel medium">
                            <label class="muted pull-right marginRight10px">Campiona Digitali</label>
                        </td>
                        <td class="fieldValue medium">
                            <div class="row-fluid">
                            <span class="span10">
                                <div class="input-append row-fluid">
                                    <div class="span12 row-fluid date">
                                        <select style="width: 50px" class="js-basic-single-select" id="campionaDigitaliId" name="campionaDigitali">
                                            <option value="Y">Si</option>
                                            <option value="N">No</option>
                                        </select>
                                    </div>
                                </div>
                            </span>
                            </div>
                        </td>
                    </tr>


                </tbody>
            </table>
              <br/><br/><br/>
        </c:if>
        <!-- FINE SEZIONE DATI VALORIZZATORE -->


        <!-- INIZIO SEZIONE DATI VALORIZZATORE SMC -->
        <c:if test="${user.hasProfile('P_CONFIGURAZIONE_SMC')}">
          <table class="table table-bordered blockContainer showInlineTable equalSplit">
            <thead>
            <tr>
                <th class="blockHeader" colspan="4">Configurazione SMC</th>
            </tr>
            </thead>
            <tbody style="display: table-row-group;">
            <tr>
                <td class="fieldLabel medium">
                    <label class="muted pull-right marginRight10px">
                        <label class="muted pull-right marginRight10px">Fine ultimo periodo di ripatizione</label>
                    </label>
                </td>
                <td class="fieldValue medium">
                    <div class="row-fluid">
                        <span class="span10">
                            <div class="input-append row-fluid">
                                <div class="span12 row-fluid date">

                                    <select style="width: 110px" class="js-basic-single-select" id="meseFinePeriodoRipartizione2243Id" name="meseFinePeriodoRipartizione2243">
                                        <option value="1">Gennaio</option>
                                        <option value="2">Febbraio</option>
                                        <option value="3">Marzo</option>
                                        <option value="4">Aprile</option>
                                        <option value="5">Maggio</option>
                                        <option value="6">Giugno</option>
                                        <option value="7">Luglio</option>
                                        <option value="8">Agosto</option>
                                        <option value="9">Settembre</option>
                                        <option value="10">Ottobre</option>
                                        <option value="11">Novembre</option>
                                        <option value="12">Dicembre</option>
                                    </select>

                                    <select style="width: 80px" class="js-basic-single-select" id="annoFinePeriodoRipartizione2243Id" name="annoFinePeriodoRipartizione2243">
                                        <option value="2015">2015</option>
                                        <option value="2016">2016</option>
                                        <option value="2017">2017</option>
                                        <option value="2018">2018</option>
                                        <option value="2019">2019</option>
                                        <option value="2020">2020</option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                        <option value="2024">2024</option>
                                        <option value="2025">2025</option>
                                        <option value="2026">2026</option>
                                        <option value="2027">2027</option>
                                        <option value="2028">2028</option>
                                        <option value="2029">2029</option>
                                        <option value="2030">2030</option>
                                    </select>

                                </div>
                            </div>
                        </span>
                    </div>
                </td>

                <td class="fieldLabel medium">
                    <!--label class="muted pull-right marginRight10px">Nome</label-->
                </td>
                <td class="fieldValue medium">
                    <!--div class="row-fluid">
                        <span class="span10">
                         <input id="Accounts_editView_fieldName_cf_2113" type="text" class="input-large" name="cf_2113" value="MARIO">
                        </span>
                    </div-->
                </td>
            </tr>

            <tr>
                <td class="fieldLabel medium">
                    <label class="muted pull-right marginRight10px">
                        <label class="muted pull-right marginRight10px">Inizio periodo contabile</label>
                    </label>
                </td>
                <td class="fieldValue medium">
                    <div class="row-fluid">
                        <span class="span10">
                            <div class="input-append row-fluid">
                                <div class="span12 row-fluid date">

                                    <select style="width: 110px" class="js-basic-single-select" id="meseInizioPeriodoContabile2243Id" name="meseInizioPeriodoContabile2243">
                                        <option value="1">Gennaio</option>
                                        <option value="2">Febbraio</option>
                                        <option value="3">Marzo</option>
                                        <option value="4">Aprile</option>
                                        <option value="5">Maggio</option>
                                        <option value="6">Giugno</option>
                                        <option value="7">Luglio</option>
                                        <option value="8">Agosto</option>
                                        <option value="9">Settembre</option>
                                        <option value="10">Ottobre</option>
                                        <option value="11">Novembre</option>
                                        <option value="12">Dicembre</option>
                                    </select>

                                    <select style="width: 80px" class="js-basic-single-select" id="annoInizioPeriodoContabile2243Id" name="annoInizioPeriodoContabile2243">
                                        <option value="2015">2015</option>
                                        <option value="2016">2016</option>
                                        <option value="2017">2017</option>
                                        <option value="2018">2018</option>
                                        <option value="2019">2019</option>
                                        <option value="2020">2020</option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                        <option value="2024">2024</option>
                                        <option value="2025">2025</option>
                                        <option value="2026">2026</option>
                                        <option value="2027">2027</option>
                                        <option value="2028">2028</option>
                                        <option value="2029">2029</option>
                                        <option value="2030">2030</option>
                                    </select>

                                </div>
                            </div>
                        </span>
                    </div>
                </td>

                <td class="fieldLabel medium">
                    <label class="muted pull-right marginRight10px">Fine periodo contabile</label>
                </td>
                <td class="fieldValue medium">
                    <div class="row-fluid">
                        <span class="span10">
                            <div class="input-append row-fluid">
                                <div class="span12 row-fluid date">
                                    <select style="width: 110px" class="js-basic-single-select" id="meseFinePeriodoContabile2243Id" name="meseFinePeriodoContabile2243">
                                        <option value="1">Gennaio</option>
                                        <option value="2">Febbraio</option>
                                        <option value="3">Marzo</option>
                                        <option value="4">Aprile</option>
                                        <option value="5">Maggio</option>
                                        <option value="6">Giugno</option>
                                        <option value="7">Luglio</option>
                                        <option value="8">Agosto</option>
                                        <option value="9">Settembre</option>
                                        <option value="10">Ottobre</option>
                                        <option value="11">Novembre</option>
                                        <option value="12">Dicembre</option>
                                    </select>

                                    <select style="width: 80px" class="js-basic-single-select" id="annoFinePeriodoContabile2243Id" name="annoFinePeriodoContabile2243">
                                        <option value="2015">2015</option>
                                        <option value="2016">2016</option>
                                        <option value="2017">2017</option>
                                        <option value="2018">2018</option>
                                        <option value="2019">2019</option>
                                        <option value="2020">2020</option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                        <option value="2024">2024</option>
                                        <option value="2025">2025</option>
                                        <option value="2026">2026</option>
                                        <option value="2027">2027</option>
                                        <option value="2028">2028</option>
                                        <option value="2029">2029</option>
                                        <option value="2030">2030</option>
                                    </select>

                                </div>
                            </div>
                        </span>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="fieldLabel medium">
                    <label class="muted pull-right marginRight10px">
                        <label class="muted pull-right marginRight10px">Tipo di Programmi Musicali</label>
                    </label>
                </td>
                <td class="fieldValue medium">
                    <div class="row-fluid">
                        <span class="span10">
                            <div class="input-append row-fluid">
                                <div class="span12 row-fluid date">
                                    <select style="width: 150px" class="js-basic-single-select" id="tipologiaPM2243Id" name="tipologiaPM2243">
                                        <option value="CARTACEO">Cartacei</option>
                                        <option value="DIGITALE">Digitali</option>
                                        <option value="Tutte">Digitali, Cartacei</option>
                                    </select>
                                </div>
                            </div>
                        </span>
                    </div>
                </td>

                <td class="fieldLabel medium">

                </td>
                <td class="fieldValue medium">

                </td>
            </tr>

            </tbody>
        </table>
        </c:if>
        <!-- FINE SEZIONE DATI VALORIZZATORE SMC -->


        <!-- INIZIO TASTI FONDO PAGINA -->
        <br/><br/>

        <div class="row-fluid">
            <div class="pull-right">
                <button class="btn btn-success" type="submit">
                    <strong>Salva</strong>
                </button>
            </div>
            <div class="clearfix"></div>
        </div>

        <!-- FINE TASTI FONDO PAGINA -->

    </form>

    <script type="text/javascript">



     $(function() {
       $("#annoFinePeriodoRipartizioneId").val(${finePeriodoRipartizioneAnno});
       $("#meseFinePeriodoRipartizioneId").val(${finePeriodoRipartizioneMese});
       $("#annoInizioPeriodoContabileId").val(${inizioPeriodoRicalcoloAnno});
       $("#meseInizioPeriodoContabileId").val(${inizioPeriodoRicalcoloMese});
       $("#annoFinePeriodoContabileId").val(${finePeriodoRicalcoloAnno});
       $("#meseFinePeriodoContabileId").val(${finePeriodoRicalcoloMese});
       $("#tipologiaPMId").val("${tipologiaPM}");
       $("#campionaDigitaliId").val('${campionaDigitali}');


         $("#annoFinePeriodoRipartizione2243Id").val(${finePeriodoRipartizioneAnno2243});
         $("#meseFinePeriodoRipartizione2243Id").val(${finePeriodoRipartizioneMese2243});
         $("#annoInizioPeriodoContabile2243Id").val(${inizioPeriodoRicalcoloAnno2243});
         $("#meseInizioPeriodoContabile2243Id").val(${inizioPeriodoRicalcoloMese2243});
         $("#annoFinePeriodoContabile2243Id").val(${finePeriodoRicalcoloAnno2243});
         $("#meseFinePeriodoContabile2243Id").val(${finePeriodoRicalcoloMese2243});
         $("#tipologiaPM2243Id").val("${tipologiaPM2243}");
    });


    </script>


    <!-- INIZIO FOOTER -->
    <jsp:include page="footer.jsp"/>
    <!-- FINE FOOTER -->

</body>
</html>