<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Valorizzatore</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="${contextPath}/css/chosen.css" type="text/css" media="screen">
	<!--<link rel="stylesheet" href="${contextPath}/css/jquery-ui-1.8.16.custom.css" type="text/css" media="screen">-->
	<!--<link rel="stylesheet" href="${contextPath}/css/select2.css" type="text/css" media="screen">-->
	<link rel="stylesheet" href="${contextPath}/css/bootstrap.css" type="text/css" media="screen">
	<link rel="stylesheet" href="${contextPath}/css/jqueryBxslider.css" type="text/css" media="screen">
	<link rel="stylesheet" href="${contextPath}/css/styles.css" type="text/css" media="screen">
	<link rel="stylesheet" href="${contextPath}/css/validationEngine.jquery.css">
	<!--<link rel="stylesheet" href="${contextPath}/css/select2.css"> -->
	<link rel="stylesheet" href="${contextPath}/css/guiders-1.2.6.css">
	<link rel="stylesheet" href="${contextPath}/css/jquery.pnotify.default.css">
	<link rel="stylesheet" href="${contextPath}/css/jquery.pnotify.default.icons.css">

	<link rel="stylesheet" href="${contextPath}/css/jquery-ui.css">

	<link rel="stylesheet" href="${contextPath}/css/style.css" type="text/css" media="screen">

	<!--<link rel="stylesheet" href="${contextPath}/css/jquery.timepicker.css" type="text/css" media="screen"> -->

	<style type="text/css">
		@media print {.noprint { display:none; }}

		.ui-progressbar {
			position: relative;
		}
		.progress-label {
			position: absolute;
			left: 50%;
			top: 15px;
			font-weight: bold;
			text-shadow: 1px 1px 0 #fff;
		}
		.progress-label2243 {
			position: absolute;
			left: 50%;
			top: 15px;
			font-weight: bold;
			text-shadow: 1px 1px 0 #fff;
		}
	</style>

	<script type="text/javascript" src="${contextPath}/js/jquery.min.js"></script>
	<!--[if IE]>
	  <script type="text/javascript" src="libraries/html5shim/html5.js"></script>
	  <script type="text/javascript" src="libraries/html5shim/respond.js"></script>
	<![endif]-->

</head>
