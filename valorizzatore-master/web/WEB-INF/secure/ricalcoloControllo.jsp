<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>

<jsp:include page="header.jsp"/>

<body data-language="it_it">

<div id="page">

    <!-- container which holds data temporarly for pjax calls -->
    <div id="pjaxContainer" class="hide noprint"></div>

    <!-- INIZIO INTESTAZIONE-->
    <jsp:include page="menu_bar.jsp"/>
    <!-- FINE INTESTAZIONE-->

    <!-- INIZIO BARRA DI RICERCA -->
    <jsp:include page="search_bar_ricalcoloControllo.jsp"/>
    <!-- FINE BARRA DI RICERCA -->


    <!-- BODY -->
    <form class="form-horizontal recordEditView" id="EditView" name="EditView" method="get" action="">

        <!-- INIZIO SEZIONE VIEW DATI -->

        <c:if test="${resultPage!=null and empty resultPage.records and elabSelected != -1}">

            <div class="contents-topscroll noprint">
                <div class="topscroll-div" style="width: 1308px; height: 70px;">

                </div>
            </div>

            <div class="listViewEntriesDiv">
                <div class="bottomscroll-div" style="width: 1308px;">
                    Esecuzione <b>${esecuzioneCorrente.id}</b> del <b>${esecuzioneCorrente.dataOraInizio}</b>. Periodo contabile (AAAAMM): <b>${esecuzioneCorrente.contabilitaInizio}</b>-<b>${esecuzioneCorrente.contabilitaFine}</b>. Esito: <div><b>${esecuzioneCorrente.descrizioneEsito}</b> </div>
                    <br/><br/>
                    <b>Nessun dato ritornato</b>
                    <br/><br/><br/>
                </div>
            </div>
        </c:if>

            <c:if test="${resultPage.records.size() > 0}">

            <div class="listViewEntriesDiv">

                    Esecuzione <b>${esecuzioneCorrente.id}</b> del <b>${esecuzioneCorrente.dataOraInizio}</b>. Periodo contabile (AAAAMM): <b>${esecuzioneCorrente.contabilitaInizio}</b>-<b>${esecuzioneCorrente.contabilitaFine}</b>.

                    <c:if test="${esecuzioneCorrente.esitoEsecuzione}<>'0'">
                       Esito: <b>${esecuzioneCorrente.descrizioneEsito}</b>
                    </c:if>

                    <div class="contentHeader row-fluid">
                        <span class="pull-left">
                         <button class="btn btn-success" onclick="javascript:setElaborationSIADA();" type="button">
                          <strong>Aggrega per invio a SIADA periodo ${esecuzioneCorrente.contabilitaFine}</strong>
                         </button>
                       </span>
                    </div>

                    <c:if test="${esecuzioneCorrente.dataOraCongelamento!=null}">
                       <br/><br/>
                       <strong>Trasmesso a SIADA per periodo contabile ${esecuzioneCorrente.contabilitaSiada}</strong>
                    </c:if>

                    <center><span style="color: #ff0000">${message}</span></center>

                    <br/><br/>
                    Numero movimenti: <b>${resultPage.records.size()}</b> di <b>${resultPage.totRecords}</b>
                    <br/>
                    <center>

                        <c:if test="${resultPage.pages.size() > 1}">
                            <c:if test="${resultPage.currentPage > 1}">
                              <a href="javascript:callPage(${resultPage.currentPage-1});">< Prec</a>
                            </c:if>

                            <c:forEach items="${resultPage.pages}" var="p">
                                <c:choose>
                                    <c:when test="${resultPage.currentPage != p}">
                                        <a href="javascript:callPage(${p});">${p} </a>
                                    </c:when>
                                    <c:otherwise>
                                        <a style="color: #09b6eb;">${p} </a>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>

                            <c:if test="${resultPage.currentPage < resultPage.pages.size()}">
                               <a href="javascript:callPage(${resultPage.currentPage+1});"> > Suc </a>
                            </c:if>
                        </c:if>

                    </center>
                    <br/><br/>
                    <table class="table table-bordered listViewEntriesTable">
                        <thead>
                        <tr class="listViewHeaders">
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Evento</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Voce<br/>Incasso</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues"  href="javascript:void(0);">Fattura</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Numero<br/>Programma</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Numero<br/>Cedole</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Durata<br/>Cedole</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Tipo<br/>Documento</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Numero PM<br/>previsti</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Importo PM<br/>SUN</a>
                            </th>

                            <c:if test="${tipoSospensione=='RICALCOLATI'}">
                                <th nowrap="" valign="center" align="center">
                                    <a class="listViewHeaderValues" href="javascript:void(0);">Importo PM<br/>ricalcolato</a>
                                </th>
                                <th nowrap="" valign="center" align="center">
                                    <a class="listViewHeaderValues" href="javascript:void(0);">Importo<br/>Unitario</a>
                                </th>
                            </c:if>

                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues" href="javascript:void(0);">Importo<br/>Evento</a>
                            </th>

                            <th nowrap="" valign="center" align="center">
                            <c:if test="${tipoSospensione=='SOSPESI'}">
                                   <a class="listViewHeaderValues">Motivazione<br/>Sospensione</a>
                            </c:if>
                            <c:if test="${tipoSospensione=='ERRORE'}">
                                    <a class="listViewHeaderValues">Errore</a>
                            </c:if>
                                </th>


                        </tr>
                        </thead>

                        <tbody>

                        <c:forEach items="${resultPage.records}" var="m">
                            <tr id="row_${m.id}" class="listViewEntries">
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.idEvento}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.voceIncasso}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.numeroFattura}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.numProgrammaMusicale}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.totaleCedole}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.totaleDurataCedole}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                                    <c:if test="${m.tipoDocumentoContabile =='501'}">
                                        Mod. 501 (Entrata)
                                    </c:if>
                                    <c:if test="${m.tipoDocumentoContabile =='221'}">
                                        Mod. 221 (Uscita)
                                    </c:if>
                                </td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.numeroPmPrevisti}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.importoTotDemFormatted}</td>
                                <c:if test="${tipoSospensione=='RICALCOLATI'}">
                                  <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.importoRicalcolatoFormatted}</td>
                                    <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                                            <c:if test="${m.importoRicalcolatoSingolaCedola != null}">
                                                &euro; ${m.importoRicalcolatoSingolaCedola}
                                            </c:if>
                                    </td>
                                </c:if>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.importoEventoFormatted}</td>
                                <c:if test="${tipoSospensione=='SOSPESI' or tipoSospensione=='ERRORE'}">
                                  <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.motivazioneSospensione}</td>
                                </c:if>
                            </tr>
                        </c:forEach>

                        </tbody>
                    </table>
                    </c:if>
                <!-- FINE SEZIONE VIEW DATI -->

        <div class="contentHeader row-fluid">
            <!--h3 class="span8 textOverflowEllipsis">Valorizzatore Programmi Musicale</h3-->
              <!-- INIZIO TASTI INIZIO PAGINA
              <span class="pull-right">
               <button class="btn btn-success" type="submit">
                <strong>Cerca</strong>
               </button>
             </span>
             -->
             <!-- FINE TASTI INIZIO PAGINA -->
        </div>

                <c:if test="${resultPage!=null and not empty resultPage.records and resultPage.records.size() > 0}">
                    <!-- INIZIO TASTI FONDO PAGINA -->
                    <br/><br/>
                    <div class="row-fluid">
                        <div class="pull-right">
                            <a href="${contextPath}/secure/exportReport?idReport=Lista_Importi_${tipoSospensione}">
                                <img src="${contextPath}/images/excel.jpg" alt="Esporta in formato excel" width="35" height="38"/>
                            </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <!-- FINE TASTI FONDO PAGINA -->
                </c:if>
    </form>

    <!-- INIZIO FOOTER -->
    <jsp:include page="footer.jsp"/>
    <!-- FINE FOOTER -->

</body>
</html>