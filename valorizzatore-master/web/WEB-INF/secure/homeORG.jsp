<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>

 <jsp:include page="header.jsp"/>

<body data-language="it_it">

<div id="js_strings" class="hide noprint"></div>
<input type="hidden" id="start_day" value="Sunday">
<input type="hidden" id="row_type" value="medium">
<input type="hidden" id="current_user_id" value="1">

<div id="page">

    <!-- container which holds data temporarly for pjax calls -->
    <div id="pjaxContainer" class="hide noprint"></div>

    <!-- INIZIO INTESTAZIONE-->
    <jsp:include page="menu_bar.jsp"/>
    <!-- FINE INTESTAZIONE-->

    <!-- INIZIO BARRA DI RICERCA -->
    <jsp:include page="search_bar.jsp"/>
    <!-- FINE BARRA DI RICERCA -->

    <!-- INIZIO BARRA TITOLO -->
    <jsp:include page="title_bar.jsp"/>
    <!-- FINE BARRA TITOLO -->


    <!-- INIZIO SEZIONE ANAGRAFICA -->
    <table class="table table-bordered blockContainer showInlineTable equalSplit">
        <thead>
        <tr>

            <th class="blockHeader" colspan="4">
                <img class="cursorPointer alignMiddle blockToggle hide " data-id="364" data-mode="hide" src=".${contextPath}/images/arrowRight.png" style="display: none;">
                <img class="cursorPointer alignMiddle blockToggle " data-id="364" data-mode="show" src="${contextPath}/images/arrowDown.png" style="display: inline;">
                Anafrafica
            </th>

        </tr>
        </thead>
        <tbody style="display: table-row-group;">
        <tr>
            <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">
                    <span class="redColor">*</span> Cognome/Ragione Sociale
                </label>
            </td>
            <td class="fieldValue medium">
                <div class="row-fluid">
     <span class="span10">
       <input id="Accounts_editView_fieldName_accountname" type="text" class="input-large nameField" name="accountname" value="CIRCOLO DELLA VELA">
     </span>
                </div>
            </td>

            <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">Nome</label>
            </td>
            <td class="fieldValue medium">
                <div class="row-fluid">
    <span class="span10">
     <input id="Accounts_editView_fieldName_cf_2113" type="text" class="input-large" name="cf_2113" value="MARIO">
    </span>
                </div>
            </td>
        </tr>

        <tr>
            <td class="fieldLabel medium"><label class="muted pull-right marginRight10px">Ruolo</label></td>
            <td class="fieldValue medium"><div class="row-fluid"><span class="span10"><input id="Accounts_editView_fieldName_cf_2109" type="text" class="input-large " name="cf_2109" value="ORGANIZZATORE" ></span></div></td>
            <td class="fieldLabel medium"><label class="muted pull-right marginRight10px">Natura Giuridica</label></td>
            <td class="fieldValue medium"><div class="row-fluid"><span class="span10"><input id="Accounts_editView_fieldName_cf_2111" type="text" class="input-large " name="cf_2111" value="CIRCOLO"></span></div></td>
        </tr>

        <tr><td class="fieldLabel medium"><label class="muted pull-right marginRight10px">Partita IVA</label></td><td class="fieldValue medium"><div class="row-fluid"><span class="span10"><input id="Accounts_editView_fieldName_cf_1383" type="text" class="input-large "  name="cf_1383" value="04524320720" ></span></div></td><td class="fieldLabel medium"><label class="muted pull-right marginRight10px">Codice Fiscale</label></td><td class="fieldValue medium"><div class="row-fluid"><span class="span10"><input id="Accounts_editView_fieldName_cf_1381" type="text" class="input-large " name="cf_1381" value="04524320720"></span></div></td></tr>

        <tr>
            <td class="fieldLabel medium"><label class="muted pull-right marginRight10px">Data di nascita</label></td>
            <td class="fieldValue medium"><div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date"><input id="Accounts_editView_fieldName_cf_2115" type="text" class="dateField" name="cf_2115" data-date-format="dd-mm-yyyy" value="" ><span class="add-on"><i class="icon-calendar"></i></span></div></div></span></div></td>
            <td class="fieldLabel medium"><label class="muted pull-right marginRight10px">Luogo di nascita</label></td><td class="fieldValue medium"><div class="row-fluid"><span class="span10"><input id="Accounts_editView_fieldName_cf_2117" type="text" class="input-large" name="cf_2117" value="" ></span></div>
        </td>
        </tr>

        <tr>
            <td class="fieldLabel medium"><label class="muted pull-right marginRight10px">Sesso</label></td>
            <td class="fieldValue medium"><div class="row-fluid"><span class="span10">
    <select class="chzn-select  chzn-done" name="cf_2119" data-selected-value="" id="selBGS" style="display: none;">
     <option value="">[Selezionare un'opzione]</option>
     <option value="M">M</option>
      <option value="F">F</option>
   </select>
 <div id="selBGS_chzn" class="chzn-container chzn-container-single" style="width: 220px;">
  <a href="javascript:void(0)" class="chzn-single"><span>[Selezionare un'opzione]</span><div><b></b></div></a>

 <div class="chzn-drop" style="left: -9000px; width: 218px; top: 28px;">
  <div class="chzn-search">
   <input type="text" autocomplete="off" style="width: 183px;">
  </div>
    <ul class="chzn-results">
      <li id="selBGS_chzn_o_0" class="active-result result-selected" style="">[Selezionare un'opzione]</li>
      <li id="selBGS_chzn_o_1" class="active-result" style="">M</li>
      <li id="selBGS_chzn_o_2" class="active-result" style="">F</li>
    </ul>
 </div>
</div>
</span>
            </div>
            </td>

            <td class="fieldLabel medium"><label class="muted pull-right marginRight10px">Nazionalità</label></td>
            <td class="fieldValue medium">
                <div class="row-fluid">
    <span class="span10">
     <input id="Accounts_editView_fieldName_cf_2121" type="text" class="input-large"  name="cf_2121" value="ITALIANA">
   </span>
                </div>
            </td>
        </tr>

        <tr>
            <td class="fieldLabel medium"><label class="muted pull-right marginRight10px">Omocodia</label></td>
            <td class="fieldValue medium"><div class="row-fluid">
                <span class="span10"><input type="hidden" name="cf_2123" value="0">
                    <input id="Accounts_editView_fieldName_cf_2123" type="checkbox" name="cf_2123"></span></div></td>
            <td class="fieldLabel medium"></td><td class="medium"></td>
        </tr>

        </tbody>
    </table>

    <!-- FINE SEZIONE ANAGRAFICA -->

    <!-- INIZIO SEZIONE SEDE LEGALE -->

    <br>
    <table class="table table-bordered blockContainer showInlineTable equalSplit">
        <thead><tr><th class="blockHeader" colspan="4">Sede Legale</th></tr></thead>
        <tbody>
        <tr>
            <td class="fieldLabel medium"><label class="muted pull-right marginRight10px">Provincia</label></td>
            <td class="fieldValue medium"><div class="row-fluid"><span class="span10">
          <input id="Accounts_editView_fieldName_bill_country" type="text" class="input-large" name="bill_country" value="BARI" ></span></div></td>
            <td class="fieldLabel medium"><label class="muted pull-right marginRight10px">Comune</label></td>
            <td class="fieldValue medium"><div class="row-fluid"><span class="span10"><input id="Accounts_editView_fieldName_bill_city" type="text" class="input-large " name="bill_city" value="BARI" >
  </span></div></td>
        </tr>
        <tr>
            <td class="fieldLabel medium"><label class="muted pull-right marginRight10px">Toponimo</label></td>
            <td class="fieldValue medium"><div class="row-fluid"><span class="span10"><input id="Accounts_editView_fieldName_cf_1387" type="text" class="input-large " name="cf_1387" value="PIAZZA" ></span></div></td><td class="fieldLabel medium"><label class="muted pull-right marginRight10px">Località</label></td>
            <td class="fieldValue medium"><div class="row-fluid"><span class="span10"><input id="Accounts_editView_fieldName_cf_2127" type="text" class="input-large " name="cf_2127" value="BARI" ></span></div></td>
        </tr>
        <tr>
            <td class="fieldLabel medium"><label class="muted pull-right marginRight10px">Indirizzo</label></td><td class="fieldValue medium"><div class="row-fluid"><span class="span10"><textarea class="row-fluid " name="bill_street">QUATTRO NOVEMBRE</textarea><div><a class="cursorPointer" name="copyAddress" data-target="shipping">Copiare lindirizzo di spedizione</a></div></span></div></td>
            <td class="fieldLabel medium"><label class="muted pull-right marginRight10px">N.Civico</label></td>
            <td class="fieldValue medium"><div class="row-fluid"><span class="span10"><input id="Accounts_editView_fieldName_cf_2129" type="text" class="input-large" name="cf_2129" value="2" ></span></div></td></tr><tr><td class="fieldLabel medium"><label class="muted pull-right marginRight10px">CAP</label></td>
            <td class="fieldValue medium"><div class="row-fluid"><span class="span10"><input id="Accounts_editView_fieldName_bill_code" type="text" class="input-large" name="bill_code" value="70122" ></span></div></td>
            <td class="fieldLabel medium"><label class="muted pull-right marginRight10px">Nazione</label></td>
            <td class="fieldValue medium"><div class="row-fluid"><span class="span10"><input id="Accounts_editView_fieldName_bill_state" type="text" class="input-large" name="bill_state" value="ITALIA"></span></div></td>
        </tr>
        </tbody>
    </table>

    <!-- FINE SEZIONE SEDE LEGALE -->

    <br/>

    <!-- INIZIO SEZIONE VIEW DATI -->
    <div class="contents-topscroll noprint">
        <div class="topscroll-div" style="width: 1308px;"> </div>
    </div>
    <div class="listViewEntriesDiv contents-bottomscroll">
        <div class="bottomscroll-div" style="width: 1308px;">
            <input id="orderBy" type="hidden" value="">
            <input id="sortOrder" type="hidden" value="">
<span id="loadingListViewModal" class="listViewLoadingImageBlock hide modal noprint">
<img class="listViewLoadingImage" title="Caricamento..." alt="no-image" src="./Organizzatori_files/loading.gif">
<p class="listViewLoadingMsg">Loading, Please wait.........</p>
</span>

            <table class="table table-bordered listViewEntriesTable">
                <thead>
                <tr class="listViewHeaders">
                    <th width="5%">
                        <input id="listViewEntriesMainCheckBox" type="checkbox">
                    </th>
                    <th nowrap="">
                        <a class="listViewHeaderValues" data-columnname="accountname" data-nextsortorderval="ASC" href="javascript:void(0);">Cognome/Ragione Sociale  </a>
                    </th>
                    <th nowrap="">
                        <a class="listViewHeaderValues" data-columnname="cf_2113" data-nextsortorderval="ASC" href="javascript:void(0);">Nome  </a>
                    </th>
                    <th nowrap="">
                        <a class="listViewHeaderValues" data-columnname="cf_1381" data-nextsortorderval="ASC" href="javascript:void(0);">Codice Fiscale  </a>
                    </th>
                    <th nowrap="" colspan="2">
                        <a class="listViewHeaderValues" data-columnname="cf_1383" data-nextsortorderval="ASC" href="javascript:void(0);">Partita IVA  </a>
                    </th>
                </tr>
                </thead>


                <tbody>

                <tr id="Accounts_listView_row_1" class="listViewEntries" data-id="2270872">
                    <td class="medium" width="5%">
                        <input class="listViewEntriesCheckBox" type="checkbox" value="2270872">
                    </td>
                    <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                        <a href="http://52.17.36.108/index.php?module=Accounts&view=Detail&record=2270872">ACCADEMIA DEL LUSSO SRL</a>
                    </td>
                    <td class="listViewEntryValue medium" nowrap="" data-field-type="string"></td>
                    <td class="listViewEntryValue medium" nowrap="" data-field-type="string">00723450482</td>
                    <td class="listViewEntryValue medium" nowrap="" data-field-type="string">03084790549</td>
                    <td class="medium" nowrap="">
                        <div class="actions pull-right">
   <span class="actionImages">
    <a href="http://52.17.36.108/index.php?module=Accounts&view=Detail&record=2270872&mode=showDetailViewByMode&requestMode=full">
       <i class="icon-th-list alignMiddle" title="Complete Details"></i>
    </a>
    <a href="http://52.17.36.108/index.php?module=Accounts&view=Edit&record=2270872">
       <i class="icon-pencil alignMiddle" title="Modifica"></i>
    </a>
    <a class="deleteRecordButton">
       <i class="icon-trash alignMiddle" title="Elimina"></i>
    </a>
   </span>
                        </div>
                    </td>
                </tr>

                <tr id="Accounts_listView_row_2" class="listViewEntries" data-recordurl="index.php?module=Accounts&view=Detail&record=2271687" data-id="2271687">
                    <td class="medium" width="5%">
                        <input class="listViewEntriesCheckBox" type="checkbox" value="2271687">
                    </td>
                    <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                        <a href="http://52.17.36.108/index.php?module=Accounts&view=Detail&record=2271687">CASA DELLA PACE ASSOCIAZIONE</a>
                    </td>
                    <td class="listViewEntryValue medium" nowrap="" data-field-type="string"></td>
                    <td class="listViewEntryValue medium" nowrap="" data-field-type="string">96401360589</td>
                    <td class="listViewEntryValue medium" nowrap="" data-field-type="string">08924121000</td>
                    <td class="medium" nowrap="">
                        <div class="actions pull-right">
                        <span class="actionImages">
                        <a href="http://52.17.36.108/index.php?module=Accounts&view=Detail&record=2271687&mode=showDetailViewByMode&requestMode=full">
                        <i class="icon-th-list alignMiddle" title="Complete Details"></i>
                        </a>
                        <a href="http://52.17.36.108/index.php?module=Accounts&view=Edit&record=2271687">
                        <i class="icon-pencil alignMiddle" title="Modifica"></i>
                        </a>
                        <a class="deleteRecordButton">
                        <i class="icon-trash alignMiddle" title="Elimina"></i>
                        </a>
                        </span>
                        </div>
                    </td>
                </tr>

                </tbody>
            </table>

            <!-- INIZIO SEZIONE VIEW DATI -->

            <!-- INIZIO TASTI FONDO PAGINA -->
            <br/><br/>
            <div class="row-fluid">
                <div class="pull-right">
                    <button class="btn btn-success" type="submit">
                        <strong>Salva</strong>
                    </button>
                    <a class="cancelLink" type="reset" onclick="javascript:window.history.back();">Annulla</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- FINE TASTI FONDO PAGINA -->

            </form>

    <!-- INIZIO FOOTER -->
    <jsp:include page="footer.jsp"/>
    <!-- FINE FOOTER -->

</body>
</html>