<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>

 <jsp:include page="header.jsp"/>

<body data-language="it_it">

<div id="js_strings" class="hide noprint"></div>
<input type="hidden" id="start_day" value="Sunday">
<input type="hidden" id="row_type" value="medium">
<input type="hidden" id="current_user_id" value="1">

<div id="page">

    <!-- container which holds data temporarly for pjax calls -->
    <div id="pjaxContainer" class="hide noprint"></div>

    <!-- INIZIO INTESTAZIONE-->
    <jsp:include page="menu_bar.jsp"/>
    <!-- FINE INTESTAZIONE-->

    <!-- INIZIO BARRA DI RICERCA -->
    <jsp:include page="search_bar.jsp"/>
    <!-- FINE BARRA DI RICERCA -->

    <!-- BODY -->
    <form class="form-horizontal recordEditView" id="RicalcoloFormId" name="RicalcoloForm" method="post" action="${contextPath}/secure/ricalcoloEsecuzioneSave">

        <script>
    // Gestione Progress Bar - Start

    $(function() {

        var progressbar = $( "#progressbar" ),
          progressLabel = $( ".progress-label" ),
          executeButton = $( "#executeButton" )
            .button()
            .on( "click", function() {

              $( this ).button( "option", {
                disabled: true,
                label: "Esecuzione in corso..."
              });

              var periodoContabileId = $("#periodoContabileId").val();

              if (periodoContabileId==null || periodoContabileId==""){

                alert("Attenzione !\nSpecificare il mese contabile da campionare.");

                $( "#executeButton" ).button( "option", {
                    disabled: false,
                    label: "Inizia campionamento"
                });

              } else {

                  var jqxhr = $.get( "${contextPath}/secure/sendCommand?info=START_CAMPIONAMENTO&periodoContabile="+periodoContabileId, function( value ) {

                       progressLabel.text( "Loading..." );

                       $("#elaborazioneValue #totValue").text("0");
                       $("#elaborazioneValue #elabValue").text("0");
                       $("#elaborazioneValue #sospValue").text("0");
                       $("#elaborazioneValue #errValue").text("0");

                       setTimeout( progress, 2000 );

                       $("#suspendDiv").show();
                       $("#resumeDiv").hide();

                  });

              }

            });

        progressbar.progressbar({
          value: false,
          change: function() {
            progressLabel.text( progressbar.progressbar( "value" ) + "%" );
          },
          complete: function() {
            progressLabel.text( "Complete!" );

            $( "#executeButton" ).button( "option", {
                disabled: false,
                label: "Inizia campionamento"
            });

          }
        });

        function progress() {

                var jqxhr = $.get( "${contextPath}/secure/getAvanzamentoAggregazione", function( value ) {

                      var val = progressbar.progressbar( "value" ) || 0;

                      $("#progressbar").progressbar( "value", value.percentuale);

                      $("#elaborazioneValue #totValue").text(value.totalePM);
                      $("#elaborazioneValue #elabValue").text(value.totaleElaborati);

                      if ( val < 100 ) {
                        setTimeout( progress, 2000 );
                      }else{
                            $( "#executeButton" ).button( "option", {
                                disabled: false,
                                label: "Inizia campionamento"
                            });
                      }
                });
        }

  });

  // Gestione Progress Bar - End

  </script>

    </form>


        <table class="table table-bordered blockContainer showInlineTable equalSplit">
            <thead>
            <tr>
                <th class="blockHeader" colspan="4">Controllo esecuzione</th>
            </tr>
            </thead>
            <tbody style="display: table-row-group;">

            <tr>
                <td class="fieldLabel medium">
                    <label class="muted pull-right marginRight10px">
                    </label>
                </td>
                <td class="fieldValue medium">
                    <div class="row-fluid">
                        <span class="span10">
                            <div class="input-append row-fluid">
                                <div class="span12 row-fluid date">
                                    <!-- Contenuto -->
                                    <div id="activeArrow" style="display:none" align="center">
                                       <img src="${contextPath}/images/green-arrow.png" height="30" width="30" alt="Processo attivo" title="Stato processo"/>
                                    </div>
                                    <div id="inactiveArrow" style="display:none" align="center">
                                        <img src="${contextPath}/images/red-arrow.png" height="30" width="30" alt="Processo attivo" title="Stato processo"/>
                                    </div>
                                </div>
                            </div>
                        </span>
                    </div>
                </td>

                <td class="fieldLabel medium">
                   <label class="muted pull-right marginRight10px">Stato processo</label>
                </td>
                <td class="fieldValue medium">
                    <div class="row-fluid">
                        <span class="span10">
                            <div class="input-append row-fluid">
                                <div class="span12 row-fluid date">
                                    <!-- Contenuto -->
                                    <div id="statoProcessoDiv" class="row-fluid">
                                       <span id="statoProcesso" class="span10 pull-left"></span>
                                    </div>
                                </div>
                            </div>
                        </span>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="fieldLabel medium">
                    <label class="muted pull-right marginRight10px">
                        <!-- Contenuto -->
                        <div style="visibility: hidden"><button id="executeButton">Inizia campionamento</button>  </div>
                        <!--
                        <div id="suspendDiv" style="display:none" align="center">
                            <button id="suspendButton" onclick="callSuspendeResume('SUSPENDE');">Sospendi</button>
                        </div>
                        <div id="resumeDiv" style="display:none" align="center">
                            <button id="resumeButton" onclick="callSuspendeResume('RESUME');">Riprendi</button>
                        </div>
                         -->
                    </label>
                </td>
                <td class="fieldValue medium">
                    <div class="row-fluid">
                        <span class="span10">
                            <div class="input-append row-fluid">

                                <div class="span12 row-fluid date">
                                    <!-- Contenuto -->
                                        <div id="progressbar"><div class="progress-label"></div></div>
                                </div>
                            </div>
                        </span>
                    </div>
                </td>
                <td class="fieldLabel medium">
                  <div id="elaborazioneLabel" style="display:none">
                    <label class="muted pull-right marginRight10px">Numero PM totali</label>
                    <label class="muted pull-right marginRight10px">Numero PM elaborati</label>
                  </div>
                </td>
                <td class="fieldValue medium">
                    <div id="elaborazioneValue" style="display:none" class="row-fluid">
                        <span id="totValue" class="span10 pull-left"></span>
                        <span id="elabValue" class="span10 pull-left"></span>
                    </div>
                </td>
            </tr>




</tbody>
            </table>


    <script>


    $(document).ready(function(){

       function progress() {

                var jqxhr = $.get( "${contextPath}/secure/getAvanzamentoAggregazione", function( value ) {

                      var val = progressbar.progressbar( "value" ) || 0;

                      $("#progressbar").progressbar( "value", value.percentuale);

                      $("#elaborazioneValue #totValue").text(value.totalePM);
                      $("#elaborazioneValue #elabValue").text(value.totaleElaborati);

                      if ( val < 100 ) {
                        setTimeout( progress, 80 );
                      }else{
                            $( "#executeButton" ).button( "option", {
                                disabled: false,
                                label: "Inizia campionamento"
                            });
                      }
                });
        }

        var request = $.ajax({
             url: "${contextPath}/secure/sendCommand?info=STATUS",
             type: "GET"
        });

        var progressbar = $( "#progressbar" ),
        progressLabel = $( ".progress-label" )

        request.done(function(value) {
          if (value == "ACTIVE"){

            $("#activeArrow").show();
            $("#inactiveArrow").hide();

            $("#activateDiv").hide();
            $("#inactiveDiv").show();

            $("#statoProcessoDiv #statoProcesso").text("Attivo");

            $("#executeButton").attr("disabled", false);

            $("#elaborazioneLabel").show();
            $("#elaborazioneValue").show();

            $("#elaborazioneValue #totValue").text("0");
            $("#elaborazioneValue #elabValue").text("0");

            var jqxhr = $.get( "${contextPath}/secure/getAvanzamentoAggregazione", function( value ) {

               setTimeout( progress, 2000 );

               progressLabel.text( "Loading..." );

               $("#elaborazioneValue #totValue").text(value.totalePM);
               $("#elaborazioneValue #elabValue").text(value.totaleElaborati);

               setTimeout( progress, 2000 );

               $("#suspendDiv").show();
               $("#resumeDiv").hide();

            });

          } else
          if (value == "INACTIVE"){
            $("#activeArrow").hide();
            $("#inactiveArrow").show();

            $("#activateDiv").show();
            $("#inactiveDiv").hide();

            $("#periodoContabileId").attr("disabled", true);

            $("#statoProcessoDiv #statoProcesso").text("Inattivo");

            $("#executeButton").button("option", {
             disabled: true
            });

            $("#elaborazioneLabel").hide();
            $("#elaborazioneValue").hide();

          }else{
              alert(value);
          }

        });

        request.fail(function(jqXHR, textStatus) {
           alert( "Request failed: " + textStatus );
        });

    });

    </script>

        <!-- INIZIO TASTI FONDO PAGINA -->
        <br/><br/>

    <!-- INIZIO FOOTER -->
    <jsp:include page="footer.jsp"/>
    <!-- FINE FOOTER -->

</body>
</html>