<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>

 <jsp:include page="header.jsp"/>

<body data-language="it_it">

<div id="js_strings" class="hide noprint"></div>
<input type="hidden" id="start_day" value="Sunday">
<input type="hidden" id="row_type" value="medium">
<input type="hidden" id="current_user_id" value="1">

<div id="page">

    <!-- container which holds data temporarly for pjax calls -->
    <div id="pjaxContainer" class="hide noprint"></div>

    <!-- INIZIO INTESTAZIONE-->
    <jsp:include page="menu_bar.jsp"/>
    <!-- FINE INTESTAZIONE-->

    <!-- INIZIO BARRA DI RICERCA -->
    <jsp:include page="search_bar.jsp"/>
    <!-- FINE BARRA DI RICERCA -->

    <!-- BODY -->
    <script>
    // Gestione Progress Bar - Start

    $(function() {

        var progressbar = $( "#progressbar" ),
          progressLabel = $( ".progress-label" ),
          executeButton = $( "#executeButton" )
            .button()
            .on( "click", function() {

              $( this ).button( "option", {
                disabled: true,
                label: "Esecuzione in corso..."
              });

              var periodoContabileId = $("#periodoContabileId").val();

              if (periodoContabileId==null || periodoContabileId==""){

                alert("Attenzione !\nSpecificare il mese contabile da campionare.");

                $( "#executeButton" ).button( "option", {
                    disabled: false,
                    label: "Inizia campionamento"
                });

              } else {

                  var jqxhr = $.get( "${contextPath}/secure/sendCommand?info=START_CAMPIONAMENTO&periodoContabile="+periodoContabileId, function( value ) {

                       progressLabel.text( "Loading..." );

                       $("#elaborazioneValue #totValue").text("0");
                       $("#elaborazioneValue #elabValue").text("0");
                       $("#elaborazioneValue #sospValue").text("0");
                       $("#elaborazioneValue #errValue").text("0");

                       setTimeout( progress, 2000 );

                       $("#suspendDiv").show();
                       $("#resumeDiv").hide();

                  });

              }

            });

        progressbar.progressbar({
          value: false,
          change: function() {
            progressLabel.text( progressbar.progressbar( "value" ) + "%" );
          },
          complete: function() {
            progressLabel.text( "Complete!" );

            $( "#executeButton" ).button( "option", {
                disabled: false,
                label: "Inizia campionamento"
            });

          }
        });

        function progress() {

                var jqxhr = $.get( "${contextPath}/secure/getAvanzamentoElaborazioneCampionamento", function( value ) {

                      var val = progressbar.progressbar( "value" ) || 0;

                      $("#progressbar").progressbar( "value", value.percentuale);

                      $("#elaborazioneValue #totValue").text(value.totalePM);
                      $("#elaborazioneValue #elabValue").text(value.totaleElaborati);
                      $("#elaborazioneValue #sospValue").text(value.totaleSospesi);
                      $("#elaborazioneValue #errValue").text(value.totaleErrori);

                      if ( val < 100 ) {
                        setTimeout( progress, 2000 );
                      }else{
                            $( "#executeButton" ).button( "option", {
                                disabled: false,
                                label: "Inizia campionamento"
                            });
                      }
                });
        }

  });

  // Gestione Progress Bar - End


  function callActivateDeactivate(status){

        var request = $.ajax({
             url: "${contextPath}/secure/sendCommand?info="+status,
             type: "GET"
        });

        request.done(function(value) {

          if (value == "ACTIVE"){

            $("#activeArrow").show();
            $("#inactiveArrow").hide();

            $("#activateDiv").hide();
            $("#inactiveDiv").show();

            $("#periodoContabileId").attr("disabled", false);

            $("#statoProcessoDiv #statoProcesso").text("Attivo");

            $("#executeButton").button("option", {
             disabled: false
            });

            $("#elaborazioneLabel").show();
            $("#elaborazioneValue").show();

             var jqxhr = $.get( "${contextPath}/secure/getAvanzamentoElaborazione", function( value ) {

               $("#elaborazioneValue #totValue").text(value.totalePM);
               $("#elaborazioneValue #elabValue").text(value.totaleElaborati);
               $("#elaborazioneValue #sospValue").text(value.totaleSospesi);
               $("#elaborazioneValue #errValue").text(value.totaleErrori);

            });


          } else
          if (value == "INACTIVE"){
            $("#activeArrow").hide();
            $("#inactiveArrow").show();

            $("#activateDiv").show();
            $("#inactiveDiv").hide();

            $("#periodoContabileId").attr("disabled", true);

            $("#statoProcessoDiv #statoProcesso").text("Inattivo");

            $("#executeButton").button("option", {
             disabled: true
            });

            $("#elaborazioneLabel").hide();
            $("#elaborazioneValue").hide();

          }else{
              alert(value);
          }

        });

        request.fail(function(jqXHR, textStatus) {
           alert( "Request failed: " + textStatus );
        });
   }

  function callSuspendeResume(status){

        //suspendDiv      resumeDiv
        //suspendButton   resumeButton
        //SUSPENDE        RESUME

        var request = $.ajax({
             url: "${contextPath}/secure/sendCommand?info="+status,
             type: "GET"
        });

        request.done(function(value) {

          if (value == "SUSPENDED"){

            $("#suspendDiv").hide();
            $("#resumeDiv").show();

          } else
          if (value == "RESUMED"){

            $("#suspendDiv").show();
             $("#resumeDiv").hide();

          }else{
              alert(value);
          }

        });

        request.fail(function(jqXHR, textStatus) {
           alert( "Request failed: " + textStatus );
        });
   }



  </script>

    <form class="form-horizontal recordEditView" id="RicalcoloFormId" name="RicalcoloForm" method="post" action="${contextPath}/secure/ricalcoloEsecuzioneSave">

    </form>


        <table class="table table-bordered blockContainer showInlineTable equalSplit">
            <thead>
            <tr>
                <th class="blockHeader" colspan="4">Controllo esecuzione</th>
            </tr>
            </thead>
            <tbody style="display: table-row-group;">

            <tr>
                <td class="fieldLabel medium">
                    <label class="muted pull-right marginRight10px">
                        <!-- Contenuto -->
                        <div id="activateDiv" style="display:none" align="center">
                            <button id="startButton" onclick="callActivateDeactivate('ACTIVATE');">Start</button>
                        </div>
                        <div id="inactiveDiv" style="display:none" align="center">
                            <button id="stopButton" onclick="callActivateDeactivate('DEACTIVATE');">Stop</button>
                        </div>
                    </label>
                </td>
                <td class="fieldValue medium">
                    <div class="row-fluid">
                        <span class="span10">
                            <div class="input-append row-fluid">
                                <div class="span12 row-fluid date">
                                    <!-- Contenuto -->
                                    <div id="activeArrow" style="display:none" align="center">
                                       <img src="${contextPath}/images/green-arrow.png" height="30" width="30" alt="Processo attivo" title="Stato processo"/>
                                    </div>
                                    <div id="inactiveArrow" style="display:none" align="center">
                                        <img src="${contextPath}/images/red-arrow.png" height="30" width="30" alt="Processo attivo" title="Stato processo"/>
                                    </div>
                                </div>
                            </div>
                        </span>
                    </div>
                </td>

                <td class="fieldLabel medium">
                   <label class="muted pull-right marginRight10px">Stato processo</label>
                </td>
                <td class="fieldValue medium">
                    <div class="row-fluid">
                        <span class="span10">
                            <div class="input-append row-fluid">
                                <div class="span12 row-fluid date">
                                    <!-- Contenuto -->
                                    <div id="statoProcessoDiv" class="row-fluid">
                                       <span id="statoProcesso" class="span10 pull-left"></span>
                                    </div>
                                </div>
                            </div>
                        </span>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="fieldLabel medium">
                    <label class="muted pull-right marginRight10px">
                        <!-- Contenuto -->
                        <button id="executeButton">Inizia campionamento</button>
                        <!--
                        <div id="suspendDiv" style="display:none" align="center">
                            <button id="suspendButton" onclick="callSuspendeResume('SUSPENDE');">Sospendi</button>
                        </div>
                        <div id="resumeDiv" style="display:none" align="center">
                            <button id="resumeButton" onclick="callSuspendeResume('RESUME');">Riprendi</button>
                        </div>
                         -->
                    </label>
                </td>
                <td class="fieldValue medium">
                    <div class="row-fluid">
                        <span class="span10">
                            <div class="input-append row-fluid">

                            <select class="js-basic-single-select" name="periodoContabile" id="periodoContabileId" style="width: 220px; display: block;" required="true">
                                <option value="" class="globalSearch_module_All">- Mese contabile-</option>
                                <c:forEach items="${resultPage.records}" var="c">
                                    <option value="${c.id}" class="globalSearch_module_All">
                                            ${c.descrizioneContabilita}
                                    </option>
                                </c:forEach>
                            </select>

                                <div class="span12 row-fluid date">
                                    <!-- Contenuto -->
                                        <div id="progressbar"><div class="progress-label"></div></div>
                                </div>
                            </div>
                        </span>
                    </div>
                </td>
                <td class="fieldLabel medium">
                  <div id="elaborazioneLabel" style="display:none">
                    <label class="muted pull-right marginRight10px">Numero PM totali</label><br/>
                    <label class="muted pull-right marginRight10px">Numero PM elaborati</label><br/>
                  </div>
                </td>
                <td class="fieldValue medium">
                    <div id="elaborazioneValue" style="display:none" class="row-fluid">
                        <span id="totValue" class="span10 pull-left"></span><br/>
                        <span id="elabValue" class="span10 pull-left"></span><br/>
                    </div>
                </td>
            </tr>




</tbody>
            </table>


    <script>

    $(document).ready(function(){

        var request = $.ajax({
             url: "${contextPath}/secure/sendCommand?info=STATUS",
             type: "GET"
        });

        request.done(function(value) {
          if (value == "ACTIVE"){

            $("#activeArrow").show();
            $("#inactiveArrow").hide();

            $("#activateDiv").hide();
            $("#inactiveDiv").show();

            $("#statoProcessoDiv #statoProcesso").text("Attivo");

            $("#executeButton").attr("disabled", false);
            $("#periodoContabileId").attr("disabled", false);

            $("#executeButton").button("option", {
             disabled: false
            });

            $("#elaborazioneLabel").show();
            $("#elaborazioneValue").show();

            var jqxhr = $.get( "${contextPath}/secure/getAvanzamentoElaborazioneCampionamento", function( value ) {

               $("#elaborazioneValue #totValue").text(value.totalePM);
               $("#elaborazioneValue #elabValue").text(value.totaleElaborati);
               $("#elaborazioneValue #sospValue").text(value.totaleSospesi);
               $("#elaborazioneValue #errValue").text(value.totaleErrori);

                //if (value.totalePM>0 && value.totaleElaborati>0 && value.){
                //    $('#executeButton').click();
                //}


            });

          } else
          if (value == "INACTIVE"){
            $("#activeArrow").hide();
            $("#inactiveArrow").show();

            $("#activateDiv").show();
            $("#inactiveDiv").hide();

            $("#periodoContabileId").attr("disabled", true);

            $("#statoProcessoDiv #statoProcesso").text("Inattivo");

            $("#executeButton").button("option", {
             disabled: true
            });

            $("#elaborazioneLabel").hide();
            $("#elaborazioneValue").hide();

          }else{
              alert(value);
          }

        });

        request.fail(function(jqXHR, textStatus) {
           alert( "Request failed: " + textStatus );
        });


    });

    </script>

        <!-- INIZIO TASTI FONDO PAGINA -->
        <br/><br/>
        <!--
        <div class="row-fluid">
            <div class="pull-right">
                <button class="btn btn-success" type="submit">
                    <strong>Salva</strong>
                </button>
                <a class="cancelLink" type="reset" onclick="javascript:window.history.back();">Annulla</a>
            </div>
            <div class="clearfix"></div>
        </div>
        -->
        <!-- FINE TASTI FONDO PAGINA -->

    <!--/form-->

    <!-- INIZIO FOOTER -->
    <jsp:include page="footer.jsp"/>
    <!-- FINE FOOTER -->

</body>
</html>