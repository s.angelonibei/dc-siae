<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<div class="navbar navbar-fixed-top  navbar-inverse noprint">
<div class="navbar" id="topMenus" style="overflow: visible; height: 40px;"><div class="navbar-inner" id="nav-inner">
<div class="menuBar row-fluid"><div class="span9">

  <ul class="nav modulesList" id="largeNav">
    <li class="tabs">
     <a class="alignMiddle " href="${contextPath}/secure/home">
       <img src="${contextPath}/images/home.png" alt="Home" title="Home">
     </a>
    </li>
 </ul>

<ul class="nav" id="commonMoreMenu">

  <li class="dropdown" id="campionamentoMenu">
    <a class="dropdown-toggle" data-toggle="dropdown" href="">
        <c:if test="${user.hasProfile('P_CAMPIONAMENTO')}">
            <strong>Campionamento&nbsp;</strong><b class="caret"></b>
        </c:if>
    </a>
    <div class="dropdown-menu ">
     <div class="row-fluid">
         <c:if test="${user.hasProfile('P_CAMPIONAMENTO_CONFIGURAZIONE')}">
            <label class="moduleNames">
                <a id="menubar_item_Configurazione" style="color: #4e525f" href="${contextPath}/secure/campionamentoConfigurazione">Configurazione</a>
            </label>
         </c:if>
         <c:if test="${user.hasProfile('P_CAMPIONAMENTO_STORICO')}">
            <label class="moduleNames">
                <a id="menubar_item_Storico" style="color: #4e525f" href="${contextPath}/secure/campionamentoStorico" >Storico Configurazione</a>
            </label>
         </c:if>
         <c:if test="${user.hasProfile('P_CAMPIONAMENTO_STORICO')}">
             <label class="moduleNames">
                 <a id="menubar_item_Storico" style="color: #4e525f" href="${contextPath}/secure/campionamentoStoricoEsecuzioni" >Storico Esecuzioni</a>
             </label>
         </c:if>
         <c:if test="${user.hasProfile('P_CAMPIONAMENTO_ESECUZIONE')}">
            <label class="moduleNames">
                <a id="menubar_item_Esecuzione" style="color: #4e525f" href="${contextPath}/secure/campionamentoEsecuzione">Esecuzione campionamento</a>
            </label>
         </c:if>
         <c:if test="${user.hasProfile('P_CAMPIONAMENTO_EXPORT_FLUSSO')}">
            <label class="moduleNames">
                <a id="menubar_item_Flusso" style="color: #4e525f" href="${contextPath}/secure/campionamentoFlussoGuida">Esportazione flusso guida PM</a>
            </label>
         </c:if>
         <c:if test="${user.hasProfile('P_CAMPIONAMENTO_PM_RIENTRATI')}">
            <label class="moduleNames">
                <a id="menubar_item_Verifiche" style="color: #4e525f" href="${contextPath}/secure/campionamentoVerificaRientrati">Caricamento PM rientrati </a>
            </label>
         </c:if>
     </div>
    </div>
  </li>

    <li class="dropdown" id="ricalcoloMenu">
        <a class="dropdown-toggle" data-toggle="dropdown" href="">
            <c:if test="${user.hasProfile('P_RICALCOLO')}">
                <strong>Ricalcolo&nbsp;</strong><b class="caret"></b>
            </c:if>
        </a>
        <div class="dropdown-menu ">
            <div class="row-fluid">
                <c:if test="${user.hasProfile('P_RICALCOLO_CONFIGURAZIONE')}">
                 <label class="moduleNames">
                    <a id="menubar_item_Ricalcolo_Configurazione" style="color: #4e525f" href="${contextPath}/secure/ricalcoloConfigurazione">Configurazione</a>
                 </label>
                </c:if>
                <c:if test="${user.hasProfile('P_RICALCOLO_ESECUZIONE')}">
                 <label class="moduleNames">
                    <a id="menubar_item_Ricalcolo_Esecuzione" style="color: #4e525f" href="${contextPath}/secure/ricalcoloEsecuzione" >Esecuzione</a>
                 </label>
                </c:if>
                <c:if test="${user.hasProfile('P_RICALCOLO_VERIFICA')}">
                 <label class="moduleNames">
                    <a id="menubar_item_VerificaImporti" style="color: #4e525f" href="${contextPath}/secure/ricalcoloControllo">Verifica importi ricalcolati</a>
                 </label>
                </c:if>
                <c:if test="${user.hasProfile('P_MODIFICA_CONTABILITA')}">
                    <label class="moduleNames">
                        <a id="menubar_item_ModificaContabilita" style="color: #4e525f" href="${contextPath}/secure/modificaContabilita">Modifica Contabilita'</a>
                    </label>
                </c:if>
            </div>
        </div>
    </li>

    <li class="dropdown" id="cruscottiMenu">
        <a class="dropdown-toggle" data-toggle="dropdown" href="">
            <c:if test="${user.hasProfile('P_CRUSCOTTI')}">
            <strong>Cruscotti&nbsp;</strong><b class="caret"></b>
            </c:if>
        </a>
        <div class="dropdown-menu ">
            <div class="row-fluid">
                <c:if test="${user.hasProfile('P_CRUSCOTTI_TRACCIAMENTO')}">
                  <label class="moduleNames">
                    <a id="menubar_item_Tracciamento" style="color: #4e525f" href="${contextPath}/secure/tracciamentoPM">Tracciamento PM </a>
                  </label>
                </c:if>
                <c:if test="${user.hasProfile('P_CRUSCOTTI_EVENTI')}">
                 <label class="moduleNames">
                    <a id="menubar_item_Eventi" style="color: #4e525f" href="${contextPath}/secure/eventiPagati">Visualizzazione Eventi</a>
                 </label>
                </c:if>
                <c:if test="${user.hasProfile('P_CRUSCOTTI_MOVIMENTI')}">
                  <label class="moduleNames">
                    <a id="menubar_item_Movimenti" style="color: #4e525f" href="${contextPath}/secure/movimenti" >Visualizzazione Movimenti</a>
                 </label>
                </c:if>
                <c:if test="${user.hasProfile('P_CRUSCOTTI_AGGREGATO_INCASSI_EVENTO')}">
                  <label class="moduleNames">
                    <a id="menubar_item_Aggregato_Incassi" style="color: #4e525f" href="${contextPath}/secure/aggregazioniIncassi">Aggregato incassi eventi</a>
                  </label>
                </c:if>
                <c:if test="${user.hasProfile('P_CRUSCOTTI_AGGREGATO_RIEPILOGO_MOVIMENTO')}">
                  <label class="moduleNames">
                    <a id="menubar_item_Aggregato_Movimenti" style="color: #4e525f" href="${contextPath}/secure/aggregazioniMovimenti">Aggregato movimenti</a>
                  </label>
                </c:if>
                <c:if test="${user.hasProfile('P_CRUSCOTTI_AGGREGATI_SIADA') and user.hasProfile('P_RICALCOLO_ALL')}">
                    <label class="moduleNames">
                        <a id="menubar_item_Verifica" style="color: #4e525f" href="${contextPath}/secure/aggregazioniSiada">Verifica dati da inviare a SIADA</a>
                    </label>
                </c:if>
                <c:if test="${user.hasProfile('P_CRUSCOTTI_AGGREGATI_SIADA') and user.hasProfile('P_RICALCOLO_SMC')}">
                    <label class="moduleNames">
                        <a id="menubar_item_Verifica_SMC" style="color: #4e525f" href="${contextPath}/secure/aggregazioniSiadaSMC">Verifica dati da inviare a SIADA (SMC)</a>
                    </label>
                </c:if>
                <!--
                <label class="moduleNames">
                    <a id="menubar_item_PM" style="color: #4e525f" href="">Dettaglio Programma Musicale</a>
                </label>-->
            </div>
        </div>
    </li>

</ul>

</div>

<div class="span3 marginLeftZero pull-right" id="headerLinks">
<span id="headerLinksBig" class="pull-right headerLinksContainer">

    <span class="dropdown span">
  <span class="dropdown-toggle" data-toggle="dropdown" href="#">
    <a id="menubar_item_right_Administrator" class="userName textOverflowEllipsis" title="Administrator">
     <strong>${userAuthenticated.principal.username}</strong>&nbsp;<i class="caret"></i>
   </a> 
  </span>
  <ul class="dropdown-menu pull-right">
       <li><a target="" id="menubar_item_Dati" href="${contextPath}/secure/userData">I miei dati</a></li>
       <li class="divider">&nbsp;</li>
      <li><a id="menubar_item_SIGN_OUT" href='javascript:document.getElementById("logoutForm").submit();'>Esci</a></li>
      <form id="logoutForm" action="<%=request.getContextPath()%>/logout" method="post">
      </form>
  </ul>
 </span>
</span>


</div>
</div> 
<div class="clearfix"></div>
</div>
</div>

<div class="announcement noprint" id="announcement">
 <marquee direction="left" scrolldelay="10" scrollamount="3" behavior="scroll" class="marStyle" onmouseover="this.setAttribute(&#39;scrollamount&#39;, 0, 0);" onmouseout="this.setAttribute(&#39;scrollamount&#39;, 6, 0);">LBL_NO_ANNOUNCEMENTS</marquee>
</div>
