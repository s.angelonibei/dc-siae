<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<div class="navbar commonActionsContainer noprint">
 <div class="actionsContainer row-fluid">
   <div class="span2">
    <span class="companyLogo">
      <img src="${contextPath}/images/siae_logo_siaetrasparente.png" title="siae_logo_siaetrasparente.png" alt="siae_logo_siaetrasparente.png">&nbsp;</span>
   </div>
     <div class="span10">
         <div class="row-fluid">

             </div>
         </div>
</div>
</div>
</div>


<div class="bodyContents" style="min-height: 488px;">

    <!-- Sezione relativa alle breadCrumbs in pagina -->
    <c:if test="${breadCrumbList.breadCrumbs!=null and breadCrumbList.breadCrumbs.size()>0}">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cruscotto:
        <c:forEach var="entry" items="${breadCrumbList.breadCrumbs}">
        <a href="${entry.url}">${entry.label}</a> >
        </c:forEach>
    </c:if>
    <!-- Sezione relativa alle breadCrumbs in pagina -->

    <div class="mainContainer row-fluid" style="min-height: 1355px;">
        <div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 1355px;">
            <div class="container-fluid editViewContainer">




