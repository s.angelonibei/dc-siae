<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>

 <jsp:include page="header.jsp"/>

<body data-language="it_it">

<div id="page">

    <!-- container which holds data temporarly for pjax calls -->
    <div id="pjaxContainer" class="hide noprint"></div>

    <!-- INIZIO INTESTAZIONE-->
    <jsp:include page="menu_bar.jsp"/>
    <!-- FINE INTESTAZIONE-->

    <!-- INIZIO BARRA DI RICERCA -->
    <jsp:include page="search_bar.jsp"/>
    <!-- FINE BARRA DI RICERCA -->

    <script type="text/javascript">
       function toggle_visibility(id) {
           var e = document.getElementById(id);
           if(e.style.display == 'block')
              e.style.display = 'none';
           else
              e.style.display = 'block';
       }

       function openPMDetail(idProgrammaMusicale, idMovimento){
           document.forms.EventoDettaglioFormId.elements.idProgrammaMusicale.value=idProgrammaMusicale;
           document.forms.EventoDettaglioFormId.elements.idMovimento.value=idMovimento;
           document.forms.EventoDettaglioFormId.action='${contextPath}/secure/pmDetail';
           document.forms.EventoDettaglioFormId.submit();
        }

    </script>

    <!-- BODY -->
    <form class="form-horizontal recordEditView" id="EventoDettaglioFormId" name="EventoDettaglioForm" method="get" action="">

        <c:if test="${evento.size() > 0}">

        <!-- INIZIO SEZIONE DATI -->
        <table class="table table-bordered blockContainer showInlineTable equalSplit">
            <thead>
            <tr>
                <th class="blockHeader" colspan="4">Informazioni Evento</th>
            </tr>
            </thead>
            <tbody style="display: table-row-group;">


                <tr>
                    <!-- blocco 1 -->
                    <td class="fieldLabel medium">
                        <label class="muted pull-right marginRight10px">
                            <label class="muted pull-right marginRight10px">ID Evento</label>
                        </label>
                    </td>
                    <td class="fieldValue medium">
                        <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                          ${evento.get(0).idEvento}
                        </div></div></span></div>
                    </td>
                    <!-- blocco 1 -->
                    <!-- blocco 2 -->
                    <td class="fieldLabel medium">
                        <label class="muted pull-right marginRight10px">
                            Seprag punto territoriale
                        </label>
                    </td>
                    <td class="fieldValue medium">
                        <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                                ${evento.get(0).seprag}
                        </div></div></span></div>
                    </td>
                    <!-- blocco 2 -->
                </tr>
                <tr>
                    <!-- blocco 1 -->
                    <td class="fieldLabel medium">
                        <label class="muted pull-right marginRight10px">
                            <label class="muted pull-right marginRight10px">Locale</label>
                        </label>
                    </td>
                    <td class="fieldValue medium">
                        <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                                ${evento.get(0).denominazioneLocale}
                        </div></div></span></div>
                    </td>
                    <!-- blocco 1 -->
                    <!-- blocco 2 -->
                    <td class="fieldLabel medium">
                        <label class="muted pull-right marginRight10px">
                            Codice BA
                        </label>
                    </td>
                    <td class="fieldValue medium">
                        <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                                ${evento.get(0).codiceBaLocale}
                        </div></div></span></div>
                    </td>
                    <!-- blocco 2 -->
                </tr>

                <tr>
                    <!-- blocco 1 -->
                    <td class="fieldLabel medium">
                        <label class="muted pull-right marginRight10px">
                            <label class="muted pull-right marginRight10px">Codice SPEI</label>
                        </label>
                    </td>
                    <td class="fieldValue medium">
                        <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                               ${evento.get(0).codiceSpeiLocale}
                        </div></div></span></div>
                    </td>
                    <!-- blocco 1 -->
                    <!-- blocco 2 -->
                    <td class="fieldLabel medium">
                        <label class="muted pull-right marginRight10px">
                            <label class="muted pull-right marginRight10px">Inizio evento</label>
                        </label>
                    </td>
                    <td class="fieldValue medium">
                        <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                                ${evento.get(0).dataInizioEventoStr} ${evento.get(0).oraInizioEvento}
                        </div></div></span></div>
                    </td>
                    <!-- blocco 2 -->
                </tr>
                    <c:if test="${evento.get(0).manifestazione !=null}">
                        <tr>
                            <!-- blocco 1 -->
                            <td class="fieldLabel medium">
                                <label class="muted pull-right marginRight10px">
                                    <label class="muted pull-right marginRight10px">Fine evento</label>
                                </label>
                            </td>
                            <td class="fieldValue medium">
                                <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                                        ${evento.get(0).manifestazione.dataFineEventoStr}  ${evento.get(0).manifestazione.oraFineEvento}
                                </div></div></span></div>
                            </td>
                            <!-- blocco 1 -->
                            <!-- blocco 2 -->
                            <td class="fieldLabel medium">
                                <label class="muted pull-right marginRight10px">
                                    Localit&aacute;
                                </label>
                            </td>
                            <td class="fieldValue medium">
                                <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                                        ${evento.get(0).manifestazione.denominazioneLocalita}
                                </div></div></span></div>
                            </td>
                            <!-- blocco 2 -->
                        </tr>
                        <tr>
                            <!-- blocco 1 -->
                            <td class="fieldLabel medium">
                                <label class="muted pull-right marginRight10px">
                                    <label class="muted pull-right marginRight10px">Provincia</label>
                                </label>
                            </td>
                            <td class="fieldValue medium">
                                <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">
                                        ${evento.get(0).manifestazione.siglaProvinciaLocale}
                                </div></div></span></div>
                            </td>
                            <!-- blocco 1 -->
                            <!-- blocco 2 -->
                            <td class="fieldLabel medium">
                                <label class="muted pull-right marginRight10px">

                                </label>
                            </td>
                            <td class="fieldValue medium">
                                <div class="row-fluid"><span class="span10"><div class="input-append row-fluid"><div class="span12 row-fluid date">

                                </div></div></span></div>
                            </td>
                            <!-- blocco 2 -->
                        </tr>
                    </c:if>

            </tbody>
          </table>

         <br/>

        <table class="table table-bordered blockContainer showInlineTable equalSplit">
            <thead>
            <tr>
                <th class="blockHeader" colspan="4">Informazioni Contabili</th>
            </tr>
            </thead>
        </table>

        <table class="table table-bordered listViewEntriesTable">
            <thead>
            <tr class="listViewHeaders">
                <th nowrap="" valign="center" align="center">
                    <a class="listViewHeaderValuesSoft">Periodo pagamento</a>
                </th>
                <th nowrap="" valign="center" align="center">
                    <a class="listViewHeaderValuesSoft">Voce incasso</a>
                </th>
                <th nowrap="" valign="center" align="center">
                    <a class="listViewHeaderValuesSoft">Numero fattura</a>
                </th>
                <th nowrap="" valign="center" align="center">
                    <a class="listViewHeaderValuesSoft">Importo DEM</a>
                </th>
                <th nowrap="" valign="center" align="center">
                    <a class="listViewHeaderValuesSoft">Tipologia documento</a>
                </th>
            </tr>
            </thead>

            <tbody>

            <c:forEach items="${evento}" var="e">
                <tr id="row_${m.id}" class="listViewEntries">
                    <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                           <c:if test="${e.contabilita != null}">
                               ${e.contabilita}
                           </c:if>
                           <c:if test="${e.contabilita == null}">
                               Nessun evento pagato trovato per Evento: ${e.idEvento} Voce Incasso: ${e.voceIncasso} Fattura: ${e.numeroFattura}
                           </c:if>
                    </td>
                    <td class="listViewEntryValue medium" nowrap="" data-field-type="string"><c:if test="${e.contabilita != null}"> ${e.voceIncasso} </c:if></td>
                    <td class="listViewEntryValue medium" nowrap="" data-field-type="string"><c:if test="${e.contabilita != null}"> ${e.numeroFattura}</c:if></td>
                    <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${e.importoDemFormatted}</td>
                    <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                        <c:if test="${e.tipoDocumentoContabile =='501'}">
                            Mod. 501 (Entrata)
                        </c:if>
                        <c:if test="${e.tipoDocumentoContabile =='221'}">
                            Mod. 221 (Uscita)
                        </c:if>
                    </td>
                </tr>
            </c:forEach>

            </tbody>
        </table>

        <br/>

        <table class="table table-bordered blockContainer showInlineTable equalSplit">
            <thead>
            <tr>
                <th class="blockHeader" colspan="4">Programmi Musicali attesi/rientrati</th>
            </tr>
            </thead>
        </table>

        <table class="table table-bordered listViewEntriesTable">
            <thead>
            <tr class="listViewHeaders">
                <th nowrap="" valign="center" align="center">
                    <a class="listViewHeaderValuesSoft">Voce incasso</a>
                </th>
                <th nowrap="" valign="center" align="center">
                    <a class="listViewHeaderValuesSoft">PM Attesi</a>
                </th>
                <th nowrap="" valign="center" align="center">
                    <a class="listViewHeaderValuesSoft">PM Rientrati</a>
                </th>
                <th nowrap="" valign="center" align="center">
                    <a class="listViewHeaderValuesSoft">Importo DEM<br/>Residuo</a>
                </th>
                <th nowrap="" valign="center" align="center">
                    <a class="listViewHeaderValuesSoft">Importo assegnato</a>
                </th>
            </tr>
            </thead>

            <tbody>
            <c:forEach items="${evento.get(0).dettaglioPM}" var="s">
                <tr class="listViewEntries">
                    <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${s.voceIncasso}</td>
                    <td class="listViewEntryValue medium" nowrap="" data-field-type="string">

                        <c:if test="${s.voceIncasso =='2244' or s.voceIncasso =='2243'}">
                            ${s.pmPrevisti} (Principale)
                        </c:if>
                        <c:if test="${s.voceIncasso =='2244' or s.voceIncasso =='2243'}">
                            ${s.pmPrevistiSpalla} (Spalla)
                        </c:if>
                        <c:if test="${s.voceIncasso !='2244' and s.voceIncasso !='2243'}">
                            ${s.pmPrevisti}
                        </c:if>
                    </td>
                    <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                        <c:if test="${s.voceIncasso =='2244' or s.voceIncasso =='2243'}">
                            ${s.pmRientrati} (Principale)
                        </c:if>
                        <c:if test="${s.voceIncasso =='2244' or s.voceIncasso =='2243'}">
                            ${s.pmRientratiSpalla} (Spalla)
                        </c:if>
                        <c:if test="${s.voceIncasso !='2244' and s.voceIncasso !='2243'}">
                            ${s.pmRientrati}
                        </c:if>
                    </td>
                    <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                            ${s.importoDisponibileFormatted}
                    </td>

                    <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                            <a href="javascript:toggle_visibility('importiDiv_${s.voceIncasso}')"><u>${s.importoAssegnatoFormatted}</u></a>
                                <div id="importiDiv_${s.voceIncasso}" style="display:none; background:#F2F2F2; border-style: groove; width: 290px;">
                                    <c:forEach items="${s.importiRicalcolati}" var="i">
                                        &#8226; Elabor. ${i.idEsecuzioneRicalcolo} del ${i.dataOraInserimentoStr}: ${i.importoFormatted}<br/>
                                    </c:forEach>
                                </div>
                    </td>

                </tr>
            </c:forEach>
            </tbody>
         </table>

            <br/>

            <table class="table table-bordered blockContainer showInlineTable equalSplit">
                <thead>
                <tr>
                    <th class="blockHeader" colspan="4">Dettaglio movimentazioni Programmi Musicali</th>
                </tr>
                </thead>
            </table>
            <table class="table table-bordered listViewEntriesTable">
                <thead>
                <tr class="listViewHeaders">
                    <th nowrap="" valign="center" align="center">
                        <a class="listViewHeaderValuesSoft">Mese Rendicontazione</a>
                    </th>
                    <th nowrap="" valign="center" align="center">
                        <a class="listViewHeaderValuesSoft">Voce incasso</a>
                    </th>
                    <th nowrap="" valign="center" align="center">
                        <a class="listViewHeaderValuesSoft">Numero fattura</a>
                    </th>
                    <th nowrap="" valign="center" align="center">
                        <a class="listViewHeaderValuesSoft">Numero</a>
                    </th>
                    <th nowrap="" valign="center" align="center">
                        <a class="listViewHeaderValuesSoft">Permesso</a>
                    </th>
                    <th nowrap="" valign="center" align="center">
                        <a class="listViewHeaderValuesSoft">Importo SUN</a>
                    </th>
                    <th nowrap="" valign="center" align="center">
                        <a class="listViewHeaderValuesSoft">Importo </a>
                    </th>
                </tr>
                </thead>

                <tbody>

                    <c:forEach items="${evento.get(0).movimentazioniPM}" var="m">
                        <tr class="listViewEntries">
                            <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                                <c:choose>
                                  <c:when test="${m.id != movimento}">
                                     ${m.contabilita}
                                  </c:when>
                                  <c:otherwise>
                                   <font color="red">
                                     ${m.contabilita}
                                   </font>
                                  </c:otherwise>
                                </c:choose>
                            </td>
                            <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                                <c:choose>
                                    <c:when test="${m.id != movimento}">
                                          ${m.voceIncasso}
                                    </c:when>
                                    <c:otherwise>
                                        <font color="red">
                                          ${m.voceIncasso}
                                        </font>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                                <c:choose>
                                    <c:when test="${m.id != movimento}">
                                          ${m.numeroFattura}
                                    </c:when>
                                    <c:otherwise>
                                        <font color="red">
                                          ${m.numeroFattura}
                                        </font>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                                <c:choose>
                                    <c:when test="${m.id != movimento}">
                                            <c:if test="${(m.voceIncasso =='2244' or m.voceIncasso =='2243') and m.flagGruppoPrincipale=='1'}">
                                                <a href="javascript:openPMDetail('${m.idProgrammaMusicale}','${m.id}')"><u>${m.numProgrammaMusicale}</u></a> (Principale)
                                            </c:if>
                                            <c:if test="${(m.voceIncasso =='2244' or m.voceIncasso =='2243') and m.flagGruppoPrincipale=='0'}">
                                                <a href="javascript:openPMDetail('${m.idProgrammaMusicale}','${m.id}')"><u>${m.numProgrammaMusicale}</u></a> (Spalla)
                                            </c:if>
                                            <c:if test="${(m.voceIncasso !='2244' and m.voceIncasso !='2243')}">
                                                <a href="javascript:openPMDetail('${m.idProgrammaMusicale}','${m.id}')"><u>${m.numProgrammaMusicale}</u></a>
                                            </c:if>
                                    </c:when>
                                    <c:otherwise>
                                            <c:if test="${(m.voceIncasso =='2244' or m.voceIncasso =='2243') and m.flagGruppoPrincipale=='1'}">
                                               <a href="javascript:openPMDetail('${m.idProgrammaMusicale}','${m.id}')"><font color="red"><u>${m.numProgrammaMusicale}</u></font></a> <font color="red">(Principale)</font>
                                            </c:if>
                                            <c:if test="${(m.voceIncasso =='2244' or m.voceIncasso =='2243') and m.flagGruppoPrincipale=='0'}">
                                               <a href="javascript:openPMDetail('${m.idProgrammaMusicale}','${m.id}')"><font color="red"><u>${m.numProgrammaMusicale}</u></font></a> <font color="red">(Spalla)</font>
                                            </c:if>
                                            <c:if test="${(m.voceIncasso !='2244' and m.voceIncasso !='2243')}">
                                               <a href="javascript:openPMDetail('${m.idProgrammaMusicale}','${m.id}')"><font color="red"><u>${m.numProgrammaMusicale}</u></font></a>
                                            </c:if>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                                <c:choose>
                                    <c:when test="${m.id != movimento}">
                                        ${m.numeroPermesso}
                                    </c:when>
                                    <c:otherwise>
                                        <font color="red">
                                                ${m.numeroPermesso}
                                        </font>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td class="listViewEntryValue medium" nowrap="" data-field-type="string">

                                <c:choose>
                                    <c:when test="${m.id != movimento}">
                                        <c:if test="${m.importiRicalcolati == null or empty m.importiRicalcolati}">
                                            ${m.importoTotDemFormatted}
                                        </c:if>
                                        <c:if test="${m.importiRicalcolati != null and not empty m.importiRicalcolati}">
                                            <strike>${m.importoTotDemFormatted}</strike>
                                        </c:if>
                                    </c:when>
                                    <c:otherwise>
                                        <c:if test="${m.importiRicalcolati == null or empty m.importiRicalcolati}">
                                            <font color="red">
                                               ${m.importoTotDemFormatted}
                                            </font>
                                        </c:if>
                                        <c:if test="${m.importiRicalcolati != null and not empty m.importiRicalcolati}">
                                            <font color="red">
                                                <strike>${m.importoTotDemFormatted}</strike>
                                            </font>
                                        </c:if>
                                    </c:otherwise>
                                </c:choose>
                            </td>

                            <td class="listViewEntryValue medium" nowrap="" data-field-type="string">
                                <c:choose>
                                    <c:when test="${m.id != movimento}">
                                        <c:if test="${m.importiRicalcolati != null and not empty m.importiRicalcolati}">
                                            <a href="javascript:toggle_visibility('importiDiv_${m.id}')"><u>${m.importiRicalcolati.get(0).importoFormatted}</u></a>
                                            <div id="importiDiv_${m.id}" style="display:none; background:#F2F2F2; border-style: groove; width: 290px;">
                                                <c:forEach items="${m.importiRicalcolati}" var="i">
                                                    &#8226; Elabor. ${i.idEsecuzioneRicalcolo} del ${i.dataOraInserimentoStr}: ${i.importoFormatted}<br/>
                                                </c:forEach>
                                            </div>
                                        </c:if>
                                        <c:if test="${m.importiRicalcolati == null or empty m.importiRicalcolati}">
                                            0.0 &#8226;
                                        </c:if>
                                    </c:when>
                                    <c:otherwise>

                                            <c:if test="${m.importiRicalcolati != null and not empty m.importiRicalcolati}">
                                                <a href="javascript:toggle_visibility('importiDiv_${m.id}')"><font color="red"><u>${m.importiRicalcolati.get(0).importoFormatted}</u></font></a>
                                                <div id="importiDiv_${m.id}" style="display:none; background:#F2F2F2; border-style: groove; width: 290px;">
                                                    <c:forEach items="${m.importiRicalcolati}" var="i">
                                                        &#8226; Elabor. ${i.idEsecuzioneRicalcolo} del ${i.dataOraInserimentoStr}: ${i.importoFormatted}<br/>
                                                    </c:forEach>
                                                </div>
                                            </c:if>
                                            <c:if test="${m.importiRicalcolati == null or empty m.importiRicalcolati}">
                                                <font color="red">0.0 &#8364;</font>
                                            </c:if>

                                    </c:otherwise>
                                </c:choose>

                            </td>
                        </tr>
                    </c:forEach>

                </tbody>
            </table>

          <br/>
          <font color="red"><i>Movimentazione selezionata</i></font>

        </c:if>

        <!-- INIZIO TASTI FONDO PAGINA -->
        <br/><br/>

        <div class="row-fluid">
            <div class="pull-right">
                <!--
                <button class="btn btn-success" type="submit">
                    <strong>Salva</strong>
                </button>
                <a class="cancelLink" type="reset" onclick="javascript:window.history.back();">Annulla</a>
                -->
                <a href="${contextPath}/secure/exportReport?idReport=Dettaglio_Movimenti&detailType=MOV&idDetail=${movimento}">
                    <img src="${contextPath}/images/excel.jpg" alt="Esporta in formato excel" width="35" height="38"/>
                </a>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- FINE TASTI FONDO PAGINA -->


        <input type="hidden" name="idProgrammaMusicale" id="idProgrammaMusicaleId" value=""/>
        <input type="hidden" name="idMovimento" id="idMovimentoId" value=""/>

    </form>


    <!-- INIZIO FOOTER -->
    <jsp:include page="footer.jsp"/>
    <!-- FINE FOOTER -->

</body>
</html>