<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>

 <jsp:include page="header.jsp"/>

<body data-language="it_it">


<div id="page">

    <!-- container which holds data temporarly for pjax calls -->
    <div id="pjaxContainer" class="hide noprint"></div>

    <!-- INIZIO INTESTAZIONE-->
    <jsp:include page="menu_bar.jsp"/>
    <!-- FINE INTESTAZIONE-->

    <!-- INIZIO BARRA DI RICERCA -->
    <jsp:include page="search_bar_campionamentoStoricoEsecuzioni.jsp"/>
    <!-- FINE BARRA DI RICERCA -->


    <!-- BODY -->
    <form class="form-horizontal recordEditView" id="EditView" name="EditView" method="post" action="">

        <!-- INIZIO SEZIONE VIEW DATI -->

        <c:if test="${resultPage!=null and empty resultPage.records}">

            <div class="listViewEntriesDiv">
                <div class="bottomscroll-div" style="width: 1308px;">
                    <b>Nessun dato ritornato</b>
                    <br/><br/><br/>
                </div>
            </div>
        </c:if>

            <c:if test="${resultPage.records.size() > 0}">

            <div class="listViewEntriesDiv">

                    <br/>
                    Numero di esecuzioni campionamenti: <b>${resultPage.records.size()}</b> di <b>${resultPage.totRecords}</b>
                    <br/>
                    <center>

                        <c:if test="${resultPage.pages.size() > 1}">
                            <c:if test="${resultPage.currentPage > 1}">
                              <a href="javascript:callPage(${resultPage.currentPage-1});">< Prec</a>
                            </c:if>

                            <c:forEach items="${resultPage.pages}" var="p">
                                <c:choose>
                                    <c:when test="${resultPage.currentPage != p}">
                                        <a href="javascript:callPage(${p});">${p} </a>
                                    </c:when>
                                    <c:otherwise>
                                        <a style="color: #09b6eb;">${p} </a>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>

                            <c:if test="${resultPage.currentPage < resultPage.pages.size()}">
                               <a href="javascript:callPage(${resultPage.currentPage+1});"> > Suc </a>
                            </c:if>
                        </c:if>

                    </center>

                    <br/><br/>

                    <table class="table table-bordered">
                        <thead>
                        <tr class="listViewHeaders">
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues">Data e Ora</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues">Servizio</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues">Utente</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues">Dettagli</a>
                            </th>
                        </tr>
                        </thead>

                        <tbody>

                        <c:forEach items="${resultPage.records}" var="m">
                            <tr id="row_${m.id}" class="listViewEntries">
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.dataInserimentoStr}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.serviceName}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.userName}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.message}</td>
                            </tr>
                        </c:forEach>

                        </tbody>
                    </table>
                    </c:if>
                <!-- FINE SEZIONE VIEW DATI -->

        <!-- INIZIO TASTI FONDO PAGINA -->
        <br/>
        <center><span style="color: red">${message}</span></center>
        <br/>

        <!-- FINE TASTI FONDO PAGINA -->

    </form>

    <!-- INIZIO FOOTER -->
    <jsp:include page="footer.jsp"/>
    <!-- FINE FOOTER -->

</body>
</html>