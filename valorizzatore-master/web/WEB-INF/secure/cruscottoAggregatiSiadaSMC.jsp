<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>

 <jsp:include page="header.jsp"/>

<body data-language="it_it">

<div id="page">

    <!-- container which holds data temporarly for pjax calls -->
    <div id="pjaxContainer" class="hide noprint"></div>

    <!-- INIZIO INTESTAZIONE-->
    <jsp:include page="menu_bar.jsp"/>
    <!-- FINE INTESTAZIONE-->

    <!-- INIZIO BARRA DI RICERCA -->
    <jsp:include page="search_bar_cruscottoAggregatiSiadaSMC.jsp"/>
    <!-- FINE BARRA DI RICERCA -->

    <script type="text/javascript">

       function openPMDetail(idProgrammaMusicale){
           document.forms.ListaAggregatiViewId.elements.idProgrammaMusicale.value=idProgrammaMusicale;
           document.forms.ListaAggregatiViewId.action='${contextPath}/secure/pmDetail';
           document.forms.ListaAggregatiViewId.submit();
        }

    </script>

    <!-- BODY -->
    <form class="form-horizontal recordEditView" id="ListaAggregatiViewId" name="ListaAggregatiView" method="get" action="${contextPath}/secure/movimentiDetail">

        <!-- INIZIO SEZIONE VIEW DATI -->

        <c:if test="${resultPage!=null and empty resultPage.records and elabSelected != -1}">
            <div class="listViewEntriesDiv">
                    <br/><br/>
                    <b>Nessun dato ritornato</b>
                    <br/><br/><br/>
            </div>
        </c:if>

            <c:if test="${resultPage.records.size() > 0}">

            <div class="listViewEntriesDiv">
                <div style="width: 1308px;">
                    <br/>
                    Numero movimenti contabili: <b>${resultPage.records.size()}</b> di <b>${resultPage.totRecords}</b>
                    <br/>
                    <center>

                        <c:if test="${resultPage.pages.size() > 1}">
                            <c:if test="${resultPage.currentPage > 1}">
                              <a href="javascript:callPage(${resultPage.currentPage-1});">< Prec</a>
                            </c:if>

                            <c:forEach items="${resultPage.pages}" var="p">
                                <c:choose>
                                    <c:when test="${resultPage.currentPage != p}">
                                        <a href="javascript:callPage(${p});">${p} </a>
                                    </c:when>
                                    <c:otherwise>
                                        <a style="color: #09b6eb;">${p} </a>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>

                            <c:if test="${resultPage.currentPage < resultPage.pages.size()}">
                               <a href="javascript:callPage(${resultPage.currentPage+1});"> > Suc </a>
                            </c:if>
                        </c:if>

                    </center>
                    <br/><br/>
                    <table class="table table-bordered listViewEntriesTable">
                        <thead>
                        <tr class="listViewHeaders">
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues">Data inoltro<br/>a SIADA</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues">Mese<br/>Contabile</a>
                            </th>
                            <!--<th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues">Mese Contabile<br/>Originale</a>
                            </th>-->
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues">Voce<br/>Incasso</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues">Voce<br/>Incasso SIADA</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues">Numero<br/>Programma Musicale</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues">Numero<br/>Fattura</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues">ID Evento</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues">Inizio Evento</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues">Fine Evento</a>
                            </th>
                            <th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues">Importo</a>
                            </th>
                            <!--<th nowrap="" valign="center" align="center">
                                <a class="listViewHeaderValues">Importo<br/>Singola Cedola</a>
                            </th>-->
                        </tr>
                        </thead>

                        <tbody>

                        <c:forEach items="${resultPage.records}" var="m">
                            <tr id="row_${m.id}" class="listViewEntries">
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.dataElaborazioneStr}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.meseContabile}</td>
                                <!--<td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.meseContabileOrig}</td> -->
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.voceIncasso}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.voceIncassoSiada}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string"><a href="javascript:openPMDetail('${m.idProgrammaMusicale}')"><u>${m.numeroProgrammaMusicale}</u></a></td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.numeroFattura}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.idEvento}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.dataInizioEventoStr}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.dataFineEventoStr}</td>
                                <td class="listViewEntryValue medium" nowrap="" data-field-type="string">${m.importoFormatted}</td>
                                <!--<td class="listViewEntryValue medium" nowrap="" data-field-type="string"></td> -->
                            </tr>
                        </c:forEach>

                        </tbody>
                    </table>
                    </c:if>

                  <input type="hidden" name="idProgrammaMusicale" name="idProgrammaMusicale" value=""/>
                <!-- FINE SEZIONE VIEW DATI -->


                    <c:if test="${resultPage!=null and not empty resultPage.records and resultPage.records.size() > 0}">
                        <!-- INIZIO TASTI FONDO PAGINA -->
                        <br/><br/>

                        <div class="row-fluid">
                            <div class="pull-right">
                                <!--
                                <button class="btn btn-success" type="submit">
                                    <strong>Salva</strong>
                                </button>
                                <a class="cancelLink" type="reset" onclick="javascript:window.history.back();">Annulla</a>
                                -->
                                <a href="${contextPath}/secure/exportReport?idReport=Verifica_PM_SIADA_SMC">
                                    <img src="${contextPath}/images/excel.jpg" alt="Esporta in formato excel" width="35" height="38"/>
                                </a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <!-- FINE TASTI FONDO PAGINA -->
                    </c:if>

    </form>

    <!-- INIZIO FOOTER -->
    <jsp:include page="footer.jsp"/>
    <!-- FINE FOOTER -->

</body>
</html>