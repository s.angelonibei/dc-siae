<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<div class="navbar commonActionsContainer noprint">
    <div class="actionsContainer row-fluid" style="height: 90px;">
        <div class="span2">
    <span class="companyLogo">
      <img src="${contextPath}/images/siae_logo_siaetrasparente.png" title="siae_logo_siaetrasparente.png" alt="siae_logo_siaetrasparente.png">&nbsp;</span>
        </div>
        <div class="span10">
            <div class="row-fluid">

                <form class="form-horizontal recordEditView" id="searchFormId" name="searchForm" method="get" action="${contextPath}/secure/eventiPagatiList">

                    <div class="searchElement span14">

                        <div class="select-search">
                            <a class="globalSearch_module_All">Contabilita' da:</a>
                            <select style="width: 110px" class="js-basic-single-select" id="meseInizioPeriodoContabileId" name="meseInizioPeriodoContabile" required="true">
                                <option value="" class="globalSearch_module_All" selected>- Mese -</option>
                                <option value="1" class="globalSearch_module_All" >Gennaio</option>
                                <option value="2" class="globalSearch_module_All" >Febbraio</option>
                                <option value="3" class="globalSearch_module_All" >Marzo</option>
                                <option value="4" class="globalSearch_module_All" >Aprile</option>
                                <option value="5" class="globalSearch_module_All" >Maggio</option>
                                <option value="6" class="globalSearch_module_All" >Giugno</option>
                                <option value="7" class="globalSearch_module_All" >Luglio</option>
                                <option value="8" class="globalSearch_module_All" >Agosto</option>
                                <option value="9" class="globalSearch_module_All" >Settembre</option>
                                <option value="10" class="globalSearch_module_All" >Ottobre</option>
                                <option value="11" class="globalSearch_module_All" >Novembre</option>
                                <option value="12" class="globalSearch_module_All" >Dicembre</option>
                            </select>
                            <select style="width: 80px" class="js-basic-single-select" id="annoInizioPeriodoContabileId" name="annoInizioPeriodoContabile" required="true">
                                <option value="" class="globalSearch_module_All" selected>- Anno -</option>
                                <option value="2015" class="globalSearch_module_All" >2015</option>
                                <option value="2016" class="globalSearch_module_All" >2016</option>
                                <option value="2017" class="globalSearch_module_All" >2017</option>
                                <option value="2018" class="globalSearch_module_All" >2018</option>
                                <option value="2019" class="globalSearch_module_All" >2019</option>
                                <option value="2020" class="globalSearch_module_All" >2020</option>
                                <option value="2021" class="globalSearch_module_All" >2021</option>
                                <option value="2022" class="globalSearch_module_All" >2022</option>
                                <option value="2023" class="globalSearch_module_All" >2023</option>
                                <option value="2024" class="globalSearch_module_All" >2024</option>
                                <option value="2025" class="globalSearch_module_All" >2025</option>
                                <option value="2026" class="globalSearch_module_All" >2026</option>
                                <option value="2027" class="globalSearch_module_All" >2027</option>
                                <option value="2028" class="globalSearch_module_All" >2028</option>
                                <option value="2029" class="globalSearch_module_All" >2029</option>
                                <option value="2030" class="globalSearch_module_All" >2030</option>
                            </select>
                            <a class="globalSearch_module_All">a:</a>
                            <select style="width: 110px" class="js-basic-single-select" id="meseFinePeriodoContabileId" name="meseFinePeriodoContabile" required="true">
                                <option value="" class="globalSearch_module_All" selected>- Mese -</option>
                                <option value="1" class="globalSearch_module_All" >Gennaio</option>
                                <option value="2" class="globalSearch_module_All" >Febbraio</option>
                                <option value="3" class="globalSearch_module_All" >Marzo</option>
                                <option value="4" class="globalSearch_module_All" >Aprile</option>
                                <option value="5" class="globalSearch_module_All" >Maggio</option>
                                <option value="6" class="globalSearch_module_All" >Giugno</option>
                                <option value="7" class="globalSearch_module_All" >Luglio</option>
                                <option value="8" class="globalSearch_module_All" >Agosto</option>
                                <option value="9" class="globalSearch_module_All" >Settembre</option>
                                <option value="10" class="globalSearch_module_All" >Ottobre</option>
                                <option value="11" class="globalSearch_module_All" >Novembre</option>
                                <option value="12" class="globalSearch_module_All" >Dicembre</option>
                            </select>
                            <select style="width: 80px" class="js-basic-single-select" id="annoFinePeriodoContabileId" name="annoFinePeriodoContabile" required="true">
                                <option value="" class="globalSearch_module_All" selected>- Anno -</option>
                                <option value="2015" class="globalSearch_module_All" >2015</option>
                                <option value="2016" class="globalSearch_module_All" >2016</option>
                                <option value="2017" class="globalSearch_module_All" >2017</option>
                                <option value="2018" class="globalSearch_module_All" >2018</option>
                                <option value="2019" class="globalSearch_module_All" >2019</option>
                                <option value="2020" class="globalSearch_module_All" >2020</option>
                                <option value="2021" class="globalSearch_module_All" >2021</option>
                                <option value="2022" class="globalSearch_module_All" >2022</option>
                                <option value="2023" class="globalSearch_module_All" >2023</option>
                                <option value="2024" class="globalSearch_module_All" >2024</option>
                                <option value="2025" class="globalSearch_module_All" >2025</option>
                                <option value="2026" class="globalSearch_module_All" >2026</option>
                                <option value="2027" class="globalSearch_module_All" >2027</option>
                                <option value="2028" class="globalSearch_module_All" >2028</option>
                                <option value="2029" class="globalSearch_module_All" >2029</option>
                                <option value="2030" class="globalSearch_module_All" >2030</option>
                            </select>

                            <div class="select-search">
                                <select style="width: 170px" class="js-basic-single-select" id="tipoDocumentoId" name="tipoDocumento">
                                    <option value="" class="globalSearch_module_All" selected>- Tipo documento -</option>
                                    <option value="501" class="globalSearch_module_All" >Entrate (501)</option>
                                    <option value="221" class="globalSearch_module_All" >Uscite (221)</option>
                                </select>
                            </div>

                        </div>

                         <br/><br/>

                        <div class="select-search">
                            <select style="width: 220px" class="js-basic-single-select" id="presenzaMovimentiId" name="presenzaMovimenti">
                                <option value="" class="globalSearch_module_All" selected>- Presenza movimenti -</option>
                                <option value="T" class="globalSearch_module_All" >Tutti gli eventi</option>
                                <option value="M" class="globalSearch_module_All" >Solo eventi con movimenti</option>
                            </select>
                        </div>
                        <div class="input-append searchBar">
                            <input type="text"  style="width: 100px" class="" value="${evento}" name="evento" id="evento" placeholder="Id Evento" size="10">&nbsp;&nbsp;
                        </div>
                        <div class="input-append searchBar">
                            <input type="text" style="width: 100px" class="" value="${fattura}" name="fattura" id="fattura" placeholder="Fattura" results="10">&nbsp;&nbsp;
                        </div>
                        <div class="input-append searchBar">
                            <input type="text" style="width: 100px" class="" value="${voceIncasso}" name="voceIncasso" id="voceIncasso" placeholder="Voce Incasso" results="5">&nbsp;&nbsp;
                        </div>
                        <div class="input-append searchBar">
                            <input type="text" style="width: 100px" class="" value="${reversale}" name="reversale" id="reversalePM" placeholder="Reversale" results="10">&nbsp;&nbsp;
                        </div>

                        <br/><br/>

                        <div class="input-append searchBar">
                            <input type="text" style="width: 140px;" placeholder="Data inizio Evento" value="${dataInizioEvento}" name="dataInizioEvento" id="dataInizioEventoId">&nbsp;&nbsp;
                        </div>

                        <div class="input-append searchBar">
                            <input type="text" style="width: 130px;" class="" value="${oraInizioEvento}" name="oraInizioEvento" id="oraInizioEventoId" placeholder="Ora Inizio (hh:mi)" results="10">&nbsp;&nbsp;
                        </div>

                        <div class="input-append searchBar">
                            <input type="text" style="width: 100px;" class="" value="${locale}" name="locale" id="localeId" placeholder="Locale" results="10">&nbsp;&nbsp;
                        </div>
                        <div class="input-append searchBar">
                            <input type="text" style="width: 100px;" class="" value="${codiceBA}" name="codiceBA" id="codiceBAId" placeholder="Codice BA" results="10">&nbsp;&nbsp;
                        </div>

                        <div class="input-append searchBar">
                            <input type="text" style="width: 100px;" class="" value="${seprag}" name="seprag" id="sepragId" placeholder="Seprag" results="10">&nbsp;&nbsp;
                        </div>

                    </div>

                    <input type="hidden" id="pageId" name="page" value="1"/>

                    <div>
                          <span class="pull-right">
                           <button class="btn btn-success" type="submit">
                            <strong>Cerca</strong>
                           </button>
                         </span>
                    </div>

                </form>

                <script type="text/javascript">

                    $(function() {
                       $("#annoInizioPeriodoContabileId").val(${annoInizioPeriodoContabile});
                       $("#meseInizioPeriodoContabileId").val(${meseInizioPeriodoContabile});
                       $("#annoFinePeriodoContabileId").val(${annoFinePeriodoContabile});
                       $("#meseFinePeriodoContabileId").val(${meseFinePeriodoContabile});
                       $("#presenzaMovimentiId").val('${presenzaMovimenti}');

                       $("#dataInizioEventoId").datepicker({ dateFormat: 'dd/mm/yy'});
                    });

                    function callPage(page){
                        $("#pageId").val(page);
                        document.getElementById("searchFormId").submit();
                    }
                    document.getElementById('tipoDocumentoId').value = '${tipoDocumento}';

                    function doSubmit(evento){

                        document.forms.ListaEventiViewId.elements.evento.value=evento;
                        document.forms.ListaEventiViewId.submit();
                    }

                </script>


                <!-- tasto + a destra -->
                <div class="notificationMessageHolder span2"></div>
                <div class="nav quickActions btn-toolbar span2 pull-right marginLeftZero">
                    <div class="pull-right commonActionsButtonContainer"><div class="btn-group cursorPointer" id="guiderHandler"></div>
                        &nbsp;
                        <div class="btn-group cursorPointer">
                        </div>&nbsp;</div>
                </div>
            </div></div></div></div>
</div>


<div class="bodyContents" style="min-height: 488px;">
    <div class="mainContainer row-fluid" style="min-height: 1355px;">
        <div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 1355px;">
            <div class="container-fluid editViewContainer">

                <!-- Sezione relativa alle breadCrumbs in pagina -->
                Cruscotto:
                <c:forEach var="entry" items="${breadCrumbList.breadCrumbs}">
                <a href="${entry.url}">${entry.label}</a> >
                </c:forEach>
                <!-- Sezione relativa alle breadCrumbs in pagina -->


