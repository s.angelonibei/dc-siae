<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<div class="navbar commonActionsContainer noprint">
    <div class="actionsContainer row-fluid">
        <div class="span2">
    <span class="companyLogo">
      <img src="${contextPath}/images/siae_logo_siaetrasparente.png" title="siae_logo_siaetrasparente.png" alt="siae_logo_siaetrasparente.png">&nbsp;</span>
        </div>
        <div class="span10">
            <div class="row-fluid">

                <form class="form-horizontal recordEditView" id="searchFormId" name="searchForm" method="get" action="${contextPath}/secure/tracciamentoPMLista">

                    <!-- INIZIO SEZIONE DATI -->

                    <table border="0" >
                        <tr>
                            <td valign="center">
                                <input type="text" style="width: 100px;" placeholder="Data inizio" value="${dataInizio}" name="dataInizio" id="dataInizioId">
                            </td>
                            <td valign="center">
                                <input type="text" style="width: 100px;" placeholder="Data fine" value="${dataFine}" name="dataFine" id="dataFineId">
                            </td>
                            <td valign="center">
                                <input type="text" class="" value="${numeroPM}" name="numeroPM" id="numeroPMId" placeholder="Numero Programma Musicale" results="10">&nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>

                    <input type="hidden" id="pageId" name="page" value="1"/>

                    <div class="contentHeader row-fluid">
                          <span class="pull-right">
                           <button class="btn btn-success" type="submit">
                            <strong>Cerca</strong>
                           </button>
                         </span>
                    </div>

                </form>

                <script type="text/javascript">

                    function callPage(page){
                        $("#pageId").val(page);
                        document.getElementById("searchFormId").submit();
                    }

                    $(function() {
                       $("#dataInizioId").datepicker({ dateFormat: 'dd/mm/yy'});
                       $("#dataFineId").datepicker({ dateFormat: 'dd/mm/yy'});
                    });

                </script>

                <!-- tasto + a destra -->
                <div class="notificationMessageHolder span2"></div>
                <div class="nav quickActions btn-toolbar span2 pull-right marginLeftZero">
                    <div class="pull-right commonActionsButtonContainer"><div class="btn-group cursorPointer" id="guiderHandler"></div>
                        &nbsp;
                        <div class="btn-group cursorPointer">
                            <!--
                                <img id="menubar_quickCreate" src="${contextPath}/images/btnAdd.png" class="alignMiddle" alt="Creazione Veloce" title="Creazione Veloce" data-toggle="dropdown">
                                <ul class="dropdown-menu dropdownStyles commonActionsButtonDropDown">
                                    <li class="title"><strong>Parametri di ricerca aggiuntivi</strong></li><hr>
                                    <li id="quickCreateModules">
                                        <div class="row-fluid">
                                            <div class="span12"><div class="row-fluid">
                                                <div class="span4"><a id="filter_Evento" class="quickCreateModule" href="javascript:void(0)">Evento</a></div>
                                                <div class="span4"><a id="filter_Locale" class="quickCreateModule" href="javascript:void(0)">Locale</a></div>
                                                <div class="span4"><a id="filter_Comune" class="quickCreateModule" href="javascript:void(0)">Comune</a></div>
                                            </div>
                                                <div class="row-fluid">
                                                    <div class="span4"><a id="filter_Organizzatore" class="quickCreateModule"  href="javascript:void(0)">Organizzatore</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                -->
                        </div>&nbsp;</div>
                </div>
            </div></div></div></div>
</div>

<div class="bodyContents" style="min-height: 488px;">
    <div class="mainContainer row-fluid" style="min-height: 1355px;">
        <div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 1355px;">
            <div class="container-fluid editViewContainer">

