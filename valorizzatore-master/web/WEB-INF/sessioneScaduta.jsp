<!DOCTYPE html>
<html>

<head>
    <title>Valorizzatore</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="${contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="${contextPath}/css/jqueryBxslider.css" rel="stylesheet" />
</head>

<body>
<div class="container-fluid login-container">
    <div class="row-fluid"><div class="span3">
        <div class="logo"><img src="${contextPath}/images/siae_logo_siaetrasparente.png" heigh="200" width="100"><br/>
            <a target="_blank" href="http://"></a>
        </div>
    </div>
        <div class="span9">
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="content-wrapper">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="carousal-container">
                            </div>
                        </div>
                        <div class="span6">


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="navbar navbar-fixed-bottom">
    <div class="navbar-inner">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6 pull-left">
                    <div class="footer-content"><small>&#169 2016&nbsp;<a href="http://www.siae.it"> SIAE</a></small>
                    </div>
                </div>

                <div class="span6 pull-right" >
                    <div class="pull-center footer-icons"><small><!--Connect with US&nbsp;--></small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>