<!DOCTYPE html>
<html>

<head>
    <title>Valorizzatore</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="css/jqueryBxslider.css" rel="stylesheet" />
</head>

<body>
<div class="container-fluid login-container">
    <div class="row-fluid"><div class="span3">
        <div class="logo"><img src="images/siae_logo_siaetrasparente.png" heigh="200" width="100"><br/>
            <a target="_blank" href="http://"></a>
        </div>
    </div>
        <div class="span9">
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="content-wrapper">
                <div class="container-fluid">
                    <div class="row-fluid">
                            <center>
                                <h3>Autorizzazione negata</h3>
                                <a id="homeIntranetId" href="">Home</a>
                            </center>

                     </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="navbar navbar-fixed-bottom">
    <div class="navbar-inner">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6 pull-left">
                    <div class="footer-content">
                        <center><small>
                            &#169 2016&nbsp;<a href="http://www.siae.it"> SIAE</a></small>
                        </center>
                    </div>
                </div>

                <div class="span6 pull-right" >
                    <div class="pull-center footer-icons"><small><!--Connect with US&nbsp;--></small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

<script>

    function getQueryParameter ( parameterName ) {
      var queryString = window.top.location.search.substring(1);
      var parameterName = parameterName + "=";
      if ( queryString.length > 0 ) {
        begin = queryString.indexOf ( parameterName );
        if ( begin != -1 ) {
          begin += parameterName.length;
          end = queryString.indexOf ( "&" , begin );
            if ( end == -1 ) {
            end = queryString.length
          }
          return unescape ( queryString.substring ( begin, end ) );
        }
      }
      return "null";
    }

    document.getElementById('homeIntranetId').href = getQueryParameter ( 'homeIntranet' );


</script>

</html>