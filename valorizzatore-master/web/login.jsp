<!DOCTYPE html>
<html>

<head>
    <title>Valorizzatore</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="css/jqueryBxslider.css" rel="stylesheet" />
</head>

<body>
<div class="container-fluid login-container">
    <div class="row-fluid"><div class="span3">
        <div class="logo"><img src="images/siae_logo_siaetrasparente.png" heigh="200" width="100"><br/>
            <a target="_blank" href="http://"></a>
        </div>
    </div>
        <div class="span9">
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="content-wrapper">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="carousal-container">
                            </div>
                        </div>
                        <div class="span6">
                            <div class="login-area">
                                <div class="login-box" id="loginDiv">
                                    <div class=""><h3 class="login-header">Login Valorizzatore</h3></div>

                                    <form class="form-horizontal login-form" style="margin:0;" action="<%=request.getContextPath()%>/appLogin" method="post">

                                        <div class="control-group">
                                            <label class="control-label" for="username"><b>Username</b></label>
                                            <div class="controls">
                                                <input type="text" id="username" name="app_username" placeholder="Username">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="password"><b>Password</b></label>
                                            <div class="controls">
                                                <input type="password" id="password" name="app_password" placeholder="Password">
                                            </div>
                                        </div>
                                        <div class="control-group signin-button">
                                            <div class="controls" id="forgotPassword">
                                                <button type="submit" class="btn btn-primary sbutton">Login</button>&nbsp;&nbsp;&nbsp;
                                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                            </div>
                                        </div>
                                    </form>

                                    <font color="red">
                                        ${SPRING_SECURITY_LAST_EXCEPTION.message}
                                    </font>

                                    <div class="login-subscript"><small> <!--Powered by --></small></div></div>

                                <div class="login-box hide" id="forgotPasswordDiv">
                                    <form class="form-horizontal login-form" style="margin:0;" action="forgotPassword.php" method="POST">
                                        <input type='hidden' name='__vtrftk' value="sid:5dc04b0c058af33d0403d83ea89ee4e68d1e10c3,1464600634" />
                                        <div class=""><h3 class="login-header"><!--Forgot Password--></h3></div>
                                        <div class="control-group"><label class="control-label" for="user_name"><b>Username</b></label>
                                            <div class="controls"><input type="text" id="user_name" name="user_name" placeholder="Username"></div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="email"><b>Email</b></label>
                                            <div class="controls">
                                                <input type="text" id="emailId" name="emailId"  placeholder="Email">
                                            </div>
                                        </div>
                                        <div class="control-group signin-button">
                                            <div class="controls" id="backButton">
                                                <input type="submit" class="btn btn-primary sbutton" value="Submit" name="retrievePassword">&nbsp;&nbsp;&nbsp;<a>Back</a></div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="navbar navbar-fixed-bottom">
    <div class="navbar-inner">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6 pull-left">
                    <div class="footer-content"><small>&#169 2016&nbsp;<a href="http://www.siae.it"> SIAE</a></small>
                    </div>
                </div>

                <div class="span6 pull-right" >
                    <div class="pull-center footer-icons"><small><!--Connect with US&nbsp;--></small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>