package com.alkemytech.sophia.tools;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.common.config.ConfigurationLoader;
import com.alkemytech.sophia.common.rest.RestClient;
import com.alkemytech.sophia.common.sqs.Iso8601Helper;
import com.alkemytech.sophia.common.sqs.SQSService;
import com.alkemytech.sophia.common.tools.Log4j2Tools;
import com.alkemytech.sophia.common.tools.StringTools;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class KillEmrZombies {

	private static final Logger logger = LoggerFactory.getLogger(KillEmrZombies.class);

	private static final class GuiceModule extends AbstractModule {

		private final String[] args;
		
		public GuiceModule(String[] args) {
			super();
			this.args = args;
		}

		@Override
		protected void configure() {
			
			// properties
			final Properties properties;
			if (null != args && args.length > 0) {
				properties = new ConfigurationLoader()
					.withCommandLineArgs(args).load();
			} else {
				properties = new ConfigurationLoader()
					.withResourceName("/emr-autokill.properties").load();
			}
			Names.bindProperties(binder(), properties);
			bind(Properties.class)
				.annotatedWith(Names.named("configuration"))
				.toInstance(properties);

			// default time zone
			TimeZone.setDefault(TimeZone.getTimeZone(properties.getProperty("defaultTimezone", "UTC")));
			
			// initialize log4j2
			Log4j2Tools.initialize(properties);
			
			// aws services
			bind(SQSService.class).asEagerSingleton();
			
			// tools
			bind(KillEmrZombies.class).asEagerSingleton();

		}
		
	}
	
	private final Properties configuration;
	private final SQSService sqsService;

	@Inject
	protected KillEmrZombies(@Named("configuration") Properties configuration,
			SQSService sqsService) {
		super();
		this.configuration = configuration;
		this.sqsService = sqsService;
	}
	
	private String getHostName() {
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			return "localhost";
		}
	}
	
	private String getAsString(JsonObject json, String key, String ifNull) {
		final JsonElement element = (null == json ? null : json.get(key));
		final String value = (null == element ? null : element.getAsString());
		return null == value ? ifNull : value;
	}
	
	public void doWork() throws Exception {
		
		final Gson gson = new GsonBuilder()
			.disableHtmlEscaping()
			.setPrettyPrinting()
			.create();
		final Pattern typeRegex = Pattern.compile(configuration.getProperty("emr_auto_kill.type_regex", "started"));
		final long timeoutSeconds = Long.parseLong(configuration.getProperty("emr_auto_kill.timeout_seconds", "1800")); // default 30 minutes
		RestClient.Result result = RestClient.get(configuration.getProperty("emr_auto_kill.url.hostnames"));
		logger.debug("doWork: result: " + result);
		if (200 == result.getStatusCode()) {
			final JsonArray resultJson = result.getJsonElement().getAsJsonArray();
			logger.debug("doWork: resultJson {}", resultJson);
			if (null != resultJson) for (JsonElement hostname : resultJson) {
				final JsonObject hostnameJson = (JsonObject) hostname;
				logger.debug("doWork: hostnameJson {}", hostnameJson);
				final String clusterId = getAsString(hostnameJson, "hostname", null);
				final String queueType = getAsString(hostnameJson, "queueType", null);
				final String serviceName = getAsString(hostnameJson, "serviceName", null);
				final String idDsr = getAsString(hostnameJson, "idDsr", null);
				final String insertTime = getAsString(hostnameJson, "insertTime", null);
				final String applicationId = getAsString(hostnameJson, "applicationId", null);
				
				if (null != clusterId &&
						null != queueType &&
						null != insertTime &&
						null != applicationId &&
						typeRegex.matcher(queueType).matches()) {
					final Date startDate = Iso8601Helper.parse(insertTime);
					logger.debug("doWork: startDate {}", startDate);
					final long elapsed = System.currentTimeMillis() - startDate.getTime();
					logger.debug("doWork: elapsed {}", elapsed);
					if (elapsed > TimeUnit.SECONDS.toMillis(timeoutSeconds)) {
						
						logger.info("doWork: killing {} after {}ms", applicationId, elapsed);

						final String killEmrStepUrl = configuration.getProperty("emr_auto_kill.url.kill_emr_step")
								.replace(":clusterId", clusterId)
								.replace(":applicationId", applicationId);
						final JsonObject requestJson = new JsonObject();
						requestJson.addProperty("clusterId", clusterId);
						requestJson.addProperty("applicationId", applicationId);
						result = RestClient.post(killEmrStepUrl, requestJson);
						logger.debug("doWork: result: " + result);

						// send message to killed queue
						final String killedQueueName = configuration.getProperty("emr_auto_kill.queue_name")
								.replace(":service", serviceName);
						logger.debug("doWork: killedQueueName {}", killedQueueName);
						if (!StringTools.isNullOrEmpty(killedQueueName)) {
							final String killedQueueUrl = sqsService.getOrCreateUrl(killedQueueName);
							logger.debug("doWork: killedQueueUrl {}", killedQueueUrl);
							final String serviceBusQueueUrl = sqsService
									.getOrCreateUrl(configuration.getProperty("emr_auto_kill.service_bus"));
							logger.debug("doWork: serviceBusQueueUrl {}", serviceBusQueueUrl);
							// header
							final JsonObject header = new JsonObject();
							header.addProperty("uuid", UUID.randomUUID().toString());
							header.addProperty("timestamp", Iso8601Helper.format(new Date()));
							header.addProperty("queue", killedQueueName);
							header.addProperty("sender", "emr_autokill");
							header.addProperty("hostname", getHostName());
							// input
							final JsonObject body = new JsonObject();
							if (null != idDsr) {
								body.addProperty("idDsr", idDsr);
							}
//							body.addProperty("dspCode", null);
//							body.addProperty("year", null);
//							body.addProperty("month", null);
//							body.addProperty("country", null);
							body.addProperty("warning", "dummy body generated by emr_autokill");
							final JsonObject input = new JsonObject();
							input.add("body", body);
							input.addProperty("warning", "dummy input generated by emr_autokill");
							// context
							final JsonObject context = new JsonObject();
							context.addProperty("applicationId", applicationId);
							context.addProperty("emrClusterId", clusterId);
							// error
							final JsonObject error = new JsonObject();
							error.addProperty("code", "0");
							error.addProperty("description", "EMR step " + applicationId 
									+ " @ " + clusterId + " killed after " + elapsed + "ms");
							// message
							final JsonObject message = new JsonObject();
							message.add("header", header);
							message.add("context", context);
							message.add("input", input);
							message.add("output", result.getJsonElement());
							message.add("error", error);
							// send message to service bus
							sqsService.sendMessage(serviceBusQueueUrl, 
									gson.toJson(message));							
						}
						
					}
				}
					
			}
		}


	}


	public static void main(String[] args) throws Exception {
		try {	
			final Injector injector = Guice.createInjector(new GuiceModule(args));
			injector.getInstance(SQSService.class).startup();
			try {
				injector.getInstance(KillEmrZombies.class).doWork();
			} finally {
				injector.getInstance(SQSService.class).shutdown();
			}
		} catch (Exception e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
}
