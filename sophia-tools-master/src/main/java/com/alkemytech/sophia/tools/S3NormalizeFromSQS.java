package com.alkemytech.sophia.tools;

import com.alkemytech.sophia.common.tools.StringTools;
import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.S3Utils;
import com.amazonaws.services.s3.model.S3Object;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.apache.commons.csv.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.math.BigDecimal;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class S3NormalizeFromSQS {

	private static final Logger logger = LoggerFactory.getLogger(S3NormalizeFromSQS.class);

	private static class GuiceModuleExtension extends GuiceModule {

		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
					.toInstance(new S3(configuration));
			bind(SQS.class)
					.toInstance(new SQS(configuration));
			// data source(s)
//			bind(DataSource.class)
//					.annotatedWith(Names.named("MCMDB"))
//					.to(McmdbDataSource.class)
//					.asEagerSingleton();
//			// data access object(s)
//			bind(McmdbDAO.class)
//					.asEagerSingleton();
			// other binding(s)
//			bind(MessageDeduplicator.class)
//					.to(McmdbMessageDeduplicator.class);
			// self
			bind(S3NormalizeFromSQS.class)
					.asEagerSingleton();
		}

	}


	public static void main(String[] args) {
		try {
			final S3NormalizeFromSQS instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/s3-normalize-from-sqs.properties"))
					.getInstance(S3NormalizeFromSQS.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Properties configuration;
	private final Charset charset;
	private final Gson gson;
	private final S3 s3;
	private final SQS sqs;


	@Inject
	protected S3NormalizeFromSQS(@Named("configuration") Properties configuration,
								 @Named("charset") Charset charset, Gson gson, S3 s3, SQS sqs) {
		super();
		this.configuration = configuration;
		this.charset = charset;
		this.gson = gson;
		this.s3 = s3;
		this.sqs = sqs;
//		this.dao = dao;
//		this.messageDeduplicator = messageDeduplicator;
	}


	public S3NormalizeFromSQS startup() throws IOException {
		s3.startup();
		sqs.startup();
		return this;
	}
	public S3NormalizeFromSQS shutdown() {
		sqs.shutdown();
		s3.shutdown();
		return this;
	}


	private S3NormalizeFromSQS process(String[] args) throws Exception {

		final int bindPort = Integer.parseInt(configuration
				.getProperty("s3_normalize_from_sqs.bind_port", configuration
						.getProperty("default.bind_port", "0")));


		String fakeMsg =  configuration.getProperty("fake_msg");
		if(fakeMsg!= null && fakeMsg.length()>0) {
			processMessage(GsonUtils.fromJson(fakeMsg, JsonObject.class));
		}else {

			// bind lock tcp port
			try (final ServerSocket socket = new ServerSocket(bindPort)) {
				logger.info("socket bound to {}", socket.getLocalSocketAddress());

				process();
			}
		}

		return this;
	}

	private void process() throws Exception {

		final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "s3_normalize_from_sqs.sqs");
		sqsMessagePump.pollingLoop(null, new SqsMessagePump.Consumer() {

			private JsonObject output;
			private JsonObject error;

			@Override
			public JsonObject getStartedMessagePayload(JsonObject message) {
				return SqsMessageHelper.formatContext();
			}

			@Override
			public boolean consumeMessage(JsonObject message) {

				try{
					output = processMessage(message);
					return true;
				}catch (Exception e ){
					logger.error("", e);
					error = SqsMessageHelper.formatError(e);
					return false;
				}


			}

			@Override
			public JsonObject getCompletedMessagePayload(JsonObject message) {
				return output;
			}

			@Override
			public JsonObject getFailedMessagePayload(JsonObject message) {
				return error;
			}

		});

	
	}

	private BigDecimal toBigDecimal(String value) {
		if (null == value) {
			return null;
		}
		try {
			return new BigDecimal(value.replace(',', '.'));
		} catch (NumberFormatException e) {
			return null;
		}
	}

//	private Long toLong(String value) {
//		if (null == value) {
//			return null;
//		}
//		try {
//			return new Long(value.replace(',', '.'));
//		} catch (NumberFormatException e) {
//			return null;
//		}
//	}




	private JsonObject processMessage(JsonObject message){
        final JsonObject output = new JsonObject();
//				final String srcBucket = configuration.getProperty("s3.src_bucket");
		final String dstBucket = configuration.getProperty("s3_normalize_from_sqs.dst_bucket");
		final String dstPrefix = configuration.getProperty("s3_normalize_from_sqs.dst_prefix");
		final String env = configuration.getProperty("default.environment");



		final int transactionIdIndex = Integer.parseInt(configuration.getProperty("input.column.transaction_id_column", "2")); // default to 2
		final int unitaryPriceIndex = Integer.parseInt(configuration.getProperty("input.column.unitary_price_column", "15")); // default to 15
		final int amountIndex = Integer.parseInt(configuration.getProperty("input.column.amount_column", "23")); // default to 23
		final String useAmountRegExp = configuration.getProperty("s3_normalize_from_sqs.line_value_model_regexp" );
		boolean useAmount= false;
		final BigDecimal totalValueScale = new BigDecimal(configuration.getProperty("s3-normalize-from-sqs.total_value_scale", "1"));
		final boolean uniqueTransactionIds = "true".equalsIgnoreCase(configuration.getProperty("s3-normalize-from-sqs.unique_transaction_ids", "true"));
		final boolean generateTransactionIds = "true".equalsIgnoreCase(configuration.getProperty("s3-normalize-from-sqs.generate_transaction_ids"));


		JsonObject body = GsonUtils.getAsJsonObject(message, "body");
		String filePath = GsonUtils.getAsString(body, "file_path");
		S3.Url fileV= new S3.Url(filePath);
		
//		List<S3.Url> fileVs = S3Utils.getS3UrlFromS3Message(message);
//		int numEle = 0;
//		for (S3.Url fileV : fileVs) {
//			numEle++;
		final File tempFile = new File(configuration.getProperty("temp_file"));
		if (null != tempFile.getParentFile()) {
			tempFile.getParentFile().mkdirs();
		}
		tempFile.delete();

		final File tempFileV = new File(configuration.getProperty("temp_file_V"));
		if (null != tempFileV.getParentFile()) {
			tempFileV.getParentFile().mkdirs();
		}
		tempFileV.delete();

		final String[] cols = new String[100];
		final AtomicLong tid = new AtomicLong(1L);

		S3Object s3VFile = s3.getObject(fileV);

		String bucket = s3VFile.getBucketName();
		String idDsr = s3VFile.getKey().substring(0,s3VFile.getKey().lastIndexOf("_V.csv")).substring(s3VFile.getKey().lastIndexOf("/")+1);
		String path = s3VFile.getKey().substring(0,s3VFile.getKey().lastIndexOf("/")+1);

		S3Object s3Gz = s3.getObject(new S3.Url(bucket,path+idDsr+".csv.gz"));

//		if(s3.doesObjectExist(new S3.Url(dstBucket,env + dstPrefix + s3VFile.getKey()))) {
//
//			throw new RuntimeException("File V già presente nella cartella di destinazione");
//		}

		Pattern pattern = Pattern.compile(useAmountRegExp);

		if(pattern.matcher(path+idDsr).matches()){ //if match use amout is trimestrali youtube
			useAmount=true;
		}
		

		logger.debug("doWork: s3summary {}", s3Gz);


		BigDecimal totalValue = new BigDecimal(0);
		BigDecimal totalSales = BigDecimal.ZERO;
		long totalRows = 0L;
		long totalRowsRemoved = 0L;
		final HashSet<String> tids = uniqueTransactionIds ? new HashSet<String>() : null;

		try (final InputStream in = new GZIPInputStream(s3Gz.getObjectContent());
			 final InputStreamReader reader = new InputStreamReader(in, StandardCharsets.UTF_8);
			 final CSVParser parser = new CSVParser(reader, CSVFormat.newFormat(';'))) {

			try (GZIPOutputStream out = new GZIPOutputStream(new FileOutputStream(tempFile, false));
				 final CSVPrinter printer = new CSVPrinter(new OutputStreamWriter(out,StandardCharsets.UTF_8),
						 CSVFormat.EXCEL.withDelimiter(';').withQuoteMode(QuoteMode.MINIMAL))) {

				// loop on csv row(s)
				for (CSVRecord record : parser) {
					totalRows++;
					logger.debug("doWork: total rows {} total sales {} total value {}", totalRows, totalSales, totalValue);
					int count = 0;
					if (record.size() > 30) {

						for (String col : record) {
							cols[count++] = col;
						}

						BigDecimal unitaryPrice = toBigDecimal(cols[unitaryPriceIndex]);
						BigDecimal amount = toBigDecimal(cols[amountIndex]);
						BigDecimal salesCount = toBigDecimal(cols[16]);

						logger.debug("doWork: amount {} (column {})", amount, amountIndex);
						logger.debug("doWork: unitaryPrice {} (column {})", unitaryPrice, unitaryPriceIndex);
						logger.debug("doWork: salesCount {}", salesCount);

						if ((null == unitaryPrice && !useAmount) ||
								(null == amount && useAmount) ||
								null == salesCount ||
								BigDecimal.ZERO.equals(salesCount) ||
								StringTools.isNullOrEmpty(cols[6]) ||
								StringTools.isNullOrEmpty(cols[7])) {

							logger.debug("doWork: skipped row {}", record);
							logger.debug("doWork: title {}", cols[6]);
							logger.debug("doWork: artists {}", cols[7]);
							totalRowsRemoved++;
							continue;
						}

						if (generateTransactionIds) {
							cols[transactionIdIndex] = Long.toString(tid.incrementAndGet());
						}

						totalSales = totalSales.add(salesCount);
						if (null == tids || tids.add(cols[transactionIdIndex])) {
							if (useAmount) {
								totalValue = totalValue.add(amount);
							} else {
								totalValue = totalValue.add(unitaryPrice.multiply(salesCount));
							}
						}

						for (int i = 0; i < count; i++) {
							printer.print(cols[i]);
						}
						printer.println();

					}else totalRowsRemoved++;
				}
			}
		}catch (Exception e) {
			throw new RuntimeException(e);
		}
		// scale total value
		totalValue = totalValue.multiply(totalValueScale);

		logger.debug("doWork: temp file size {}", tempFile.length());


		try (final InputStream in = s3VFile.getObjectContent();
			 final InputStreamReader reader = new InputStreamReader(in, StandardCharsets.UTF_8);
			 final CSVParser parser = new CSVParser(reader, CSVFormat.newFormat(';'))) {

			try (FileWriter out = new FileWriter(tempFileV, false);
				 final CSVPrinter printer = new CSVPrinter(out,
						 CSVFormat.EXCEL.withDelimiter(';').withQuoteMode(QuoteMode.MINIMAL))) {

				// loop on csv row(s)
				for (CSVRecord record : parser) {

					BigDecimal totalValueV = toBigDecimal(record.get(0));
					String totalSubscriptionsV = record.get(1);
					String totalSalesV = record.get(2);

					logger.debug("doWork: totalValue {} --> {}", totalValueV, totalValue);
					logger.debug("doWork: totalSales {} --> {}", totalSalesV, totalSales);

					if (totalValue.compareTo(BigDecimal.ZERO) <= 0) {
						totalValue = totalValueV;
						logger.debug("doWork: using totalValue from _V {}", totalValue);
					}

					printer.print(totalValue);
					printer.print(totalSubscriptionsV);
					printer.print(totalSales);
					printer.println();

					break;
				}
			}
		}catch (Exception e){ throw new RuntimeException(e.getMessage());}



		logger.debug("doWork: temp file size _V {}", tempFileV.length());

		s3.upload(new S3.Url(dstBucket,env + dstPrefix + s3Gz.getKey() ), tempFile);
		s3.upload(new S3.Url(dstBucket,env + dstPrefix + s3VFile.getKey() ), tempFileV);

		tempFile.delete();
		tempFileV.delete();

		output.addProperty("idDsr",idDsr);
		output.addProperty("dsr_file",env + dstPrefix + s3Gz.getKey());
		output.addProperty("V_file",env + dstPrefix + s3VFile.getKey());
		output.addProperty("removed_rows",totalRowsRemoved);
		output.addProperty("total_rows",totalRows);

//		}

//		output.addProperty("totalFiles" ,fileVs.size());

		return output;


	}


}
