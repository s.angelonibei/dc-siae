package com.alkemytech.sophia.tools;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Properties;
import java.util.zip.GZIPOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.common.s3.S3Service;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class S3RenameGzip {

	private static final Logger logger = LoggerFactory.getLogger(S3RenameGzip.class);

	private final Properties configuration;
	private final S3Service s3Service;

	@Inject
	protected S3RenameGzip(@Named("configuration") Properties configuration,
			S3Service s3Service) {
		super();
		this.configuration = configuration;
		this.s3Service = s3Service;
	}
	
	public void doWork() throws Exception {
			
		final String srcBucket = configuration.getProperty("s3.src_bucket");
		final String srcFolder = configuration.getProperty("s3.src_folder");
		final String srcSuffix = configuration.getProperty("s3.src_suffix");
		final String srcSuffixV = configuration.getProperty("s3.src_suffix_V");

		final String dstBucket = configuration.getProperty("s3.dst_bucket");
		final String dstFolder = configuration.getProperty("s3.dst_folder");
		final String dstSuffix = configuration.getProperty("s3.dst_suffix");
		final String dstSuffixV = configuration.getProperty("s3.dst_suffix_V");
		
		final File file = new File(configuration.getProperty("temp_file"));
		if (null != file.getParentFile()) {
			file.getParentFile().mkdirs();
		}
		file.delete();
		
		List<S3ObjectSummary> srcListing = s3Service.listObjects(srcBucket, srcFolder);
		for (S3ObjectSummary s3summary : srcListing) {
			
			logger.debug("doWork: s3summary {}", s3summary);
			
			String key = s3summary.getKey();
			if (key.endsWith(srcSuffix)) {
				key = key.substring(srcFolder.length(), key.lastIndexOf(srcSuffix));
				logger.debug("doWork: key {}", key);
				logger.debug("doWork: s3summary size {}", s3summary.getSize());
				try (GZIPOutputStream out = new GZIPOutputStream(new FileOutputStream(file, false))) {
					s3Service.download(s3summary.getBucketName(), s3summary.getKey(), out);
				}
				logger.debug("doWork: temp file size {}", file.length());
				s3Service.upload(dstBucket, dstFolder + key + dstSuffix, file);
				file.delete();
			} else if (key.endsWith(srcSuffixV)) {
				key = key.substring(srcFolder.length(), key.lastIndexOf(srcSuffixV));
				logger.debug("doWork: key {}", key);
				logger.debug("doWork: s3summary size {}", s3summary.getSize());
				s3Service.download(s3summary.getBucketName(), s3summary.getKey(), file);
				logger.debug("doWork: temp file size {}", file.length());
				s3Service.upload(dstBucket, dstFolder + key + dstSuffixV, file);
				file.delete();
			}
			
		}
		
		
	}

	public static void main(String[] args) throws Exception {
		try {	
			final Injector injector = Guice.createInjector(new GuiceModule(args));
			//injector.getInstance(SQSService.class).startup();
			injector.getInstance(S3Service.class).startup();
			try {
				injector.getInstance(S3RenameGzip.class).doWork();
			} finally {
				//injector.getInstance(SQSService.class).shutdown();
				injector.getInstance(S3Service.class).shutdown();
			}
		} catch (Exception e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
}
