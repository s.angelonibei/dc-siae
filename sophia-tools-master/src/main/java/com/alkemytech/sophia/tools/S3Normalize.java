package com.alkemytech.sophia.tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.common.tools.StringTools;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class S3Normalize {

	private static final Logger logger = LoggerFactory.getLogger(S3Normalize.class);

	private final Properties configuration;
	private final S3Service s3Service;

	@Inject
	protected S3Normalize(@Named("configuration") Properties configuration,
			S3Service s3Service) {
		super();
		this.configuration = configuration;
		this.s3Service = s3Service;
	}
	
	private BigDecimal toBigDecimal(String value) {
		if (null == value) {
			return null;
		}
		try {
			return new BigDecimal(value.replace(',', '.'));
		} catch (NumberFormatException e) {
			return null;
		}
	}
	
//	private Long toLong(String value) {
//		if (null == value) {
//			return null;
//		}
//		try {
//			return new Long(value.replace(',', '.'));
//		} catch (NumberFormatException e) {
//			return null;
//		}
//	}
	
	public void doWork() throws Exception {
			
		final String srcBucket = configuration.getProperty("s3.src_bucket");
		final String srcFolder = configuration.getProperty("s3.src_folder");
		final String srcSuffix = configuration.getProperty("s3.src_suffix");
		final String srcSuffixV = configuration.getProperty("s3.src_suffix_V");

		final String dstBucket = configuration.getProperty("s3.dst_bucket");
		final String dstFolder = configuration.getProperty("s3.dst_folder");
		final String dstSuffix = configuration.getProperty("s3.dst_suffix");
		final String dstSuffixV = configuration.getProperty("s3.dst_suffix_V");
		
		final int transactionIdIndex = Integer.parseInt(configuration.getProperty("transaction_id_column", "2")); // default to 2
		final int unitaryPriceIndex = Integer.parseInt(configuration.getProperty("unitary_price_column", "15")); // default to 15
		final int amountIndex = Integer.parseInt(configuration.getProperty("amount_column", "23")); // default to 23
		final boolean useAmount = "amount".equalsIgnoreCase(configuration.getProperty("line_value_model", "unitary_price"));
		final BigDecimal totalValueScale = new BigDecimal(configuration.getProperty("total_value_scale", "1"));
		final boolean uniqueTransactionIds = "true".equalsIgnoreCase(configuration.getProperty("unique_transaction_ids", "true"));
		final boolean generateTransactionIds = "true".equalsIgnoreCase(configuration.getProperty("generate_transaction_ids"));
		
		final File tempFile = new File(configuration.getProperty("temp_file"));
		if (null != tempFile.getParentFile()) {
			tempFile.getParentFile().mkdirs();
		}
		tempFile.delete();

		final File tempFileV = new File(configuration.getProperty("temp_file_V"));
		if (null != tempFileV.getParentFile()) {
			tempFileV.getParentFile().mkdirs();
		}
		tempFileV.delete();
		
		final String[] cols = new String[100];
		final AtomicLong tid = new AtomicLong(1L);
		
		List<S3ObjectSummary> srcListing = s3Service.listObjects(srcBucket, srcFolder);
		for (S3ObjectSummary s3Summary : srcListing) {
			
			logger.debug("doWork: s3summary {}", s3Summary);
			
			String key = s3Summary.getKey();
			if (key.endsWith(srcSuffix)) {
				key = key.substring(srcFolder.length(), key.lastIndexOf(srcSuffix));
				logger.debug("doWork: key {}", key);
				logger.debug("doWork: s3summary size {}", s3Summary.getSize());
				
				BigDecimal totalValue = new BigDecimal(0);
				BigDecimal totalSales = BigDecimal.ZERO;
				long totalRows = 0L;
				final HashSet<String> tids = uniqueTransactionIds ? new HashSet<String>() : null;
				
				S3Object s3Object = s3Service.getObject(s3Summary.getBucketName(), s3Summary.getKey());
				try (final InputStream in = new GZIPInputStream(s3Object.getObjectContent());
						final InputStreamReader reader = new InputStreamReader(in, StandardCharsets.UTF_8);
						final CSVParser parser = new CSVParser(reader, CSVFormat.newFormat(';'))) {											

					try (GZIPOutputStream out = new GZIPOutputStream(new FileOutputStream(tempFile, false));
							final CSVPrinter printer = new CSVPrinter(new OutputStreamWriter(out,StandardCharsets.UTF_8),
								CSVFormat.EXCEL.withDelimiter(';').withQuoteMode(QuoteMode.MINIMAL))) {
						
						// loop on csv row(s)
						for (CSVRecord record : parser) {
							totalRows ++;
							logger.debug("doWork: total rows {} total sales {} total value {}", totalRows, totalSales, totalValue);
							int count = 0;
							if (record.size() > 30) {
								
								for (String col : record) {
									cols[count ++] = col;
								}

								BigDecimal unitaryPrice = toBigDecimal(cols[unitaryPriceIndex]);
								BigDecimal amount = toBigDecimal(cols[amountIndex]);
								BigDecimal salesCount = toBigDecimal(cols[16]);

								logger.debug("doWork: amount {} (column {})", amount, amountIndex);
								logger.debug("doWork: unitaryPrice {} (column {})", unitaryPrice, unitaryPriceIndex);
								logger.debug("doWork: salesCount {}", salesCount);

								if (null == unitaryPrice ||
										null == salesCount ||
										BigDecimal.ZERO == salesCount ||
										StringTools.isNullOrEmpty(cols[6]) ||
										StringTools.isNullOrEmpty(cols[7])) {
									
									logger.debug("doWork: skipped row {}", record);
									logger.debug("doWork: title {}", cols[6]);
									logger.debug("doWork: artists {}", cols[7]);

									continue;
								}

								if (generateTransactionIds) {
									cols[transactionIdIndex] = Long.toString(tid.incrementAndGet());									
								}

								totalSales = totalSales.add(salesCount);
								if (null == tids || tids.add(cols[transactionIdIndex])) {
									if (useAmount) {
										totalValue = totalValue.add(amount);										
									} else {
										totalValue = totalValue.add(unitaryPrice.multiply(salesCount));
									}
								}

								for (int i = 0; i < count; i ++) {
									printer.print(cols[i]);						
								}
								printer.println();
								
							}	
						}
					}
				}
				
				// scale total value
				totalValue = totalValue.multiply(totalValueScale);

				logger.debug("doWork: temp file size {}", tempFile.length());

				s3Object = s3Service.getObject(s3Summary.getBucketName(), srcFolder + key + srcSuffixV);
				try (final InputStream in = s3Object.getObjectContent();
						final InputStreamReader reader = new InputStreamReader(in, StandardCharsets.UTF_8);
						final CSVParser parser = new CSVParser(reader, CSVFormat.newFormat(';'))) {											

					try (FileWriter out = new FileWriter(tempFileV, false);
							final CSVPrinter printer = new CSVPrinter(out,
								CSVFormat.EXCEL.withDelimiter(';').withQuoteMode(QuoteMode.MINIMAL))) {
						
						// loop on csv row(s)
						for (CSVRecord record : parser) {
							
							BigDecimal totalValueV = toBigDecimal(record.get(0));
							String totalSubscriptionsV = record.get(1);
							String totalSalesV = record.get(2);

							logger.debug("doWork: totalValue {} --> {}", totalValueV, totalValue);
							logger.debug("doWork: totalSales {} --> {}", totalSalesV, totalSales);

							if (totalValue.compareTo(BigDecimal.ZERO) <= 0) {
								totalValue = totalValueV;
								logger.debug("doWork: using totalValue from _V {}", totalValue);
							}
							
							printer.print(totalValue);						
							printer.print(totalSubscriptionsV);						
							printer.print(totalSales);						
							printer.println();
							
							break;
						}
					}
				}


				
				logger.debug("doWork: temp file size _V {}", tempFileV.length());
				
				s3Service.upload(dstBucket, dstFolder + key + dstSuffix, tempFile);
				s3Service.upload(dstBucket, dstFolder + key + dstSuffixV, tempFileV);
				
				tempFile.delete();
				tempFileV.delete();
			}
			
		}
		
		
	}

	public static void main(String[] args) throws Exception {
		try {	
			final Injector injector = Guice.createInjector(new GuiceModule(args));
			//injector.getInstance(SQSService.class).startup();
			injector.getInstance(S3Service.class).startup();
			try {
				injector.getInstance(S3Normalize.class).doWork();
			} finally {
				//injector.getInstance(SQSService.class).shutdown();
				injector.getInstance(S3Service.class).shutdown();
			}
		} catch (Exception e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
}
