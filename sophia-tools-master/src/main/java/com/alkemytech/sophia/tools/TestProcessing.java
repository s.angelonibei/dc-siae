package com.alkemytech.sophia.tools;

import org.anarres.parallelgzip.ParallelGZIPInputStream;
import org.anarres.parallelgzip.ParallelGZIPOutputStream;
import org.apache.commons.csv.*;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.Channel;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class TestProcessing {

    final static String input = "/home/marco/Report_BMAT_YouTube_AdSupport-music-longtail_2016-Q4_ES_1of1_20170105T120627.csv.gz";
    final static String output = "/home/marco/OUT1_Report_BMAT_YouTube_AdSupport-music-longtail_2016-Q4_ES_1of1_20170105T120627.csv.gz";

    public static void main(String[] args) throws Exception {
        final long startTime = System.nanoTime();
        mode1();
//        mode2();
        final long duration = System.nanoTime() - startTime;
        System.out.println("Elapsed time: " +duration/1000000000 + " sec.");


    }


    private static void mode1() throws Exception {

        final String[] cols = new String[100];

        int totalRows = 0;
        try (final InputStream in = new GZIPInputStream(new FileInputStream(input));
             final BufferedReader reader = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
             final CSVParser parser = new CSVParser(reader, CSVFormat.newFormat(';')))
        {

            try (GZIPOutputStream out = new GZIPOutputStream(new FileOutputStream(output));
                 final CSVPrinter printer = new CSVPrinter(new OutputStreamWriter(out,StandardCharsets.UTF_8),
                         CSVFormat.EXCEL.withDelimiter(';').withQuoteMode(QuoteMode.MINIMAL))) {


                for (CSVRecord record : parser) {
                    int count = 0;

                    totalRows++;
                    if(totalRows % 10000 == 0)
                        System.out.print(".");
                    if(totalRows % 1000000 == 0){
                        System.out.println();

                    }

                    for (String col : record) {
                        cols[count++] = col;
                    }


                    for (int i = 0; i < count; i++) {
                        printer.print(cols[i]);
                    }
                    printer.println();
                }
            }
        }
        System.out.println();
        System.out.println("TOT SIZE: "+totalRows);

    }


}


