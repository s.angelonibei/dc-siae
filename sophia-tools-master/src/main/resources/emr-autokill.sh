#!/bin/sh

ENVNAME=dev
APPNAME=emr-autokill
VERSION=1.0.1

JARNAME=sophia-tools-$VERSION-jar-with-dependencies.jar
CFGNAME=$APPNAME.properties
RUNNAME=logs/crontab.log
HOME=/var/local/sophia/$ENVNAME
#OUTPUT=/dev/null
OUTPUT=$HOME/logs/$APPNAME.out

nohup java -cp $HOME/$JARNAME com.alkemytech.sophia.tools.KillEmrZombies $HOME/$CFGNAME 2>&1 >> $OUTPUT &

echo "$(date +%Y%m%d%H%M%S) $ENVNAME $APPNAME" >> $HOME/$RUNNAME