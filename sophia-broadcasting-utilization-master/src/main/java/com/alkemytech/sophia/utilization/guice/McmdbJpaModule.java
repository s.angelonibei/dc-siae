package com.alkemytech.sophia.utilization.guice;

import com.alkemytech.sophia.broadcasting.service.*;
import com.alkemytech.sophia.broadcasting.service.interfaces.*;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.utilization.EmailNotification;
import com.alkemytech.sophia.utilization.KpiEvaluator;
import com.alkemytech.sophia.utilization.Utilizer;
import com.alkemytech.sophia.utilization.job.FileJob;
import com.google.inject.Provider;
import com.google.inject.persist.PersistService;
import com.google.inject.persist.jpa.JpaPersistModule;

import javax.persistence.EntityManager;

public class McmdbJpaModule extends AbstractJpaModule {
    public static final String UNIT_NAME = "mcmdb";

    public McmdbJpaModule(String[] args) {
        super(args, UNIT_NAME);
    }

    @Override
    protected void configure() {
        final JpaPersistModule jpaPersistModule = new JpaPersistModule(UNIT_NAME);
        jpaPersistModule.properties(loadConfig());
        install(jpaPersistModule);
        final Provider<EntityManager> entityManagerProvider = binder().getProvider(EntityManager.class);
        final DecodeService decodeService = new DecodeServiceImpl(entityManagerProvider);
        final BdcCanaliService canaleService = new BdcCanaliServiceImpl(entityManagerProvider);
        final BdcIndicatoreDTOService indicatoreDTOService = new BdcIndicatoreDTOServiceImpl(entityManagerProvider, canaleService);
        final NormalizedFileService normalizedFileService = new NormalizedServiceImpl(entityManagerProvider, decodeService, canaleService);

        //BIND
        bind(BroadcasterConfigService.class).toInstance(new BroadcasterConfigServiceImpl(entityManagerProvider));
        bind(InformationFileEntityService.class).toInstance(new InformationFileEntityServiceImpl(entityManagerProvider));
        bind(DecodeService.class).toInstance(decodeService);
        bind(BdcCanaliService.class).toInstance(canaleService);
        bind(BdcIndicatoreDTOService.class).toInstance(indicatoreDTOService);
        bind(NormalizedFileService.class).toInstance(normalizedFileService);
        bind(UtilizationService.class).toInstance(new UtilizationServiceImpl(entityManagerProvider, normalizedFileService, canaleService,indicatoreDTOService));
        bind(S3Service.class).asEagerSingleton();
        bind(Utilizer.class).asEagerSingleton();
        bind(EmailNotification.class).asEagerSingleton();
        bind(KpiEvaluator.class).asEagerSingleton();

        //EXPOSE
        expose(BroadcasterConfigService.class);
        expose(InformationFileEntityService.class);
        expose(DecodeService.class);
        expose(NormalizedFileService.class);
        expose(UtilizationService.class);
        expose(S3Service.class);
        expose(PersistService.class);
        expose(Utilizer.class);
        expose(EmailNotification.class);
        expose(KpiEvaluator.class);

        bind(FileJob.class).asEagerSingleton();
        expose(FileJob.class);
    }

}
