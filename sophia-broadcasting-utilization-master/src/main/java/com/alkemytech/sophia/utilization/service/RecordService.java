package com.alkemytech.sophia.utilization.service;

import com.alkemytech.sophia.broadcasting.dto.BdcConfigDTO;
import com.alkemytech.sophia.broadcasting.dto.NormalizedOrderDTO;
import com.alkemytech.sophia.broadcasting.dto.SearchBroadcasterConfigDTO;
import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;
import com.alkemytech.sophia.broadcasting.model.BdcInformationFileEntity;
import com.alkemytech.sophia.broadcasting.model.BdcNormalizedFile;
import com.alkemytech.sophia.broadcasting.model.TipoBroadcaster;
import com.alkemytech.sophia.broadcasting.service.interfaces.BroadcasterConfigService;
import com.alkemytech.sophia.broadcasting.service.interfaces.UtilizationService;
import com.alkemytech.sophia.common.mapper.TemplateMapper;
import com.alkemytech.sophia.common.s3.S3Service;
import com.amazonaws.services.s3.AmazonS3URI;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import javafx.scene.chart.Chart;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Alessandro Russo on 14/12/2017.
 */
@Singleton
public class RecordService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private S3Service s3Service;
    private BroadcasterConfigService broadcasterConfigService;
    private UtilizationService utilizationService;

    private Boolean activeOrderFileNormalize;


    @Inject
    public RecordService(S3Service s3Service, BroadcasterConfigService broadcasterConfigService, UtilizationService utilizationService) {
        this.s3Service = s3Service;
        this.broadcasterConfigService = broadcasterConfigService;
        this.utilizationService = utilizationService;
    }

    public AmazonS3URI reserveSingleFile(String filePath, String suffix){
        if( StringUtils.isNotEmpty( filePath ) ){
            AmazonS3URI amazonS3URI = new AmazonS3URI(filePath);
            if( !s3Service.lockFile(amazonS3URI.getBucket(), amazonS3URI.getKey(), suffix) ){
                logger.debug("File {} preaviously reserved by other process", amazonS3URI.getKey());
                return null;
            }
            logger.debug("Reserved file at {}", filePath);
            return amazonS3URI;
        }
        return null;
    }

    public void releaseSingleFile(String bucket, String key, String suffix){
        if(s3Service.doesObjectExist(bucket, key+suffix)){
            s3Service.delete(bucket, key+suffix);
            logger.debug("Released file at {}", bucket+key);
        }
        logger.debug("Doesn't exist file at {}", bucket+key+suffix);
    }

    public BdcConfigDTO retrieveConfig(BdcNormalizedFile normFile) {
        BdcBroadcasters broadcaster = normFile.getBroadcaster();
        if(broadcaster != null) {
            SearchBroadcasterConfigDTO searchBroadcasterConfigDTO = new SearchBroadcasterConfigDTO(broadcaster.getId(),  normFile.getCanale()!=null?normFile.getCanale().getId():null);
            searchBroadcasterConfigDTO.setActiveDate(null);
            return broadcasterConfigService.findActive(searchBroadcasterConfigDTO);
        }
        return null;
    }

    public List<Object> parseFile(BdcConfigDTO config, BdcNormalizedFile currentFile, Boolean activeOrderFileNormalize){
        this.activeOrderFileNormalize = activeOrderFileNormalize;
        return parseFile(config, currentFile);
    }

    public List<Object> parseFile(BdcConfigDTO config, BdcNormalizedFile currentFile){
        try{
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            AmazonS3URI amazonS3URI = new AmazonS3URI(currentFile.getFileBucket());
            TemplateMapper mapper = new TemplateMapper();
            try{
                s3Service.download(amazonS3URI.getBucket(), amazonS3URI.getKey(), bos);

                // ordinamento file

                if (activeOrderFileNormalize && currentFile.getBroadcaster().getTipo_broadcaster().equals(TipoBroadcaster.TELEVISIONE)) {
                    ByteArrayOutputStream origBos = bos;
                    logger.info("Ordinamento File Normalizzato in corso...");

                   /*
                     SERVE PER CAPIRE LA DIFFERENZA TRA IL FILE ORDINATO E QUELLO NON ORDINATO

                    ByteArrayOutputStream byteArrayOutputStream = bos;
                    try(OutputStream outputStream = new FileOutputStream("bosBefore.csv")) {
                        byteArrayOutputStream.writeTo(outputStream);
                    }
                    */

                    try {

                        bos = ordinaFileNormalizzato(bos);
                        if (bos == null)
                        {
                            bos = origBos;
                            logger.info("Non è stato possibile ordinare il file. Ripristinato file normalizzato NON ordinato");
                        }
                    }catch(Exception e )
                    {
                        logger.error("Errore ordinamento file:" + e.getMessage());
                        logger.info("Errore imprevisto: Ripristinato file normalizzato NON ordinato");
                        bos = origBos;
                    }


                   /*  SERVE SOLO PER CAPIRE COME HA ORDINATO IL FILE

                     byteArrayOutputStream = bos;
                    try(OutputStream outputStream = new FileOutputStream("bosAfter.csv")) {
                        byteArrayOutputStream.writeTo(outputStream);
                    }
                   */

                    logger.info("Fine ordinamento");
                }

                return mapper.parseFile(config.getWriteConfigPath(), config.getWriteConfigStream(), bos, s3Service,Charset.defaultCharset());
            }
            finally {
                bos.flush();
                bos.close();
            }
        }
        catch (Exception e ){
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        return null;
    }

    public Map<Integer, Set<String>> saveEntity(List<Object> rows, BdcNormalizedFile normalizedFile, Integer overlapLimit) throws IOException {
        if( TipoBroadcaster.RADIO.equals(normalizedFile.getBroadcaster().getTipo_broadcaster()) ){
            return utilizationService.saveRadioEntityList(rows, normalizedFile, overlapLimit);
        }
        return utilizationService.saveTvEntityList( rows, normalizedFile);
    }

    /**
     *
     * @param kpiToEvaluate Map<id_canale, Set<anno-mese>> es: <58, (1, 12, 3)>
     * @return true se sono stati calcolati tutti gli indicatori, false se c'è stato qualche problema
     */
    public boolean evaluateKpi(Map<Integer, Set<String>> kpiToEvaluate) {
        boolean allKpiEvalueted = true;
        if(kpiToEvaluate != null){
            Set<Integer> setCanali = kpiToEvaluate.keySet();
            for(Integer idCanale : setCanali){
                Set<String> setMeseAnno = kpiToEvaluate.get(idCanale);
                for(String meseAnno : setMeseAnno){
                    Integer anno = extractAnnoFromMeseAnno(meseAnno);
                    Integer mese = extractMeseFromMeseAnno(meseAnno);
                    if(anno != null && mese != null){
                        logger.info("@@@@ INFO evaluateKpi @@@@ -> Inizio il calcolo KPI per la coppia [canale, mese-anno]: [" + idCanale + ", " + meseAnno + "]");
                        if(!utilizationService.evaluateKPI(idCanale, anno, mese)){
                            allKpiEvalueted = false;
                            logger.error("#### ERRORE evaluateKpi #### -> Si è verificato un errore durante il calcolo dei KPI per la coppia [canale, mese-anno]: [" + idCanale + ", " + meseAnno + "]");
                        }
                        logger.info("@@@@ INFO evaluateKpi @@@@ -> Terminato calcolo KPI per la coppia [canale, mese-anno]: [" + idCanale + ", " + meseAnno + "]");
                    }else{
                        logger.error("#### ERRORE evaluateKpi #### -> Richiesto il calcolo KPI per una coppia [canale, mese-anno] non valido: [" + idCanale + ", " + meseAnno + "]");
                        allKpiEvalueted = false;
                    }
                }
            }
        }
        return allKpiEvalueted;
    }

    private Integer extractAnnoFromMeseAnno(String meseAnno) {
        Integer iAnno = null;
        if(meseAnno != null){
            try{
                String anno = meseAnno.substring(0, meseAnno.lastIndexOf("-"));
                iAnno = Integer.parseInt(anno);
            }catch(Exception e){
                return null;
            }
        }
        return iAnno;
    }

    private Integer extractMeseFromMeseAnno(String meseAnno) {
        Integer iMese = null;
        if(meseAnno != null){
            try{
                String mese = meseAnno.substring(meseAnno.lastIndexOf("-")+1);
                iMese = Integer.parseInt(mese);
            }catch(Exception e){
                return null;
            }
        }
        return iMese;
    }
    private  ByteArrayOutputStream ordinaFileNormalizzato(ByteArrayOutputStream boss) {
        //SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        CsvMapper m = new CsvMapper();
        //m.setDateFormat(df);
        CsvSchema schema = m.schemaFor(NormalizedOrderDTO.class)
                .withHeader()
                .withLineSeparator("\n")
                .withColumnSeparator(';')
                //.withoutQuoteChar() tolto perche non riesce a convertire una stringa che ha come separatore il ;
                ;

        //logger.info("BOS size input" + boss.size());
        InputStreamReader fileStreamReader = new InputStreamReader(new ByteArrayInputStream(boss.toByteArray()));



        try {
            MappingIterator<NormalizedOrderDTO> r = m.readerWithSchemaFor(NormalizedOrderDTO.class).with(schema).readValues(fileStreamReader);
            List list = r.readAll();
            Collections.sort(list, Comparator.comparing(NormalizedOrderDTO::getDataInizioTrasmissione,  Comparator.nullsFirst(Comparator.naturalOrder()))
                    .thenComparing(NormalizedOrderDTO::getOrarioInizioTrasmissione, Comparator.nullsFirst(Comparator.naturalOrder()))
                    .thenComparing(NormalizedOrderDTO::getOrarioFineTrasmissione, Comparator.nullsFirst(Comparator.naturalOrder()))
                    .thenComparing(NormalizedOrderDTO::getTitoloTrasmissione)
                    .thenComparing(NormalizedOrderDTO::getCategoriaUso)
                    );

            fileStreamReader.close();

            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            ObjectWriter myObjectWriter = m.writer(schema);
            myObjectWriter.writeValue(bos, list);
            //logger.info("list size : " + list.size());
            //logger.info("BOS size output" + bos.size());

            return bos;

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error:" + e.getMessage());
        }
        return null;
    }
}
