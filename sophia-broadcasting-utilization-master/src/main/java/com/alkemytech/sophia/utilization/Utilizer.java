package com.alkemytech.sophia.utilization;

import com.alkemytech.sophia.broadcasting.dto.BdcConfigDTO;
import com.alkemytech.sophia.broadcasting.enums.Stato;
import com.alkemytech.sophia.broadcasting.model.BdcInformationFileEntity;
import com.alkemytech.sophia.broadcasting.model.BdcNormalizedFile;
import com.alkemytech.sophia.broadcasting.service.interfaces.DecodeService;
import com.alkemytech.sophia.broadcasting.service.interfaces.NormalizedFileService;
import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.utilization.guice.McmdbJpaModule;
import com.alkemytech.sophia.utilization.service.RecordService;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.util.CollectionUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;
import com.google.inject.persist.PersistService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.ServerSocket;
import java.util.*;

public class Utilizer {

    private static final Logger logger = LoggerFactory.getLogger(Utilizer.class);

    private final Integer utilizationListenPort;
    private RecordService recordService;
    private NormalizedFileService normalizedFileService;
    private Properties configuration;

    @Inject
    public Utilizer(
                    @Named("configuration") Properties configuration,
                    @Named("utilizationListenPort") Integer utilizationListenPort,
                    RecordService recordService,
                    NormalizedFileService normalizedFileService
    )
    {
        this.configuration = configuration;
        this.utilizationListenPort = utilizationListenPort;
        this.recordService = recordService;
        this.normalizedFileService = normalizedFileService;
    }

    public static void main(String[] args) {
        try {
            final  Injector injector = Guice.createInjector(new McmdbJpaModule(args));
            try{
                //Setup services
                injector.getInstance(PersistService.class).start();
                injector.getInstance(S3Service.class).startup();
                injector.getInstance(DecodeService.class).start();
                Utilizer utilizer = injector.getInstance(Utilizer.class);

                //Run business logic
                logger.info("START PROCESS");
                utilizer.process();
                logger.info("END PROCESS");
            }
            finally {
                injector.getInstance(PersistService.class).stop();
                injector.getInstance(S3Service.class).shutdown();
            }
        }
        catch (Exception e){
            logger.error("main", e);
        }
        finally {
            System.exit(0);
        }
    }

    /**
     * Process normalized File and save utilizations
     * @throws Exception
     */
    private void process() throws Exception {
        try(final ServerSocket socket = new ServerSocket( utilizationListenPort );){
            Gson gson = new GsonBuilder().create();
            logger.info("PERSIST REPORT UTILIZATIONS TASK START: listening on {}", socket.getLocalSocketAddress());
            Integer overlapLimit = Integer.parseInt(configuration.getProperty("overlap.limit"));
            //logger.debug("executing sqlConfigurazione {}", overlapLimit);
            //Retrieve files to process
            List<BdcNormalizedFile> fileToProcessList = normalizedFileService.getNormalizedFileList(Stato.VALIDATO);
            if( !CollectionUtils.isNullOrEmpty(fileToProcessList) ){
                logger.debug("Retrieved {} files to process ", fileToProcessList.size());
                for(BdcNormalizedFile currentFile : fileToProcessList){
                    try{
                        logger.info("----------------------------------------");
                        logger.info("START Working on {}", currentFile.getFileBucket());
                        logger.info("----------------------------------------");
                        AmazonS3URI currentFileUri  = recordService.reserveSingleFile(currentFile.getFileBucket(), Constants.LOCK_SUFFIX);
                        if( currentFileUri != null ){
                            try{
                                //Load valid Broadcaster configuration
                                BdcConfigDTO broadcasterConfig = recordService.retrieveConfig(currentFile);
                                if(broadcasterConfig == null){
                                    logger.error("ERROR - No valid configuration for file with id {} and name {}", currentFile.getId(), currentFile.getFileName() );
                                    recordService.releaseSingleFile(currentFileUri.getBucket(), currentFileUri.getKey(), Constants.LOCK_SUFFIX);
                                    continue;
                                }



                                //Parsing normalized Report via BEANIO


                                Boolean ordinamento =  Boolean.parseBoolean(configuration.getProperty("activeOrderFileNormalize"));

                                List<Object> parsedRows = recordService.parseFile(broadcasterConfig,currentFile, ordinamento);
                                if( !CollectionUtils.isNullOrEmpty(parsedRows) ){
                                    logger.info("File parsed successfully!");
                                    //Create and persist DB Entities
                                    Map<Integer, Set<String>> kpiToEvaluate = recordService.saveEntity(parsedRows, currentFile, overlapLimit);
                                    if(kpiToEvaluate != null){
                                        logger.info("Records saved successfully!");
                                        for(BdcInformationFileEntity originalReport : currentFile.getOriginalReport()){
                                            originalReport.setStato(Stato.ELABORATO);
                                            originalReport.setDataProcessamento(new Date());
                                            originalReport.setAmbitoKpi(gson.toJson(kpiToEvaluate));
                                            normalizedFileService.save(currentFile);
                                        }




                                        //calculate KPI
/*
                                        if(recordService.evaluateKpi(kpiToEvaluate)){
                                            logger.info("----------------------------------------");
                                            logger.info("SUCCESS - {}", currentFile.getFileBucket());
                                            logger.info("----------------------------------------");
                                        }
                                        else{
                                            logger.error("ERROR - Evaluating KPI error" );
                                            //Keep file locking
                                        }
*/
                                    }
                                    else{
                                        for(BdcInformationFileEntity originalReport : currentFile.getOriginalReport()){
                                            originalReport.setStato(Stato.ERRORE);
                                            originalReport.setDataProcessamento(new Date());
                                            normalizedFileService.save(currentFile);
                                        }
                                        logger.error("ERROR - Peristing utilization error" );
                                        recordService.releaseSingleFile(currentFileUri.getBucket(), currentFileUri.getKey(), Constants.LOCK_SUFFIX);
                                    }
                                }
                                else{
                                    for(BdcInformationFileEntity originalReport : currentFile.getOriginalReport()){
                                        originalReport.setStato(Stato.ERRORE);
                                        originalReport.setDataProcessamento(new Date());
                                        normalizedFileService.save(currentFile);
                                    }
                                    logger.error("ERROR - No Persist Data from report {}", currentFile.getFileBucket());
                                    recordService.releaseSingleFile(currentFileUri.getBucket(), currentFileUri.getKey(), Constants.LOCK_SUFFIX);
                                }
                            }
                            finally {
                                recordService.releaseSingleFile(currentFileUri.getBucket(), currentFileUri.getKey(), Constants.LOCK_SUFFIX);
                            }
                        }
                        else{
                            logger.info("File {} already locked by other process!");
                        }
                    }
                    catch (Exception e){
                        for(BdcInformationFileEntity originalReport : currentFile.getOriginalReport()){
                            originalReport.setStato(Stato.ERRORE);
                            originalReport.setDataProcessamento(new Date());
                            normalizedFileService.save(currentFile);
                        }
                        logger.error("-- UTILIZZATIONS TASK ERROR {}", e.getMessage());
                        continue;
                    }
                }
            }
            else {
                logger.info("No Report with status {} found!", Stato.VALIDATO);
            }
            logger.debug("-- PERSIST REPORT UTILIZATIONS TASK END --");
            return;
        }
    }
    }