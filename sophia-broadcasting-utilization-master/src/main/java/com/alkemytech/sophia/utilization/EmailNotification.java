//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.alkemytech.sophia.utilization;

import com.alkemytech.sophia.common.rest.RestClient;
import com.alkemytech.sophia.utilization.guice.McmdbJpaModule;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.google.inject.persist.PersistService;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.persistence.EntityManager;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.persistence.sessions.Record;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EmailNotification {
    private static final Logger logger = LoggerFactory.getLogger(EmailNotification.class);
    private final Provider<EntityManager> provider;
    private final Properties configuration;

    @Inject
    public EmailNotification(Provider<EntityManager> provider, @Named("configuration") Properties configuration) {
        this.provider = provider;
        this.configuration = configuration;
    }

    public static void main(String[] args) {
        try {
            Injector injector = Guice.createInjector(new Module[]{new McmdbJpaModule(args)});

            try {
                ((PersistService)injector.getInstance(PersistService.class)).start();
                EmailNotification emailNotification = (EmailNotification)injector.getInstance(EmailNotification.class);
                emailNotification.process();
            } finally {
                ((PersistService)injector.getInstance(PersistService.class)).stop();
            }
        } catch (Exception var12) {
            logger.error("main", var12);
        } finally {
            System.exit(0);
        }

    }

    private void process() throws IOException {
        ServerSocket socket = new ServerSocket(Integer.parseInt(this.configuration.getProperty("email_notification.bind_port", "0")));
        Throwable var2 = null;

        try {
            logger.info("socket bound to {}", socket.getLocalSocketAddress());
            String[] scenarios = this.configuration.getProperty("email_notification.scenarios").split(",");
            EntityManager em = (EntityManager)this.provider.get();
            String[] var5 = scenarios;
            int var6 = scenarios.length;

            for(int var7 = 0; var7 < var6; ++var7) {
                String scenario = var5[var7];
                logger.info("start scenario {}", scenario);
                String sqlFile = this.configuration.getProperty(String.format("email_notification.%s.sql.select_file_record", scenario));
                String templateIdsProperty = this.configuration.getProperty(String.format("email_notification.%s.email_template_ids", scenario), "");
                String[] templateIds = StringUtils.isNotBlank(templateIdsProperty) ? templateIdsProperty.split(",") : new String[0];
                String sqlUpdateFile = this.configuration.getProperty(String.format("email_notification.%s.sql.update_file_record", scenario));
                List<Record> resultList = em.createNativeQuery(sqlFile).setHint("eclipselink.result-type", "Map").getResultList();
                Iterator var14 = resultList.iterator();

                while(var14.hasNext()) {
                    Record record = (Record)var14.next();
                    em.getTransaction().begin();
                    String[] var16 = templateIds;
                    int var17 = templateIds.length;

                    for(int var18 = 0; var18 < var17; ++var18) {
                        String templateId = var16[var18];
                        Record template = (Record)em.createNativeQuery(this.configuration.getProperty("email_notification.sql.select_email_template_by_id")).setParameter(1, templateId).setHint("eclipselink.result-type", "Map").getSingleResult();
                        String subject = formatTemplate((String)template.get("OGGETTO"), record);
                        String body = StringEscapeUtils.escapeHtml(convertToHTMLCodes(formatTemplate((String)template.get("CORPO"), record)));
                        String mailRecipient;
                        switch ((String)template.get("DESTINATARIO")) {
                            case "U":
                                mailRecipient = (String)record.get("email");
                                break;
                            case "S":
                                mailRecipient = this.configuration.getProperty("email_notification.type_s_recipient");
                                break;
                            default:
                                throw new IllegalStateException("");
                        }

                        JsonArray toListArray = new JsonArray();
                        JsonObject email = new JsonObject();
                        email.addProperty("email", mailRecipient);
                        toListArray.add(email);
                        Map<String, String> params = new HashMap();
                        params.put("codiceAbbonamento", "12345678");
                        params.put("isHtml", "1");
                        params.put("mailSender", "noreply@siae.it");
                        params.put("messageBody", body);
                        params.put("subject", subject);
                        params.put("toList", toListArray.toString());
                        String muleSendMailUrl = this.configuration.getProperty("email_notification.ws.mule_send_mail");
                        RestClient.Result result = RestClient.post(muleSendMailUrl, params, true);
                        if (result.getJsonElement() != null) {
                            boolean ko = result.getJsonElement().getAsJsonObject().get("KO").getAsBoolean();
                            if (result.getStatusCode() != 200 || ko) {
                                throw new RuntimeException(String.format("errore nella chiamata al servizio %s", muleSendMailUrl));
                            }
                        }
                    }

                    em.createNativeQuery(sqlUpdateFile).setParameter(1, record.get("id")).executeUpdate();
                    em.getTransaction().commit();
                }
            }
        } catch (Throwable var37) {
            var2 = var37;
            throw var37;
        } finally {
            if (socket != null) {
                if (var2 != null) {
                    try {
                        socket.close();
                    } catch (Throwable var36) {
                        var2.addSuppressed(var36);
                    }
                } else {
                    socket.close();
                }
            }

        }

    }

    private static String formatTemplate(String template, Map<String, String> tokens) {
        String patternString = "%%(" + StringUtils.join(tokens.keySet(), "|") + ")%%";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(template);
        StringBuffer sb = new StringBuffer();

        while(matcher.find()) {
            if (tokens.get(matcher.group(1)) != null) {
                matcher.appendReplacement(sb, Matcher.quoteReplacement((String)tokens.get(matcher.group(1))));
            } else {
                matcher.appendReplacement(sb, "null");
            }
        }

        matcher.appendTail(sb);
        return sb.toString();
    }

    public static String convertToHTMLCodes(String str) {
        StringBuilder sb = new StringBuilder();
        int len = str.length();

        for(int i = 0; i < len; ++i) {
            char c = str.charAt(i);
            if (c > 127) {
                sb.append("&#");
                sb.append(Integer.toString(c, 10));
                sb.append(";");
            } else {
                sb.append(c);
            }
        }

        return sb.toString();
    }
}
