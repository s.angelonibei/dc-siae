package com.alkemytech.sophia.utilization.job;

import com.alkemytech.sophia.broadcasting.enums.Stato;
import com.alkemytech.sophia.broadcasting.model.BdcInformationFileEntity;
import com.alkemytech.sophia.broadcasting.service.interfaces.InformationFileEntityService;
import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.s3.S3Service;
import com.amazonaws.services.s3.AmazonS3URI;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.tools.file.FileUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;

public class FileJob {

    private static final Logger logger = LoggerFactory.getLogger(FileJob.class);
    private S3Service s3Service;
    private InformationFileEntityService informationFileEntityService;
    private String tmpDir;
    private String awsFilelock;

    @Inject
    public FileJob(InformationFileEntityService informationFileEntityService, S3Service s3Service){

        this.s3Service = s3Service;
        this.informationFileEntityService = informationFileEntityService;

    }

    public List<BdcInformationFileEntity> getFileToElaborate(Stato stato){
        return informationFileEntityService.findByStato( stato );
    }

    public ByteArrayOutputStream getFileFromS3Service(BdcInformationFileEntity bdcinfo ){

        ByteArrayOutputStream out = null;

        // avvio dei servizi necessari
        startAllService();;

        // download del fiel da Amazon S3
        out = downloadFileFromS3(bdcinfo);

        // se il file stato persistito con successo effettua un lock sul file presente in Amazon S3
        if (out != null && out.size() != 0){
            return out;
        }

        return out;

    }

    private ByteArrayOutputStream downloadFileFromS3( BdcInformationFileEntity bdcinfo ){

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        // percorso del file su S£
        if (bdcinfo != null && bdcinfo.getPercorso() != null) {

            AmazonS3URI amazonS3URI = new AmazonS3URI(bdcinfo.getPercorso());

            // verifica che il file sia presente su S3 e non sia in lock
            if (!s3Service.doesObjectExist(amazonS3URI.getBucket(), amazonS3URI.getKey() + Constants.LOCK_SUFFIX ) ) {
                logger.debug("found file: {} on Amazon S3", amazonS3URI.getKey());

                if (s3Service.lockFile(amazonS3URI.getBucket(), amazonS3URI.getKey())) {
                    logger.debug("il file {} è stato lockato", amazonS3URI.getKey());
                    s3Service.download(amazonS3URI.getBucket(), amazonS3URI.getKey(), out);
                }

            }
        }
        return out;
    }

    public void startAllService(){
        this.s3Service.startup();
        logger.debug("servizi avviati con successo.");
    }

    public void stopAllService(){
        this.s3Service.shutdown();
    }

    public boolean unloackFileToS3(BdcInformationFileEntity bdcinfo) {
        // se l'elaborazione è terminata con successo elimino il lock
        AmazonS3URI amazonS3URI = new AmazonS3URI(bdcinfo.getPercorso());
        if (s3Service.delete(amazonS3URI.getBucket(), amazonS3URI.getKey() + Constants.LOCK_SUFFIX)) {
            logger.debug("eliminato il lock dal file. elaborazione terminata");
            return true;
        }
        return false;
    }

}
