package com.alkemytech.sophia.utilization;

import com.alkemytech.sophia.broadcasting.dto.FileKpiDTO;
import com.alkemytech.sophia.broadcasting.service.interfaces.InformationFileEntityService;
import com.alkemytech.sophia.utilization.guice.McmdbJpaModule;
import com.alkemytech.sophia.utilization.service.RecordService;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;
import com.google.inject.persist.PersistService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.ServerSocket;
import java.util.*;


public class KpiEvaluator {

    private static final Logger logger = LoggerFactory.getLogger(KpiEvaluator.class);

    private RecordService recordService;
    private InformationFileEntityService informationFileEntityService;
    private Properties configuration;

    @Inject
    public KpiEvaluator(RecordService recordService,
                        InformationFileEntityService informationFileEntityService,
                        @Named("configuration") Properties configuration){
        this.recordService = recordService;
        this.informationFileEntityService = informationFileEntityService;
        this.configuration = configuration;
    }

    public static void main(String[] args) {
        try {
                logger.debug("prima di injector");
                final  Injector injector = Guice.createInjector(new McmdbJpaModule(args));
            try{
                //Setup services
                injector.getInstance(PersistService.class).start();
                KpiEvaluator kpiEvaluator = injector.getInstance(KpiEvaluator.class);
                logger.debug("dopo injector");

                logger.debug("process");

                kpiEvaluator.process();
        		
            }
            finally {
                injector.getInstance(PersistService.class).stop();
            }
        }
        catch (Exception e){
            logger.error("main", e);
        }
        finally {
            System.exit(0);
        }
    }

    private void process() throws Exception {
        try(final ServerSocket socket = new ServerSocket(
                Integer.parseInt(configuration.getProperty("kpiEvaluatorListenPort", "0")))){
            logger.info("socket bound to {}", socket.getLocalSocketAddress());

            logger.debug("richiesta dei file da elaborare");

            List<FileKpiDTO> kpiToEvaluateList = informationFileEntityService.getFileKpiToWork();

            logger.debug("dopo la query");

            ListIterator<FileKpiDTO> listIterator = kpiToEvaluateList.listIterator();
            while (listIterator.hasNext()){

                FileKpiDTO fileKpiDTO =  listIterator.next();

                Map<Integer, Set<String>> kpiToEvaluate = fileKpiDTO.getAmbitoKpi();
                logger.debug("kpiToEvaluateList id="+ fileKpiDTO.getBdcUtilizationFile()+" started");

                recordService.evaluateKpi(kpiToEvaluate);
                logger.debug("EvaluateList id="+ fileKpiDTO.getBdcUtilizationFile()+" completed");

                fileKpiDTO.getBdcUtilizationFile().setKpiOk(new Integer(1));
                logger.debug("save file id="+ fileKpiDTO.getBdcUtilizationFile()+" start");
                informationFileEntityService.save(fileKpiDTO.getBdcUtilizationFile());
                logger.debug("save file id="+ fileKpiDTO.getBdcUtilizationFile()+" completed");

            }
        }

    }

}