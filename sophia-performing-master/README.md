# Sophia Performing 

`<@TODO Inserire Descrizione Utile...>`


## Local Build

`mvn clean install`

## Distribution Build

`mvn clean package -P distribution -Ddeploy.env=test`

<p>Produrrà un file gz nella directory target per l'ambiente indicato nella property deploy.env.  
I valori possibili della property sono quelli delle cartelle contenute sotto deploy (dev,test,prod).  
Il file gz conterrà tutti i file da rilasciare nell'ambiente selezionato<p>

* * *


> <center>Riportare SEMPRE le modifiche ai file di configurazione nelle directory dei vari ambienti!</center>