#!/bin/sh

DEPLOY_DIR=/home/roberto.cerfogli/sophia-performing/test

scp -r test/* roberto.cerfogli@10.2.0.194:$DEPLOY_DIR
scp -r lib roberto.cerfogli@10.2.0.194:$DEPLOY_DIR
#scp -r lib/sophia*.jar roberto.cerfogli@10.2.0.194:$DEPLOY_DIR/lib
scp -r sophia*.jar roberto.cerfogli@10.2.0.194:$DEPLOY_DIR

ssh roberto.cerfogli@10.2.0.194


