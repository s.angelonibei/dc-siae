#!/bin/sh

#HOME=/var/local/sophia/test/performing/codifica
HOME=/home/roberto.cerfogli/sophia-performing/test

JPACKAGE=com.alkemytech.sophia.performing
JCLASS=ResponseAdapter
JCONFIG=response-adapter.properties
#JSTDOUT=$HOME/response-adapter.out
JSTDOUT=/dev/null
JOPTIONS="-Xmx2G -Ddefault.home_folder=$HOME"
JVERSION=1.0.0

#cd $HOME

nohup java -cp "$HOME/sophia-performing-$JVERSION.jar:$HOME/lib/*" $JOPTIONS $JPACKAGE.$JCLASS $HOME/$JCONFIG 2>&1 >> $JSTDOUT &


