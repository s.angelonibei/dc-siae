#!/bin/sh

#HOME=/var/local/sophia/dev/performing/codifica
HOME=/home/roberto.cerfogli/sophia-performing/dev

JPACKAGE=com.alkemytech.sophia.performing
JCLASS=Combana
JCONFIG=combana.properties
#JSTDOUT=$HOME/combana.out
JSTDOUT=/dev/null
JOPTIONS="-Xmx2G -Ddefault.home_folder=$HOME"
JVERSION=1.0.0

#cd $HOME

nohup java -cp "$HOME/sophia-performing-$JVERSION.jar:$HOME/lib/*" $JOPTIONS $JPACKAGE.$JCLASS $HOME/$JCONFIG 2>&1 >> $JSTDOUT &


