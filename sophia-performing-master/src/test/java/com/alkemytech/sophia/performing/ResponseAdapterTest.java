package com.alkemytech.sophia.performing;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.ConfigurationLoader;
import com.alkemytech.sophia.commons.guice.Log4j2;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.performing.drools.DroolsClassifier;
import com.alkemytech.sophia.performing.drools.SophiaDroolsClassifier;
import com.alkemytech.sophia.performing.drools.SophiaDroolsDBConfigClassifier;
import com.alkemytech.sophia.performing.jdbc.McmdbDAO;
import com.alkemytech.sophia.performing.jdbc.McmdbDataSource;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ResponseAdapterTest {
	
	private Properties configuration;
	private Charset charset;
	private Gson gson;
	private S3 s3;
	private SQS sqs;
	private MessageDeduplicator messageDeduplicator;
	private DataSource datasource;
	private McmdbDAO dao;
	private String testFileUrl;
	private File testFile;
	
	private String fileNameTestRischio;
	private File fileTestRischio;
	
	@Before
	public void setUp() throws Exception {
		Locale.setDefault(Locale.US);
		final Properties defaults = new ConfigurationLoader().withResourceName("/defaults.properties").load();
		configuration = new ConfigurationLoader().withResourceName("/response-adapter.properties").load(defaults);
		ConfigurationLoader.expandVariables(configuration);
		charset = Charset.forName(configuration.getProperty("default.charset", "UTF-8"));
		gson = new GsonBuilder().disableHtmlEscaping().create();
		s3 = new S3(configuration);
		sqs = mock(SQS.class);
		messageDeduplicator = mock(MessageDeduplicator.class);
		Log4j2.initialize(configuration);
		datasource = new McmdbDataSource(configuration, configuration.getProperty("mcmdb_data_source"));
		dao = new McmdbDAO(configuration,datasource);
		File workingDirectory = new File(configuration
				.getProperty("default.home_folder"));
		testFileUrl = configuration.getProperty("test.file.url");
		String fileName = testFileUrl.substring(testFileUrl.lastIndexOf("/"));
		testFile = new File(workingDirectory,fileName);
		if(!testFile.exists()) {
			testFile.createNewFile();
			S3 tmpS3 = new S3(configuration);
			tmpS3.startup();
			tmpS3.download(new S3.Url(testFileUrl), testFile);
			tmpS3.shutdown();
		}
		
		fileNameTestRischio = "test_rischio_2.csv";
		fileTestRischio = new File(workingDirectory,fileNameTestRischio);
		
	}
	
	@Ignore
	@Test
	public void testClassifiersConsistency() throws Exception {
		
		String compression = testFileUrl.substring(testFileUrl.lastIndexOf(".") + 1);
		
		final Map<String, String> dataStore1 = new HashMap<>();
		
		McmdbDAO fakeDao1 = mock(McmdbDAO.class);
				
		when(fakeDao1.executeUpdate(Mockito.anyString(),ArgumentMatchers.<Map<String,String>>any())).then(new Answer<Integer>() {
			@Override
			public Integer answer(InvocationOnMock invocation) throws Throwable {
				Map<String, String> parameters =(Map<String, String>) invocation.getArgument(1);
				if(parameters!=null) {
				String idCombana = parameters.get("idCombana");
				String tipoApprovazione = parameters.get("tipoApprovazione");
				dataStore1.put(idCombana, tipoApprovazione);
				}
				return 1;
			}
		});
		
		DroolsClassifier currentDroolsClassifier = new SophiaDroolsClassifier(configuration, s3, dao, "sophia_drools_classifier");
		
		ResponseAdapter responseAdapter = new ResponseAdapter(configuration, charset, gson, s3, sqs, fakeDao1, messageDeduplicator, currentDroolsClassifier);
		
		responseAdapter.startup();
								
		responseAdapter.parseOutputFile(testFile, compression);
		
		responseAdapter.shutdown();
		
		DroolsClassifier droolsDRTClassifier = new SophiaDroolsDBConfigClassifier(configuration, dao, "sophia_drools_dbconfig_classifier");
		
		McmdbDAO fakeDao2 = mock(McmdbDAO.class);
		
		final Map<String, String> dataStore2 = new HashMap<>();
		
		when(fakeDao2.executeUpdate(Mockito.anyString(),ArgumentMatchers.<Map<String,String>>any())).then(new Answer<Integer>() {
			@Override
			public Integer answer(InvocationOnMock invocation) throws Throwable {
				Map<String, String> parameters =(Map<String, String>) invocation.getArgument(1);
				if(parameters!=null) {
				String idCombana = parameters.get("idCombana");
				String tipoApprovazione = parameters.get("tipoApprovazione");
				dataStore2.put(idCombana, tipoApprovazione);
				}
				return 1;
			}
		});
		
		
		ResponseAdapter responseAdapterDRT = new ResponseAdapter(configuration, charset, gson, s3, sqs, fakeDao2, messageDeduplicator, droolsDRTClassifier);
		
		responseAdapterDRT.startup();
				
		responseAdapterDRT.parseOutputFile(testFile, compression);
		
		responseAdapterDRT.shutdown();
		
		// Check Results consistency
	    for (Map.Entry<String, String> entry : dataStore1.entrySet()) {
	    	assertEquals(entry.getValue(), dataStore2.get(entry.getKey()));
	    }
		
	}
	
	@Ignore
	@Test
	public void testDBConfigClassifier() throws Exception {
				
		DroolsClassifier droolsDRTClassifier = new SophiaDroolsDBConfigClassifier(configuration, dao, "sophia_drools_dbconfig_classifier");
		
		McmdbDAO fakeDao2 = mock(McmdbDAO.class);
		
		final Map<String, String> dataStore2 = new HashMap<>();
		
		when(fakeDao2.executeUpdate(Mockito.anyString(),ArgumentMatchers.<Map<String,String>>any())).then(new Answer<Integer>() {
			@Override
			public Integer answer(InvocationOnMock invocation) throws Throwable {
				Map<String, String> parameters =(Map<String, String>) invocation.getArgument(1);
				if(parameters!=null) {
				String idCombana = parameters.get("idCombana");
				String tipoApprovazione = parameters.get("tipoApprovazione");
				dataStore2.put(idCombana, tipoApprovazione);
				}
				return 1;
			}
		});
		
		
		ResponseAdapter responseAdapterDRT = new ResponseAdapter(configuration, charset, gson, s3, sqs, fakeDao2, messageDeduplicator, droolsDRTClassifier);
		
		responseAdapterDRT.startup();
				
		responseAdapterDRT.parseOutputFile(fileTestRischio, "none");
		
		for (Map.Entry<String, String>  entry : dataStore2.entrySet()) {
			System.out.println(entry.getKey()+"\t"+entry.getValue());
		}
		
		responseAdapterDRT.shutdown();
		
		
	}	

}
