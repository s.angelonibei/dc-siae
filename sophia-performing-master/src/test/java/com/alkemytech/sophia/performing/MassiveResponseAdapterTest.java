package com.alkemytech.sophia.performing;

import com.alkemytech.sophia.commons.util.GsonUtils;
import com.google.gson.JsonObject;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MassiveResponseAdapterTest {

    private String message = "{\n" +
            "\t\"header\": {\n" +
            "\t\t\"uuid\": \"213b5042-999c-4a45-b6bc-9b2309f4d83d\",\n" +
            "\t\t\"timestamp\": \"2019-05-17T16:26:51.237Z\",\n" +
            "\t\t\"queue\": \"debug_completed_massiva_performing\",\n" +
            "\t\t\"sender\": \"sophia-identification\",\n" +
            "\t\t\"hostname\": \"siae_sophia_codifiche_dev\"\n" +
            "\t},\n" +
            "\t\"input\": {\n" +
            "\t\t\"header\": {\n" +
            "\t\t\t\"uuid\": \"fadcd83a-412b-4f55-96c8-8ae55303dda0\",\n" +
            "\t\t\t\"timestamp\": \"2019-05-17T16:26:07.249Z\",\n" +
            "\t\t\t\"queue\": \"debug_to_process_core_identification\"\n" +
            "\t\t},\n" +
            "\t\t\"body\": {\n" +
            "\t\t\t\"configuration\": {\n" +
            "\t\t\t\t\"artist_weight\": 0.333,\n" +
            "\t\t\t\t\"title_weight\": 0.667,\n" +
            "\t\t\t\t\"artist_exponent\": 1.0,\n" +
            "\t\t\t\t\"title_exponent\": 1.0,\n" +
            "\t\t\t\t\"risk_flag\": 0,\n" +
            "\t\t\t\t\"relevance_flag\": 0,\n" +
            "\t\t\t\t\"risk_period\": 4,\n" +
            "\t\t\t\t\"risk_weight\": 0.333,\n" +
            "\t\t\t\t\"risk_exponent\": 1.0,\n" +
            "\t\t\t\t\"relevance_weight\": 0.333,\n" +
            "\t\t\t\t\"relevance_period\": 4,\n" +
            "\t\t\t\t\"relevance_exponent\": 1.0,\n" +
            "\t\t\t\t\"maturato.threshold.1\": 1000,\n" +
            "\t\t\t\t\"maturato.threshold.3\": 100000,\n" +
            "\t\t\t\t\"maturato.threshold.2\": 10000,\n" +
            "\t\t\t\t\"maturato.threshold.4\": 100000,\n" +
            "\t\t\t\t\"rischio.threshold.1\": 0.25,\n" +
            "\t\t\t\t\"rischio.threshold.2\": 0.5,\n" +
            "\t\t\t\t\"rischio.threshold.3\": 0.5\n" +
            "\t\t\t},\n" +
            "\t\t\t\"version\": \"1\",\n" +
            "\t\t\t\"compression\": \"guess\",\n" +
            "\t\t\t\"configUrl\": \"s3://siae-sophia-identification/test/config/performing.properties\",\n" +
            "\t\t\t\"input\": {\n" +
            "\t\t\t\t\"header\": {\n" +
            "\t\t\t\t\t\"timestamp\": \"1558110352600\",\n" +
            "\t\t\t\t\t\"uuid\": \"1554483d-6cf1-4b1d-989e-b6d2f57f599e\",\n" +
            "\t\t\t\t\t\"queue\": \"debug_to_process_massive_request_adapter\",\n" +
            "\t\t\t\t\t\"sender\": \"codifica_manuale_webapp\",\n" +
            "\t\t\t\t\t\"process\": \"massivo\"\n" +
            "\t\t\t\t},\n" +
            "\t\t\t\t\"body\": {\n" +
            "\t\t\t\t\t\"configuration\": {\n" +
            "\t\t\t\t\t\t\"risk_exponent\": 1,\n" +
            "\t\t\t\t\t\t\"title_weight\": 0.667,\n" +
            "\t\t\t\t\t\t\"relevance_period\": 4,\n" +
            "\t\t\t\t\t\t\"rischio.threshold.1\": 0.25,\n" +
            "\t\t\t\t\t\t\"title_exponent\": 1,\n" +
            "\t\t\t\t\t\t\"risk_weight\": 0.333,\n" +
            "\t\t\t\t\t\t\"relevance_exponent\": 1,\n" +
            "\t\t\t\t\t\t\"rischio.threshold.3\": 0.5,\n" +
            "\t\t\t\t\t\t\"rischio.threshold.2\": 0.5,\n" +
            "\t\t\t\t\t\t\"risk_flag\": 0,\n" +
            "\t\t\t\t\t\t\"maturato.threshold.1\": 1000,\n" +
            "\t\t\t\t\t\t\"relevance_weight\": 0.333,\n" +
            "\t\t\t\t\t\t\"risk_period\": 4,\n" +
            "\t\t\t\t\t\t\"artist_weight\": 0.333,\n" +
            "\t\t\t\t\t\t\"relevance_flag\": 0,\n" +
            "\t\t\t\t\t\t\"maturato.threshold.4\": 100000,\n" +
            "\t\t\t\t\t\t\"maturato.threshold.2\": 10000,\n" +
            "\t\t\t\t\t\t\"artist_exponent\": 1,\n" +
            "\t\t\t\t\t\t\"maturato.threshold.3\": 100000\n" +
            "\t\t\t\t\t},\n" +
            "\t\t\t\t\t\"configurationId\": 77,\n" +
            "\t\t\t\t\t\"version\": \"1\",\n" +
            "\t\t\t\t\t\"compression\": \"guess\",\n" +
            "\t\t\t\t\t\"configUrl\": \"s3://siae-sophia-identification/dev/config/upload_massive.properties\",\n" +
            "\t\t\t\t\t\"items\": [{\n" +
            "\t\t\t\t\t\t\"inputUrl\": \"s3://siae-sophia-identification/dev/codifica-massiva/csv/input/Test_UAT_Upgrade_Codifica_Modificato-1554483d-6cf1-4b1d-989e-b6d2f57f599e.csv.gz\",\n" +
            "\t\t\t\t\t\t\"outputUrl\": \"s3://siae-sophia-identification/dev/codifica-massiva/csv/output/Test_UAT_Upgrade_Codifica_Modificato-1554483d-6cf1-4b1d-989e-b6d2f57f599e.csv.gz\"\n" +
            "\t\t\t\t\t}],\n" +
            "\t\t\t\t\t\"startedQueue\": \"debug_started_core_identification\",\n" +
            "\t\t\t\t\t\"completedQueue\": \"debug_completed_massiva_performing\",\n" +
            "\t\t\t\t\t\"failedQueue\": \"debug_failed_massiva_performing\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t},\n" +
            "\t\t\t\"items\": [{\n" +
            "\t\t\t\t\"inputUrl\": \"s3://siae-sophia-identification/test/input/performing/part-0-8008d226-8af6-478f-a4cf-7d189983e025.csv.gz\",\n" +
            "\t\t\t\t\"outputUrl\": \"s3://siae-sophia-identification/test/output/performing/part-0-8008d226-8af6-478f-a4cf-7d189983e025.csv.gz\"\n" +
            "\t\t\t}],\n" +
            "\t\t\t\"completedQueue\": \"debug_completed_massiva_performing\",\n" +
            "\t\t\t\"failedQueue\": \"debug_failed_massiva_performing\"\n" +
            "\t\t}\n" +
            "\t},\n" +
            "\t\"output\": {\n" +
            "\t\t\"items\": [{\n" +
            "\t\t\t\"inputUrl\": \"s3://siae-sophia-identification/test/input/performing/part-0-8008d226-8af6-478f-a4cf-7d189983e025.csv.gz\",\n" +
            "\t\t\t\"outputUrl\": \"s3://siae-sophia-identification/test/output/performing/part-0-8008d226-8af6-478f-a4cf-7d189983e025.csv.gz\",\n" +
            "\t\t\t\"inputFileSize\": 19731,\n" +
            "\t\t\t\"outputFileSize\": 110,\n" +
            "\t\t\t\"totalRecords\": 486,\n" +
            "\t\t\t\"identifiedRecords\": 0,\n" +
            "\t\t\t\"identifiedRecordsWorkCode\": 0,\n" +
            "\t\t\t\"identifiedRecordsIswc\": 0,\n" +
            "\t\t\t\"identifiedRecordsIsrc\": 0,\n" +
            "\t\t\t\"identifiedRecordsTitleArtist\": 0,\n" +
            "\t\t\t\"unidentifiedRecords\": 486,\n" +
            "\t\t\t\"invalidRecords\": 486,\n" +
            "\t\t\t\"workCodeQueries\": 0,\n" +
            "\t\t\t\"iswcQueries\": 0,\n" +
            "\t\t\t\"iswcQueriesWithResults\": 0,\n" +
            "\t\t\t\"iswcPostings\": 0,\n" +
            "\t\t\t\"isrcQueries\": 0,\n" +
            "\t\t\t\"isrcQueriesWithResults\": 0,\n" +
            "\t\t\t\"isrcPostings\": 0,\n" +
            "\t\t\t\"titleArtistQueries\": 0,\n" +
            "\t\t\t\"titleArtistQueriesWithResults\": 0,\n" +
            "\t\t\t\"titleArtistPostings\": 0,\n" +
            "\t\t\t\"beginTimeMillis\": 1558110410786,\n" +
            "\t\t\t\"endTimeMillis\": 1558110411216,\n" +
            "\t\t\t\"elapsedTimeMillis\": 430,\n" +
            "\t\t\t\"beginTime\": \"May 17, 2019 4:26:50 PM\",\n" +
            "\t\t\t\"endTime\": \"May 17, 2019 4:26:51 PM\",\n" +
            "\t\t\t\"elapsedTime\": \"430ms\"\n" +
            "\t\t}]\n" +
            "\t}\n" +
            "}";


    @Test
    public void nomeacaso() {


        JsonObject input = GsonUtils.getAsJsonObject(GsonUtils.fromJson(message, JsonObject.class), "input");


        String uuid = getUuid(input);


        assertEquals(uuid, "1554483d-6cf1-4b1d-989e-b6d2f57f599e");

    }

    private String getUuid(JsonObject input) {
        JsonObject body = GsonUtils.getAsJsonObject(input, "body");
        JsonObject input1 = GsonUtils.getAsJsonObject(body, "input");
        JsonObject header = GsonUtils.getAsJsonObject(input1, "header");
        return GsonUtils.getAsString(header, "uuid");
    }
}
