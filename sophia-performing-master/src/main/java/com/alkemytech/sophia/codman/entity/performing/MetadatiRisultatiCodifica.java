package com.alkemytech.sophia.codman.entity.performing;

import java.util.List;
import java.util.Map;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class MetadatiRisultatiCodifica {

	private Double sogliaConf;
    private Double distSecond;
    private Double ecoValue;
    private String azione;
    
    private List<Map<String, Object>> candidate;

    public MetadatiRisultatiCodifica() {
    	super();
    }

    public MetadatiRisultatiCodifica(Double sogliaConf, Double distSecond, Double ecoValue, String azione) {
    	super();
        this.sogliaConf = sogliaConf;
        this.distSecond = distSecond;
        this.ecoValue = ecoValue;
        this.azione = azione;
    }

    public Double getSogliaConf() {
        sogliaConf.toString().replace(',', '.');
        return sogliaConf;
    }

    public void setSogliaConf(Double sogliaConf) {
        this.sogliaConf = sogliaConf;
    }

    public Double getDistSecond() {
        distSecond.toString().replace(',', '.');
        return distSecond;
    }

    public void setDistSecond(Double distSecond) {
        this.distSecond = distSecond;
    }

    public Double getEcoValue() {
        ecoValue.toString().replace(',', '.');
        return ecoValue;
    }

    public void setEcoValue(Double ecoValue) {
        this.ecoValue = ecoValue;
    }

    public String getAzione() {
        return azione;
    }

    public void setAzione(String azione) {
        this.azione = azione;
    }

	public List<Map<String, Object>> getCandidate() {
		return candidate;
	}

	public void setCandidate(List<Map<String, Object>> candidate) {
		this.candidate = candidate;
	}

	@Override
	public String toString() {
		return "MetadatiRisultatiCodifica [sogliaConf=" + sogliaConf + ", distSecond=" + distSecond + ", ecoValue=" + ecoValue + ", azione="
				+ azione + "]";
	}
	
	
}
