package com.alkemytech.sophia.performing.drools;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.DecisionTableConfiguration;
import org.drools.builder.DecisionTableInputType;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class DroolsClassifier {

	protected KnowledgeBase getKnowledgeBaseFromXls(File xlsFile) {
		final DecisionTableConfiguration configuration = KnowledgeBuilderFactory.newDecisionTableConfiguration();
		configuration.setInputType(DecisionTableInputType.XLS);
		final KnowledgeBuilder knowledgeBuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
		knowledgeBuilder.add(ResourceFactory.newFileResource(xlsFile), ResourceType.DTABLE, configuration);
		if (knowledgeBuilder.hasErrors())
			throw new RuntimeException(knowledgeBuilder.getErrors().toString());
		final KnowledgeBase knowledgeBase = KnowledgeBaseFactory.newKnowledgeBase();
		knowledgeBase.addKnowledgePackages(knowledgeBuilder.getKnowledgePackages());
		return knowledgeBase;
	}
	
	public void startup() throws Exception {
		
	}
	
	public void shutdown() {
		
	}

	public abstract String getClassification(double sogliaConf, double distSecond, double ecoValue);
	
	
	public String getClassification(double sogliaConf, double distSecond, double ecoValue, List<Map<String, Object>> candidate) {
		return getClassification(sogliaConf, distSecond, ecoValue);
	}

}


