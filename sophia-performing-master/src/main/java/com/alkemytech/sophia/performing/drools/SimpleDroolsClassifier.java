package com.alkemytech.sophia.performing.drools;


import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.drools.runtime.StatefulKnowledgeSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.entity.performing.MetadatiRisultatiCodifica;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class SimpleDroolsClassifier extends DroolsClassifier {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Properties configuration;
	private final String propertyPrefix;

	private StatefulKnowledgeSession session;
	
	@Inject
	public SimpleDroolsClassifier(@Named("configuration") Properties configuration,
			@Named("drools_classifier") String propertyPrefix) throws IOException {
		super();
		this.configuration = configuration;
		this.propertyPrefix = propertyPrefix;
	}
	
	@Override
	public void startup() {
		final File xlsFile = new File(configuration
				.getProperty(propertyPrefix + ".xls_file_path"));
		logger.debug("xlsFile: {} ({})", xlsFile,
				TextUtils.formatSize(xlsFile.length()));
		session = getKnowledgeBaseFromXls(xlsFile)
				.newStatefulKnowledgeSession();
	}
	
	@Override
	public String getClassification(double sogliaConf, double distSecond, double ecoValue) {
		final MetadatiRisultatiCodifica event = new MetadatiRisultatiCodifica();
		event.setSogliaConf(sogliaConf);
		event.setDistSecond(distSecond);
		event.setEcoValue(ecoValue);
		session.insert(event);
		session.fireAllRules();
		return event.getAzione();
	}

}


