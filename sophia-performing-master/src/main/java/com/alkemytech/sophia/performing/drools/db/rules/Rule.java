package com.alkemytech.sophia.performing.drools.db.rules;

import java.util.ArrayList;
import java.util.List;

public class Rule {

    private String template;
    
    private int salience;
    
    private Action action;
        
    private String name;
    
	private List<Condition> conditions = new ArrayList<Condition>();
	
	public String getTemplate() {
		return template;
	}
	
	public void setTemplate(String template) {
		this.template = template;
	}

	public int getSalience() {
		return salience;
	}

	public void setSalience(int salience) {
		this.salience = salience;
	}
	
	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}
	    
    public String activation(){
        StringBuilder statementBuilder = new StringBuilder();

        for (Condition condition : getConditions()) {

            statementBuilder.append(condition.getField()).append(" ").append(condition.getRelation().getLiteral()).append(" ");
            
            statementBuilder.append(condition.getValue());

            statementBuilder.append(", ");
        }

        String statement = statementBuilder.toString();

        return statement.substring(0, statement.length() - 2);
    }
    
	public List<Condition> getConditions() {
        return conditions;
    }

    public void setConditions(List<Condition> conditions) {
        this.conditions = conditions;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	

}
