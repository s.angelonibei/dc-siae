package com.alkemytech.sophia.performing.drools.db.rules;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.performing.drools.db.rules.Condition.Relation;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class RuleFactory {
	
	public static final int MAX_SALIENCE=100;

	public Rule createDefaultRule() {
		Rule defaultRule = new Rule();
		defaultRule.setSalience(-1);
		Condition defaultCondition = new Condition();
		defaultCondition.setField("sogliaConf");
		defaultCondition.setOperator(Relation.GT);
		defaultCondition.setValue(0d);
		defaultRule.setConditions(Arrays.asList(defaultCondition));
		defaultRule.setAction(Action.MANUAL);
		defaultRule.setTemplate("metadati-codifica.drt");
		defaultRule.setName("regola-di-default");
		return defaultRule;
	}

	public Rule createRule(String ruleName, Integer priority, String ruleConfig, String ruleParams) {
		if(ruleName.equalsIgnoreCase("MetadatiRisultatiCodificaRule")) {
			Rule rule = new Rule();
			rule.setSalience(MAX_SALIENCE - priority);
			rule.setAction(buildAction(ruleConfig));
			rule.setConditions(buildCondition(ruleParams));
			rule.setTemplate("metadati-codifica.drt");
			rule.setName(ruleName + "-" + priority);
			return rule;
		}
		if(ruleName.equalsIgnoreCase("ProposalFieldRule")) {
			Type mapList = new TypeToken<ArrayList<Map<String,String>>>(){}.getType();
			List<Map<String,String>> allParams = new Gson().fromJson(ruleParams, mapList);
			if (allParams != null && allParams.size() > 0) {
				Map<String, String> decodedParams = allParams.get(0);
				if (decodedParams.get("value") != null && decodedParams.get("value").equals("1")) {
					Map<String, String> decodedConfig = GsonUtils.decodeJsonMap(ruleConfig);
					decodedParams.putAll(decodedConfig);
					if (decodedParams.get("field") != null && decodedParams.get("value") != null && decodedParams.get("relation") != null) {
						Rule rule = new ProposalValueRule();
						rule.setSalience(MAX_SALIENCE - priority);
						rule.setAction(buildAction(ruleConfig));
						Condition c = new Condition();
						c.setField(decodedParams.get("field"));
						c.setOperator(Relation.valueOf(decodedParams.get("relation")));
						c.setValue(decodedParams.get("value"));
						rule.setConditions(Arrays.asList(c));
						rule.setTemplate("rule-candidata.drt");
						rule.setName(ruleName + "-" + priority);
						return rule;
					}
				}
			}
		}
		if (ruleName.equalsIgnoreCase("ProposalFlagRule")) {
			List<Condition> conditions = buildCondition(ruleParams);
			if (conditions.size() == 1 && conditions.get(0).getValue().toString().equals("1")) {
				Rule rule = new ProposalFlagRule();
				rule.setSalience(MAX_SALIENCE - priority);
				rule.setAction(buildAction(ruleConfig));
				rule.setConditions(conditions);
				rule.setTemplate("rule-candidata.drt");
				rule.setName(ruleName + "-" + priority);
				return rule;
			}
		}
		return null;
	}
	
	protected Action buildAction(String ruleConfig) {
		Map<String, String> decoded = GsonUtils.decodeJsonMap(ruleConfig);
		if (decoded.containsKey("action")) {
			return Action.fromKey(decoded.get("action"));
		} else {
			throw new RuntimeException("Unsupported Action");
		}
	}
	
	protected List<Condition> buildCondition(String ruleParams){
		Type listType = new TypeToken<ArrayList<Condition>>(){}.getType();
		List<Condition> conditions = new Gson().fromJson(ruleParams, listType);		
		return conditions;
	}

}
