package com.alkemytech.sophia.performing;

import java.io.IOException;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.performing.jdbc.McmdbDAO;
import com.alkemytech.sophia.performing.jdbc.McmdbDataSource;
import com.google.common.base.Strings;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class Combana {
	
	private static final Logger logger = LoggerFactory.getLogger(Combana.class);

	private static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("MCMDB"))
				.to(McmdbDataSource.class)
				.asEagerSingleton();
			// data access object(s)
			bind(McmdbDAO.class)
				.asEagerSingleton();
			// self
			bind(Combana.class)
				.asEagerSingleton();
		}
		
	}

	public static void main(String[] args) {
		try {
			final Combana instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/combana.properties"))
					.getInstance(Combana.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Properties configuration;
	private final Charset charset;
	private final McmdbDAO dao;
	private final MessageDigest md5;

	@Inject
	protected Combana(@Named("configuration") Properties configuration,
			@Named("charset") Charset charset, McmdbDAO dao) throws NoSuchAlgorithmException {
		super();
		this.configuration = configuration;
		this.charset = charset;
		this.dao = dao;
		this.md5 = MessageDigest.getInstance("MD5");
	}
	
	public Combana startup() {
		if ("true".equalsIgnoreCase(configuration.getProperty("combana.locked", "true"))) {
			throw new IllegalStateException("application locked");
		}
		return this;
	}

	public Combana shutdown() {
		return this;
	}
	
	private Combana process(String[] args) throws SQLException, IOException {
		
		final int bindPort = Integer.parseInt(configuration
				.getProperty("combana.bind_port", configuration
						.getProperty("default.bind_port", "0")));

		// bind lock tcp port
		try (final ServerSocket socket = new ServerSocket(bindPort)) {
			logger.info("socket bound to {}", socket.getLocalSocketAddress());
			
			// process database record(s)
			process();

		}

		return this;
	}
	
	private boolean isLavorabile(Map<String, String> record) {
		return !Strings.isNullOrEmpty(record.get("titolo")) &&
				(!Strings.isNullOrEmpty(record.get("autori")) ||
						!Strings.isNullOrEmpty(record.get("compositori")) ||
						!Strings.isNullOrEmpty(record.get("interpreti")));
	}
	
	private String computeCodiceCombana(Map<String, String> record) {
		final StringBuilder raw = new StringBuilder()
			.append(TextUtils.nullOrEmptyValue(record.get("titolo"), "/"))
			.append(TextUtils.nullOrEmptyValue(record.get("autori"), "/"))
			.append(TextUtils.nullOrEmptyValue(record.get("compositori"), "/"))
			.append(TextUtils.nullOrEmptyValue(record.get("interpreti"), "/"));
		return Hex.encodeHexString(md5
				.digest(raw.toString().getBytes(charset)));
	}

	private void process() throws SQLException {
		
		final long startTimeMillis = System.currentTimeMillis();

		long foundCombana = 0L;
		long insertedCombana = 0L;
		long updatedUtilizzazioni = 0L;
		
		final HeartBeat heartbeat = HeartBeat.constant("record", Integer.parseInt(configuration
				.getProperty("combana.heartbeat", configuration.getProperty("default.heartbeat", "1000"))));

		// select utilizzazioni
		final List<Map<String, String>> records = dao
				.executeQuery("combana.sql.select_utilizzazioni");
		logger.info("{} rows(s) to process", records.size());
		for (final Map<String, String> record : records) {
			
			// compute lavorabile
			final boolean lavorabile = isLavorabile(record);
			record.put("lavorabile", lavorabile ? "1" : "0");
			
			// compute codice combana
			record.put("codiceCombana", computeCodiceCombana(record));
			
			// select combana
			final Map<String, String> combana = dao
					.executeSingleRowQuery("combana.sql.select_combana", record);
			Long idCombana = null;
			
			// insert if not found
			if (null == combana) {
				
				// insert combana
				int count = dao
						.executeUpdate("combana.sql.insert_combana", record, true);
				if (count >= 1) {
					insertedCombana ++;
					idCombana = dao.getLastGeneratedKey();
				}
				
			} else {
				
				foundCombana ++;
				idCombana = Long.valueOf(combana.get("idCombana"));
				
			}

			// update utilizzazione
			if (null != idCombana) {
				record.put("idCombana", idCombana.toString());
				if (dao.executeUpdate("combana.sql.update_utilizzazione", record) >= 1) {
					updatedUtilizzazioni ++;
				}
			}

			heartbeat.pump();
			
		}
		
		logger.info("{} records processed", heartbeat.getTotalPumps());
		logger.info("combana generation completed in {}",
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		// log stats
		logger.info("{} combana found", foundCombana);
		logger.info("{} combana inserted", insertedCombana);
		logger.info("{} utilizzazioni updated", updatedUtilizzazioni);

	}

}

