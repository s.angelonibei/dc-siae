package com.alkemytech.sophia.performing.drools;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatefulKnowledgeSession;
import org.drools.template.ObjectDataCompiler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.entity.performing.MetadatiRisultatiCodifica;
import com.alkemytech.sophia.performing.drools.db.rules.Rule;
import com.alkemytech.sophia.performing.drools.db.rules.RuleFactory;
import com.alkemytech.sophia.performing.jdbc.McmdbDAO;
import com.google.inject.Inject;
import com.google.inject.name.Named;


public class SophiaDroolsDBConfigClassifier extends DroolsClassifier {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Properties configuration;
	private final McmdbDAO dao;
	private final String propertyPrefix;
	
	private StatefulKnowledgeSession session;
	
	@Inject
	public SophiaDroolsDBConfigClassifier(@Named("configuration") Properties configuration,
			McmdbDAO dao,
			@Named("drools_classifier") String propertyPrefix) throws IOException, SQLException {
		super();
		this.configuration = configuration;
		this.dao = dao;
		this.propertyPrefix = propertyPrefix;
	}
	
	@Override
	public void startup() throws IOException, SQLException {
	
		List<Map<String, String>> rulesData = dao.executeQuery(propertyPrefix + ".rules.sql");
		
		if (null == rulesData || rulesData.isEmpty())
			throw new IOException("Configuration parameters not found!"); 
		
		List<Rule> rules = getRules(rulesData);	
		
		ObjectDataCompiler objectDataCompiler = new ObjectDataCompiler();
		
		ArrayList<String> drls = new ArrayList<String>();
				
		for(Rule rule : rules) {
			final InputStream in = new FileInputStream(configuration.getProperty(propertyPrefix + ".templates") + rule.getTemplate());
			String drl = objectDataCompiler.compile(Arrays.asList(rule), in);
			in.close();
			drls.add(drl);
		}
		
		if(logger.isDebugEnabled()) {
			for (String drl : drls) {
				logger.debug(String.format("Generated DRL: %s", drl));
			}
		}
		
		session = getKnowledgeBaseFromDRL(drls).newStatefulKnowledgeSession();
	}

	protected List<Rule> getRules(List<Map<String, String>> sqlParameters) {
		List<Rule> rules = new ArrayList<Rule>();
		RuleFactory factory = new RuleFactory();
		for (Map<String, String> paramMap : sqlParameters) {
			Integer priority = Integer.parseInt(paramMap.get("PROCESSING_ORDER"));
			String ruleName = paramMap.get("PROCESSOR");
			String ruleConfig = paramMap.get("PROCESSOR_CONFIG");
			String ruleParams = paramMap.get("PROCESSOR_PARAMS");
			Rule rule = factory.createRule(ruleName,priority,ruleConfig,ruleParams);
			if(rule!=null) {
				rules.add(rule);
			}
		}
		Rule defaultRule = factory.createDefaultRule();
		rules.add(defaultRule);
		return rules;
	}
	
	@Override
	public String getClassification(double sogliaConf, double distSecond, double ecoValue) {
		return getClassification(sogliaConf, distSecond, ecoValue, new ArrayList<Map<String,Object>>());
	}
	
	@Override
	public String getClassification(double sogliaConf, double distSecond, double ecoValue, List<Map<String, Object>> proposals) {
		final MetadatiRisultatiCodifica event = new MetadatiRisultatiCodifica();
		event.setSogliaConf(sogliaConf);
		event.setDistSecond(distSecond);
		event.setEcoValue(ecoValue);
		event.setCandidate(proposals);
		logger.debug("Submitting ... {}", event.toString());
		session.insert(event);
		session.fireAllRules();
		return event.getAzione();
	}

	protected KnowledgeBase getKnowledgeBaseFromDRL(List<String> drls) {
		final KnowledgeBuilder knowledgeBuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
		for (String drl : drls) {
			knowledgeBuilder.add(ResourceFactory.newByteArrayResource(drl.getBytes()), ResourceType.DRL);
		}
		if (knowledgeBuilder.hasErrors())
			throw new RuntimeException(knowledgeBuilder.getErrors().toString());
		final KnowledgeBase knowledgeBase = KnowledgeBaseFactory.newKnowledgeBase();
		knowledgeBase.addKnowledgePackages(knowledgeBuilder.getKnowledgePackages());
		return knowledgeBase;
	}	

}


