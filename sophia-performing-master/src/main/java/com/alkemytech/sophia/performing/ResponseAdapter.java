package com.alkemytech.sophia.performing;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.sql.DataSource;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.io.CompressionAwareFileInputStream;
import com.alkemytech.sophia.commons.sqs.McmdbMessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.commons.util.MySqlUtils;
import com.alkemytech.sophia.performing.drools.DroolsClassifier;
import com.alkemytech.sophia.performing.drools.SophiaDroolsClassifier;
import com.alkemytech.sophia.performing.jdbc.McmdbDAO;
import com.alkemytech.sophia.performing.jdbc.McmdbDataSource;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ResponseAdapter {
	
	private static final Logger logger = LoggerFactory.getLogger(ResponseAdapter.class);

	private static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.toInstance(new S3(configuration));
			bind(SQS.class)
				.toInstance(new SQS(configuration));
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("MCMDB"))
				.to(McmdbDataSource.class)
				.asEagerSingleton();
			// data access object(s)
			bind(McmdbDAO.class)
				.asEagerSingleton();
			// other binding(s)
			bind(MessageDeduplicator.class)
				.to(McmdbMessageDeduplicator.class);
			bind(DroolsClassifier.class)
				.to(classForPrefix(DroolsClassifier.class,
					configuration.getProperty("drools_classifier")));
			bind(SophiaDroolsClassifier.class);
			// self
			bind(ResponseAdapter.class)
				.asEagerSingleton();
		}
		
	}

	public static void main(String[] args) {
		try {
			final ResponseAdapter instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/response-adapter.properties"))
					.getInstance(ResponseAdapter.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Properties configuration;
	private final Charset charset;
	private final Gson gson;
	private final S3 s3;
	private final SQS sqs;
	private final McmdbDAO dao;
	private final MessageDeduplicator messageDeduplicator;
	private final DroolsClassifier droolsClassifier;

	@Inject
	protected ResponseAdapter(@Named("configuration") Properties configuration,
			@Named("charset") Charset charset, Gson gson, S3 s3, SQS sqs,
			McmdbDAO dao, MessageDeduplicator messageDeduplicator,
			DroolsClassifier droolsClassifier) {
		super();
		this.configuration = configuration;
		this.charset = charset;
		this.gson = gson;
		this.s3 = s3;
		this.sqs = sqs;
		this.dao = dao;
		this.messageDeduplicator = messageDeduplicator;
		this.droolsClassifier = droolsClassifier;
	}
	
	public ResponseAdapter startup() throws Exception {
		if ("true".equalsIgnoreCase(configuration.getProperty("response_adapter.locked", "true"))) {
			throw new IllegalStateException("application locked");
		}
		s3.startup();
		sqs.startup();
		droolsClassifier.startup();
		return this;
	}

	public ResponseAdapter shutdown() {
		droolsClassifier.shutdown();
		sqs.shutdown();
		s3.shutdown();
		return this;
	}
	
	private ResponseAdapter process(String[] args) throws Exception {
		
		final int bindPort = Integer.parseInt(configuration
				.getProperty("response_adapter.bind_port", configuration
						.getProperty("default.bind_port", "0")));

		// bind lock tcp port
		try (final ServerSocket socket = new ServerSocket(bindPort)) {
			logger.info("socket bound to {}", socket.getLocalSocketAddress());
			
			// sqs polling thread(s)
			final ExecutorService executorService = Executors.newCachedThreadPool();
			
			// completed notification(s)
			executorService.execute(new Runnable() {
				
				@Override
				public void run() {
					try {
						pollCompletedQueue();
					} catch (Exception e) {
						logger.error("polling completed queue", e);
					}
				}
				
			});
			
			// failed notification(s)
			executorService.execute(new Runnable() {
				
				@Override
				public void run() {
					try {
						pollFailedQueue();					
					} catch (Exception e) {
						logger.error("polling failed queue", e);
					}
				}
				
			});

			// wait sqs polling thread(s) termination
			executorService.shutdown();
			while (!executorService.awaitTermination(1L, TimeUnit.MINUTES))
				logger.debug("waiting sqs polling thread(s) termination...");
			
		}

		return this;
	}

	public void parseOutputFile(File outputFile, String compression) throws Exception {

		// config parameter(s)
//		final String compression = configuration
//				.getProperty("response_adapter.file.compression");
		final char delimiter = configuration
				.getProperty("response_adapter.file.delimiter").charAt(0);
		final int columnIdCombana = Integer.parseInt(configuration
				.getProperty("response_adapter.file.column.id_combana"));
//		final int columnCodiceCombana = Integer.parseInt(configuration
//				.getProperty("response_adapter.file.column.codice_combana"));
//		final int columnTitolo = Integer.parseInt(configuration
//				.getProperty("response_adapter.file.column.titolo"));
//		final int columnAutori = Integer.parseInt(configuration
//				.getProperty("response_adapter.file.column.autori"));
//		final int columnCompositori = Integer.parseInt(configuration
//				.getProperty("response_adapter.file.column.compositori"));
//		final int columnInterpreti = Integer.parseInt(configuration
//				.getProperty("response_adapter.file.column.interpreti"));
		final int columnUtilizzazioni = Integer.parseInt(configuration
				.getProperty("response_adapter.file.column.utilizzazioni"));
		final int columnValoreEconomico = Integer.parseInt(configuration
				.getProperty("response_adapter.file.column.valore_economico"));
		final int columnIdCodifica = Integer.parseInt(configuration
				.getProperty("response_adapter.file.column.id_codifica"));
		final int columnResultJson = Integer.parseInt(configuration
				.getProperty("response_adapter.file.column.result_json"));
		int rowsToSkip = Integer.parseInt(configuration
				.getProperty("response_adapter.file.rows_to_skip", "0"));
		final Map<String, String> droolsMapping = GsonUtils
				.decodeJsonMap(configuration
					.getProperty("response_adapter.drools_mapping"));

		// heart beat
		final HeartBeat heartbeat = HeartBeat.constant("record", Integer.parseInt(configuration
				.getProperty("response_adapter.heartbeat", configuration.getProperty("default.heartbeat", "1000"))));

		// parse local file
		try (final CompressionAwareFileInputStream inputStream = new CompressionAwareFileInputStream(outputFile, compression);
				final InputStreamReader inputStreamReader = new InputStreamReader(inputStream, charset);
				final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				final CSVParser csvParser = new CSVParser(bufferedReader, CSVFormat.newFormat(delimiter)
						.withIgnoreEmptyLines()
						.withIgnoreSurroundingSpaces()
						.withQuoteMode(QuoteMode.MINIMAL)
						.withQuote('"'))) {

			final Iterator<CSVRecord> csvIterator = csvParser.iterator();

			// skip initial row(s)
			for ( ; rowsToSkip > 0 && csvIterator.hasNext(); rowsToSkip --) {
				final CSVRecord record = csvIterator.next();
				logger.debug("record skipped {}", record);
			}

			// loop on row(s)
			while (csvIterator.hasNext()) {
				final CSVRecord csvRecord = csvIterator.next();

				// parse record
				final String idCombana = csvRecord.get(columnIdCombana);
				final String idCodifica = csvRecord.get(columnIdCodifica);
				final String valoreEconomico = csvRecord.get(columnValoreEconomico);
				int nUtilizzazioni = 0;
				try {
					nUtilizzazioni = Integer.parseInt(csvRecord.get(columnUtilizzazioni));
				} catch (Exception e) {
					nUtilizzazioni = 0;
				}
				final String utilizzazioni = Integer.toString(nUtilizzazioni);
				final String resultJson = csvRecord.get(columnResultJson);

				// skip unidentified row(s)
				if (Strings.isNullOrEmpty(resultJson))
					continue;

				// decode json
				logger.debug("resultJson: {}", resultJson);
				final JsonArray resultArray = gson.fromJson(resultJson, JsonArray.class);
				final JsonObject firstResult = GsonUtils.getAsJsonObject(resultArray, 0);
				if (null == firstResult)
					continue;
				final String workCode = GsonUtils.getAsString(firstResult, "codiceOpera");
				final String firstScore = GsonUtils.getAsString(firstResult, "gradoDiConf");
				String scoreDelta = "100.0";
				final JsonObject secondResult = GsonUtils.getAsJsonObject(resultArray, 1);
				if (null != secondResult) {
					final String secodScore = GsonUtils.getAsString(secondResult, "gradoDiConf");
					scoreDelta = String.format("%.3f",
							Double.parseDouble(firstScore) - Double.parseDouble(secodScore));
				}
				logger.debug("workCode: {}", workCode);
				logger.debug("firstScore: {}", firstScore);
				logger.debug("scoreDelta: {}", scoreDelta);
				
				List<Map<String,Object>> proposals = new ArrayList<Map<String,Object>>();
				Type mapType = new TypeToken<Map<String, Object>>(){}.getType();
				for (JsonElement jsonElement : resultArray) {
					Map<String, Object> proposal = gson.fromJson(jsonElement, mapType);
					proposals.add(proposal);
				}
				
				// classify results with drools
				final String classification = droolsClassifier
						.getClassification(Double.parseDouble(firstScore),
								Double.parseDouble(scoreDelta),
								Double.parseDouble(Strings.isNullOrEmpty(valoreEconomico) ? "0.0" : valoreEconomico), proposals);
				final String tipoApprovazione = droolsMapping.get(classification);

				// statement paramter(s)
				final Map<String, String> parameters = new HashMap<>();
				parameters.put("idCombana", idCombana);
				parameters.put("idCodifica", idCodifica);
				parameters.put("valoreEconomico", valoreEconomico);
				parameters.put("nUtilizzazioni", utilizzazioni);
				parameters.put("candidate", MySqlUtils.escapeJson(resultJson));
				parameters.put("codiceOperaSuggerito", workCode);
				parameters.put("confidenza", firstScore);
				parameters.put("distSecCandidata", scoreDelta);
				parameters.put("tipoApprovazione", tipoApprovazione);

				// insert into perf_codifica
				if (Strings.isNullOrEmpty(idCodifica)) {				
					int count = dao.executeUpdate("response_adapter.sql.insert_perf_codifica", parameters);
					if (count < 1) {
						logger.warn("zero row(s) inserted executing statement: response_adapter.sql.insert_perf_codifica");
					}
				}

				// update perf_codifica
				else {
					int count = dao.executeUpdate("response_adapter.sql.update_perf_codifica", parameters);
					if (count < 1) {
						logger.warn("zero row(s) updated executing statement: response_adapter.sql.update_perf_codifica");
					}
				}

				heartbeat.pump();
			}
			
		}
		
	}

	private void pollCompletedQueue() throws Exception {
		
		// config parameter(s)
		final File workingDirectory = new File(configuration
				.getProperty("response_adapter.working_directory"));
		workingDirectory.mkdirs();
		final boolean deleteInputUrls = "true".equalsIgnoreCase(configuration
				.getProperty("response_adapter.delete_input_urls", "true"));
		final boolean deleteOutputUrls = "true".equalsIgnoreCase(configuration
				.getProperty("response_adapter.delete_output_urls", "true"));
		
		// sqs message pump(s)
		final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs,
				configuration, "response_adapter.sqs");
		final SqsMessagePump completedSqsMessagePump = new SqsMessagePump(sqs,
				configuration, "response_adapter.completed.sqs");
		completedSqsMessagePump.pollingLoop(messageDeduplicator, new SqsMessagePump.Consumer() {
			
			@Override
			public JsonObject getStartedMessagePayload(JsonObject message) {
				return null;
			}
			
			@Override
			public JsonObject getFailedMessagePayload(JsonObject message) {
				return null;
			}
			
			@Override
			public JsonObject getCompletedMessagePayload(JsonObject message) {
				return null;
			}
			
			@Override
			public boolean consumeMessage(JsonObject message) {
				
				// parse completed json message
				final JsonObject input = GsonUtils
						.getAsJsonObject(message, "input");
				final JsonObject output = GsonUtils
						.getAsJsonObject(message, "output");
				final JsonArray items = GsonUtils
						.getAsJsonArray(output, "items");
				final String compression = GsonUtils
						.getAsString(GsonUtils
							.getAsJsonObject(input, "body"), "compression");
				JsonObject error = null;

				// send started message
				sqsMessagePump.sendStartedMessage(input,
						SqsMessageHelper.formatContext(), false);

				// loop on item(s)
				for (JsonElement element : items) {
					final JsonObject item = element.getAsJsonObject();
					
					// delete input file
					if (deleteInputUrls) {
						final String inputUrl = GsonUtils
								.getAsString(item, "inputUrl");
						logger.debug("deleting input file: {}", inputUrl);
						if (!s3.delete(new S3.Url(inputUrl))) {
							logger.warn("error input deleting file: {}", inputUrl);
						}
					}

					// download, parse & delete output file
					final String outputUrl = GsonUtils
							.getAsString(item, "outputUrl");
					final File localFile = new File(workingDirectory,
							outputUrl.substring(1 + outputUrl.lastIndexOf('/')));
					localFile.deleteOnExit();
					logger.debug("downloading output file: {}", outputUrl);
					if (s3.download(new S3.Url(outputUrl), localFile)) {
						try {
							parseOutputFile(localFile, compression);
						} catch (Exception e) {
							logger.error("parsing output file", e);
							error = SqsMessageHelper.formatError(e);
							break;
						}
					} else {
						logger.warn("error downloading output file: {}", outputUrl);
						error = SqsMessageHelper.formatError("0", String
								.format("error downloading output file: %s", outputUrl));
						break;
					}
					localFile.delete();
					if (deleteOutputUrls) {
						logger.debug("deleting output file: {}", outputUrl);
						if (!s3.delete(new S3.Url(outputUrl))) {
							logger.warn("error deleting output file: {}", outputUrl);
						}
					}

				}

				// send completed/failed message
				if (null == error) {
					sqsMessagePump.sendCompletedMessage(input, output, false);
				} else {
					sqsMessagePump.sendFailedMessage(input, error, false);
				}				
				
				return false;
			}
			
		});
		
	}

	private void pollFailedQueue() throws Exception {
		
		// configuration
		final boolean deleteInputUrls = "true".equalsIgnoreCase(configuration
				.getProperty("response_adapter.delete_input_urls", "true"));
		final boolean deleteOutputUrls = "true".equalsIgnoreCase(configuration
				.getProperty("response_adapter.delete_output_urls", "true"));

		// sqs message pump(s)
		final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs,
				configuration, "response_adapter.sqs");
		final SqsMessagePump failedSqsMessagePump = new SqsMessagePump(sqs,
				configuration, "response_adapter.failed.sqs");
		failedSqsMessagePump.pollingLoop(messageDeduplicator, new SqsMessagePump.Consumer() {
			
			@Override
			public JsonObject getStartedMessagePayload(JsonObject message) {
				return null;
			}
			
			@Override
			public JsonObject getFailedMessagePayload(JsonObject message) {
				return null;
			}
			
			@Override
			public JsonObject getCompletedMessagePayload(JsonObject message) {
				return null;
			}
			
			@Override
			public boolean consumeMessage(JsonObject message) {

				// parse failed json message
				final JsonObject input = GsonUtils
						.getAsJsonObject(message, "input");
				final JsonArray items = GsonUtils
						.getAsJsonArray(GsonUtils
							.getAsJsonObject(input, "body"), "items");
				final JsonObject error = GsonUtils
						.getAsJsonObject(message, "error");

				// send started message
				sqsMessagePump.sendStartedMessage(input,
						SqsMessageHelper.formatContext(), false);

				// loop on item(s)
				for (JsonElement item : items) {

					// delete input file
					if (deleteInputUrls) {
						final String inputUrl = GsonUtils
								.getAsString(item.getAsJsonObject(), "inputUrl");
						logger.debug("deleting input file: {}", inputUrl);
						if (!s3.delete(new S3.Url(inputUrl))) {
							logger.warn("error deleting input file: {}", inputUrl);
						}
					}
					
					// delete output file
					if (deleteOutputUrls) {
						final String outputUrl = GsonUtils
								.getAsString(item.getAsJsonObject(), "outputUrl");
						logger.debug("deleting output file: {}", outputUrl);
						if (!s3.delete(new S3.Url(outputUrl))) {
							logger.warn("error deleting output file: {}", outputUrl);
						}
					}					
				}
				
				// send failed message
				sqsMessagePump.sendFailedMessage(input, error, false);
				
				return true;

			}
			
		});

	}

}

