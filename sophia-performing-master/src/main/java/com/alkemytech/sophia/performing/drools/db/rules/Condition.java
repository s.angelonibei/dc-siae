package com.alkemytech.sophia.performing.drools.db.rules;

public class Condition {

    private String field;
    private Relation relation;
    private Object value;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Relation getRelation() {
        return relation;
    }

    public void setOperator(Relation relation) {
        this.relation = relation;
    }

    public static enum Relation {
        GT(">"),
        LT("<"),
        GTE(">="),
        LTE("<="),
    	EQ("==");
    	
        private final String literal;

        private Relation(String literal) {
            this.literal = literal;
        }

		public String getLiteral() {
			return literal;
		}
    }

}
