package com.alkemytech.sophia.performing.drools.db.rules;

import java.util.HashMap;
import java.util.Map;

public enum Action {
	AUTO("auto", "AUTOMATICA"), MANUAL("manual", "MANUALE");

	private final String key;
	private final String literal;

	private Action(String key, String literal) {
		this.key = key;
		this.literal = literal;
	}

	public String getLiteral() {
		return literal;
	}

	@Override
	public String toString() {
		return literal;
	}

	private static Map<String, Action> mapping = new HashMap<String, Action>();

	static {
		for (Action a : values()) {
			mapping.put(a.key, a);
		}
	}

	public static Action fromKey(String key) {
		Action action = mapping.get(key);
		if (action == null) {
			throw new IllegalArgumentException(key);
		} else {
			return action;
		}
	}

}