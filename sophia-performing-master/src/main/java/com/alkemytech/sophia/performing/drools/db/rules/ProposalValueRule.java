package com.alkemytech.sophia.performing.drools.db.rules;

public class ProposalValueRule extends Rule {

	@Override
	public String activation() {
        StringBuilder statementBuilder = new StringBuilder();                

        for (Condition condition : getConditions()) {
        	
        	statementBuilder.append("get(\"");

            statementBuilder.append(condition.getField()).append("\") ").append(condition.getRelation().getLiteral()).append(" ");
            
            statementBuilder.append(condition.getValue());

            statementBuilder.append(", ");
        }

        String statement = statementBuilder.toString();

        return statement.substring(0, statement.length() - 2);
	}
	

}
