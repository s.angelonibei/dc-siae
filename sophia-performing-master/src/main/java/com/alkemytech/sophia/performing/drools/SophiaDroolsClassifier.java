package com.alkemytech.sophia.performing.drools;


import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.drools.runtime.StatefulKnowledgeSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.entity.performing.MetadatiRisultatiCodifica;
import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.performing.jdbc.McmdbDAO;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class SophiaDroolsClassifier extends DroolsClassifier {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Properties configuration;
	private final S3 s3;
	private final McmdbDAO dao;
	private final String propertyPrefix;
	
	private StatefulKnowledgeSession session;
	
	@Inject
	public SophiaDroolsClassifier(@Named("configuration") Properties configuration,
			S3 s3, McmdbDAO dao,
			@Named("drools_classifier") String propertyPrefix) throws IOException, SQLException {
		super();
		this.configuration = configuration;
		this.s3 = s3;
		this.dao = dao;
		this.propertyPrefix = propertyPrefix;
	}
	
	@Override
	public void startup() throws IOException, SQLException {
		final Map<String, String> row = dao
				.executeSingleRowQuery(propertyPrefix + ".sql.select_file_url");
		if (null == row)
			throw new SQLException("decision table url not found"); 
		final String fileUrl = row.get("fileUrl");
		logger.debug("fileUrl: {}", fileUrl);
		final File workingDirectory = new File(configuration
				.getProperty(propertyPrefix + ".working_directory"));
		final File xlsFile = new File(workingDirectory,
				UUID.randomUUID().toString() + ".xls");
		xlsFile.deleteOnExit();
		logger.debug("xlsFile: {} ({})", xlsFile,
				TextUtils.formatSize(xlsFile.length()));
		if (!s3.download(new S3.Url(fileUrl), xlsFile))
			throw new IOException(String
					.format("error downloading: %s", fileUrl)); 
		if (!xlsFile.exists())
			throw new IOException(String
					.format("file not found: %s", xlsFile.getAbsolutePath())); 
		session = getKnowledgeBaseFromXls(xlsFile)
				.newStatefulKnowledgeSession();
	}
	
	@Override
	public String getClassification(double sogliaConf, double distSecond, double ecoValue) {
		final MetadatiRisultatiCodifica event = new MetadatiRisultatiCodifica();
		event.setSogliaConf(sogliaConf);
		event.setDistSecond(distSecond);
		event.setEcoValue(ecoValue);
		session.insert(event);
		session.fireAllRules();
		return event.getAzione();
	}

}


