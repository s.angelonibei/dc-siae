package com.alkemytech.sophia.performing;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import javax.sql.DataSource;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.QuoteMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.io.CompressionAwareFileOutputStream;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.performing.jdbc.McmdbDAO;
import com.alkemytech.sophia.performing.jdbc.McmdbDataSource;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class RequestAdapter {
	
	private static final Logger logger = LoggerFactory.getLogger(RequestAdapter.class);

	private static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.toInstance(new S3(configuration));
			bind(SQS.class)
				.toInstance(new SQS(configuration));
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("MCMDB"))
				.to(McmdbDataSource.class)
				.asEagerSingleton();
			// data access object(s)
			bind(McmdbDAO.class)
				.asEagerSingleton();
			// self
			bind(RequestAdapter.class)
				.asEagerSingleton();
		}
		
	}

	public static void main(String[] args) {
		try {
			final RequestAdapter instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/request-adapter.properties"))
					.getInstance(RequestAdapter.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Properties configuration;
	private final Charset charset;
	private final S3 s3;
	private final SQS sqs;
	private final DataSource dataSource;

	@Inject
	protected RequestAdapter(@Named("configuration") Properties configuration,
			@Named("charset") Charset charset, S3 s3, SQS sqs,
			@Named("MCMDB") DataSource dataSource) {
		super();
		this.configuration = configuration;
		this.charset = charset;
		this.s3 = s3;
		this.sqs = sqs;
		this.dataSource = dataSource;
	}
	
	public RequestAdapter startup() {
		if ("true".equalsIgnoreCase(configuration.getProperty("request_adapter.locked", "false"))) {
			throw new IllegalStateException("application locked");
		}
		s3.startup();
		sqs.startup();
		return this;
	}

	public RequestAdapter shutdown() {
		sqs.shutdown();
		s3.shutdown();
		return this;
	}
	
	private RequestAdapter process(String[] args) throws Exception {
		
		final int bindPort = Integer.parseInt(configuration
				.getProperty("request_adapter.bind_port", configuration
						.getProperty("default.bind_port", "0")));

		// bind lock tcp port
		try (final ServerSocket socket = new ServerSocket(bindPort)) {
			logger.info("socket bound to {}", socket.getLocalSocketAddress());
			
			// process database record(s)
			process();

		}

		return this;
	}

	private void process() throws Exception {
		
		final long startTimeMillis = System.currentTimeMillis();

		// config parameter(s)
		final File workingDirectory = new File(configuration
				.getProperty("request_adapter.working_directory"));
		workingDirectory.mkdirs();
		final String version = configuration
				.getProperty("request_adapter.message.version", "1");
		final String compression = configuration
				.getProperty("request_adapter.message.compression", "guess");
		final String configUrl = configuration
				.getProperty("request_adapter.message.config_url");
		final String inputUrl = configuration
				.getProperty("request_adapter.message.input_url");
		final String outputUrl = configuration
				.getProperty("request_adapter.message.output_url");
		final String startedQueue = configuration
				.getProperty("request_adapter.message.started_queue");
		final String completedQueue = configuration
				.getProperty("request_adapter.message.completed_queue");
		final String failedQueue = configuration
				.getProperty("request_adapter.message.failed_queue");
		final int maxItems = TextUtils.parseIntSize(configuration
				.getProperty("request_adapter.message.max_items", "10K"));
		final int fileMaxRows = TextUtils.parseIntSize(configuration
				.getProperty("request_adapter.file.max_rows", "10K"));
		final String filename = configuration
				.getProperty("request_adapter.file.filename");
		final char delimiter = configuration
				.getProperty("request_adapter.file.delimiter").charAt(0);
		final String[] fileColumns = configuration
				.getProperty("request_adapter.file.columns").split(",");
		final int fetchSize = TextUtils.parseIntSize(configuration
				.getProperty("request_adapter.sql.fetch_size", "4096"));
		
		// heart beat
		final HeartBeat heartbeat = HeartBeat.constant("record", Integer.parseInt(configuration
				.getProperty("request_adapter.heartbeat", configuration.getProperty("default.heartbeat", "1000"))));
		
		// sqs message pump
		final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "request_adapter.sqs");

		// connect to db
		try (final Connection connection = dataSource.getConnection()) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			connection.setAutoCommit(true);
			final Statement statement;
			if (fetchSize < 0) {
				statement = connection.createStatement();						
			} else {
				statement = connection
						.createStatement(ResultSet.TYPE_FORWARD_ONLY,
								ResultSet.CONCUR_READ_ONLY);
				statement.setFetchSize(fetchSize);
			}
			// sql query
			final String sql = configuration.getProperty("request_adapter.sql.select_combana");
			logger.debug("executing sql {}", sql);
			final Map<String, String> record = new HashMap<>();
			// select combana
			boolean resultSetExhausted = false;
			try (final ResultSet resultSet = statement.executeQuery(sql)) {
				final ResultSetMetaData metadata = resultSet.getMetaData();
				final int columnCount = metadata.getColumnCount();

				// local csv file(s)
				final List<File> localFiles = new ArrayList<>(maxItems);
				for (int itemNumber = 0; !resultSetExhausted; itemNumber ++) {
					
					// create csv file
					final File csvFile = new File(workingDirectory, filename
							.replace("{itemNumber}", Integer.toString(itemNumber))
							.replace("{uuid}", UUID.randomUUID().toString()));
					csvFile.deleteOnExit();
					try (final CompressionAwareFileOutputStream outputStream = new CompressionAwareFileOutputStream(csvFile);
						final OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, charset);
						final BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
						final CSVPrinter csvPrinter = new CSVPrinter(bufferedWriter,  CSVFormat.newFormat(delimiter)
								.withQuoteMode(QuoteMode.MINIMAL)
								.withQuote('"')
								.withRecordSeparator('\n'))) {

						// print header
						for (String fileColumn : fileColumns) {
							csvPrinter.print(fileColumn);
						}
						csvPrinter.println();

						// loop on selected combana
						int rows = 0;
						while (rows < fileMaxRows) {
							if (!resultSet.next()) {
								resultSetExhausted = true;
								break;
							}
							// build record map
							record.clear();
							for (int i = 1; i <= columnCount; i ++) {
								final String column = resultSet.getString(i);
								if (null != column)
									record.put(metadata.getColumnLabel(i), column);
							}
							// print row(s)
							for (String fileColumn : fileColumns) {
								csvPrinter.print(record.get(fileColumn));
							}
							csvPrinter.println();
							heartbeat.pump();
							rows ++;
						}
						
						// append created file to local csv file(s) array
						if (rows > 0)
							localFiles.add(csvFile);

					}
					
					// format & send sqs message
					if (localFiles.size() >= maxItems || resultSetExhausted) {

						// upload file(s) to s3
						final JsonArray items = new JsonArray();
						for (File localFile : localFiles) {

							// upload file to s3
							final String remoteFileUrl = inputUrl
									.replace("{filename}", localFile.getName());
							logger.debug("uploading file {} to {}", localFile, remoteFileUrl);
							if (!s3.upload(new S3.Url(remoteFileUrl), localFile)) {
								logger.error("file upload error: {}", remoteFileUrl);
								throw new IOException(String
										.format("file upload error: %s", remoteFileUrl));
							}

							// format json item
							final JsonObject item = new JsonObject();
							item.addProperty("inputUrl", remoteFileUrl);
							item.addProperty("outputUrl", outputUrl
									.replace("{filename}", localFile.getName()));
							items.add(item);

							// delete local file
							localFile.delete();
							
						}

						// sql query
						final String sqlConfigurazione = configuration.getProperty("request_adapter.sql.select_configurazione");
						logger.debug("executing sqlConfigurazione {}", sqlConfigurazione);
						final Map<String, String> recordConfigurazione = new HashMap<>();

						// send sqs message
						final JsonObject body = new JsonObject();

						try (final ResultSet resultSetConfigurazione = statement.executeQuery(sqlConfigurazione)) {
							final JsonObject configuration = new JsonObject();
							while (resultSetConfigurazione.next()) {

								switch (resultSetConfigurazione.getString(3)) {
									case "Decimal":
										configuration.addProperty(resultSetConfigurazione.getString(1), resultSetConfigurazione.getDouble(2));
										break;
									case "Boolean":
										configuration.addProperty(resultSetConfigurazione.getString(1), resultSetConfigurazione.getInt(2));
										break;
									case "Integer":
										configuration.addProperty(resultSetConfigurazione.getString(1), resultSetConfigurazione.getInt(2));
										break;
									default:
										configuration.addProperty(resultSetConfigurazione.getString(1), resultSetConfigurazione.getString(2));
								}

							}
							body.add("configuration", configuration);
						}


						body.addProperty("version", version);
						body.addProperty("compression", compression);
						body.addProperty("configUrl", configUrl);
						body.add("items", items);
						if (!Strings.isNullOrEmpty(startedQueue)) {
							body.addProperty("startedQueue", startedQueue);
						}
						if (!Strings.isNullOrEmpty(completedQueue)) {
							body.addProperty("completedQueue", completedQueue);
						}
						if (!Strings.isNullOrEmpty(failedQueue)) {
							body.addProperty("failedQueue", failedQueue);
						}
						logger.debug("sending sqs message with body {}", new GsonBuilder()
								.disableHtmlEscaping().setPrettyPrinting().create().toJson(body));
						sqsMessagePump.sendToProcessMessage(body, false);

						// clear local csv file(s) array
						localFiles.clear();

					}
					
				}
			}		
			
		}

		logger.info("{} records processed", heartbeat.getTotalPumps());
		logger.info("adapter completed in {}",
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

	}
	
}

