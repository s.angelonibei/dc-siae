package com.alkemytech.sophia.performing;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.io.CompressionAwareFileInputStream;
import com.alkemytech.sophia.commons.io.CompressionAwareFileOutputStream;
import com.alkemytech.sophia.commons.sqs.McmdbMessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.performing.jdbc.McmdbDAO;
import com.alkemytech.sophia.performing.jdbc.McmdbDataSource;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.*;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

public class MassiveRequestAdapter {

    private static final Logger logger = LoggerFactory.getLogger(MassiveRequestAdapter.class);

    private static class GuiceModuleExtension extends GuiceModule {

        public GuiceModuleExtension(String[] args, String resourceName) {
            super(args, resourceName);
        }

        @Override
        protected void configure() {
            super.configure();
            // amazon service(s)
            bind(S3.class)
                    .toInstance(new S3(configuration));
            bind(SQS.class)
                    .toInstance(new SQS(configuration));
            // data source(s)
            bind(DataSource.class)
                    .annotatedWith(Names.named("MCMDB"))
                    .to(McmdbDataSource.class)
                    .asEagerSingleton();
            // data access object(s)
            bind(McmdbDAO.class)
                    .asEagerSingleton();

            bind(MessageDeduplicator.class)
                    .to(McmdbMessageDeduplicator.class);
            // self
            bind(MassiveRequestAdapter.class)
                    .asEagerSingleton();
        }

    }

    public static void main(String[] args) {
        try {
            final MassiveRequestAdapter instance = Guice
                    .createInjector(new GuiceModuleExtension(args,
                            "/massive-request-adapter.properties"))
                    .getInstance(MassiveRequestAdapter.class)
                    .startup();
            try {
                instance.process(args);
            } finally {
                instance.shutdown();
            }
        } catch (Throwable e) {
            logger.error("main", e);
        } finally {
            System.exit(0);
        }
    }

    private final Properties configuration;
    private final Charset charset;
    private final S3 s3;
    private final SQS sqs;
    private final MessageDeduplicator messageDeduplicator;
    private final DataSource dataSource;

    @Inject
    protected MassiveRequestAdapter(@Named("configuration") Properties configuration,
                                    @Named("charset") Charset charset, S3 s3, SQS sqs,
                                    MessageDeduplicator messageDeduplicator,
                                    @Named("MCMDB") DataSource dataSource) {
        super();
        this.configuration = configuration;
        this.charset = charset;
        this.s3 = s3;
        this.sqs = sqs;
        this.messageDeduplicator = messageDeduplicator;
        this.dataSource = dataSource;
    }

    public MassiveRequestAdapter startup() {
        if ("true".equalsIgnoreCase(configuration.getProperty("request_adapter.locked", "false"))) {
            throw new IllegalStateException("application locked");
        }
        s3.startup();
        sqs.startup();
        return this;
    }

    public MassiveRequestAdapter shutdown() {
        sqs.shutdown();
        s3.shutdown();
        return this;
    }

    private MassiveRequestAdapter process(String[] args) throws Exception {

        final int bindPort = Integer.parseInt(configuration
                .getProperty("request_adapter.bind_port", configuration
                        .getProperty("default.bind_port", "0")));

        // bind lock tcp port
        try (final ServerSocket socket = new ServerSocket(bindPort)) {
            logger.info("socket bound to {}", socket.getLocalSocketAddress());

            // process database record(s)
            process();

        }

        return this;
    }

    private void process() throws Exception {

        final long startTimeMillis = System.currentTimeMillis();

        // config parameter(s)
        final File workingDirectory = new File(configuration
                .getProperty("request_adapter.working_directory"));
        workingDirectory.mkdirs();
        final String version = configuration
                .getProperty("request_adapter.message.version", "1");
        final String compression = configuration
                .getProperty("request_adapter.message.compression", "guess");
        final String configUrl = configuration
                .getProperty("request_adapter.message.config_url");
        final String inputUrl = configuration
                .getProperty("request_adapter.message.input_url");
        final String outputUrl = configuration
                .getProperty("request_adapter.message.output_url");
        final String startedQueue = configuration
                .getProperty("request_adapter.message.started_queue");
        final String completedQueue = configuration
                .getProperty("request_adapter.message.completed_queue");
        final String failedQueue = configuration
                .getProperty("request_adapter.message.failed_queue");
        final int maxItems = TextUtils.parseIntSize(configuration
                .getProperty("request_adapter.message.max_items", "10K"));
        final int fileMaxRows = TextUtils.parseIntSize(configuration
                .getProperty("request_adapter.file.max_rows", "10K"));
        final String filename = configuration
                .getProperty("request_adapter.file.filename");


        // heart beat
        final HeartBeat heartbeat = HeartBeat.constant("record", Integer.parseInt(configuration
                .getProperty("request_adapter.heartbeat", configuration.getProperty("default.heartbeat", "1000"))));

        // sqs message pump
        final SqsMessagePump toProcessCoreIdentification = new SqsMessagePump(sqs, configuration, "core_identification.sqs");


        final SqsMessagePump toProcessRequestAdapter = new SqsMessagePump(sqs, configuration, "request_adapter.sqs");
        toProcessRequestAdapter.pollingLoop(messageDeduplicator, new SqsMessagePump.Consumer() {
            @Override
            public JsonObject getStartedMessagePayload(JsonObject message) {
                return null;
            }

            @Override
            public boolean consumeMessage(JsonObject message) {
                JsonArray items = GsonUtils.getAsJsonArray(GsonUtils.getAsJsonObject(message, "body"), "items");

                if (items == null || items.size() != 1)
                    return false;

                String inputFile = GsonUtils.getAsString(GsonUtils.getAsJsonObject(items.get(0)), "inputUrl");


                //TODO sqsMessagePump.sendStartedMessage

                try (final Connection connection = dataSource.getConnection()) {
                    logger.debug("jdbc connected to {} {}", connection.getMetaData()
                            .getDatabaseProductName(), connection.getMetaData().getURL());
                    connection.setAutoCommit(true);
                    final Statement statement = connection.createStatement();


                    S3.Url s3url = new S3.Url(inputFile);
                    File fileToWork = new File(workingDirectory, s3url.key);
                    fileToWork.deleteOnExit();
                    s3.download(s3url, fileToWork);
                    CompressionAwareFileInputStream compressionAwareFileInputStream = new CompressionAwareFileInputStream(fileToWork);
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(compressionAwareFileInputStream, charset));

                    String line = bufferedReader.readLine();
                    String header = line;
                    List<File> localFiles = new ArrayList<>();

                    for (int itemNumber = 0; line != null; itemNumber++) {
                        final File csvFile = new File(workingDirectory, filename
                                .replace("{itemNumber}", Integer.toString(itemNumber))
                                .replace("{uuid}", UUID.randomUUID().toString()));
                        csvFile.deleteOnExit();
                        try (final CompressionAwareFileOutputStream outputStream = new CompressionAwareFileOutputStream(csvFile);
                             final OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, charset);
                             final BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter)) {

                            bufferedWriter.write(header + "\n");

                            int rows = 0;
                            while (rows < fileMaxRows) {
                                line = bufferedReader.readLine();

                                if (line == null)
                                    break;

                                bufferedWriter.write(line + "\n");
                                rows++;
                            }

                            if (rows > 0)
                                localFiles.add(csvFile);

                        }
                    }

                    if (!localFiles.isEmpty()) {

                        // upload file(s) to s3
                        final JsonArray items2 = new JsonArray();
                        for (File localFile : localFiles) {

                            // upload file to s3
                            final String remoteFileUrl = inputUrl
                                    .replace("{filename}", localFile.getName());
                            logger.debug("uploading file {} to {}", localFile, remoteFileUrl);
                            if (!s3.upload(new S3.Url(remoteFileUrl), localFile)) {
                                logger.error("file upload error: {}", remoteFileUrl);
                                throw new IOException(String
                                        .format("file upload error: %s", remoteFileUrl));
                            }

                            // format json item
                            final JsonObject item = new JsonObject();
                            item.addProperty("inputUrl", remoteFileUrl);
                            item.addProperty("outputUrl", outputUrl
                                    .replace("{filename}", localFile.getName()));
                            items2.add(item);

                            // delete local file
                            localFile.delete();

                        }


                        // send sqs message
                        final JsonObject body2 = new JsonObject();


                        final String sqlConfigurazione = configuration.getProperty("request_adapter.sql.select_configurazione");
                        logger.debug("executing sqlConfigurazione {}", sqlConfigurazione);

                        try (final ResultSet resultSetConfigurazione = statement.executeQuery(sqlConfigurazione)) {
                            final JsonObject configuration = new JsonObject();
                            while (resultSetConfigurazione.next()) {

                                switch (resultSetConfigurazione.getString(3)) {
                                    case "Decimal":
                                        configuration.addProperty(resultSetConfigurazione.getString(1), resultSetConfigurazione.getDouble(2));
                                        break;
                                    case "Boolean":
                                        configuration.addProperty(resultSetConfigurazione.getString(1), resultSetConfigurazione.getInt(2));
                                        break;
                                    case "Integer":
                                        configuration.addProperty(resultSetConfigurazione.getString(1), resultSetConfigurazione.getInt(2));
                                        break;
                                    default:
                                        configuration.addProperty(resultSetConfigurazione.getString(1), resultSetConfigurazione.getString(2));
                                }

                            }
                            body2.add("configuration", configuration);
                        }


                        body2.addProperty("version", version);
                        body2.addProperty("compression", compression);
                        body2.addProperty("configUrl", configUrl);
                        body2.add("input", message);
                        body2.add("items", items2);
                        if (!Strings.isNullOrEmpty(startedQueue)) {
                            body2.addProperty("startedQueue", startedQueue);
                        }
                        if (!Strings.isNullOrEmpty(completedQueue)) {
                            body2.addProperty("completedQueue", completedQueue);
                        }
                        if (!Strings.isNullOrEmpty(failedQueue)) {
                            body2.addProperty("failedQueue", failedQueue);
                        }
                        logger.debug("sending sqs message with body {}", new GsonBuilder()
                                .disableHtmlEscaping().setPrettyPrinting().create().toJson(body2));
                        toProcessCoreIdentification.sendToProcessMessage(body2, false);

                        // clear local csv file(s) array
                        localFiles.clear();

                    }

                    logger.info("{} records processed", heartbeat.getTotalPumps());
                    logger.info("adapter completed in {}",
                            TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

                } catch (Exception e) {
                    logger.error("", e);
                }

                return false;
            }

            @Override
            public JsonObject getCompletedMessagePayload(JsonObject message) {
                return null;
            }

            @Override
            public JsonObject getFailedMessagePayload(JsonObject message) {
                return null;
            }
        });


    }


}

