package com.alkemytech.sophia.utilizer.service.radioinstore;

import com.alkemytech.sophia.utilizer.model.radioinstore.PerfPalinsesto;
import com.alkemytech.sophia.utilizer.model.radioinstore.PerfRsKpiEntity;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import org.apache.commons.lang.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.math.BigDecimal;
import java.util.*;

@Singleton
public class RadioInStoreKpiService {

    private final Provider<EntityManager> provider;

    @Inject
    public RadioInStoreKpiService(Provider<EntityManager> provider) {
        this.provider = provider;
    }

    public List<PerfRsKpiEntity> listaIndicatori(Long idMusicProvider, Long idPalinsesto, Integer anno, Integer mese) {

        EntityManager entityManager = provider.get();
        List<PerfRsKpiEntity> listaIndicatori;

        String q = "select b " +
                "from PerfRsKpiEntity b " +
                "WHERE b.idMusicProvider = :idMusicProvider " +
                "AND b.idPalinsesto = :idPalinsesto " +
                "AND b.anno=:anno " +
                "AND b.mese=:mese ";

        TypedQuery<PerfRsKpiEntity> queryKPI = entityManager.createQuery(q, PerfRsKpiEntity.class);

        listaIndicatori = queryKPI
                .setParameter("idMusicProvider", idMusicProvider)
                .setParameter("idPalinsesto", idPalinsesto)
                .setParameter("anno", anno)
                .setParameter("mese", mese)
                .getResultList();

        return listaIndicatori;
    }


    public Map<String, PerfRsKpiEntity> mapIndicatori(Long idMusicProvider, Long idPalinsesto, Integer anno, Integer mese) {
        Map<String, PerfRsKpiEntity> mapIndicatori = new HashMap<String, PerfRsKpiEntity>();

        List<PerfRsKpiEntity> listaIndicatori = listaIndicatori(idMusicProvider, idPalinsesto, anno, mese);
        for(PerfRsKpiEntity indicatore : listaIndicatori){
            if(StringUtils.isNotBlank(indicatore.getIndicatore())){
                mapIndicatori.put(indicatore.getIndicatore(), indicatore);
            }
        }
        return mapIndicatori;
    }

    public boolean saveIndicatori(List<PerfRsKpiEntity> listaIndicatoriDaPersistere) {
        EntityManager em = provider.get();
        try{
            em.getTransaction().begin();
            for(PerfRsKpiEntity indicatore : listaIndicatoriDaPersistere){
                if(indicatore != null){
                    em.merge(indicatore);
                    em.flush();
                }else{
                    throw new Exception();
                }
            }
            em.getTransaction().commit();
            return true;
        }catch(Exception e){
            if(em.getTransaction().isActive()){
                em.getTransaction().rollback();
            }
        }
        return false;
    }

    public boolean evaluateIndicatori(PerfPalinsesto palinsesto, Integer anno, Integer mese) {
        if(palinsesto == null || anno == null || mese == null || mese < 1 || mese > 12){
            return false;
        }

        Long recordScartati = calcolaRecordScartati(palinsesto.getId(), anno, mese);
        if(recordScartati == null){
            return false;
        }
        Long recordTotali = calcolaRecordTotali(palinsesto.getId(), anno, mese);
        if(recordTotali == null){
            return false;
        }
        Long recordInRitardo = calcolaInRitardo(palinsesto.getId(), anno, mese);
        if(recordInRitardo == null){
            return false;
        }

        Long durataBrani = calcolaDurataBrani(palinsesto.getId(), anno, mese);
        if(durataBrani == null){
            return false;
        }

        Long durataBraniTitoliErronei = calcolaDurataBraniTitoliErronei(palinsesto.getId(), anno, mese);
        if(durataBraniTitoliErronei == null){
            return false;
        }

        Long durataBraniAutoriErronei = calcolaDurataAutoriTitoliErronei(palinsesto.getId(), anno, mese);
        if(durataBraniAutoriErronei == null){
            return false;
        }

        Map<String, Long> mapIndicatoriRicalcolati = new HashMap<String, Long>();
        mapIndicatoriRicalcolati.put(PerfRsKpiEntity.RECORD_SCARTATI, recordScartati);
        mapIndicatoriRicalcolati.put(PerfRsKpiEntity.RECORD_TOTALI, recordTotali);
        mapIndicatoriRicalcolati.put(PerfRsKpiEntity.RECORD_IN_RITARDO, recordInRitardo);
        mapIndicatoriRicalcolati.put(PerfRsKpiEntity.DURATA_BRANI, durataBrani);
        mapIndicatoriRicalcolati.put(PerfRsKpiEntity.DURATA_BRANI_TITOLI_ERRONEI, durataBraniTitoliErronei);
        mapIndicatoriRicalcolati.put(PerfRsKpiEntity.DURATA_BRANI_AUTORI_ERRONEI, durataBraniAutoriErronei);

        Map<String, PerfRsKpiEntity> mapIndicatori = mapIndicatori(palinsesto.getIdMusicProvider(), palinsesto.getId(), anno, mese);

        List<PerfRsKpiEntity> listaIndicatoriDaPersistere =
                listaIndicatoriDaPersistere(mapIndicatori, mapIndicatoriRicalcolati, palinsesto.getIdMusicProvider(), palinsesto.getId(), anno, mese);

        return saveIndicatori(listaIndicatoriDaPersistere);

    }

    private Long calcolaRecordScartati(Long idPalinsesto, Integer anno, Integer mese) {
        EntityManager entityManager = provider.get();
        String q =    " SELECT COUNT(*) FROM( "
                + " SELECT DISTINCT t.ID_RS_ERR_SHOW_SCHEDULE "
                + " FROM PERF_RS_ERR_SHOW_SCHEDULE t "
                + " LEFT JOIN PERF_RS_ERR_SHOW_MUSIC m "
                + " ON t.ID_RS_ERR_SHOW_SCHEDULE = m.ID_RS_ERR_SHOW_SCHEDULE "
                + " LEFT JOIN PERF_RS_UTILIZATION_NORMALIZED_FILE z "
                + " ON t.ID_NORMALIZED_FILE = z.ID_NORMALIZED_FILE "
                + " LEFT JOIN PERF_RS_UTILIZATION_FILE b "
                + " ON z.ID_UTILIZATION_FILE = b.id "
                + " WHERE ((t.ID_PALINSESTO = ?1 AND b.ID_PALINSESTO IS NULL) OR b.ID_PALINSESTO = ?1) "
                + " AND ((t.SCHEDULE_YEAR  = ?2 AND b.anno IS NULL) OR b.anno = ?2) "
                + "	AND ((t.SCHEDULE_MONTH = ?3 AND b.mese IS NULL) OR b.mese = ?3) "
                + " GROUP BY t.ID_RS_ERR_SHOW_SCHEDULE) as conteggio ";

        Query query = entityManager.createNativeQuery(q);
        Object ret = query.setParameter(1, idPalinsesto)
                .setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();

        Long recordScartati = new Long(0);
        if(ret != null && ret instanceof Long){
            recordScartati = (Long)ret;
        }
        return recordScartati;
    }

    private Long calcolaRecordTotali(Long idPalinsesto, Integer anno, Integer mese) {
        EntityManager entityManager = provider.get();
        String q = "select count(*) "
                + " FROM PERF_RS_SHOW_SCHEDULE t left join PERF_RS_SHOW_MUSIC m on t.ID_RS_SHOW_SCHEDULE = m.ID_RS_SHOW_SCHEDULE "
                + " WHERE t.ID_PALINSESTO = ?1 "
                + " AND t.SCHEDULE_YEAR = ?2"
                + " AND t.SCHEDULE_MONTH = ?3";

        Query query = entityManager.createNativeQuery(q);
        Object ret = query.setParameter(1, idPalinsesto)
                .setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();

        Long recordValidi = new Long(0);
        if(ret != null && ret instanceof Long){
            recordValidi = (Long)ret;
        }
        return recordValidi;
    }

    private Long calcolaInRitardo(Long idCanale, Integer anno, Integer mese) {
        EntityManager entityManager = provider.get();
        String q = "SELECT count(*) "
                + " FROM PERF_RS_SHOW_SCHEDULE t LEFT JOIN PERF_RS_SHOW_MUSIC m ON t.ID_RS_SHOW_SCHEDULE = m.ID_RS_SHOW_SCHEDULE "
                + " WHERE t.ID_PALINSESTO = ?1 "
                + " AND t.SCHEDULE_YEAR = ?2"
                + " AND t.SCHEDULE_MONTH = ?3 "
                + " AND t.DAYS_FROM_UPLOAD > ?4";

        Query query = entityManager.createNativeQuery(q);

        Object ret = query.setParameter(1, idCanale)
                .setParameter(2, anno)
                .setParameter(3, mese)
                .setParameter(4, 90)
                .getSingleResult();

        Long recordInRitardo = new Long(0);
        if(ret != null && ret instanceof Long){
            recordInRitardo = (Long)ret;
        }
        return recordInRitardo;
    }

    private Long calcolaDurataBrani(Long idPalinsesto, Integer anno, Integer mese) {
        EntityManager entityManager = provider.get();
        String q = "SELECT sum(st.DURATION) "
                + " FROM PERF_RS_SHOW_SCHEDULE t, PERF_RS_SHOW_MUSIC st "
                + " WHERE t.ID_RS_SHOW_SCHEDULE = st.ID_RS_SHOW_SCHEDULE "
                + " AND t.ID_PALINSESTO = ?1 "
                + " AND t.SCHEDULE_YEAR = ?2 "
                + " AND t.SCHEDULE_MONTH = ?3 ";

        Query query = entityManager.createNativeQuery(q);

        Object ret = query.setParameter(1, idPalinsesto)
                .setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();

        Long durata = new Long(0);
        if(ret != null && ret instanceof BigDecimal){
            durata = ((BigDecimal)ret).longValue();
        }
        return durata;
    }

    private Long calcolaDurataBraniTitoliErronei(Long idPalinsesto, Integer anno, Integer mese) {
        EntityManager entityManager = provider.get();
        String q = "SELECT sum(PERF_RS_SHOW_MUSIC.DURATION) "
                + " FROM PERF_RS_ERR_BRANO_TITOLO, PERF_RS_SHOW_SCHEDULE "
                + " LEFT JOIN PERF_RS_SHOW_MUSIC "
                + " ON PERF_RS_SHOW_SCHEDULE.ID_RS_SHOW_SCHEDULE = PERF_RS_SHOW_MUSIC.ID_RS_SHOW_SCHEDULE "
                + " WHERE PERF_RS_SHOW_SCHEDULE.ID_PALINSESTO = ?1 "
                + " AND PERF_RS_SHOW_SCHEDULE.SCHEDULE_YEAR = ?2 "
                + " AND PERF_RS_SHOW_SCHEDULE.SCHEDULE_MONTH = ?3 "
                + " AND LOWER(PERF_RS_SHOW_MUSIC.TITLE) IN (LOWER(PERF_RS_ERR_BRANO_TITOLO.RS_ERR_BRANO_TITOLO)) ";

        Query query = entityManager.createNativeQuery(q);

        Object ret = query.setParameter(1, idPalinsesto)
                .setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();

        Long durata = new Long(0);
        if(ret != null && ret instanceof BigDecimal){
            durata = ((BigDecimal)ret).longValue();
        }
        return durata;
    }

    private Long calcolaDurataAutoriTitoliErronei(Long idPalinsesto, Integer anno, Integer mese) {
        EntityManager entityManager = provider.get();
        String q = "SELECT sum(PERF_RS_SHOW_MUSIC.DURATION) "
                + " FROM PERF_RS_ERR_COMPOSITORE_TITOLO, PERF_RS_SHOW_SCHEDULE "
                + " LEFT JOIN PERF_RS_SHOW_MUSIC "
                + " ON PERF_RS_SHOW_SCHEDULE.ID_RS_SHOW_SCHEDULE = PERF_RS_SHOW_MUSIC.ID_RS_SHOW_SCHEDULE "
                + " WHERE PERF_RS_SHOW_SCHEDULE.ID_PALINSESTO = ?1 "
                + " AND PERF_RS_SHOW_SCHEDULE.SCHEDULE_YEAR = ?2 "
                + " AND PERF_RS_SHOW_SCHEDULE.SCHEDULE_MONTH = ?3 "
                + " AND LOWER(PERF_RS_SHOW_MUSIC.TITLE) IN (LOWER(PERF_RS_ERR_COMPOSITORE_TITOLO.RS_ERR_COMPOSITORE_TITOLO)) ";

        Query query = entityManager.createNativeQuery(q);

        Object ret = query.setParameter(1, idPalinsesto)
                .setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();

        Long durata = new Long(0);
        if(ret != null && ret instanceof BigDecimal){
            durata = ((BigDecimal)ret).longValue();
        }
        return durata;
    }

    private List<PerfRsKpiEntity> listaIndicatoriDaPersistere(
            Map<String, PerfRsKpiEntity> mapIndicatori, Map<String, Long> mapIndicatoriRicalcolati,
            Long idMusicProvider, Long idPalinsesto, Integer anno, Integer mese)
    {
        List<PerfRsKpiEntity> listaIndicatoriDaPersistere = new ArrayList<PerfRsKpiEntity>();

        Set<String> keySet = mapIndicatoriRicalcolati.keySet();
        for(String keyIndicatore : keySet){
            PerfRsKpiEntity indicatore = null;
            Long valoreIndicatore = mapIndicatoriRicalcolati.get(keyIndicatore);
            if(mapIndicatori.containsKey(keyIndicatore)){
                indicatore = mapIndicatori.get(keyIndicatore);
                //aggiorno il valore dell'indicatore e la data di ultima modifica
                indicatore.setValore(valoreIndicatore);
                indicatore.setLastUpdate(new Date());
            }else{
                indicatore = new PerfRsKpiEntity();
                indicatore.setIdMusicProvider(idMusicProvider);
                indicatore.setIdPalinsesto(idPalinsesto);
                indicatore.setAnno(anno);
                indicatore.setMese(mese);
                indicatore.setIndicatore(keyIndicatore);
                indicatore.setValore(valoreIndicatore);
                indicatore.setLastUpdate(new Date());
            }
            listaIndicatoriDaPersistere.add(indicatore);
        }
        return listaIndicatoriDaPersistere;
    }
}