package com.alkemytech.sophia.utilizer.service;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Alessandro Russo on 05/12/2018.
 */
@Singleton
public class DbService {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    Provider<EntityManager> provider;

    private Map<String, String> queriesMap;
    private Map<String, String> fileStatusMap;

    @Inject
    public DbService(Provider<EntityManager> provider,
                     @Named("utilization.query.json") String harmonizationQueryJson,
                     @Named("file.status.json") String fileStatusJson) {

        this.provider = provider;
        Gson gson = new Gson();
        queriesMap = StringUtils.isNotEmpty(harmonizationQueryJson) ? gson.fromJson(harmonizationQueryJson, Map.class) : new HashMap<>();
        fileStatusMap = StringUtils.isNotEmpty(fileStatusJson) ? gson.fromJson(fileStatusJson, Map.class) : new HashMap<>();
    }

    public List<Object[]> retrieveFileToProcess() throws Exception {
        if( fileStatusMap.containsKey("to.process") ){
            return retrieveUtilizationFile(fileStatusMap.get("to.process"));
        }
        throw new Exception("PROPERTY -> to.process NOT FOUND in fileStatusJson");
    }

    public List<Object[]> retrieveValidatedFile() throws Exception {
        if( fileStatusMap.containsKey("validated") ){
            return retrieveUtilizationFile(fileStatusMap.get("validated"));
        }
        throw new Exception("PROPERTY -> validated NOT FOUND in fileStatusJson");
    }

    public List<Object[]> retrieveErrorFile() throws Exception {
        if( fileStatusMap.containsKey("error") ){
            return retrieveUtilizationFile(fileStatusMap.get("error"));
        }
        throw new Exception("PROPERTY -> error NOT FOUND in fileStatusJson");
    }

    private List<Object[]> retrieveUtilizationFile(String fileStatus) throws Exception {
        if( queriesMap.containsKey("retrieve.utilization.file.query")){
            EntityManager entityManager = provider.get();
            final Query q = entityManager.createNativeQuery(queriesMap.get("retrieve.utilization.file.query"));
            List<Object[]> queryResult = q.setParameter(1, fileStatus).getResultList();
            return queryResult;
        }
        throw new Exception("PROPERTY -> retrieve.utilization.file.query NOT FOUND in harmonizationQueryJson");
    }

    public boolean updateFileStatus(Long idFile, String status) throws Exception {
        if( queriesMap.containsKey("update.utilization.file.status.query")){
            EntityManager entityManager = provider.get();
            openTransaction(entityManager);
            try{
                final Query q = entityManager.createNativeQuery(queriesMap.get("update.utilization.file.status.query"));
                int result = q.setParameter(1, status).setParameter(2, new Date()).setParameter(3, idFile).executeUpdate();
                commitTransaction(entityManager);
                return result > 0;
            }
            catch (Exception e){
                logger.error( "Error updating Utilization File Status", e );
                rollbackTransaction(entityManager);
            }
            return false;
        }
        throw new Exception("PROPERTY -> update.utilization.file.status.query  NOT FOUND in harmonizationQueryJson");
    }

    private void openTransaction( EntityManager em ){
        if( !em.getTransaction().isActive() ){
            em.getTransaction().begin();
        }
    }

    private void commitTransaction( EntityManager em ){
        if( em.getTransaction().isActive() ){
            em.getTransaction().commit();
        }
    }

    private void rollbackTransaction( EntityManager em ){
        if( em.getTransaction().isActive() ){
            em.getTransaction().rollback();
        }
    }


}