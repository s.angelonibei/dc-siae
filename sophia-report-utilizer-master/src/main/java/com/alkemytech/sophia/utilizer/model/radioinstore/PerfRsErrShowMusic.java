package com.alkemytech.sophia.utilizer.model.radioinstore;

import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.tools.StringTools;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import static com.alkemytech.sophia.utilizer.utily.Utilities.getMapValue;

@Entity
@Table(name="PERF_RS_ERR_SHOW_MUSIC")
@NamedQuery(name="PerfRsErrShowMusic.findAll", query="SELECT p FROM PerfRsErrShowMusic p")
public class PerfRsErrShowMusic implements Serializable {

    @Id
    @GeneratedValue( strategy =  GenerationType.IDENTITY)
    @Column( name= "ID_RS_ERR_SHOW_MUSIC")
    private Long id;

    @Column(name = "ID_RS_ERR_SHOW_SCHEDULE")
    private Long idErrRdShowSchedule;

    @Column(name = "ID_NORMALIZED_FILE")
    private Long idNormalizedFile;

    @Column( name = "ID_MUSIC_TYPE")
    private Long idMusicType;

    @Column( name = "TITLE")
    private String title;

    @Column( name= "COMPOSER")
    private String composer;

    @Column( name= "PERFORMER")
    private String performer;

    @Column( name= "ALBUM")
    private String album;

    @Column( name = "DURATION")
    private Integer duration;

    @Column( name= "ISWC_CODE")
    private String iswcCode;

    @Column( name= "ISRC_CODE")
    private String isrcCode;

    @Column( name = "BEGIN_TIME")
    private Date beginTime;

    @Column( name = "REAL_DURATION")
    private Integer realDuration;

    @Column( name = "REPORT_DURATION")
    private String reportDuration;

    @Column( name = "REPORT_BEGIN_TIME")
    private String reportBeginTime;

    @Column( name = "REPORT_MUSIC_TYPE")
    private String reportMusicType;

    @Column( name = "DAYS_FROM_UPLOAD")
    private Integer daysFromUpload;

    @Column( name = "CREATION_DATE")
    private Date creationDate;

    @Column( name = "MODIFY_DATE")
    private Date modifyDate;

    public PerfRsErrShowMusic() {
    }

    public PerfRsErrShowMusic(PerfRSNormalizedFile normalizedFile, Date beginTimeOp, Map<String, Object> record) {
        this.idMusicType = getMapValue(record, Constants.DECODE_ID_CATEGORIA_USO_OP) != null ? Long.parseLong(getMapValue(record, Constants.DECODE_ID_CATEGORIA_USO_OP)) : null;
        this.idNormalizedFile = normalizedFile.getIdNormalizedFile();
        this.title = getMapValue(record, Constants.TITOLO_OP);
        this.composer = getMapValue(record,Constants.COMPOSITORE_OP);
        this.performer = getMapValue(record,Constants.ESECUTORE_OP);
        this.album = getMapValue(record,Constants.ALBUM_OP);
        if( getMapValue(record, Constants.DURATA_OP) != null && getMapValue(record, Constants.DURATA_OP).matches(StringTools.DURATION_FORMAT) ){
            this.duration = Integer.parseInt( StringTools.parseTimeToSeconds( getMapValue(record, Constants.DURATA_OP) ) ) ;
        }
        if( getMapValue(record, Constants.DURATA_EFFETTIVA_OP) != null && getMapValue(record, Constants.DURATA_EFFETTIVA_OP).matches(StringTools.DURATION_FORMAT) ){
            this.realDuration = Integer.parseInt( StringTools.parseTimeToSeconds( getMapValue(record, Constants.DURATA_EFFETTIVA_OP) ) ) ;
        }
        this.iswcCode = getMapValue(record,Constants.CODICE_ISWC_OP);
        this.isrcCode = getMapValue(record,Constants.CODICE_ISRC_OP);
        this.beginTime = beginTime;
        this.reportDuration = getMapValue(record, Constants.DURATA_OP);
        this.reportMusicType = getMapValue(record, Constants.CATEGORIA_USO_OP);
        this.reportBeginTime = getMapValue(record, Constants.ORARIO_INIZIO_OP);
        this.creationDate = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdErrRdShowSchedule() {
        return idErrRdShowSchedule;
    }

    public void setIdErrRdShowSchedule(Long idErrRdShowSchedule) {
        this.idErrRdShowSchedule = idErrRdShowSchedule;
    }

    public Long getIdNormalizedFile() {
        return idNormalizedFile;
    }

    public void setIdNormalizedFile(Long idNormalizedFile) {
        this.idNormalizedFile = idNormalizedFile;
    }

    public Long getIdMusicType() {
        return idMusicType;
    }

    public void setIdMusicType(Long idMusicType) {
        this.idMusicType = idMusicType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getComposer() {
        return composer;
    }

    public void setComposer(String composer) {
        this.composer = composer;
    }

    public String getPerformer() {
        return performer;
    }

    public void setPerformer(String performer) {
        this.performer = performer;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getIswcCode() {
        return iswcCode;
    }

    public void setIswcCode(String iswcCode) {
        this.iswcCode = iswcCode;
    }

    public String getIsrcCode() {
        return isrcCode;
    }

    public void setIsrcCode(String isrcCode) {
        this.isrcCode = isrcCode;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Integer getRealDuration() {
        return realDuration;
    }

    public void setRealDuration(Integer realDuration) {
        this.realDuration = realDuration;
    }

    public String getReportDuration() {
        return reportDuration;
    }

    public void setReportDuration(String reportDuration) {
        this.reportDuration = reportDuration;
    }

    public String getReportBeginTime() {
        return reportBeginTime;
    }

    public void setReportBeginTime(String reportBeginTime) {
        this.reportBeginTime = reportBeginTime;
    }

    public String getReportMusicType() {
        return reportMusicType;
    }

    public void setReportMusicType(String reportMusicType) {
        this.reportMusicType = reportMusicType;
    }

    public Integer getDaysFromUpload() {
        return daysFromUpload;
    }

    public void setDaysFromUpload(Integer daysFromUpload) {
        this.daysFromUpload = daysFromUpload;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    public String toString() {
        return "PerfRsErrShowMusic{" +
                "id=" + id +
                ", idErrRdShowSchedule=" + idErrRdShowSchedule +
                ", idNormalizedFile=" + idNormalizedFile +
                ", idMusicType=" + idMusicType +
                ", title='" + title + '\'' +
                ", composer='" + composer + '\'' +
                ", performer='" + performer + '\'' +
                ", album='" + album + '\'' +
                ", duration=" + duration +
                ", iswcCode='" + iswcCode + '\'' +
                ", isrcCode='" + isrcCode + '\'' +
                ", beginTime=" + beginTime +
                ", realDuration=" + realDuration +
                ", reportDuration='" + reportDuration + '\'' +
                ", reportBeginTime='" + reportBeginTime + '\'' +
                ", reportMusicType='" + reportMusicType + '\'' +
                ", daysFromUpload=" + daysFromUpload +
                ", creationDate=" + creationDate +
                ", modifyDate=" + modifyDate +
                '}';
    }
}