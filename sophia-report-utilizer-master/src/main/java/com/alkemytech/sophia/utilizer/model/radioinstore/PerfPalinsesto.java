package com.alkemytech.sophia.utilizer.model.radioinstore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by Alessandro Russo on 10/12/2018.
 */
@Entity
@Table( name = "PERF_PALINSESTO")
public class PerfPalinsesto {

    @Id
    @Column(name = "ID_PALINSESTO")
    private Long id;

    @Column(name = "ID_MUSIC_PROVIDER")
    private Long idMusicProvider;

    @Column(name = "NOME")
    private String nome;

    @Column(name = "CODICE_DITTA")
    private String codiceDitta;

    @Column(name = "CODICE_ID")
    private String codiceId;

    @Column(name = "DATA_INIZIO_VALIDITA")
    private Date dataInizioValid;

    @Column(name = "DATA_FINE_VALIDITA")
    private Date dataFineValid;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdMusicProvider() {
        return idMusicProvider;
    }

    public void setIdMusicProvider(Long idMusicProvider) {
        this.idMusicProvider = idMusicProvider;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodiceDitta() {
        return codiceDitta;
    }

    public void setCodiceDitta(String codiceDitta) {
        this.codiceDitta = codiceDitta;
    }

    public String getCodiceId() {
        return codiceId;
    }

    public void setCodiceId(String codiceId) {
        this.codiceId = codiceId;
    }

    public Date getDataInizioValid() {
        return dataInizioValid;
    }

    public void setDataInizioValid(Date dataInizioValid) {
        this.dataInizioValid = dataInizioValid;
    }

    public Date getDataFineValid() {
        return dataFineValid;
    }

    public void setDataFineValid(Date dataFineValid) {
        this.dataFineValid = dataFineValid;
    }

    @Override
    public String toString() {
        return "PerfPalinsesto{" +
                "id=" + id +
                ", idMusicProvider=" + idMusicProvider +
                ", nome='" + nome + '\'' +
                ", codiceDitta='" + codiceDitta + '\'' +
                ", codiceId='" + codiceId + '\'' +
                ", dataInizioValid=" + dataInizioValid +
                ", dataFineValid=" + dataFineValid +
                '}';
    }
}
