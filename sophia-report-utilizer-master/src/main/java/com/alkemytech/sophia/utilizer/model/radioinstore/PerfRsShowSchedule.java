package com.alkemytech.sophia.utilizer.model.radioinstore;

import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.tools.StringTools;
import com.amazonaws.util.CollectionUtils;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.alkemytech.sophia.utilizer.utily.Utilities.getMapValue;

@XmlRootElement
@Entity
@Table(name = "PERF_RS_SHOW_SCHEDULE")
public class PerfRsShowSchedule implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column( name="ID_RS_SHOW_SCHEDULE", nullable = false)
    private Long id;

    @OneToMany(cascade = {CascadeType.ALL})
    @JoinColumn(name = "ID_RS_SHOW_SCHEDULE")
    List<PerfRsShowMusic> rsShowMusicList;

    @Column(name = "ID_NORMALIZED_FILE")
    private Long idNormalizedFile;

    @Column(name = "ID_PALINSESTO")
    private Long idPalinsesto;

    @Column( name="TITLE")
    private String title;

    @Column( name = "BEGIN_TIME")
    private Date beginTime;

    @Column( name = "END_TIME")
    private Date endTime;

    @Column( name = "DURATION")
    private Integer duration;

    @Column( name = "REGIONAL_OFFICE")
    private String regionalOffice;

    @Column( name = "NOTE")
    private String note;

    @Column( name = "OVERLAP" )
    private boolean overlap;

    @Column( name = "SCHEDULE_YEAR")
    private Integer scheduleYear;

    @Column( name = "SCHEDULE_MONTH")
    private Integer scheduleMonth;

    @Column( name = "DAYS_FROM_UPLOAD")
    private Integer daysFromUpload;

    @Column( name = "CREATION_DATE")
    private Date creationDate;

    @Column( name = "MODIFY_DATE")
    private Date modifyDate;

    public PerfRsShowSchedule() {
    }

    public PerfRsShowSchedule(PerfRsShowMusic rsShowMusic, PerfRSNormalizedFile normalizedFile, Date beginTimeTx, Date endTimeTx, Map<String, Object> record) {
        this.getRsShowMusicList().add(rsShowMusic);
        this.idPalinsesto = getMapValue(record, Constants.DECODE_ID_CANALE) != null ? Long.parseLong( getMapValue(record, Constants.DECODE_ID_CANALE) ) : null;
        this.idNormalizedFile = normalizedFile.getIdNormalizedFile();
        this.beginTime = beginTimeTx;
        this.endTime = endTimeTx;
        if( getMapValue(record, Constants.DURATA_TX) != null && getMapValue(record, Constants.DURATA_TX).matches(StringTools.DURATION_FORMAT) ){
            this.duration = Integer.parseInt( StringTools.parseTimeToSeconds( getMapValue(record, Constants.DURATA_TX) ) ) ;
        }
        this.title = getMapValue(record, Constants.TITOLO_TX);
        this.regionalOffice = getMapValue(record, Constants.DIFFUSIONE_REGIONALE);
        this.creationDate = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<PerfRsShowMusic> getRsShowMusicList() {
        if ( rsShowMusicList == null ){
            rsShowMusicList = new ArrayList<>();
        }
        return rsShowMusicList;
    }

    public void setRsShowMusicList(List<PerfRsShowMusic> rsShowMusicList) {
        this.rsShowMusicList = rsShowMusicList;
    }

    public Long getIdNormalizedFile() {
        return idNormalizedFile;
    }

    public void setIdNormalizedFile(Long idNormalizedFile) {
        this.idNormalizedFile = idNormalizedFile;
    }

    public Long getIdPalinsesto() {
        return idPalinsesto;
    }

    public void setIdPalinsesto(Long idPalinsesto) {
        this.idPalinsesto = idPalinsesto;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getRegionalOffice() {
        return regionalOffice;
    }

    public void setRegionalOffice(String regionalOffice) {
        this.regionalOffice = regionalOffice;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isOverlap() {
        return overlap;
    }

    public void setOverlap(boolean overlap) {
        this.overlap = overlap;
    }

    public Integer getScheduleYear() {
        return scheduleYear;
    }

    public void setScheduleYear(Integer scheduleYear) {
        this.scheduleYear = scheduleYear;
    }

    public Integer getScheduleMonth() {
        return scheduleMonth;
    }

    public void setScheduleMonth(Integer scheduleMonth) {
        this.scheduleMonth = scheduleMonth;
    }

    public Integer getDaysFromUpload() {
        return daysFromUpload;
    }

    public void setDaysFromUpload(Integer daysFromUpload) {
        this.daysFromUpload = daysFromUpload;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Date getDate(){
        Date ret = this.getBeginTime();
        if( ret == null ){
            for(PerfRsShowMusic music : this.getRsShowMusicList()){
                if(music.getBeginTime() != null){
                    ret = music.getBeginTime();
                    break;
                }
            }
        }
        return ret;
    }

    @Override
    public String toString() {
        return "PerfRsShowSchedule{" +
                "id=" + id +
                ", rsShowMusicList=" + rsShowMusicList +
                ", idNormalizedFile=" + idNormalizedFile +
                ", idPalinsesto=" + idPalinsesto +
                ", title='" + title + '\'' +
                ", beginTime=" + beginTime +
                ", endTime=" + endTime +
                ", duration=" + duration +
                ", regionalOffice='" + regionalOffice + '\'' +
                ", note='" + note + '\'' +
                ", overlap=" + overlap +
                ", scheduleYear=" + scheduleYear +
                ", scheduleMonth=" + scheduleMonth +
                ", daysFromUpload=" + daysFromUpload +
                ", creationDate=" + creationDate +
                ", modifyDate=" + modifyDate +
                '}';
    }
}