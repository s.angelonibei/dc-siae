package com.alkemytech.sophia.utilizer.service;

import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.mapper.TemplateMapper;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.utilizer.model.generic.FieldError;
import com.alkemytech.sophia.utilizer.model.generic.HarmonizationConfig;
import com.alkemytech.sophia.utilizer.model.generic.NormalizedReport;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.util.CollectionUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Alessandro Russo on 10/12/2018.
 */
@Singleton
public class NormalizedFileService {

    Logger logger = LoggerFactory.getLogger(getClass());

    protected DbService dbService;
    protected S3Service s3Service;

    @Inject
    public NormalizedFileService(DbService dbService, S3Service s3Service) {
        this.dbService = dbService;
        this.s3Service = s3Service;
    }

    // -- CONCRETE METHODS
    public boolean changeUtilizationFileStatus(Long idFile, String fileStatus) throws Exception {
        return dbService.updateFileStatus(idFile, fileStatus);
    }

    public AmazonS3URI reserveSingleFile(String filePath, String suffix){
        if( StringUtils.isNotEmpty( filePath ) ){
            AmazonS3URI amazonS3URI = new AmazonS3URI(filePath);
            if( !s3Service.lockFile(amazonS3URI.getBucket(), amazonS3URI.getKey(), suffix) ){
                logger.debug("File {} preaviously reserved by other process", amazonS3URI.getKey());
                return null;
            }
            logger.debug("Reserved file at {}", filePath);
            return amazonS3URI;
        }
        return null;
    }

    public void releaseSingleFile(String bucket, String key, String suffix){
        if(s3Service.doesObjectExist(bucket, key+suffix)){
            s3Service.delete(bucket, key+suffix);
            logger.debug("Released file at {}", bucket+key);
        }
        logger.debug("Doesn't exist file at {}", bucket+key+suffix);
    }

    public List<Object> parseFile(String fileBucket, String writeConfigPath, String wrireConfigStream ) {
        try{
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            AmazonS3URI amazonS3URI = new AmazonS3URI(fileBucket);
            TemplateMapper mapper = new TemplateMapper();
            try{
                s3Service.download(amazonS3URI.getBucket(), amazonS3URI.getKey(), bos);
                return mapper.parseFile(writeConfigPath, wrireConfigStream, bos, s3Service);
            }
            finally {
                bos.flush();
                bos.close();
            }
        }
        catch( Exception e ){
            logger.error("Error parsing normalized file", e);
        }
        return null;
    }

    public boolean containsErrors(Map<String, Object> currentRow){
        if( currentRow.containsKey(Constants.ERRORS) && StringUtils.isNotEmpty((String) currentRow.get(Constants.ERRORS)) ){
            ObjectMapper mapper = new ObjectMapper();
            try {
                Map<String, List<FieldError>> errorList = mapper.readValue((String) currentRow.get(Constants.ERRORS), Map.class);
                for(String key : errorList.keySet()){
                    if(!CollectionUtils.isNullOrEmpty(errorList.get(key))){
                        return true;
                    }
                }
            } catch (IOException e) {
                logger.error(e.getMessage());
                return true;
            }
        }
        return false;
    }
}
