package com.alkemytech.sophia.utilizer.utily;

import com.alkemytech.sophia.common.tools.DateTools;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;

/**
 * Created by Alessandro Russo on 28/11/2017.
 */
public class Utilities {
    //DATE FORMAT
    public static final String DATE_ENG_FORMAT_SLASH        = "MM/dd/yyyy";
    public static final String DATE_ENG_FORMAT_DASH         = "MM-dd-yyyy";
    public static final String DATE_ITA_FORMAT_SLASH        = "dd/MM/yyyy";
    public static final String DATE_ITA_FORMAT_DASH         = "dd-MM-yyyy";

    //DATETIME FORMAT
    public static final String DATETIME_ENG_FORMAT_SLASH    = "MM/dd/yyyy HH:mm:ss";
    public static final String DATETIME_ENG_FORMAT_DASH     = "MM-dd-yyyy HH:mm:ss";
    public static final String DATETIME_ITA_FORMAT_SLASH    = "dd/MM/yyyy HH:mm:ss";
    public static final String DATETIME_ITA_FORMAT_DASH     = "dd-MM-yyyy HH:mm:ss";
    public static final String DATETIME_MYSQL_FORMAT        = "yyyy-MM-dd HH:mm:ss";

    //TIME FORMAT
    public static final String TIME_SIMPLE_FORMAT           = "HH:mm";
    public static final String TIME_COMPLETE_FORMAT         = "HH:mm:ss";

    public static String dateToString (Date date, String format) {
        DateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }

    public static Date stringToDate (String dateString, String format) {
        DateFormat df = new SimpleDateFormat(format);
        try {
            return df.parse(dateString);
        } catch (ParseException e) {
            return null;
        }
    }

    public static Date manageDate (Date inputDate, int quantity, int timeUnit){
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(inputDate);
        calendar.add(timeUnit, quantity);
        return calendar.getTime();
    }

    public static String getMapValue(Map<String, Object> record, String key){
        if( record.containsKey(key) && StringUtils.isNotEmpty( (String) record.get(key) ) ){
            return (String) record.get( key );
        }
        return null;
    }

    public static boolean isFilleMap(Map<String, Object> record){
        for( Object obj : record.values() ){
            if(obj != null && !(obj instanceof String) ) {
                return true;
            }
            else if(obj != null &&  obj instanceof String && StringUtils.isNotEmpty( ( (String) obj ).trim() ) ){
                return true;
            }
        }
        return false;
    }

    public static Date getDateFromMonthAndYear( Integer month, Integer year){
        Calendar cal = Calendar.getInstance();
        cal.set(year, month-1,1,0,0,0);
        cal.set(Calendar.MILLISECOND,0);
        return cal.getTime();
    }

    public static Date getMonthStart(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),1,0,0,0);
        return cal.getTime();
    }

    public static Date getMonthEnd(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int day = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),day,23,59,59);
        return cal.getTime();
    }

    public static boolean isBetweenInclusive(Date input, Date start, Date end){
        return input.compareTo(start) * end.compareTo(input) >= 0;
    }

    public static boolean isBeetweenExclusive(Date input, Date start, Date end){
        return ( input.after(start) && input.before(end) );
    }

    public static int getDateField(Date input, int dateField){
        Calendar cal = Calendar.getInstance();
        cal.setTime(input);
        return  cal.get(dateField);
    }

    public static Integer extractAnnoFromMeseAnno(String meseAnno) {
        Integer iAnno = null;
        if(meseAnno != null){
            try{
                String anno = meseAnno.substring(0, meseAnno.lastIndexOf("-"));
                iAnno = Integer.parseInt(anno);
            }catch(Exception e){
                return null;
            }
        }
        return iAnno;
    }

    public static Integer extractMeseFromMeseAnno(String meseAnno) {
        Integer iMese = null;
        if(meseAnno != null){
            try{
                String mese = meseAnno.substring(meseAnno.lastIndexOf("-")+1);
                iMese = Integer.parseInt(mese);
            }catch(Exception e){
                return null;
            }
        }
        return iMese;
    }
}