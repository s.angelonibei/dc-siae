package com.alkemytech.sophia.utilizer.model.radioinstore;

import com.alkemytech.sophia.utilizer.model.generic.NormalizedReport;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alessandro Russo on 07/12/2018.
 */
@Entity
@Table(name = "PERF_RS_NORMALIZED_FILE")
public class PerfRSNormalizedFile extends NormalizedReport {

    @OneToMany( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinTable( name = "PERF_RS_UTILIZATION_NORMALIZED_FILE",
            joinColumns = {@JoinColumn( name = "ID_NORMALIZED_FILE" )},
            inverseJoinColumns = {@JoinColumn( name = "ID_UTILIZATION_FILE" )} )
    private List<PerfRSUtilizationFile> utilizationFiles;

    public List<PerfRSUtilizationFile> getUtilizationFiles() {
        if( utilizationFiles == null ){
            utilizationFiles = new ArrayList<>();
        }
        return utilizationFiles;
    }

    public void setUtilizationFiles(List<PerfRSUtilizationFile> utilizationFiles) {
        this.utilizationFiles = utilizationFiles;
    }
}