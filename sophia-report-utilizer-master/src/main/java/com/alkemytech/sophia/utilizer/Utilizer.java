package com.alkemytech.sophia.utilizer;

import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.utilizer.guice.McmdbJpaModule;
import com.alkemytech.sophia.utilizer.model.generic.HarmonizationConfig;
import com.alkemytech.sophia.utilizer.model.generic.NormalizedReport;
import com.alkemytech.sophia.utilizer.service.NormalizedFileService;
import com.alkemytech.sophia.utilizer.service.interfaces.RecordService;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.util.CollectionUtils;
import com.google.gson.Gson;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;
import com.google.inject.persist.PersistService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.ServerSocket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Utilizer {

    private static final Logger logger = LoggerFactory.getLogger(Utilizer.class);
    private static String recordServiceImplementation;
    private static Integer utilizationListenPort;

    private NormalizedFileService normalizedFileService;
    private RecordService recordService;

    @Inject
    public Utilizer(@Named("listenPort") Integer utilizationListenPort,
                    @Named("record.service.implementation") String recordServiceImplementation,
                    NormalizedFileService normalizedFileService){
        this.utilizationListenPort = utilizationListenPort;
        this.recordServiceImplementation = recordServiceImplementation;
        this.normalizedFileService = normalizedFileService;
    }

    public static void main(String[] args) {
        try {
            final  Injector injector = Guice.createInjector(new McmdbJpaModule(args));
            try{
                //Setup services
                injector.getInstance(PersistService.class).start();
                injector.getInstance(S3Service.class).startup();
                Utilizer utilizer = injector.getInstance(Utilizer.class);

                utilizer.recordService = (RecordService) injector.getInstance(Class.forName(recordServiceImplementation));
                //Run business logic
                utilizer.recordService.process( utilizationListenPort );
            }
            finally {
                injector.getInstance(PersistService.class).stop();
                injector.getInstance(S3Service.class).shutdown();
            }
        }
        catch (Exception e){
            e.printStackTrace();
            logger.error("main", e);
        }
        finally {
            System.exit(0);
        }
    }
}