package com.alkemytech.sophia.utilizer.model.radioinstore;

import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.tools.StringTools;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.alkemytech.sophia.utilizer.utily.Utilities.getMapValue;

@XmlRootElement
@Entity
@Table(name = "PERF_RS_ERR_SHOW_SCHEDULE")
public class PerfRsErrShowSchedule implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column( name="ID_RS_ERR_SHOW_SCHEDULE")
    private Long id;

    @OneToMany(cascade = {CascadeType.ALL})
    @JoinColumn(name = "ID_RS_ERR_SHOW_SCHEDULE")
    List<PerfRsErrShowMusic> errRsShowMusicList;

    @Column(name = "ID_NORMALIZED_FILE")
    private Long idNormalizedFile;

    @Column(name = "ID_PALINSESTO")
    private Long idPalinsesto;

    @Column( name="TITLE")
    private String title;

    @Column( name = "BEGIN_TIME")
    private Date beginTime;

    @Column( name = "END_TIME")
    private Date endTime;

    @Column( name = "DURATION")
    private Integer duration;

    @Column( name = "REGIONAL_OFFICE")
    private String regionalOffice;

    @Column( name = "ERROR")
    private String error;

    @Column( name = "GLOBAL_ERROR")
    private String globalError;

    @Column( name = "REPORT_PALINSESTO")
    private String reportPalinsesto;

    @Column( name = "REPORT_BEGIN_DATE")
    private String reportBeginDate;

    @Column( name = "REPORT_BEGIN_TIME")
    private String reportBeginTime;

    @Column( name = "REPORT_DURATION")
    private String reportDuration;

    @Column( name = "SCHEDULE_YEAR")
    private Integer schedulYear;

    @Column( name = "SCHEDULE_MONTH")
    private Integer scheduleMonth;

    @Column( name = "DAYS_FROM_UPLOAD")
    private Integer daysFromUpload;

    @Column( name = "CREATION_DATE")
    private Date creationDate;

    @Column( name = "MODIFY_DATE")
    private Date modifyDate;

    public PerfRsErrShowSchedule() {
    }

    public PerfRsErrShowSchedule(PerfRsErrShowMusic rsErrShowMusic, PerfRSNormalizedFile normalizedFile, Date beginTimeTx, Date endTimeTx, Map<String, Object> record) {
        this.getErrRsShowMusicList().add(rsErrShowMusic);
        this.idPalinsesto = getMapValue(record, Constants.DECODE_ID_CANALE) != null ? Long.parseLong( getMapValue(record, Constants.DECODE_ID_CANALE) ) : null;
        this.idNormalizedFile = normalizedFile.getIdNormalizedFile();
        this.beginTime = beginTimeTx;
        this.endTime = endTimeTx;
        if( getMapValue(record, Constants.DURATA_TX) != null && getMapValue(record, Constants.DURATA_TX).matches(StringTools.DURATION_FORMAT) ){
            this.duration = Integer.parseInt( StringTools.parseTimeToSeconds( getMapValue(record, Constants.DURATA_TX) ) ) ;
        }
        this.title = getMapValue(record, Constants.TITOLO_TX);
        this.regionalOffice = getMapValue(record, Constants.DIFFUSIONE_REGIONALE);
        this.error = getMapValue(record, Constants.ERRORS);
        this.reportPalinsesto = getMapValue(record, Constants.CANALE);
        this.reportBeginDate = getMapValue(record, Constants.DATA_INIZIO_TX);
        this.reportBeginTime = getMapValue(record, Constants.ORARIO_INIZIO_TX);
        this.reportDuration = getMapValue(record, Constants.DURATA_TX);
        this.creationDate = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<PerfRsErrShowMusic> getErrRsShowMusicList() {
        if( errRsShowMusicList == null ){
            errRsShowMusicList = new ArrayList<>();
        }
        return errRsShowMusicList;
    }

    public void setErrRsShowMusicList(List<PerfRsErrShowMusic> errRsShowMusicList) {
        this.errRsShowMusicList = errRsShowMusicList;
    }

    public Long getIdNormalizedFile() {
        return idNormalizedFile;
    }

    public void setIdNormalizedFile(Long idNormalizedFile) {
        this.idNormalizedFile = idNormalizedFile;
    }

    public Long getIdPalinsesto() {
        return idPalinsesto;
    }

    public void setIdPalinsesto(Long idPalinsesto) {
        this.idPalinsesto = idPalinsesto;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getRegionalOffice() {
        return regionalOffice;
    }

    public void setRegionalOffice(String regionalOffice) {
        this.regionalOffice = regionalOffice;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getGlobalError() {
        return globalError;
    }

    public void setGlobalError(String globalError) {
        this.globalError = globalError;
    }

    public String getReportPalinsesto() {
        return reportPalinsesto;
    }

    public void setReportPalinsesto(String reportPalinsesto) {
        this.reportPalinsesto = reportPalinsesto;
    }

    public String getReportBeginDate() {
        return reportBeginDate;
    }

    public void setReportBeginDate(String reportBeginDate) {
        this.reportBeginDate = reportBeginDate;
    }

    public String getReportBeginTime() {
        return reportBeginTime;
    }

    public void setReportBeginTime(String reportBeginTime) {
        this.reportBeginTime = reportBeginTime;
    }

    public String getReportDuration() {
        return reportDuration;
    }

    public void setReportDuration(String reportDuration) {
        this.reportDuration = reportDuration;
    }

    public Integer getSchedulYear() {
        return schedulYear;
    }

    public void setSchedulYear(Integer schedulYear) {
        this.schedulYear = schedulYear;
    }

    public Integer getScheduleMonth() {
        return scheduleMonth;
    }

    public void setScheduleMonth(Integer scheduleMonth) {
        this.scheduleMonth = scheduleMonth;
    }

    public Integer getDaysFromUpload() {
        return daysFromUpload;
    }

    public void setDaysFromUpload(Integer daysFromUpload) {
        this.daysFromUpload = daysFromUpload;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    public String toString() {
        return "PerfRsErrShowSchedule{" +
                "id=" + id +
                ", errRsShowMusicList=" + errRsShowMusicList +
                ", idNormalizedFile=" + idNormalizedFile +
                ", idPalinsesto=" + idPalinsesto +
                ", title='" + title + '\'' +
                ", beginTime=" + beginTime +
                ", endTime=" + endTime +
                ", duration=" + duration +
                ", regionalOffice='" + regionalOffice + '\'' +
                ", error='" + error + '\'' +
                ", globalError='" + globalError + '\'' +
                ", reportPalinsesto='" + reportPalinsesto + '\'' +
                ", reportBeginDate='" + reportBeginDate + '\'' +
                ", reportBeginTime='" + reportBeginTime + '\'' +
                ", reportDuration='" + reportDuration + '\'' +
                ", schedulYear=" + schedulYear +
                ", scheduleMonth=" + scheduleMonth +
                ", daysFromUpload=" + daysFromUpload +
                ", creationDate=" + creationDate +
                ", modifyDate=" + modifyDate +
                '}';
    }
}