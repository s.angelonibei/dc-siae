package com.alkemytech.sophia.utilizer.service.radioinstore;

import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.tools.DateTools;
import com.alkemytech.sophia.common.tools.StringTools;
import com.alkemytech.sophia.utilizer.Utilizer;
import com.alkemytech.sophia.utilizer.model.generic.HarmonizationConfig;
import com.alkemytech.sophia.utilizer.model.radioinstore.*;
import com.alkemytech.sophia.utilizer.service.NormalizedFileService;
import com.alkemytech.sophia.utilizer.service.interfaces.RecordService;
import com.alkemytech.sophia.utilizer.utily.Utilities;
import com.amazonaws.services.s3.AmazonS3URI;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.math.BigDecimal;
import java.net.ServerSocket;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static com.alkemytech.sophia.utilizer.utily.Utilities.*;

/**
 * Created by Alessandro Russo on 10/12/2018.
 */
@Singleton
public class RadioInStoreRecordService implements RecordService {

    private static final Logger logger = LoggerFactory.getLogger(Utilizer.class);

    private final static String MSG_RADIO_IN_STORE_WITHOUT_MUSIC = "Utilizzazione Radio in Store senza opere musicali";
    private final static String MSG_OVERLAPPING = "Utilizzazione Radio in Store in sovrapposizione";
    private final static String MSG_PALINSESTO_NON_ATTIVO = "Palinsesto non attivo alla data utilizzazione";

    Provider<EntityManager> provider;
    private Map<String, String> queriesMap;
    private Map<String, String> fileStatusMap;
    private NormalizedFileService normalizedFileService;
    private RadioInStoreKpiService radioInStoreKpiService;

    @Inject
    public RadioInStoreRecordService(Provider<EntityManager> provider,
                                     @Named("utilization.query.json") String harmonizationQueryJson,
                                     @Named("file.status.json") String fileStatusJson,
                                     NormalizedFileService normalizedFileService,
                                     RadioInStoreKpiService radioInStoreKpiService) {
        this.provider = provider;
        Gson gson = new Gson();
        this.queriesMap = StringUtils.isNotEmpty(harmonizationQueryJson) ? gson.fromJson(harmonizationQueryJson, Map.class) : new HashMap<>();
        this.fileStatusMap = StringUtils.isNotEmpty(fileStatusJson) ? gson.fromJson(fileStatusJson, Map.class) : new HashMap<>();
        this.normalizedFileService = normalizedFileService;
        this.radioInStoreKpiService = radioInStoreKpiService;
    }

    /**
     * Process normalized File and save utilizations
     * @throws Exception
     */
    @Override
    public void process(Integer utilizationListenPort) throws Exception {

        try(final ServerSocket socket = new ServerSocket( utilizationListenPort );){
            logger.info("PERSIST REPORT UTILIZATION TASK START: listening on {}", socket.getLocalSocketAddress());
            //Retrieve files to process
            List<PerfRSNormalizedFile> validatedFileList = retrieveValidatedFile();
            if( CollectionUtils.isNotEmpty( validatedFileList ) ){
                logger.debug("Retrieved {} validated files", validatedFileList.size());
                for( PerfRSNormalizedFile currentFile : validatedFileList ){
                    try{
                        logger.info("----------------------------------------");
                        logger.info("START Working on {}", currentFile.getFileBucket());
                        logger.info("----------------------------------------");
                        AmazonS3URI currentFileUri  = normalizedFileService.reserveSingleFile(currentFile.getFileBucket(), Constants.LOCK_SUFFIX);
                        if( currentFileUri != null ){
                            try{
                                HarmonizationConfig harmonizationConfig = retrieveConfiguration(currentFile);
                                if(harmonizationConfig == null){
                                    logger.error("ERROR - No valid configuration for file with id {} and name {}", currentFile.getIdNormalizedFile(), currentFile.getFileName() );
                                    normalizedFileService.releaseSingleFile(currentFileUri.getBucket(), currentFileUri.getKey(), Constants.LOCK_SUFFIX);
                                    continue;
                                }
                                //Parsing normalized Report via BEANIO
                                List<Object> parsedRows = normalizedFileService.parseFile(currentFile.getFileBucket(), harmonizationConfig.getWriteConfigPath(), harmonizationConfig.getWriteConfigStream());
                                if( CollectionUtils.isNotEmpty(parsedRows) ){
                                    logger.info("File parsed successfully!");
                                    //Create and persist DB Entities
                                    Map<Long, Set<String>> kpiToEvaluate = saveUtilizations(parsedRows, currentFile);
                                    if(kpiToEvaluate != null){
                                        logger.info("Records saved successfully!");
                                        normalizedFileService.changeUtilizationFileStatus(currentFile.getIdNormalizedFile(), fileStatusMap.get("processed"));
                                        //calculate Kpi
                                        if(evaluateKpi(kpiToEvaluate)){
                                            logger.info("----------------------------------------");
                                            logger.info("SUCCESS - {}", currentFile.getFileBucket());
                                            logger.info("----------------------------------------");
                                        }
                                        else{
                                            logger.error("ERROR - Evaluating Kpi error" );
                                            //Keep file locking
                                        }
                                    }
                                    else{
                                        normalizedFileService.changeUtilizationFileStatus(currentFile.getIdNormalizedFile(), fileStatusMap.get("error"));
                                        normalizedFileService.releaseSingleFile(currentFileUri.getBucket(), currentFileUri.getKey(), Constants.LOCK_SUFFIX);
                                        logger.error("ERROR - Peristing utilization error" );
                                    }
                                }
                                else{
                                    normalizedFileService.changeUtilizationFileStatus(currentFile.getIdNormalizedFile(), fileStatusMap.get("error"));
                                    normalizedFileService.releaseSingleFile(currentFileUri.getBucket(), currentFileUri.getKey(), Constants.LOCK_SUFFIX);
                                    logger.error("ERROR - No Persist Data from report {}", currentFile.getFileBucket());
                                }
                            }
                            finally {
                                normalizedFileService.releaseSingleFile(currentFileUri.getBucket(), currentFileUri.getKey(), Constants.LOCK_SUFFIX);
                            }
                        }
                        else{
                            logger.info("File {} already locked by other process!");
                        }
                    }
                    catch (Exception e){
                        normalizedFileService.changeUtilizationFileStatus(currentFile.getIdNormalizedFile(), fileStatusMap.get("error"));
                        logger.error("-- HARMONIZATION TASK ERROR --", e);
                        continue;
                    }
                }
            }
            else {
                logger.info("No Report with status {} found!", fileStatusMap.get("validated"));
            }
            logger.debug("-- PERSIST REPORT UTILIZATIONS TASK END --");
            return;
        }
    }

    public HarmonizationConfig retrieveConfiguration(PerfRSNormalizedFile normalizedReport) throws Exception {
        if(CollectionUtils.isNotEmpty(normalizedReport.getUtilizationFiles())){
            String jsonConfig = retrieveHarmonizationConfiguration(normalizedReport.getUtilizationFiles().get(0).getIdMusicProvider());
            if( StringUtils.isNotEmpty(jsonConfig) ){
                return new Gson().fromJson(jsonConfig, HarmonizationConfig.class);
            }
        }
        return null;
    }

    public Map<Long, Set<String>> saveUtilizations(List<Object> rows, PerfRSNormalizedFile normalizedFile) throws Exception {

        PerfRSUtilizationFile utilizationFile = normalizedFile.getUtilizationFiles().size() > 0 ? normalizedFile.getUtilizationFiles().get(0) : null;
        if (utilizationFile != null) {
            EntityManager em = provider.get();
            em.getTransaction().begin();
            List<Object> discardedRows = new ArrayList<>(); //lista delle utilizzazioni da scartare
            Map<Long, Set<String>> ret = new HashMap<>(); //mappa <idCanale, List<Anno-Mese>>
            Map<Long, Map<String, Map<String, List<PerfRsShowSchedule>>>> existingMap = new HashMap<>(); //utilizzazioni già presenti
            Map<Long, PerfPalinsesto> palinsesti = new HashMap<>();
            if ("SINGOLO".equalsIgnoreCase(utilizationFile.getTipoUpload())) {
                ret.put(utilizationFile.getIdPalinsesto(), new TreeSet<String>());
                String yearMonth = String.format("%d-%d", utilizationFile.getAnno(), utilizationFile.getMese());
                ret.get(utilizationFile.getIdPalinsesto()).add(yearMonth);
            }
            try {
                for (Object currentRow : rows) {
                    try{
                        checkPalinsestoAttivo( (Map<String, Object>)currentRow, palinsesti);
                        if(normalizedFileService.containsErrors((Map<String, Object>) currentRow)){
                            discardedRows.add( fromMapToErrShow((Map<String, Object>) currentRow, normalizedFile));
                        }
                        else{
                            PerfRsShowSchedule correctShow = fromMapToShow((Map<String, Object>) currentRow, normalizedFile);
                            correctShow.setModifyDate(correctShow.getId() != null ? new Date() : null);
                            em.persist(correctShow);
                            em.flush();
                            String yearMonth = String.format("%d-%d", correctShow.getScheduleYear(), correctShow.getScheduleMonth());
                            String day = new Integer(Utilities.getDateField(correctShow.getDate(), Calendar.DAY_OF_MONTH)).toString();
                            updateKpiToEvaluate(ret, correctShow.getIdPalinsesto(), yearMonth);
                            if (correctShow.getModifyDate() == null) {
                                updateRadioInStoreExistingMap(yearMonth, day, correctShow, existingMap);
                            }
                        }
                    }
                    catch (Exception e) {
                        logger.error("Error Managing RADIO IN STORE record", e);
                        PerfRsErrShowSchedule errShow = fromMapToErrShow((Map<String, Object>) currentRow, normalizedFile);
                        errShow.setGlobalError(e.getMessage());
                        discardedRows.add(errShow);
                    }
                }
                //PERSIST SHOW WITH ERROR
                for (Object obj : discardedRows) {
                    PerfRsErrShowSchedule errShow = (PerfRsErrShowSchedule) obj;
                    if (errShow.getIdPalinsesto() != null) {
                        if (errShow.getSchedulYear() != null && errShow.getScheduleMonth() != null) {
                            String yearMonth = String.format("%d-%d", errShow.getSchedulYear(), errShow.getScheduleMonth());
                            updateKpiToEvaluate(ret, errShow.getIdPalinsesto(), yearMonth);
                        }
                    }
                    em.merge(errShow);
                    em.flush();
                }
                em.getTransaction().commit();
                return ret;
            } catch (Exception e) {
                logger.error(e.getMessage());
                if (em.getTransaction().isActive()) {
                    em.getTransaction().rollback();
                }
                return null;
            }
        }
        return null;
    }

    private void checkPalinsestoAttivo( Map<String, Object> record, Map<Long, PerfPalinsesto> palinsesti) throws Exception {
        Long idPalinsesto = getMapValue(record, Constants.DECODE_ID_CANALE) != null ? Long.parseLong( getMapValue(record, Constants.DECODE_ID_CANALE) ) : null;
        Date recordDate = getMapValue(record,Constants.DATA_INIZIO_TX) != null ?  DateTools.stringToDate(getMapValue(record,Constants.DATA_INIZIO_TX), DateTools.DATE_ITA_FORMAT_SLASH) : null;
        if( idPalinsesto != null && recordDate != null ){
            if( !palinsesti.containsKey(idPalinsesto) ){
                //Verifico che il palinsesto sia attivo alla data dell'utilizzazione
                PerfPalinsesto palinsesto = findPalinsestoById(idPalinsesto);
                if( palinsesto != null ){
                   if( ( palinsesto.getDataFineValid() != null && palinsesto.getDataFineValid().before(recordDate) ) ||
                       ( palinsesto.getDataInizioValid() != null && palinsesto.getDataInizioValid().after(recordDate) ) ){
                       throw new Exception(MSG_PALINSESTO_NON_ATTIVO);
                   }
                   palinsesti.put(idPalinsesto, palinsesto);
                }
            }
        }
    }

    private PerfPalinsesto findPalinsestoById( Long idPalinsesto){
        EntityManager em = provider.get();
        return  em.find(PerfPalinsesto.class, idPalinsesto);
    }

    private void updateRadioInStoreExistingMap(String yearMonth, String day, PerfRsShowSchedule showToPersist, Map<Long, Map<String, Map<String, List<PerfRsShowSchedule>>>> existingMap) {
        if( !existingMap.containsKey( showToPersist.getIdPalinsesto() ) ){
            existingMap.put(showToPersist.getIdPalinsesto(), new HashMap<String, Map<String, List<PerfRsShowSchedule>>>());
        }
        if( !existingMap.get(showToPersist.getIdPalinsesto()).containsKey(yearMonth) ) {
            existingMap.get(showToPersist.getIdPalinsesto()).put(yearMonth, new HashMap<String, List<PerfRsShowSchedule>>());
        }
        if(!existingMap.get(showToPersist.getIdPalinsesto()).get(yearMonth).containsKey(day)){
            existingMap.get(showToPersist.getIdPalinsesto()).get(yearMonth).put(day,new ArrayList<PerfRsShowSchedule>());
        }
        List<PerfRsShowSchedule> updatedExistingShows = existingMap.get(showToPersist.getIdPalinsesto()).get(yearMonth).get(day);
        int index = 0;
        for(int i =0; i < updatedExistingShows.size() ; i++){
            PerfRsShowSchedule existingShow = updatedExistingShows.get(i);
            if(showToPersist.getBeginTime() != null && existingShow.getBeginTime() != null ){
                if(showToPersist.getBeginTime().before(existingShow.getBeginTime())){
                    break;
                }
            }
            index ++;
        }
        updatedExistingShows.add(index, showToPersist);
    }


    private void updateKpiToEvaluate ( Map<Long, Set<String>> kpiToEvaluate, Long idPalinsesto, String yearMonth ){
        if( idPalinsesto != null ){
            if(!kpiToEvaluate.containsKey(idPalinsesto)){
                kpiToEvaluate.put(idPalinsesto, new TreeSet<String>());
            }
            kpiToEvaluate.get(idPalinsesto).add(yearMonth);
        }
    }

    private Set<PerfRsShowSchedule> verifyNormalizedRdEntity(PerfRsShowSchedule show, Map<Long, Map<String, Map<String, List<PerfRsShowSchedule>>>> existingMap ) throws Exception {

        if( CollectionUtils.isEmpty(show.getRsShowMusicList()) ){
            throw new Exception(MSG_RADIO_IN_STORE_WITHOUT_MUSIC);
        }

        Set<PerfRsShowSchedule> showsToPersist = new HashSet<>();
        //calculate map keys
        Long idPalinsesto = show.getIdPalinsesto();
        String yearMonth = String.format("%d-%d", show.getScheduleYear(), show.getScheduleMonth());
        String day = show.getDate() != null ? new Integer (Utilities.getDateField(show.getDate(), Calendar.DAY_OF_MONTH)).toString() : "0";
        //load shows from DB
        loadRadioInStoreShows(idPalinsesto, show.getScheduleYear(), show.getScheduleMonth(), existingMap);
        loadRadioInStoreNearMonth(show, existingMap, true);
        loadRadioInStoreNearMonth(show, existingMap, false);

        //Verify overlapping record
        List<PerfRsShowSchedule> overlappingShowList = evaluateOverlapping(idPalinsesto, yearMonth, day, show, existingMap);
        showsToPersist.add(show);
        if( CollectionUtils.isNotEmpty(overlappingShowList ) ){
            showsToPersist.addAll(overlappingShowList);
        }
        return showsToPersist;
    }

    private List<PerfRsShowSchedule> evaluateOverlapping(Long idPalinsesto, String yearMonth, String day, PerfRsShowSchedule show, Map<Long, Map<String, Map<String, List<PerfRsShowSchedule>>>> existingMap) throws Exception {
        List<PerfRsShowSchedule> overlappingShowList = new ArrayList<>();
        boolean existShowList = existingMap.containsKey(idPalinsesto) && existingMap.get(idPalinsesto).containsKey(yearMonth) && existingMap.get(idPalinsesto).get(yearMonth).containsKey(day);
        PerfRsShowSchedule lastShowPreviousDay = getNearShow(show, existingMap, true);
        PerfRsShowSchedule firstShowNextDay= getNearShow(show, existingMap, false);
        List<PerfRsShowSchedule> dayShowList = existShowList ? new ArrayList<>(existingMap.get(idPalinsesto).get(yearMonth).get(day)) : new ArrayList<PerfRsShowSchedule>();
        if(lastShowPreviousDay != null){
            dayShowList.add(0, lastShowPreviousDay);
        }
        if(firstShowNextDay != null){
            dayShowList.add(dayShowList.size(), firstShowNextDay);
        }
        for (PerfRsShowSchedule existingShow : dayShowList) {
            boolean isBothNational = StringUtils.isEmpty(show.getRegionalOffice()) && StringUtils.isEmpty(existingShow.getRegionalOffice()) ? true : false;
            boolean isSameRegionalOffice = StringUtils.isNotEmpty(show.getRegionalOffice()) &&  StringUtils.isNotEmpty(existingShow.getRegionalOffice()) && show.getRegionalOffice().equalsIgnoreCase(existingShow.getRegionalOffice());
            if( isBothNational || isSameRegionalOffice ){
                Boolean startInside = null;
                Boolean endInside = null;
                Boolean completeOverlap = null;
                for( PerfRsShowMusic existingMusic : existingShow.getRsShowMusicList() ){
                    Date beginCurrent = existingMusic.getBeginTime();
                    if( beginCurrent != null ){
                        Date endCurrent = DateUtils.addSeconds(beginCurrent, existingMusic.getDuration());
                        for( PerfRsShowMusic newMusic : show.getRsShowMusicList()){
                            Date beginNew = newMusic.getBeginTime();
                            Date endNew = DateUtils.addSeconds(beginNew, newMusic.getDuration());
                            startInside = (beginNew.after(beginCurrent) && beginNew.before(endCurrent)) || beginNew.equals(beginCurrent);
                            endInside = (endNew.after(beginCurrent) && endNew.before(endCurrent)) || endNew.equals(endCurrent);
                            completeOverlap = beginNew.before(beginCurrent) && endNew.after(endCurrent);
                            if( startInside || endInside || completeOverlap ){
                                throw new Exception(MSG_OVERLAPPING);
                            }
                        }
                    }
                }
            }
        }
        return overlappingShowList;
    }

    private PerfRsShowSchedule getNearShow(PerfRsShowSchedule show, Map<Long, Map<String, Map<String, List<PerfRsShowSchedule>>>> existingMap, boolean isLast) {
        Date updatedDate = isLast ? Utilities.manageDate(show.getDate(), -1, Calendar.DAY_OF_MONTH) : Utilities.manageDate(show.getDate(), 1, Calendar.DAY_OF_MONTH);
        String yearMonth = String.format("%d-%d", Utilities.getDateField(updatedDate,Calendar.YEAR), Utilities.getDateField(updatedDate,Calendar.MONTH)+1);
        Integer day = Utilities.getDateField(updatedDate, Calendar.DAY_OF_MONTH);

        if( existingMap.containsKey(show.getIdPalinsesto()) && existingMap.get(show.getIdPalinsesto()).containsKey(yearMonth) && existingMap.get(show.getIdPalinsesto()).get(yearMonth).containsKey(day.toString())  ){
            List<PerfRsShowSchedule> dailyShows = existingMap.get(show.getIdPalinsesto()).get(yearMonth).get(day.toString());
            if( CollectionUtils.isNotEmpty(dailyShows) ){
                List<PerfRsShowSchedule> listDailyShows = new ArrayList<>(dailyShows);
                return isLast ? listDailyShows.get(dailyShows.size()-1): listDailyShows.get(0);
            }
        }
        return null;
    }

    private void loadRadioInStoreNearMonth(PerfRsShowSchedule show, Map<Long, Map<String, Map<String, List<PerfRsShowSchedule>>>> existingMap, boolean isPrevious) {
        int quantity = isPrevious ? -1 : 1;
        Date updatedDate = show.getDate();
        Utilities.manageDate(updatedDate, quantity, Calendar.MONTH);
        loadRadioInStoreShows(show.getIdPalinsesto(), Utilities.getDateField(updatedDate, Calendar.YEAR), Utilities.getDateField(updatedDate, Calendar.MONTH)+1, existingMap);
    }

    private void loadRadioInStoreShows(Long idPalinsesto, Integer scheduleYear, Integer scheduleMonth, Map<Long, Map<String, Map<String, List<PerfRsShowSchedule>>>> existingMap) {
        String yearMonth = String.format("%d-%d", scheduleYear, scheduleMonth);
        if(existingMap.containsKey(idPalinsesto) && existingMap.get(idPalinsesto).containsKey(yearMonth)){
            return;
        }
        else{
            Map<String, List<PerfRsShowSchedule>> dailyShowSchedule = loadRadioInStoreDailySchedule(idPalinsesto, scheduleYear, scheduleMonth);
            if( !dailyShowSchedule.isEmpty() ){
                if( !existingMap.containsKey( idPalinsesto ) ){
                    existingMap.put(idPalinsesto, new HashMap<String, Map<String, List<PerfRsShowSchedule>>>());
                }
                if( !existingMap.get(idPalinsesto).containsKey(yearMonth) ) {
                    existingMap.get(idPalinsesto).put(yearMonth, new HashMap<String, List<PerfRsShowSchedule>>());
                }
                existingMap.get(idPalinsesto).get(yearMonth).putAll(dailyShowSchedule);
            }
        }
    }

    private Map<String, List<PerfRsShowSchedule>> loadRadioInStoreDailySchedule(Long idPalinsesto, Integer scheduleYear, Integer scheduleMonth) {
        Map<String, List<PerfRsShowSchedule>> ret = new HashMap<>();
        try{
            EntityManager em = provider.get();
            TypedQuery<PerfRsShowSchedule> q = em.createQuery("Select show FROM PerfRsShowSchedule show " +
                    "WHERE show.idPalinsesto = :idPalinsesto and show.scheduleYear = :scheduleYear and show.scheduleMonth = :scheduleMonth " +
                    "ORDER BY show.beginTime ASC ", PerfRsShowSchedule.class);
            q.setParameter("idPalinsesto", idPalinsesto);
            q.setParameter("scheduleYear", scheduleYear);
            q.setParameter("scheduleMonth", scheduleMonth);
            List<PerfRsShowSchedule> resulSet = q.getResultList();

            if( CollectionUtils.isNotEmpty(resulSet) ){
                for(PerfRsShowSchedule current : resulSet){
                    Integer day = Utilities.getDateField(current.getDate(), Calendar.DAY_OF_MONTH);
                    if(!ret.containsKey(day.toString())){
                        ret.put(day.toString(), new ArrayList<PerfRsShowSchedule>());
                    }
                    ret.get(day.toString()).add(current);
                }
            }
        }
        catch (NoResultException e){
            logger.error("No result found for palinsesto {} year {} month {} ", idPalinsesto, scheduleYear, scheduleMonth);
        }
        return ret;
    }

    private PerfRsShowSchedule fromMapToShow(Map<String, Object> record, PerfRSNormalizedFile normalizedFile) {
        Long idMusicType = getMapValue(record, Constants.DECODE_ID_CATEGORIA_USO_OP) != null ? Long.parseLong( getMapValue(record, Constants.DECODE_ID_CATEGORIA_USO_OP) ) : null;

        String begin = getMapValue(record,Constants.DATA_INIZIO_TX);
        String timeTx = getMapValue(record,Constants.ORARIO_INIZIO_TX) != null ? getMapValue(record,Constants.ORARIO_INIZIO_TX) : "00:00:00";
        String timeOp = getMapValue(record,Constants.ORARIO_INIZIO_OP) != null ? getMapValue(record,Constants.ORARIO_INIZIO_OP) : null;
        String beginTx = begin + " " + timeTx;
        String beginOp = timeOp != null ?begin + " " + timeOp : begin;
        Date beginTimeTx = StringUtils.isNotEmpty(beginTx.trim()) ? DateTools.stringToDate(beginTx, DateTools.DATETIME_ITA_FORMAT_SLASH) : null;
        Date beginTimeOp = StringUtils.isNotEmpty(beginOp.trim()) ? DateTools.stringToDate(beginOp, DateTools.DATETIME_ITA_FORMAT_SLASH) : null;
        Date endTimeTx = null;
        Integer duration = null;
        if(getMapValue(record, Constants.DURATA_TX) != null && getMapValue(record, Constants.DURATA_TX).matches(StringTools.DURATION_FORMAT)){
            duration = Integer.parseInt( StringTools.parseTimeToSeconds( getMapValue(record, Constants.DURATA_TX) ) ) ;
        }
        endTimeTx = (beginTimeTx != null && duration != null) ? DateTools.manageDate(beginTimeTx, duration, Calendar.SECOND): null;

        PerfRsShowMusic rsShowMusic = new PerfRsShowMusic(normalizedFile, beginTimeOp, record);

        PerfRsShowSchedule rsShowSchedule = new PerfRsShowSchedule(rsShowMusic, normalizedFile, beginTimeTx, endTimeTx, record);
        //SET Kpi PROPERTIES
        Date recordDate = beginTimeTx != null ? beginTimeTx : beginTimeOp;
        Calendar cal = Calendar.getInstance();
        cal.setTime(recordDate);
        rsShowSchedule.setScheduleYear(cal.get(Calendar.YEAR));
        rsShowSchedule.setScheduleMonth(cal.get(Calendar.MONTH)+1);
        long diff = normalizedFile.getUtilizationFiles().get(0).getDataUpload().getTime() - recordDate.getTime();
        rsShowSchedule.setDaysFromUpload((int) (long) TimeUnit.MILLISECONDS.toDays(diff));
        rsShowMusic.setDaysFromUpload((int) (long) TimeUnit.MILLISECONDS.toDays(diff));
        logger.debug( "RADIO IN STORE RECORD: {}", rsShowSchedule.toString() );
        return rsShowSchedule;
    }


    public PerfRsErrShowSchedule fromMapToErrShow(Map<String, Object> record, PerfRSNormalizedFile normalizedFile) {
        String begin = getMapValue(record,Constants.DATA_INIZIO_TX);
        String timeTx = getMapValue(record,Constants.ORARIO_INIZIO_TX) != null ? getMapValue(record,Constants.ORARIO_INIZIO_TX) : "00:00:00";
        String timeOp = getMapValue(record,Constants.ORARIO_INIZIO_OP) != null ? getMapValue(record,Constants.ORARIO_INIZIO_OP) : null;
        String beginTx = begin + " " + timeTx;
        String beginOp = begin + " " + timeOp;
        Date beginTimeTx = org.apache.commons.lang.StringUtils.isNotEmpty(beginTx.trim()) ? DateTools.stringToDate(beginTx, DateTools.DATETIME_ITA_FORMAT_SLASH) : null;
        Date beginTimeOp = org.apache.commons.lang.StringUtils.isNotEmpty(beginOp.trim()) ? DateTools.stringToDate(beginOp, DateTools.DATETIME_ITA_FORMAT_SLASH) : null;
        Date endTimeTx = null;
        Integer duration = null;
        if(getMapValue(record, Constants.DURATA_TX) != null && getMapValue(record, Constants.DURATA_TX).matches(StringTools.DURATION_FORMAT)){
            duration = Integer.parseInt( StringTools.parseTimeToSeconds( getMapValue(record, Constants.DURATA_TX) ) ) ;
        }
        endTimeTx = (beginTimeTx != null && duration != null) ? DateTools.manageDate(beginTimeTx, duration, Calendar.SECOND): null;

        PerfRsErrShowMusic rsErrShowMusic = new PerfRsErrShowMusic(normalizedFile, beginTimeOp, record);

        PerfRsErrShowSchedule rsErrShowSchedule = new PerfRsErrShowSchedule(rsErrShowMusic, normalizedFile, beginTimeTx, endTimeTx, record);

        //SET Kpi PROPERTIES
        Date recordDate = beginTimeTx != null ? beginTimeTx : beginTimeOp;
        if(recordDate != null){
            Calendar cal = Calendar.getInstance();
            cal.setTime(recordDate);
            rsErrShowSchedule.setSchedulYear(cal.get(Calendar.YEAR));
            rsErrShowSchedule.setScheduleMonth(cal.get(Calendar.MONTH)+1);
            long diff = normalizedFile.getUtilizationFiles().get(0).getDataUpload().getTime() - recordDate.getTime();
            rsErrShowSchedule.setDaysFromUpload((int) (long) TimeUnit.MILLISECONDS.toDays(diff));
            rsErrShowMusic.setDaysFromUpload((int) (long) TimeUnit.MILLISECONDS.toDays(diff));
        }
        logger.debug( "RADIO ERROR: {}", rsErrShowSchedule.toString() );
        return rsErrShowSchedule;
    }

    /**
     *
     * @param kpiToEvaluate Map<id_canale, Set<anno-mese>> es: <58, (1, 12, 3)>
     * @return true se sono stati calcolati tutti gli indicatori, false se c'è stato qualche problema
     */
    private boolean evaluateKpi(Map<Long, Set<String>> kpiToEvaluate) {
        boolean allKpiEvalueted = true;
        if(kpiToEvaluate != null){
            Set<Long> setPalinsesti = kpiToEvaluate.keySet();
            for(Long idPalinsesto : setPalinsesti){
                PerfPalinsesto palinsesto = findPalinsestoById(idPalinsesto);
                Set<String> setMeseAnno = kpiToEvaluate.get(idPalinsesto);
                for(String meseAnno : setMeseAnno){
                    Integer anno = extractAnnoFromMeseAnno(meseAnno);
                    Integer mese = extractMeseFromMeseAnno(meseAnno);
                    if(anno != null && mese != null){
                        logger.info("@@@@ INFO evaluateKpi @@@@ -> Inizio il calcolo Kpi per la coppia [palinsesto, mese-anno]: [" + idPalinsesto + ", " + meseAnno + "]");
                        if(!radioInStoreKpiService.evaluateIndicatori(palinsesto, anno, mese)){
                            allKpiEvalueted = false;
                            logger.error("#### ERRORE evaluateKpi #### -> Si è verificato un errore durante il calcolo dei Kpi per la coppia [palinsesto, mese-anno]: [" + idPalinsesto + ", " + meseAnno + "]");
                        }
                        logger.info("@@@@ INFO evaluateKpi @@@@ -> Terminato calcolo Kpi per la coppia [palinsesto, mese-anno]: [" + idPalinsesto + ", " + meseAnno + "]");
                    }else{
                        logger.error("#### ERRORE evaluateKpi #### -> Richiesto il calcolo Kpi per una coppia [palinsesto, mese-anno] non valido: [" + idPalinsesto + ", " + meseAnno + "]");
                        allKpiEvalueted = false;
                    }
                }
            }
        }
        return allKpiEvalueted;
    }



    private String retrieveHarmonizationConfiguration(Long idCustomer) throws Exception {
        if( queriesMap.containsKey("retrieve.harmonization.config.query")){
            EntityManager entityManager = provider.get();
            final Query q = entityManager.createNativeQuery(queriesMap.get("retrieve.harmonization.config.query"));
            String queryResult = (String) q.setParameter(1, idCustomer).getSingleResult();
            return queryResult;
        }
        throw new Exception("PROPERTY -> retrieve.harmonization.config.query NOT FOUND in harmonizationQueryJson");
    }

    private List<PerfRSNormalizedFile> retrieveValidatedFile() throws Exception {
        return retrieveNormalizedFile( fileStatusMap.get("validated"));
    }

    private List<PerfRSNormalizedFile> retrieveNormalizedFile(String fileStatus) throws Exception {
        if( queriesMap.containsKey("retrieve.normalized.file.query")){
            EntityManager entityManager = provider.get();
            final Query q = entityManager.createNativeQuery(queriesMap.get("retrieve.normalized.file.query"), PerfRSNormalizedFile.class);
            List<PerfRSNormalizedFile> queryResult = q.setParameter(1, fileStatus).getResultList();
            return queryResult;
        }
        throw new Exception("PROPERTY -> retrieve.utilization.file.query NOT FOUND in harmonizationQueryJson");
    }
}