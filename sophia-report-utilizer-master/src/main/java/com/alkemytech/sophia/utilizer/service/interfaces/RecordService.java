package com.alkemytech.sophia.utilizer.service.interfaces;

import com.alkemytech.sophia.utilizer.model.generic.HarmonizationConfig;
import com.alkemytech.sophia.utilizer.model.generic.NormalizedReport;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Alessandro Russo on 10/12/2018.
 */
public interface RecordService {

    public void process(Integer utilizationListenPort) throws Exception;

}
