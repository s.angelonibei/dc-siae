package com.alkemytech.sophia.utilizer.model.radioinstore;

import com.alkemytech.sophia.utilizer.model.generic.Kpi;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.*;

@XmlRootElement
@Entity
@Table(name = "PERF_RS_KPI")
@SqlResultSetMapping(name="PerfIndicatoreMapping",
        classes = {
                @ConstructorResult(targetClass = PerfRsKpiEntity.class,
                        columns = {
                                @ColumnResult(name="indicatore", type = String.class),
                                @ColumnResult(name="valore", type = Integer.class),
                                @ColumnResult(name="lastUpdate", type = Date.class)

                        }
                )
        }
)
public class PerfRsKpiEntity {
    public static final String DURATA_DISPONIBILE = "Durata disponibile";
    public static final String DURATA_FILM = "Durata film";
    public static final String DURATA_GENERI_VUOTI = "Durata generi vuoti";
    public static final String DURATA_PUBBLICITA = "Durata pubblicita";
    public static final String DURATA_TRASMISSIONE = "Durata trasmissione";
    public static final String RECORD_IN_RITARDO = "Record in ritardo";
    public static final String RECORD_SCARTATI = "Record scartati";
    public static final String RECORD_TOTALI = "Record totali";
    public static final String DURATA_BRANI = "Durata brani";
    public static final String DURATA_BRANI_DICHIARATI = "Durata totale brani dichiarati";
    public static final String DURATA_BRANI_TITOLI_ERRONEI = "Durata brani titoli erronei";
    public static final String DURATA_BRANI_AUTORI_ERRONEI = "Durata brani autori erronei";
    public static final String DURATA_FILM_TITOLI_ERRONEI = "Durata film titoli erronei";
    public static final String DURATA_PROGRAMMI_TITOLI_ERRONEI = "Durata programmi titoli erronei";
    public static final String TRASMISSIONI_MUSICA_ECCEDENTE = "Trasmissioni musica eccedente";
    public static final String TRASMISSIONI_TOTALI = "Trasmissioni totali";

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="ID_PERF_RS_KPI", nullable=false)
    private int id;

    @Column(name="ID_MUSIC_PROVIDER")
    private Long idMusicProvider;

    @Column(name="ID_PALINSESTO")
    private Long idPalinsesto;

    @Column(name="ANNO")
    private int anno;

    @Column(name="MESE")
    private int mese;

    @Column(name="INDICATORE")
    private String indicatore;

    @Column(name="VALORE")
    private Long valore;

    @Column(name = "LAST_UPDATE")
    private Date lastUpdate;

    @Transient
    private List<PerfPalinsesto> palinsesti;
    @Transient
    private List<Short> mesi;
    @Transient
    private List<Kpi> listaKPI;

    public PerfRsKpiEntity() {
    }

    public PerfRsKpiEntity(Long idMusicProvider, Long idPalinsesto, int anno, int mese, String indicatore, Long valore, Date lastUpdate, List<Short> mesi, List<Kpi> listaKPI, List<PerfPalinsesto> palinsesti) {
        this.idMusicProvider = idMusicProvider;
        this.idPalinsesto = idPalinsesto;
        this.anno = anno;
        this.mese = mese;
        this.indicatore = indicatore;
        this.valore = valore;
        this.lastUpdate = lastUpdate;
        this.mesi = mesi;
        this.listaKPI = listaKPI;
        this.palinsesti = palinsesti;
    }

    @Transient
    public List<Short> getMesi() {
        return mesi;
    }

    public String getMesiToString() {
        String sMesi = "";
        for (Short m: mesi
                ) {
            if(!sMesi.equals("")) {
                sMesi += "," + m;
            }else{
                sMesi += m;
            }
        }
        return sMesi;
    }

    public void setMesi(List<Short> mesi) {
        this.mesi = mesi;
    }

    @Transient
    public List<PerfPalinsesto> getPalinsesti() {
        return palinsesti;
    }

    public String getCanaliToString() {
        Set<String> setPalinsesti = new HashSet<>();
        for ( PerfPalinsesto n: palinsesti ) {
            setPalinsesti.add(n.getNome());
        }
        return StringUtils.join(setPalinsesti, ",");
    }

    public void setCanali(List<PerfPalinsesto> palinsesti) {
        this.palinsesti = palinsesti;
    }

    @Transient
    public List<Kpi> getListaKPI() {
        return listaKPI;
    }

    public void setListaKPI(List<Kpi> listaKPI) {
        this.listaKPI = listaKPI;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Long getIdMusicProvider() {
        return idMusicProvider;
    }

    public void setIdMusicProvider(Long idMusicProvider) {
        this.idMusicProvider = idMusicProvider;
    }

    public Long getIdPalinsesto() {
        return idPalinsesto;
    }

    public void setIdPalinsesto(Long idPalinsesto) {
        this.idPalinsesto = idPalinsesto;
    }

    public int getAnno() {
        return anno;
    }

    public void setAnno(int anno) {
        this.anno = anno;
    }

    public int getMese() {
        return mese;
    }

    public void setMese(int mese) {
        this.mese = mese;
    }

    public String getIndicatore() {
        return indicatore;
    }

    public void setIndicatore(String indicatore) {
        this.indicatore = indicatore;
    }

    public Long getValore() {
        return valore;
    }

    public void setValore(Long valore) {
        this.valore = valore;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PerfRsKpiEntity)) return false;
        PerfRsKpiEntity that = (PerfRsKpiEntity) o;
        return id == that.id &&
                anno == that.anno &&
                mese == that.mese &&
                Objects.equals(idMusicProvider, that.idMusicProvider) &&
                Objects.equals(idPalinsesto, that.idPalinsesto) &&
                Objects.equals(indicatore, that.indicatore) &&
                Objects.equals(valore, that.valore) &&
                Objects.equals(lastUpdate, that.lastUpdate) &&
                Objects.equals(palinsesti, that.palinsesti) &&
                Objects.equals(mesi, that.mesi) &&
                Objects.equals(listaKPI, that.listaKPI);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, idMusicProvider, idPalinsesto, anno, mese, indicatore, valore, lastUpdate, palinsesti, mesi, listaKPI);
    }
}