package com.alkemytech.sophia.utilizer.model.radioinstore;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Alessandro Russo on 08/12/2018.
 */
@Entity
@Table(name = "PERF_RS_UTILIZATION_FILE")
public class PerfRSUtilizationFile {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "ID" )
    private Long idUtilizationFile;

    @Column( name = "ID_MUSIC_PROVIDER" )
    private Long idMusicProvider;

    @Column( name = "ID_PALINSESTO" )
    private Long idPalinsesto;

    @Column( name = "ID_UTENTE" )
    private Long idUtente;

    @Column( name = "ANNO" )
    private Integer anno;

    @Column( name = "MESE" )
    private Integer mese;

    @Column( name = "DATA_UPLOAD" )
    private Date dataUpload;

    @Column( name = "PERCORSO" )
    private String percorso;

    @Column( name = "TIPO_UPLOAD" )
    private String tipoUpload;

    @Column( name = "STATO" )
    private String stato;

    @Column( name = "DATA_PROCESSAMENTO" )
    private Date dataProcessamento;


    public Long getIdUtilizationFile() {
        return idUtilizationFile;
    }


    public void setIdUtilizationFile(Long idUtilizationFile) {
        this.idUtilizationFile = idUtilizationFile;
    }

    public Long getIdMusicProvider() {
        return idMusicProvider;
    }

    public void setIdMusicProvider(Long idMusicProvider) {
        this.idMusicProvider = idMusicProvider;
    }

    public Long getIdPalinsesto() {
        return idPalinsesto;
    }

    public void setIdPalinsesto(Long idPalinsesto) {
        this.idPalinsesto = idPalinsesto;
    }

    public Long getIdUtente() {
        return idUtente;
    }

    public void setIdUtente(Long idUtente) {
        this.idUtente = idUtente;
    }

    public Integer getAnno() {
        return anno;
    }

    public void setAnno(Integer anno) {
        this.anno = anno;
    }

    public Integer getMese() {
        return mese;
    }

    public void setMese(Integer mese) {
        this.mese = mese;
    }

    public Date getDataUpload() {
        return dataUpload;
    }

    public void setDataUpload(Date dataUpload) {
        this.dataUpload = dataUpload;
    }

    public String getPercorso() {
        return percorso;
    }

    public void setPercorso(String percorso) {
        this.percorso = percorso;
    }

    public String getTipoUpload() {
        return tipoUpload;
    }

    public void setTipoUpload(String tipoUpload) {
        this.tipoUpload = tipoUpload;
    }

    public String getStato() {
        return stato;
    }

    public void setStato(String stato) {
        this.stato = stato;
    }

    public Date getDataProcessamento() {
        return dataProcessamento;
    }

    public void setDataProcessamento(Date dataProcessamento) {
        this.dataProcessamento = dataProcessamento;
    }

}
