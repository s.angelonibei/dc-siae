package com.alkemytech.sophia.utilizer.model.radioinstore;

import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.tools.StringTools;
import com.alkemytech.sophia.utilizer.utily.Utilities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import static com.alkemytech.sophia.utilizer.utily.Utilities.getMapValue;

@Entity
@Table(name="PERF_RS_SHOW_MUSIC")
@NamedQuery(name="PerfRsShowMusic.findAll", query="SELECT p FROM PerfRsShowMusic p")
public class PerfRsShowMusic implements Serializable {

    @Id
    @GeneratedValue( strategy =  GenerationType.IDENTITY)
    @Column( name= "ID_RS_SHOW_MUSIC")
    private Long id;

    @Column(name = "ID_NORMALIZED_FILE")
    private Long idNnormalizedFile;

    @Column(name = "ID_RS_SHOW_SCHEDULE")
    private Long idRsShowSchedule;

    @Column( name = "ID_MUSIC_TYPE")
    private Long idMusicType;

    @Column( name = "TITLE")
    private String title;

    @Column( name= "COMPOSER")
    private String composer;

    @Column( name= "PERFORMER")
    private String performer;

    @Column( name= "ALBUM")
    private String album;

    @Column( name = "DURATION")
    private Integer duration;

    @Column( name= "ISWC_CODE")
    private String iswcCode;

    @Column( name= "ISRC_CODE")
    private String isrcCode;

    @Column( name = "BEGIN_TIME")
    private Date beginTime;

    @Column( name = "REAL_DURATION")
    private Integer realDuration;

    @Column( name = "NOTE")
    private String note;

    @Column( name = "DAYS_FROM_UPLOAD")
    private Integer daysFromUpload;

    @Column( name = "CREATION_DATE")
    private Date creationDate;

    @Column( name = "MODIFY_DATE")
    private Date modifyDate;

    public PerfRsShowMusic() {
    }

    public PerfRsShowMusic(PerfRSNormalizedFile normalizedFile, Date beginTimeOp, Map<String, Object> record) {
        this.idMusicType = getMapValue(record, Constants.DECODE_ID_CATEGORIA_USO_OP) != null ? Long.parseLong(getMapValue(record, Constants.DECODE_ID_CATEGORIA_USO_OP)) : null;
        this.idNnormalizedFile = normalizedFile.getIdNormalizedFile();
        this.title = getMapValue(record, Constants.TITOLO_OP);
        this.composer = getMapValue(record,Constants.COMPOSITORE_OP);
        this.performer = getMapValue(record,Constants.ESECUTORE_OP);
        this.album = getMapValue(record,Constants.ALBUM_OP);
        if( getMapValue(record, Constants.DURATA_OP) != null && getMapValue(record, Constants.DURATA_OP).matches(StringTools.DURATION_FORMAT) ){
            this.duration = Integer.parseInt( StringTools.parseTimeToSeconds( getMapValue(record, Constants.DURATA_OP) ) ) ;
        }
        if( getMapValue(record, Constants.DURATA_EFFETTIVA_OP) != null && getMapValue(record, Constants.DURATA_EFFETTIVA_OP).matches(StringTools.DURATION_FORMAT) ){
            this.realDuration = Integer.parseInt( StringTools.parseTimeToSeconds( getMapValue(record, Constants.DURATA_EFFETTIVA_OP) ) ) ;
        }
        this.iswcCode = getMapValue(record,Constants.CODICE_ISWC_OP);
        this.isrcCode = getMapValue(record,Constants.CODICE_ISRC_OP);
        this.beginTime = beginTimeOp;
        this.creationDate = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdNnormalizedFile() {
        return idNnormalizedFile;
    }

    public void setIdNnormalizedFile(Long idNnormalizedFile) {
        this.idNnormalizedFile = idNnormalizedFile;
    }

    public Long getIdRsShowSchedule() {
        return idRsShowSchedule;
    }

    public void setIdRsShowSchedule(Long idRsShowSchedule) {
        this.idRsShowSchedule = idRsShowSchedule;
    }

    public Long getIdMusicType() {
        return idMusicType;
    }

    public void setIdMusicType(Long idMusicType) {
        this.idMusicType = idMusicType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getComposer() {
        return composer;
    }

    public void setComposer(String composer) {
        this.composer = composer;
    }

    public String getPerformer() {
        return performer;
    }

    public void setPerformer(String performer) {
        this.performer = performer;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getIswcCode() {
        return iswcCode;
    }

    public void setIswcCode(String iswcCode) {
        this.iswcCode = iswcCode;
    }

    public String getIsrcCode() {
        return isrcCode;
    }

    public void setIsrcCode(String isrcCode) {
        this.isrcCode = isrcCode;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Integer getRealDuration() {
        return realDuration;
    }

    public void setRealDuration(Integer realDuration) {
        this.realDuration = realDuration;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getDaysFromUpload() {
        return daysFromUpload;
    }

    public void setDaysFromUpload(Integer daysFromUpload) {
        this.daysFromUpload = daysFromUpload;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PerfRsShowMusic)) return false;

        PerfRsShowMusic that = (PerfRsShowMusic) o;

        if (idMusicType != null ? !idMusicType.equals(that.idMusicType) : that.idMusicType!= null) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        return duration != null ? duration.equals(that.duration) : that.duration == null;

    }

    @Override
    public int hashCode() {
        int result = idMusicType != null ? idMusicType.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PerfRsShowMusic{" +
                "id=" + id +
                ", idNnormalizedFile=" + idNnormalizedFile +
                ", idRsShowSchedule=" + idRsShowSchedule +
                ", idMusicType=" + idMusicType +
                ", title='" + title + '\'' +
                ", composer='" + composer + '\'' +
                ", performer='" + performer + '\'' +
                ", album='" + album + '\'' +
                ", duration=" + duration +
                ", iswcCode='" + iswcCode + '\'' +
                ", isrcCode='" + isrcCode + '\'' +
                ", beginTime=" + beginTime +
                ", realDuration=" + realDuration +
                ", note='" + note + '\'' +
                ", daysFromUpload=" + daysFromUpload +
                ", creationDate=" + creationDate +
                ", modifyDate=" + modifyDate +
                '}';
    }
}