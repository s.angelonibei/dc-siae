package it.siae.sophia.kb.massive.extract.dto;

import com.google.gson.annotations.SerializedName;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class UtilizzazioneOperaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@SerializedName("periodo")
	private String periodo;
	@SerializedName("offerta")
	private String offerta;
	@SerializedName("dsp")
	private String dsp;
	
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public String getOfferta() {
		return offerta;
	}
	public void setOfferta(String offerta) {
		this.offerta = offerta;
	}
	public String getDsp() {
		return dsp;
	}
	public void setDsp(String dsp) {
		this.dsp = dsp;
	}

}
