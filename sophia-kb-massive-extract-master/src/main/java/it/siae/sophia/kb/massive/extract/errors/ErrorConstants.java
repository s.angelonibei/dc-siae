/*
 * @Autor A.D.C.
 */

package it.siae.sophia.kb.massive.extract.errors;


public final class ErrorConstants {

    public static final String ERR_CONCURRENCY_FAILURE = "error.concurrencyFailure";
    public static final String ERR_VALIDATION = "error.validation";
    public static final String PROBLEM_BASE_URL = "sophia-kb-massive-extract";
    public static final String DEFAULT_TYPE = (PROBLEM_BASE_URL + "/problem-with-message");
    public static final String CONSTRAINT_VIOLATION_TYPE = (PROBLEM_BASE_URL + "/contraint-violation");
    public static final String PARAMETERIZED_TYPE = (PROBLEM_BASE_URL + "/parameterized");
    public static final String INVALID_FILTER_TYPE = (PROBLEM_BASE_URL + "/invalid-filter : Must be codiceOpera or uuid");
    public static final String ROW_ALREADY_EXISTS = (PROBLEM_BASE_URL + "/row-already-exists");
    public static final String ROW_DUPLICATE = (PROBLEM_BASE_URL + "/row-duplicate");
    public static final String LOGIN_ALREADY_USED_TYPE = (PROBLEM_BASE_URL + "/login-already-used");
    public static final String EXCEL_NOT_FOUND_TYPE = (PROBLEM_BASE_URL + "/email-not-found");
    public static final String RESPONSE_ERROR = (PROBLEM_BASE_URL + "/response error: The response not return any results");

    private ErrorConstants() {
    }
}
