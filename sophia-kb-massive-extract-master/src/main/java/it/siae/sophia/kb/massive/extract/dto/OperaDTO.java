package it.siae.sophia.kb.massive.extract.dto;

import com.google.gson.annotations.SerializedName;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class OperaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@SerializedName("codiceOpera")
	private String codiceOpera;
	@SerializedName("gradoDiConfidenza")
	private String gradoDiConfidenza;
	@SerializedName("titoloOriginale")
	private String titoloOriginale;
	@SerializedName("confidenzaTitoloOriginale")
	private String confidenzaTitoloOriginale;
	@SerializedName("titoloAlternativo")
	private String titoloAlternativo;
	@SerializedName("confidenzaTitoloAlternativo")
	private String confidenzaTitoloAlternativo;
	@SerializedName("autore")
	private String autore;
	@SerializedName("confidenzaAutore")
	private String confidenzaAutore;
	@SerializedName("compositore")
	private String compositore;
	@SerializedName("confCompositore")
	private String confCompositore;
	@SerializedName("interprete")
	private String interprete;
	@SerializedName("confInterprete")
	private String confInterprete;
	@SerializedName("tipoCodifica")
	private String tipoCodifica;
	@SerializedName("codiceSiae")
	private String codiceSiae;
	@SerializedName("codiceAda")
	private String codiceAda;
	@SerializedName("iswc")
	private String iswc;
	@SerializedName("utilizzazioni")
	private List<UtilizzazioneOperaDTO> utilizzazioni;
	
	

	public String getConfCompositore() {
		return confCompositore;
	}

	public void setConfCompositore(String confCompositore) {
		this.confCompositore = confCompositore;
	}

	public String getConfInterprete() {
		return confInterprete;
	}

	public void setConfInterprete(String confInterprete) {
		this.confInterprete = confInterprete;
	}

	public String getCodiceSiae() {
		return codiceSiae;
	}

	public void setCodiceSiae(String codiceSiae) {
		this.codiceSiae = codiceSiae;
	}

	public String getCodiceAda() {
		return codiceAda;
	}

	public void setCodiceAda(String codiceAda) {
		this.codiceAda = codiceAda;
	}

	public String getIswc() {
		return iswc;
	}

	public void setIswc(String iswc) {
		this.iswc = iswc;
	}

	public String getCodiceOpera() {
		return codiceOpera;
	}

	public void setCodiceOpera(String codiceOpera) {
		this.codiceOpera = codiceOpera;
	}

	public String getGradoDiConfidenza() {
		return gradoDiConfidenza;
	}

	public void setGradoDiConfidenza(String gradoDiConfidenza) {
		this.gradoDiConfidenza = gradoDiConfidenza;
	}

	public String getTitoloOriginale() {
		return titoloOriginale;
	}

	public void setTitoloOriginale(String titoloOriginale) {
		this.titoloOriginale = titoloOriginale;
	}

	public String getConfidenzaTitoloOriginale() {
		return confidenzaTitoloOriginale;
	}

	public void setConfidenzaTitoloOriginale(String confidenzaTitoloOriginale) {
		this.confidenzaTitoloOriginale = confidenzaTitoloOriginale;
	}

	public String getTitoloAlternativo() {
		return titoloAlternativo;
	}

	public void setTitoloAlternativo(String titoloAlternativo) {
		this.titoloAlternativo = titoloAlternativo;
	}

	public String getConfidenzaTitoloAlternativo() {
		return confidenzaTitoloAlternativo;
	}

	public void setConfidenzaTitoloAlternativo(String confidenzaTitoloAlternativo) {
		this.confidenzaTitoloAlternativo = confidenzaTitoloAlternativo;
	}

	public String getAutore() {
		return autore;
	}

	public void setAutore(String autore) {
		this.autore = autore;
	}

	public String getConfidenzaAutore() {
		return confidenzaAutore;
	}

	public void setConfidenzaAutore(String confidenzaAutore) {
		this.confidenzaAutore = confidenzaAutore;
	}

	public String getCompositore() {
		return compositore;
	}

	public void setCompositore(String compositore) {
		this.compositore = compositore;
	}


	public String getInterprete() {
		return interprete;
	}

	public void setInterprete(String interprete) {
		this.interprete = interprete;
	}


	public String getTipoCodifica() {
		return tipoCodifica;
	}

	public void setTipoCodifica(String tipoCodifica) {
		this.tipoCodifica = tipoCodifica;
	}

	public List<UtilizzazioneOperaDTO> getUtilizzazioni() {
		return utilizzazioni;
	}

	public void setUtilizzazioni(List<UtilizzazioneOperaDTO> utilizzazioni) {
		this.utilizzazioni = utilizzazioni;
	}

}
