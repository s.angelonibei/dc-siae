package it.siae.sophia.kb.massive.extract;

import com.alkemytech.sophia.common.rest.RestClient;
import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.http.HTTP;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import it.siae.sophia.kb.massive.extract.config.Configuration;
import it.siae.sophia.kb.massive.extract.dto.RicercaOpereKBDTO;
import it.siae.sophia.kb.massive.extract.errors.BadRequestAlertException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static it.siae.sophia.kb.massive.extract.errors.ErrorConstants.INVALID_FILTER_TYPE;
import static it.siae.sophia.kb.massive.extract.errors.ErrorConstants.RESPONSE_ERROR;

public class KbMassiveExtract {

    private static final Logger logger = LoggerFactory.getLogger(KbMassiveExtract.class);
    private final Properties configuration;
    private final Gson gson;

    @Inject
    public KbMassiveExtract(@Named("configuration") Properties configuration,
                            Gson gson) {
        super();
        this.configuration = configuration;
        this.gson = gson;
    }

    protected static class GuiceModuleExtension extends GuiceModule {

        public GuiceModuleExtension(String[] args, String resourceName) {
            super(args, resourceName);
        }

        @Override
        protected void configure() {
            super.configure();
            // amazon service(s)
            bind(S3.class)
                    .in(Scopes.SINGLETON);
            bind(SQS.class)
                    .in(Scopes.SINGLETON);
            // http
            bind(HTTP.class)
                    .toInstance(new HTTP(configuration));
            bind(Configuration.class)
                    .asEagerSingleton();
            bind(KbMassiveExtract.class)
                    .asEagerSingleton();
        }

    }

    public static void main(final String[] args) throws Exception {

        final KbMassiveExtract instance = Guice
                .createInjector(new GuiceModuleExtension(args,
                        "/configuration.properties")).getInstance(KbMassiveExtract.class);
        new Thread(() -> {
            try {
                String fileNameIn = null;
                try{
                    fileNameIn = args[1];
                    if(fileNameIn.isEmpty() || fileNameIn.equalsIgnoreCase("standalone")){
                        System.out.println("Non e' stato trovato il file di input passato come parametro alla shell. start.sh nomefile.csv standalone");
                        System.out.println("I file di input devono essere presenti nella cartella definita come input");
                        System.exit(-1);
                    }
                } catch (Exception e) {
                    System.out.println("Non e' stato trovato il file di input passato come parametro alla shell. start.sh nomefile.csv");
                    System.out.println("I file di input devono essere presenti nella cartella definita come input");
                    System.exit(-1);
                }
                System.out.println("Elaborazione in corso ...");
                JsonObject output = new JsonObject();
                // standalone mode
                if (Arrays.asList(args).contains("standalone") ||
                        "true".equalsIgnoreCase(instance.configuration
                                .getProperty("kbMassiveExtract.standalone", "false"))) {
                    final JsonObject input = GsonUtils.fromJson(instance.configuration
                            .getProperty("kbMassiveExtract.standalone.message_body"), JsonObject.class);
                    logger.debug("standalone input {}", new GsonBuilder()
                            .setPrettyPrinting().create().toJson(input));
                    processMessage(input, output, instance);
                    logger.debug("standalone output {}", new GsonBuilder()
                            .setPrettyPrinting().create().toJson(output));
                    return;
                } else {
                    JsonObject input = new JsonObject();
                    input.addProperty("path_input", instance.configuration.getProperty("kbMassiveExtract.path_input"));
                    input.addProperty("path_output", instance.configuration.getProperty("kbMassiveExtract.path_output"));
                    input.addProperty("output_format", instance.configuration.getProperty("kbMassiveExtract.output_format"));
                    input.addProperty("delimiter", instance.configuration.getProperty("kbMassiveExtract.output.csv.delimiter"));
                    input.addProperty("filter", instance.configuration.getProperty("kbMassiveExtract.filter"));
                    input.addProperty("fileNameIn", fileNameIn);
                    logger.debug("input {}", new GsonBuilder()
                            .setPrettyPrinting().create().toJson(input));
                    processMessage(input, output, instance);
                    logger.debug("output {}", new GsonBuilder()
                            .setPrettyPrinting().create().toJson(output));
                }

            } catch (IOException | SQLException | InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

    }

    public static JsonObject processMessage(final JsonObject input, final JsonObject output, final KbMassiveExtract instance) throws IOException, SQLException, InterruptedException {

        final File homeFolder = new File(instance.configuration
                .getProperty("default.home_folder"));
        final char kbMassiveExtractCsvDelimiter = input.get("delimiter").getAsString().charAt(0);
        logger.debug("homeFolder {}", homeFolder);

        new Thread(new Runnable() {
            @Override
            public void run() {
                KbMassiveExtract.elaborateCsv(kbMassiveExtractCsvDelimiter, input, output, instance, homeFolder);
            }
        }).start();
        return output;
    }

    public static JsonObject elaborateCsv(char delimiter, final JsonObject input, JsonObject output, final KbMassiveExtract instance, File homeFolder) {
        //configuration
        final Integer maxThread = Integer.parseInt(instance.configuration.getProperty("kbMassiveExtract.trhead_max"));
        final Integer executorTimeout = Integer.parseInt(instance.configuration.getProperty("kbMassiveExtract.executor_shutdown"));
        final String uploadDir = input.get("path_output").getAsString();
        final String filter = input.get("filter").getAsString();
        final List<String[]> records;
        try {
            File csvInput = null;
            //Download File input
            String inPath = input.get("path_input").getAsString();
            csvInput = new File(inPath+input.get("fileNameIn").getAsString());
            logger.debug("Ho preso il file input");

            File csvFinal = new File(uploadDir + csvInput.getName().replace(".csv", "") + "_output_" + new SimpleDateFormat("yyyyMMddHHmm").format(new Date()) + ".csv");
            final String[] columns = input.get("output_format").getAsString().split(String.valueOf(delimiter));

            //Scrittura dell'header
            writeToCsv(columns, csvFinal, delimiter);
            //
            logger.debug("Ho creato il file finale");
            FileReader fileToRead = new FileReader(csvInput);
            CSVReader csvReader = new CSVReaderBuilder(fileToRead)
                    .withSkipLines(1)
                    .build();
            logger.debug("Csv di input loaded: " + csvInput.getName());

            records = csvReader.readAll();
            logger.debug("loaded all records: " + records.size());
            fileToRead.close();
            csvReader.close();

            int partitionSize = records.size() / maxThread;

            if (records.size() <= maxThread) {
                partitionSize = records.size();
            }
            logger.debug("records " + records);
            logger.debug("partitionSize " + partitionSize);


            //Threads
            ExecutorService es = Executors.newCachedThreadPool();

            for (final List<String[]> partitions : Lists.partition(records, partitionSize)) {
                es.execute(new Runnable() {
                    @Override
                    public void run() {
                        logger.debug("Starting elaborateRecords on new Thread " + Thread.currentThread() + " " + partitions.size() + " files");
                        System.out.println("Starting on new Thread " + Thread.currentThread() + " " + partitions.size() + " files");
                        //call service search
                        for (String[] r : partitions) {
                            try {
                                RicercaOpereKBDTO opereKBDTO = new RicercaOpereKBDTO();
                                if (filter.equalsIgnoreCase("codiceOpera")) {
                                    opereKBDTO.setCodice(r[0]);
                                } else if (filter.equalsIgnoreCase("uuid")) {
                                    opereKBDTO.setUuid(r[0]);
                                } else {
                                    throw new BadRequestAlertException(INVALID_FILTER_TYPE, "CSV MASSIVE EXTRACT", "errorFilterSet");
                                }
                                String[] opera = ricercaOpereMassive(opereKBDTO, columns, instance, r[0]);
                                if (opera != null) {
                                    writeToCsv(opera, csvFinal, delimiter);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                                logger.debug("1 catch " + e);
                            }
                        }
                        System.out.println("Terminated Thread " + Thread.currentThread() + " " + partitions.size() + " files");
                    }
                });
            }
            es.shutdown();
            es.awaitTermination(executorTimeout, TimeUnit.MINUTES);

            logger.debug("CSV completed ... ");
            System.out.println("Elaborazione terminata");


        } catch (Exception e) {
            output = input;
            output.addProperty("Stato", "Errore");
            output.addProperty("Error Description", e.getMessage());
            System.out.println("Elaborazione terminata con errore "+e.getMessage());
            return output;
        }
        output = input;
        output.addProperty("Stato", "Completed");
        return output;
    }

    public static String[] ricercaOpereMassive(RicercaOpereKBDTO opereKBDTO, String[] columns, KbMassiveExtract instance, String filter) throws IOException {
        String[] opera = new String[columns.length + 1];
        opera[0] = filter;
        try {
            String searchUrl = instance.configuration.getProperty("ricerca.opere");
            logger.debug(String.format("RicercaOpereMassive: Calling to %s", searchUrl));
            final RestClient.Result response = RestClient.post(searchUrl, GsonUtils.toJson(opereKBDTO));
            logger.debug("Response: " + response.toString());
            if (response.getStatusCode() == 200 && response.getJsonElement().getAsJsonArray().size() != 0) {
                final JsonObject rb = response.getJsonElement().getAsJsonArray().get(0).getAsJsonObject();
                int i = 1;
                for (String column : columns) {
                    logger.debug("col :" + column);
                    switch (column.toLowerCase().replace(" ", "")) {
                        case "uuid":
                            if (rb.has("codiceOpera")) {
                                opera[i] = rb.get("codiceOpera").getAsString();
                            } else {
                                opera[i] = "";
                            }
                            i++;
                            break;
                        case "codiceopera":
                            if (rb.has("codiceSiae")) {
                                opera[i] = rb.get("codiceSiae").getAsString();
                            } else {
                                opera[i] = "";
                            }
                            i++;
                            break;
                        case "titolo":
                            if (rb.has("titoloOriginale")) {
                                opera[i] = rb.get("titoloOriginale").getAsString();
                            } else {
                                opera[i] = "";
                            }
                            i++;
                            break;
                        case "artisti":
                            if (rb.has("autore")) {
                                opera[i] = rb.get("autore").getAsString();
                            } else {
                                opera[i] = "";
                            }
                            i++;
                            break;
                        case "iswc":
                            if (rb.has("iswc")) {
                                opera[i] = rb.get("iswc").getAsString();
                            } else {
                                opera[i] = "";
                            }
                            i++;
                            break;
                        case "gradodiconfidenza":
                            if (rb.has("gradoDiConfidenza")) {
                                opera[i] = rb.get("gradoDiConfidenza").getAsString();
                            } else {
                                opera[i] = "";
                            }
                            i++;
                            break;
                    }
                }
            } else {
                throw new BadRequestAlertException(RESPONSE_ERROR, "RICERCA OPERE MASSIVE", "errorResponse");
            }
        } catch (Exception e) {
            logger.error("RicercaOpereMassive: ", e);
        } finally {
            return opera;
        }
    }

    // ********* UTILS METHOD *********

    private static void writeToCsv(String[] opere, File file, char delimiter) {
        try {
            final FileWriter writer = new FileWriter(file, true);
            final BufferedWriter bufferedWriter = new BufferedWriter(writer);
            final CSVPrinter printer = new CSVPrinter(bufferedWriter, CSVFormat.EXCEL.withDelimiter(delimiter));


            //Utilizzato printrecord perchè il semplice 'print' non è compatibilità con l'append
            printer.printRecord(opere);
            printer.flush();
            printer.close();
            bufferedWriter.close();
            writer.close();

        } catch (Exception e) {
            logger.error("error while writing to csv");
        }
    }

}
