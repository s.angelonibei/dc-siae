#!/bin/sh

DEPLOY_DIR=/var/local/sophia/dev/goal/codifica/

sudo cp -r dev/* $DEPLOY_DIR
sudo rm -r $DEPLOY_DIR/lib/*
sudo cp -r lib/* $DEPLOY_DIR/lib/
sudo cp sophia*.jar $DEPLOY_DIR

cd $DEPLOY_DIR

sudo chown -R orchestrator.sophia *

sudo su orchestrator

