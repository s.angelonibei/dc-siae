#!/bin/sh

HOME=/var/local/sophia/dev/goal/codifica

JPACKAGE=com.alkemytech.sophia.goal
JCLASS=RoyaltySchemesParser
JCONFIG=royalty-schemes-parser.properties
JSTDOUT=$HOME/royalty-schemes-parser.out
#JSTDOUT=/dev/null
JOPTIONS=-Xmx4G
JVERSION=1.2.2

#cd $HOME

nohup java -cp "$HOME/sophia-goal-$JVERSION.jar:$HOME/lib/*" $JOPTIONS $JPACKAGE.$JCLASS $HOME/$JCONFIG 2>&1 >> $JSTDOUT &


