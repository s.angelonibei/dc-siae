#!/bin/sh

HOME=/var/local/sophia/dev/goal/codifica

JPACKAGE=com.alkemytech.sophia.goal
JCLASS=InvoiceLoader
JCONFIG=invoice-loader.properties
JOPTIONS="-Xmx1G -Dlog4j.configuration=file://$HOME/log4j.properties -Dlog4j.configurationFile=$HOME/invoice-loader-log4j2.xml"
JVERSION=1.2.2

#cd $HOME

java -cp "$HOME/sophia-goal-$JVERSION.jar:$HOME/lib/*" $JOPTIONS $JPACKAGE.$JCLASS $HOME/$JCONFIG $1 $2 $3 $4 $5 $6 $7 $8


