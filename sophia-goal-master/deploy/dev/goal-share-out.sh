#!/bin/sh

HOME=/var/local/sophia/dev/goal/codifica

JPACKAGE=com.alkemytech.sophia.goal
JCLASS=GoalShareOut
JCONFIG=goal-share-out.properties
JSTDOUT=$HOME/goal-share-out.out
#JSTDOUT=/dev/null
JOPTIONS=-Xmx1G
JVERSION=1.2.2

#cd $HOME

nohup java -cp "$HOME/sophia-goal-$JVERSION.jar:$HOME/lib/*" $JOPTIONS $JPACKAGE.$JCLASS $HOME/$JCONFIG 2>&1 >> $JSTDOUT &


