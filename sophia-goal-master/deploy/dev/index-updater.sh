#!/bin/sh

HOME=/var/local/sophia/dev/goal/codifica

JPACKAGE=com.alkemytech.sophia.goal
JCLASS=IndexUpdater
JCONFIG=index-updater.properties
JSTDOUT=$HOME/index-updater.out
#JSTDOUT=/dev/null
JOPTIONS=-Xmx8G
JVERSION=1.2.2

#cd $HOME

nohup java -cp "$HOME/sophia-goal-$JVERSION.jar:$HOME/lib/*" $JOPTIONS $JPACKAGE.$JCLASS $HOME/$JCONFIG 2>&1 >> $JSTDOUT &



