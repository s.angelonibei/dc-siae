#!/bin/sh

HOME=/var/local/sophia/test/goal/codifica

JPACKAGE=com.alkemytech.sophia.goal
JCLASS=ItacaJsonParser
JCONFIG=itaca-json-parser.properties
JSTDOUT=$HOME/itaca-json-parser.out
#JSTDOUT=/dev/null
JOPTIONS=-Xmx4G
JVERSION=1.2.2

#cd $HOME

nohup java -cp "$HOME/sophia-goal-$JVERSION.jar:$HOME/lib/*" $JOPTIONS $JPACKAGE.$JCLASS $HOME/$JCONFIG 2>&1 >> $JSTDOUT &



