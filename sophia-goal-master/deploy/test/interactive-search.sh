#!/bin/sh

HOME=/var/local/sophia/test/goal/codifica

JPACKAGE=com.alkemytech.sophia.goal
JCLASS=InteractiveSearch
JCONFIG=interactive-search.properties
JOPTIONS=-Xmx1G
JVERSION=1.2.2

#cd $HOME

java -cp "$HOME/sophia-goal-$JVERSION.jar:$HOME/lib/*" $JOPTIONS $JPACKAGE.$JCLASS $HOME/$JCONFIG


