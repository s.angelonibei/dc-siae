#!/bin/sh

HOME=/home/orchestrator/sophia/goal/codifica

JPACKAGE=com.alkemytech.sophia.goal
JCLASS=GoalScoring
JCONFIG=goal-scoring.properties
JSTDOUT=$HOME/goal-scoring.out
#JSTDOUT=/dev/null
JOPTIONS=-Xmx8G
JVERSION=1.2.2

#cd $HOME

nohup java -cp "$HOME/sophia-goal-$JVERSION.jar:$HOME/lib/*" $JOPTIONS $JPACKAGE.$JCLASS $HOME/$JCONFIG 2>&1 >> $JSTDOUT &


