#!/bin/sh

HOME=/home/orchestrator/sophia/goal/codifica

JPACKAGE=com.alkemytech.sophia.goal
JCLASS=MuleWsRetry
JCONFIG=mule-ws-retry.properties
JSTDOUT=$HOME/mule-ws-retry.out
#JSTDOUT=/dev/null
JOPTIONS=-Xmx2G
JVERSION=1.2.2

#cd $HOME

nohup java -cp "$HOME/sophia-goal-$JVERSION.jar:$HOME/lib/*" $JOPTIONS $JPACKAGE.$JCLASS $HOME/$JCONFIG 2>&1 >> $JSTDOUT &


