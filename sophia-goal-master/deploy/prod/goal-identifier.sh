#!/bin/sh

HOME=/home/orchestrator/sophia/goal/codifica

JPACKAGE=com.alkemytech.sophia.goal
JCLASS=GoalIdentifier
JCONFIG=goal-identifier.properties
JSTDOUT=$HOME/goal-identifier.out
#JSTDOUT=/dev/null
JOPTIONS=-Xmx16G
JVERSION=1.2.2

#cd $HOME

nohup java -cp "$HOME/sophia-goal-$JVERSION.jar:$HOME/lib/*" $JOPTIONS $JPACKAGE.$JCLASS $HOME/$JCONFIG 2>&1 >> $JSTDOUT &


