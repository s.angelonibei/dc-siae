package com.alkemytech.sophia.goal.document;

import java.io.IOException;

import com.alkemytech.sophia.goal.model.Document;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface DocumentStore {

	public DocumentStore startup() throws IOException;
	public DocumentStore shutdown();
	public boolean isReadOnly();
	public Document getById(long id) throws IOException;
	public Document upsert(Document document) throws IOException;
	public DocumentStore select(Document.Selector selector) throws Exception;
	public DocumentStore truncate() throws IOException;
	public DocumentStore defrag() throws IOException;

}
