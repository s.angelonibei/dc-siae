package com.alkemytech.sophia.goal.pojo;

import java.math.BigDecimal;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ToProcessBean {

	public long id;
	public String license;
	public String licenseeCode;
	public String licenseeName;
	public String service;
	public String period;
	public String periodDateFrom;
	public String periodDateTo;
	public long utilizationMode;
	public String fruitionMode;
	public boolean subscription;
	public String reportId;
	public boolean keepWorkCode;
	public String inputFileUrl;
	public String invoiceId;
	public String commercialOffer;
	public String useType;
	public String shareOutId;
	public BigDecimal totalPoints;
	public BigDecimal exArt18Points;
	public BigDecimal amount;

}
