package com.alkemytech.sophia.goal.model;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class Origin {

	public static final int undefined = 0;
	public static final int siae = 1;
	public static final int itunes = 2;
	public static final int spotify = 3;

	public static int valueOf(String text) {
		if ("siae".equals(text)) {
			return siae;
		} else if ("itunes".equals(text)) {
			return itunes;
		} else if ("spotify".equals(text)) {
			return spotify;
		}
		return undefined;
	}
	
	public static String toString(int origin) {
		switch (origin) {
		case siae: return "siae";
		case itunes: return "itunes";
		case spotify: return "spotify";
		default: return "undefined";
		}
	}

}
