package com.alkemytech.sophia.goal.document;

import java.util.Collection;
import java.util.Properties;

import com.alkemytech.sophia.commons.score.TextScore;
import com.alkemytech.sophia.commons.util.SplitCharSequence;
import com.alkemytech.sophia.goal.model.Document;
import com.alkemytech.sophia.goal.model.Text;
import com.alkemytech.sophia.goal.model.TextType;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class GeometricDocumentScore implements DocumentScore {
	
	private final double weight;
	private final double exponent;
	private final TextScore titleScore;
	private final TextScore artistScore;

	public GeometricDocumentScore(Properties configuration, String propertyPrefix) {
		super();
		weight = Double.parseDouble(configuration
				.getProperty(propertyPrefix + ".weight"));
		exponent = Double.parseDouble(configuration
				.getProperty(propertyPrefix + ".exponent"));
		titleScore = newScoreInstance(configuration
				.getProperty(propertyPrefix + ".title_score"));
		artistScore = newScoreInstance(configuration
				.getProperty(propertyPrefix + ".artist_score"));
	}
	
	private TextScore newScoreInstance(String className) {
		try {
			return (TextScore) Class.forName(className).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			throw new IllegalArgumentException(String
					.format("unknown class name \"%s\"", className), e);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.seqrware.copyright.score.DocumentScore#getScore(com.seqrware.copyright.score.SplitCharSequence, java.util.Collection, com.seqrware.copyright.model.Document)
	 */
	@Override
	public double getScore(SplitCharSequence title, Collection<SplitCharSequence> artists, Document document) {
		double titleScore = 0.0;
		double artistScore = 0.0;
		for (Text text : document.texts) {
			if (TextType.title == text.type) {
				if (titleScore < 1.0) {
					final double score = this.titleScore
							.getScore(title, new SplitCharSequence(text.text));
					if (score > titleScore) {
						titleScore = score;
					}
				}
			} else if (TextType.artist == text.type) {
				if (artistScore < 1.0) {
					for (SplitCharSequence artist : artists) {
						final double score = this.artistScore
								.getScore(artist, new SplitCharSequence(text.text));
						if (score > artistScore) {
							artistScore = score;
						}
					}
				}
			}
		}
		return Math.pow(weight * titleScore * artistScore, exponent);
	}
}
