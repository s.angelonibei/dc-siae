package com.alkemytech.sophia.goal.config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.goal.pojo.AngloAmericanRule;
import com.amazonaws.services.s3.model.S3Object;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class AngloAmericanConfig {
		
	private static final Logger logger = LoggerFactory.getLogger(AngloAmericanConfig.class);

	private final Properties configuration;
	private final String propertyPrefix;
	private final S3 s3;

	public AngloAmericanConfig(@Named("configuration") Properties configuration, String propertyPrefix, S3 s3) {
		super();
		this.configuration = configuration;
		this.propertyPrefix = propertyPrefix;
		this.s3 = s3;
	}
	
	public List<AngloAmericanRule> getConfig(String s3FileUrl) throws IOException {
		logger.debug("anglo-american configuration from {}", s3FileUrl);

		int skipRows = Integer.parseInt(configuration
				.getProperty(propertyPrefix + ".skip_rows", "0"));
		final String delimiter = configuration
				.getProperty(propertyPrefix + ".anglo_american.delimiter", ";").trim();	
		final String subDelimiter = configuration
				.getProperty(propertyPrefix + ".anglo_american.sub_delimiter", ":").trim();	

		try (final S3Object s3Object = s3.getObject(new S3.Url(s3FileUrl));
				final InputStream in = s3Object.getObjectContent();
				final InputStreamReader inputStreamReader = new InputStreamReader(in, Charset.forName("UTF-8"));
				final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				final CSVParser csvParser = new CSVParser(bufferedReader, CSVFormat.newFormat(delimiter.charAt(0))
						.withQuoteMode(QuoteMode.MINIMAL)
						.withQuote('"')
						.withIgnoreEmptyLines()
						.withIgnoreSurroundingSpaces())) {			
			
			final Iterator<CSVRecord> csvIterator = csvParser.iterator();
			for (; skipRows > 0 && csvIterator.hasNext(); skipRows --) {
				final CSVRecord record = csvIterator.next();
				logger.debug("record skipped {}", record);
			}			
			
			final List<AngloAmericanRule> rules = new ArrayList<>();
			while (csvIterator.hasNext()) {
				final CSVRecord record = csvIterator.next();
				logger.debug("record {}", record);
				int column = 0;
				final AngloAmericanRule rule = new AngloAmericanRule();
				rule.repertoireCode = record.get(column++);
				rule.repertoireName = record.get(column++);
				rule.seIpiCodes = new HashSet<>(Arrays.asList(record.get(column++).split(subDelimiter)));
				rule.societies = new HashSet<>(Arrays.asList(record.get(column++).split(subDelimiter)));
				for (String society : rule.societies)
					if (society.startsWith("!")) {
						rule.notPrefixedSocieties = true;
						break;
					}
				if (rule.notPrefixedSocieties) {
					final Set<String> societies = new HashSet<>();
					for (String society : rule.societies)
						if (society.startsWith("!"))
							societies.add(society.substring(1)); // strip leading '!'
						else
							throw new IOException(String
									.format("mixed not operators in societies set {}", rule.societies));
					rule.societies = societies;
				}
				final String[] excludedDsps = record.get(column++).split(subDelimiter);
				rule.excludedDsps = null == excludedDsps || 0 == excludedDsps.length ?
						null : new HashSet<>(Arrays.asList(excludedDsps));
				rule.demSiaePosition = record.get(column++);
				rule.demAccountId = record.get(column++);
				rule.demAccountCode = record.get(column++);
				rule.drmSiaePosition = record.get(column++);
				rule.drmAccountId = record.get(column++);
				rule.drmAccountCode = record.get(column++);
				logger.debug("rule {}", rule);
				rules.add(rule);
			}
			
			return rules;
		}	
	}
	
}
