package com.alkemytech.sophia.goal;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.http.HTTP;
import com.alkemytech.sophia.commons.util.BigDecimals;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.goal.jdbc.McmdbDAO;
import com.alkemytech.sophia.goal.jdbc.McmdbDataSource;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import com.ibm.icu.text.DateFormat;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class GoalShareOut {
	
	private static final Logger logger = LoggerFactory.getLogger(GoalShareOut.class);

	private static final String DATE_FORMAT = "[0-9]{4}\\-[0-9]{2}\\-[0-9]{2}.*";

	private static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// http
			bind(HTTP.class)
				.toInstance(new HTTP(configuration));
			// amazon service(s)
			bind(S3.class)
				.toInstance(new S3(configuration));
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("MCMDB"))
				.to(McmdbDataSource.class)
				.asEagerSingleton();
			// data access object(s)
			bind(McmdbDAO.class)
				.asEagerSingleton();
			// this
			bind(GoalShareOut.class)
				.asEagerSingleton();
		}
		
	}

	public static void main(String[] args) {
		try {
			final GoalShareOut instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/goal-share-out.properties"))
					.getInstance(GoalShareOut.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Properties configuration;
	private final Charset charset;
	private final Gson gson;
	private final HTTP http;
	private final S3 s3;
	private final McmdbDAO dao;

	@Inject
	protected GoalShareOut(@Named("configuration") Properties configuration,
			@Named("charset") Charset charset,
			Gson gson, HTTP http, S3 s3, McmdbDAO dao) throws IOException {
		super();
		this.configuration = configuration;
		this.charset = charset;
		this.gson = gson;
		this.http = http;
		this.s3 = s3;
		this.dao = dao;
	}
	
	public GoalShareOut startup() throws IOException {
		if ("true".equalsIgnoreCase(configuration.getProperty("goal_share_out.locked", "true"))) {
			throw new IllegalStateException("application locked");
		}
		http.startup();
		s3.startup();
		return this;
	}

	public GoalShareOut shutdown() throws IOException {
		s3.shutdown();
		http.shutdown();
		return this;
	}
	
	private String escapeCsvValue(String value, char delimiter) {
		if (Strings.isNullOrEmpty(value))
			return "";
		if (-1 != value.indexOf(delimiter))
			return new StringBuilder()
					.append('"')
					.append(value.replace("\"", "\"\""))
					.append('"')
					.toString();
		return value;
	}
	
	private GoalShareOut process(String[] args) throws SQLException, IOException {
		
		final int bindPort = Integer.parseInt(configuration
				.getProperty("goal_share_out.bind_port", configuration
						.getProperty("default.bind_port", "0")));

		// bind lock tcp port
		try (final ServerSocket socket = new ServerSocket(bindPort)) {
			logger.debug("socket bound to {}", socket.getLocalSocketAddress());
			
			// process database record(s)
			final List<Map<String, String>> reports = dao
					.executeQuery("goal_share_out.sql.select_to_process");
			logger.debug("{} report(s) to process", reports.size());
			for (Map<String, String> report : reports)
				process(report);

		}
		
		return this;
	}
	
	private void process(final Map<String, String> toProcess) throws SQLException, IOException {
		logger.debug("toProcess {}", toProcess);

		final long startTimeMillis = System.currentTimeMillis();

		// compute period
		final String periodDateFrom = toProcess.get("periodDateFrom");
		final String periodDateTo = toProcess.get("periodDateTo");
		if (!Pattern.matches(DATE_FORMAT, periodDateFrom))
			throw new IllegalArgumentException(String
					.format("malformed date %s", periodDateFrom));
		if (!Pattern.matches(DATE_FORMAT, periodDateTo))
			throw new IllegalArgumentException(String
					.format("malformed date %s", periodDateTo));
		final String year = periodDateTo.substring(0, 4);
		final String month = periodDateTo.substring(5, 7);
		toProcess.put("year", year);
		toProcess.put("month", month);
		logger.debug("year {}", year);
		logger.debug("month {}", month);

		// configuration params
		final File homeFolder = new File(configuration
				.getProperty("goal_share_out.home_folder"));
		
		int skipRows = Integer.parseInt(configuration
				.getProperty("goal_share_out.skip_rows", "0"));
		final char delimiter = configuration
				.getProperty("goal_share_out.delimiter", ";").trim().charAt(0);	
		final int columns = Integer.parseInt(configuration
				.getProperty("goal_share_out.columns"));
		final int columnInvoiceId = Integer.parseInt(configuration
				.getProperty("goal_share_out.column.invoice_id"));
		final int columnDemPoints = Integer.parseInt(configuration
				.getProperty("goal_share_out.column.dem_points"));
		final int columnDrmPoints = Integer.parseInt(configuration
				.getProperty("goal_share_out.column.drm_points"));

		final HeartBeat heartbeat = HeartBeat.constant("record", Integer.parseInt(configuration
				.getProperty("goal_share_out.heartbeat", configuration.getProperty("default.heartbeat", "1000"))));

		// additional info
		final JsonObject additionalInfo = new JsonObject();
		try {
			additionalInfo.addProperty("hostname", InetAddress.getLocalHost().getHostName());
		} catch (UnknownHostException e) {
			additionalInfo.addProperty("hostname", "localhost");
		}
		
		// process started
		toProcess.put("additionalInfo", gson.toJson(additionalInfo));
		dao.executeUpdate("goal_share_out.sql.update_started", toProcess);

		// temporary local file(s)
		final UUID uuid = UUID.randomUUID();
		final File inputFile = new File(homeFolder, uuid + "-input.csv");
		final File outputFile = new File(homeFolder, uuid + "-output.csv");
		inputFile.deleteOnExit();
		outputFile.deleteOnExit();

		// download file from s3
		final String inputFileUrl = toProcess.get("inputFileUrl");
		if (!s3.download(new S3.Url(inputFileUrl), inputFile)) {
			logger.warn("input file download error: {}", inputFileUrl);
			// process failed
			additionalInfo.addProperty("errorMessage", String
					.format("input file download error: %s", inputFileUrl));
			toProcess.put("additionalInfo", gson.toJson(additionalInfo));
			dao.executeUpdate("goal_share_out.sql.update_failed", toProcess);
			return;
		} else
			logger.debug("file downloaded {}", inputFileUrl);

		// pre-computed values
		final BigDecimal totalPoints = new BigDecimal(toProcess.get("totalPoints"));
		final BigDecimal exArt18Points = new BigDecimal(toProcess.get("exArt18Points"));
		final BigDecimal amount = new BigDecimal(toProcess.get("amount"));
		final BigDecimal pointValue = BigDecimals.isAlmostZero(totalPoints) ?
				BigDecimal.ZERO : BigDecimals.divide(amount, totalPoints);
		final BigDecimal exArt18Amount = BigDecimals
				.setScale(pointValue.multiply(exArt18Points));
		logger.debug("pointValue {}", BigDecimals.toPlainString(pointValue));
		logger.debug("exArt18Amount {}", BigDecimals.toPlainString(exArt18Amount));
		
		// constant(s)
		final String invoiceId = toProcess.get("invoiceId");
		
		// stats
		long totalRecords = 0L;

		// process report
		logger.debug("processing input file {}", inputFile);
		long elapsedTimeMillis = 0L;
		
		try (final OutputStream outputStream = new FileOutputStream(outputFile);
				final BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);
				final FileInputStream inputStream = new FileInputStream(inputFile);
				final InputStreamReader inputStreamReader = new InputStreamReader(inputStream, charset);
				final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				final CSVParser csvParser = new CSVParser(bufferedReader, CSVFormat.newFormat(delimiter)
						.withQuoteMode(QuoteMode.MINIMAL)
						.withQuote('"')
						.withIgnoreEmptyLines()
						.withIgnoreSurroundingSpaces())) {			
			
			final Iterator<CSVRecord> csvIterator = csvParser.iterator();
			for (; skipRows > 0 && csvIterator.hasNext(); skipRows --) {
				final CSVRecord record = csvIterator.next();
				logger.debug("record skipped {}", record);
			}

			while (csvIterator.hasNext()) {
				final CSVRecord record = csvIterator.next();
				
				if (columns != record.size())
					throw new IllegalStateException(String
							.format("bad number of columns %d %s", record.size(), record.toString()));

				// parse line
				final String demPoints = record.get(columnDemPoints);
				final String drmPoints = record.get(columnDrmPoints);
				
				totalRecords ++;
				
				// DEM amount
				final BigDecimal demAmount = Strings.isNullOrEmpty(demPoints) ?
						BigDecimal.ZERO : new BigDecimal(demPoints).multiply(pointValue);
				
				// DRM amount
				final BigDecimal drmAmount = Strings.isNullOrEmpty(drmPoints) ?
						BigDecimal.ZERO : new BigDecimal(drmPoints).multiply(pointValue);

//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  NOME_CAMPO                              TIPO_DI_DATO   NULLABLE    NOTE
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Codice provider                         CHAR(4)        FALSE       Codice di riferimento del provider SOPHIA
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Offerta commerciale                     CHAR           FALSE       Offerta commerciale SOPHIA
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Tipo utilizzo                           CHAR(4)        FALSE       Codice tipo utilizzo dal CCID
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Territorio                              CHAR(3)        FALSE       Territorio dell'utilizzazione
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Mese di incasso fattura                 NUMBER(6)      FALSE       Anno e mese di incasso fattura 
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Anno di riferimento utilizzazione       NUMBER(4)      FALSE    
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Tipo di periodo di riferimento          CHAR(1)        FALSE       M = "Mensile"; T = "Trimestrale", S = "Semestrale"; A = "Annuale"
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Periodo di riferimento utilizzazione    NUMBER(5)      FALSE       "Periodo di riferimento dell'utilizzazione.
//	                                                                       In caso di "Tipo di periodo di riferimento" = M i valori possibili saranno: 1-12
//	                                                                       In caso di "Tipo di periodo di riferimento" = T i valori possibili saranno: 1-4
//	                                                                       In caso di "Tipo di periodo di riferimento" = S i valori possibili saranno: 1-2
//	                                                                       In caso di "Tipo di periodo di riferimento" = A il valore possibile è: 1"
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Tipo utilizzazione                      CHAR           FALSE       O = "Online"; B = "Broadcasting"; P = "Performing"
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Tipo repertorio                         CHAR           FALSE       M = "Musica"
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Quantità utilizzazioni                  NUMBER         FALSE       Numero di utilizzazioni dell'opera
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Codice opera                            CHAR(11)       TRUE        Codice opera a 11 cifre
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Flag opera                              CHAR           FALSE       R = "Regolare"; I = "Irregolare"; P = "Provvisoria"; N = "Non Riconosciuta"
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Tipo codifica                           CHAR           TRUE        A = "Automatica"; F = "Fuzzy"; M = "Manuale"; W = "ISWC"; R = "ISRC"
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Qualifica Avente Diritto                CHAR(2)        TRUE        La qualifica è sempre presente tranne nei casi in cui le quote vengono accantonate su conti speciali
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Posizione Siae Avente Diritto           NUMBER         TRUE        Posizione SIAE dell'avente diritto
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  IPI number AD                           CHAR(9)        TRUE        IPI number dell'avente diritto così come presente nello schema di riparto
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Numeratore quota DEM                    NUMBER         FALSE       Numeratore della quota DEM
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Denominatore quota DEM                  NUMBER         FALSE       Denominatore della quota DEM
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Codice conto DEM                        CHAR(2)        FALSE       Codice conto quota DEM
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Numero conto DEM                        CHAR(9)        FALSE       Numero conto quota DEM
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Compenso DEM netta Avente Diritto       NUMBER         FALSE       Compenso DEM per Avente Diritto al netto dell'aggio SIAE
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Numeratore quota DRM                    NUMBER         FALSE       Numeratore della percentuale DRM espressa in diecimillesimi
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Denominatore quota DRM                  NUMBER(5)      FALSE       Denominatore della quota DRM fisso a 10000
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Codice conto DRM                        CHAR(2)        FALSE       Codice conto quota DRM
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Numero conto DRM                        CHAR(9)        FALSE       Numero conto quota DRM
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Compenso DRM netta Avente Diritto       NUMBER         FALSE       Compenso DRM per Avente Diritto al netto dell'aggio SIAE
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Percentuale di Claim                    NUMBER(7)      TRUE        Percentuale totale di claim per questa riga, dopo l'applicazione dello split (x 10000)
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Titolo utilizzazione                    STRING         FALSE       Titolo dell'utilizzazione presente nel DSR
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Nominativo utilizzazione                STRING         TRUE        Nominativo AADD/Esecutori come da report
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------

				// format output csv line
				final StringBuilder outputLine = new StringBuilder();
				final Iterator<String> iterator = record.iterator();
				outputLine.append(iterator.next());
				for (int column = 1; iterator.hasNext(); column ++) {
					final String text = iterator.next();
					outputLine.append(delimiter);
					if (column == columnInvoiceId) {
						outputLine.append(invoiceId);
					} else if (column == columnDemPoints) {
						outputLine.append(BigDecimals.toPlainString(demAmount));
					} else if (column == columnDrmPoints) {
						outputLine.append(BigDecimals.toPlainString(drmAmount));
					} else {
						outputLine.append(escapeCsvValue(text, delimiter));
					}
				}

				// write to output
				byte[] outputBytes = outputLine
						.append('\n')
						.toString()
						.getBytes(charset);
				bufferedOutputStream.write(outputBytes);
				
				heartbeat.pump();
			}

		} catch (Exception e) {
			logger.error("process", e);
			// process failed
			final StringWriter stackTrace = new StringWriter();
			e.printStackTrace(new PrintWriter(stackTrace));
			additionalInfo.addProperty("errorMessage", "error processing file");
			additionalInfo.addProperty("stackTrace", stackTrace.toString());
			toProcess.put("additionalInfo", gson.toJson(additionalInfo));
			dao.executeUpdate("goal_share_out.sql.update_failed", toProcess);
			return;
		}
		
		logger.debug("{} records processed", heartbeat.getTotalPumps());
		elapsedTimeMillis = System.currentTimeMillis() - startTimeMillis;
		logger.debug("share-out completed in {}",
				TextUtils.formatDuration(elapsedTimeMillis));

		// upload output file to s3
		final String outputFileUrl = configuration
				.getProperty("goal_share_out.output_file_url")
				.replace("{shareOutId}", toProcess.get("shareOutId"))
				.replace("{period}", toProcess.get("period"))
				.replace("{reportId}", toProcess.get("reportId"));
		if (!s3.upload(new S3.Url(outputFileUrl), outputFile)) {
			logger.error("output file upload error: {}", outputFileUrl);
			// process failed
			toProcess.put("additionalInfo", String
					.format("{\"errorMessage\":\"output file upload error: %s\"}", outputFileUrl));
			dao.executeUpdate("goal_share_out.sql.update_failed", toProcess);
		return;
		} else
			logger.debug("file {} uploaded to {}", outputFile.getName(), outputFileUrl);

		// process completed
		additionalInfo.addProperty("totalRecords", totalRecords);
		additionalInfo.addProperty("startTime", DateFormat
				.getInstance().format(new Date(startTimeMillis)));
		additionalInfo.addProperty("totalDuration", 
				TextUtils.formatDuration(elapsedTimeMillis));
		logger.debug("additionalInfo {}", additionalInfo);
		toProcess.put("outputFileUrl", outputFileUrl);
		toProcess.put("exArt18Amount", BigDecimals.toPlainString(exArt18Amount));
		toProcess.put("additionalInfo", gson.toJson(additionalInfo));
		dao.executeUpdate("goal_share_out.sql.update_completed", toProcess);


		// delete temporary files
		inputFile.delete();
		outputFile.delete();
		
	}

}
