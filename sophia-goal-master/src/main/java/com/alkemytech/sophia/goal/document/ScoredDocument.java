package com.alkemytech.sophia.goal.document;

import com.alkemytech.sophia.goal.model.Document;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ScoredDocument {

	public final Document document;
	public final double score;
	
	public ScoredDocument(Document document, double score) {
		super();
		this.document = document;
		this.score = score;
	}
	
}
