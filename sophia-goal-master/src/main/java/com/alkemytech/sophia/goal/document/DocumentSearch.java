package com.alkemytech.sophia.goal.document;

import java.io.IOException;
import java.util.Collection;

import com.alkemytech.sophia.commons.index.LevenshteinIndex;
import com.alkemytech.sophia.commons.query.TitleArtistQuery;
import com.alkemytech.sophia.commons.text.TextTokenizer;
import com.alkemytech.sophia.commons.util.LongArray;
import com.alkemytech.sophia.commons.util.SplitCharSequence;
import com.alkemytech.sophia.goal.model.Document;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class DocumentSearch {
		
	private final DocumentStore documentStore;
	private final TextTokenizer titleTokenizer;
	private final TextTokenizer artistTokenizer;
	private final TitleArtistQuery titleArtistQuery;
	private final DocumentScore documentScore;

	@Inject
	public DocumentSearch(DocumentStore documentStore,
			@Named("title_tokenizer") TextTokenizer titleTokenizer,
			@Named("artist_tokenizer") TextTokenizer artistTokenizer,
			DocumentScore documentScore,
			TitleArtistQuery titleArtistQuery) {
		super();
		this.documentStore = documentStore;
		this.titleTokenizer = titleTokenizer;
		this.artistTokenizer = artistTokenizer;
		this.documentScore = documentScore;
		this.titleArtistQuery = titleArtistQuery;
	}
	
	private ScoredDocument findBestDocument(SplitCharSequence normalizedTitle, Collection<SplitCharSequence> normalizedArtists, LongArray postings, int maxPostings, double autoScore) throws IOException {
		Document bestDocument = null;
		double bestScore = 0.0;
		final int limit = Math.min(maxPostings, postings.size());
		for (int i = 0; i < limit && bestScore < autoScore; i ++) {
			final Document document = documentStore
					.getById(postings.get(i));
			final double score = documentScore
					.getScore(normalizedTitle, normalizedArtists, document);
			if (bestScore < score) {
				bestScore = score;
				bestDocument = document;
			}
		}
		if (null == bestDocument)
			return null;
		return new ScoredDocument(bestDocument, bestScore);
	}
	
	public ScoredDocument search(LevenshteinIndex codeIndex, SplitCharSequence normalizedTitle, Collection<SplitCharSequence> normalizedArtists, String code, int maxPostings, double autoScore) throws IOException {
		final LevenshteinIndex.TermSet termSet = codeIndex.search(code);
		return termSet.next() ? findBestDocument(normalizedTitle, normalizedArtists,
				termSet.getPostings(), maxPostings, autoScore) : null;
	}
	
	public ScoredDocument search(LevenshteinIndex titleIndex, LevenshteinIndex artistIndex, SplitCharSequence normalizedTitle, Collection<SplitCharSequence> normalizedArtists, int maxPostings, double autoScore) throws IOException {
		
		// title term(s)
		final Collection<String> titleTerms = titleTokenizer.clear()
				.tokenize(normalizedTitle.toString()).getTokens();
		if (titleTerms.isEmpty()) {
			return null;
		}

		// artist term(s)
		artistTokenizer.clear();
		for (SplitCharSequence normalizedArtist : normalizedArtists) {
			artistTokenizer.tokenize(normalizedArtist.toString());
		}
		final Collection<String> artistTerms = artistTokenizer.getTokens();
		if (artistTerms.isEmpty()) {
			return null;
		}

		// search posting(s)
		final LongArray postings = titleArtistQuery
				.search(titleIndex, artistIndex, titleTerms, artistTerms);
		if (null == postings || postings.isEmpty()) {
			return null;
		}

		// score posting(s)
		return findBestDocument(normalizedTitle, normalizedArtists, postings, maxPostings, autoScore);
		
	}
	
}