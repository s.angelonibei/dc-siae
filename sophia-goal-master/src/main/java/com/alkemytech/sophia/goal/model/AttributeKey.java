package com.alkemytech.sophia.goal.model;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface AttributeKey {

	public static final int undefined = 0;
	public static final int flag_pd = 1;
	public static final int flag_el = 2;
	public static final int ambito = 3;
	public static final int rilevanza = 4;
	
}
