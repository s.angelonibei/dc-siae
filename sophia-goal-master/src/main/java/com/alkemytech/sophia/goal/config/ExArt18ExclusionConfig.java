package com.alkemytech.sophia.goal.config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.goal.pojo.ExArt18Exclusion;
import com.amazonaws.services.s3.model.S3Object;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ExArt18ExclusionConfig {
		
	private static final Logger logger = LoggerFactory.getLogger(ExArt18ExclusionConfig.class);

	private final Properties configuration;
	private final String propertyPrefix;
	private final S3 s3;

	public ExArt18ExclusionConfig(@Named("configuration") Properties configuration, String propertyPrefix, S3 s3) {
		super();
		this.configuration = configuration;
		this.propertyPrefix = propertyPrefix;
		this.s3 = s3;
	}
	
	public List<ExArt18Exclusion> getConfig(String s3FileUrl) throws IOException {
		logger.debug("ex art 18 exclusions configuration from {}", s3FileUrl);

		int skipRows = Integer.parseInt(configuration
				.getProperty(propertyPrefix + ".skip_rows", "0"));
		final String delimiter = configuration
				.getProperty(propertyPrefix + ".delimiter", ";").trim();

		try (final S3Object s3Object = s3.getObject(new S3.Url(s3FileUrl));
				final InputStream in = s3Object.getObjectContent();
				final InputStreamReader inputStreamReader = new InputStreamReader(in, Charset.forName("UTF-8"));
				final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				final CSVParser csvParser = new CSVParser(bufferedReader, CSVFormat.newFormat(delimiter.charAt(0))
						.withQuoteMode(QuoteMode.MINIMAL)
						.withQuote('"')
						.withIgnoreEmptyLines()
						.withIgnoreSurroundingSpaces())) {			
			
			final Iterator<CSVRecord> csvIterator = csvParser.iterator();
			for (; skipRows > 0 && csvIterator.hasNext(); skipRows --) {
				final CSVRecord record = csvIterator.next();
				logger.debug("record skipped {}", record);
			}			
			
			final List<ExArt18Exclusion> exclusions = new ArrayList<>();
			while (csvIterator.hasNext()) {
				final CSVRecord record = csvIterator.next();
				logger.debug("record {}", record);
				int column = 0;
				final ExArt18Exclusion exclusion = new ExArt18Exclusion();
				exclusion.type = record.get(column++).trim();
				exclusion.value = record.get(column++).trim();
				if (!exclusion.isAngloAmerican() && !exclusion.isSociety())
					throw new IOException(String
							.format("invalid exclusion type {}", exclusion.type));
				logger.debug("exclusion {}", exclusion);
				exclusions.add(exclusion);
			}
			
			return exclusions;
		}		
	}
	
}
