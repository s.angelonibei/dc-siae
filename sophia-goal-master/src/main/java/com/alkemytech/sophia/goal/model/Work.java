package com.alkemytech.sophia.goal.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.alkemytech.sophia.commons.nosql.NoSqlEntity;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;
import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@Entity
public class Work implements NoSqlEntity {

	public static interface Selector {
		public void select(Work work) throws Exception;
	}
	
	public static boolean isValid(Work work) {
		if (null == work) {
			return false;
		} else if (0L == work.id) {
			return false;
		}
		if (null == work.codes || work.codes.isEmpty()) {
			return false;
		} else {
			for (Code code : work.codes) {
				if (CodeType.undefined == code.type) {
					return false;
				} else if (Strings.isNullOrEmpty(code.text)) {
					return false;
				}
			}
		}
		if (null == work.titles || work.titles.isEmpty()) {
			return false;
		} else {
			boolean originalTitle = false;
			for (Title title : work.titles) {
				if (TitleType.undefined == title.type) {
					return false;
				} else if (Strings.isNullOrEmpty(title.text)) {
					return false;
				}
				if (TitleType.original == title.type) {
					originalTitle = true;
				}
			}
			if (!originalTitle) {
				return false;
			}
		}
		if (null == work.artists || work.artists.isEmpty()) {
			return false;
		} else {
			for (Artist artist : work.artists) {
				if (ArtistRole.undefined == artist.role) {
					return false;
				} else if (Strings.isNullOrEmpty(artist.text)) {
					return false;
				}
			}
		}
		return true;
	}
	
	@PrimaryKey
	public long id;
	public Set<Code> codes;
	public Set<Title> titles;
	public Set<Artist> artists;
	public Set<Attribute> attributes;

	public Work() {
		super();
	}

	public Work(long id) {
		super();
		this.id = id;
	}
	
	public Work(long id, Set<Code> codes, Set<Title> titles, Set<Artist> artists, Set<Attribute> attributes) {
		super();
		this.id = id;
		this.codes = codes;
		this.titles = titles;
		this.artists = artists;
		this.attributes = attributes;
	}
	
	public boolean add(Code code) {
		if (null == codes)
			codes = new HashSet<>();
		return codes.add(code);
	}

	public boolean add(Title title) {
		if (null == titles)
			titles = new HashSet<>();
		return titles.add(title);
	}

	public boolean add(Artist artist) {
		if (null == artists)
			artists = new HashSet<>();
		return artists.add(artist);
	}

	public boolean add(Attribute attribute) {
		if (null == attributes)
			attributes = new HashSet<>();
		return attributes.add(attribute);
	}

	public List<String> getCodes(int type) {
		if (null == codes)
			return null;
		List<String> result = null;
		for (Code code : codes) {
			if (code.type == type) {
				if (null == result)
					result = new ArrayList<>(2);
				result.add(code.text);
			}
		}
		return result;
	}
	
	public String getAttribute(int key, int origin) {
		if (null != attributes) {
			for (Attribute attribute : attributes) {
				if (origin == attribute.origin) {
					if (key == attribute.key) {
						return attribute.value;
					}
				}
			}
		}
		return null;
	}
	
	public boolean setAttribute(int key, String value, int origin) {
		if (null == attributes)
			attributes = new HashSet<>();
		return attributes.add(new Attribute(key, value, origin));
	}

	@Override
	public String getPrimaryKey() {
		return Long.toString(id);
	}

	@Override
	public String getSecondaryKey() {
		return null;
	}

	@Override
	public int hashCode() {
		int result = null == artists ? 31 : 31 + artists.hashCode();
		result = 31 * result + (null == attributes ? 0 : attributes.hashCode());
		result = 31 * result + (null == codes ? 0 : codes.hashCode());
		result = 31 * result + (int) (id ^ (id >>> 32));
		return 31 * result + (null == titles ? 0 : titles.hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (null == obj) {
			return false;
		} else if (getClass() != obj.getClass()) {
			return false;
		}
		final Work other = (Work) obj;
		if (null == artists) {
			if (null != other.artists) {
				return false;
			}
		} else if (!artists.equals(other.artists)) {
			return false;
		}
		if (null == attributes) {
			if (null != other.attributes) {
				return false;
			}
		} else if (!attributes.equals(other.attributes)) {
			return false;
		}
		if (null == codes) {
			if (null != other.codes) {
				return false;
			}
		} else if (!codes.equals(other.codes)) {
			return false;
		}
		if (id != other.id) {
			return false;
		}
		if (null == titles) {
			if (null != other.titles) {
				return false;
			}
		} else if (!titles.equals(other.titles)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.disableHtmlEscaping()
				.create()
				.toJson(this);
	}
	
}
