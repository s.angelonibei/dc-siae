package com.alkemytech.sophia.goal.model;

import com.google.gson.GsonBuilder;
import com.sleepycat.persist.model.Persistent;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@Persistent
public class Artist {

	public String text;
	public int role;
	public int origin;

	public Artist() {
		super();
	}

	public Artist(String text, int role, int origin) {
		super();
		this.text = text;
		this.role = role;
		this.origin = origin;
	}

	@Override
	public int hashCode() {
		return null == text ? 31 : 31 + text.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (null == obj) {
			return false;
		} else if (getClass() != obj.getClass()) {
			return false;
		}
		final Artist other = (Artist) obj;
		if (null == text) {
			if (null != other.text) {
				return false;
			}
		} else if (!text.equals(other.text)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.disableHtmlEscaping()
				.create()
				.toJson(this);
	}

}
