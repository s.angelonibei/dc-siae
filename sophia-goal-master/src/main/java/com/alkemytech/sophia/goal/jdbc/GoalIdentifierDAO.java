package com.alkemytech.sophia.goal.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.goal.pojo.CompletedBean;
import com.alkemytech.sophia.goal.pojo.ToProcessBean;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class GoalIdentifierDAO {

	private final Logger logger = LoggerFactory.getLogger(GoalIdentifierDAO.class);

	private final Properties configuration;
	private final DataSource dataSource;
	

	@Inject
	public GoalIdentifierDAO(@Named("configuration") Properties configuration,
			@Named("MCMDB") DataSource dataSource) {
		super();
		this.configuration = configuration;
		this.dataSource = dataSource;
	}

	public DataSource getDataSource() {
		return dataSource;
	}
	
	public double selectThreshold() throws SQLException {
		final String sql = configuration
				.getProperty("goal_identifier.sql.select_threshold");
		logger.debug("executing sql \"{}\"", sql);
		try (final Connection connection = dataSource.getConnection();
				final Statement statement = connection.createStatement();
				final ResultSet resultSet = statement.executeQuery(sql)) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			if (resultSet.next()) {
				return resultSet.getDouble("threshold");
			}
			return 0.0;
		}
	}

	public List<ToProcessBean> selectToProcess() throws SQLException {
		final List<ToProcessBean> resultList = new ArrayList<>();
		final String sql = configuration
				.getProperty("goal_identifier.sql.select_to_process");
		logger.debug("executing sql \"{}\"", sql);
		try (final Connection connection = dataSource.getConnection();
				final Statement statement = connection.createStatement();
				final ResultSet resultSet = statement.executeQuery(sql)) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			while (resultSet.next()) {
				final ToProcessBean bean = new ToProcessBean();
				bean.id = resultSet.getLong("id");
				bean.license = resultSet.getString("license");
				bean.licenseeCode = resultSet.getString("licenseeCode");
				bean.licenseeName = resultSet.getString("licenseeName");
				bean.service = resultSet.getString("service");
				bean.period = resultSet.getString("period");
				bean.periodDateFrom = resultSet.getString("periodDateFrom");
				bean.periodDateTo = resultSet.getString("periodDateTo");
				bean.utilizationMode = resultSet.getLong("utilizationMode");
				bean.fruitionMode = resultSet.getString("fruitionMode");
				bean.subscription = resultSet.getBoolean("subscription");
				bean.reportId = resultSet.getString("reportId");
				bean.keepWorkCode = resultSet.getBoolean("keepWorkCode");
				bean.inputFileUrl = resultSet.getString("inputFileUrl");
				resultList.add(bean);
			}
		}
		return resultList;
	}

	public synchronized boolean updateStarted(ToProcessBean request, String additionalInfo) throws SQLException {
		final String sql = configuration
				.getProperty("goal_identifier.sql.update_started")
				.replace("{id}", Long.toString(request.id))
				.replace("{additionalInfo}", additionalInfo.replace("'", "''"));
		logger.debug("executing sql \"{}\"", sql);
		try (final Connection connection = dataSource.getConnection();
				final Statement statement = connection.createStatement()) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			return 1 == statement.executeUpdate(sql);
		}
	}
	
	public synchronized boolean updateCompleted(ToProcessBean request, CompletedBean response) throws SQLException {
		final String sql = configuration
				.getProperty("goal_identifier.sql.update_completed")
				.replace("{id}", Long.toString(request.id))
				.replace("{outputFileUrl}", response.outputFileUrl)
				.replace("{totalRecords}", Long.toString(response.totalRecords))
				.replace("{identifiedRecords}", Long.toString(response.identifiedRecords))
				.replace("{totalSales}", Long.toString(response.totalSales))
				.replace("{identifiedSales}", Long.toString(response.identifiedSales))
				.replace("{webServiceSuccess}", Boolean.toString(response.webServiceSuccess))
				.replace("{webServicePayload}", response.webServicePayload.replace("'", "''"))
				.replace("{threshold}", String.format("%.3f", selectThreshold()))
				.replace("{additionalInfo}", response.additionalInfo.replace("'", "''"));
		logger.debug("executing sql \"{}\"", sql);
		try (final Connection connection = dataSource.getConnection();
				final Statement statement = connection.createStatement()) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			return 1 == statement.executeUpdate(sql);
		}
	}

	public synchronized boolean updateFailed(ToProcessBean request, String additionalInfo) throws SQLException {
		final String sql = configuration
				.getProperty("goal_identifier.sql.update_failed")
				.replace("{id}", Long.toString(request.id))
				.replace("{additionalInfo}", additionalInfo.replace("'", "''"));
		logger.debug("executing sql \"{}\"", sql);
		try (final Connection connection = dataSource.getConnection();
				final Statement statement = connection.createStatement()) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			return 1 == statement.executeUpdate(sql);
		}
	}
	
}
