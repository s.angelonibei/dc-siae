package com.alkemytech.sophia.goal.royalty;

import java.math.BigDecimal;
import java.math.BigInteger;

import com.alkemytech.sophia.goal.pojo.DemRoyalty;
import com.alkemytech.sophia.goal.pojo.DrmRoyalty;
import com.alkemytech.sophia.goal.pojo.Royalty;
import com.alkemytech.sophia.goal.pojo.RoyaltyScheme;
import com.sleepycat.bind.tuple.TupleBinding;
import com.sleepycat.bind.tuple.TupleInput;
import com.sleepycat.bind.tuple.TupleOutput;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class BdbRoyaltySchemeBinding extends TupleBinding<RoyaltyScheme> {
	
	private static final int BIT_DEM					= 1<<0;
	private static final int BIT_DEM_PUBLIC_DOMAIN		= 1<<1;
	private static final int BIT_DEM_SIAE				= 1<<2;
	private static final int BIT_DEM_NOT_ASSOCIATED		= 1<<3;
	private static final int BIT_DEM_SPECIAL_ACCOUNT	= 1<<9;
	private static final int BIT_DEM_EX_ART18_EXCLUDED	= 1<<4;

	private static final int BIT_DRM					= 1<<5;
	private static final int BIT_DRM_PUBLIC_DOMAIN		= 1<<6;
	private static final int BIT_DRM_SIAE				= 1<<7;
	private static final int BIT_DRM_NOT_ASSOCIATED		= 1<<8;
	private static final int BIT_DRM_SPECIAL_ACCOUNT	= 1<<10;
	
	private Integer readPositiveInteger(TupleInput in) {
		final int value = in.readInt();
		return -1 == value ? null : value;
	}
	
	private void writePositiveInteger(TupleOutput out, Integer integer) {
		out.writeInt(null == integer ? -1 : integer.intValue());
	}
	
	private BigDecimal readBigDecimal(TupleInput in) {
		final int length = in.readInt();
		if (-1 == length)
			return null;
		final byte[] array = new byte[length];
		in.read(array);
		return new BigDecimal(new BigInteger(array), in.readInt());
	}
	
	private void writeBigDecimal(TupleOutput out, BigDecimal decimal) {
		if (null == decimal) {
			out.writeInt(-1);
		} else {
			final byte[] array = decimal.stripTrailingZeros()
					.unscaledValue().toByteArray();
			out.writeInt(array.length);
			out.write(array);
			out.writeInt(decimal.scale());
		}
	}

	@Override
	public RoyaltyScheme entryToObject(TupleInput in) {
		final String code = in.readString();
		final RoyaltyScheme scheme = new RoyaltyScheme(code);
		int size = in.readInt();
		for (; size > 0; size --) {
			final String ipiCode = in.readString();
			final String siaePosition = in.readString();
			final String role = in.readString();
			final String accountCode = in.readString();
			final String accountId = in.readString();
			final String society = in.readString();			
			final Integer irregularityType = readPositiveInteger(in);
			final String angloAmericanRepertoire = in.readString();
			final int mask = in.readShort();
			if (BIT_DEM == (mask & BIT_DEM)) {
				final BigDecimal denominator = readBigDecimal(in);
				final BigDecimal numerator = readBigDecimal(in);
				final boolean publicDomain = 0 != (mask & BIT_DEM_PUBLIC_DOMAIN);
				final boolean siae = 0 != (mask & BIT_DEM_SIAE);
				final boolean notAssociated = 0 != (mask & BIT_DEM_NOT_ASSOCIATED);
				final boolean specialAccount = 0 != (mask & BIT_DEM_SPECIAL_ACCOUNT);
				final boolean exArt18Excluded = 0 != (mask & BIT_DEM_EX_ART18_EXCLUDED);
				scheme.add(new DemRoyalty(ipiCode, siaePosition, role, accountCode, accountId, society,
						irregularityType, notAssociated, specialAccount, publicDomain, siae,
						angloAmericanRepertoire, denominator, numerator, exArt18Excluded));
			} else if (BIT_DRM == (mask & BIT_DRM)) {
				final BigDecimal percentage = readBigDecimal(in);
				final boolean publicDomain = 0 != (mask & BIT_DRM_PUBLIC_DOMAIN);
				final boolean siae = 0 != (mask & BIT_DRM_SIAE);
				final boolean notAssociated = 0 != (mask & BIT_DRM_NOT_ASSOCIATED);
				final boolean specialAccount = 0 != (mask & BIT_DRM_SPECIAL_ACCOUNT);
				scheme.add(new DrmRoyalty(ipiCode, siaePosition, role, accountCode, accountId, society,
						irregularityType, notAssociated, specialAccount, publicDomain, siae,
						angloAmericanRepertoire, percentage));
			}
		}
		scheme.publicDomain = in.readBoolean();
		scheme.demSum = readBigDecimal(in);
		scheme.drmSum = readBigDecimal(in);
		return scheme;
	}

	@Override
	public void objectToEntry(RoyaltyScheme scheme, TupleOutput out) {
		out.writeString(scheme.code);
		out.writeInt(scheme.size());
		for (Royalty royalty : scheme) {
			out.writeString(royalty.ipiCode);
			out.writeString(royalty.siaePosition);
			out.writeString(royalty.role);
			out.writeString(royalty.accountCode);
			out.writeString(royalty.accountId);
			out.writeString(royalty.society);
			writePositiveInteger(out, royalty.irregularityType);
			out.writeString(royalty.angloAmericanRepertoire);
			if (royalty.isDem()) {
				final DemRoyalty dem = (DemRoyalty) royalty;
				int mask = BIT_DEM;
				if (royalty.publicDomain)
					mask |= BIT_DEM_PUBLIC_DOMAIN;
				if (royalty.siae)
					mask |= BIT_DEM_SIAE;
				if (royalty.notAssociated)
					mask |= BIT_DEM_NOT_ASSOCIATED;
				if (royalty.specialAccount)
					mask |= BIT_DEM_SPECIAL_ACCOUNT;
				if (dem.exArt18Excluded)
					mask |= BIT_DEM_EX_ART18_EXCLUDED;
				out.writeShort(mask);
				writeBigDecimal(out, dem.denominator);
				writeBigDecimal(out, dem.numerator);
			} else if (royalty.isDrm()) {
				final DrmRoyalty drm = (DrmRoyalty) royalty;
				int mask = BIT_DRM;
				if (royalty.publicDomain)
					mask |= BIT_DRM_PUBLIC_DOMAIN;
				if (royalty.siae)
					mask |= BIT_DRM_SIAE;
				if (royalty.notAssociated)
					mask |= BIT_DRM_NOT_ASSOCIATED;
				if (royalty.specialAccount)
					mask |= BIT_DRM_SPECIAL_ACCOUNT;
				out.writeShort(mask);
				writeBigDecimal(out, drm.percentage);
			}
		}
		out.writeBoolean(scheme.publicDomain);
		writeBigDecimal(out, scheme.demSum);
		writeBigDecimal(out, scheme.drmSum);
	}

}