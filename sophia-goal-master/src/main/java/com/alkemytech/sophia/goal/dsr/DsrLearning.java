package com.alkemytech.sophia.goal.dsr;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.zip.GZIPInputStream;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.index.ReverseIndex;
import com.alkemytech.sophia.commons.index.ReverseIndexProvider;
import com.alkemytech.sophia.commons.query.TitleArtistQuery;
import com.alkemytech.sophia.commons.query.TitleArtistQueryProvider;
import com.alkemytech.sophia.commons.score.TextScore;
import com.alkemytech.sophia.commons.text.TextNormalizer;
import com.alkemytech.sophia.commons.text.TextNormalizerProvider;
import com.alkemytech.sophia.commons.text.TextTokenizer;
import com.alkemytech.sophia.commons.text.TextTokenizerProvider;
import com.alkemytech.sophia.commons.util.SplitCharSequence;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.goal.document.DocumentScore;
import com.alkemytech.sophia.goal.document.DocumentScoreProvider;
import com.alkemytech.sophia.goal.document.DocumentSearch;
import com.alkemytech.sophia.goal.document.DocumentStore;
import com.alkemytech.sophia.goal.document.DocumentStoreProvider;
import com.alkemytech.sophia.goal.document.ScoredDocument;
import com.alkemytech.sophia.goal.model.Artist;
import com.alkemytech.sophia.goal.model.ArtistRole;
import com.alkemytech.sophia.goal.model.Code;
import com.alkemytech.sophia.goal.model.CodeType;
import com.alkemytech.sophia.goal.model.Origin;
import com.alkemytech.sophia.goal.model.Title;
import com.alkemytech.sophia.goal.model.TitleType;
import com.alkemytech.sophia.goal.model.Work;
import com.alkemytech.sophia.goal.work.WorkDatabase;
import com.alkemytech.sophia.goal.work.WorkDatabaseProvider;
import com.google.common.base.Strings;
import com.google.common.util.concurrent.Striped;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class DsrLearning {
	
	private static final Logger logger = LoggerFactory.getLogger(DsrLearning.class);
	
	private static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.toInstance(new S3(configuration));
			// normalizer(s)
			bind(TextNormalizer.class)
				.annotatedWith(Names.named("iswc_normalizer"))
				.toProvider(new TextNormalizerProvider(configuration, "iswc_normalizer"));
			bind(TextNormalizer.class)
				.annotatedWith(Names.named("isrc_normalizer"))
				.toProvider(new TextNormalizerProvider(configuration, "isrc_normalizer"));
			bind(TextNormalizer.class)
				.annotatedWith(Names.named("title_normalizer"))
				.toProvider(new TextNormalizerProvider(configuration, "title_normalizer"));
			bind(TextNormalizer.class)
				.annotatedWith(Names.named("artist_normalizer"))
				.toProvider(new TextNormalizerProvider(configuration, "artist_normalizer"));
			// tokenizer(s)
			bind(TextTokenizer.class)
				.annotatedWith(Names.named("title_tokenizer"))
				.toProvider(new TextTokenizerProvider(configuration, "title_tokenizer"));
			bind(TextTokenizer.class)
				.annotatedWith(Names.named("artist_tokenizer"))
				.toProvider(new TextTokenizerProvider(configuration, "artist_tokenizer"));
			// title artist quer(y/ies)
			bind(TitleArtistQuery.class)
				.toProvider(new TitleArtistQueryProvider(configuration, 
						configuration.getProperty("dsr_learning.title_artist_query")));
			// document score(s)
			bind(DocumentScore.class)
				.toProvider(new DocumentScoreProvider(configuration,
						configuration.getProperty("dsr_learning.document_score")));
			// reverse index(es)
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("iswc_index"))
				.toProvider(new ReverseIndexProvider(configuration, "iswc_index"))
				.asEagerSingleton();
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("isrc_index"))
				.toProvider(new ReverseIndexProvider(configuration, "isrc_index"))
				.asEagerSingleton();
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("title_index"))
				.toProvider(new ReverseIndexProvider(configuration, "title_index"))
				.asEagerSingleton();
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("artist_index"))
				.toProvider(new ReverseIndexProvider(configuration, "artist_index"))
				.asEagerSingleton();
			// work database
			bind(WorkDatabase.class)
				.toProvider(new WorkDatabaseProvider(configuration, "work_database"))
				.asEagerSingleton();			
			// document store
			bind(DocumentStore.class)
				.toProvider(new DocumentStoreProvider(configuration, "document_store"))
				.asEagerSingleton();
			// this
			bind(DsrLearning.class)
				.asEagerSingleton();
		}

	}
	
	public static void main(String[] args) {
		try {
			final DsrLearning instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/dsr-learning.properties"))
					.getInstance(DsrLearning.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Properties configuration;
	private final WorkDatabase workDatabase;
	private final DocumentStore documentStore;
	private final S3 s3;
	private final ReverseIndex iswcIndex;
	private final ReverseIndex isrcIndex;
	private final ReverseIndex titleIndex;
	private final ReverseIndex artistIndex;
	private final Provider<TextNormalizer> iswcNormalizerProvider;
	private final Provider<TextNormalizer> isrcNormalizerProvider;
	private final Provider<TextNormalizer> titleNormalizerProvider;
	private final Provider<TextNormalizer> artistNormalizerProvider;
	private final Provider<DocumentSearch> documentSearchProvider;
	private final boolean debug;

	@Inject
	protected DsrLearning(@Named("configuration") Properties configuration,
			S3 s3, WorkDatabase workDatabase,
			DocumentStore documentStore,
			@Named("iswc_index") ReverseIndex iswcIndex,
			@Named("isrc_index") ReverseIndex isrcIndex,
			@Named("title_index") ReverseIndex titleIndex,
			@Named("artist_index") ReverseIndex artistIndex,
			@Named("iswc_normalizer") Provider<TextNormalizer> iswcNormalizerProvider,
			@Named("isrc_normalizer") Provider<TextNormalizer> isrcNormalizerProvider,
			@Named("title_normalizer") Provider<TextNormalizer> titleNormalizerProvider,
			@Named("artist_normalizer") Provider<TextNormalizer> artistNormalizerProvider,
			Provider<DocumentSearch> documentSearchProvider) throws IOException {
		super();
		this.configuration = configuration;
		this.s3 = s3;
		this.workDatabase = workDatabase;
		this.documentStore = documentStore;
		this.iswcIndex = iswcIndex;
		this.isrcIndex = isrcIndex;
		this.titleIndex = titleIndex;
		this.artistIndex = artistIndex;
		this.iswcNormalizerProvider = iswcNormalizerProvider;
		this.isrcNormalizerProvider = isrcNormalizerProvider;
		this.titleNormalizerProvider = titleNormalizerProvider;
		this.artistNormalizerProvider = artistNormalizerProvider;
		this.documentSearchProvider = documentSearchProvider;
		this.debug = "true".equalsIgnoreCase(configuration
				.getProperty("dsr_learning.debug", configuration
						.getProperty("default.debug")));
	}
	
	public DsrLearning startup() throws IOException {
		if ("true".equalsIgnoreCase(configuration.getProperty("dsr_learning.locked", "true"))) {
			throw new IllegalStateException("application locked");
		}
		s3.startup();
		workDatabase.startup();
		documentStore.startup();
		return this;
	}

	public DsrLearning shutdown() {
		documentStore.shutdown();
		workDatabase.shutdown();
		s3.shutdown();
		return this;
	}
	
	public DsrLearning process(String[] args) throws IOException, SQLException, InterruptedException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		
		final int bindPort = Integer.parseInt(configuration
				.getProperty("dsr_learning.bind_port", configuration
						.getProperty("default.bind_port", "0")));

		// bind lock tcp port
		try (final ServerSocket socket = new ServerSocket(bindPort)) {
			logger.debug("socket bound to {}", socket.getLocalSocketAddress());
			
			process();
			
		}
		
		return this;
	}
	
	private int decodeRole(String text) {
		if ("Adapter".equals(text)) {
			return ArtistRole.arranger;
		} else if ("Arranger".equals(text)) {
			return ArtistRole.arranger;
		} else if ("AssociatedPerformer".equals(text)) {
			return ArtistRole.performer;
		} else if ("Author".equals(text)) {
			return ArtistRole.author;
		} else if ("Composer".equals(text)) {
			return ArtistRole.composer;
		} else if ("ComposerAuthor".equals(text)) {
			return ArtistRole.author;
		} else if ("MainArtist".equals(text)) {
			return ArtistRole.performer;
		} else if ("Performer".equals(text)) {
			return ArtistRole.performer;
		}
		return ArtistRole.undefined;
	}
	
	private void process() throws IOException, SQLException, InterruptedException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		
		final int threadCount = Integer.parseInt(configuration
				.getProperty("dsr_learning.thread_count", "1"));

		final File inputFile = new File(configuration
				.getProperty("dsr_learning.input_file"));

		final char delimiter = configuration
				.getProperty("dsr_learning.delimiter", ";").trim().charAt(0);
		final String splitRegex = configuration
				.getProperty("dsr_learning.split_regex", "\\|").trim();
		final int columnTitle = Integer.parseInt(configuration
				.getProperty("dsr_learning.column.title"));
		final int columnArtists = Integer.parseInt(configuration
				.getProperty("dsr_learning.column.artists"));
		final int columnRoles = Integer.parseInt(configuration
				.getProperty("dsr_learning.column.roles"));
		final int columnIswc = Integer.parseInt(configuration
				.getProperty("dsr_learning.column.iswc"));
		final int columnIsrc = Integer.parseInt(configuration
				.getProperty("dsr_learning.column.isrc"));
		final int columnOriginalTitle = Integer.parseInt(configuration
				.getProperty("dsr_learning.column.original_title"));
		final int columnOriginalArtists = Integer.parseInt(configuration
				.getProperty("dsr_learning.column.original_artists"));
		final int columnOriginalRoles = Integer.parseInt(configuration
				.getProperty("dsr_learning.column.original_roles"));
		final int columnOriginalIswc = Integer.parseInt(configuration
				.getProperty("dsr_learning.column.original_iswc"));
		final int columnOriginalIsrc = Integer.parseInt(configuration
				.getProperty("dsr_learning.column.original_isrc"));
		
		final double iswcMinScore = Double.parseDouble(configuration
				.getProperty("dsr_learning.min_score.iswc"));
		final double isrcMinScore = Double.parseDouble(configuration
				.getProperty("dsr_learning.min_score.isrc"));
		final double titleArtistMinScore = Double.parseDouble(configuration
				.getProperty("dsr_learning.min_score.title_artist"));
		final double autoScore = Double.parseDouble(configuration
				.getProperty("dsr_learning.auto_score", "0.999"));
		final int maxPostings = Integer.parseInt(configuration
				.getProperty("dsr_learning.max_postings"));
		
		final TextScore sameTitleScore = (TextScore) Class
				.forName(configuration.getProperty("dsr_learning.same_title.score_class"))
						.newInstance();
		final double sameTitleMinScore = Double.parseDouble(configuration
				.getProperty("dsr_learning.same_title.min_score"));
		final TextScore sameArtistScore = (TextScore) Class
				.forName(configuration.getProperty("dsr_learning.same_artist.score_class"))
						.newInstance();
		final double sameArtistMinScore = Double.parseDouble(configuration
				.getProperty("dsr_learning.same_artist.min_score"));
		final boolean saveModifiedWorks = "true".equalsIgnoreCase(configuration
				.getProperty("dsr_learning.save_modified_works"));
		
		final int origin = Origin.valueOf(configuration
				.getProperty("dsr_learning.origin"));
		
		final long statsFrequency = TextUtils.parseLongDuration(configuration
				.getProperty("dsr_learning.stats_frequency"));

		// stats
		final AtomicLong totalRecords = new AtomicLong(0L);
		final AtomicLong identifiedRecords = new AtomicLong(0L);
		final AtomicLong identifiedIswc = new AtomicLong(0L);
		final AtomicLong identifiedOriginalIswc = new AtomicLong(0L);
		final AtomicLong identifiedIsrc = new AtomicLong(0L);
		final AtomicLong identifiedOriginalIsrc = new AtomicLong(0L);
		final AtomicLong identifiedTitleArtist = new AtomicLong(0L);
		final AtomicLong identifiedOriginalTitleArtist = new AtomicLong(0L);
		final AtomicLong learnedIswc = new AtomicLong(0L);
		final AtomicLong learnedOriginalIswc = new AtomicLong(0L);
		final AtomicLong learnedIsrc = new AtomicLong(0L);
		final AtomicLong learnedOriginalIsrc = new AtomicLong(0L);
		final AtomicLong learnedTitle = new AtomicLong(0L);
		final AtomicLong learnedOriginalTitle = new AtomicLong(0L);
		final AtomicLong learnedArtist = new AtomicLong(0L);
		final AtomicLong learnedOriginalArtist = new AtomicLong(0L);

		logger.debug("processing input file {}", inputFile);
		final long startTimeMillis = System.currentTimeMillis();
		long elapsedTimeMillis = 0L;
		
		try (final FileInputStream inputStream = new FileInputStream(inputFile);
				final GZIPInputStream gzipInputStream = new GZIPInputStream(inputStream);
				final InputStreamReader inputStreamReader = new InputStreamReader(gzipInputStream, Charset.forName("UTF-8"));
				final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				final CSVParser csvParser = new CSVParser(bufferedReader, CSVFormat.newFormat(delimiter)
						.withQuoteMode(QuoteMode.MINIMAL)
						.withQuote('"')
						.withIgnoreEmptyLines()
						.withIgnoreSurroundingSpaces())) {				
			
			final Iterator<CSVRecord> csvIterator = csvParser.iterator();
			final Object monitor = new Object();
			final ExecutorService executorService = Executors.newCachedThreadPool();
			final AtomicInteger runningThreads = new AtomicInteger(0);
			final Striped<Lock> locks = Striped.lock(threadCount);
			while (runningThreads.get() < threadCount) {
				logger.debug("starting thread {}", runningThreads.incrementAndGet());
				executorService.execute(new Runnable() {
										
					@Override
					public void run() {
						try {
							
							final TextNormalizer iswcNormalizer = iswcNormalizerProvider.get();
							final TextNormalizer isrcNormalizer = isrcNormalizerProvider.get();
							final TextNormalizer titleNormalizer = titleNormalizerProvider.get();
							final TextNormalizer artistNormalizer = artistNormalizerProvider.get();
							final DocumentSearch documentSearch = documentSearchProvider.get();
							
							while (true) {

								final CSVRecord record;
								synchronized (csvIterator) {
									if (csvIterator.hasNext()) {
										record = csvIterator.next();
									} else {
										return;
									}
								}
							
								// parse record
								final String title = record.get(columnTitle);
								final String artists = record.get(columnArtists);
								final String[] roles = record.get(columnRoles)
										.split(splitRegex);
								final String iswc = record.get(columnIswc);
								final String isrc = record.get(columnIsrc);
								final String originalTitle = record.get(columnOriginalTitle);
								final String originalArtists = record.get(columnOriginalArtists);
								final String[] originalRoles = record.get(columnOriginalRoles)
										.split(splitRegex);
								final String originalIswc = record.get(columnOriginalIswc);
								final String originalIsrc = record.get(columnOriginalIsrc);

								// normalized iswc
								String normalizedIswc = iswcNormalizer.normalize(iswc);
								// normalized original_iswc
								String normalizedOriginalIswc = iswcNormalizer.normalize(originalIswc);
								// normalized isrc
								String normalizedIsrc = isrcNormalizer.normalize(isrc);
								// normalized original_isrc
								String normalizedOriginalIsrc = iswcNormalizer.normalize(originalIsrc);
								// normalized title
								String text = titleNormalizer.normalize(title);
								final SplitCharSequence normalizedTitle = Strings
										.isNullOrEmpty(text) ? null : new SplitCharSequence(text);
								// normalized artists
								final HashSet<SplitCharSequence> normalizedArtists = new HashSet<>();
								for (String artist : artists.split(splitRegex)) {
									artist = artistNormalizer.normalize(artist);
									if (!Strings.isNullOrEmpty(artist)) {
										normalizedArtists.add(new SplitCharSequence(artist));
									}
								}
								// normalized original_title
								text = titleNormalizer.normalize(originalTitle);
								final SplitCharSequence normalizedOriginalTitle = Strings
										.isNullOrEmpty(text) ? null : new SplitCharSequence(text);
								// normalized original_artists
								final HashSet<SplitCharSequence> normalizedOriginalArtists = new HashSet<>();
								for (String artist : originalArtists.split(splitRegex)) {
									artist = artistNormalizer.normalize(artist);
									if (!Strings.isNullOrEmpty(artist)) {
										normalizedOriginalArtists.add(new SplitCharSequence(artist));
									}
								}
								
								ScoredDocument document = null;
								IdentificationType source = null;

								// iswc
								if (null == document) {
									if (!Strings.isNullOrEmpty(normalizedIswc)) {
										document = documentSearch.search(iswcIndex,
												normalizedTitle, normalizedArtists, normalizedIswc, maxPostings, autoScore);
										if (null != document && document.score >= iswcMinScore) {
											source = IdentificationType.iswc;
											identifiedIswc.incrementAndGet();
										}
									}
								}

								// original_iswc
								if (null == document) {
									if (!Strings.isNullOrEmpty(normalizedOriginalIswc)) {
										document = documentSearch.search(iswcIndex,
												normalizedTitle, normalizedArtists, normalizedOriginalIswc, maxPostings, autoScore);
										if (null != document && document.score >= iswcMinScore) {
											source = IdentificationType.original_iswc;
											identifiedOriginalIswc.incrementAndGet();
										}
									}
								}

								// isrc
								if (null == document) {
									if (!Strings.isNullOrEmpty(normalizedIsrc)) {
										document = documentSearch.search(isrcIndex,
												normalizedTitle, normalizedArtists, normalizedIsrc, maxPostings, autoScore);
										if (null != document && document.score >= isrcMinScore) {
											source = IdentificationType.isrc;
											identifiedIsrc.incrementAndGet();
										}
									}
								}
								
								// original_isrc
								if (null == document) {
									if (!Strings.isNullOrEmpty(normalizedOriginalIsrc)) {
										document = documentSearch.search(isrcIndex,
												normalizedTitle, normalizedArtists, normalizedOriginalIsrc, maxPostings, autoScore);
										if (null != document && document.score >= isrcMinScore) {
											source = IdentificationType.original_isrc;
											identifiedOriginalIsrc.incrementAndGet();
										}
									}
								}
								
								// title
								if (null == document) {
									if (null != normalizedTitle && !normalizedArtists.isEmpty()) {
										document = documentSearch.search(titleIndex, artistIndex,
												normalizedTitle, normalizedArtists, maxPostings, autoScore);
										if (null != document && document.score >= titleArtistMinScore) {
											source = IdentificationType.title_artists;
											identifiedTitleArtist.incrementAndGet();
										}
									}
								}
								
								// original_title
								if (null == document) {
									if (null != normalizedOriginalTitle && !normalizedOriginalArtists.isEmpty()) {
										document = documentSearch.search(titleIndex, artistIndex,
												normalizedOriginalTitle, normalizedOriginalArtists, maxPostings, autoScore);
										if (null != document && document.score >= titleArtistMinScore) {
											source = IdentificationType.original_title_artists;
											identifiedOriginalTitleArtist.incrementAndGet();
										}
									}
								}

								if (null != document) {
									
									identifiedRecords.incrementAndGet();
									
									// lock work id to avoid concurrent inconsistent updates
									final Lock lock = locks.get(document.document.workId);
									lock.lock();
									try {
										
										final Work work = workDatabase.getById(document.document.workId);
										
										boolean modified = false;

										// iswc
										switch (source) {
										case isrc:
										case original_isrc:
										case original_iswc:
										case original_title_artists:
										case title_artists:
											if (!Strings.isNullOrEmpty(normalizedIswc)) {
												if (work.add(new Code(normalizedIswc, CodeType.iswc, origin))) {
													learnedIswc.incrementAndGet();
													modified = true;
													if (debug) logger.debug("adding iswc \"{}\" to work {}", iswc, work);
												}
											}
											break;
										default:
											break;
										}
										
										// original_iswc
										switch (source) {
										case isrc:
										case iswc:
										case original_isrc:
										case original_title_artists:
										case title_artists:
											if (!Strings.isNullOrEmpty(normalizedOriginalIswc)) {
												if (work.add(new Code(normalizedOriginalIswc, CodeType.iswc, origin))) {
													learnedOriginalIswc.incrementAndGet();
													modified = true;
													if (debug) logger.debug("adding original_iswc \"{}\" to work {}", originalIswc, work);
												}
											}
											break;
										default:
											break;
										}

										// isrc
										switch (source) {
										case iswc:
										case original_isrc:
										case original_iswc:
										case original_title_artists:
										case title_artists:
											if (!Strings.isNullOrEmpty(normalizedIsrc)) {
												if (work.add(new Code(normalizedIsrc, CodeType.isrc, origin))) {
													learnedIsrc.incrementAndGet();
													modified = true;
													if (debug) logger.debug("adding isrc \"{}\" to work {}", isrc, work);
												}
											}
											break;
										default:
											break;
										}

										// original_isrc
										switch (source) {
										case isrc:
										case iswc:
										case original_iswc:
										case original_title_artists:
										case title_artists:
											if (!Strings.isNullOrEmpty(normalizedOriginalIsrc)) {
												if (work.add(new Code(normalizedOriginalIsrc, CodeType.isrc, origin))) {
													learnedOriginalIsrc.incrementAndGet();
													modified = true;
													if (debug) logger.debug("adding original_isrc \"{}\" to work {}", originalIsrc, work);
												}
											}
											break;
										default:
											break;
										}

										// title
										switch (source) {
										case isrc:
										case iswc:
										case original_isrc:
										case original_iswc:
										case original_title_artists: {
											if (null != normalizedTitle) {
												boolean found = false;
												for (Title workTitle : work.titles) {
													final String normalizedWorkTitle = titleNormalizer.normalize(workTitle.text);
													if (!Strings.isNullOrEmpty(normalizedWorkTitle)) {
														if (sameTitleScore.getScore(normalizedTitle,
																new SplitCharSequence(normalizedWorkTitle)) > sameTitleMinScore) {
															found = true;
															break;
														}
													}
												}
												if (!found) {
													if (work.add(new Title(title, TitleType.alternative, origin))) {
														learnedTitle.incrementAndGet();
														modified = true;
														if (debug) logger.debug("adding title \"{}\" to work {}", title, work);
													}
												}												
												break;
											}										
										}
										default:
											break;
										}
										
										// original_title
										switch (source) {
										case isrc:
										case iswc:
										case original_isrc:
										case original_iswc:
										case title_artists: {
											if (null != normalizedOriginalTitle) {
												boolean found = false;
												for (Title workTitle : work.titles) {
													final String normalizedWorkTitle = titleNormalizer.normalize(workTitle.text);
													if (!Strings.isNullOrEmpty(normalizedWorkTitle)) {
														if (sameTitleScore.getScore(normalizedOriginalTitle,
																new SplitCharSequence(normalizedWorkTitle)) > sameTitleMinScore) {
															found = true;
															break;
														}
													}
												}
												if (!found) {
													if (work.add(new Title(originalTitle, TitleType.alternative, origin))) {
														learnedOriginalTitle.incrementAndGet();
														modified = true;
														if (debug) logger.debug("adding original_title \"{}\" to work {}", originalTitle, work);
													}
												}												
												break;
											}
										}
										default:
											break;
										}

										// normalized work artists
										final List<SplitCharSequence> normalizedWorkArtists = new ArrayList<>(work.artists.size());
										for (Artist workArtist : work.artists) {
											final String normalizedWorkArtist = artistNormalizer.normalize(workArtist.text);
											if (!Strings.isNullOrEmpty(normalizedWorkArtist)) {
												normalizedWorkArtists.add(new SplitCharSequence(normalizedWorkArtist));
											}
										}

										// artists
										if (!Strings.isNullOrEmpty(artists)) {
											int index = 0;
											for (String artist : artists.split(splitRegex)) {
												final String normalizedArtist = artistNormalizer.normalize(artist);
												if (!Strings.isNullOrEmpty(normalizedArtist)) {
													boolean found = false;
													for (SplitCharSequence normalizedWorkArtist : normalizedWorkArtists) {
														if (sameArtistScore.getScore(new SplitCharSequence(normalizedArtist),
																normalizedWorkArtist) > sameArtistMinScore) {
															found = true;
															break;
														}
													}
													if (!found) {
														final int role = decodeRole(roles[index]);
														if (ArtistRole.undefined != role) {
															if (work.add(new Artist(artist, role, origin))) {
																normalizedWorkArtists.add(new SplitCharSequence(normalizedArtist));
																learnedArtist.incrementAndGet();
																modified = true;
																if (debug) logger.debug("adding artist \"{}\" to work {}", artist, work);
															}
														}
													}												
												}
												index ++;
											}
										}
										
										// original_artists
										if (!Strings.isNullOrEmpty(originalArtists)) {
											int index = 0;
											for (String originalArtist : originalArtists.split(splitRegex)) {
												final String normalizedOriginalArtist = artistNormalizer.normalize(originalArtist);
												if (!Strings.isNullOrEmpty(normalizedOriginalArtist)) {
													boolean found = false;
													for (SplitCharSequence normalizedWorkArtist : normalizedWorkArtists) {
														if (sameArtistScore.getScore(new SplitCharSequence(normalizedOriginalArtist),
																normalizedWorkArtist) > sameArtistMinScore) {
															found = true;
															break;
														}
													}
													if (!found) {
														final int originalRole = decodeRole(originalRoles[index]);
														if (ArtistRole.undefined != originalRole) {
															if (work.add(new Artist(originalArtist, originalRole, origin))) {
																normalizedWorkArtists.add(new SplitCharSequence(normalizedOriginalArtist));
																learnedOriginalArtist.incrementAndGet();
																modified = true;
																if (debug) logger.debug("adding original_artist \"{}\" to work {}", originalArtist, work);
															}
														}
													}												
												}
												index ++;
											}
										}
										
										if (modified && saveModifiedWorks) {
											workDatabase.upsert(work);
										}	
										
									} finally {
										lock.unlock();
									}
																		
								}

								totalRecords.incrementAndGet();
							
							}
							
						} catch (IOException e) {
							logger.error("run", e);
						} finally {
							logger.debug("thread {} finished", runningThreads.getAndDecrement());
							synchronized (monitor) {
								monitor.notify();
							}
						}
					}

				});
			}
			
			while (runningThreads.get() > 0) {
				synchronized (monitor) {
					monitor.wait(statsFrequency);
				}
				
				elapsedTimeMillis = System.currentTimeMillis() - startTimeMillis;
				// print stats
				logger.debug("---------- stats ----------");
				logger.debug("processing time {}", 
						TextUtils.formatDuration(elapsedTimeMillis));
				logger.debug("average line processing time {}ms", 
						Math.round((double) elapsedTimeMillis / (double) totalRecords.get()));
				logger.debug("processed {}", totalRecords);
				logger.debug("  unidentified {} ({}%)", totalRecords.get() - identifiedRecords.get(),
						Math.round(100.0 * (totalRecords.get() - identifiedRecords.get()) / (double) totalRecords.get()));
				logger.debug("  identified {} ({}%)", identifiedRecords,
						Math.round(100.0 * identifiedRecords.get() / (double) totalRecords.get()));
				logger.debug("    iswc {} ({}%)", identifiedIswc,
						Math.round(100.0 * identifiedIswc.get() / (double) totalRecords.get()));
				logger.debug("    original_iswc {} ({}%)", identifiedOriginalIswc,
						Math.round(100.0 * identifiedOriginalIswc.get() / (double) totalRecords.get()));
				logger.debug("    isrc {} ({}%)", identifiedIsrc,
						Math.round(100.0 * identifiedIsrc.get() / (double) totalRecords.get()));
				logger.debug("    original_isrc {} ({}%)", identifiedOriginalIsrc,
						Math.round(100.0 * identifiedOriginalIsrc.get() / (double) totalRecords.get()));
				logger.debug("    title_artist {} ({}%)", identifiedTitleArtist,
						Math.round(100.0 * identifiedTitleArtist.get() / (double) totalRecords.get()));
				logger.debug("    original_title_artist {} ({}%)", identifiedOriginalTitleArtist,
						Math.round(100.0 * identifiedOriginalTitleArtist.get() / (double) totalRecords.get()));
				logger.debug("  learned");
				logger.debug("    iswc {}", learnedIswc);
				logger.debug("    original_iswc {}", learnedOriginalIswc);
				logger.debug("    isrc {}", learnedIsrc);
				logger.debug("    original_isrc {}", learnedOriginalIsrc);
				logger.debug("    title {}", learnedTitle);
				logger.debug("    original_title {}", learnedOriginalTitle);
				logger.debug("    artist {}", learnedArtist);
				logger.debug("    original_artist {}", learnedOriginalArtist);
			}
			
		}
		
		logger.debug("{} records processed", totalRecords);
		elapsedTimeMillis = System.currentTimeMillis() - startTimeMillis;
		logger.debug("learning completed in {}",
				TextUtils.formatDuration(elapsedTimeMillis));
	}
	
}