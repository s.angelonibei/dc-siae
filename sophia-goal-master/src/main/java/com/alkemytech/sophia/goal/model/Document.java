package com.alkemytech.sophia.goal.model;

import java.util.HashSet;
import java.util.Set;

import com.alkemytech.sophia.commons.nosql.NoSqlEntity;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;
import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@Entity
public class Document implements NoSqlEntity {
	
	public static interface Selector {
		public void select(Document document) throws Exception;
	}

	public static boolean isValid(Document document) {
		if (null == document) {
			return false;
		} else if (0L == document.id) {
			return false;
		} else if (0L == document.workId) {
			return false;
		}
		if (null == document.texts || document.texts.isEmpty()) {
			return false;
		} else {
			int titles = 0;
			int artists = 0;
			for (Text text : document.texts) {
				if (TextType.undefined == text.type) {
					return false;
				} else if (Strings.isNullOrEmpty(text.text)) {
					return false;
				} 
				if (TextType.title == text.type) {
					titles ++;
				} else if (TextType.artist == text.type) {
					artists ++;
				}
			}
			if (0 == titles || 0 == artists) {
				return false;
			}
		}
		return true;
	}
	
	@PrimaryKey
	public long id;
	public long workId;
	public Set<Text> texts;

	public Document() {
		super();
	}

	public Document(long id, long workId, Set<Text> texts) {
		super();
		this.id = id;
		this.workId = workId;
		this.texts = texts;
	}

	public boolean add(Text text) {
		if (null == texts)
			texts = new HashSet<>();
		return texts.add(text);
	}
	
	@Override
	public String getPrimaryKey() {
		return Long.toString(id);
	}

	@Override
	public String getSecondaryKey() {
		return null;
	}

	@Override
	public int hashCode() {
		int result = 31 + (int) (id ^ (id >>> 32));
		result = 31 * result + (null == texts ? 0 : texts.hashCode());
		return 31 * result + (int) (workId ^ (workId >>> 32));
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (null == obj) {
			return false;
		} else if (getClass() != obj.getClass()) {
			return false;
		}
		final Document other = (Document) obj;
		if (id != other.id) {
			return false;
		}
		if (null == texts) {
			if (null != other.texts) {
				return false;
			}
		} else if (!texts.equals(other.texts)) {
			return false;
		}
		if (workId != other.workId) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.disableHtmlEscaping()
				.create()
				.toJson(this);
	}
	
}
