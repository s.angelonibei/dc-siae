package com.alkemytech.sophia.goal.work;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.mmap.ObjectCodec;
import com.alkemytech.sophia.commons.nosql.ConcurrentNoSql;
import com.alkemytech.sophia.commons.nosql.NoSql;
import com.alkemytech.sophia.commons.nosql.NoSqlConfig;
import com.alkemytech.sophia.commons.nosql.NoSqlException;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.goal.model.Work;
import com.alkemytech.sophia.goal.model.Work.Selector;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class NoSqlWorkDatabase implements WorkDatabase {

	private static class ThreadLocalObjectCodec extends ThreadLocal<ObjectCodec<Work>> {

		private final Charset charset;
		
	    public ThreadLocalObjectCodec(Charset charset) {
			super();
			this.charset = charset;
		}

		@Override
	    protected synchronized ObjectCodec<Work> initialValue() {
	        return new WorkCodec(charset);
	    }
	    
	}

	private final Logger logger = LoggerFactory.getLogger(NoSqlWorkDatabase.class);
	
	private final AtomicInteger startupCalls;
	private final NoSqlConfig<Work> config;
	
	private NoSql<Work> database;
	
	public NoSqlWorkDatabase(Properties configuration, String propertyPrefix) {
		super();
		this.startupCalls = new AtomicInteger(0);
		this.config = new NoSqlConfig<Work>()
			.setHomeFolder(new File(configuration
					.getProperty(propertyPrefix + ".home_folder")))
			.setReadOnly("true".equalsIgnoreCase(configuration
					.getProperty(propertyPrefix + ".read_only", "true")))
			.setPageSize(TextUtils.parseIntSize(configuration
					.getProperty(propertyPrefix + ".page_size", "-1")))
			.setCacheSize(TextUtils.parseIntSize(configuration
					.getProperty(propertyPrefix + ".cache_size", "0")))
			.setCodec(new ThreadLocalObjectCodec(Charset.forName("UTF-8")));
	}

	@Override
	public NoSqlWorkDatabase startup() throws IOException {
		if (0 == startupCalls.getAndIncrement()) {
			database = new ConcurrentNoSql<>(config);
		}
		return this;
	}

	@Override
	public NoSqlWorkDatabase shutdown() {
		if (0 == startupCalls.decrementAndGet()) {
			try {
				database.close();
			} catch (IOException e) {
				logger.error("close", e);
			}
		}
		return this;
	}

	@Override
	public boolean isReadOnly() {
		return database.isReadOnly();
	}
	
	@Override
	public File getHomeFolder() {
		return database.getHomeFolder();
	}

	@Override
	public long getNextId() {
		return database.nextSequenceId();
	}
	
	@Override
	public Work getById(long id) {
		return database.getByPrimaryKey(Long.toString(id));
	}

	@Override
	public Work upsert(Work work) {
		return database.upsert(work);
	}

	@Override
	public boolean delete(long id) {
		return database.deleteByPrimaryKey(Long.toString(id));
	}
	
	@Override
	public NoSqlWorkDatabase select(final Selector selector) throws Exception {
		database.select(new NoSql.Selector<Work>() {
			@Override
			public void select(Work work) {
				try {
					selector.select(work);
				} catch (Exception e) {
					throw new NoSqlException(e);
				}
			}
		});
		return this;
	}

	@Override
	public NoSqlWorkDatabase truncate() {
		database.truncate();
		return this;
	}

	@Override
	public NoSqlWorkDatabase defrag() {
		database.defrag();
		return this;
	}
	
}
