package com.alkemytech.sophia.goal.collecting;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Comparator;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.mmap.ObjectCodec;
import com.alkemytech.sophia.commons.nosql.ConcurrentNoSql;
import com.alkemytech.sophia.commons.nosql.NoSql;
import com.alkemytech.sophia.commons.nosql.NoSqlConfig;
import com.alkemytech.sophia.commons.nosql.NoSqlException;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.goal.model.CollectingCode;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class NoSqlCollectingDatabase implements CollectingDatabase {

	private static class ThreadLocalObjectCodec extends ThreadLocal<ObjectCodec<CollectingCode>> {

		private final Charset charset;
		
	    public ThreadLocalObjectCodec(Charset charset) {
			super();
			this.charset = charset;
		}

		@Override
	    protected synchronized ObjectCodec<CollectingCode> initialValue() {
	        return new CollectingCodeCodec(charset);
	    }
	    
	}

	private final Logger logger = LoggerFactory.getLogger(NoSqlCollectingDatabase.class);
	
	private final AtomicInteger startupCalls;
	private final NoSqlConfig<CollectingCode> config;
	
	private NoSql<CollectingCode> database;
	
	public NoSqlCollectingDatabase(Properties configuration, String propertyPrefix) {
		super();
		this.startupCalls = new AtomicInteger(0);
		this.config = new NoSqlConfig<CollectingCode>()
			.setHomeFolder(new File(configuration
					.getProperty(propertyPrefix + ".home_folder")))
			.setReadOnly("true".equalsIgnoreCase(configuration
					.getProperty(propertyPrefix + ".read_only", "true")))
			.setPageSize(TextUtils.parseIntSize(configuration
					.getProperty(propertyPrefix + ".page_size", "-1")))
			.setCacheSize(TextUtils.parseIntSize(configuration
					.getProperty(propertyPrefix + ".cache_size", "0")))
			.setCodec(new ThreadLocalObjectCodec(Charset.forName("UTF-8")))
			.setSecondaryIndex(true);
	}

	@Override
	public NoSqlCollectingDatabase startup() throws IOException {
		if (0 == startupCalls.getAndIncrement()) {
			database = new ConcurrentNoSql<>(config);
		}
		return this;
	}

	@Override
	public NoSqlCollectingDatabase shutdown() {
		if (0 == startupCalls.decrementAndGet()) {
			try {
				database.close();
			} catch (IOException e) {
				logger.error("close", e);
			}
		}
		return this;
	}

	@Override
	public boolean isReadOnly() {
		return database.isReadOnly();
	}
	
	@Override
	public File getHomeFolder() {
		return database.getHomeFolder();
	}
	
	@Override
	public CollectingCode getByCode(String code) throws IOException {
		return database.getByPrimaryKey(code);
	}
	
	@Override
	public CollectingCode getByWorkId(long workId) throws IOException {
		return database.getBySecondaryKey(Long.toString(workId));
	}

	@Override
	public CollectingCode upsert(CollectingCode collectingCode) throws IOException {
		return database.upsert(collectingCode);
	}
	
	@Override
	public NoSqlCollectingDatabase selectOrderByWeight(final CollectingCode.Selector selector, boolean ascending) throws Exception {
		final Comparator<CollectingCode> comparator;
		if (ascending) {
			comparator = new Comparator<CollectingCode>() {
				@Override
				public int compare(CollectingCode o1, CollectingCode o2) {
					return o1.weight.compareTo(o2.weight);
				}
			};
		} else {
			comparator = new Comparator<CollectingCode>() {
				@Override
				public int compare(CollectingCode o1, CollectingCode o2) {
					return o2.weight.compareTo(o1.weight);
				}
			};			
		}
		database.select(new NoSql.Selector<CollectingCode>() {
			@Override
			public void select(CollectingCode collectingCode) {
				try {
					selector.select(collectingCode);
				} catch (Exception e) {
					throw new NoSqlException(e);
				}
			}
		}, comparator);
		return this;
	}

	@Override
	public NoSqlCollectingDatabase truncate() {
		database.truncate();
		return this;
	}
	
	@Override
	public NoSqlCollectingDatabase defrag() {
		database.defrag();
		return this;
	}
	
}
