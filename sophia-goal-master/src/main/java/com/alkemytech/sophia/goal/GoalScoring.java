package com.alkemytech.sophia.goal;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.http.HTTP;
import com.alkemytech.sophia.commons.util.BigDecimals;
import com.alkemytech.sophia.commons.util.FileUtils;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.goal.config.AngloAmericanConfig;
import com.alkemytech.sophia.goal.jdbc.McmdbDAO;
import com.alkemytech.sophia.goal.jdbc.McmdbDataSource;
import com.alkemytech.sophia.goal.pojo.AngloAmericanRule;
import com.alkemytech.sophia.goal.pojo.DemRoyalty;
import com.alkemytech.sophia.goal.pojo.DrmRoyalty;
import com.alkemytech.sophia.goal.pojo.RightSplit;
import com.alkemytech.sophia.goal.pojo.Royalty;
import com.alkemytech.sophia.goal.pojo.RoyaltyHolder;
import com.alkemytech.sophia.goal.pojo.RoyaltyScheme;
import com.alkemytech.sophia.goal.royalty.BdbRoyaltyDatabase;
import com.alkemytech.sophia.goal.royalty.NoSqlRoyaltyDatabase;
import com.alkemytech.sophia.goal.royalty.RoyaltyDatabase;
import com.alkemytech.sophia.goal.royalty.RoyaltyDatabaseMetadata;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.common.base.Strings;
import com.google.common.util.concurrent.Striped;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class GoalScoring {
	
	private static final Logger logger = LoggerFactory.getLogger(GoalScoring.class);

	private static final String DATE_FORMAT = "[0-9]{4}\\-[0-9]{2}\\-[0-9]{2}.*";
	private static final BigDecimal ONE_HUNDRED = new BigDecimal(100.0);
	private static final BigDecimal TEN_THOUSANDS = new BigDecimal(10000.0);

	private static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// http
			bind(HTTP.class)
				.toInstance(new HTTP(configuration));
			// amazon service(s)
			bind(S3.class)
				.toInstance(new S3(configuration));
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("MCMDB"))
				.to(McmdbDataSource.class)
				.asEagerSingleton();
			// data access object(s)
			bind(McmdbDAO.class)
				.asEagerSingleton();
			// this
			bind(GoalScoring.class)
				.asEagerSingleton();
		}
		
	}

	private static class Accumulators {
		
		private final Map<String, Long> integers;
		private final Map<String, BigDecimal> decimals;

		public Accumulators() {
			super();
			integers = new ConcurrentHashMap<>();
			decimals = new ConcurrentHashMap<>();
		}
		
		public boolean isEmpty() {
			return integers.isEmpty() && decimals.isEmpty();
		}
		
		public void add(String name, BigDecimal delta) {
			synchronized (decimals) {
				final BigDecimal value = decimals.get(name);
				if (null == value)
					decimals.put(name, delta);
				else
					decimals.put(name, value.add(delta));
			}
		}
		
		public void add(String name, long delta) {
			synchronized (integers) {
				final Long value = integers.get(name);
				if (null == value)
					integers.put(name, delta);
				else
					integers.put(name, value + delta);				
			}
		}
		
		public long getInteger(String name) {
			final Long value= integers.get(name);
			return null == value ? 0L : value;
		}

		public BigDecimal getDecimal(String name) {
			final BigDecimal value = decimals.get(name);
			return null == value ? BigDecimal.ZERO : value;
		}

		public Map<String, Number> getAll() {
			final Map<String, Number> result = new LinkedHashMap<>();
			final ArrayList<Map.Entry<String, Long>> sortedIntegers = new ArrayList<>(integers.entrySet());
			Collections.sort(sortedIntegers, new Comparator<Map.Entry<String, Long>>() {

				@Override
				public int compare(Entry<String, Long> o1, Entry<String, Long> o2) {
					return o1.getKey().compareTo(o2.getKey());
				}
				
			});
			for (Map.Entry<String, Long> entry : sortedIntegers) {
				result.put(entry.getKey(), entry.getValue());
			}
			final ArrayList<Map.Entry<String, BigDecimal>> sortedDecimals = new ArrayList<>(decimals.entrySet());
			Collections.sort(sortedDecimals, new Comparator<Map.Entry<String, BigDecimal>>() {

				@Override
				public int compare(Entry<String, BigDecimal> o1, Entry<String, BigDecimal> o2) {
					return o1.getKey().compareTo(o2.getKey());
				}
				
			});
			for (Map.Entry<String, BigDecimal> entry : sortedDecimals) {
				BigDecimal value = entry.getValue();
				value = BigDecimals.isAlmostZero(value) ?
						BigDecimal.ZERO : BigDecimals.setScale(value).stripTrailingZeros();
				result.put(entry.getKey(), value);
			}
			return result;
		}
		
	}

	public static void main(String[] args) {
		try {
			final GoalScoring instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/goal-scoring.properties"))
					.getInstance(GoalScoring.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Properties configuration;
	private final Charset charset;
	private final Gson gson;
	private final HTTP http;
	private final S3 s3;
	private final McmdbDAO dao;

	@Inject
	protected GoalScoring(@Named("configuration") Properties configuration,
			@Named("charset") Charset charset,
			Gson gson, HTTP http, S3 s3, McmdbDAO dao) throws IOException {
		super();
		this.configuration = configuration;
		this.charset = charset;
		this.gson = gson;
		this.http = http;
		this.s3 = s3;
		this.dao = dao;
	}
	
	public GoalScoring startup() throws IOException {
		if ("true".equalsIgnoreCase(configuration.getProperty("goal_scoring.locked", "true"))) {
			throw new IllegalStateException("application locked");
		}
		http.startup();
		s3.startup();
		return this;
	}

	public GoalScoring shutdown() throws IOException {
		s3.shutdown();
		http.shutdown();
		return this;
	}
	
	private String escapeCsvValue(String value, char delimiter) {
		if (Strings.isNullOrEmpty(value))
			return "";
		if (-1 != value.indexOf(delimiter))
			return new StringBuilder()
					.append('"')
					.append(value.replace("\"", "\"\""))
					.append('"')
					.toString();
		return value;
	}
	
	private String getUnidentifiedCode(String prefix, long reportId, long rowId) {
		return new StringBuilder()
				.append(prefix)
				.append(TextUtils.lpad(Long.toString(reportId, 36).toUpperCase(), 4, '0'))
				.append(TextUtils.lpad(Long.toString(rowId, 36).toUpperCase(), 5, '0'))
				.toString();
	}
	
	private GoalScoring process(String[] args) throws SQLException, IOException {
		
		final int bindPort = Integer.parseInt(configuration
				.getProperty("goal_scoring.bind_port", configuration
						.getProperty("default.bind_port", "0")));

		// bind lock tcp port
		try (final ServerSocket socket = new ServerSocket(bindPort)) {
			logger.debug("socket bound to {}", socket.getLocalSocketAddress());
			
			// process database record(s)
			final List<Map<String, String>> reports = dao
					.executeQuery("goal_scoring.sql.select_to_process");
			logger.debug("{} report(s) to process", reports.size());
			for (Map<String, String> report : reports)
				process(report);
			
		}
		
		return this;
	}
	
	private void process(final Map<String, String> toProcess) throws SQLException, IOException {
		logger.debug("toProcess {}", toProcess);

		final long startTimeMillis = System.currentTimeMillis();

		// compute period
		final String periodDateFrom = toProcess.get("periodDateFrom");
		final String periodDateTo = toProcess.get("periodDateTo");
		if (!Pattern.matches(DATE_FORMAT, periodDateFrom))
			throw new IllegalArgumentException(String
					.format("malformed date %s", periodDateFrom));
		if (!Pattern.matches(DATE_FORMAT, periodDateTo))
			throw new IllegalArgumentException(String
					.format("malformed date %s", periodDateTo));
		final String year = periodDateTo.substring(0, 4);
		final String monthFrom = periodDateFrom.substring(5, 7);
		final String monthTo = periodDateTo.substring(5, 7);
		final String periodType;
		final int period;
		switch (Integer.parseInt(monthTo) - Integer.parseInt(monthFrom)) {
		case 0: 
			periodType = "M";
			period = Integer.parseInt(monthFrom);
			break;
		case 2: 
			periodType = "T";
			period = 1 + Integer.parseInt(monthFrom) / 3;
			break;
		case 5: 
			periodType = "S";
			period = Integer.parseInt(monthFrom) < 6 ? 1 : 2;
			break;
		case 11: 
			periodType = "A";
			period = 1;
			break;
		default:
			throw new IllegalArgumentException(String
					.format("malformed date %s", periodDateTo));
		}	
		toProcess.put("year", year);
		toProcess.put("month", monthTo);
		toProcess.put("monthTo", monthTo);
		toProcess.put("monthFrom", monthFrom);
		toProcess.put("validFrom", String.format("%s-%s-01", year, monthTo));
		
		logger.debug("year {}", year);
		logger.debug("monthFrom {}", monthFrom);
		logger.debug("monthTo {}", monthTo);
		logger.debug("periodType {}", periodType);
		logger.debug("period {}", period);
		
		// configuration params
		final int threadCount = Integer.parseInt(configuration
				.getProperty("goal_scoring.thread_count", "1"));
		final File homeFolder = new File(configuration
				.getProperty("goal_scoring.home_folder"));

		final File databaseLocalFolder = new File(configuration
				.getProperty("goal_scoring.royalty_database.local_folder")
				.replace("{year}", year)
				.replace("{month}", monthTo));
		final String nosqlS3FolderUrl = configuration
				.getProperty("goal_scoring.royalty_database.s3_folder_url")
				.replace("{year}", year)
				.replace("{month}", monthTo);
		String databaseFormat = configuration
				.getProperty("goal_scoring.royalty_database.format",
						NoSqlRoyaltyDatabase.class.getName());
		final int databaseCacheSize = Integer.parseInt(configuration
				.getProperty("goal_scoring.royalty_database.cache_size", "-1"));

		final File latestFile = new File(homeFolder,configuration
				.getProperty("goal_scoring.latest_filename", "_LATEST"));
		final boolean removeZeroRoyalties = "true".equalsIgnoreCase(configuration
				.getProperty("goal_scoring.remove_zero_royalties", "true"));
		final boolean removeUnidentified = "true".equalsIgnoreCase(configuration
				.getProperty("goal_scoring.remove_unidentified", "false"));
		final boolean removeProvisional = "true".equalsIgnoreCase(configuration
				.getProperty("goal_scoring.remove_provisional", "false"));
		final boolean removeZeroSales = "true".equalsIgnoreCase(configuration
				.getProperty("goal_scoring.remove_zero_sales", "true"));
		final String unidentifiedCodePrefix = configuration
				.getProperty("goal_scoring.unidentified_code_prefix", "UL");
		final String provisionalCodePrefix = configuration
				.getProperty("goal_scoring.provisional_code_prefix", "P");
		final String notAssociatedAccountCode = configuration
				.getProperty("goal_scoring.not_associated_account_code", "09");
		final Set<String> permanentAccountCodes = new HashSet<>(Arrays.asList(configuration
				.getProperty("goal_scoring.permanent_account_codes", "55,56,86").split(",")));
		
		final BigDecimal exArt18Rate = new BigDecimal(configuration
				.getProperty("goal_scoring.ex_art18_rate", "0.05"));
		final boolean earlyExArt18 = "true".equalsIgnoreCase(configuration
				.getProperty("goal_scoring.early_ex_art18", "false"));
		
		int skipRows = Integer.parseInt(configuration
				.getProperty("goal_scoring.skip_rows", "0"));
		final char delimiter = configuration
				.getProperty("goal_scoring.delimiter", ";").trim().charAt(0);	
		final int columns = Integer.parseInt(configuration
				.getProperty("goal_scoring.columns"));
		final int columnTitoloOpera = Integer.parseInt(configuration
				.getProperty("goal_scoring.column.titolo_opera"));
		final int columnAutore = Integer.parseInt(configuration
				.getProperty("goal_scoring.column.autore"));
		final int columnEditore = Integer.parseInt(configuration
				.getProperty("goal_scoring.column.editore"));
		final int columnInterprete = Integer.parseInt(configuration
				.getProperty("goal_scoring.column.interprete"));
		final int columnUtilizzazioni = Integer.parseInt(configuration
				.getProperty("goal_scoring.column.utilizzazioni"));
		final int columnCodiceOpera = Integer.parseInt(configuration
				.getProperty("goal_scoring.column.codice_opera"));

		final long statsFrequency = TextUtils.parseLongDuration(configuration
				.getProperty("goal_scoring.stats_frequency"));

		// additional info
		final JsonObject additionalInfo = new JsonObject();
		try {
			additionalInfo.addProperty("hostname", InetAddress.getLocalHost().getHostName());
		} catch (UnknownHostException e) {
			additionalInfo.addProperty("hostname", "localhost");
		}

		// process started
		toProcess.put("additionalInfo", gson.toJson(additionalInfo));
		dao.executeUpdate("goal_scoring.sql.update_started", toProcess);

		// load local royalty database metadata
		final String referencePeriod = year + monthTo;
		boolean databaseFound = false;
		String databaseVersion = null;
		String databaseOrigin = null;
		if (databaseLocalFolder.exists()) {
			final RoyaltyDatabaseMetadata metadata = new RoyaltyDatabaseMetadata(databaseLocalFolder);
			logger.debug("existing metadata version {}", metadata
					.getProperty(RoyaltyDatabaseMetadata.VERSION, null));
			logger.debug("existing metadata period {}", metadata
					.getProperty(RoyaltyDatabaseMetadata.PERIOD, null));
			logger.debug("existing metadata format {}", metadata
					.getProperty(RoyaltyDatabaseMetadata.FORMAT, null));
			logger.debug("existing metadata origin {}", metadata
					.getProperty(RoyaltyDatabaseMetadata.ORIGIN, null));
			databaseFound = referencePeriod.equals(metadata
					.getProperty(RoyaltyDatabaseMetadata.PERIOD, null));
			if (databaseFound) {
				databaseVersion = metadata.getProperty(RoyaltyDatabaseMetadata.VERSION, databaseVersion);
				databaseFormat = metadata.getProperty(RoyaltyDatabaseMetadata.FORMAT, databaseFormat);
				databaseOrigin = metadata.getProperty(RoyaltyDatabaseMetadata.ORIGIN, databaseOrigin);
			}
			logger.debug("databaseFound {}", databaseFound);
			logger.debug("databaseVersion {}", databaseVersion);
			logger.debug("databaseFormat {}", databaseFormat);
			logger.debug("databaseOrigin {}", databaseOrigin);
		}
		
		// check nosql _LATEST version
		final String latestS3FileUrl = new StringBuilder()
				.append(nosqlS3FolderUrl)
				.append('/')
				.append(latestFile.getName())
				.toString();
		final String latestVersion = s3
				.getObjectContents(new S3.Url(latestS3FileUrl), charset);
		logger.debug("latestVersion {}", latestVersion);
		if (Strings.isNullOrEmpty(latestVersion)) {
			logger.error("latest version file is empty or does not exist: {}", latestS3FileUrl);
			// process failed
			additionalInfo.addProperty("errorMessage", String
					.format("latest version file is empty or does not exist: %s", latestS3FileUrl));
			toProcess.put("additionalInfo", gson.toJson(additionalInfo));
			dao.executeUpdate("goal_scoring.sql.update_failed", toProcess);
			return;
		}		
		if (!latestVersion.equals(databaseVersion))
			databaseFound = false;
		databaseVersion = latestVersion;
		logger.debug("databaseVersion {}", databaseVersion);

		// download nosql database from s3
		if (!databaseFound) {

			// delete existing file(s)
			logger.debug("deleting local nosql files");
			if (databaseLocalFolder.exists())
				FileUtils.deleteFolder(databaseLocalFolder, true);
			databaseLocalFolder.mkdirs();

			// download s3 file(s)
			logger.debug("downloading nosql files from s3");
			final String s3FolderUrl = new StringBuilder()
					.append(nosqlS3FolderUrl)
					.append('/')
					.append(databaseVersion)
					.toString();
			final List<S3ObjectSummary> objects = s3.listObjects(new S3.Url(s3FolderUrl));
			for (S3ObjectSummary object : objects) {
				final String key = object.getKey();
				final String s3BucketUrl = new StringBuilder()
						.append("s3://")
						.append(object.getBucketName())
						.append('/')
						.toString();
				final String s3FileUrl = s3BucketUrl + key;
				logger.debug("s3FileUrl {}", s3FileUrl);
				String relativePath = key.substring(s3FolderUrl.length() - s3BucketUrl.length());
				logger.debug("relativePath {}", relativePath);
				if (Strings.isNullOrEmpty(relativePath) ||
						relativePath.endsWith("/"))
					continue;
				while (relativePath.startsWith("/"))
					relativePath = relativePath.substring(1);
				logger.debug("relativePath {}", relativePath);
				final File file = new File(databaseLocalFolder, relativePath);
				logger.debug("file {}", file);
				if (-1 != relativePath.indexOf('/'))
					file.getParentFile().mkdirs();
				if (!s3.download(new S3.Url(s3FileUrl), file)) {
					logger.error("nosql file download error: {}", s3FileUrl);
					// process failed
					additionalInfo.addProperty("errorMessage", String
							.format("nosql file download error: %s", s3FileUrl));
					toProcess.put("additionalInfo", gson.toJson(additionalInfo));
					dao.executeUpdate("goal_scoring.sql.update_failed", toProcess);
					return;
				} else {
					logger.debug("nosql file downloaded {}", s3FileUrl);
				}
			}

			// check downloaded database reference period
			final RoyaltyDatabaseMetadata metadata = new RoyaltyDatabaseMetadata(databaseLocalFolder);
			logger.debug("downloaded metadata version {}", metadata
					.getProperty(RoyaltyDatabaseMetadata.VERSION, null));
			logger.debug("downloaded metadata period {}", metadata
					.getProperty(RoyaltyDatabaseMetadata.PERIOD, null));
			logger.debug("downloaded metadata format {}", metadata
					.getProperty(RoyaltyDatabaseMetadata.FORMAT, null));
			logger.debug("downloaded metadata origin {}", metadata
					.getProperty(RoyaltyDatabaseMetadata.ORIGIN, null));
			databaseFound = referencePeriod.equals(metadata
					.getProperty(RoyaltyDatabaseMetadata.PERIOD, null));
			if (databaseFound) {
				databaseVersion = metadata.getProperty(RoyaltyDatabaseMetadata.FORMAT, databaseVersion);
				databaseFormat = metadata.getProperty(RoyaltyDatabaseMetadata.FORMAT, databaseFormat);
				databaseOrigin = metadata.getProperty(RoyaltyDatabaseMetadata.ORIGIN, databaseOrigin);
			}
			logger.debug("databaseFound {}", databaseFound);
			logger.debug("databaseFormat {}", databaseFormat);	
			logger.debug("databaseVersion {}", databaseVersion);	
			logger.debug("databaseOrigin {}", databaseOrigin);				

			// database not found
			if (!databaseFound) {
				logger.error("error downloading nosql database");
				// process failed
				additionalInfo.addProperty("errorMessage", String
						.format("error downloading nosql database: %s", s3FolderUrl));
				toProcess.put("additionalInfo", gson.toJson(additionalInfo));
				dao.executeUpdate("goal_scoring.sql.update_failed", toProcess);
				return;
			}
		}
		
		// load right split
		final Map<String, String> rightSplitMap = dao
				.executeSingleRowQuery("goal_scoring.sql.select_right_split", toProcess);
		logger.debug("rightSplitMap {}", rightSplitMap);
		if (null == rightSplitMap || rightSplitMap.isEmpty()) {
			logger.error("right split not found: service {}, valid from {}-{}-01",
					toProcess.get("service"), year, monthTo);
			// process failed
			additionalInfo.addProperty("errorMessage", String
					.format("right split not found: service %s, valid from %s-%s-01", toProcess.get("service"), year, monthTo));
			toProcess.put("additionalInfo", gson.toJson(additionalInfo));
			dao.executeUpdate("goal_scoring.sql.update_failed", toProcess);
			return;
		}
		final RightSplit rightSplit = new RightSplit(BigDecimals
				.setScale(new BigDecimal(rightSplitMap.get("dem"))),
				BigDecimals.setScale(new BigDecimal(rightSplitMap.get("drm"))));
		logger.debug("rightSplit {}", rightSplit);

		// anglo-american rule(s)
		final Map<String, String> angloAmericanConfigUrl = dao
				.executeSingleRowQuery("goal_scoring.sql.select_anglo_american_config", toProcess);
		final AngloAmericanConfig angloAmericanConfig = new AngloAmericanConfig(configuration,
				"goal_scoring.anglo_american", s3);
		final List<AngloAmericanRule> angloAmericanRulesMaster = angloAmericanConfig
				.getConfig(angloAmericanConfigUrl.get("s3FileUrl"));
		if (null == angloAmericanRulesMaster || angloAmericanRulesMaster.isEmpty()) {
			logger.error("anglo-american config not found: {} {}", year, monthTo);
			// process failed
			additionalInfo.addProperty("errorMessage", String
					.format("anglo-american config not found: %s %s", year, monthTo));
			toProcess.put("additionalInfo", gson.toJson(additionalInfo));
			dao.executeUpdate("goal_scoring.sql.update_failed", toProcess);
			return;
		}

		// temporary local file(s)
		final UUID uuid = UUID.randomUUID();
		final File inputFile = new File(homeFolder, uuid + "-input.csv");
		final File outputFile = new File(homeFolder, uuid + "-output.csv");
		inputFile.deleteOnExit();
		outputFile.deleteOnExit();

		// download file from s3
		final String inputFileUrl = toProcess.get("inputFileUrl");
		if (!s3.download(new S3.Url(inputFileUrl), inputFile)) {
			logger.error("input file download error: {}", inputFileUrl);
			// process failed
			additionalInfo.addProperty("errorMessage", String
					.format("input file download error: %s", inputFileUrl));
			toProcess.put("additionalInfo", gson.toJson(additionalInfo));
			dao.executeUpdate("goal_scoring.sql.update_failed", toProcess);
			return;
		} else
			logger.debug("file downloaded {}", inputFileUrl);

		// constant(s)
		final long idMlReport = Long.parseLong(toProcess.get("id"));
		final String licenseeCode = toProcess.get("licenseeCode");
		final String commercialOffer = toProcess.get("commercialOffer");
		final String useType = toProcess.get("useType");
		final String invoiceId = toProcess.get("invoiceId");
		
		// stats
		final Accumulators stats = new Accumulators();
		final Map<String, Accumulators> angloAmericanStatsMap = new ConcurrentHashMap<>();
		for (AngloAmericanRule rule : angloAmericanRulesMaster)
			angloAmericanStatsMap.put(rule.repertoireCode, new Accumulators());
		stats.add("recordNonCodificati", 0);
		stats.add("utilizzazioniNonCodificate", 0);								
		stats.add("recordCodificatiProvvisori", 0);
		stats.add("utilizzazioniCodificateProvvisorie", 0);								
		stats.add("recordSchemaNonTrovato", 0);
		stats.add("utilizzazioniSchemaNonTrovato", 0);
		stats.add("recordPubblicoDominio", 0);
		stats.add("utilizzazioniPubblicoDominio", 0);
		stats.add("recordNonCodificati", 0);
		stats.add("utilizzazioniNonCodificate", 0);
		stats.add("recordCodificatiProvvisori", 0);
		stats.add("utilizzazioniCodificateProvvisorie", 0);								
		stats.add("recordCodificatiRegolari", 0);
		stats.add("utilizzazioniCodificateRegolari", 0);								
		stats.add("recordCodificatiIrregolari", 0);
		stats.add("utilizzazioniCodificateIrregolari", 0);		
		stats.add("punti", BigDecimal.ZERO);
		stats.add("puntiDem", BigDecimal.ZERO);
		stats.add("puntiDrm", BigDecimal.ZERO);
		stats.add("puntiExArt18", BigDecimal.ZERO);
//		stats.add("puntiEsclusiExArt18", BigDecimal.ZERO);
//		stats.add("puntiDemSiae", BigDecimal.ZERO);
//		stats.add("puntiDrmSiae", BigDecimal.ZERO);
//		stats.add("puntiDemPubblicoDominio", BigDecimal.ZERO);
//		stats.add("puntiDrmPubblicoDominio", BigDecimal.ZERO);
//		stats.add("puntiDemAngloAmericani", BigDecimal.ZERO);
//		stats.add("puntiDrmAngloAmericani", BigDecimal.ZERO);
//		stats.add("puntiDemNonSoci", BigDecimal.ZERO);								
//		stats.add("puntiDrmNonSoci", BigDecimal.ZERO);
//		stats.add("puntiDemContiSpeciali", BigDecimal.ZERO);								
//		stats.add("puntiDrmContiSpeciali", BigDecimal.ZERO);
//		stats.add("puntiDemRimanentiCasi", BigDecimal.ZERO);
//		stats.add("puntiDrmRimanentiCasi", BigDecimal.ZERO);

		// process report
		logger.debug("processing input file {}", inputFile);
		long elapsedTimeMillis = 0L;
		
		logger.debug("opening royalty database");
		try (final RoyaltyDatabase database = BdbRoyaltyDatabase.class.getName().equals(databaseFormat) ?
				new BdbRoyaltyDatabase(databaseLocalFolder, true, false, charset, false) :
					NoSqlRoyaltyDatabase.class.getName().equals(databaseFormat) ?
						new NoSqlRoyaltyDatabase(databaseLocalFolder, true, -1, databaseCacheSize, charset, false) : null) {

			if (null == database)
				throw new IllegalStateException(String
						.format("unknown royalty database type: %s", databaseFormat));

			try (final OutputStream outputStream = new FileOutputStream(outputFile);
					final BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);
					final FileInputStream inputStream = new FileInputStream(inputFile);
					final InputStreamReader inputStreamReader = new InputStreamReader(inputStream, charset);
					final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
					final CSVParser csvParser = new CSVParser(bufferedReader, CSVFormat.newFormat(delimiter)
							.withQuoteMode(QuoteMode.MINIMAL)
							.withQuote('"')
							.withIgnoreEmptyLines()
							.withIgnoreSurroundingSpaces())) {
	
				final Iterator<CSVRecord> csvIterator = csvParser.iterator();
				for (; skipRows > 0 && csvIterator.hasNext(); skipRows --) {
					final CSVRecord record = csvIterator.next();
					logger.debug("record skipped {}", record);
				}
				final Object monitor = new Object();
				final ExecutorService executorService = Executors.newCachedThreadPool();
				final AtomicInteger runningThreads = new AtomicInteger(0);
				final AtomicReference<Exception> threadException = new AtomicReference<>(null);
				final Striped<Lock> schemeLocks = Striped.lock(threadCount);
				while (runningThreads.get() < threadCount) {
					logger.debug("starting thread {}", runningThreads.incrementAndGet());
					executorService.execute(new Runnable() {
						
						@Override
						public void run() {
							try {
								
								// WARNING: duplicate for thread safety
								final Map<String, AngloAmericanRule> angloAmericanRules = new HashMap<>();
								for (AngloAmericanRule rule : angloAmericanRulesMaster)
									angloAmericanRules.put(rule.repertoireCode, rule);
								final Set<String> angloAmericanRepertoires = new HashSet<>();
								
								while (null == threadException.get()) {
	
									// read next csv line
									final CSVRecord record;
									synchronized (csvIterator) {
										if (!csvIterator.hasNext())
											return;
										record = csvIterator.next();
									}
//									logger.debug("record {}", record);

									// update stats
									stats.add("recordTotali", 1);

									// malformed record
									if (columns != record.size()) {
//										logger.debug("bad column number {} {}", record.size(), record);
										stats.add("recordNonValidi", 1);
										continue;
									}
									
									// parse line
									final String titoloOpera = record.get(columnTitoloOpera);
									final String autore = record.get(columnAutore);
									final String editore = record.get(columnEditore);
									final String interprete = record.get(columnInterprete);
									final String utilizzazioni = record.get(columnUtilizzazioni);
									final String codiceOpera = record.get(columnCodiceOpera);
									final int sales = Integer.parseInt(TextUtils
											.nullOrEmptyValue(utilizzazioni, "0"));
									final BigDecimal decimalSales = sales > 0 ?
											new BigDecimal(sales) : BigDecimal.ZERO;
	
									// update stats
									stats.add("utilizzazioniTotali", sales);
	
									// zero sales
									if (0 == sales) {
//										logger.debug("zero sales");
										if (removeZeroSales) {
											stats.add("recordZeroUtilizzazioni", 1);
											continue;
										}
									}
	
									// work code & royalty scheme
									String workCode = null;
									RoyaltyScheme scheme = null;
									
									// unidentified
									if (Strings.isNullOrEmpty(codiceOpera)) {
//	 									logger.debug("unidentified (sales {})", sales);
										if (removeUnidentified) {
											stats.add("recordNonCodificati", 1);
											stats.add("utilizzazioniNonCodificate", sales);								
											continue;
										}
										workCode = getUnidentifiedCode(unidentifiedCodePrefix, idMlReport, record.getRecordNumber());
										scheme = new RoyaltyScheme(workCode);
										scheme.add(new DemRoyalty("", "", "", "13", "97777777600", "", null, false, true, false, false, null, new BigDecimal(24), new BigDecimal(24), false));
										scheme.add(new DrmRoyalty("", "", "", "13", "97777777600", "", null, false, true, false, false, null, new BigDecimal(100)));
									}
									
									// provisional code
									else if (codiceOpera.startsWith(provisionalCodePrefix)) {
//										logger.debug("provisional {} (sales {})", codiceOpera, sales);
										if (removeProvisional) {
											stats.add("recordCodificatiProvvisori", 1);
											stats.add("utilizzazioniCodificateProvvisorie", sales);								
											continue;
										}
										workCode = codiceOpera;
										scheme = new RoyaltyScheme(workCode);
										scheme.add(new DemRoyalty("", "", "", "18", "97777777500", "", null, false, true, false, false, null, new BigDecimal(24), new BigDecimal(24), false));
										scheme.add(new DrmRoyalty("", "", "", "18", "97777777500", "", null, false, true, false, false, null, new BigDecimal(100)));
									}
	
									// lookup royalty scheme
									else {
										workCode = codiceOpera;
										scheme = database.getScheme(workCode);
									}
//									logger.debug("scheme {}", scheme);
	
									// royalty scheme not found
									if (null == scheme || scheme.isEmpty()) {	
//										logger.debug("scheme not found {} (sales {})", workCode, sales);
										stats.add("recordSchemaNonTrovato", 1);
										stats.add("utilizzazioniSchemaNonTrovato", sales);
										continue;
									}
	
									// acquire striped lock to access royalty schemes
									final Lock lock = schemeLocks.get(scheme);
									try {
										lock.lock();
	
										// 100% public domain
										if (scheme.publicDomain) {
											stats.add("recordPubblicoDominio", 1);
											stats.add("utilizzazioniPubblicoDominio", sales);
											continue;									
										}
	
										// work type
										final char workType;
										if (workCode.startsWith(unidentifiedCodePrefix))
											workType = 'N';
										else if(workCode.startsWith(provisionalCodePrefix))
											workType = 'P';
										else if(scheme.isIrregular())
											workType = 'I';
										else
											workType = 'R';
										switch (workType) {
										case 'N':
											stats.add("recordNonCodificati", 1);
											stats.add("utilizzazioniNonCodificate", sales);								
											break;
										case 'P':
											stats.add("recordCodificatiProvvisori", 1);
											stats.add("utilizzazioniCodificateProvvisorie", sales);								
											break;
										case 'R':
											stats.add("recordCodificatiRegolari", 1);
											stats.add("utilizzazioniCodificateRegolari", sales);								
											break;
										case 'I':
											stats.add("recordCodificatiIrregolari", 1);
											stats.add("utilizzazioniCodificateIrregolari", sales);								
											break;
										}
	
										// normalize DEM royalties sum to 1.0, if greater than 1.0
										boolean normalizeDem = null != scheme.demSum &&
												BigDecimals.isReallyGreaterThan(scheme.demSum, BigDecimal.ONE);
//										if (normalizeDem)
//											logger.warn("DEM royalties sum too big {}", scheme.demSum);
	
										// normalize DEM royalties sum to 1.0, if greater than 1.0
										boolean normalizeDrm = null != scheme.drmSum &&
												BigDecimals.isReallyGreaterThan(scheme.drmSum, BigDecimal.ONE);
//										if (normalizeDrm)
//											logger.warn("DRM royalties sum too big {}", scheme.drmSum);
	
										// work stats
										BigDecimal points = BigDecimal.ZERO;
										BigDecimal exArt18Points = BigDecimal.ZERO;
										BigDecimal exArt18ExcludedPoints = BigDecimal.ZERO;
										BigDecimal demDustConsumersRate = BigDecimal.ZERO;
										BigDecimal drmDustConsumersRate = BigDecimal.ZERO;
										BigDecimal demDustPoints = BigDecimal.ZERO;
										BigDecimal drmDustPoints = BigDecimal.ZERO;
										BigDecimal demPublicDomainPoints = BigDecimal.ZERO;
										BigDecimal drmPublicDomainPoints = BigDecimal.ZERO;
										BigDecimal demAngloAmericanPoints = BigDecimal.ZERO;
										BigDecimal drmAngloAmericanPoints = BigDecimal.ZERO;
										BigDecimal demNotAssociatedPoints = BigDecimal.ZERO;
										BigDecimal drmNotAssociatedPoints = BigDecimal.ZERO;
										BigDecimal demSpecialAccountPoints = BigDecimal.ZERO;
										BigDecimal drmSpecialAccountPoints = BigDecimal.ZERO;
										BigDecimal demSiaePoints = BigDecimal.ZERO;
										BigDecimal drmSiaePoints = BigDecimal.ZERO;
										BigDecimal demOtherPoints = BigDecimal.ZERO;
										BigDecimal drmOtherPoints = BigDecimal.ZERO;
										BigDecimal schemeDemSum = BigDecimal.ZERO;
										BigDecimal schemeDrmSum = BigDecimal.ZERO;
										int demDustConsumers = 0;
										int drmDustConsumers = 0;
										angloAmericanRepertoires.clear();
													
										// 1st loop on royalties
										for (Royalty royalty : scheme) {
											
											// DEM
											if (royalty.isDem()) {
												final DemRoyalty dem = (DemRoyalty) royalty;
												
												// normalize royalty
												if (normalizeDem) {
													royalty.rate = BigDecimals
														.divide(royalty.rate, scheme.demSum);
													schemeDemSum = schemeDemSum
															.add(royalty.rate);
												}
	
												// public domain
												if (royalty.publicDomain)
													royalty.dustProducer = false;
	
												// anglo-american repertoire
												else if (royalty.isAngloAmerican()) {
													angloAmericanRepertoires.add(royalty.angloAmericanRepertoire);
													// anglo-american exclusion(s)
													final AngloAmericanRule rule = angloAmericanRules
															.get(royalty.angloAmericanRepertoire);
													if (null != rule) {
														if (rule.isDspExcluded(licenseeCode)) {
															royalty.dustProducer = true;
														} else {
															royalty.dustProducer = false;
															if (!Strings.isNullOrEmpty(rule.demAccountCode))
																royalty.accountCode = rule.demAccountCode;
															if (!Strings.isNullOrEmpty(rule.demAccountId))
																royalty.accountId = rule.demAccountId;
															if (!Strings.isNullOrEmpty(rule.demSiaePosition)) {
																royalty.siaePosition = rule.demSiaePosition;
																royalty.ipiCode = null;
															}
														}
													} else {
														royalty.dustProducer = false;
													}
												}
												
												// SIAE
												else if (royalty.siae)
													royalty.dustProducer = false;
												
												// not associated
												else if (royalty.notAssociated)
													royalty.dustProducer = false;
												
												// special account
												else if (royalty.specialAccount)
													royalty.dustProducer = false;
	
												// all other cases
												else
													royalty.dustProducer = false;
												
												// compute points (<rate> * <sales> * <right split>)
												royalty.points = decimalSales
														.multiply(rightSplit.dem)
														.multiply(royalty.rate);
	
												// total points
												points = points.add(royalty.points);
	
												// ex art. 18
												if (earlyExArt18) {
													final BigDecimal exArt18 = royalty.points
															.multiply(exArt18Rate);
													if (dem.exArt18Excluded) {
														exArt18ExcludedPoints = exArt18ExcludedPoints
																.add(exArt18);												
													} else if (!BigDecimals.isAlmostZero(exArt18)) {
														royalty.points = royalty.points
																.subtract(exArt18);
														exArt18Points = exArt18Points.add(exArt18);												
													}
												}
	
												// accumulate dust points
												if (royalty.dustProducer)
													demDustPoints = demDustPoints.add(royalty.points);
												
												// accumulate dust consumers rates
												royalty.dustConsumer = !royalty.dustProducer;
												if (royalty.dustConsumer) {
													demDustConsumersRate = demDustConsumersRate.add(royalty.rate);
													demDustConsumers ++;
												}
												
											}
											
											// DRM
											else if (royalty.isDrm()) {
												
												// normalize royalty
												if (normalizeDrm) {
													royalty.rate = BigDecimals
															.divide(royalty.rate, scheme.drmSum);
													schemeDrmSum = schemeDrmSum
															.add(royalty.rate);
												}
												
												// public domain
												if (royalty.publicDomain)
													royalty.dustProducer = false;
												
												// anglo america repertoire
												else if (royalty.isAngloAmerican()) {
													angloAmericanRepertoires.add(royalty.angloAmericanRepertoire);
													// anglo-american exclusion(s)
													final AngloAmericanRule rule = angloAmericanRules
															.get(royalty.angloAmericanRepertoire);
													if (null != rule) {
														if (rule.isDspExcluded(licenseeCode)) {
															royalty.dustProducer = true;
														} else {
															royalty.dustProducer = false;
															if (!Strings.isNullOrEmpty(rule.drmAccountCode))
																royalty.accountCode = rule.drmAccountCode;
															if (!Strings.isNullOrEmpty(rule.drmAccountId))
																royalty.accountId = rule.drmAccountId;
															if (!Strings.isNullOrEmpty(rule.drmSiaePosition)) {
																royalty.siaePosition = rule.drmSiaePosition;
																royalty.ipiCode = null;
															}
														}
													} else
														royalty.dustProducer = false;
												}
												
												// SIAE
												else if (royalty.siae)
													royalty.dustProducer = false;
												
												// not associated
												else if (royalty.notAssociated)
													royalty.dustProducer = false;
	
												// special account
												else if (royalty.specialAccount)
													royalty.dustProducer = false;
	
												// all other cases
												else
													royalty.dustProducer = false;
	
												// compute points (<rate> * <sales> * <right split>)
												royalty.points = decimalSales
														.multiply(rightSplit.drm)
														.multiply(royalty.rate);
	
												// total points
												points = points.add(royalty.points);
	
												// accumulate dust points
												if (royalty.dustProducer)
													drmDustPoints = drmDustPoints.add(royalty.points);
												
												// accumulate dust consumers rates
												royalty.dustConsumer = !royalty.dustProducer;
												if (royalty.dustConsumer) {
													drmDustConsumersRate = drmDustConsumersRate.add(royalty.rate);
													drmDustConsumers ++;
												}
												
											}
																				
										} // 1st loop on royalties
	
										if (normalizeDem)
											scheme.demSum = BigDecimals
												.setScale(schemeDemSum).stripTrailingZeros();
										if (normalizeDrm)
											scheme.drmSum = BigDecimals
													.setScale(schemeDrmSum).stripTrailingZeros();
										
										// work stats
										BigDecimal demPoints = BigDecimal.ZERO;
										BigDecimal drmPoints = BigDecimal.ZERO;
	
										// dust dispersion DEM scale factor
										BigDecimal demDustScaledPoints = null;
										BigDecimal demRemainingDustPoints = demDustPoints;
										if (!BigDecimals.isAlmostZero(demDustConsumersRate)) {
											demDustScaledPoints = BigDecimals
												.divide(demDustPoints, demDustConsumersRate);
											if (BigDecimals.isAlmostZero(demDustScaledPoints))
												demDustScaledPoints = null;
										}
	
										// dust dispersion DRM scale factor
										BigDecimal drmDustScaledPoints = null;
										BigDecimal drmRemainingDustPoints = drmDustPoints;
										if (!BigDecimals.isAlmostZero(drmDustConsumersRate)) {
											drmDustScaledPoints = BigDecimals
												.divide(drmDustPoints, drmDustConsumersRate);
											if (BigDecimals.isAlmostZero(drmDustScaledPoints))
												drmDustScaledPoints = null;
										}
	
										// 2nd loop on royalties
										for (Iterator<Royalty> iterator = scheme.iterator(); iterator.hasNext(); ) {
											final Royalty royalty = iterator.next();									
	
											// remove dust producer(s)
											if (royalty.dustProducer)
												iterator.remove();
	
											// DEM
											else if (royalty.isDem()) {
												final DemRoyalty dem = (DemRoyalty) royalty;
												
												// dust dispersion
												if (null != demDustScaledPoints && royalty.dustConsumer) {
													if (demDustConsumers > 1) {
														final BigDecimal dust = demDustScaledPoints
																.multiply(royalty.rate);
														royalty.points = royalty.points.add(dust);
														demRemainingDustPoints = demRemainingDustPoints
																.subtract(dust);
													} else {
														royalty.points = royalty.points
																.add(demRemainingDustPoints);
														demRemainingDustPoints = null;
													}
													demDustConsumers --;
												}
												
												// ex art. 18
												if (!earlyExArt18) {
													final BigDecimal exArt18 = exArt18Rate
															.multiply(royalty.points);
													if (dem.exArt18Excluded) {
														exArt18ExcludedPoints = exArt18ExcludedPoints
																.add(exArt18);												
													} else if (!BigDecimals.isAlmostZero(exArt18)) {
														royalty.points = royalty.points
																.subtract(exArt18);
														exArt18Points = exArt18Points.add(exArt18);												
													}
												}
												
												// sum points
												demPoints = demPoints.add(royalty.points);
	
												// work stats
												if (royalty.publicDomain) {
													demPublicDomainPoints = demPublicDomainPoints.add(royalty.points);
												} else if (royalty.isAngloAmerican()) {
													demAngloAmericanPoints = demAngloAmericanPoints.add(royalty.points);
													final Accumulators angloAmericanStats = angloAmericanStatsMap
															.get(royalty.angloAmericanRepertoire);
													angloAmericanStats.add("puntiDem", royalty.points);
												} else if (royalty.siae) {
													demSiaePoints = demSiaePoints.add(royalty.points);
												} else if (royalty.notAssociated) {
													demNotAssociatedPoints = demNotAssociatedPoints.add(royalty.points);
													if (!Strings.isNullOrEmpty(notAssociatedAccountCode)) {
														if (!permanentAccountCodes.contains(royalty.accountCode)) {
															royalty.accountCode = notAssociatedAccountCode;
														}
													}
												} else if (royalty.specialAccount) {
													demSpecialAccountPoints = demSpecialAccountPoints.add(royalty.points);
												} else {
													demOtherPoints = demOtherPoints.add(royalty.points);
												}
												
											}

											// DRM
											else if (royalty.isDrm()) {
												
												// dust dispersion
												if (null != drmDustScaledPoints && royalty.dustConsumer) {
													if (drmDustConsumers > 1) {
														final BigDecimal dust = drmDustScaledPoints
																.multiply(royalty.rate);
														royalty.points = royalty.points.add(dust);
														drmRemainingDustPoints = drmRemainingDustPoints
																.subtract(dust);
													} else {
														royalty.points = royalty.points
																.add(drmRemainingDustPoints);
														drmRemainingDustPoints = null;
													}
													drmDustConsumers --;
												}
												
												// sum points
												drmPoints = drmPoints.add(royalty.points);
	
												// work stats
												if (royalty.publicDomain) {
													drmPublicDomainPoints = drmPublicDomainPoints.add(royalty.points);
												} else if (royalty.isAngloAmerican()) {
													drmAngloAmericanPoints = drmAngloAmericanPoints.add(royalty.points);
													final Accumulators angloAmericanStats = angloAmericanStatsMap
															.get(royalty.angloAmericanRepertoire);
													angloAmericanStats.add("puntiDrm", royalty.points);
												} else if (royalty.siae) {
													drmSiaePoints = drmSiaePoints.add(royalty.points);
												} else if (royalty.notAssociated) {
													drmNotAssociatedPoints = drmNotAssociatedPoints.add(royalty.points);
													if (!Strings.isNullOrEmpty(notAssociatedAccountCode)) {
														if (!permanentAccountCodes.contains(royalty.accountCode)) {
															royalty.accountCode = notAssociatedAccountCode;
														}
													}
												} else if (royalty.specialAccount) {
													drmSpecialAccountPoints = drmSpecialAccountPoints.add(royalty.points);
												} else {
													drmOtherPoints = drmOtherPoints.add(royalty.points);
												}
												
											}
											
										} // 2nd loop on royalties
	
										// check for points sum errors
										final BigDecimal pointsError = points
												.subtract(demPoints)
												.subtract(drmPoints)
												.subtract(exArt18Points);
										if (!BigDecimals.isAlmostZero(pointsError)) {
											logger.debug("points error on scheme {}", scheme);
											throw new IllegalStateException(String.format("bad points sum \"%s\"",
													BigDecimals.toPlainString(pointsError)));
										}
										
										// update statistics
										stats.add("punti", points);
										stats.add("puntiDem", demPoints);
										stats.add("puntiDrm", drmPoints);
										stats.add("puntiExArt18", exArt18Points);
										stats.add("puntiEsclusiExArt18", exArt18ExcludedPoints);
										stats.add("puntiDemSiae", demSiaePoints);
										stats.add("puntiDrmSiae", drmSiaePoints);
										stats.add("puntiDemPubblicoDominio", demPublicDomainPoints);
										stats.add("puntiDrmPubblicoDominio", drmPublicDomainPoints);
										stats.add("puntiDemAngloAmericani", demAngloAmericanPoints);
										stats.add("puntiDrmAngloAmericani", drmAngloAmericanPoints);
										stats.add("puntiDemNonSoci", demNotAssociatedPoints);								
										stats.add("puntiDrmNonSoci", drmNotAssociatedPoints);
										stats.add("puntiDemContiSpeciali", demSpecialAccountPoints);								
										stats.add("puntiDrmContiSpeciali", drmSpecialAccountPoints);
										stats.add("puntiDemRimanentiCasi", demOtherPoints);
										stats.add("puntiDrmRimanentiCasi", drmOtherPoints);
										for (String repertoire : angloAmericanRepertoires) {
											final Accumulators angloAmericanStats = angloAmericanStatsMap.get(repertoire);
											if (null == angloAmericanStats)
												throw new IllegalStateException(String
														.format("bad anglo-america repertoire code \"%s\"", repertoire));
											angloAmericanStats.add("numeroRecord", 1);
											angloAmericanStats.add("numeroUtilizzazioni", sales);
										}
										
										// write output csv lines
										final String territorio = "ITA";
										final String saleType = "L";
										final String tipoRepertorio = "M";
										final String identificationType = "F";
	
										// group royalties into royalty holders
										final List<RoyaltyHolder> holders = new ArrayList<>();
										while (!scheme.isEmpty()) {
											final Iterator<Royalty> iterator = scheme.iterator();
											DemRoyalty dem = null;
											DrmRoyalty drm = null;
											// find first royalty (eventually removing zeroes)
											while (null == dem && null == drm && iterator.hasNext()) {
												final Royalty royalty = iterator.next();
												iterator.remove();
												if (!removeZeroRoyalties || !royalty.isZero()) {
													dem = royalty.isDem() ? (DemRoyalty) royalty : null;
													drm = royalty.isDrm() ? (DrmRoyalty) royalty : null;
												}
											}
											// find second royalty (eventually removing zeroes)
											while (iterator.hasNext()) {
												final Royalty royalty = iterator.next();
												if (removeZeroRoyalties && royalty.isZero())
													iterator.remove();
												else if (null != dem) {
													drm = royalty.isDrm() ? (DrmRoyalty) royalty : null;
													if (null != drm) {
														if (dem.getHolderId().equals(drm.getHolderId())) {
															iterator.remove();
															break;
														}
														drm = null;
													}
												} else if (null != drm) {
													dem = royalty.isDem() ? (DemRoyalty) royalty : null;
													if (null != dem) {
														if (dem.getHolderId().equals(drm.getHolderId())) {
															iterator.remove();
															break;
														}
														dem = null;
													}
												}
											}
											if (null != dem || null != drm)
												holders.add(new RoyaltyHolder(dem, drm));
										}
	
										// loop on royalty holders
										final StringBuilder outputLines = new StringBuilder();
										for (RoyaltyHolder holder : holders) {
	
											final DemRoyalty dem = holder.dem;
											final DrmRoyalty drm = holder.drm;
											final Royalty royalty = null != dem ? dem : null != drm ? drm : null;
	
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  NOME_CAMPO                              TIPO_DI_DATO   NULLABLE    NOTE
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Codice provider                         CHAR(4)        FALSE       Codice di riferimento del provider SOPHIA
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Offerta commerciale                     CHAR           FALSE       Offerta commerciale SOPHIA
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Tipo utilizzo                           CHAR(4)        FALSE       Codice tipo utilizzo dal CCID
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Territorio                              CHAR(3)        FALSE       Territorio dell'utilizzazione
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Mese di incasso fattura                 NUMBER(6)      FALSE       Anno e mese di incasso fattura 
//    (NB: nel file della valorizzazione il campo è vuoto)
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Anno di riferimento utilizzazione       NUMBER(4)      FALSE    
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Tipo di periodo di riferimento          CHAR(1)        FALSE       M = "Mensile"; T = "Trimestrale", S = "Semestrale"; A = "Annuale"
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Periodo di riferimento utilizzazione    NUMBER(5)      FALSE       "Periodo di riferimento dell'utilizzazione.
//	                                                                       In caso di "Tipo di periodo di riferimento" = M i valori possibili saranno: 1-12
//	                                                                       In caso di "Tipo di periodo di riferimento" = T i valori possibili saranno: 1-4
//	                                                                       In caso di "Tipo di periodo di riferimento" = S i valori possibili saranno: 1-2
//	                                                                       In caso di "Tipo di periodo di riferimento" = A il valore possibile è: 1"
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Tipo utilizzazione                      CHAR           FALSE       O = "Online"; B = "Broadcasting"; P = "Performing"
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Tipo repertorio                         CHAR           FALSE       M = "Musica"
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Quantità utilizzazioni                  NUMBER         FALSE       Numero di utilizzazioni dell'opera
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Codice opera                            CHAR(11)       TRUE        Codice opera a 11 cifre
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Flag opera                              CHAR           FALSE       R = "Regolare"; I = "Irregolare"; P = "Provvisoria"; N = "Non Riconosciuta"
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Tipo codifica                           CHAR           TRUE        A = "Automatica"; F = "Fuzzy"; M = "Manuale"; W = "ISWC"; R = "ISRC"
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Qualifica Avente Diritto                CHAR(2)        TRUE        La qualifica è sempre presente tranne nei casi in cui le quote vengono accantonate su conti speciali
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Posizione Siae Avente Diritto           NUMBER         TRUE        Posizione SIAE dell'avente diritto
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  IPI number AD                           CHAR(9)        TRUE        IPI number dell'avente diritto così come presente nello schema di riparto
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Numeratore quota DEM                    NUMBER         FALSE       Numeratore della quota DEM
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Denominatore quota DEM                  NUMBER         FALSE       Denominatore della quota DEM
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Codice conto DEM                        CHAR(2)        FALSE       Codice conto quota DEM
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Numero conto DEM                        CHAR(9)        FALSE       Numero conto quota DEM
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Compenso DEM netta Avente Diritto       NUMBER         FALSE       Compenso DEM per Avente Diritto al netto dell'aggio SIAE
//    (NB: il file della valorizzazione contiene invece i punti DEM)
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Numeratore quota DRM                    NUMBER         FALSE       Numeratore della percentuale DRM espressa in diecimillesimi
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Denominatore quota DRM                  NUMBER(5)      FALSE       Denominatore della quota DRM fisso a 10000
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Codice conto DRM                        CHAR(2)        FALSE       Codice conto quota DRM
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Numero conto DRM                        CHAR(9)        FALSE       Numero conto quota DRM
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Compenso DRM netta Avente Diritto       NUMBER         FALSE       Compenso DRM per Avente Diritto al netto dell'aggio SIAE
//    (NB: il file della valorizzazione contiene invece i punti DRM)
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Percentuale di Claim                    NUMBER(7)      TRUE        Percentuale totale di claim per questa riga, dopo l'applicazione dello split (x 10000)
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Titolo utilizzazione                    STRING         FALSE       Titolo dell'utilizzazione presente nel DSR
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------
//  Nominativo utilizzazione                STRING         TRUE        Nominativo AADD/Esecutori come da report
//  --------------------------------------- -------------- ----------- --------------------------------------------------------------------------------------------------------

											BigDecimal percentualeDiClaim = BigDecimal.ZERO;
											if (sales > 0) {
												if (null != dem)
													percentualeDiClaim.add(rightSplit
															.dem.multiply(dem.rate));
												if (null != drm)
													percentualeDiClaim.add(rightSplit
															.drm.multiply(drm.rate));
												percentualeDiClaim = percentualeDiClaim.multiply(TEN_THOUSANDS);
											}
	
											// format output csv line
											final StringBuilder outputLine = new StringBuilder()
												.append(TextUtils.nullValue(licenseeCode, ""))
												.append(delimiter)
												.append(TextUtils.nullValue(commercialOffer, ""))
												.append(delimiter)
												.append(TextUtils.nullValue(useType, ""))
												.append(delimiter)
												.append(territorio)
												.append(delimiter)
												.append(TextUtils.nullValue(invoiceId, ""))
												.append(delimiter)
												.append(year)
												.append(delimiter)
												.append(periodType)
												.append(delimiter)
												.append(period)
												.append(delimiter)
												.append(saleType)
												.append(delimiter)
												.append(tipoRepertorio)
												.append(delimiter)
												.append(TextUtils.nullOrEmptyValue(utilizzazioni, ""))
												.append(delimiter)
												.append(TextUtils.nullOrEmptyValue(workCode, ""))
												.append(delimiter)
												.append(workType)
												.append(delimiter)
												.append(identificationType)
												.append(delimiter)
												.append(TextUtils.nullOrEmptyValue(royalty.role, ""))
												.append(delimiter)
												.append(TextUtils.nullOrEmptyValue(royalty.siaePosition, ""))
												.append(delimiter)
												.append(TextUtils.nullOrEmptyValue(royalty.ipiCode, ""));
											if (null == dem) {
												outputLine
													.append(delimiter)
													.append("0")
													.append(delimiter)
													.append("24")
													.append(delimiter)
													.append(TextUtils.nullOrEmptyValue(drm.accountCode, ""))
													.append(delimiter)
													.append(TextUtils.nullOrEmptyValue(drm.accountId, ""))
													.append(delimiter)
													.append("0");										
											} else {
												outputLine
													.append(delimiter)
													.append(BigDecimals.toPlainString(dem.numerator))
													.append(delimiter)
													.append(BigDecimals.toPlainString(dem.denominator))
													.append(delimiter)
													.append(TextUtils.nullOrEmptyValue(dem.accountCode, ""))
													.append(delimiter)
													.append(TextUtils.nullOrEmptyValue(dem.accountId, ""))
													.append(delimiter)
													.append(BigDecimals.toPlainString(dem.points));										
											}
											if (null == drm) {
												outputLine
													.append(delimiter)
													.append("0")
													.append(delimiter)
													.append("10000")
													.append(delimiter)
													.append(TextUtils.nullOrEmptyValue(dem.accountCode, ""))
													.append(delimiter)
													.append(TextUtils.nullOrEmptyValue(dem.accountId, ""))
													.append(delimiter)
													.append("0");
											} else {
												outputLine
													.append(delimiter)
													.append(drm.percentage.multiply(ONE_HUNDRED).toBigInteger())
													.append(delimiter)
													.append("10000")
													.append(delimiter)
													.append(TextUtils.nullOrEmptyValue(drm.accountCode, ""))
													.append(delimiter)
													.append(TextUtils.nullOrEmptyValue(drm.accountId, ""))
													.append(delimiter)
													.append(BigDecimals.toPlainString(drm.points));												
											}
											outputLine
												.append(delimiter)
												.append(percentualeDiClaim.toBigInteger())
												.append(delimiter)
												.append(escapeCsvValue(titoloOpera, delimiter))
												.append(delimiter)
												.append(!Strings.isNullOrEmpty(autore) ? escapeCsvValue(autore, delimiter) :
													!Strings.isNullOrEmpty(interprete) ? escapeCsvValue(interprete, delimiter) :
													!Strings.isNullOrEmpty(editore) ? escapeCsvValue(editore, delimiter) : "");
	
											// append to output
											outputLines.append(outputLine)
												.append('\n');
											
										}
										
										// write to output
										final byte[] outputBytes = outputLines
												.toString().getBytes(charset);
										synchronized (bufferedOutputStream) {
											bufferedOutputStream.write(outputBytes);
										}
										
									} finally {
										// release striped lock to access royalty schemes
										lock.unlock();
									}
								}
								
							} catch (Exception e) {
								logger.error("run", e);
								threadException.compareAndSet(null, e);
							} finally {
								logger.debug("thread {} finished", runningThreads.getAndDecrement());
								synchronized (monitor) {
									monitor.notify();
								}
							}
						}
	
					});				
				}
				
				while (runningThreads.get() > 0) {
					synchronized (monitor) {
						monitor.wait(statsFrequency);
					}
					
					elapsedTimeMillis = System.currentTimeMillis() - startTimeMillis;
					// print stats
					logger.debug("---------- stats ----------");
					logger.debug("processing time {}",
							TextUtils.formatDuration(elapsedTimeMillis));
					logger.debug("average line processing time {}ms", 
							Math.round((double) elapsedTimeMillis / (double) stats.getInteger("recordTotali")));
					logger.debug("    recordTotali {}", stats.getInteger("recordTotali"));
					logger.debug("    utilizzazioniTotali {}", stats.getInteger("utilizzazioniTotali"));
					logger.debug("    puntiDem {}", BigDecimals
							.toPlainString(stats.getDecimal("puntiDem")));
					logger.debug("    puntiDrm {}", BigDecimals
							.toPlainString(stats.getDecimal("puntiDrm")));
					logger.debug("    puntiExArt18 {}", BigDecimals
							.toPlainString(stats.getDecimal("puntiExArt18")));
				}
				
				// re-throw thread exception
				if (null != threadException.get())
					throw threadException.getAndSet(null);
	
			}
			
		} catch (Exception e) {
			logger.error("process", e);
			// process failed
			final StringWriter stackTrace = new StringWriter();
			e.printStackTrace(new PrintWriter(stackTrace));
			additionalInfo.addProperty("errorMessage", "error processing file");
			additionalInfo.addProperty("stackTrace", stackTrace.toString());
			toProcess.put("additionalInfo", gson.toJson(additionalInfo));
			dao.executeUpdate("goal_scoring.sql.update_failed", toProcess);
			return;
		}

		logger.debug("{} records processed", stats
				.getInteger("recordTotali"));
		elapsedTimeMillis = System.currentTimeMillis() - startTimeMillis;
		logger.debug("scoring completed in {}",
				TextUtils.formatDuration(elapsedTimeMillis));

		// upload output file to s3
		final String outputFileUrl = configuration
				.getProperty("goal_scoring.output_file_url")
				.replace("{period}", toProcess.get("period"))
				.replace("{reportId}", toProcess.get("reportId"));
		if (!s3.upload(new S3.Url(outputFileUrl), outputFile)) {
			logger.warn("output file upload error: {}", outputFileUrl);
			// process failed
			additionalInfo.addProperty("errorMessage", String
					.format("output file upload error: %s", outputFileUrl));
			toProcess.put("additionalInfo", gson.toJson(additionalInfo));
			dao.executeUpdate("goal_scoring.sql.update_failed", toProcess);
			return;
		} else
			logger.debug("file {} uploaded to {}", outputFile.getName(), outputFileUrl);

		// process completed
		for (Map.Entry<String, Number> entry : stats.getAll().entrySet())
			additionalInfo.addProperty(entry.getKey(), entry.getValue());
		final JsonArray repertoriAngloAmericani = new JsonArray();
		for (AngloAmericanRule rule : angloAmericanRulesMaster) {
			final Accumulators angloAmericanStats = angloAmericanStatsMap.get(rule.repertoireCode);
			if (!angloAmericanStats.isEmpty()) {
				final JsonObject repertorioAngloAmericano = new JsonObject();
				repertorioAngloAmericano.addProperty("repertorio", rule.repertoireCode);
				repertorioAngloAmericano.addProperty("descrizioneRepertorio", rule.repertoireName);
				repertorioAngloAmericano.addProperty("tipo", rule
						.isDspExcluded(toProcess.get("licenseeCode")) ? "escluso" : "incluso");
				for (Map.Entry<String, Number> entry : angloAmericanStats.getAll().entrySet())
					repertorioAngloAmericano.addProperty(entry.getKey(), entry.getValue());
				repertoriAngloAmericani.add(repertorioAngloAmericano);
			}
		}
		additionalInfo.add("repertoriAngloAmericani", repertoriAngloAmericani);
		additionalInfo.addProperty("rightSplitDem", BigDecimals
				.setScale(rightSplit.dem).stripTrailingZeros());
		additionalInfo.addProperty("rightSplitDrm", BigDecimals
				.setScale(rightSplit.drm).stripTrailingZeros());
		additionalInfo.addProperty("databaseFormat", databaseFormat);
		additionalInfo.addProperty("databaseOrigin", databaseOrigin);
		additionalInfo.addProperty("startTime", DateFormat
				.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM)
					.format(new Date(startTimeMillis)));
		additionalInfo.addProperty("totalDuration", 
				TextUtils.formatDuration(elapsedTimeMillis));
		logger.debug("additionalInfo {}", additionalInfo);
		toProcess.put("outputFileUrl", outputFileUrl);
		toProcess.put("additionalInfo", gson.toJson(additionalInfo));
		dao.executeUpdate("goal_scoring.sql.update_completed", toProcess);

		// delete temporary files
		inputFile.delete();
		outputFile.delete();

	}

}

