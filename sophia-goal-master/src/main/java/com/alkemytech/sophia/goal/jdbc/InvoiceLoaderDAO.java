package com.alkemytech.sophia.goal.jdbc;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class InvoiceLoaderDAO {

	private final Logger logger = LoggerFactory.getLogger(InvoiceLoaderDAO.class);

	private final Properties configuration;
	private final DataSource dataSource;

	@Inject
	public InvoiceLoaderDAO(@Named("configuration") Properties configuration,
			@Named("MCMDB") DataSource dataSource) {
		super();
		this.configuration = configuration;
		this.dataSource = dataSource;
	}

	public DataSource getDataSource() {
		return dataSource;
	}
	
	public boolean updateRecord(String reportId, String invoiceId, BigDecimal amount, String shareOutId) throws SQLException {
		final String sql = configuration
				.getProperty("invoice_loader.sql.update_record")
				.replace("{reportId}", reportId)
				.replace("{invoiceId}", invoiceId)
				.replace("{amount}", amount.stripTrailingZeros().toPlainString())
				.replace("{shareOutId}", shareOutId);
		logger.debug("executing sql \"{}\"", sql);
		try (final Connection connection = dataSource.getConnection();
				final Statement statement = connection.createStatement()) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			return 1 == statement.executeUpdate(sql);
		}
	}
	
}
