package com.alkemytech.sophia.goal.dsr;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public enum IdentificationType {
	
	iswc, isrc, title_artists, original_iswc, original_isrc, original_title_artists;

}
