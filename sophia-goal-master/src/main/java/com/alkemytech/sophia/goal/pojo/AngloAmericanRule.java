package com.alkemytech.sophia.goal.pojo;

import java.util.Set;

import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class AngloAmericanRule {
	
	public String repertoireCode;		// SONY
	public String repertoireName;		// Sony/ATV
	public Set<String> seIpiCodes;		// 00009576175:00176544252:00467151940:00035943081:00011618128:00241326402:00039793831:00124217804:00198537711:00465397124:00517428649:00161534391
	public Set<String> societies;		// PRS:IMRO:ASCAP:BMI:SESAC:SOCAN:APRA:AMRA:SAMRO
	public boolean notPrefixedSocieties;// !SIAE
	public Set<String> excludedDsps;	// list of CODICE_LICENZIATARIO (or empty)
	public String demAccountCode;		// 06 (or empty)
	public String drmAccountCode;		// 06 (or empty)
	public String demAccountId;			// 000204306 (or empty)
	public String drmAccountId;			// 000204306 (or empty)
	public String demSiaePosition;		// 204306 (or empty)
	public String drmSiaePosition;		// 204306 (or empty)
	
	// WARNING: quando sono valorizzati numero_conto, codice_conto e posizione_siae vanno usate al
	//          posto di quelle del mini-cubo e il campo ipi_number_ad del file 648 va messo a null,
	//          ma solo se non subisce esclusione per dsp

	public boolean isDspExcluded(String dsp) {
		if (null != excludedDsps)
			return excludedDsps.contains(dsp);
		return false;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}

}
