package com.alkemytech.sophia.goal.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.goal.pojo.ToRetryWsBean;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class MuleWsRetryDAO {

	private final Logger logger = LoggerFactory.getLogger(MuleWsRetryDAO.class);

	private final Properties configuration;
	private final DataSource dataSource;
	
	@Inject
	public MuleWsRetryDAO(@Named("configuration") Properties configuration,
			@Named("MCMDB") DataSource dataSource) {
		super();
		this.configuration = configuration;
		this.dataSource = dataSource;
	}

	public DataSource getDataSource() {
		return dataSource;
	}
	
	public List<ToRetryWsBean> selectToRetry() throws SQLException {
		final List<ToRetryWsBean> resultList = new ArrayList<>();
		final String sql = configuration
				.getProperty("mule_ws_retry.sql.select_to_retry");
		logger.debug("executing sql \"{}\"", sql);
		try (final Connection connection = dataSource.getConnection();
				final Statement statement = connection.createStatement();
				final ResultSet resultSet = statement.executeQuery(sql)) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			while (resultSet.next()) {
				final ToRetryWsBean bean = new ToRetryWsBean();
				bean.id = resultSet.getLong("id");
				bean.webServicePayload = resultSet.getString("webServicePayload");
				bean.columnName = resultSet.getString("columnName");
				bean.wsUrl = resultSet.getString("wsUrl");
				resultList.add(bean);
			}
		}
		return resultList;
	}

	public boolean updateCompleted(ToRetryWsBean request) throws SQLException {
		final String sql = configuration
				.getProperty("mule_ws_retry.sql.update_completed")
				.replace("{id}", Long.toString(request.id))
				.replace("{columnName}", request.columnName);
		logger.debug("executing sql \"{}\"", sql);
		try (final Connection connection = dataSource.getConnection();
				final Statement statement = connection.createStatement()) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			return 1 == statement.executeUpdate(sql);
		}
	}
	
}
