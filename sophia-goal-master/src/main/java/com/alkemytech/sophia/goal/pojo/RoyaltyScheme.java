package com.alkemytech.sophia.goal.pojo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.alkemytech.sophia.commons.nosql.NoSqlEntity;
import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class RoyaltyScheme implements NoSqlEntity, Iterable<Royalty> {

	private final List<Royalty> royalties;

	// fields from parquet file
	public final String code;				// 04821523100
//	public final BigDecimal drmTotal;		// 100.0
//	public final BigDecimal demTotal;		// 1.0

	// computed fields
	public boolean publicDomain;			// true | false
	public BigDecimal drmSum;				// 1.0
	public BigDecimal demSum;				// 1.0
	
	// WARNING: i totali DEM e DRM vengono ricalcolati a partire dalle singole quote
	
	public RoyaltyScheme(String code) {
		super();
		this.code = code;
		this.royalties = new ArrayList<>(4);
	}

	public boolean isIrregular() {
		return !royalties.isEmpty() &&
				null != royalties.get(0).irregularityType;
	}
	
	public void add(Royalty royalty) {
		royalties.add(royalty);
	}
	
	public void addAll(Collection<Royalty> royalties) {
		for (Royalty royalty : royalties)
			add(royalty);
	}

	public void addAll(Royalty...royalties) {
		for (Royalty royalty : royalties)
			add(royalty);
	}

	public Royalty get(int index) {
		return royalties.get(index);
	}
	
	public int size() {
		return royalties.size();
	}
	
	public boolean isEmpty() {
		return royalties.isEmpty();
	}
	
	@Override
	public Iterator<Royalty> iterator() {
		return royalties.iterator();
	}

	@Override
	public String getPrimaryKey() {
		return code;
	}

	@Override
	public String getSecondaryKey() {
		return null;
	}

	@Override
	public int hashCode() {
		return 31 + (null == code ? 0 : code.hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (null == obj)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final RoyaltyScheme other = (RoyaltyScheme) obj;
		if (null == code) {
			if (null != other.code)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
