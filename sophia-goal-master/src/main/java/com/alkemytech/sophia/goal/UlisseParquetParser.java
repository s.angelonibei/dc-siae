package com.alkemytech.sophia.goal;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPOutputStream;

import javax.sql.DataSource;

import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.util.Utf8;
import org.apache.commons.codec.binary.Hex;
import org.apache.hadoop.fs.Path;
import org.apache.parquet.Strings;
import org.apache.parquet.avro.AvroParquetReader;
import org.apache.parquet.hadoop.ParquetReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.collecting.SiaeCode;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.sqs.McmdbMessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.util.BigDecimals;
import com.alkemytech.sophia.commons.util.FileUtils;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.goal.config.AngloAmericanConfig;
import com.alkemytech.sophia.goal.config.ExArt18ExclusionConfig;
import com.alkemytech.sophia.goal.jdbc.McmdbDataSource;
import com.alkemytech.sophia.goal.jdbc.UlisseParquetDAO;
import com.alkemytech.sophia.goal.pojo.AngloAmericanRule;
import com.alkemytech.sophia.goal.pojo.DemRoyalty;
import com.alkemytech.sophia.goal.pojo.DrmRoyalty;
import com.alkemytech.sophia.goal.pojo.ExArt18Exclusion;
import com.alkemytech.sophia.goal.pojo.ParquetRoyalty;
import com.alkemytech.sophia.goal.pojo.Royalty;
import com.alkemytech.sophia.goal.pojo.RoyaltyScheme;
import com.alkemytech.sophia.goal.royalty.BdbRoyaltyDatabase;
import com.alkemytech.sophia.goal.royalty.NoSqlRoyaltyDatabase;
import com.alkemytech.sophia.goal.royalty.RoyaltyDatabase;
import com.alkemytech.sophia.goal.royalty.RoyaltyDatabaseMetadata;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class UlisseParquetParser {
	
	private static final Logger logger = LoggerFactory.getLogger(UlisseParquetParser.class);
	
	private static class GuiceModuleExtension extends GuiceModule {

		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.toInstance(new S3(configuration));
			bind(SQS.class)
				.toInstance(new SQS(configuration));
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("MCMDB"))
				.to(McmdbDataSource.class)
				.asEagerSingleton();
			// data access object(s)
			bind(UlisseParquetDAO.class)
				.asEagerSingleton();
			// this
			bind(UlisseParquetParser.class)
				.asEagerSingleton();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			final UlisseParquetParser instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/ulisse-parquet-parser.properties"))
					.getInstance(UlisseParquetParser.class)
					.startup();
			try {
				instance.process();
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	private final Properties configuration;
	private final Charset charset;
	private final S3 s3;
	private final SQS sqs;
	private final Gson gson;
	private final UlisseParquetDAO dao;
	private final String siaeSocietyName;
	private final Set<String> pdAccountCodes;
	private final Set<String> angloAmericanArtistRoles;
	private final Set<String> angloAmericanSubEditorRoles;
	private final boolean angloAmericanCrossDemDrm;
	private final boolean angloAmericanExcludeZero;
	private final boolean removeZeroRoyalties;

	@Inject
	protected UlisseParquetParser(@Named("configuration") Properties configuration,
			@Named("charset") Charset charset, S3 s3, SQS sqs, Gson gson, UlisseParquetDAO dao) {
		super();
		this.configuration = configuration;
		this.charset = charset;
		this.s3 = s3;
		this.sqs = sqs;
		this.gson = gson;
		this.dao = dao;
		this.siaeSocietyName = configuration
				.getProperty("ulisse_parquet_parser.siae_society_name", "SIAE");
		this.pdAccountCodes = new HashSet<>(Arrays.asList(configuration
				.getProperty("ulisse_parquet_parser.pd_account_codes", "10,65,78").split(",")));
		this.angloAmericanArtistRoles = new HashSet<>(Arrays.asList(configuration
				.getProperty("ulisse_parquet_parser.anglo_american.artist_roles", "CO,AO,CA").split(",")));
		this.angloAmericanSubEditorRoles = new HashSet<>(Arrays.asList(configuration
				.getProperty("ulisse_parquet_parser.anglo_american.sub_editor_roles", "SE").split(",")));
		this.angloAmericanCrossDemDrm = "true".equalsIgnoreCase(configuration
				.getProperty("ulisse_parquet_parser.anglo_american.cross_dem_drm", "true"));
		this.angloAmericanExcludeZero = "true".equalsIgnoreCase(configuration
				.getProperty("ulisse_parquet_parser.anglo_american.exclude_zero", "true"));
		this.removeZeroRoyalties = "true".equalsIgnoreCase(configuration
				.getProperty("ulisse_parquet_parser.remove_zero_royalties", "true"));
	}

	public UlisseParquetParser startup() throws SQLException, IOException {
		if ("true".equalsIgnoreCase(configuration.getProperty("ulisse_parquet_parser.locked", "true"))) {
			throw new IllegalStateException("application locked");
		}
		s3.startup();
		sqs.startup();
		return this;
	}

	public UlisseParquetParser shutdown() {
		sqs.shutdown();
		s3.shutdown();
		return this;
	}

	private String getAsString(Object object) {
		if (null == object)
			return null;
		if (object instanceof Utf8) 
			return object.toString();
		return null;
	}

//	private BigDecimal getAsBigDecimal(Object object) {
//		if (null == object)
//			return null;
//		if (object instanceof Double) {
//			final Double number = (Double) object;
//			return new BigDecimal(number.doubleValue());
//		}
//		return null;
//	}

	private List<String> getAsStringList(Object object) {
		if (null == object)
			return null;
		if (object instanceof List) {
			final List<?> list = (List<?>) object;
			final List<String> result = new ArrayList<>(list.size());
			for (Object item : list) {
				if (null == item) {
					result.add(null);
				} else if (item instanceof GenericData.Record) {
					final GenericData.Record record = (GenericData.Record) item;
					final String element = getAsString(record.get(0));
					result.add(Strings.isNullOrEmpty(element) ? null : element);
				}
			}
			return result;
		}
		return null;
	}

	private List<Integer> getAsIntegerList(Object object) {
		if (null == object)
			return null;
		if (object instanceof List) {
			final List<?> list = (List<?>) object;
			final List<Integer> result = new ArrayList<>(list.size());
			for (Object item : list) {
				if (null == item) {
					result.add(null);
				} else if (item instanceof GenericData.Record) {
					final GenericData.Record record = (GenericData.Record) item;
					final String element = getAsString(record.get(0));
					result.add(Strings.isNullOrEmpty(element) ?
							null : Integer.parseInt(element));
				}
			}
			return result;
		}
		return null;
	}

	private List<BigDecimal> getAsBigDecimalList(Object object) {
		if (null == object)
			return null;
		if (object instanceof List) {
			final List<?> list = (List<?>) object;
			final List<BigDecimal> result = new ArrayList<>(list.size());
			for (Object item : list) {
				if (null == item) {
					result.add(null);
				} else if (item instanceof GenericData.Record) {
					final GenericData.Record record = (GenericData.Record) item;
					final String element = getAsString(record.get(0));
					result.add(Strings.isNullOrEmpty(element) ? null : BigDecimals
							.setScale(new BigDecimal(element)).stripTrailingZeros());
				}
			}
			return result;
		}
		return null;
	}

	private void deleteS3FolderContents(String s3FolderUrl) {
		if (!s3FolderUrl.endsWith("/"))
			s3FolderUrl += '/';
		final List<S3ObjectSummary> objects = s3
				.listObjects(new S3.Url(s3FolderUrl));
		for (S3ObjectSummary object : objects) {
			final String s3FileUrl = new StringBuilder()
					.append("s3://")
					.append(object.getBucketName())
					.append('/')
					.append(object.getKey())
					.toString();
			if (s3.delete(new S3.Url(s3FileUrl)))
				logger.debug("file deleted {}", s3FileUrl);
			else
				logger.error("error deleting file {}", s3FileUrl);
		}
	}

	private String formatCsvRecord(RoyaltyScheme scheme, String delimiter, String subDelimiter, String endOfLine) {
		final StringBuilder line = new StringBuilder()
			.append(scheme.code);
		// ipi code
		String separator = delimiter;
		for (Royalty royalty : scheme) {
			line.append(separator)
				.append(null == royalty.ipiCode ? "" : royalty.ipiCode);
			separator = subDelimiter;
		}
		// siae position
		separator = delimiter;
		for (Royalty royalty : scheme) {
			line.append(separator)
				.append(null == royalty.siaePosition ? "" : royalty.siaePosition);
			separator = subDelimiter;
		}
		// role
		separator = delimiter;
		for (Royalty royalty : scheme) {
			line.append(separator)
				.append(null == royalty.role ? "" : royalty.role);
			separator = subDelimiter;
		}
		// dem denominator
		separator = delimiter;
		for (Royalty royalty : scheme) {
			line.append(separator);
			if (royalty.isDem()) {
				final DemRoyalty dem = (DemRoyalty) royalty;
				line.append(null == dem.denominator ? "" : dem.denominator
							.stripTrailingZeros().toPlainString());
			} else {
				line.append("0");
			}
			separator = subDelimiter;
		}
		// dem numerator
		separator = delimiter;
		for (Royalty royalty : scheme) {
			line.append(separator);
			if (royalty.isDem()) {
				final DemRoyalty dem = (DemRoyalty) royalty;
				line.append(null == dem.numerator ? "" : dem.numerator
							.stripTrailingZeros().toPlainString());
			} else {
				line.append("0");
			}
			separator = subDelimiter;
		}
		// royalty type
		separator = delimiter;
		for (Royalty royalty : scheme) {
			line.append(separator)
				.append(royalty.isDem() ? "DEM" : royalty.isDrm() ? "DRM" : "");
			separator = subDelimiter;
		}
		// drm percentage
		separator = delimiter;
		for (Royalty royalty : scheme) {
			line.append(separator);
			if (royalty.isDrm()) {
				final DrmRoyalty drm = (DrmRoyalty) royalty;
				line.append(null == drm.percentage ? "" : drm.percentage
							.stripTrailingZeros().toPlainString());
			} else {
				line.append("0");
			}
		}
		// name
//		separator = delimiter;
//		for (Royalty royalty : scheme) {
//			line.append(separator)
//				.append(null == royalty.name ? "" : royalty.name);
//			separator = subDelimiter;
//		}
		// account code
		separator = delimiter;
		for (Royalty royalty : scheme) {
			line.append(separator)
				.append(null == royalty.accountCode ? "" : royalty.accountCode);
			separator = subDelimiter;
		}
		// account id
		separator = delimiter;
		for (Royalty royalty : scheme) {
			line.append(separator)
				.append(null == royalty.accountId ? "" : royalty.accountId);
			separator = subDelimiter;
		}
		// society
		separator = delimiter;
		for (Royalty royalty : scheme) {
			line.append(separator)
				.append(null == royalty.society ? "" : royalty.society);
			separator = subDelimiter;
		}
		// irregularity type
		separator = delimiter;
		for (Royalty royalty : scheme) {
			line.append(separator)
				.append(null == royalty.irregularityType ? "" : royalty.irregularityType);
			separator = subDelimiter;
		}
		// public domain flag
		separator = delimiter;
		for (Royalty royalty : scheme) {
			line.append(separator)
				.append(royalty.publicDomain ? '1' : '0');
			separator = subDelimiter;
		}
		// siae flag
		separator = delimiter;
		for (Royalty royalty : scheme) {
			line.append(separator)
				.append(royalty.siae ? '1' : '0');
			separator = subDelimiter;
		}
		// not associated flag
		separator = delimiter;
		for (Royalty royalty : scheme) {
			line.append(separator)
				.append(royalty.notAssociated ? '1' : '0');
			separator = subDelimiter;
		}
		// special account flag
		separator = delimiter;
		for (Royalty royalty : scheme) {
			line.append(separator)
				.append(royalty.specialAccount ? '1' : '0');
			separator = subDelimiter;
		}
		// anglo-american repertoire
		separator = delimiter;
		for (Royalty royalty : scheme) {
			line.append(separator)
				.append(null == royalty.angloAmericanRepertoire ? "" : royalty.angloAmericanRepertoire);
			separator = subDelimiter;
		}
		// ex.art.18 exclusion flag
		separator = delimiter;
		for (Royalty royalty : scheme) {
			line.append(separator);
			if (royalty.isDem()) {
				final DemRoyalty dem = (DemRoyalty) royalty;
				line.append(dem.exArt18Excluded ? '1' : '0');
			} else {
				line.append("0");
			}
			separator = subDelimiter;
		}
		// computed rate
		separator = delimiter;
		for (Royalty royalty : scheme) {
			line.append(separator)
				.append(royalty.rate.stripTrailingZeros()
						.toPlainString());
			separator = subDelimiter;
		}
		return line
			.append(delimiter)
			.append(null == scheme.drmSum ? "" : scheme
					.drmSum.stripTrailingZeros().toPlainString())
			.append(delimiter)
			.append(null == scheme.demSum ? "" : scheme
					.demSum.stripTrailingZeros().toPlainString())
			.append(endOfLine)
			.toString();
	}

	private boolean isPublicDomain(Royalty royalty) {
		return pdAccountCodes.contains(royalty.accountCode);
	}

	private boolean isSiae(Royalty royalty) {
		return siaeSocietyName.equalsIgnoreCase(royalty.society);
	}

	private boolean isNotAssociated(Royalty royalty) {
		return !Strings.isNullOrEmpty(royalty.role) &&
				Strings.isNullOrEmpty(royalty.society);
	}

	private boolean isSpecialAccount(Royalty royalty) {
		return Strings.isNullOrEmpty(royalty.role) &&
				Strings.isNullOrEmpty(royalty.ipiCode);
	}

	private boolean isExcludedExArt18(Set<String> noExArt18Societies, Set<String> noExArt18AngloAmericans, Royalty royalty) {
		// society exclusion(s)
		if (noExArt18Societies.contains(royalty.society))
			return true;
		// anglo-american exclusion(s)
		if (noExArt18AngloAmericans.contains(royalty.angloAmericanRepertoire))
			return true;
		return false;
	}

	private Map<Royalty, AngloAmericanRule> getAngloAmericanSubEditors(List<AngloAmericanRule> angloAmericanRules, List<Royalty> royalties, RoyaltyScheme scheme) {
		Map<Royalty, AngloAmericanRule> angloAmericanSubEditors = null;
		for (AngloAmericanRule rule : angloAmericanRules) {
			// loop on sub-editor(s)
			for (Royalty subEditor : scheme) {
				if (angloAmericanSubEditorRoles.contains(subEditor.role)) {
					if (rule.seIpiCodes.contains(subEditor.ipiCode)) {
						// search artist(s)
						for (Royalty artist : royalties) {
							if (angloAmericanArtistRoles.contains(artist.role) &&
									(angloAmericanCrossDemDrm ||
										(subEditor.isDem() && artist.isDem()) ||
											(subEditor.isDrm() && artist.isDrm()))) {
								final boolean found = rule.societies.contains(artist.society);
								if ((rule.notPrefixedSocieties && !found) ||
										(!rule.notPrefixedSocieties && found)) {
									if (null == angloAmericanSubEditors)
										angloAmericanSubEditors = new HashMap<>();
									angloAmericanSubEditors.put(subEditor, rule);
									break;
								}
							}
						}
					}
				}
			}
		}
		return angloAmericanSubEditors;
	}
	
	private void analyzeRoyaltyScheme(List<AngloAmericanRule> angloAmericanRules, Set<String> noExArt18Societies, Set<String> noExArt18AngloAmericans, List<Royalty> royalties, RoyaltyScheme scheme) {
		final Map<Royalty, AngloAmericanRule> angloAmericanSubEditors = 
				getAngloAmericanSubEditors(angloAmericanRules, royalties, scheme);
		scheme.publicDomain = true;
		scheme.demSum = BigDecimal.ZERO;
		scheme.drmSum = BigDecimal.ZERO;
		for (Royalty royalty : scheme) {
			// DEM
			if (royalty.isDem()) {
				if (!BigDecimals.isAlmostZero(royalty.rate))
					scheme.demSum = scheme.demSum.add(royalty.rate);
				// public domain
				if (!(royalty.publicDomain = isPublicDomain(royalty)))
					scheme.publicDomain = false;
				// SIAE
				royalty.siae = isSiae(royalty);
				// not associated
				royalty.notAssociated = isNotAssociated(royalty);
				// special account
				royalty.specialAccount = isSpecialAccount(royalty);
				// anglo-american sub-editor(s)
				if (null != angloAmericanSubEditors) {
					final AngloAmericanRule rule = angloAmericanSubEditors.get(royalty);
					if (null != rule)
						royalty.angloAmericanRepertoire = rule.repertoireCode;
				}
				// ex art. 18 exclusions
				// WARNING: must be called after initializing angloAmericanRepertoire
				final DemRoyalty dem = (DemRoyalty) royalty;
				dem.exArt18Excluded = isExcludedExArt18(noExArt18Societies,
						noExArt18AngloAmericans, royalty);						
			}
			// DRM
			else if (royalty.isDrm()) {
				if (!BigDecimals.isAlmostZero(royalty.rate))
					scheme.drmSum = scheme.drmSum.add(royalty.rate);
				// public domain
				if (!(royalty.publicDomain = isPublicDomain(royalty)))
					scheme.publicDomain = false;
				// SIAE
				royalty.siae = isSiae(royalty);
				// not associated
				royalty.notAssociated = isNotAssociated(royalty);
				// special account
				royalty.specialAccount = isSpecialAccount(royalty);
				// anglo-american sub-editor(s)
				if (null != angloAmericanSubEditors) {
					final AngloAmericanRule rule = angloAmericanSubEditors.get(royalty);
					if (null != rule)
						royalty.angloAmericanRepertoire = rule.repertoireCode;
				}
			}
			scheme.demSum = BigDecimals.isAlmostZero(scheme.demSum) ?
					BigDecimal.ZERO : BigDecimals.setScale(scheme.demSum).stripTrailingZeros();
			scheme.drmSum = BigDecimals.isAlmostZero(scheme.drmSum) ?
					BigDecimal.ZERO : BigDecimals.setScale(scheme.drmSum).stripTrailingZeros();
		}
	}

	private UlisseParquetParser process() throws Exception {
		
		final int bindPort = Integer.parseInt(configuration.getProperty("ulisse_parquet_parser.bind_port",
				configuration.getProperty("default.bind_port", "0")));

		// bind lock tcp port
		try (ServerSocket socket = new ServerSocket(bindPort)) {
			logger.debug("socket bound to {}", socket.getLocalSocketAddress());
			// sqs message pump
			final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "ulisse_parquet_parser.sqs");
			sqsMessagePump.pollingLoop(new McmdbMessageDeduplicator(dao.getDataSource()), new SqsMessagePump.Consumer() {

				private JsonObject output;
				private JsonObject error;

				@Override
				public JsonObject getStartedMessagePayload(JsonObject message) {
					output = null;
					error = null;
					return SqsMessageHelper.formatContext();
				}

				@Override
				public boolean consumeMessage(JsonObject message) {
					try {
						output = new JsonObject();
						processMessage(message, output);
						error = null;
						return true;
					} catch (Exception e) {
						output = null;
						error = SqsMessageHelper.formatError(e);
						return false;
					}
				}

				@Override
				public JsonObject getCompletedMessagePayload(JsonObject message) {
					return output;
				}

				@Override
				public JsonObject getFailedMessagePayload(JsonObject message) {
					return error;
				}

			});
		}

		return this;
	}

	private void processMessage(JsonObject input, JsonObject output) throws Exception {

//		{
//		  "body": {
//		    "year": "2017",
//		    "month": "01",
//		    "force": false
//		  },
//		  "header": {
//		    "queue": "debug_to_process_schema_riparto_ml",
//		    "timestamp": "2018-05-10T00:00:00.000000+02:00",
//		    "uuid": "7f36a2bb-0637-4d4d-9990-4ed9644adefc",
//		    "sender": "ml-ripartizione"
//		  }
//		}

		final long startTimeMillis = System.currentTimeMillis();
	
		// parse input json message
		final JsonObject inputBody = input.getAsJsonObject("body");
		final String year = GsonUtils.getAsString(inputBody, "year");
		final String month = GsonUtils.getAsString(inputBody, "month");
		final boolean force = "true".equals(GsonUtils
				.getAsString(inputBody, "force", "false"));
		if (Strings.isNullOrEmpty(year) || Strings.isNullOrEmpty(month))
			throw new IllegalArgumentException("invalid input message");
		
		final int threadCount = Integer.parseInt(configuration
				.getProperty("ulisse_parquet_parser.thread_count", "1"));
		final File homeFolder = new File(configuration
				.getProperty("default.home_folder"));
		
		final File databaseLocalFolder = new File(configuration
				.getProperty("ulisse_parquet_parser.royalty_database.local_folder"));
		final String databaseFormat = configuration
				.getProperty("ulisse_parquet_parser.royalty_database.format",
						NoSqlRoyaltyDatabase.class.getName());
		final boolean databaseDeferredWrite = "true".equalsIgnoreCase(configuration
				.getProperty("ulisse_parquet_parser.royalty_database.deferred_write", "false"));
		final int databasePageSize = TextUtils.parseIntSize(configuration
				.getProperty("ulisse_parquet_parser.royalty_database.page_size", "32Mb"));

		final File successFile = new File(homeFolder,configuration
				.getProperty("ulisse_parquet_parser.success_filename", "_SUCCESS"));
		final File latestFile = new File(homeFolder,configuration
				.getProperty("ulisse_parquet_parser.latest_filename", "_LATEST"));
		final String inputS3FolderUrl = configuration
				.getProperty("ulisse_parquet_parser.input.s3_folder_url")
				.replace("{year}", year)
				.replace("{month}", month);
		final String nosqlS3FolderUrl = configuration
				.getProperty("ulisse_parquet_parser.royalty_database.s3_folder_url")
				.replace("{year}", year)
				.replace("{month}", month);
		final Pattern filterRegex = Pattern.compile(configuration
				.getProperty("ulisse_parquet_parser.filter_regex"));
		final String outputS3FolderUrl = configuration
				.getProperty("ulisse_parquet_parser.output.s3_folder_url")
				.replace("{year}", year)
				.replace("{month}", month);
		final String delimiter = configuration
				.getProperty("ulisse_parquet_parser.output.delimiter").trim();
		final String subDelimiter = configuration
				.getProperty("ulisse_parquet_parser.output.sub_delimiter").trim();

		final int codeIndex = Integer.parseInt(configuration
				.getProperty("ulisse_parquet_parser.column.codice"));
		final int ipiCodesIndex = Integer.parseInt(configuration
				.getProperty("ulisse_parquet_parser.column.vettore_codice_ipi"));
		final int siaePositionsIndex = Integer.parseInt(configuration
				.getProperty("ulisse_parquet_parser.column.vettore_posizione_siae"));
		final int demDenominatorsIndex = Integer.parseInt(configuration
				.getProperty("ulisse_parquet_parser.column.vettore_quota_denominatore"));
		final int demNumeratorsIndex = Integer.parseInt(configuration
				.getProperty("ulisse_parquet_parser.column.vettore_quota_numeratore"));
		final int repertoireIdsIndex = Integer.parseInt(configuration
				.getProperty("ulisse_parquet_parser.column.vettore_id_repertorio"));
		final int roleTypesIndex = Integer.parseInt(configuration
				.getProperty("ulisse_parquet_parser.column.vettore_tipo_qualifica"));
		final int rightTypesIndex = Integer.parseInt(configuration
				.getProperty("ulisse_parquet_parser.column.vettore_tipo_quota"));
		final int drmPercentagesIndex = Integer.parseInt(configuration
				.getProperty("ulisse_parquet_parser.column.vettore_quota_percentuale"));
		final int namesIndex = Integer.parseInt(configuration
				.getProperty("ulisse_parquet_parser.column.vettore_cognome"));
		final int accountCodesIndex = Integer.parseInt(configuration
				.getProperty("ulisse_parquet_parser.column.vettore_codice_conto"));
		final int accountIdsIndex = Integer.parseInt(configuration
				.getProperty("ulisse_parquet_parser.column.vettore_id_conto"));
		final int sharesIndex = Integer.parseInt(configuration
				.getProperty("ulisse_parquet_parser.column.vettore_shr"));
		final int societiesIndex = Integer.parseInt(configuration
				.getProperty("ulisse_parquet_parser.column.vettore_societa"));
		final int irregularityTypesIndex = Integer.parseInt(configuration
				.getProperty("ulisse_parquet_parser.column.vettore_tipo_irregolarita"));
//		final int drmTotalIndex = Integer.parseInt(configuration
//				.getProperty("ulisse_parquet_parser.column.total_drm"));
//		final int demTotalIndex = Integer.parseInt(configuration
//				.getProperty("ulisse_parquet_parser.column.total_dem"));

		final HeartBeat heartbeat = HeartBeat.constant("parquet",
				Integer.parseInt(configuration.getProperty("ulisse_parquet_parser.heartbeat",
						configuration.getProperty("default.heartbeat", "1000"))));		

		// check nosql _LATEST file & retrieve latest version number
		final String latestS3FileUrl = new StringBuilder()
				.append(nosqlS3FolderUrl)
				.append('/')
				.append(latestFile.getName())
				.toString();
		final String latestVersion = s3
				.getObjectContents(new S3.Url(latestS3FileUrl), charset);
		logger.debug("database latestVersion {}", latestVersion);
		// return immediately if a valid version already exists and no force flag
		if (!force && !Strings.isNullOrEmpty(latestVersion)) {
			// output
			output.addProperty("period", year + month);
			output.addProperty("version", latestVersion);
			output.addProperty("nosqlS3FolderUrl", new StringBuilder()
					.append(nosqlS3FolderUrl).append('/').append(latestVersion).toString());
			output.addProperty("startTime", DateFormat
					.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM)
						.format(new Date(startTimeMillis)));
			output.addProperty("totalDuration", TextUtils
					.formatDuration(System.currentTimeMillis() - startTimeMillis));
			output.addProperty("additionalInfo", String
					.format("version %s for period %s%s already exists", latestVersion, year, month));
			logger.info("version {} for period {}{} already exists", latestVersion, year, month);
			logger.debug("message processed in {}",
					TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
			return;
		}
		// increment latest database version
		final String databaseVersion;
		if (Strings.isNullOrEmpty(latestVersion)) {
			databaseVersion = "1";
		} else {
			databaseVersion = Integer.toString(1 +
					Integer.parseInt(latestVersion));
		}
		logger.debug("databaseVersion {}", databaseVersion);

		// anglo-american rule(s)
		final AngloAmericanConfig angloAmericanConfig = new AngloAmericanConfig(configuration,
				"ulisse_parquet_parser.anglo_american", s3);
		final List<AngloAmericanRule> angloAmericanRulesTemplate = angloAmericanConfig
				.getConfig(dao.selectAngloAmericanConfig(year, month));

		// ex-art-18-exclusion rule(s)
		final ExArt18ExclusionConfig exArt18ExclusionConfig = new ExArt18ExclusionConfig(configuration,
				"ulisse_parquet_parser.ex_art_18", s3);
		final Set<String> noExArt18SocietiesTemplate = new HashSet<String>();
		final Set<String> noExArt18AngloAmericansTemplate = new HashSet<String>();
		final List<ExArt18Exclusion> exArt18Exclusions = exArt18ExclusionConfig
				.getConfig(dao.selectExArt18Config(year, month));
		for (ExArt18Exclusion exclusion : exArt18Exclusions) {
			if (exclusion.isSociety())
				noExArt18SocietiesTemplate.add(exclusion.value);
			if (exclusion.isAngloAmerican())
				noExArt18AngloAmericansTemplate.add(exclusion.value);
		}
		
		// delete dirty files in output folder
		logger.debug("deleting existing files in {}", outputS3FolderUrl);
		deleteS3FolderContents(outputS3FolderUrl);		

		// input file(s) fingerprint
		String inputFingerprint;
		final MessageDigest inputDigest = MessageDigest.getInstance("MD5");

		// stat(s)
		final AtomicLong inputRows = new AtomicLong(0L);
		final AtomicLong outputRows = new AtomicLong(0L);
		final AtomicLong discardedRows = new AtomicLong(0L);
		final AtomicInteger downloadedFiles = new AtomicInteger(0);
		final AtomicInteger uploadedFiles = new AtomicInteger(0);
		final AtomicInteger localDatabaseFiles = new AtomicInteger(0);
		final AtomicInteger uploadedDatabaseFiles = new AtomicInteger(0);
		final AtomicLong inputSize = new AtomicLong(0L);
		final AtomicLong outputSize = new AtomicLong(0L);
		final AtomicLong databaseSize = new AtomicLong(0L);

		// open/create local berkeley-db database
		logger.debug("opening royalty database");
		try (final RoyaltyDatabase database = BdbRoyaltyDatabase.class.getName().equals(databaseFormat) ?
				new BdbRoyaltyDatabase(databaseLocalFolder, false, databaseDeferredWrite, charset, true) :
					NoSqlRoyaltyDatabase.class.getName().equals(databaseFormat) ?
						new NoSqlRoyaltyDatabase(databaseLocalFolder, false, databasePageSize, -1, charset, true) : null) {

			if (null == database)
				throw new IllegalStateException(String
						.format("unknown royalty database type: %s", databaseFormat));

			logger.debug("importing parquet file(s) from {}", inputS3FolderUrl);

			// set database metadata
			database.getMetadata()
				.setProperty(RoyaltyDatabaseMetadata.VERSION, databaseVersion)
				.setProperty(RoyaltyDatabaseMetadata.FORMAT, databaseFormat)
				.setProperty(RoyaltyDatabaseMetadata.PERIOD, year + month)
				.setProperty(RoyaltyDatabaseMetadata.ORIGIN, inputS3FolderUrl)
				.setProperty(RoyaltyDatabaseMetadata.ANGLOAMERICAN,
						gson.toJson(angloAmericanRulesTemplate))
				.setProperty(RoyaltyDatabaseMetadata.NOEXART18,
						gson.toJson(exArt18Exclusions))
				.commit();

			// build s3 file(s) list
			final List<S3ObjectSummary> objects = s3.listObjects(new S3.Url(inputS3FolderUrl));
			final List<String> s3FileUrls = Collections
					.synchronizedList(new ArrayList<String>(objects.size()));
			for (S3ObjectSummary object : objects) {
				final String s3FileUrl = new StringBuilder()
						.append("s3://")
						.append(object.getBucketName())
						.append('/')
						.append(object.getKey())
						.toString();
				if (filterRegex.matcher(s3FileUrl).matches()) {
					s3FileUrls.add(s3FileUrl);
				}
			}

			// sort s3 file(s) list
			Collections.sort(s3FileUrls);

			// process s3 file(s)
			final Iterator<String> s3FileUrlsIterator = s3FileUrls.iterator();
			final Object monitor = new Object();
			final ExecutorService executorService = Executors.newCachedThreadPool();
			final AtomicInteger runningThreads = new AtomicInteger(0);
			final AtomicReference<Exception> threadException = new AtomicReference<>(null);
			while (runningThreads.get() < threadCount) {
				logger.debug("starting thread {}", runningThreads.incrementAndGet());
				executorService.execute(new Runnable() {

					@Override
					public void run() {
						try {
							
							// WARNING: duplicate collections for thread safety
							final List<AngloAmericanRule> angloAmericanRules = new ArrayList<>(angloAmericanRulesTemplate);
							final Set<String> noExArt18Societies = new HashSet<String>(noExArt18SocietiesTemplate);
							final Set<String> noExArt18AngloAmericans = new HashSet<String>(noExArt18AngloAmericansTemplate);
							final List<Royalty> royalties = new ArrayList<>();

							while (null == threadException.get()) {

								// next parquet file
								String s3FileUrl;
								synchronized (s3FileUrls) {
									if (!s3FileUrlsIterator.hasNext())
										return;
									s3FileUrl = s3FileUrlsIterator.next();
								}
																
								logger.debug("downloading file {}", s3FileUrl);
								final File inputFile = new File(homeFolder, UUID.randomUUID().toString());
								inputFile.deleteOnExit();
								if (s3.download(new S3.Url(s3FileUrl), inputFile)) {
									downloadedFiles.incrementAndGet();

									// compute input file(s) size
									logger.debug("downloaded file size {}", TextUtils.formatSize(inputFile.length()));
									inputSize.addAndGet(inputFile.length());
									
									// compute file hash
									logger.debug("computing hash of file {}", inputFile.getName());
									synchronized (inputDigest) {
										FileUtils.updateHash(inputFile, inputDigest);										
									}
									logger.debug("hash computed for file {}", inputFile.getName());
									
									logger.debug("processing file {}", s3FileUrl);
									final File outputFile = new File(homeFolder, UUID.randomUUID().toString());
									outputFile.deleteOnExit();
									final Path path = new Path(inputFile.toURI());
									try (final ParquetReader<GenericRecord> reader = AvroParquetReader.<GenericRecord>builder(path).build();
											final FileOutputStream out = new FileOutputStream(outputFile);
											final GZIPOutputStream gzip = new GZIPOutputStream(out, 65536);
											final Writer writer = new OutputStreamWriter(gzip, charset);
											final BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
													
										for (GenericRecord record; null != (record = reader.read()); ) {
//											logger.debug("record {}", record);
											inputRows.incrementAndGet();

											final String code = SiaeCode.normalize(getAsString(record.get(codeIndex)));
											final List<String> ipiCodes = getAsStringList(record.get(ipiCodesIndex));
											final List<String> siaePositions = getAsStringList(record.get(siaePositionsIndex));
											final List<BigDecimal> demDenominators = getAsBigDecimalList(record.get(demDenominatorsIndex));
											final List<BigDecimal> demNumerators = getAsBigDecimalList(record.get(demNumeratorsIndex));
											final List<String> repertoireIds = getAsStringList(record.get(repertoireIdsIndex));
											final List<String> roleTypes = getAsStringList(record.get(roleTypesIndex));
											final List<String> rightTypes = getAsStringList(record.get(rightTypesIndex));
											final List<BigDecimal> drmPercentages = getAsBigDecimalList(record.get(drmPercentagesIndex));
											final List<String> names = getAsStringList(record.get(namesIndex));
											final List<String> accountCodes = getAsStringList(record.get(accountCodesIndex));
											final List<String> accountIds = getAsStringList(record.get(accountIdsIndex));
											final List<BigDecimal> shares = getAsBigDecimalList(record.get(sharesIndex));
											final List<String> societies = getAsStringList(record.get(societiesIndex));
											final List<Integer> irregularityTypes = getAsIntegerList(record.get(irregularityTypesIndex));
//											final BigDecimal drmTotal = getAsBigDecimal(record.get(drmTotalIndex));
//											final BigDecimal demTotal = getAsBigDecimal(record.get(demTotalIndex));
											
											// check array lengths
											final int size = ipiCodes.size();
											if (size != siaePositions.size() ||
													size != demDenominators.size() ||
													size != demNumerators.size() ||
													size != repertoireIds.size() ||
													size != roleTypes.size() ||
													size != rightTypes.size() ||
													size != drmPercentages.size() ||
													size != names.size() ||
													size != accountCodes.size() ||
													size != accountIds.size() ||
													size != shares.size() ||
													size != societies.size() ||
													size != irregularityTypes.size()) {
												logger.error("invalid record (non uniform vectors size) {}", record);
												discardedRows.incrementAndGet();
												continue;
											}
															
											royalties.clear();
											final RoyaltyScheme scheme = new RoyaltyScheme(code);
											for (int index = 0; index < size; index ++) {
												final ParquetRoyalty parquetRoyalty = new ParquetRoyalty(ipiCodes.get(index),
														siaePositions.get(index),
														demDenominators.get(index),
														demNumerators.get(index),
														repertoireIds.get(index),
														roleTypes.get(index),
														rightTypes.get(index),
														drmPercentages.get(index),
														names.get(index),
														accountCodes.get(index),
														accountIds.get(index),
														shares.get(index),
														societies.get(index),
														irregularityTypes.get(index));
												final Royalty royalty = parquetRoyalty.toRoyalty();
												if (royalty.isZero()) {
													if (!angloAmericanExcludeZero)
														royalties.add(royalty);
													if (!removeZeroRoyalties)
														scheme.add(royalty);
												} else {
													royalties.add(royalty);
													scheme.add(royalty);
												}
											}
											
											// compute mini-cube data
											analyzeRoyaltyScheme(angloAmericanRules,
													noExArt18Societies, noExArt18AngloAmericans,
														royalties, scheme);
//											logger.debug("scheme {}", scheme);
											
											// discard owner(s) with zero royalties
											if (scheme.isEmpty()) {
												logger.error("invalid record (all zero) {}", record);
												discardedRows.incrementAndGet();
												continue;
											}
											
											// insert into royalty database
											if (!database.putScheme(scheme)) {
												logger.error("database upsert error {}", record);
												discardedRows.incrementAndGet();
												continue;
											}												

											// write to output
											final String line = formatCsvRecord(scheme, delimiter, subDelimiter, "\n");
											bufferedWriter.write(line);
											
											outputRows.incrementAndGet();

											heartbeat.pump();
										}
									
									}

									// compute output size
									logger.debug("output file size {}", TextUtils.formatSize(outputFile.length()));
									outputSize.addAndGet(outputFile.length());

									// upload mini-cube file to S3
									final Matcher matcher = filterRegex.matcher(s3FileUrl);
									if (matcher.matches()) {
										s3FileUrl = new StringBuilder()
											.append(outputS3FolderUrl)
											.append('/')
											.append(matcher.group(1)) // filename without extension
											.append(".csv.gz")
											.toString();
										logger.debug("uploading output to {}", s3FileUrl);
										if (!s3.upload(new S3.Url(s3FileUrl), outputFile)) {
											logger.error("error uploading file {} to {}", outputFile.getName(), s3FileUrl);
											throw new IOException(String
													.format("error uploading file %s to %s", outputFile.getName(), s3FileUrl));
										} else {
											logger.debug("file {} uploaded to {}", outputFile.getName(), s3FileUrl);
											uploadedFiles.incrementAndGet();
										}
									} else {
										logger.error("unable to extract filename from url: {}", s3FileUrl);
										throw new IllegalStateException(String
												.format("unable to extract filename from url: %s", s3FileUrl));
									}

									// delete temporary files
									inputFile.delete();
									outputFile.delete();
								}

							}
						} catch (Exception e) {
							logger.error("run", e);
							threadException.compareAndSet(null, e);
						} finally {
							logger.debug("thread {} finished", runningThreads.getAndDecrement());
							synchronized (monitor) {
								monitor.notify();
							}
						}
					}
					
				});
			}

			// join running thread(s)
			while (runningThreads.get() > 0) {
				synchronized (monitor) {
					monitor.wait(1000L);
				}					
			}
			
			// re-throw thread exception
			if (null != threadException.get())
				throw threadException.getAndSet(null);

			// WARNING: database must be closed before files manipulation
			logger.debug("closing royalty database...");
			database.close();
			logger.debug("royalty database closed");
						
			// input file(s) fingerprint
			inputFingerprint = Hex.encodeHexString(inputDigest.digest());
			
			// update database metadata
			database.getMetadata()
				.setProperty(RoyaltyDatabaseMetadata.FINGERPRINT,
						inputFingerprint)
				.commit();
			
			// upload nosql files to s3 folder
			if ("true".equalsIgnoreCase(configuration
					.getProperty("ulisse_parquet_parser.royalty_database.upload_to_s3", "true"))) {

				final String s3FolderUrl = new StringBuilder()
						.append(nosqlS3FolderUrl)
						.append('/')
						.append(databaseVersion)
						.toString();

				// delete all files already in s3 folder
				logger.debug("deleting dirty file(s) in {}", s3FolderUrl);
				deleteS3FolderContents(s3FolderUrl);		
				
				// upload to s3 folder
				logger.debug("uploading nosql files to {}", s3FolderUrl);
				final ConcurrentLinkedDeque<File> files = new ConcurrentLinkedDeque<>();
				files.addAll(Arrays.asList(databaseLocalFolder.listFiles()));
				for (final Iterator<File> iterator = files.iterator(); iterator.hasNext(); ) {
					final File file = iterator.next();
					if (file.isDirectory()) {
						iterator.remove();
						files.addAll(Arrays.asList(file.listFiles()));
					}
				}
				localDatabaseFiles.set(files.size());
				final Iterator<File> iterator = files.iterator();
				
				// start upload thread(s)
				runningThreads.set(0);
				while (runningThreads.get() < threadCount) {
					logger.debug("starting thread {}", runningThreads.incrementAndGet());
					executorService.execute(new Runnable() {
						
						@Override
						public void run() {
							try {
								while (null == threadException.get()) {
									final File file;
									synchronized (iterator) {
										if (!iterator.hasNext())
											return;
										file = iterator.next();
									}
									// compute database size on disk
									databaseSize.addAndGet(file.length());
									// upload database file
									String relativePath = file.getAbsolutePath()
											.substring(databaseLocalFolder
													.getAbsolutePath().length());
									while (relativePath.startsWith("/"))
										relativePath = relativePath.substring(1);
									final String s3FileUrl = new StringBuilder()
											.append(s3FolderUrl)
											.append('/')
											.append(relativePath)
											.toString(); 
									if (!s3.upload(new S3.Url(s3FileUrl), file)) {
										logger.error("error uploading file {} to {}", relativePath, s3FileUrl);
										throw new IOException(String
												.format("error uploading file %s to %s", relativePath, s3FileUrl));
									} else {
										logger.debug("file {} uploaded to {}", relativePath, s3FileUrl);
										uploadedDatabaseFiles.incrementAndGet();
									}
								}
							} catch (Exception e) {
								logger.error("run", e);
								threadException.compareAndSet(null, e);
							} finally {
								logger.debug("thread {} finished", runningThreads.getAndDecrement());
								synchronized (monitor) {
									monitor.notify();
								}
							}
						}
					});
				}

				// join upload thread(s)
				while (runningThreads.get() > 0) {
					synchronized (monitor) {
						monitor.wait(1000L);
					}					
				}

				// re-throw thread exception
				if (null != threadException.get())
					throw threadException.getAndSet(null);

				// create & upload nosql latest file
				try (final FileWriter writer = new FileWriter(latestFile)) {
					writer.write(databaseVersion);
				}
				final String s3FileUrl = new StringBuilder()
						.append(nosqlS3FolderUrl)
						.append('/')
						.append(latestFile.getName())
						.toString();
				latestFile.deleteOnExit();
				if (!s3.upload(new S3.Url(s3FileUrl), latestFile)) {
					logger.error("error uploading file {} to {}", latestFile.getName(), s3FileUrl);
					throw new IOException(String
							.format("error uploading file %s to %s", latestFile.getName(), s3FileUrl));
				} else {
					logger.debug("file {} uploaded to {}", latestFile.getName(), s3FileUrl);
				}
				latestFile.delete();
			}

		}
 
		// create & upload success file
		final String s3FileUrl = new StringBuilder()
				.append(outputS3FolderUrl)
				.append('/')
				.append(successFile.getName())
				.toString();
		successFile.createNewFile();
		successFile.deleteOnExit();
		if (!s3.upload(new S3.Url(s3FileUrl), successFile)) {
			logger.error("error uploading file {} to {}", successFile.getName(), s3FileUrl);
			throw new IOException(String
					.format("error uploading file %s to %s", successFile.getName(), s3FileUrl));
		} else {
			logger.debug("file {} uploaded to {}", successFile.getName(), s3FileUrl);
		}
		successFile.delete();

		// delete nosql on success
		if ("true".equalsIgnoreCase(configuration
				.getProperty("ulisse_parquet_parser.royalty_database.delete_on_success", "true"))) {
			FileUtils.deleteFolder(databaseLocalFolder, true);
		}
		
		// output
		output.addProperty("inputRows", inputRows.get());
		output.addProperty("outputRows", outputRows.get());
		output.addProperty("discardedRows", discardedRows.get());
		output.addProperty("downloadedFiles", downloadedFiles.get());
		output.addProperty("uploadedFiles", uploadedFiles.get());
		output.addProperty("localDatabaseFiles", localDatabaseFiles.get());
		output.addProperty("uploadedDatabaseFiles", uploadedDatabaseFiles.get());
		output.addProperty("inputSizeInBytes", inputSize.get());
		output.addProperty("outputSizeInBytes", outputSize.get());
		output.addProperty("databaseSizeInBytes", databaseSize.get());
		output.addProperty("inputSize", TextUtils.formatSize(inputSize.get()));
		output.addProperty("outputSize", TextUtils.formatSize(outputSize.get()));
		output.addProperty("databaseSize", TextUtils.formatSize(databaseSize.get()));
		output.addProperty("databaseFormat", databaseFormat);
		output.addProperty("databaseVersion", databaseVersion);
		output.addProperty("databaseOrigin", inputS3FolderUrl);
		output.addProperty("inputFingerprint", inputFingerprint);
		output.add("angloAmericanRules", gson.toJsonTree(angloAmericanRulesTemplate));
		output.add("exArt18Exclusions", gson.toJsonTree(exArt18Exclusions));
		output.addProperty("nosqlS3FolderUrl", nosqlS3FolderUrl);
		output.addProperty("startTime", DateFormat
				.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM)
					.format(new Date(startTimeMillis)));
		output.addProperty("totalDuration", TextUtils
				.formatDuration(System.currentTimeMillis() - startTimeMillis));

		logger.debug("{} records parsed", heartbeat.getTotalPumps());
		logger.debug("message processed in {}", TextUtils
				.formatDuration(System.currentTimeMillis() - startTimeMillis));

	}

}