package com.alkemytech.sophia.goal.pojo;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ToRetryWsBean {

	public long id;
	public String webServicePayload;
	public String columnName;
	public String wsUrl;
	
}
