package com.alkemytech.sophia.goal.model;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class ArtistRole {

	public static final int undefined = 0;
	public static final int unknown = 1;
	public static final int author = 2;
	public static final int composer = 3;
	public static final int performer = 4;
	public static final int arranger = 5;
	
	public static int valueOf(String text) {
		if ("unknown".equals(text)) {
			return unknown;
		} else if ("author".equals(text)) {
			return author;
		} else if ("composer".equals(text)) {
			return composer;
		} else if ("performer".equals(text)) {
			return performer;
		} else if ("arranger".equals(text)) {
			return arranger;
		}
		return undefined;
	}
	
	public static String toString(int role) {
		switch (role) {
		case unknown: return "unknown";
		case author: return "author";
		case composer: return "composer";
		case performer: return "performer";
		case arranger: return "arranger";
		default: return "undefined";
		}
	}
	
}
