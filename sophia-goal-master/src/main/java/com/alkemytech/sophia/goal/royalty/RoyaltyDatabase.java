package com.alkemytech.sophia.goal.royalty;

import java.io.Closeable;
import java.io.File;

import com.alkemytech.sophia.goal.pojo.RoyaltyScheme;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface RoyaltyDatabase extends Closeable {

	public RoyaltyDatabaseMetadata getMetadata() throws RoyaltyDatabaseException;
	public File getHomeFolder();
	public boolean isReadOnly();	
	public RoyaltyScheme getScheme(String code) throws RoyaltyDatabaseException;	
	public boolean putScheme(RoyaltyScheme scheme) throws RoyaltyDatabaseException;

}
