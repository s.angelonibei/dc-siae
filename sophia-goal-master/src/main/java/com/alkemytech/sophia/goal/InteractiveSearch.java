package com.alkemytech.sophia.goal;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.index.ReverseIndex;
import com.alkemytech.sophia.commons.index.ReverseIndexProvider;
import com.alkemytech.sophia.commons.query.QueryParser;
import com.alkemytech.sophia.commons.util.LongArray;
import com.alkemytech.sophia.goal.collecting.CollectingDatabase;
import com.alkemytech.sophia.goal.collecting.CollectingDatabaseProvider;
import com.alkemytech.sophia.goal.document.DocumentStore;
import com.alkemytech.sophia.goal.document.DocumentStoreProvider;
import com.alkemytech.sophia.goal.model.CollectingCode;
import com.alkemytech.sophia.goal.model.Document;
import com.alkemytech.sophia.goal.model.Work;
import com.alkemytech.sophia.goal.pojo.RoyaltyScheme;
import com.alkemytech.sophia.goal.royalty.NoSqlRoyaltyDatabase;
import com.alkemytech.sophia.goal.royalty.RoyaltyDatabase;
import com.alkemytech.sophia.goal.work.WorkDatabase;
import com.alkemytech.sophia.goal.work.WorkDatabaseProvider;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class InteractiveSearch {
	
	private static final Logger logger = LoggerFactory.getLogger(InteractiveSearch.class);

	private static interface SearchEngine {
		public void search(String line) throws Exception;
	}
	
	protected static class GuiceModuleExtension extends GuiceModule {

		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// reverse index(es)
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("iswc_index"))
				.toProvider(new ReverseIndexProvider(configuration, "iswc_index"))
				.in(Scopes.SINGLETON);
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("isrc_index"))
				.toProvider(new ReverseIndexProvider(configuration, "isrc_index"))
				.in(Scopes.SINGLETON);
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("title_index"))
				.toProvider(new ReverseIndexProvider(configuration, "title_index"))
				.in(Scopes.SINGLETON);
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("artist_index"))
				.toProvider(new ReverseIndexProvider(configuration, "artist_index"))
				.in(Scopes.SINGLETON);
			// collecting database
			bind(CollectingDatabase.class)
				.toProvider(new CollectingDatabaseProvider(configuration, "collecting_database"))
				.in(Scopes.SINGLETON);
			// work database
			bind(WorkDatabase.class)
				.toProvider(new WorkDatabaseProvider(configuration, "work_database"))
				.in(Scopes.SINGLETON);
			// document store
			bind(DocumentStore.class)
				.toProvider(new DocumentStoreProvider(configuration, "document_store"))
				.in(Scopes.SINGLETON);
			// this
			bind(InteractiveSearch.class)
				.asEagerSingleton();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			final InteractiveSearch instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/interactive-search.properties"))
					.getInstance(InteractiveSearch.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	private final Properties configuration;
	private final WorkDatabase workDatabase;
	private final CollectingDatabase collectingDatabase;
	private final DocumentStore documentStore;
	private final ReverseIndex iswcIndex;
	private final ReverseIndex isrcIndex;
	private final ReverseIndex titleIndex;
	private final ReverseIndex artistIndex;

	@Inject
	protected InteractiveSearch(@Named("configuration") Properties configuration,
			WorkDatabase workDatabase, 
			CollectingDatabase collectingDatabase,
			DocumentStore documentStore,
			@Named("iswc_index") ReverseIndex iswcIndex,
			@Named("isrc_index") ReverseIndex isrcIndex,
			@Named("title_index") ReverseIndex titleIndex,
			@Named("artist_index") ReverseIndex artistIndex) {
		super();
		this.configuration = configuration;
		this.workDatabase = workDatabase;
		this.collectingDatabase = collectingDatabase;
		this.documentStore = documentStore;
		this.iswcIndex = iswcIndex;
		this.isrcIndex = isrcIndex;
		this.titleIndex = titleIndex;
		this.artistIndex = artistIndex;
	}
	
	public InteractiveSearch startup() throws IOException {
		workDatabase.startup();
		collectingDatabase.startup();
		documentStore.startup();
		return this;
	}

	public InteractiveSearch shutdown() throws IOException {
		documentStore.shutdown();
		collectingDatabase.shutdown();
		workDatabase.shutdown();
		return this;
	}
	
	private RoyaltyDatabase createRoyaltyDatabase(File royaltyDatabaseHome) {
		try {
			return new NoSqlRoyaltyDatabase(royaltyDatabaseHome, true, -1, 0, Charset.forName("UTF-8"), false);
		} catch (Exception e) {
			logger.error("createRoyaltyDatabase", e);
		}
		return null;
	}
	
	public InteractiveSearch process(String[] args) throws Exception {
		
		final boolean printWorks = "true".equalsIgnoreCase(configuration
				.getProperty("interactive_search.print_works"));
		final File royaltyDatabaseHome = new File(configuration
				.getProperty("interactive_search.royalty_database.local_folder"));
		final String useIndexCmd = configuration
				.getProperty("interactive_search.command.index_search", "index");
		final String useCollectingCmd = configuration
				.getProperty("interactive_search.command.collecting_search", "collecting");
		final String useWorkCmd = configuration
				.getProperty("interactive_search.command.work_search", "work");
		final String useDocumentCmd = configuration
				.getProperty("interactive_search.command.document_search", "document");
		final String useRoyaltyCmd = configuration
				.getProperty("interactive_search.command.royalty_search", "royalty");
				
		final QueryParser queryParser = QueryParser.create();
		queryParser.addIndex(iswcIndex);
		queryParser.addIndex(isrcIndex);
		queryParser.addIndex(titleIndex);
		queryParser.addIndex(artistIndex);

		final RoyaltyDatabase royaltyDatabase = createRoyaltyDatabase(royaltyDatabaseHome);

		String using = useIndexCmd;
		SearchEngine searchEngine = new SearchEngine() {
			
			@Override
			public void search(String line) throws IOException {
				final LongArray postings = queryParser.evaluate(line);
				System.out.println("results: " + postings);
				if (printWorks) {
					for (int i = 0; i < postings.size(); i ++) {
						final Document document = documentStore.getById(postings.get(i));
						final Work work = workDatabase.getById(document.workId);
						System.out.println("  " + work);
					}					
				}				
			}
			
		};

		System.out.println();
		System.out.println(String.format("type \"use <%s|%s|%s|%s|%s>\" to select search target",
				useIndexCmd, useCollectingCmd, useWorkCmd, useDocumentCmd, useRoyaltyCmd));
		System.out.println("type \"exit\" to quit");
		System.out.println("using " + using);
		try (final BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in))) {
			for (String line = stdin.readLine(); !"exit".equals(line); line = stdin.readLine()) {
				if (null == line || "".equals(line)) {
					continue;
				}
				System.out.println("input text: \"" + line + "\"");
				
				if (line.startsWith("use ")) {
					
					if (("use " + useIndexCmd).equals(line)) {
						if (!using.equals(useIndexCmd)) {
							using = useIndexCmd;
							searchEngine = new SearchEngine() {
								
								@Override
								public void search(String line) throws IOException {
									final LongArray postings = queryParser.evaluate(line);
									System.out.println("results: " + postings);
									if (printWorks) {
										for (int i = 0; i < postings.size(); i ++) {
											final Document document = documentStore.getById(postings.get(i));
											final Work work = workDatabase.getById(document.workId);
											System.out.println("  " + work);
										}					
									}				
								}
								
							};							
						}
					} else if (("use " + useCollectingCmd).equals(line)) {
						if (!using.equals(useCollectingCmd)) {
							using = useCollectingCmd;			
							searchEngine = new SearchEngine() {
								
								@Override
								public void search(String line) throws IOException {
									final CollectingCode collectingCode = collectingDatabase.getByCode(line);
									System.out.println("results: " + collectingCode);
									if (printWorks && null != collectingCode) {
										final Work work = workDatabase.getById(collectingCode.workId);
										System.out.println("  " + work);
									}				
								}
								
							};	
						}
					} else if (("use " + useWorkCmd).equals(line)) {
						if (!using.equals(useWorkCmd)) {
							using = useWorkCmd;
							searchEngine = new SearchEngine() {
								
								@Override
								public void search(String line) throws IOException {
									final Work work = workDatabase.getById(Long.parseLong(line));
									System.out.println("results: " + work);
								}
								
							};							
						}
					} else if (("use " + useDocumentCmd).equals(line)) {
						if (!using.equals(useDocumentCmd)) {
							using = useDocumentCmd;
							searchEngine = new SearchEngine() {
								
								@Override
								public void search(String line) throws IOException {
									final Document document = documentStore.getById(Long.parseLong(line));
									System.out.println("results: " + document);
									if (printWorks && null != document) {
										final Work work = workDatabase.getById(document.workId);
										System.out.println("  " + work);
									}				
								}
								
							};							
						}
					} else if (("use " + useRoyaltyCmd).equals(line)) {
						if (!using.equals(useRoyaltyCmd)) {
							using = useRoyaltyCmd;
							searchEngine = new SearchEngine() {
								
								@Override
								public void search(String line) throws IOException {
									final RoyaltyScheme royaltyScheme = royaltyDatabase.getScheme(line);
									System.out.println("results: " + royaltyScheme);
									if (printWorks && null != royaltyScheme) {
										final CollectingCode collectingCode = collectingDatabase.getByCode(line);
										if (null != collectingCode) {
											final Work work = workDatabase.getById(collectingCode.workId);
											System.out.println("  " + work);
										}
									}				
								}
								
							};
						}
					} else {
						System.out.println("unknown command " + line);
					}
					System.out.println("using " + using);
				
				} else {
					
					searchEngine.search(line);

				}
				
				System.out.println();				
			}
		}
		
		return this;
	}

}