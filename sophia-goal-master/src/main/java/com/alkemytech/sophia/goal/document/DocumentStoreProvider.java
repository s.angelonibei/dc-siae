package com.alkemytech.sophia.goal.document;

import java.util.Properties;

import com.google.inject.Provider;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class DocumentStoreProvider implements Provider<DocumentStore> {
	
	private final Provider<DocumentStore> provider;
	
	public DocumentStoreProvider(final Properties configuration, final String propertyPrefix) {
		super();
		final String className = configuration
				.getProperty(propertyPrefix + ".class_name");
		if (BdbDocumentStore.class.getName().equals(className)) {
			provider = new Provider<DocumentStore>() {
				@Override
				public DocumentStore get() {
					return new BdbDocumentStore(configuration, propertyPrefix);
				}
			}; 
		} else if (NoSqlDocumentStore.class.getName().equals(className)) {
			provider = new Provider<DocumentStore>() {
				@Override
				public DocumentStore get() {
					return new NoSqlDocumentStore(configuration, propertyPrefix);
				}
			}; 
		} else {
			throw new IllegalArgumentException(String
					.format("unknown class name \"%s\"", className));
		}
	}
	
	@Override
	public DocumentStore get() {
		return provider.get();
	}
	
}
