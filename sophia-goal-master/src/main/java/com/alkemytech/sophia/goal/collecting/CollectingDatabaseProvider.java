package com.alkemytech.sophia.goal.collecting;

import java.util.Properties;

import com.google.inject.Provider;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class CollectingDatabaseProvider implements Provider<CollectingDatabase> {
	
	private final Provider<CollectingDatabase> provider;
	
	public CollectingDatabaseProvider(final Properties configuration, final String propertyPrefix) {
		super();
		final String className = configuration
				.getProperty(propertyPrefix + ".class_name");
		if (BdbCollectingDatabase.class.getName().equals(className)) {
			provider = new Provider<CollectingDatabase>() {
				@Override
				public CollectingDatabase get() {
					return new BdbCollectingDatabase(configuration, propertyPrefix);
				}
			}; 
		} else if (NoSqlCollectingDatabase.class.getName().equals(className)) {
			provider = new Provider<CollectingDatabase>() {
				@Override
				public CollectingDatabase get() {
					return new NoSqlCollectingDatabase(configuration, propertyPrefix);
				}
			}; 
		} else {
			throw new IllegalArgumentException(String
					.format("unknown class name \"%s\"", className));
		}
	}
	
	@Override
	public CollectingDatabase get() {
		return provider.get();
	}
	
}
