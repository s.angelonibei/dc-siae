package com.alkemytech.sophia.goal.model;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface TitleType {

	public static final int undefined = 0;
	public static final int original = 1;
	public static final int alternative = 2;

}
