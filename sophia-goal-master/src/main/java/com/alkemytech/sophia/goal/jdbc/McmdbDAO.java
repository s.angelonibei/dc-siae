package com.alkemytech.sophia.goal.jdbc;

import java.util.Properties;

import javax.sql.DataSource;

import com.alkemytech.sophia.commons.jdbc.GenericDAO;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class McmdbDAO extends GenericDAO {

	@Inject
	public McmdbDAO(@Named("configuration") Properties configuration,
			@Named("MCMDB") DataSource dataSource) {
		super(configuration, dataSource);
	}

}
