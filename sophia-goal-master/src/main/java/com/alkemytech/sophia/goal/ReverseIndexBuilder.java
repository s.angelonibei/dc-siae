package com.alkemytech.sophia.goal;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.index.ReverseIndex;
import com.alkemytech.sophia.commons.index.ReverseIndexProvider;
import com.alkemytech.sophia.commons.spelling.NumberSpeller;
import com.alkemytech.sophia.commons.text.TextTokenizer;
import com.alkemytech.sophia.commons.text.TextTokenizerProvider;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.goal.document.DocumentStore;
import com.alkemytech.sophia.goal.document.DocumentStoreProvider;
import com.alkemytech.sophia.goal.model.Document;
import com.alkemytech.sophia.goal.model.Text;
import com.alkemytech.sophia.goal.model.TextType;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ReverseIndexBuilder {
	
	private static final Logger logger = LoggerFactory.getLogger(ReverseIndexBuilder.class);

	protected static class GuiceModuleExtension extends GuiceModule {

		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// tokenizer(s)
			bind(TextTokenizer.class)
				.annotatedWith(Names.named("title_tokenizer"))
				.toProvider(new TextTokenizerProvider(configuration, "title_tokenizer"));
			bind(TextTokenizer.class)
				.annotatedWith(Names.named("artist_tokenizer"))
				.toProvider(new TextTokenizerProvider(configuration, "artist_tokenizer"));
			// reverse index(es)
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("iswc_index"))
				.toProvider(new ReverseIndexProvider(configuration, "iswc_index"))
				.asEagerSingleton();
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("isrc_index"))
				.toProvider(new ReverseIndexProvider(configuration, "isrc_index"))
				.asEagerSingleton();
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("title_index"))
				.toProvider(new ReverseIndexProvider(configuration, "title_index"))
				.asEagerSingleton();
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("artist_index"))
				.toProvider(new ReverseIndexProvider(configuration, "artist_index"))
				.asEagerSingleton();
			// document store
			bind(DocumentStore.class)
				.toProvider(new DocumentStoreProvider(configuration, "document_store"))
				.asEagerSingleton();
			// this
			bind(ReverseIndexBuilder.class)
				.asEagerSingleton();
		}

	}
	
	public static void main(String[] args) {
		try {
			final ReverseIndexBuilder instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/reverse-index-builder.properties"))
					.getInstance(ReverseIndexBuilder.class)
					.startup();
			try {
				instance.process();
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	private final Properties configuration;
	private final DocumentStore documentStore;
	private final ReverseIndex iswcIndex;
	private final ReverseIndex isrcIndex;
	private final ReverseIndex titleIndex;
	private final ReverseIndex artistIndex;
	private final TextTokenizer titleTokenizer;
	private final TextTokenizer artistTokenizer;

	@Inject
	protected ReverseIndexBuilder(@Named("configuration") Properties configuration,
			DocumentStore documentStore,
			@Named("iswc_index") ReverseIndex iswcIndex,
			@Named("isrc_index") ReverseIndex isrcIndex,
			@Named("title_index") ReverseIndex titleIndex,
			@Named("artist_index") ReverseIndex artistIndex,
			@Named("title_tokenizer") TextTokenizer titleTokenizer,
			@Named("artist_tokenizer") TextTokenizer artistTokenizer) {
		super();
		this.configuration = configuration;
		this.documentStore = documentStore;
		this.iswcIndex = iswcIndex;
		this.isrcIndex = isrcIndex;
		this.titleIndex = titleIndex;
		this.artistIndex = artistIndex;
		this.titleTokenizer = titleTokenizer;
		this.artistTokenizer = artistTokenizer;
	}

	public ReverseIndexBuilder startup() throws IOException {
		if ("true".equalsIgnoreCase(configuration.getProperty("reverse_index_builder.locked", "true"))) {
			throw new IllegalStateException("application locked");
		}
		documentStore.startup();
		return this;
	}

	public ReverseIndexBuilder shutdown() {
		documentStore.shutdown();
		return this;
	}
	
	public ReverseIndexBuilder process() throws Exception {

		final int bindPort = Integer.parseInt(configuration.getProperty("reverse_index_builder.bind_port",
				configuration.getProperty("default.bind_port", "0")));

		// bind to lock tcp port
		try (ServerSocket socket = new ServerSocket(bindPort)) {

			// iswc(s) index
			if ("true".equalsIgnoreCase(configuration.getProperty("reverse_index_builder.iswc.create"))) {
				createIndex(iswcIndex, TextType.iswc);
			}

			// isrc(s) index
			if ("true".equalsIgnoreCase(configuration.getProperty("reverse_index_builder.isrc.create"))) {
				createIndex(isrcIndex, TextType.isrc);
			}

			// title(s) index
			if ("true".equalsIgnoreCase(configuration.getProperty("reverse_index_builder.title.create"))) {
				createIndex(titleIndex, TextType.title, titleTokenizer);
			}

			// artist(s) index
			if ("true".equalsIgnoreCase(configuration.getProperty("reverse_index_builder.artist.create"))) {
				createIndex(artistIndex, TextType.artist, artistTokenizer);
			}

		}

		return this;
	}

	private void createIndex(ReverseIndex reverseIndex, final int textType) throws Exception {
		
		final HeartBeat heartbeat = HeartBeat.constant(TextType.toString(textType),
				Integer.parseInt(configuration.getProperty("reverse_index_builder.heartbeat",
						configuration.getProperty("default.heartbeat", "1000"))));

		logger.debug("adding terms to {} index...", TextType.toString(textType));
		long startTimeMillis = System.currentTimeMillis();
		
		final ReverseIndex.Editor editor = reverseIndex.edit();
		editor.rewind();
		documentStore.select(new Document.Selector() {

			@Override
			public void select(Document document) throws IOException {
				for (Text text : document.texts) {
					if (textType == text.type) {
						editor.add(text.text, document.id);
					}
				}
				heartbeat.pump();
			}
			
		});
		logger.debug("{} processed documents", heartbeat.getTotalPumps());
		logger.info("terms added to {} index in {}", TextType.toString(textType),
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		logger.debug("saving {} index...", TextType.toString(textType));
		startTimeMillis = System.currentTimeMillis();
		editor.commit();
		editor.rewind();
		logger.info("{} index saved in {}", TextType.toString(textType),
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
		
	}
	
	private void createIndex(ReverseIndex reverseIndex, final int textType, final TextTokenizer tokenizer) throws Exception {
		
		final NumberSpeller numberSpeller = "true".equalsIgnoreCase(configuration
				.getProperty("reverse_index_builder." + textType + ".spell_numbers")) ? 
						new NumberSpeller(configuration, "number_speller") : null;
		final HeartBeat heartbeat = HeartBeat.constant(TextType.toString(textType), 
				Integer.parseInt(configuration.getProperty("reverse_index_builder.heartbeat", 
						configuration.getProperty("default.heartbeat", "1000"))));

		logger.debug("adding terms to {} index...", TextType.toString(textType));
		long startTimeMillis = System.currentTimeMillis();

		final ReverseIndex.Editor editor = reverseIndex.edit();
		editor.rewind();
		documentStore.select(new Document.Selector() {

			@Override
			public void select(Document document) throws IOException {
				tokenizer.clear();
				for (Text text : document.texts) {
					if (textType == text.type) {
						tokenizer.tokenize(text.text);
					}
					if (null != numberSpeller) {
						for (String spelled : numberSpeller.spell(text.text)) {
							tokenizer.tokenize(spelled);
						}
					}
				}
				editor.addAll(tokenizer.getTokens(), document.id);
				heartbeat.pump();
			}

		});
		logger.debug("{} processed documents", heartbeat.getTotalPumps());
		logger.info("terms added to {} index in {}", TextType.toString(textType),
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

		logger.debug("saving {} index...", TextType.toString(textType));
		startTimeMillis = System.currentTimeMillis();
		editor.commit();
		editor.rewind();
		logger.info("{} index saved in {}", TextType.toString(textType),
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
		
	}

}