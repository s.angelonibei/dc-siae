package com.alkemytech.sophia.goal.pojo;

import java.math.BigDecimal;

import com.alkemytech.sophia.commons.util.BigDecimals;
import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class DrmRoyalty extends Royalty {

	// original parquet file fields
	public BigDecimal percentage;			// 100
	
	public DrmRoyalty(String ipiCode, String siaePosition, String role, String accountCode, String accountId,
			String society, Integer irregularityType, BigDecimal percentage) {
		super();
		this.ipiCode = ipiCode;
		this.siaePosition = siaePosition;
		this.role = role;
		this.accountCode = accountCode;
		this.accountId = accountId;
		this.society = society;
		this.irregularityType = irregularityType;
		this.percentage = percentage = BigDecimals.isAlmostZero(percentage) ?
				BigDecimal.ZERO : BigDecimals.setScale(percentage).stripTrailingZeros();
		this.rate = ParquetRoyalty.computeDrmRate(percentage);
	}

	public DrmRoyalty(String ipiCode, String siaePosition, String role, String accountCode, String accountId,
			String society, Integer irregularityType, boolean notAssociated, boolean specialAccount, boolean publicDomain, boolean siae,
			String angloAmericanRepertoire, BigDecimal percentage) {
		super();
		this.ipiCode = ipiCode;
		this.siaePosition = siaePosition;
		this.role = role;
		this.accountCode = accountCode;
		this.accountId = accountId;
		this.society = society;
		this.irregularityType = irregularityType;
		this.notAssociated = notAssociated;
		this.specialAccount = specialAccount;
		this.publicDomain = publicDomain;
		this.siae = siae;
		this.angloAmericanRepertoire = angloAmericanRepertoire;
		this.percentage = percentage = BigDecimals.isAlmostZero(percentage) ?
				BigDecimal.ZERO : BigDecimals.setScale(percentage).stripTrailingZeros();
		this.rate = ParquetRoyalty.computeDrmRate(percentage);
	}

	@Override
	public boolean isDem() {
		return false;
	}

	@Override
	public boolean isDrm() {
		return true;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}

}
