package com.alkemytech.sophia.goal.work;

import java.util.Properties;

import com.google.inject.Provider;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class WorkDatabaseProvider implements Provider<WorkDatabase> {
	
	private final Provider<WorkDatabase> provider;
	
	public WorkDatabaseProvider(final Properties configuration, final String propertyPrefix) {
		super();
		final String className = configuration
				.getProperty(propertyPrefix + ".class_name");
		if (BdbWorkDatabase.class.getName().equals(className)) {
			provider = new Provider<WorkDatabase>() {
				@Override
				public WorkDatabase get() {
					return new BdbWorkDatabase(configuration, propertyPrefix);
				}
			}; 
		} else if (NoSqlWorkDatabase.class.getName().equals(className)) {
			provider = new Provider<WorkDatabase>() {
				@Override
				public WorkDatabase get() {
					return new NoSqlWorkDatabase(configuration, propertyPrefix);
				}
			}; 
		} else {
			throw new IllegalArgumentException(String
					.format("unknown class name \"%s\"", className));
		}
	}
	
	@Override
	public WorkDatabase get() {
		return provider.get();
	}
	
}
