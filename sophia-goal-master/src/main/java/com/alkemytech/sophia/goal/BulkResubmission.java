package com.alkemytech.sophia.goal;

import java.io.IOException;
import java.net.ServerSocket;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.sqs.McmdbMessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.goal.jdbc.McmdbDAO;
import com.alkemytech.sophia.goal.jdbc.McmdbDataSource;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class BulkResubmission {
	
	private static final Logger logger = LoggerFactory.getLogger(BulkResubmission.class);
	
	private static class GuiceModuleExtension extends GuiceModule {

		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.toInstance(new S3(configuration));
			bind(SQS.class)
				.toInstance(new SQS(configuration));
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("MCMDB"))
				.to(McmdbDataSource.class)
				.asEagerSingleton();
			// data access object(s)
			bind(McmdbDAO.class);
			// this
			bind(BulkResubmission.class)
				.asEagerSingleton();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			final BulkResubmission instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/bulk-resubmission.properties"))
					.getInstance(BulkResubmission.class)
					.startup();
			try {
				instance.process();
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	private final Properties configuration;
	private final S3 s3;
	private final SQS sqs;
	private final Gson gson;
	private final DataSource dataSource;
	private final Provider<McmdbDAO> daoProvider;

	@Inject
	protected BulkResubmission(@Named("configuration") Properties configuration,
			S3 s3, SQS sqs, Gson gson,
			@Named("MCMDB") DataSource dataSource, Provider<McmdbDAO> daoProvider) {
		super();
		this.configuration = configuration;
		this.s3 = s3;
		this.sqs = sqs;
		this.gson = gson;
		this.dataSource = dataSource;
		this.daoProvider = daoProvider;
	}

	public BulkResubmission startup() throws SQLException, IOException {
		if ("true".equalsIgnoreCase(configuration.getProperty("bulk_resubmission.locked", "true"))) {
			throw new IllegalStateException("application locked");
		}
		s3.startup();
		sqs.startup();
		return this;
	}

	public BulkResubmission shutdown() {
		sqs.shutdown();
		s3.shutdown();
		return this;
	}	
	
	private BulkResubmission process() throws Exception {
		
		final int bindPort = Integer.parseInt(configuration.getProperty("bulk_resubmission.bind_port",
				configuration.getProperty("default.bind_port", "0")));

		// bind lock tcp port
		try (ServerSocket socket = new ServerSocket(bindPort)) {
			logger.debug("socket bound to {}", socket.getLocalSocketAddress());
						
			final Object monitor = new Object();
			final ExecutorService executorService = Executors.newCachedThreadPool();
			final AtomicInteger runningThreads = new AtomicInteger(0);
						
//			processMessage(gson.fromJson("{\"body\":{\"force\":false}}", JsonObject.class), new JsonObject());
			
			// {env}_to_process_bulk_resubmission_ml queue handler
			runningThreads.incrementAndGet();
			executorService.execute(new Runnable() {
				
				@Override
				public void run() {
					try {
						// sqs message pump
						final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "bulk_resubmission.sqs");
						sqsMessagePump.pollingLoop(new McmdbMessageDeduplicator(dataSource), new SqsMessagePump.Consumer() {

							private JsonObject output;
							private JsonObject error;

							@Override
							public JsonObject getStartedMessagePayload(JsonObject message) {
								output = null;
								error = null;
								return SqsMessageHelper.formatContext();
							}

							@Override
							public boolean consumeMessage(JsonObject message) {
								try {
									output = new JsonObject();
									processMessage(message, output);
									error = null;
									return true;
								} catch (Exception e) {
									output = null;
									error = SqsMessageHelper.formatError(e);
									return false;
								}
							}

							@Override
							public JsonObject getCompletedMessagePayload(JsonObject message) {
								return output;
							}

							@Override
							public JsonObject getFailedMessagePayload(JsonObject message) {
								return error;
							}

						});	
					} catch (Exception e) {
						logger.error("run", e);
					} finally {
						logger.debug("thread {} finished", runningThreads.getAndDecrement());
						synchronized (monitor) {
							monitor.notify();
						}
					}
				}
				
			});
			
			// send sqs message(s) thread
			runningThreads.incrementAndGet();
			executorService.execute(new Runnable() {
				
				@Override
				public void run() {
					try {
						sendSqsMessages();
					} catch (Exception e) {
						logger.error("run", e);
					} finally {
						logger.debug("thread {} finished", runningThreads.getAndDecrement());
						synchronized (monitor) {
							monitor.notify();
						}
					}
				}
				
			});
			
			// receive {env}_completed_{service} sqs message(s) thread
			runningThreads.incrementAndGet();
			executorService.execute(new Runnable() {
				
				@Override
				public void run() {
					try {
						receiveSqsMessages();
					} catch (Exception e) {
						logger.error("run", e);
					} finally {
						logger.debug("thread {} finished", runningThreads.getAndDecrement());
						synchronized (monitor) {
							monitor.notify();
						}
					}
				}
				
			});
									
			// join threads
			while (runningThreads.get() > 0) {
				synchronized (monitor) {
					monitor.wait(1000L);
				}	
			}
			
		}

		return this;
	}

	private void processMessage(JsonObject input, JsonObject output) throws Exception {

//		{
//		  "body": {
//		    "force": false
//		  },
//		  "header": {
//		    "queue": "debug_to_process_bulk_resubmission_ml",
//		    "timestamp": "2018-05-10T00:00:00.000000+02:00",
//		    "uuid": "7f36a2bb-0637-4d4d-9990-4ed9644adefc",
//		    "sender": "ml-ripartizione"
//		  }
//		}

		final McmdbDAO dao = daoProvider.get();
		final long startTimeMillis = System.currentTimeMillis();
	
		// parse input json message
		final JsonObject inputBody = input.getAsJsonObject("body");
		final boolean force = GsonUtils.getAsBoolean(inputBody, "force", false);
		logger.debug("force {}", force);
		
		// check if non completed bulk resubmission(s) exist
		final List<Map<String, String>> stillRunning = dao
				.executeQuery("bulk_resubmission.sql.select_still_running");
		if (!force && !stillRunning.isEmpty()) {
			// output
			output.addProperty("startTime", DateFormat
					.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM)
						.format(new Date(startTimeMillis)));
			output.addProperty("totalDuration", TextUtils
					.formatDuration(System.currentTimeMillis() - startTimeMillis));
			output.addProperty("additionalInfo", String
					.format("bulk resubmission still running %s", stillRunning.toString()));
			logger.info("bulk resubmission still running: {}", stillRunning);
			logger.debug("message processed in {}",
					TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
			return;
		}
		
		// begin transaction
		dao.beginTransaction();
		try {
		
			// insert resubmission
			int count = dao.executeUpdate("bulk_resubmission.sql.insert_resubmission", true);
			logger.debug("insert_ml_riprocessamento: {}", count);
			final long resubmissionId = dao.getLastGeneratedKey();
			logger.debug("resubmissionId: {}", resubmissionId);
	
			// duplicate resubmittable report(s)
			final List<Map<String, String>> resubmittableReports = dao
					.executeQuery("bulk_resubmission.sql.select_resubmittable_reports");
			if (resubmittableReports.isEmpty())
				throw new IllegalStateException("no reports available for resubmission");
			for (Map<String, String> resubmittableReport : resubmittableReports) {
				logger.debug("resubmittableReport: {}", resubmittableReport);
				
				// update resubmittable report (set active = 0)
				count = dao.executeUpdate("bulk_resubmission.sql.update_resubmittable_reports", resubmittableReport);
				logger.debug("update_resubmittable_reports: {}", count);

				// override column(s)
				String reportId = resubmittableReport.get("reportId");
				String reportSeq = resubmittableReport.get("reportSeq");
				if (!reportId.endsWith(reportSeq))
					throw new IllegalStateException(String
							.format("reportSeq %s incompatible with reportId %s", reportSeq, reportId));
				reportId = reportId.substring(0, reportId.length() - reportSeq.length());
				reportSeq = Integer.toString(1 + Integer.parseInt(reportSeq));
				resubmittableReport.put("reportId", reportId + reportSeq);
				resubmittableReport.put("reportSeq", reportSeq);
				resubmittableReport.put("resubmissionId", Long.toString(resubmissionId));
				
				// insert resubmitted report
				count = dao.executeUpdate("bulk_resubmission.sql.insert_resubmitted_report", resubmittableReport);
				logger.debug("insert_resubmitted_report: {}", count);
			}
			
			// insert resubmission period(s)
			final List<Map<String, String>> resubmissionPeriods = dao
					.executeQuery("bulk_resubmission.sql.select_resubmission_periods",
							ImmutableMap.of("resubmissionId",  Long.toString(resubmissionId)));
			for (Map<String, String> resubmissionPeriod : resubmissionPeriods) {
				logger.debug("resubmissionPeriod: {}", resubmissionPeriod);
				
				// override column(s)
				resubmissionPeriod.put("resubmissionId", Long.toString(resubmissionId));
				
				// insert resubmission period
				count = dao.executeUpdate("bulk_resubmission.sql.insert_resubmission_period", resubmissionPeriod);
				logger.debug("insert_resubmission_period: {}", count);
			}
			
			// commit transaction
			dao.commitTransaction();
			
		} catch (Exception e) {
			
			// rollback transaction
			dao.rollbackTransaction();
			
			throw e;
		}
				
		// output
		output.addProperty("startTime", DateFormat
				.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM)
					.format(new Date(startTimeMillis)));
		output.addProperty("totalDuration", TextUtils
				.formatDuration(System.currentTimeMillis() - startTimeMillis));

		logger.debug("message processed in {}", TextUtils
				.formatDuration(System.currentTimeMillis() - startTimeMillis));

	}

	private void sendSqsMessages() throws Exception {
		
		final McmdbDAO dao = daoProvider.get();
		final String serviceBusQueueUrl = sqs.getOrCreateUrl(configuration
				.getProperty("bulk_resubmission.sqs.service_bus_queue")); 
		final long runTimeoutMillis = TextUtils.parseLongDuration(configuration
				.getProperty("bulk_resubmission.run_timeout", "4m30s")); // default to 4 minutes and 30 seconds
		final long pollingPeriod = TextUtils.parseLongDuration(configuration
				.getProperty("bulk_resubmission.polling_period", "15s")); // default to 15 seconds
		final boolean force = "true".equalsIgnoreCase(configuration
				.getProperty("bulk_resubmission.outbound_force", "true"));
		
		final long startTimeMillis = System.currentTimeMillis();
		while (System.currentTimeMillis() - startTimeMillis < runTimeoutMillis) {
			int sentMessages = 0;
			
			// send update index
			final List<Map<String, String>> outgoingUpdateIndexes = dao
					.executeQuery("bulk_resubmission.sql.select_outgoing_update_index");
			for (Map<String, String> outgoingUpdateIndex : outgoingUpdateIndexes) {
				logger.debug("outgoingUpdateIndex: {}", outgoingUpdateIndex);
				// format message
				final String queue = configuration
						.getProperty("bulk_resubmission.sqs.update_index.to_process_queue");
				final String sender = configuration
						.getProperty("bulk_resubmission.sqs.sender_name");
				final String uuid = UUID.randomUUID().toString();
				final JsonObject body = new JsonObject();
				body.addProperty("force", force);
				final JsonObject message = SqsMessageHelper.formatToProcessJson(queue, uuid, sender, body);
				if (sqs.sendMessage(serviceBusQueueUrl, gson.toJson(message))) {
					logger.debug("message successfully sent: {}", message);
					sentMessages ++;
					
					// override column(s)
					outgoingUpdateIndex.put("updateIndexRequestUuid", uuid);
					
					// update resubmission
					int count = dao.executeUpdate("bulk_resubmission.sql.update_outgoing_update_index", outgoingUpdateIndex);
					logger.debug("update_outgoing_update_index: {}", count);

				} else {
					logger.warn("error sending message: {}", message);
				}
			}
			
			// send ulisse ipi dump
			final List<Map<String, String>> outgoingUlisseIpiDumps = dao
					.executeQuery("bulk_resubmission.sql.select_outgoing_ulisse_ipi_dump");
			for (Map<String, String> outgoingUlisseIpiDump : outgoingUlisseIpiDumps) {
				logger.debug("outgoingUlisseIpiDump: {}", outgoingUlisseIpiDump);
				// format message
				final String queue = configuration
						.getProperty("bulk_resubmission.sqs.ulisse_ipi_dump.to_process_queue");
				final String sender = configuration
						.getProperty("bulk_resubmission.sqs.sender_name");
				final String uuid = UUID.randomUUID().toString();
				final String periodDateTo = outgoingUlisseIpiDump.get("periodDateTo");
				final JsonObject body = new JsonObject();
				body.addProperty("year", periodDateTo.substring(0, 4));
				body.addProperty("month", periodDateTo.substring(5, 7));
				body.addProperty("force", force);
				final JsonObject message = SqsMessageHelper.formatToProcessJson(queue, uuid, sender, body);
				if (sqs.sendMessage(serviceBusQueueUrl, gson.toJson(message))) {
					logger.debug("message successfully sent: {}", message);
					sentMessages ++;
					
					// override column(s)
					outgoingUlisseIpiDump.put("ulisseIpiDumpRequestUuid", uuid);
					
					// update resubmission
					int count = dao.executeUpdate("bulk_resubmission.sql.update_outgoing_ulisse_ipi_dump", outgoingUlisseIpiDump);
					logger.debug("update_outgoing_ulisse_ipi_dump: {}", count);

				} else {
					logger.warn("error sending message: {}", message);
				}
			}
			
			// send schema riparto
			final List<Map<String, String>> outgoingRoyaltySchemes = dao
					.executeQuery("bulk_resubmission.sql.select_outgoing_royalty_scheme");
			for (Map<String, String> outgoingRoyaltyScheme : outgoingRoyaltySchemes) {
				logger.debug("outgoingRoyaltyScheme: {}", outgoingRoyaltyScheme);
				// format message
				final String queue = configuration
						.getProperty("bulk_resubmission.sqs.royalty_scheme.to_process_queue");
				final String sender = configuration
						.getProperty("bulk_resubmission.sqs.sender_name");
				final String uuid = UUID.randomUUID().toString();
				final String periodDateTo = outgoingRoyaltyScheme.get("periodDateTo");
				final JsonObject body = new JsonObject();
				body.addProperty("year", periodDateTo.substring(0, 4));
				body.addProperty("month", periodDateTo.substring(5, 7));
				body.addProperty("force", force);			
				final JsonObject message = SqsMessageHelper.formatToProcessJson(queue, uuid, sender, body);
				if (sqs.sendMessage(serviceBusQueueUrl, gson.toJson(message))) {
					logger.debug("message successfully sent: {}", message);
					sentMessages ++;
					
					// override column(s)
					outgoingRoyaltyScheme.put("royaltySchemesRequestUuid", uuid);
					
					// update resubmission
					int count = dao.executeUpdate("bulk_resubmission.sql.update_outgoing_royalty_scheme", outgoingRoyaltyScheme);
					logger.debug("update_outgoing_ulisse_ipi_dump: {}", count);

				} else {
					logger.warn("error sending message: {}", message);
				}
			}

			// no message sent, wait a bit...
			if (0 == sentMessages)
				Thread.sleep(pollingPeriod);
		}
		logger.info("exiting database loop after {}", TextUtils
				.formatDuration(System.currentTimeMillis() - startTimeMillis));
	}
	
	private void receiveSqsMessages() throws Exception {

		final McmdbDAO dao = daoProvider.get();
		final long runTimeoutMillis = TextUtils.parseLongDuration(configuration
				.getProperty("bulk_resubmission.sqs.max_polling_duration", "4m30s")); // default to 4 minutes and 30 seconds
		final long pollingPeriod = TextUtils.parseLongDuration(configuration
				.getProperty("bulk_resubmission.sqs.polling_period", "15s")); // default to 15 seconds

		final String completedUpdateIndexQueueUrl = sqs.getOrCreateUrl(configuration
				.getProperty("bulk_resubmission.sqs.update_index.completed_queue")); 
		final String failedUpdateIndexQueueUrl = sqs.getOrCreateUrl(configuration
				.getProperty("bulk_resubmission.sqs.update_index.failed_queue")); 
		
		final String completedUlisseIpiDumpQueueUrl = sqs.getOrCreateUrl(configuration
				.getProperty("bulk_resubmission.sqs.ulisse_ipi_dump.completed_queue")); 
		final String failedUlisseIpiDumpQueueUrl = sqs.getOrCreateUrl(configuration
				.getProperty("bulk_resubmission.sqs.ulisse_ipi_dump.failed_queue")); 
		
		final String completedRoyaltySchemeQueueUrl = sqs.getOrCreateUrl(configuration
				.getProperty("bulk_resubmission.sqs.royalty_scheme.completed_queue")); 
		final String failedRoyaltySchemeQueueUrl = sqs.getOrCreateUrl(configuration
				.getProperty("bulk_resubmission.sqs.royalty_scheme.failed_queue"));
		
		final long startTimeMillis = System.currentTimeMillis();
		while (System.currentTimeMillis() - startTimeMillis < runTimeoutMillis) {
			int receivedMessages = 0;

			// poll completed update index queue
			List<SQS.Msg> messages = sqs.receiveMessages(completedUpdateIndexQueueUrl);
			logger.debug("{} completed update index message(s) received", messages.size());
			if (null != messages) for (SQS.Msg message : messages) {
				receivedMessages ++;
				try {				
					// parse message json
					logger.debug("parsing message {}", message.text);
					final JsonObject messageJson = gson.fromJson(message.text, JsonObject.class);
					final String updateIndexRequestUuid = messageJson.getAsJsonObject("input")
							.getAsJsonObject("header").get("uuid").getAsString();
					// update database
					final int count = dao.executeUpdate("bulk_resubmission.sql.update_completed_update_index",
							ImmutableMap.of("updateIndexRequestUuid", updateIndexRequestUuid));
					logger.debug("update_completed_update_index: {}", count);
				} finally {
					// delete message from queue
					sqs.deleteMessage(completedUpdateIndexQueueUrl, message.receiptHandle);
				}
			}
			
			// poll failed update index queue
			messages = sqs.receiveMessages(failedUpdateIndexQueueUrl);
			logger.debug("{} failed update index message(s) received", messages.size());
			if (null != messages) for (SQS.Msg message : messages) {
				receivedMessages ++;
				try {				
					// parse message json
					logger.debug("parsing message {}", message.text);
					final JsonObject messageJson = gson.fromJson(message.text, JsonObject.class);
					final String updateIndexRequestUuid = messageJson.getAsJsonObject("input")
							.getAsJsonObject("header").get("uuid").getAsString();
					// update database
					final int count = dao.executeUpdate("bulk_resubmission.sql.update_failed_update_index",
							ImmutableMap.of("updateIndexRequestUuid", updateIndexRequestUuid));
					logger.debug("update_failed_update_index: {}", count);
				} finally {
					// delete message from queue
					sqs.deleteMessage(failedUpdateIndexQueueUrl, message.receiptHandle);
				}
			}

			// poll completed ulisse ipi dump queue
			messages = sqs.receiveMessages(completedUlisseIpiDumpQueueUrl);
			logger.debug("{} completed ulisse ipi dump message(s) received", messages.size());
			if (null != messages) for (SQS.Msg message : messages) {
				receivedMessages ++;
				try {				
					// parse message json
					logger.debug("parsing message {}", message.text);
					final JsonObject messageJson = gson.fromJson(message.text, JsonObject.class);
					final String ulisseIpiDumpRequestUuid = messageJson.getAsJsonObject("input")
							.getAsJsonObject("header").get("uuid").getAsString();
					// update database
					final int count = dao.executeUpdate("bulk_resubmission.sql.update_completed_ulisse_ipi_dump",
							ImmutableMap.of("ulisseIpiDumpRequestUuid", ulisseIpiDumpRequestUuid));
					logger.debug("update_completed_ulisse_ipi_dump: {}", count);
				} finally {
					// delete message from queue
					sqs.deleteMessage(completedUlisseIpiDumpQueueUrl, message.receiptHandle);
				}
			}
			
			// poll failed ulisse ipi dump queue
			messages = sqs.receiveMessages(failedUlisseIpiDumpQueueUrl);
			logger.debug("{} failed ulisse ipi dump message(s) received", messages.size());
			if (null != messages) for (SQS.Msg message : messages) {
				receivedMessages ++;
				try {				
					// parse message json
					logger.debug("parsing message {}", message.text);
					final JsonObject messageJson = gson.fromJson(message.text, JsonObject.class);
					final String ulisseIpiDumpRequestUuid = messageJson.getAsJsonObject("input")
							.getAsJsonObject("header").get("uuid").getAsString();
					// update database
					final int count = dao.executeUpdate("bulk_resubmission.sql.update_failed_ulisse_ipi_dump",
							ImmutableMap.of("ulisseIpiDumpRequestUuid", ulisseIpiDumpRequestUuid));
					logger.debug("update_failed_ulisse_ipi_dump: {}", count);
				} finally {
					// delete message from queue
					sqs.deleteMessage(failedUlisseIpiDumpQueueUrl, message.receiptHandle);
				}
			}
			
			// poll completed royalty scheme queue
			messages = sqs.receiveMessages(completedRoyaltySchemeQueueUrl);
			logger.debug("{} completed royalty scheme message(s) received", messages.size());
			if (null != messages) for (SQS.Msg message : messages) {
				receivedMessages ++;
				try {
					// parse message json
					logger.debug("parsing message {}", message.text);
					final JsonObject messageJson = gson.fromJson(message.text, JsonObject.class);
					final String royaltySchemeRequestUuid = messageJson.getAsJsonObject("input")
							.getAsJsonObject("header").get("uuid").getAsString();
					// update database
					final int count = dao.executeUpdate("bulk_resubmission.sql.update_completed_royalty_scheme",
							ImmutableMap.of("royaltySchemeRequestUuid", royaltySchemeRequestUuid));
					logger.debug("update_completed_royalty_scheme: {}", count);
				} finally {
					// delete message from queue
					sqs.deleteMessage(completedRoyaltySchemeQueueUrl, message.receiptHandle);
				}
			}
			
			// poll failed royalty scheme queue
			messages = sqs.receiveMessages(failedRoyaltySchemeQueueUrl);
			logger.debug("{} failed royalty scheme message(s) received", messages.size());
			if (null != messages) for (SQS.Msg message : messages) {
				receivedMessages ++;
				try {
					// parse message json
					logger.debug("parsing message {}", message.text);
					final JsonObject messageJson = gson.fromJson(message.text, JsonObject.class);
					final String royaltySchemeRequestUuid = messageJson.getAsJsonObject("input")
							.getAsJsonObject("header").get("uuid").getAsString();
					// update database
					final int count = dao.executeUpdate("bulk_resubmission.sql.update_failed_royalty_scheme",
							ImmutableMap.of("royaltySchemeRequestUuid", royaltySchemeRequestUuid));
					logger.debug("update_failed_royalty_scheme: {}", count);
				} finally {
					// delete message from queue
					sqs.deleteMessage(failedRoyaltySchemeQueueUrl, message.receiptHandle);
				}
			}

			// no message received, wait a bit...
			if (0 == receivedMessages)
				Thread.sleep(pollingPeriod);
		}
		logger.info("exiting message loop after {}", TextUtils
				.formatDuration(System.currentTimeMillis() - startTimeMillis));

		// begin transaction
		dao.beginTransaction();
		try {
			
			// ready to start period(s)
			final List<Map<String, String>> readyToStartPeriods = dao
					.executeQuery("bulk_resubmission.sql.select_ready_to_start_periods");
			for (Map<String, String> readyToStartPeriod : readyToStartPeriods) {
				logger.debug("readyToStartPeriod: {}", readyToStartPeriod);
				
				// update ready to start report(s)
				int count = dao.executeUpdate("bulk_resubmission.sql.update_ready_to_start_reports", readyToStartPeriod);
				logger.debug("update_ready_to_start_reports: {}", count);

				// update ready to start period
				count = dao.executeUpdate("bulk_resubmission.sql.update_ready_to_start_period", readyToStartPeriod);
				logger.debug("update_ready_to_start_period: {}", count);
			}
			
			// commit transaction
			dao.commitTransaction();

		} catch (Exception e) {
			
			// rollback transaction
			dao.rollbackTransaction();
			
			throw e;
		}
		
	}

}