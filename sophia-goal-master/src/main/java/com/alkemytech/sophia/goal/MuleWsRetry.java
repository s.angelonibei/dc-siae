package com.alkemytech.sophia.goal;

import java.io.IOException;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.http.HTTP;
import com.alkemytech.sophia.goal.jdbc.McmdbDAO;
import com.alkemytech.sophia.goal.jdbc.McmdbDataSource;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class MuleWsRetry {
	
	private static final Logger logger = LoggerFactory.getLogger(MuleWsRetry.class);
	
	private static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// http
			bind(HTTP.class)
				.toInstance(new HTTP(configuration));
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("MCMDB"))
				.to(McmdbDataSource.class)
				.asEagerSingleton();
			// data access object(s)
			bind(McmdbDAO.class)
				.asEagerSingleton();
			// this
			bind(MuleWsRetry.class)
				.asEagerSingleton();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			final MuleWsRetry instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/mule-ws-retry.properties"))
					.getInstance(MuleWsRetry.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Properties configuration;
	private final McmdbDAO dao;
	private final Gson gson;
	private final Charset charset;
	private final HTTP http;

	@Inject
	protected MuleWsRetry(@Named("configuration") Properties configuration,
			@Named("charset") Charset charset, 
			McmdbDAO dao, Gson gson, HTTP http) throws IOException {
		super();
		this.configuration = configuration;
		this.charset = charset;
		this.dao = dao;
		this.gson = gson;
		this.http = http;
	}
	
	public MuleWsRetry startup() throws IOException {
		if ("true".equalsIgnoreCase(configuration.getProperty("mule_ws_retry.locked", "true")))
			throw new IllegalStateException("application locked");
		http.startup();
		return this;
	}

	public MuleWsRetry shutdown() throws IOException {
		http.shutdown();
		return this;
	}
	
	private MuleWsRetry process(String[] args) throws SQLException, IOException {
		
		final int bindPort = Integer.parseInt(configuration
				.getProperty("mule_ws_retry.bind_port", configuration
						.getProperty("default.bind_port", "0")));

		// bind lock tcp port
		try (final ServerSocket socket = new ServerSocket(bindPort)) {
			logger.debug("socket bound to {}", socket.getLocalSocketAddress());
			
			// process database record(s)
			final List<Map<String, String>> reports = dao
					.executeQuery("mule_ws_retry.sql.select_to_retry");
			logger.debug("{} report(s) to retry", reports.size());
			for (Map<String, String> report : reports)
				process(report);
			
		}
		
		return this;
	}

	private void process(final Map<String, String> toRetry) throws SQLException {
		logger.debug("toRetry {}", toRetry);
		
		try {
			final HTTP.Response response = http
					.post(toRetry.get("wsUrl"), toRetry.get("webServicePayload"), "application/json");
			logger.debug("response status  {}", response.status);
			logger.debug("response body {}", response.getBodyAsString(charset));
			if (response.isOK()) {
				final JsonObject responseBody = gson
						.fromJson(response.getBodyAsString(charset),
								JsonObject.class);
				if (null != responseBody) {
					final JsonElement ko = responseBody.get("KO");
					if (null != ko && !ko.getAsBoolean()) {
						dao.executeUpdate("mule_ws_retry.sql.update_completed", toRetry);
					}
				}
			}
		} catch (Exception e) {
			logger.error("process", e);
		}
		
	}

}
