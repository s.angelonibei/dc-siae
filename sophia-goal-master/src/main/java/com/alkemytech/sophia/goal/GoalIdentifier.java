package com.alkemytech.sophia.goal;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import javax.sql.DataSource;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.collecting.SiaeCode;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.http.HTTP;
import com.alkemytech.sophia.commons.index.ReverseIndex;
import com.alkemytech.sophia.commons.index.ReverseIndexProvider;
import com.alkemytech.sophia.commons.query.TitleArtistQuery;
import com.alkemytech.sophia.commons.query.TitleArtistQueryProvider;
import com.alkemytech.sophia.commons.text.TextNormalizer;
import com.alkemytech.sophia.commons.text.TextNormalizerProvider;
import com.alkemytech.sophia.commons.text.TextTokenizer;
import com.alkemytech.sophia.commons.text.TextTokenizerProvider;
import com.alkemytech.sophia.commons.util.FileUtils;
import com.alkemytech.sophia.commons.util.SplitCharSequence;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.goal.collecting.CollectingDatabase;
import com.alkemytech.sophia.goal.collecting.CollectingDatabaseProvider;
import com.alkemytech.sophia.goal.document.DocumentScore;
import com.alkemytech.sophia.goal.document.DocumentScoreProvider;
import com.alkemytech.sophia.goal.document.DocumentSearch;
import com.alkemytech.sophia.goal.document.DocumentStore;
import com.alkemytech.sophia.goal.document.DocumentStoreProvider;
import com.alkemytech.sophia.goal.document.ScoredDocument;
import com.alkemytech.sophia.goal.index.ReverseIndexMetadata;
import com.alkemytech.sophia.goal.jdbc.McmdbDAO;
import com.alkemytech.sophia.goal.jdbc.McmdbDataSource;
import com.alkemytech.sophia.goal.model.CollectingCode;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import com.ibm.icu.text.DateFormat;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class GoalIdentifier {
	
	private static final Logger logger = LoggerFactory.getLogger(GoalIdentifier.class);
	
	private static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// http
			bind(HTTP.class)
				.toInstance(new HTTP(configuration));
			// amazon service(s)
			bind(S3.class)
				.toInstance(new S3(configuration));
			// normalizer(s)
			bind(TextNormalizer.class)
				.annotatedWith(Names.named("iswc_normalizer"))
				.toProvider(new TextNormalizerProvider(configuration, "iswc_normalizer"));
			bind(TextNormalizer.class)
				.annotatedWith(Names.named("isrc_normalizer"))
				.toProvider(new TextNormalizerProvider(configuration, "isrc_normalizer"));
			bind(TextNormalizer.class)
				.annotatedWith(Names.named("title_normalizer"))
				.toProvider(new TextNormalizerProvider(configuration, "title_normalizer"));
			bind(TextNormalizer.class)
				.annotatedWith(Names.named("artist_normalizer"))
				.toProvider(new TextNormalizerProvider(configuration, "artist_normalizer"));
			// tokenizer(s)
			bind(TextTokenizer.class)
				.annotatedWith(Names.named("title_tokenizer"))
				.toProvider(new TextTokenizerProvider(configuration, "title_tokenizer"));
			bind(TextTokenizer.class)
				.annotatedWith(Names.named("artist_tokenizer"))
				.toProvider(new TextTokenizerProvider(configuration, "artist_tokenizer"));
			// title artist quer(y/ies)
			bind(TitleArtistQuery.class)
				.toProvider(new TitleArtistQueryProvider(configuration, 
						configuration.getProperty("goal_identifier.title_artist_query")));
			// document search
			bind(DocumentSearch.class);
			// document score(s)
			bind(DocumentScore.class)
				.toProvider(new DocumentScoreProvider(configuration,
						configuration.getProperty("goal_identifier.document_score")));
			// reverse index(es)
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("iswc_index"))
				.toProvider(new ReverseIndexProvider(configuration, "iswc_index"))
				.in(Scopes.SINGLETON);
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("isrc_index"))
				.toProvider(new ReverseIndexProvider(configuration, "isrc_index"))
				.in(Scopes.SINGLETON);
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("title_index"))
				.toProvider(new ReverseIndexProvider(configuration, "title_index"))
				.in(Scopes.SINGLETON);
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("artist_index"))
				.toProvider(new ReverseIndexProvider(configuration, "artist_index"))
				.in(Scopes.SINGLETON);
			// collecting database
			bind(CollectingDatabase.class)
				.toProvider(new CollectingDatabaseProvider(configuration, "collecting_database"))
				.in(Scopes.SINGLETON);
			// document store
			bind(DocumentStore.class)
				.toProvider(new DocumentStoreProvider(configuration, "document_store"))
				.in(Scopes.SINGLETON);
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("MCMDB"))
				.to(McmdbDataSource.class)
				.asEagerSingleton();
			// data access object(s)
			bind(McmdbDAO.class)
				.asEagerSingleton();
			// this
			bind(GoalIdentifier.class)
				.asEagerSingleton();
		}
	}
	
	public static void main(String[] args) {
		try {
			final GoalIdentifier instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/goal-identifier.properties"))
					.getInstance(GoalIdentifier.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Properties configuration;
	private final Gson gson;
	private final Charset charset;
	private final HTTP http;
	private final S3 s3;
	private final McmdbDAO dao;
	private final Provider<CollectingDatabase> collectingDatabaseProvider;
	private final Provider<DocumentStore> documentStoreProvider;
	private final Provider<ReverseIndex> iswcIndexProvider;
	private final Provider<ReverseIndex> isrcIndexProvider;
	private final Provider<ReverseIndex> titleIndexProvider;
	private final Provider<ReverseIndex> artistIndexProvider;
	private final Provider<TextNormalizer> iswcNormalizerProvider;
	private final Provider<TextNormalizer> isrcNormalizerProvider;
	private final Provider<TextNormalizer> titleNormalizerProvider;
	private final Provider<TextNormalizer> artistNormalizerProvider;
	private final Provider<DocumentSearch> documentSearchProvider;

	@Inject
	protected GoalIdentifier(@Named("configuration") Properties configuration,
			@Named("charset") Charset charset, 
			McmdbDAO dao, Gson gson, HTTP http, S3 s3,
			Provider<CollectingDatabase> collectingDatabaseProvider,
			Provider<DocumentStore> documentStoreProvider,
			@Named("iswc_index") Provider<ReverseIndex> iswcIndexProvider,
			@Named("isrc_index") Provider<ReverseIndex> isrcIndexProvider,
			@Named("title_index") Provider<ReverseIndex> titleIndexProvider,
			@Named("artist_index") Provider<ReverseIndex> artistIndexProvider,
			@Named("iswc_normalizer") Provider<TextNormalizer> iswcNormalizerProvider,
			@Named("isrc_normalizer") Provider<TextNormalizer> isrcNormalizerProvider,
			@Named("title_normalizer") Provider<TextNormalizer> titleNormalizerProvider,
			@Named("artist_normalizer") Provider<TextNormalizer> artistNormalizerProvider,
			Provider<DocumentSearch> documentSearchProvider) throws IOException {
		super();
		this.configuration = configuration;
		this.charset = charset;
		this.dao = dao;
		this.gson = gson;
		this.http = http;
		this.s3 = s3;
		this.collectingDatabaseProvider = collectingDatabaseProvider;
		this.documentStoreProvider = documentStoreProvider;
		this.iswcIndexProvider = iswcIndexProvider;
		this.isrcIndexProvider = isrcIndexProvider;
		this.titleIndexProvider = titleIndexProvider;
		this.artistIndexProvider = artistIndexProvider;
		this.iswcNormalizerProvider = iswcNormalizerProvider;
		this.isrcNormalizerProvider = isrcNormalizerProvider;
		this.titleNormalizerProvider = titleNormalizerProvider;
		this.artistNormalizerProvider = artistNormalizerProvider;
		this.documentSearchProvider = documentSearchProvider;
	}
	
	public GoalIdentifier startup() throws IOException {
		if ("true".equalsIgnoreCase(configuration.getProperty("goal_identifier.locked", "true"))) {
			throw new IllegalStateException("application locked");
		}
		http.startup();
		s3.startup();
		return this;
	}

	public GoalIdentifier shutdown() throws IOException {
		s3.shutdown();
		http.shutdown();
		return this;
	}
	
	private GoalIdentifier process(String[] args) throws Exception {
		
		final int bindPort = Integer.parseInt(configuration
				.getProperty("goal_identifier.bind_port", configuration
						.getProperty("default.bind_port", "0")));

		// bind lock tcp port
		try (final ServerSocket socket = new ServerSocket(bindPort)) {
			logger.debug("socket bound to {}", socket.getLocalSocketAddress());
			
			// process request(s)
			final List<Map<String, String>> reports = dao
					.executeQuery("goal_identifier.sql.select_to_process");
			logger.debug("{} report(s) to process", reports.size());
			for (Map<String, String> report : reports)
				process(report);
			
		}
		
		return this;
	}
	
	private String escapeCsvValue(String value, char delimiter) {
		if (Strings.isNullOrEmpty(value))
			return "";
		if (-1 != value.indexOf(delimiter))
			return new StringBuilder()
					.append('"')
					.append(value.replace("\"", "\"\""))
					.append('"')
					.toString();
		return value;
	}

	private void process(final Map<String, String> toProcess) throws Exception {
		logger.debug("toProcess {}", toProcess);
		
		final long startTimeMillis = System.currentTimeMillis();

		final File homeFolder = new File(configuration
				.getProperty("default.home_folder"));
		final int threadCount = Integer.parseInt(configuration
				.getProperty("goal_identifier.thread_count", "1"));
		
		final File latestFile = new File(configuration
				.getProperty("goal_identifier.latest_file", "_LATEST"));
		final String indexS3FolderUrl = configuration
				.getProperty("goal_identifier.index.s3_folder_url");
		final File indexLocalFolder = new File(configuration
				.getProperty("goal_identifier.index.local_folder"));

		int skipRows = Integer.parseInt(configuration
				.getProperty("goal_identifier.skip_rows", "0"));
		final boolean copySkippedRows = "true".equalsIgnoreCase(configuration
				.getProperty("goal_identifier.copy_skipped_rows"));
		final char delimiter = configuration
				.getProperty("goal_identifier.delimiter", ";").trim().charAt(0);	
//		final char quote = configuration
//				.getProperty("goal_identifier.quote", "\"").trim().charAt(0);	
		final String splitRegex = configuration
				.getProperty("dsr_identifier.split_regex", "\\|").trim();
		final int columnTitoloOpera = Integer.parseInt(configuration
				.getProperty("goal_identifier.column.titolo_opera"));
		final int columnAutore = Integer.parseInt(configuration
				.getProperty("goal_identifier.column.autore"));
		final int columnEditore = Integer.parseInt(configuration
				.getProperty("goal_identifier.column.editore"));
		final int columnInterprete = Integer.parseInt(configuration
				.getProperty("goal_identifier.column.interprete"));
		final int columnUtilizzazioni = Integer.parseInt(configuration
				.getProperty("goal_identifier.column.utilizzazioni"));
		final int columnPrezzo = Integer.parseInt(configuration
				.getProperty("goal_identifier.column.prezzo_di_vendita"));
		final int columnCodiceOpera = Integer.parseInt(configuration
				.getProperty("goal_identifier.column.codice_opera"));
		final int columnCodiceIswc = Integer.parseInt(configuration
				.getProperty("goal_identifier.column.codice_iswc"));
		final int columnCodiceIsrc = Integer.parseInt(configuration
				.getProperty("goal_identifier.column.codice_isrc"));

		final double iswcMinScore = Double.parseDouble(configuration
				.getProperty("goal_identifier.min_score.iswc"));
		final double isrcMinScore = Double.parseDouble(configuration
				.getProperty("goal_identifier.min_score.isrc"));
		final double titleArtistMinScore = Double.parseDouble(configuration
				.getProperty("goal_identifier.min_score.title_artist"));
		final double autoScore = Double.parseDouble(configuration
				.getProperty("goal_identifier.auto_score", "0.999"));
		final int maxPostings = Integer.parseInt(configuration
				.getProperty("goal_identifier.max_postings"));
		
		final String wsUrl = configuration.getProperty("goal_identifier.ws.url");
		final String wsDateFormat = configuration.getProperty("goal_identifier.ws.date_format");
		
		final long statsFrequency = TextUtils.parseLongDuration(configuration
				.getProperty("goal_identifier.stats_frequency"));
		final boolean debug = "true".equalsIgnoreCase(configuration
				.getProperty("goal_identifier.debug", configuration
						.getProperty("default.debug")));

		// additional info
		final JsonObject additionalInfo = new JsonObject();
		try {
			additionalInfo.addProperty("hostname", InetAddress.getLocalHost().getHostName());
		} catch (UnknownHostException e) {
			additionalInfo.addProperty("hostname", "localhost");
		}
		
		// process started
		toProcess.put("additionalInfo", gson.toJson(additionalInfo));
		dao.executeUpdate("goal_identifier.sql.update_started", toProcess);
		
		// check index _LATEST version
		final String latestS3FileUrl = new StringBuilder()
				.append(indexS3FolderUrl)
				.append('/')
				.append(latestFile.getName())
				.toString();
		final String latestVersion = s3
				.getObjectContents(new S3.Url(latestS3FileUrl), charset);
		logger.debug("latestVersion {}", latestVersion);
		if (Strings.isNullOrEmpty(latestVersion)) {
			additionalInfo.addProperty("errorMessage", String
					.format("latest knowledge-base not found on s3: %s", latestS3FileUrl));
			toProcess.put("additionalInfo", gson.toJson(additionalInfo));
			dao.executeUpdate("goal_identifier.sql.update_failed", toProcess);
			return;
		}		
		// index metadata
		final ReverseIndexMetadata metadata = new ReverseIndexMetadata(indexLocalFolder);
		final String localVersion = metadata
				.getProperty(ReverseIndexMetadata.VERSION, null);
		logger.debug("localVersion {}", localVersion);
		// download latest index version from s3
		if (!latestVersion.equalsIgnoreCase(localVersion)) {
			
			// delete existing file(s)
			logger.debug("deleting local knowledge-base files");
			if (indexLocalFolder.exists())
				FileUtils.deleteFolder(indexLocalFolder, true);
			indexLocalFolder.mkdirs();

			// download s3 file(s)
			logger.debug("downloading latest knowledge-base files from s3");
			final String s3FolderUrl = new StringBuilder()
					.append(indexS3FolderUrl)
					.append('/')
					.append(latestVersion)
					.toString();
			final List<S3ObjectSummary> objects = s3.listObjects(new S3.Url(s3FolderUrl));
			for (S3ObjectSummary object : objects) {
				final String key = object.getKey();
				final String s3BucketUrl = new StringBuilder()
						.append("s3://")
						.append(object.getBucketName())
						.append('/')
						.toString();
				final String s3FileUrl = s3BucketUrl + key;
				logger.debug("s3FileUrl {}", s3FileUrl);
				String relativePath = key.substring(s3FolderUrl.length() - s3BucketUrl.length());
				logger.debug("relativePath {}", relativePath);
				if (Strings.isNullOrEmpty(relativePath) ||
						relativePath.endsWith("/"))
					continue;
				while (relativePath.startsWith("/"))
					relativePath = relativePath.substring(1);
				logger.debug("relativePath {}", relativePath);
				final File file = new File(indexLocalFolder, relativePath);
				logger.debug("file {}", file);
				if (-1 != relativePath.indexOf('/'))
					file.getParentFile().mkdirs();
				if (!s3.download(new S3.Url(s3FileUrl), file)) {
					logger.error("knowledge-base file download error: {}", s3FileUrl);
					// process failed
					additionalInfo.addProperty("errorMessage", String
							.format("knowledge-base file download error: %s", s3FileUrl));
					toProcess.put("additionalInfo", gson.toJson(additionalInfo));
					dao.executeUpdate("goal_identifier.sql.update_failed", toProcess);
					return;
				} else {
					logger.debug("knowledge-base file downloaded {}", s3FileUrl);
				}
			}

		}
		
		// startup database(s)
		final DocumentStore documentStore = documentStoreProvider.get();
		final CollectingDatabase collectingDatabase = collectingDatabaseProvider.get();
		documentStore.startup();
		collectingDatabase.startup();

		// setup index(es)
		final ReverseIndex iswcIndex = iswcIndexProvider.get();
		final ReverseIndex isrcIndex = isrcIndexProvider.get();
		final ReverseIndex titleIndex = titleIndexProvider.get();
		final ReverseIndex artistIndex = artistIndexProvider.get();

		// temporary local file(s)
		final UUID uuid = UUID.randomUUID();
		final File inputFile = new File(homeFolder, uuid + "-input.csv");
		final File outputFile = new File(homeFolder, uuid + "-output.csv");
		final File enhancedFile = new File(homeFolder, uuid + "-enhanced.csv");
		inputFile.deleteOnExit();
		outputFile.deleteOnExit();
		enhancedFile.deleteOnExit();

		// download file from s3
		final String inputFileUrl = toProcess.get("inputFileUrl");
		logger.debug("downloading s3 file {}", inputFileUrl);
		if (!s3.download(new S3.Url(inputFileUrl), inputFile)) {
			logger.warn("input file download error: {}", inputFileUrl);
			// process failed
			additionalInfo.addProperty("errorMessage", String
					.format("input file download error: %s", inputFileUrl));
			toProcess.put("additionalInfo", gson.toJson(additionalInfo));
			dao.executeUpdate("goal_identifier.sql.update_failed", toProcess);
			return;
		}

		// constant(s)
		final boolean keepWorkCode = "true".equalsIgnoreCase(toProcess.get("keepWorkCode"))
				|| "1".equalsIgnoreCase(toProcess.get("keepWorkCode"));
		final String escapedLicense = escapeCsvValue(toProcess.get("license"), delimiter);
		final String escapedLicenseeCode = escapeCsvValue(toProcess.get("licenseeCode"), delimiter);
		final String escapedLicenseeName = escapeCsvValue(toProcess.get("licenseeName"), delimiter);
		final String escapedService = escapeCsvValue(toProcess.get("service"), delimiter);
		final String escapedPeriod = escapeCsvValue(toProcess.get("period"), delimiter);
		final String escapedPeriodDateFrom = escapeCsvValue(toProcess.get("periodDateFrom"), delimiter);
		final String escapedPeriodDateTo = escapeCsvValue(toProcess.get("periodDateTo"), delimiter);
		final String escapedUtilizationMode = escapeCsvValue(toProcess.get("utilizationMode"), delimiter);
		final String escapedFruitionMode = escapeCsvValue(toProcess.get("fruitionMode"), delimiter);
		final String escapedSubscription = escapeCsvValue(toProcess.get("subscription"), delimiter);
		final String escapedReportId = escapeCsvValue(toProcess.get("reportId"), delimiter);
		
		// stats
		final AtomicLong totalRecords = new AtomicLong(0L);
		final AtomicLong identifiedRecords = new AtomicLong(0L);
		final AtomicLong identifiedRecordsWorkCode = new AtomicLong(0L);
		final AtomicLong identifiedRecordsIswc = new AtomicLong(0L);
		final AtomicLong identifiedRecordsIsrc = new AtomicLong(0L);
		final AtomicLong identifiedRecordsTitleArtist = new AtomicLong(0L);
		final AtomicLong totalSales = new AtomicLong(0L);
		final AtomicLong identifiedSales = new AtomicLong(0L);
		final AtomicLong identifiedSalesWorkCode = new AtomicLong(0L);
		final AtomicLong identifiedSalesIswc = new AtomicLong(0L);
		final AtomicLong identifiedSalesIsrc = new AtomicLong(0L);
		final AtomicLong identifiedSalesTitleArtist = new AtomicLong(0L);

		logger.debug("processing input file {}", inputFile);
		long elapsedTimeMillis = 0L;
		
		try (final OutputStream outputStream = new FileOutputStream(outputFile);
				final BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);
				final OutputStream enhancedStream = new FileOutputStream(enhancedFile);
				final BufferedOutputStream bufferedEnhancedStream = new BufferedOutputStream(enhancedStream);
				final FileInputStream inputStream = new FileInputStream(inputFile);
				final InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
				final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				final CSVParser csvParser = new CSVParser(bufferedReader, CSVFormat.newFormat(delimiter)
						.withQuoteMode(QuoteMode.MINIMAL)
						.withQuote('"')
						.withIgnoreEmptyLines()
						.withIgnoreSurroundingSpaces())) {			
			
			final Iterator<CSVRecord> csvIterator = csvParser.iterator();
			for (; skipRows > 0 && csvIterator.hasNext(); skipRows --) {
				final CSVRecord record = csvIterator.next();
				if (copySkippedRows) {
					final Iterator<String> iterator = record.iterator();
					if (iterator.hasNext())
						bufferedOutputStream.write(iterator
								.next().getBytes(charset));
					while (iterator.hasNext()) {
						bufferedOutputStream.write(delimiter);
						bufferedOutputStream.write(iterator
								.next().getBytes(charset));
					}
					bufferedOutputStream.write('\n');
					logger.debug("record copied {}", record);
				} else {
					logger.debug("record skipped {}", record);
				}
			}
			final Object monitor = new Object();
			final ExecutorService executorService = Executors.newCachedThreadPool();
			final AtomicInteger runningThreads = new AtomicInteger(0);
			final AtomicReference<Exception> threadException = new AtomicReference<>(null);
			while (runningThreads.get() < threadCount) {
				logger.debug("starting thread {}", runningThreads.incrementAndGet());
				executorService.execute(new Runnable() {
					
					@Override
					public void run() {
						try {
							
							// WARNING: duplicate for thread safety
							final TextNormalizer iswcNormalizer = iswcNormalizerProvider.get();
							final TextNormalizer isrcNormalizer = isrcNormalizerProvider.get();
							final TextNormalizer titleNormalizer = titleNormalizerProvider.get();
							final TextNormalizer artistNormalizer = artistNormalizerProvider.get();
							final DocumentSearch documentSearch = documentSearchProvider.get();
							
							while (null == threadException.get()) {

								// read next csv line
								final CSVRecord record;
								synchronized (csvIterator) {
									if (csvIterator.hasNext()) {
										record = csvIterator.next();
									} else {
										return;
									}
								}
								
								// parse line
								final String titoloOpera = record.get(columnTitoloOpera);
								final String autore = record.get(columnAutore);
								final String editore = record.get(columnEditore);
								final String interprete = record.get(columnInterprete);
								final String utilizzazioni = record.get(columnUtilizzazioni);
								final String prezzo = record.get(columnPrezzo);
								final String codiceOpera = record.get(columnCodiceOpera);
								final String codiceIswc = record.get(columnCodiceIswc);
								final String codiceIsrc = record.get(columnCodiceIsrc);
								
								Long workId = null;
								String workCode = null;
								ScoredDocument bestScoredDocument = null;

								// sales count
								int salesCount = Strings.isNullOrEmpty(utilizzazioni) ?
										0 : Integer.parseInt(utilizzazioni);
								totalSales.addAndGet(salesCount);

								// work_code
								if (keepWorkCode) {
									workCode = SiaeCode.normalize(codiceOpera);
									if (SiaeCode.isValid(workCode)) {
										identifiedRecordsWorkCode.incrementAndGet();
										identifiedSalesWorkCode.addAndGet(salesCount);
									} else {
										workCode = null;
									}
								}
								if (null == workId && Strings.isNullOrEmpty(workCode)) {
									
									// title
									String title = titleNormalizer.normalize(titoloOpera);
									if (!Strings.isNullOrEmpty(title)) {
										
										final SplitCharSequence normalizedTitle = new SplitCharSequence(title);
										if (debug)
											logger.debug("normalizedTitle \"{}\"", normalizedTitle);
										
										// artist(s)
										final Set<SplitCharSequence> normalizedArtists = new HashSet<>();
										for (String splitArtist : autore.split(splitRegex)) {
											splitArtist = artistNormalizer.normalize(splitArtist);
											if (!Strings.isNullOrEmpty(splitArtist)) {
												normalizedArtists.add(new SplitCharSequence(splitArtist));
											}
										}
										for (String splitArtist : interprete.split(splitRegex)) {
											splitArtist = artistNormalizer.normalize(splitArtist);
											if (!Strings.isNullOrEmpty(splitArtist)) {
												normalizedArtists.add(new SplitCharSequence(splitArtist));
											}
										}
										if (debug)
											logger.debug("normalizedArtists \"{}\"", normalizedArtists);
										if (!normalizedArtists.isEmpty()) {
											
											// iswc
											if (null == workId && !Strings.isNullOrEmpty(codiceIswc)) {
												final String iswc = iswcNormalizer.normalize(codiceIswc);
												if (!Strings.isNullOrEmpty(iswc)) {
													final ScoredDocument scoredDocument = documentSearch.search(iswcIndex,
															normalizedTitle, normalizedArtists, iswc, maxPostings, autoScore);
													if (null != scoredDocument) {
														bestScoredDocument = scoredDocument;
														if (scoredDocument.score >= iswcMinScore) {
															workId = scoredDocument.document.workId;
															identifiedRecordsIswc.incrementAndGet();
															identifiedSalesIswc.addAndGet(salesCount);
														}														
													}
												}
											}

											// isrc
											if (null == workId && !Strings.isNullOrEmpty(codiceIsrc)) {
												final String isrc = isrcNormalizer.normalize(codiceIsrc);
												if (!Strings.isNullOrEmpty(isrc)) {
													final ScoredDocument scoredDocument = documentSearch.search(isrcIndex,
															normalizedTitle, normalizedArtists, isrc, maxPostings, autoScore);
													if (null != scoredDocument) {
														if (null == bestScoredDocument) {
															bestScoredDocument = scoredDocument;
														} else if (scoredDocument.score > bestScoredDocument.score) {
															bestScoredDocument = scoredDocument;															
														}
														if (scoredDocument.score >= isrcMinScore) {
															workId = scoredDocument.document.workId;
															identifiedRecordsIsrc.incrementAndGet();
															identifiedSalesIsrc.addAndGet(salesCount);
														}
													}
												}
											}
											
											// title_artists
											if (null == workId) {
												final ScoredDocument scoredDocument = documentSearch.search(titleIndex, artistIndex,
														normalizedTitle, normalizedArtists, maxPostings, autoScore);
												if (null != scoredDocument) {
													if (null == bestScoredDocument) {
														bestScoredDocument = scoredDocument;
													} else if (scoredDocument.score > bestScoredDocument.score) {
														bestScoredDocument = scoredDocument;															
													}
													if (scoredDocument.score >= titleArtistMinScore) {
														workId = scoredDocument.document.workId;
														identifiedRecordsTitleArtist.incrementAndGet();
														identifiedSalesTitleArtist.addAndGet(salesCount);
													}
												}
											}

										}
										
									}

								}
								
								// collecting's work code
								if (null != workId || !Strings.isNullOrEmpty(workCode)) {
									identifiedRecords.incrementAndGet();
									identifiedSales.addAndGet(salesCount);
									if (Strings.isNullOrEmpty(workCode)) {
										final CollectingCode collecting = collectingDatabase
												.getByWorkId(workId);	
										workCode = collecting.code;
									}
								}
								if (debug)
									logger.debug("workCode \"{}\"", workCode);

								// best scored document
								String bestWorkCode = null;
								String bestScore = null;
								if (null != bestScoredDocument) {
									if (null != workId && workId == bestScoredDocument.document.workId) {
										bestWorkCode = workCode;
									} else {
										final CollectingCode collecting = collectingDatabase
												.getByWorkId(bestScoredDocument.document.workId);
										bestWorkCode = collecting.code;
									}
									bestScore = String.format("%.2f", bestScoredDocument.score * 100.0);
								}
								
								// format output csv line
								final StringBuilder outputLine = new StringBuilder()
									.append(escapeCsvValue(titoloOpera, delimiter))
									.append(delimiter)
									.append(escapeCsvValue(autore, delimiter))
									.append(delimiter)
									.append(escapeCsvValue(editore, delimiter))
									.append(delimiter)
									.append(escapeCsvValue(interprete, delimiter))
									.append(delimiter)
									.append(escapeCsvValue(utilizzazioni, delimiter))
									.append(delimiter)
									.append(escapeCsvValue(prezzo, delimiter))
									.append(delimiter)
									.append(!Strings.isNullOrEmpty(workCode) ? workCode :
											!Strings.isNullOrEmpty(codiceOpera) ? codiceOpera : "")
									.append(delimiter)
									.append(escapeCsvValue(codiceIswc, delimiter))
									.append(delimiter)
									.append(escapeCsvValue(codiceIsrc, delimiter));

								// write to output
								byte[] outputBytes = outputLine
										.toString().getBytes("UTF-8");
								synchronized (bufferedOutputStream) {
									bufferedOutputStream.write(outputBytes);
									bufferedOutputStream.write('\n');
								}
								
								// enhance output csv line
								outputLine.append(delimiter)
									.append(escapedLicense)
									.append(delimiter)
									.append(escapedLicenseeCode)
									.append(delimiter)
									.append(escapedLicenseeName)
									.append(delimiter)
									.append(escapedService)
									.append(delimiter)
									.append(escapedPeriod)
									.append(delimiter)
									.append(escapedPeriodDateFrom)
									.append(delimiter)
									.append(escapedPeriodDateTo)
									.append(delimiter)
									.append(escapedUtilizationMode)
									.append(delimiter)
									.append(escapedFruitionMode)
									.append(delimiter)
									.append(escapedSubscription)
									.append(delimiter)
									.append(escapedReportId)
									.append(delimiter)
									.append(escapeCsvValue(bestWorkCode, delimiter))
									.append(delimiter)
									.append(escapeCsvValue(bestScore, delimiter));

								// write enhanced output
								outputBytes = outputLine
										.toString().getBytes("UTF-8");
								synchronized (bufferedEnhancedStream) {
									bufferedEnhancedStream.write(outputBytes);
									bufferedEnhancedStream.write('\n');
								}
								
								totalRecords.incrementAndGet();

							}
							
						} catch (Exception e) {
							logger.error("run", e);
							threadException.compareAndSet(null, e);
						} finally {
							logger.debug("thread {} finished", runningThreads.getAndDecrement());
							synchronized (monitor) {
								monitor.notify();
							}
						}
					}
					
				});				
			}
			
			while (runningThreads.get() > 0) {
				synchronized (monitor) {
					monitor.wait(statsFrequency);
				}
				
				elapsedTimeMillis = System.currentTimeMillis() - startTimeMillis;
				// print stats
				logger.debug("---------- stats ----------");
				logger.debug("processing time {}",
						TextUtils.formatDuration(elapsedTimeMillis));
				logger.debug("average line processing time {}ms", 
						Math.round((double) elapsedTimeMillis / (double) totalRecords.get()));
				logger.debug("processed {}", totalRecords);
				logger.debug("  unidentified {} ({}%)", totalRecords.get() - identifiedRecords.get(),
						Math.round(100.0 * (totalRecords.get() - identifiedRecords.get()) / (double) totalRecords.get()));
				logger.debug("  identified {} ({}%)", identifiedRecords,
						Math.round(100.0 * identifiedRecords.get() / (double) totalRecords.get()));
				if (keepWorkCode) {
					logger.debug("    work_code {} ({}%)", identifiedRecordsWorkCode,
							Math.round(100.0 * identifiedRecordsWorkCode.get() / (double) totalRecords.get()));
				}
				if (-1 != columnCodiceIswc) {
					logger.debug("    iswc {} ({}%)", identifiedRecordsIswc,
							Math.round(100.0 * identifiedRecordsIswc.get() / (double) totalRecords.get()));
				}
				if (-1 != columnCodiceIsrc) {
					logger.debug("    isrc {} ({}%)", identifiedRecordsIsrc,
							Math.round(100.0 * identifiedRecordsIsrc.get() / (double) totalRecords.get()));
				}
				logger.debug("    title_artist {} ({}%)", identifiedRecordsTitleArtist,
						Math.round(100.0 * identifiedRecordsTitleArtist.get() / (double) totalRecords.get()));
				logger.debug("    total_sales {}", totalSales);
				logger.debug("    identified_sales {} ({}%)", identifiedSales,
						Math.round(100.0 * identifiedSales.get() / (double) totalSales.get()));
				logger.debug("    identified_sales_work_code {} ({}%)", identifiedSalesWorkCode,
						Math.round(100.0 * identifiedSalesWorkCode.get() / (double) totalSales.get()));
				
			}
			
			// re-throw thread exception
			if (null != threadException.get())
				throw threadException.get();
			
		} catch (Exception e) {
			logger.error("process", e);
			// process failed
			final StringWriter stackTrace = new StringWriter();
			e.printStackTrace(new PrintWriter(stackTrace));
			additionalInfo.addProperty("errorMessage", "error processing file");
			additionalInfo.addProperty("stackTrace", stackTrace.toString());
			toProcess.put("additionalInfo", gson.toJson(additionalInfo));
			dao.executeUpdate("goal_identifier.sql.update_failed", toProcess);
			return;
		}
		
		logger.debug("{} records processed", totalRecords);
		elapsedTimeMillis = System.currentTimeMillis() - startTimeMillis;
		logger.debug("identification completed in {}",
				TextUtils.formatDuration(elapsedTimeMillis));

		// upload output file to s3
		final String outputFileUrl = configuration
				.getProperty("goal_identifier.output_file_url")
				.replace("{period}", toProcess.get("period"))
				.replace("{reportId}", toProcess.get("reportId"));
		logger.debug("uploading file {} to {}", outputFile.getName(), outputFileUrl);
		if (!s3.upload(new S3.Url(outputFileUrl), outputFile)) {
			logger.error("output file upload error: {}", outputFileUrl);
			// process failed
			additionalInfo.addProperty("errorMessage", String
					.format("output file upload error: %s", outputFileUrl));
			toProcess.put("additionalInfo", gson.toJson(additionalInfo));
			dao.executeUpdate("goal_identifier.sql.update_failed", toProcess);
			return;
		}
		
		// upload enhanced output file to s3
		final String enhancedFileUrl = configuration
				.getProperty("goal_identifier.enhanced_file_url")
				.replace("{period}", toProcess.get("period"))
				.replace("{reportId}", toProcess.get("reportId"));
		logger.debug("uploading file {} to {}", enhancedFile.getName(), enhancedFileUrl);
		if (!s3.upload(new S3.Url(enhancedFileUrl), enhancedFile)) {
			logger.error("enhanced output file upload error: {}", enhancedFileUrl);
			// process failed
			additionalInfo.addProperty("errorMessage", String
					.format("enhanced output file upload error: %s", enhancedFileUrl));
			toProcess.put("additionalInfo", gson.toJson(additionalInfo));
			dao.executeUpdate("goal_identifier.sql.update_failed", toProcess);
			return;
		}
		
		// web service
		final Map<String, String> thresholdMap = dao
				.executeSingleRowQuery("goal_identifier.sql.select_threshold");
		final double threshold = Double.parseDouble(thresholdMap.get("threshold"));
		final double identifiedSalesPercentage = 100.0 * identifiedSales.get() / totalSales.get();
		final JsonObject requestContent = new JsonObject();
		requestContent.addProperty("id", toProcess.get("reportId"));
		requestContent.addProperty("esitoCodifica", 
				identifiedSalesPercentage >= threshold ?
						configuration.getProperty("goal_identifier.threshold_greater") :
						configuration.getProperty("goal_identifier.threshold_smaller"));
		requestContent.addProperty("sogliaCodifica", threshold);
//		muleWSRequest.addProperty("sogliaCodifica",
//				String.format("%.2f", threshold));
		requestContent.addProperty("numOpereCodificate", 
				identifiedRecords.get());
		requestContent.addProperty("numUtilizzazioniCodificate", 
				identifiedSales.get());
		requestContent.addProperty("dataLavorazioneCodifica", 
				new SimpleDateFormat(wsDateFormat).format(new Date()));
		final String webServicePayload = gson.toJson(requestContent);
		logger.debug("webServicePayload {}", webServicePayload);
		boolean webServiceSuccess = false;
		try {
			final HTTP.Response response = http
					.post(wsUrl, webServicePayload, "application/json");
			if (response.isOK()) {
				final JsonObject responseBody = gson
						.fromJson(response.getBodyAsString(charset),
								JsonObject.class);
				if (null != responseBody) {
					final JsonElement ko = responseBody.get("KO");
					if (null != ko) {
						webServiceSuccess = !ko.getAsBoolean();
					}
				}
			}
		} catch (Exception e) {
			logger.error("process", e);
		}
		logger.debug("webServiceSuccess {}", webServiceSuccess);

		// process completed
		additionalInfo.addProperty("totalRecords", totalRecords.get());
		additionalInfo.addProperty("identifiedRecords", identifiedRecords.get());
		additionalInfo.addProperty("identifiedRecordsWorkCode", identifiedRecordsWorkCode.get());
		additionalInfo.addProperty("identifiedRecordsIswc", identifiedRecordsIswc.get());
		additionalInfo.addProperty("identifiedRecordsIsrc", identifiedRecordsIsrc.get());
		additionalInfo.addProperty("identifiedRecordsTitleArtist", identifiedRecordsTitleArtist.get());
		additionalInfo.addProperty("totalSales", totalSales.get());
		additionalInfo.addProperty("identifiedSales", identifiedSales.get());
		additionalInfo.addProperty("identifiedSalesWorkCode", identifiedSalesWorkCode.get());
		additionalInfo.addProperty("identifiedSalesIswc", identifiedSalesIswc.get());
		additionalInfo.addProperty("identifiedSalesIsrc", identifiedSalesIsrc.get());
		additionalInfo.addProperty("identifiedSalesTitleArtist", identifiedSalesTitleArtist.get());
		additionalInfo.addProperty("startTime", DateFormat
				.getInstance().format(new Date(startTimeMillis)));
		additionalInfo.addProperty("totalDuration", 
				TextUtils.formatDuration(elapsedTimeMillis));
		logger.debug("additionalInfo {}", additionalInfo);
		toProcess.putAll(thresholdMap);
		toProcess.put("outputFileUrl", outputFileUrl);
		toProcess.put("totalRecords", totalRecords.toString());
		toProcess.put("identifiedRecords", identifiedRecords.toString());
		toProcess.put("totalSales", totalSales.toString());
		toProcess.put("identifiedSales", identifiedSales.toString());
		toProcess.put("webServiceSuccess", webServiceSuccess ? "true" : "false");
		toProcess.put("webServicePayload", webServicePayload);
		toProcess.put("additionalInfo", gson.toJson(additionalInfo));
		dao.executeUpdate("goal_identifier.sql.update_completed", toProcess);

		// shutdown database(s)
		documentStore.shutdown();
		collectingDatabase.shutdown();
		
		// delete temporary files
		inputFile.delete();
		outputFile.delete();
		enhancedFile.delete();
		
	}

}
