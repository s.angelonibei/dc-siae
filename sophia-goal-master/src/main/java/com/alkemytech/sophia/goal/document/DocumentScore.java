package com.alkemytech.sophia.goal.document;

import java.util.Collection;

import com.alkemytech.sophia.commons.util.SplitCharSequence;
import com.alkemytech.sophia.goal.model.Document;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface DocumentScore {

	public double getScore(SplitCharSequence title, Collection<SplitCharSequence> artists, Document document);
	
}
