package com.alkemytech.sophia.goal.pojo;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class CompletedBean {

	public String outputFileUrl;
	public long totalRecords;
	public long identifiedRecords;
	public long totalSales;
	public long identifiedSales;
	public String additionalInfo;
	public boolean webServiceSuccess;
	public String webServicePayload;
	
}
