package com.alkemytech.sophia.goal.pojo;

import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.parquet.Strings;

import com.alkemytech.sophia.commons.util.BigDecimals;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class Royalty {

	private static final AtomicInteger sequence = new AtomicInteger(0);
	
	private final int index;

	// original parquet file fields
	public String ipiCode;						// 181888820
	public String siaePosition;					// 128940
	public String role;							// CA
	public String accountCode;					// 09
	public String accountId;					// 000099966
	public String society;						// SIAE
	public Integer irregularityType;			// 68

	// pre-computed fields
	public boolean notAssociated;				// true | false
	public boolean specialAccount;				// true | false
	public boolean publicDomain;				// true | false
	public boolean siae;						// true | false
	public String angloAmericanRepertoire;		// EMI

	// computed fields
	public BigDecimal rate;						// [0.0, 1.0]
	public boolean dustProducer;				// true | false
	public boolean dustConsumer;				// true | false
	public BigDecimal points;					// 0.0

	// WARNING: le irregolarità sono automaticamente ricondotte a una coppia di quote SIAE
	//          DEM e DRM, entrambe al 100% e non esenti da trattenute ex art. 18
	
	// WARNING: tutti i calcoli vengono effettuati in termini di DEM rate e DRM rate che
	//          sono sempre valori compresi nell'intervallo [0.0, 1.0]
	
	public Royalty() {
		super();
		this.index = sequence.incrementAndGet();
	}
	
	public abstract boolean isDem();
	public abstract boolean isDrm();

	public String getHolderId() {
		return new StringBuilder()
				.append(ipiCode)
				.append('/')
				.append(siaePosition)
				.append('/')
				.append(role)
				.append('/')
				.append(irregularityType)
				.toString();
	}
	
	public boolean isAngloAmerican() {
		return !Strings.isNullOrEmpty(angloAmericanRepertoire);
	}
	
	public boolean isZero() {
		return BigDecimals.isAlmostZero(rate);
	}
	
	@Override
	public int hashCode() {
		return 31 + index;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (null == obj)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Royalty other = (Royalty) obj;
		if (index != other.index)
			return false;
		return true;
	}
	
}
