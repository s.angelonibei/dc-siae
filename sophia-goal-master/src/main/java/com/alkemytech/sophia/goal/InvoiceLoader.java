package com.alkemytech.sophia.goal;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.goal.jdbc.McmdbDAO;
import com.alkemytech.sophia.goal.jdbc.McmdbDataSource;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class InvoiceLoader {
	
	private static final Logger logger = LoggerFactory.getLogger(InvoiceLoader.class);

	private static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("MCMDB"))
				.to(McmdbDataSource.class)
				.asEagerSingleton();
			// data access object(s)
			bind(McmdbDAO.class)
				.asEagerSingleton();
			// this
			bind(InvoiceLoader.class)
				.asEagerSingleton();
		}
		
	}

	public static void main(String[] args) {
		try {
			final InvoiceLoader instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/invloice-loader.properties"))
					.getInstance(InvoiceLoader.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Properties configuration;
	private final Charset charset;
	private final McmdbDAO dao;

	@Inject
	protected InvoiceLoader(@Named("configuration") Properties configuration,
			@Named("charset") Charset charset, McmdbDAO dao) throws IOException {
		super();
		this.configuration = configuration;
		this.charset = charset;
		this.dao = dao;
	}
	
	private void writeToOutput(OutputStream out, boolean echoToStdout, String line) throws IOException {
		if (echoToStdout)
			System.out.println(line);
		out.write(line.getBytes(charset));
		out.write('\n');
	}
	
	public InvoiceLoader startup() throws IOException {
		if ("true".equalsIgnoreCase(configuration.getProperty("invoice_loader.locked", "true"))) {
			throw new IllegalStateException("application locked");
		}
		return this;
	}

	public InvoiceLoader shutdown() throws IOException {
		return this;
	}

	private InvoiceLoader process(String[] args) throws SQLException, IOException {
		
		final int bindPort = Integer.parseInt(configuration
				.getProperty("invoice_loader.bind_port", configuration
						.getProperty("default.bind_port", "0")));

		// bind lock tcp port
		try (final ServerSocket socket = new ServerSocket(bindPort)) {
			logger.debug("socket bound to {}", socket.getLocalSocketAddress());
			
			// process input file(s)
			for (String filename : args)
				if (!filename.endsWith(".properties"))
					process(filename);

		}

		return this;
	}
	
	private void process(String filename) throws SQLException, IOException {

		logger.debug("filename {}", filename);

		// configuration params
		final File homeFolder = new File(configuration
				.getProperty("invoice_loader.home_folder"));
		final boolean echoToStdout = "true".equalsIgnoreCase(configuration
				.getProperty("invoice_loader.echo_to_stdout"));
		int skipRows = Integer.parseInt(configuration
				.getProperty("invoice_loader.skip_rows", "0"));
		final char delimiter = configuration
				.getProperty("invoice_loader.delimiter", ";").trim().charAt(0);	
		final int columns = Integer.parseInt(configuration
				.getProperty("invoice_loader.columns"));
		final int columnReportId = Integer.parseInt(configuration
				.getProperty("invoice_loader.column.report_id"));
		final int columnInvoiceId = Integer.parseInt(configuration
				.getProperty("invoice_loader.column.invoice_id"));
		final int columnAmount = Integer.parseInt(configuration
				.getProperty("invoice_loader.column.amount"));
		final int columnShareOutId = Integer.parseInt(configuration
				.getProperty("invoice_loader.column.share_out_id"));

		// local file(s)
		File inputFile = new File(filename);
		if (!inputFile.exists()) {
			inputFile = new File(homeFolder, filename);
			if (!inputFile.exists()) {
				logger.error("file not found: {}", filename);
				throw new IOException(String
						.format("file not found: %s", filename));
			}
		}
		final File outputFile = new File(inputFile.getAbsolutePath() + ".log");

		// process report
		logger.debug("processing input file {}", inputFile);
		final long startTimeMillis = System.currentTimeMillis();
		long elapsedTimeMillis = 0L;
		try (final OutputStream outputStream = new FileOutputStream(outputFile);
				final BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);
				final FileInputStream inputStream = new FileInputStream(inputFile);
				final InputStreamReader inputStreamReader = new InputStreamReader(inputStream, charset);
				final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				final CSVParser csvParser = new CSVParser(bufferedReader, CSVFormat.newFormat(delimiter)
						.withQuoteMode(QuoteMode.MINIMAL)
						.withQuote('"')
						.withIgnoreEmptyLines()
						.withIgnoreSurroundingSpaces())) {			

			// write header
			writeToOutput(bufferedOutputStream, echoToStdout,
					"STATO;IDENTIFICATIVO_REPORT;DESCRIZIONE_ERRORE");

			// skip (header) rows
			final Iterator<CSVRecord> csvIterator = csvParser.iterator();
			for (; skipRows > 0 && csvIterator.hasNext(); skipRows --) {
				final CSVRecord record = csvIterator.next();
				logger.debug("record skipped {}", record);
			}

			while (csvIterator.hasNext()) {
				final CSVRecord record = csvIterator.next();

				// check column number
				if (columns != record.size()) {
					writeToOutput(bufferedOutputStream, echoToStdout, String
							.format("KO;;\"riga %d: numero di colonne errato %d (valore atteso %d)\"",
									record.getRecordNumber(), record.size(), columns));
					continue;
				}

				// parse line
				final String reportId = record.get(columnReportId);
				final String invoiceId = record.get(columnInvoiceId);
				final String amount = record.get(columnAmount);
				final String shareOutId = record.get(columnShareOutId);
				
				// validate line
				BigDecimal decimalAmount = null;
				if (Strings.isNullOrEmpty(reportId)) {
					writeToOutput(bufferedOutputStream, echoToStdout, String
							.format("KO;;\"riga %d: colonna identificativo report mancante %s\"",
									record.getRecordNumber(), reportId));
					continue;
				}
				if (Strings.isNullOrEmpty(invoiceId)) {
					writeToOutput(bufferedOutputStream, echoToStdout, String
							.format("KO;%s;\"riga %d: colonna fattura mancante %s\"",
									reportId, record.getRecordNumber(), invoiceId));
					continue;
				}
				if (Strings.isNullOrEmpty(shareOutId)) {
					writeToOutput(bufferedOutputStream, echoToStdout, String
							.format("KO;%s;\"riga %d: colonna ripartizione mancante %s\"",
									reportId, record.getRecordNumber(), shareOutId));
					continue;
				}
				try {
					decimalAmount = new BigDecimal(amount);
				} catch (Exception e) {
					writeToOutput(bufferedOutputStream, echoToStdout, String
							.format("KO;%s;\"riga %d: importo non numerico %s\"",
									reportId, record.getRecordNumber(), amount));
					continue;
				}

				// update database record
				final int count = dao.executeUpdate("invoice_loader.sql.update_record",
						ImmutableMap.of("reportId", reportId,
								"invoiceId", invoiceId,
								"amount", decimalAmount.stripTrailingZeros().toPlainString(),
								"shareOutId", shareOutId));
				if (count > 0) {
					writeToOutput(bufferedOutputStream, echoToStdout, String
							.format("OK;%s;\"riga %d: elaborata correttamente\"",
									reportId, record.getRecordNumber()));
				} else {
					writeToOutput(bufferedOutputStream, echoToStdout, String
							.format("KO;%s;\"riga %d: identificativo report non valido %s\"",
									reportId, record.getRecordNumber(), reportId));
				}
				
			}

		}
		
		elapsedTimeMillis = System.currentTimeMillis() - startTimeMillis;
		logger.debug("invoice loading completed in {}",
				TextUtils.formatDuration(elapsedTimeMillis));
		
	}

}
