package com.alkemytech.sophia.goal.pojo;

import java.math.BigDecimal;

import com.alkemytech.sophia.commons.util.BigDecimals;
import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ParquetRoyalty {

	public static final BigDecimal ONE_HUNDRED = new BigDecimal(100.0);
	
	public static final String ROYALTY_TYPE_DEM = "DEM";
	public static final String ROYALTY_TYPE_DRM = "DRM";
	
	public static BigDecimal computeDemRate(BigDecimal numerator, BigDecimal denominator) {
		if (null == numerator)
			return BigDecimal.ZERO;
		if (null == denominator)
			return BigDecimal.ZERO;
		if (BigDecimals.isAlmostZero(numerator))
			return BigDecimal.ZERO;
		if (BigDecimals.isAlmostZero(denominator))
			return BigDecimal.ZERO;
		final BigDecimal rate = BigDecimals.divide(numerator, denominator);
		return BigDecimals.isAlmostZero(rate) ?
				BigDecimal.ZERO : rate.stripTrailingZeros();
	}
	

	public static BigDecimal computeDrmRate(BigDecimal percentage) {
		if (null == percentage)
			return BigDecimal.ZERO;
		if (BigDecimals.isAlmostZero(percentage))
			return BigDecimal.ZERO;
		final BigDecimal rate = BigDecimals.divide(percentage, ONE_HUNDRED);
		return BigDecimals.isAlmostZero(rate) ?
				BigDecimal.ZERO : rate.stripTrailingZeros();
	}

	public final String ipiCode;				// 181888820
	public final String siaePosition;			// 128940
	public final BigDecimal demDenominator;		// 24
	public final BigDecimal demNumerator;		// 12
	public final String repertoireId;			// 1
	public final String role;					// CA
	public final String royaltyType;			// DEM
	public final BigDecimal drmPercentage;		// 100
	public final String name;					// SOSPESO PER EDITORE ORIGINALE
	public final String accountCode;			// 09
	public final String accountId;				// 000099966
	public final BigDecimal shr;				// 100
	public final String society;				// SIAE
	public final Integer irregularityType;		// 68
	
	public ParquetRoyalty(String ipiCode, String siaePosition, BigDecimal demDenominator,
			BigDecimal demNumerator, String repertoireId, String role, String royaltyType, BigDecimal drmPercentage,
			String name, String accountCode, String accountId, BigDecimal shr, String society, Integer irregularityType) {
		super();
		this.ipiCode = ipiCode;
		this.siaePosition = siaePosition;
		this.demDenominator = demDenominator;
		this.demNumerator = demNumerator;
		this.repertoireId = repertoireId;
		this.role = role;
		this.royaltyType = royaltyType;
		this.drmPercentage = drmPercentage;
		this.name = name;
		this.accountCode = accountCode;
		this.accountId = accountId;
		this.shr = shr;
		this.society = society;
		this.irregularityType = irregularityType;
	}
	
	public Royalty toRoyalty() {
		if (ParquetRoyalty.ROYALTY_TYPE_DEM.equals(royaltyType))
			return new DemRoyalty(ipiCode, 
					siaePosition, role, accountCode,
					accountId, society, irregularityType, 
					demDenominator, demNumerator);
		if (ParquetRoyalty.ROYALTY_TYPE_DRM.equals(royaltyType))
			return new DrmRoyalty(ipiCode, 
					siaePosition, role, accountCode,
					accountId, society, irregularityType,
					drmPercentage);
		throw new IllegalArgumentException(String
				.format("unknown royalty type \"%s\"", royaltyType));
	}
	
	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
