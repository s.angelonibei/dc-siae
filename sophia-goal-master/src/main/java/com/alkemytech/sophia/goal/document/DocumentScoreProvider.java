package com.alkemytech.sophia.goal.document;

import java.util.Properties;

import com.google.inject.Provider;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class DocumentScoreProvider implements Provider<DocumentScore> {
	
	private final Provider<DocumentScore> provider;
	
	public DocumentScoreProvider(final Properties configuration, final String propertyPrefix) {
		super();
		final String className = configuration
				.getProperty(propertyPrefix + ".class_name");
		if (LinearDocumentScore.class.getName().equals(className)) {
			provider = new Provider<DocumentScore>() {
				@Override
				public DocumentScore get() {
					return new LinearDocumentScore(configuration, propertyPrefix);
				}
			}; 
		} else if (GeometricDocumentScore.class.getName().equals(className)) {
			provider = new Provider<DocumentScore>() {
				@Override
				public DocumentScore get() {
					return new GeometricDocumentScore(configuration, propertyPrefix);
				}
			}; 
		} else {
			throw new IllegalArgumentException(String
					.format("unknown class name \"%s\"", className));
		}
	}
	
	@Override
	public DocumentScore get() {
		return provider.get();
	}
	
}
