package com.alkemytech.sophia.goal.royalty;

import java.io.File;
import java.nio.charset.Charset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.util.FileUtils;
import com.alkemytech.sophia.goal.pojo.RoyaltyScheme;
import com.google.common.base.Strings;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseEntry;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.je.LockMode;
import com.sleepycat.je.OperationStatus;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class BdbRoyaltyDatabase implements RoyaltyDatabase {

	private final Logger logger = LoggerFactory.getLogger(BdbRoyaltyDatabase.class);
	
	private static class ThreadLocalRoyaltySchemeBinding extends ThreadLocal<BdbRoyaltySchemeBinding> {

	    @Override
	    protected synchronized BdbRoyaltySchemeBinding initialValue() {
	        return new BdbRoyaltySchemeBinding();
	    }

	}

	private final File environmentHome;
	private final boolean readOnly;
	private final boolean deferredWrite;
	private final Charset charset;
	private final ThreadLocal<BdbRoyaltySchemeBinding> binding;

	private Environment environment;
	private Database database;
	private RoyaltyDatabaseMetadata metadata;

	public BdbRoyaltyDatabase(File environmentHome, boolean readOnly, boolean deferredWrite, Charset charset, boolean deleteExistingFiles) {
		super();
		this.environmentHome = environmentHome;
		this.charset = charset;
		this.readOnly = readOnly;
		this.deferredWrite = !readOnly && deferredWrite;
		this.binding = new ThreadLocalRoyaltySchemeBinding();
		if (!readOnly && deleteExistingFiles && environmentHome.exists())
			FileUtils.deleteFolder(environmentHome, false);
		environmentHome.mkdirs();
		environment = new Environment(environmentHome,
				(EnvironmentConfig) EnvironmentConfig.DEFAULT
					.setReadOnly(readOnly)
					.setAllowCreate(!readOnly)
					.setTransactional(false));
		database = environment.openDatabase(null, "royaltyScheme", 
				DatabaseConfig.DEFAULT
					.setReadOnly(readOnly)
					.setAllowCreate(!readOnly)
					.setTransactional(false)
					.setDeferredWrite(deferredWrite));
	}

	@Override
	public RoyaltyDatabaseMetadata getMetadata() {
		if (null == metadata)
			metadata = new RoyaltyDatabaseMetadata(environmentHome);
		return metadata;
	}
	
	@Override
	public File getHomeFolder() {
		return environmentHome;
	}
	
	@Override
	public boolean isReadOnly() {
		return readOnly;
	}
	
	@Override
	public synchronized void close() {
		try {
			if (null != database) {
				if (deferredWrite) {
					database.sync();
				}
				database.close();
				database = null;
			}
		} catch (DatabaseException e) {
			logger.error("close", e);
		}
		try {
			if (null != environment) {
				if (!readOnly) {
					environment.sync();
					environment.cleanLog();
				}
				environment.close();
				environment = null;
			}
		} catch (DatabaseException e) {
			logger.error("close", e);
		}
	}
		
	@Override
	public RoyaltyScheme getScheme(String code) {
		if (Strings.isNullOrEmpty(code)) {
			throw new IllegalArgumentException("code");
		}
		final DatabaseEntry keyEntry = new DatabaseEntry(code.getBytes(charset));
		final DatabaseEntry dataEntry = new DatabaseEntry();
		final OperationStatus status = database
				.get(null, keyEntry, dataEntry, LockMode.DEFAULT);
		if (status == OperationStatus.SUCCESS) {
			return binding.get().entryToObject(dataEntry);
		}
		return null;
	}

	@Override
	public boolean putScheme(RoyaltyScheme scheme) {		
		final DatabaseEntry keyEntry = new DatabaseEntry(scheme.code.getBytes(charset));
		final DatabaseEntry dataEntry = new DatabaseEntry();
		binding.get().objectToEntry(scheme, dataEntry);
		final OperationStatus status = database.put(null, keyEntry, dataEntry);
		return OperationStatus.SUCCESS == status;
	}

}
