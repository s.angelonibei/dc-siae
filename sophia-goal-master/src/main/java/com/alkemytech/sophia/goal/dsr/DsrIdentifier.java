package com.alkemytech.sophia.goal.dsr;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.index.ReverseIndex;
import com.alkemytech.sophia.commons.index.ReverseIndexProvider;
import com.alkemytech.sophia.commons.query.TitleArtistQuery;
import com.alkemytech.sophia.commons.query.TitleArtistQueryProvider;
import com.alkemytech.sophia.commons.text.TextNormalizer;
import com.alkemytech.sophia.commons.text.TextNormalizerProvider;
import com.alkemytech.sophia.commons.text.TextTokenizer;
import com.alkemytech.sophia.commons.text.TextTokenizerProvider;
import com.alkemytech.sophia.commons.util.SplitCharSequence;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.goal.collecting.BdbCollectingDatabase;
import com.alkemytech.sophia.goal.collecting.CollectingDatabase;
import com.alkemytech.sophia.goal.collecting.CollectingDatabaseProvider;
import com.alkemytech.sophia.goal.document.DocumentScore;
import com.alkemytech.sophia.goal.document.DocumentScoreProvider;
import com.alkemytech.sophia.goal.document.DocumentSearch;
import com.alkemytech.sophia.goal.document.DocumentStore;
import com.alkemytech.sophia.goal.document.DocumentStoreProvider;
import com.alkemytech.sophia.goal.document.ScoredDocument;
import com.alkemytech.sophia.goal.model.CollectingCode;
import com.google.common.base.Strings;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class DsrIdentifier {
	
	private static final Logger logger = LoggerFactory.getLogger(DsrIdentifier.class);
	
	private static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.toInstance(new S3(configuration));
			// normalizer(s)
			bind(TextNormalizer.class)
				.annotatedWith(Names.named("iswc_normalizer"))
				.toProvider(new TextNormalizerProvider(configuration, "iswc_normalizer"));
			bind(TextNormalizer.class)
				.annotatedWith(Names.named("isrc_normalizer"))
				.toProvider(new TextNormalizerProvider(configuration, "isrc_normalizer"));
			bind(TextNormalizer.class)
				.annotatedWith(Names.named("title_normalizer"))
				.toProvider(new TextNormalizerProvider(configuration, "title_normalizer"));
			bind(TextNormalizer.class)
				.annotatedWith(Names.named("artist_normalizer"))
				.toProvider(new TextNormalizerProvider(configuration, "artist_normalizer"));
			// tokenizer(s)
			bind(TextTokenizer.class)
				.annotatedWith(Names.named("title_tokenizer"))
				.toProvider(new TextTokenizerProvider(configuration, "title_tokenizer"));
			bind(TextTokenizer.class)
				.annotatedWith(Names.named("artist_tokenizer"))
				.toProvider(new TextTokenizerProvider(configuration, "artist_tokenizer"));
			// title artist quer(y/ies)
			bind(TitleArtistQuery.class)
				.toProvider(new TitleArtistQueryProvider(configuration, 
						configuration.getProperty("dsr_identifier.title_artist_query")));
			// document score(s)
			bind(DocumentScore.class)
				.toProvider(new DocumentScoreProvider(configuration,
						configuration.getProperty("dsr_identifier.document_score")));
			// reverse index(es)
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("iswc_index"))
				.toProvider(new ReverseIndexProvider(configuration, "iswc_index"))
				.asEagerSingleton();
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("isrc_index"))
				.toProvider(new ReverseIndexProvider(configuration, "isrc_index"))
				.asEagerSingleton();
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("title_index"))
				.toProvider(new ReverseIndexProvider(configuration, "title_index"))
				.asEagerSingleton();
			bind(ReverseIndex.class)
				.annotatedWith(Names.named("artist_index"))
				.toProvider(new ReverseIndexProvider(configuration, "artist_index"))
				.asEagerSingleton();
			// collecting database
			bind(CollectingDatabase.class)
				.toProvider(new CollectingDatabaseProvider(configuration, "collecting_database"))
				.asEagerSingleton();			
			// document store
			bind(DocumentStore.class)
				.toProvider(new DocumentStoreProvider(configuration, "document_store"))
				.asEagerSingleton();
			// this
			bind(DsrIdentifier.class)
				.asEagerSingleton();
		}

	}
	
	public static void main(String[] args) {
		try {
			final DsrIdentifier instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/dsr-identifier.properties"))
					.getInstance(DsrIdentifier.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Properties configuration;
	private final BdbCollectingDatabase collectingDatabase;
	private final DocumentStore documentStore;
	private final S3 s3;
	private final ReverseIndex iswcIndex;
	private final ReverseIndex isrcIndex;
	private final ReverseIndex titleIndex;
	private final ReverseIndex artistIndex;
	private final Provider<TextNormalizer> iswcNormalizerProvider;
	private final Provider<TextNormalizer> isrcNormalizerProvider;
	private final Provider<TextNormalizer> titleNormalizerProvider;
	private final Provider<TextNormalizer> artistNormalizerProvider;
	private final Provider<DocumentSearch> documentSearchProvider;

	@Inject
	protected DsrIdentifier(@Named("configuration") Properties configuration,
			S3 s3, BdbCollectingDatabase collectingDatabase,
			DocumentStore documentStore,
			@Named("iswc_index") ReverseIndex iswcIndex,
			@Named("isrc_index") ReverseIndex isrcIndex,
			@Named("title_index") ReverseIndex titleIndex,
			@Named("artist_index") ReverseIndex artistIndex,
			@Named("iswc_normalizer") Provider<TextNormalizer> iswcNormalizerProvider,
			@Named("isrc_normalizer") Provider<TextNormalizer> isrcNormalizerProvider,
			@Named("title_normalizer") Provider<TextNormalizer> titleNormalizerProvider,
			@Named("artist_normalizer") Provider<TextNormalizer> artistNormalizerProvider,
			Provider<DocumentSearch> documentSearchProvider) throws IOException {
		super();
		this.configuration = configuration;
		this.s3 = s3;
		this.collectingDatabase = collectingDatabase;
		this.documentStore = documentStore;
		this.iswcIndex = iswcIndex;
		this.isrcIndex = isrcIndex;
		this.titleIndex = titleIndex;
		this.artistIndex = artistIndex;
		this.iswcNormalizerProvider = iswcNormalizerProvider;
		this.isrcNormalizerProvider = isrcNormalizerProvider;
		this.titleNormalizerProvider = titleNormalizerProvider;
		this.artistNormalizerProvider = artistNormalizerProvider;
		this.documentSearchProvider = documentSearchProvider;
	}
	
	public DsrIdentifier startup() throws IOException {
		if ("true".equalsIgnoreCase(configuration.getProperty("dsr_identifier.locked", "true"))) {
			throw new IllegalStateException("application locked");
		}
		s3.startup();
		collectingDatabase.startup();
		documentStore.startup();
		return this;
	}

	public DsrIdentifier shutdown() {
		documentStore.shutdown();
		collectingDatabase.shutdown();
		s3.shutdown();
		return this;
	}
	
	public DsrIdentifier process(String[] args) throws IOException, SQLException, InterruptedException {
		
		final int bindPort = Integer.parseInt(configuration
				.getProperty("dsr_identifier.bind_port", configuration
						.getProperty("default.bind_port", "0")));

		// bind lock tcp port
		try (final ServerSocket socket = new ServerSocket(bindPort)) {
			logger.debug("socket bound to {}", socket.getLocalSocketAddress());
			
			process();
			
		}
		
		return this;
	}

	private void process() throws IOException, SQLException, InterruptedException {
		
		final int threadCount = Integer.parseInt(configuration
				.getProperty("dsr_identifier.thread_count", "1"));

//		final String s3FileUrl = configuration
//			.getProperty("dsr_identifier.s3_file_url");
		final File inputFile = new File(configuration
				.getProperty("dsr_identifier.input_file"));
		final File outputFile = new File(configuration
				.getProperty("dsr_identifier.output_file"));

		final char delimiter = configuration
				.getProperty("dsr_identifier.delimiter", ";").trim().charAt(0);
		final String splitRegex = configuration
				.getProperty("dsr_identifier.split_regex", "\\|").trim();
		final int columnTitle = Integer.parseInt(configuration
				.getProperty("dsr_identifier.column.title"));
		final int columnArtists = Integer.parseInt(configuration
				.getProperty("dsr_identifier.column.artists"));
		final int columnIswc = Integer.parseInt(configuration
				.getProperty("dsr_identifier.column.iswc", "-1"));
		final int columnIsrc = Integer.parseInt(configuration
				.getProperty("dsr_identifier.column.isrc", "-1"));
		final int columnOriginalTitle = Integer.parseInt(configuration
				.getProperty("dsr_identifier.column.original_title", "-1"));
		final int columnOriginalArtists = Integer.parseInt(configuration
				.getProperty("dsr_identifier.column.original_artists", "-1"));
		final int columnOriginalIswc = Integer.parseInt(configuration
				.getProperty("dsr_identifier.column.original_iswc", "-1"));
		final int columnOriginalIsrc = Integer.parseInt(configuration
				.getProperty("dsr_identifier.column.original_isrc", "-1"));
		
		final double iswcMinScore = Double.parseDouble(configuration
				.getProperty("dsr_identifier.min_score.iswc"));
		final double isrcMinScore = Double.parseDouble(configuration
				.getProperty("dsr_identifier.min_score.isrc"));
		final double titleArtistMinScore = Double.parseDouble(configuration
				.getProperty("dsr_identifier.min_score.title_artist"));
		final double autoScore = Double.parseDouble(configuration
				.getProperty("dsr_identifier.auto_score", "0.999"));
		final int maxPostings = Integer.parseInt(configuration
				.getProperty("dsr_identifier.max_postings"));
		
		final long statsFrequency = TextUtils.parseLongDuration(configuration
				.getProperty("dsr_identifier.stats_frequency"));

		// statistics
		final AtomicLong totalRecords = new AtomicLong(0L);
		final AtomicLong identifiedRecords = new AtomicLong(0L);
		final AtomicLong identifiedRecordsIswc = new AtomicLong(0L);
		final AtomicLong identifiedRecordsOriginalIswc = new AtomicLong(0L);
		final AtomicLong identifiedRecordsIsrc = new AtomicLong(0L);
		final AtomicLong identifiedRecordsOriginalIsrc = new AtomicLong(0L);
		final AtomicLong identifiedRecordsTitleArtist = new AtomicLong(0L);
		final AtomicLong identifiedRecordsOriginalTitleArtist = new AtomicLong(0L);
		final AtomicLong searchesIswc = new AtomicLong(0L);
		final AtomicLong searchesOriginalIswc = new AtomicLong(0L);
		final AtomicLong searchesIsrc = new AtomicLong(0L);
		final AtomicLong searchesOriginalIsrc = new AtomicLong(0L);
		final AtomicLong searchesTitleArtist = new AtomicLong(0L);
		final AtomicLong searchesOriginalTitleArtist = new AtomicLong(0L);

//		logger.debug("processing s3 file {}", s3FileUrl);
		logger.debug("processing input file {}", inputFile);
		final long startTimeMillis = System.currentTimeMillis();
		long elapsedTimeMillis = 0L;
		
//		final S3Object object = s3.getObject(new S3.Url(s3FileUrl));
		try (final OutputStream outputStream = new FileOutputStream(outputFile);
				final BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);
				final GZIPOutputStream gzipOutputStream = new GZIPOutputStream(bufferedOutputStream);
//					final S3ObjectInputStream inputStream = object.getObjectContent();
				final FileInputStream inputStream = new FileInputStream(inputFile);
				final GZIPInputStream gzipInputStream = new GZIPInputStream(inputStream);
				final InputStreamReader inputStreamReader = new InputStreamReader(gzipInputStream, Charset.forName("UTF-8"));
				final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				final CSVParser csvParser = new CSVParser(bufferedReader, CSVFormat.newFormat(delimiter)
						.withQuoteMode(QuoteMode.MINIMAL)
						.withQuote('"')
						.withIgnoreEmptyLines()
						.withIgnoreSurroundingSpaces())) {				
			
			final Iterator<CSVRecord> csvIterator = csvParser.iterator();
			final Object monitor = new Object();
			final ExecutorService executorService = Executors.newCachedThreadPool();
			final AtomicInteger runningThreads = new AtomicInteger(0);
			while (runningThreads.get() < threadCount) {
				logger.debug("starting thread {}", runningThreads.incrementAndGet());
				executorService.execute(new Runnable() {
					
					@Override
					public void run() {
						try {
							
							final TextNormalizer iswcNormalizer = iswcNormalizerProvider.get();
							final TextNormalizer isrcNormalizer = isrcNormalizerProvider.get();
							final TextNormalizer titleNormalizer = titleNormalizerProvider.get();
							final TextNormalizer artistNormalizer = artistNormalizerProvider.get();
							final DocumentSearch documentSearch = documentSearchProvider.get();
							
							while (true) {

								final CSVRecord record;
								synchronized (csvIterator) {
									if (csvIterator.hasNext()) {
										record = csvIterator.next();
									} else {
										return;
									}
								}
							
								ScoredDocument document = null;
								IdentificationType source = null;

								// title
								String title = record.get(columnTitle);
								title = titleNormalizer.normalize(title);
								if (!Strings.isNullOrEmpty(title)) {

									SplitCharSequence normalizedTitle = new SplitCharSequence(title);

									// artists
									String artists = record.get(columnArtists);
									String[] splitArtists = artists.split(splitRegex);
									final Set<SplitCharSequence> normalizedArtists = new HashSet<>(splitArtists.length);
									for (String splittedArtist : splitArtists) {
										splittedArtist = artistNormalizer.normalize(splittedArtist);
										if (!Strings.isNullOrEmpty(splittedArtist)) {
											normalizedArtists.add(new SplitCharSequence(splittedArtist));
										}
									}						
									if (!normalizedArtists.isEmpty()) {

										// iswc
										if (null == document && -1 != columnIswc) {
											final String iswc = iswcNormalizer.normalize(record.get(columnIswc));
											if (!Strings.isNullOrEmpty(iswc)) {
												searchesIswc.incrementAndGet();
												document = documentSearch.search(iswcIndex,
														normalizedTitle, normalizedArtists, iswc, maxPostings, autoScore);
												if (null != document && document.score >= iswcMinScore) {
													source = IdentificationType.iswc;
													identifiedRecordsIswc.incrementAndGet();
												}
											}
										}

										// original_iswc
										if (null == document && -1 != columnOriginalIswc) {
											final String originalIswc = iswcNormalizer.normalize(record.get(columnOriginalIswc));
											if (!Strings.isNullOrEmpty(originalIswc)) {
												document = documentSearch.search(iswcIndex,
														normalizedTitle, normalizedArtists, originalIswc, maxPostings, autoScore);
												if (null != document && document.score >= iswcMinScore) {
													searchesOriginalIswc.incrementAndGet();
													source = IdentificationType.original_iswc;
													identifiedRecordsOriginalIswc.incrementAndGet();
												}
											}
										}

										// isrc
										if (null == document && -1 != columnIsrc) {
											final String isrc = isrcNormalizer.normalize(record.get(columnIsrc));
											if (!Strings.isNullOrEmpty(isrc)) {
												searchesIsrc.incrementAndGet();
												document = documentSearch.search(isrcIndex,
														normalizedTitle, normalizedArtists, isrc, maxPostings, autoScore);
												if (null != document && document.score >= isrcMinScore) {
													source = IdentificationType.isrc;
													identifiedRecordsIsrc.incrementAndGet();
												}
											}
										}
										
										// original_isrc
										if (null == document && -1 != columnOriginalIsrc) {
											final String originalIsrc = isrcNormalizer.normalize(record.get(columnOriginalIsrc));
											if (!Strings.isNullOrEmpty(originalIsrc)) {
												searchesOriginalIsrc.incrementAndGet();
												document = documentSearch.search(isrcIndex,
														normalizedTitle, normalizedArtists, originalIsrc, maxPostings, autoScore);
												if (null != document && document.score >= isrcMinScore) {
													source = IdentificationType.original_isrc;
													identifiedRecordsOriginalIsrc.incrementAndGet();
												}
											}
										}

										if (null == document) {
											searchesTitleArtist.incrementAndGet();
											document = documentSearch.search(titleIndex, artistIndex,
													normalizedTitle, normalizedArtists, maxPostings, titleArtistMinScore);
											if (null != document) {
												source = IdentificationType.title_artists;
												identifiedRecordsTitleArtist.incrementAndGet();
											}
										}
										
										if (null == document && -1 != columnOriginalTitle && -1 != columnOriginalArtists) {
											
											// original_title
											title = record.get(columnOriginalTitle);
											title = titleNormalizer.normalize(title);
											if (!Strings .isNullOrEmpty(title)) {
												normalizedTitle = new SplitCharSequence(title);
												// original_artists
												artists = record.get(columnOriginalArtists);
												splitArtists = artists.split(splitRegex);
												normalizedArtists.clear();
												for (String splitArtist : splitArtists) {
													splitArtist = artistNormalizer.normalize(splitArtist);
													if (!Strings.isNullOrEmpty(splitArtist)) {
														normalizedArtists.add(new SplitCharSequence(splitArtist));
													}
												}
												if (!normalizedArtists.isEmpty()) {
													searchesOriginalTitleArtist.incrementAndGet();													
													document = documentSearch.search(titleIndex, artistIndex,
															normalizedTitle, normalizedArtists, maxPostings, autoScore);
													if (null != document && document.score >= titleArtistMinScore) {
														source = IdentificationType.original_title_artists;
														identifiedRecordsOriginalTitleArtist.incrementAndGet();
													}
												}
											}
											
										}
										
									}
									
								}

								// format output line
								final StringBuilder outputLine = new StringBuilder();
								if (null != document) {
									identifiedRecords.incrementAndGet();
									final CollectingCode collecting = collectingDatabase
											.getByWorkId(document.document.workId);
									if (null != collecting) {
										outputLine
											.append(document.document.workId)
											.append(delimiter)
											.append(collecting.code)
											.append(delimiter)
											.append(source);
									} else {
										outputLine
											.append(document.document.workId)
											.append(delimiter)
											.append(delimiter)
											.append(source);
									}
								} else {
									outputLine.append(delimiter)
										.append(delimiter);						
								}
								for (String field : record) {
									outputLine.append(delimiter)
										.append(field);
								}
								outputLine.append('\n');
								
								// write to output
								final byte[] outputBytes = outputLine
										.toString().getBytes("UTF-8");
								synchronized (gzipOutputStream) {
									gzipOutputStream.write(outputBytes);									
								}

								totalRecords.incrementAndGet();
							
							}
							
						} catch (IOException e) {
							logger.error("run", e);
						} finally {
							logger.debug("thread {} finished", runningThreads.getAndDecrement());
							synchronized (monitor) {
								monitor.notify();
							}
						}
					}

				});
			}
			
			while (runningThreads.get() > 0) {
				synchronized (monitor) {
					monitor.wait(statsFrequency);
				}
				
				elapsedTimeMillis = System.currentTimeMillis() - startTimeMillis;
				// print stats
				logger.debug("---------- stats ----------");
				logger.debug("processing time {}", 
						TextUtils.formatDuration(elapsedTimeMillis));
				logger.debug("average line processing time {}ms", 
						Math.round((double) elapsedTimeMillis / (double) totalRecords.get()));
				logger.debug("processed {}", totalRecords);
				logger.debug("  unidentified {} ({}%)", totalRecords.get() - identifiedRecords.get(),
						Math.round(100.0 * (totalRecords.get() - identifiedRecords.get()) / (double) totalRecords.get()));
				logger.debug("  identified {} ({}%)", identifiedRecords,
						Math.round(100.0 * identifiedRecords.get() / (double) totalRecords.get()));
				if (-1 != columnIswc) {
					logger.debug("    iswc {} ({}%)", identifiedRecordsIswc,
							Math.round(100.0 * identifiedRecordsIswc.get() / (double) totalRecords.get()));
				}
				if (-1 != columnOriginalIswc) {
					logger.debug("    original_iswc {} ({}%)", identifiedRecordsOriginalIswc,
							Math.round(100.0 * identifiedRecordsOriginalIswc.get() / (double) totalRecords.get()));
				}
				if (-1 != columnIsrc) {
					logger.debug("    isrc {} ({}%)", identifiedRecordsIsrc,
							Math.round(100.0 * identifiedRecordsIsrc.get() / (double) totalRecords.get()));
				}
				if (-1 != columnOriginalIsrc) {
					logger.debug("    original_isrc {} ({}%)", identifiedRecordsOriginalIsrc,
							Math.round(100.0 * identifiedRecordsOriginalIsrc.get() / (double) totalRecords.get()));
				}
				logger.debug("    title_artist {} ({}%)", identifiedRecordsTitleArtist,
						Math.round(100.0 * identifiedRecordsTitleArtist.get() / (double) totalRecords.get()));
				if (-1 != columnOriginalTitle && -1 != columnOriginalArtists) {
					logger.debug("    original_title_artist {} ({}%)", identifiedRecordsOriginalTitleArtist,
							Math.round(100.0 * identifiedRecordsOriginalTitleArtist.get() / (double) totalRecords.get()));
				}
				logger.debug("  searches");
				if (-1 != columnIswc) {
					logger.debug("    iswc {}", searchesIswc);
				}
				if (-1 != columnOriginalIswc) {
					logger.debug("    original_iswc {}", searchesOriginalIswc);
				}
				if (-1 != columnIsrc) {
					logger.debug("    isrc {}", searchesIsrc);
				}
				if (-1 != columnOriginalIsrc) {
					logger.debug("    original_isrc {}", searchesOriginalIsrc);
				}
				logger.debug("    title_artist {}", searchesTitleArtist);
				if (-1 != columnOriginalTitle && -1 != columnOriginalArtists) {
					logger.debug("    original_title_artist {}", searchesOriginalTitleArtist);
				}
			}
			
		}
		
		logger.debug("{} records processed", totalRecords);
		elapsedTimeMillis = System.currentTimeMillis() - startTimeMillis;
		logger.debug("identification completed in {}",
				TextUtils.formatDuration(elapsedTimeMillis));
	}

}