package com.alkemytech.sophia.goal.work;

import java.io.File;
import java.io.IOException;

import com.alkemytech.sophia.goal.model.Work;
import com.alkemytech.sophia.goal.model.Work.Selector;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface WorkDatabase {

	public WorkDatabase startup() throws IOException;
	public WorkDatabase shutdown();
	public boolean isReadOnly();
	public File getHomeFolder();
	public long getNextId() throws IOException;
	public Work getById(long id) throws IOException;
	public Work upsert(Work work) throws IOException;
	public boolean delete(long id) throws IOException;
	public WorkDatabase select(Selector selector) throws Exception;
	public WorkDatabase truncate() throws IOException;
	public WorkDatabase defrag() throws IOException;

}
