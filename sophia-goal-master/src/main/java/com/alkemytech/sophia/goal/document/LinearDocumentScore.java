package com.alkemytech.sophia.goal.document;

import java.util.Collection;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.score.TextScore;
import com.alkemytech.sophia.commons.util.SplitCharSequence;
import com.alkemytech.sophia.goal.model.Document;
import com.alkemytech.sophia.goal.model.Text;
import com.alkemytech.sophia.goal.model.TextType;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class LinearDocumentScore implements DocumentScore {
	
	private static final Logger logger = LoggerFactory.getLogger(LinearDocumentScore.class);

	private final double titleWeight;
	private final double titleExponent;
	private final TextScore titleScore;
	private final double artistWeight;
	private final double artistExponent;
	private final TextScore artistScore;
	
	public LinearDocumentScore(Properties configuration, String propertyPrefix) {
		super();
		titleWeight = Double.parseDouble(configuration
				.getProperty(propertyPrefix + ".title_weight"));
		titleExponent = Double.parseDouble(configuration
				.getProperty(propertyPrefix + ".title_exponent"));
		titleScore = newScoreInstance(configuration
				.getProperty(propertyPrefix + ".title_score"));
		artistWeight = Double.parseDouble(configuration
				.getProperty(propertyPrefix + ".artist_weight"));
		artistExponent = Double.parseDouble(configuration
				.getProperty(propertyPrefix + ".artist_exponent"));
		artistScore = newScoreInstance(configuration
				.getProperty(propertyPrefix + ".artist_score"));
		final double weightsSum = Math.abs(titleWeight + artistWeight);
		if (weightsSum > 1.0) {
			logger.warn("non unit weights sum {}", weightsSum);
		}
	}
	
	private TextScore newScoreInstance(String className) {
		try {
			return (TextScore) Class.forName(className).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			throw new IllegalArgumentException(String
					.format("unknown class name \"%s\"", className), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.seqrware.copyright.score.DocumentScore#getScore(com.seqrware.copyright.score.SplitCharSequence, java.util.Collection, com.seqrware.copyright.model.Document)
	 */
	@Override
	public double getScore(SplitCharSequence title, Collection<SplitCharSequence> artists, Document document) {
		double titleScore = 0.0;
		double artistScore = 0.0;
		for (Text text : document.texts) {
			if (TextType.title == text.type) {
				if (titleScore < 1.0) {
					final double score = this.titleScore
							.getScore(title, new SplitCharSequence(text.text));
					if (score > titleScore) {
						titleScore = score;
					}
				}
			} else if (TextType.artist == text.type) {
				if (artistScore < 1.0) {
					for (SplitCharSequence artist : artists) {
						final double score = this.artistScore
								.getScore(artist, new SplitCharSequence(text.text));
						if (score > artistScore) {
							artistScore = score;
						}
					}
				}
			}
		}
		return titleWeight * Math.pow(titleScore, titleExponent) + 
				artistWeight * Math.pow(artistScore, artistExponent);
	}
	
}
