package com.alkemytech.sophia.goal.pojo;

import java.math.BigDecimal;

import com.alkemytech.sophia.commons.util.BigDecimals;
import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class DemRoyalty extends Royalty {

	// original parquet file fields
	public BigDecimal denominator;				// 24
	public BigDecimal numerator;				// 12

	// computed fields
	public boolean exArt18Excluded;				// true | false

	// WARNING: la trattenuta ex art 18 viene effettuata solo sulle quote DEM
	
	public DemRoyalty(String ipiCode, String siaePosition, String role, String accountCode, String accountId,
			String society, Integer irregularityType, BigDecimal denominator, BigDecimal numerator) {
		super();
		this.ipiCode = ipiCode;
		this.siaePosition = siaePosition;
		this.role = role;
		this.accountCode = accountCode;
		this.accountId = accountId;
		this.society = society;
		this.irregularityType = irregularityType;
		this.denominator = denominator = BigDecimals.isAlmostZero(denominator) ?
				BigDecimal.ZERO : BigDecimals.setScale(denominator).stripTrailingZeros();
		this.numerator = numerator = BigDecimals.isAlmostZero(numerator) ?
				BigDecimal.ZERO : BigDecimals.setScale(numerator).stripTrailingZeros();
		this.rate = ParquetRoyalty.computeDemRate(numerator, denominator);
	}
	
	public DemRoyalty(String ipiCode, String siaePosition, String role, String accountCode, String accountId,
			String society, Integer irregularityType, boolean notAssociated, boolean specialAccount, boolean publicDomain, boolean siae,
			String angloAmericanRepertoire, BigDecimal denominator, BigDecimal numerator, boolean exArt18Excluded) {
		super();		
		this.ipiCode = ipiCode;
		this.siaePosition = siaePosition;
		this.role = role;
		this.accountCode = accountCode;
		this.accountId = accountId;
		this.society = society;
		this.irregularityType = irregularityType;
		this.notAssociated = notAssociated;
		this.specialAccount = specialAccount;
		this.publicDomain = publicDomain;
		this.siae = siae;
		this.angloAmericanRepertoire = angloAmericanRepertoire;
		this.denominator = denominator = BigDecimals.isAlmostZero(denominator) ?
				BigDecimal.ZERO : BigDecimals.setScale(denominator).stripTrailingZeros();
		this.numerator = numerator = BigDecimals.isAlmostZero(numerator) ?
				BigDecimal.ZERO : BigDecimals.setScale(numerator).stripTrailingZeros();
		this.exArt18Excluded = exArt18Excluded;
		this.rate = ParquetRoyalty.computeDemRate(numerator, denominator);
	}
		
	@Override
	public boolean isDem() {
		return true;
	}

	@Override
	public boolean isDrm() {
		return false;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}

}
