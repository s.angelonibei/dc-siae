package com.alkemytech.sophia.goal.model;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class TextType {

	public static final int undefined = 0;
	public static final int iswc = 1;
	public static final int isrc = 2;
	public static final int title = 3;
	public static final int artist = 4;

	public static int valueOf(String text) {
		if ("iswc".equals(text)) {
			return iswc;
		} else if ("isrc".equals(text)) {
			return isrc;
		} else if ("title".equals(text)) {
			return title;
		} else if ("artist".equals(text)) {
			return artist;
		}
		return undefined;
	}
	
	public static String toString(int textType) {
		switch (textType) {
		case iswc: return "iswc";
		case isrc: return "isrc";
		case title: return "title";
		case artist: return "artist";
		default: return "undefined";
		}
	}
	
}
