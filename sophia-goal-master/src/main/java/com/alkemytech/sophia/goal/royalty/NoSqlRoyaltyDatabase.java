package com.alkemytech.sophia.goal.royalty;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.mmap.ObjectCodec;
import com.alkemytech.sophia.commons.nosql.ConcurrentNoSql;
import com.alkemytech.sophia.commons.nosql.NoSql;
import com.alkemytech.sophia.commons.nosql.NoSqlConfig;
import com.alkemytech.sophia.commons.util.FileUtils;
import com.alkemytech.sophia.goal.pojo.RoyaltyScheme;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class NoSqlRoyaltyDatabase implements RoyaltyDatabase {

	private static class ThreadLocalObjectCodec extends ThreadLocal<ObjectCodec<RoyaltyScheme>> {

		private final Charset charset;
		
	    public ThreadLocalObjectCodec(Charset charset) {
			super();
			this.charset = charset;
		}

		@Override
	    protected synchronized ObjectCodec<RoyaltyScheme> initialValue() {
	        return new RoyaltySchemeCodec(charset);
	    }
	    
	}

	private final Logger logger = LoggerFactory.getLogger(NoSqlRoyaltyDatabase.class);
	
	private NoSql<RoyaltyScheme> database;
	private RoyaltyDatabaseMetadata metadata;

	public NoSqlRoyaltyDatabase(File homeFolder, boolean readOnly, int pageSize, int cacheSize, final Charset charset, boolean deleteExistingFiles) throws IOException {
		super();
		if (!readOnly && deleteExistingFiles && homeFolder.exists())
			FileUtils.deleteFolder(homeFolder, true);
		database = new ConcurrentNoSql<>(new NoSqlConfig<RoyaltyScheme>()
				.setHomeFolder(homeFolder)
				.setReadOnly(readOnly)
				.setPageSize(pageSize)
				.setCacheSize(cacheSize)
				.setCodec(new ThreadLocalObjectCodec(Charset.forName("UTF-8"))));
	}

	@Override
	public RoyaltyDatabaseMetadata getMetadata() {
		if (null == metadata)
			metadata = new RoyaltyDatabaseMetadata(database.getHomeFolder());
		return metadata;
	}

	@Override
	public File getHomeFolder() {
		return database.getHomeFolder();
	}
	
	@Override
	public boolean isReadOnly() {
		return database.isReadOnly();
	}
	
	@Override
	public void close() {
		try {
			database.close();
		} catch (IOException e) {
			logger.error("close", e);
		}
	}

	@Override
	public RoyaltyScheme getScheme(String code) {
		return database.getByPrimaryKey(code);
	}

	@Override
	public boolean putScheme(RoyaltyScheme scheme) {
		database.upsert(scheme);
		return true;
	}

}
