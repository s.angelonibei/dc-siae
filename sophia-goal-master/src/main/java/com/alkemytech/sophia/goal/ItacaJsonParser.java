package com.alkemytech.sophia.goal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.collecting.SiaeCode;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.text.TextNormalizer;
import com.alkemytech.sophia.commons.text.TextNormalizerProvider;
import com.alkemytech.sophia.commons.util.BigDecimals;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.goal.collecting.CollectingDatabase;
import com.alkemytech.sophia.goal.collecting.CollectingDatabaseProvider;
import com.alkemytech.sophia.goal.model.Artist;
import com.alkemytech.sophia.goal.model.ArtistRole;
import com.alkemytech.sophia.goal.model.Attribute;
import com.alkemytech.sophia.goal.model.AttributeKey;
import com.alkemytech.sophia.goal.model.Code;
import com.alkemytech.sophia.goal.model.CodeType;
import com.alkemytech.sophia.goal.model.CollectingCode;
import com.alkemytech.sophia.goal.model.Origin;
import com.alkemytech.sophia.goal.model.Title;
import com.alkemytech.sophia.goal.model.TitleType;
import com.alkemytech.sophia.goal.model.Work;
import com.alkemytech.sophia.goal.work.WorkDatabase;
import com.alkemytech.sophia.goal.work.WorkDatabaseProvider;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ItacaJsonParser {
	
	private static final Logger logger = LoggerFactory.getLogger(ItacaJsonParser.class);

	private static class GuiceModuleExtension extends GuiceModule {

		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.toInstance(new S3(configuration));
			// normalizer(s)
			bind(TextNormalizer.class)
				.annotatedWith(Names.named("iswc_normalizer"))
				.toProvider(new TextNormalizerProvider(configuration, "iswc_normalizer"));
			bind(TextNormalizer.class)
				.annotatedWith(Names.named("isrc_normalizer"))
				.toProvider(new TextNormalizerProvider(configuration, "isrc_normalizer"));
			// collecting database
			bind(CollectingDatabase.class)
				.toProvider(new CollectingDatabaseProvider(configuration, "collecting_database"))
				.asEagerSingleton();			
			// work database
			bind(WorkDatabase.class)
				.toProvider(new WorkDatabaseProvider(configuration, "work_database"))
				.asEagerSingleton();			
			// this
			bind(ItacaJsonParser.class)
				.asEagerSingleton();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			final ItacaJsonParser instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/itaca-json-parser.properties"))
					.getInstance(ItacaJsonParser.class)
					.startup();
			try {
				instance.process();
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	private final Properties configuration;
	private final WorkDatabase workDatabase;
	private final CollectingDatabase collectingDatabase;
	private final S3 s3;
	private final Gson gson;
	private final TextNormalizer iswcNormalizer;
	private final TextNormalizer isrcNormalizer;
	
	@Inject
	protected ItacaJsonParser(@Named("configuration") Properties configuration,
			WorkDatabase workDatabase, CollectingDatabase collectingDatabase,
			S3 s3, Gson gson,
			@Named("iswc_normalizer") TextNormalizer iswcNormalizer,
			@Named("isrc_normalizer") TextNormalizer isrcNormalizer) {
		super();
		this.configuration = configuration;
		this.workDatabase = workDatabase;
		this.collectingDatabase = collectingDatabase;
		this.s3 = s3;
		this.gson = gson;
		this.iswcNormalizer = iswcNormalizer;
		this.isrcNormalizer = isrcNormalizer;
	}
	
	public ItacaJsonParser startup() throws IOException {
		if ("true".equalsIgnoreCase(configuration.getProperty("itaca_json_parser.locked", "true"))) {
			throw new IllegalStateException("application locked");
		}
		workDatabase.startup();
		collectingDatabase.startup();
		s3.startup();
		return this;
	}

	public ItacaJsonParser shutdown() {
		s3.shutdown();
		collectingDatabase.shutdown();
		workDatabase.shutdown();
		return this;
	}
	
	private String readResumeFile(File file) throws IOException {
		int length = (int) file.length();
		if (length > 0) {
			try (final FileInputStream in = new FileInputStream(file)) {
				final byte[] buffer = new byte[length];
				for (length = 0; length < buffer.length; ) {
					final int count = in.read(buffer, length, buffer.length - length);
					if (-1 == count) {
						break;
					}
					length += count;
				}
				return new String(buffer, 0, length, "UTF-8");
			}			
		}
		return null;
	}
	
	private void writeResumeFile(File file, String text) throws IOException {
		try (final FileOutputStream out = new FileOutputStream(file)) {
			out.write(text.getBytes("UTF-8"));
		}
	}
	
	private ItacaJsonParser process() throws IOException {
		
		final String s3FolderUrl = configuration
				.getProperty("itaca_json_parser.s3_folder_url");
		final int s3Retries = Integer.parseInt(configuration
				.getProperty("itaca_json_parser.s3_retries", "1"));
		final File resumeFile = new File(configuration
				.getProperty("itaca_json_parser.resume_file"));
		final Pattern filterRegex = Pattern.compile(configuration
				.getProperty("itaca_json_parser.filter_regex"));
		final Pattern isFullRegex = Pattern.compile(configuration
				.getProperty("itaca_json_parser.is_full_regex"));

		final int origin = Origin.valueOf(configuration
				.getProperty("itaca_json_parser.origin"));
		final boolean overwriteExisting = "true".equalsIgnoreCase(configuration
				.getProperty("itaca_json_parser.overwrite_existing"));

		final String codeField = configuration
				.getProperty("itaca_json_parser.field.code");
		final String[] originalTitleFields = configuration
				.getProperty("itaca_json_parser.field.original_title").split(",");
		final String[] alternativeTitleFields = configuration
				.getProperty("itaca_json_parser.field.alternative_title").split(",");
		final String authorField = configuration
				.getProperty("itaca_json_parser.field.author");
		final String composerField = configuration
				.getProperty("itaca_json_parser.field.composer");
		final String performerField = configuration
				.getProperty("itaca_json_parser.field.performer");
		final String iswcField = configuration
				.getProperty("itaca_json_parser.field.iswc");
		final String isrcField = configuration
				.getProperty("itaca_json_parser.field.isrc");
		final String elField = configuration
				.getProperty("itaca_json_parser.field.el");
		final String pdField = configuration
				.getProperty("itaca_json_parser.field.pd");
		final String scopeField = configuration
				.getProperty("itaca_json_parser.field.scope");
		final String weightField = configuration
				.getProperty("itaca_json_parser.field.weight");
		
		final HeartBeat heartbeat = HeartBeat.constant("json",
				Integer.parseInt(configuration.getProperty("itaca_json_parser.heartbeat",
						configuration.getProperty("default.heartbeat", "1000"))));
		final int bindPort = Integer.parseInt(configuration.getProperty("itaca_json_parser.bind_port",
				configuration.getProperty("default.bind_port", "0")));

		// bind lock tcp port
		try (ServerSocket socket = new ServerSocket(bindPort)) {
			logger.debug("socket bound to {}", socket.getLocalSocketAddress());
			
			logger.debug("importing json from {}", s3FolderUrl);
			final long startTimeMillis = System.currentTimeMillis();

			// build s3 file(s) list
			final List<S3ObjectSummary> objects = s3.listObjects(new S3.Url(s3FolderUrl));
			final List<String> s3FileUrls = new ArrayList<>(objects.size());
			for (S3ObjectSummary object : objects) {
				final String s3FileUrl = new StringBuilder()
						.append("s3://")
						.append(object.getBucketName())
						.append('/')
						.append(object.getKey())
						.toString();
				if (filterRegex.matcher(s3FileUrl).matches()) {
					s3FileUrls.add(s3FileUrl);
				}
			}
			
			// sort lexicographically
			Collections.sort(s3FileUrls);
			
			// find first file of last full export
			int fullIndex = 0;
			boolean previousWasDelta = true;
			boolean fullExportFound = false;
			for (int i = 0; i < s3FileUrls.size(); i ++) {
				final String s3FileUrl = s3FileUrls.get(i);
				if (isFullRegex.matcher(s3FileUrl).matches()) {
					fullExportFound = true;
					if (previousWasDelta) {
						fullIndex = i;
						previousWasDelta = false;
					}
				} else {
					previousWasDelta = true;
				}
			}
			if (!fullExportFound) {
				logger.error("full export not found");
				return this;
			}
			s3FileUrls.subList(0, fullIndex).clear();
			
			// skip already processed
			final String lastProcessed = readResumeFile(resumeFile);
			if (null != lastProcessed) {
				for (int i = 0; i < s3FileUrls.size(); i ++) {
					final String s3FileUrl = s3FileUrls.get(i);
					if (s3FileUrl.equals(lastProcessed)) {
						s3FileUrls.subList(0, i + 1).clear();
						break;
					}
				}
			}
			
			// process s3 file(s)
			int s3Retry = 0;
			for (String s3FileUrl : s3FileUrls) {
				for (; s3Retry < s3Retries; s3Retry ++) {
					try {
						logger.debug("processing file {}", s3FileUrl);
						final S3Object object = s3.getObject(new S3.Url(s3FileUrl));
						try (final S3ObjectInputStream inputStream = object.getObjectContent();
								final GZIPInputStream gzipInputStream = new GZIPInputStream(inputStream);
								final InputStreamReader inputStreamReader = new InputStreamReader(gzipInputStream);
								final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
								final JsonReader jsonReader = new JsonReader(bufferedReader)) {
							jsonReader.beginArray();
							while (jsonReader.hasNext()) {
								final JsonObject jsonObject = gson.fromJson(jsonReader, JsonObject.class);
								JsonElement jsonElement;
								
								// siae code
								String code = null;
								jsonElement = jsonObject.get(codeField);
								if (null != jsonElement) {
									String text = SiaeCode.normalize(jsonElement.getAsString());
									if (!Strings.isNullOrEmpty(text)) {
										code = text;
									}
								}
								if (null == code) {
									logger.warn("missing code: {}", jsonObject);
									continue;
								}

								// code(s)
								final HashSet<Code> codes = new HashSet<>();
								// siae
								codes.add(new Code(code, CodeType.siae, origin));
								// iswc(s)
								jsonElement = jsonObject.get(iswcField);
								if (null != jsonElement) {
									if (jsonElement.isJsonArray()) {
										for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
											String text = iswcNormalizer
													.normalize(jsonArrayElement.getAsString());
											if (!Strings.isNullOrEmpty(text)) {
												codes.add(new Code(text, CodeType.iswc, origin));
											}
										}												
									} else if (jsonElement.isJsonPrimitive()) {
										String text = iswcNormalizer
												.normalize(jsonElement.getAsString());
										if (!Strings.isNullOrEmpty(text)) {
											codes.add(new Code(text, CodeType.iswc, origin));
										}
									}
								}
								// isrc(s)
								jsonElement = jsonObject.get(isrcField);
								if (null != jsonElement) {
									if (jsonElement.isJsonArray()) {
										for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
											String text = isrcNormalizer
													.normalize(jsonArrayElement.getAsString());
											if (!Strings.isNullOrEmpty(text)) {
												codes.add(new Code(text, CodeType.isrc, origin));
											}
										}												
									} else if (jsonElement.isJsonPrimitive()) {
										String text = isrcNormalizer
												.normalize(jsonElement.getAsString());
										if (!Strings.isNullOrEmpty(text)) {
											codes.add(new Code(text, CodeType.isrc, origin));
										}
									}
								}
								
								// title(s)
								final HashSet<Title> titles = new HashSet<>();
								// original title
								for (String originalTitleField : originalTitleFields) {
									jsonElement = jsonObject.get(originalTitleField);
									if (null != jsonElement) {
										String text = jsonElement.getAsString();
										if (!Strings.isNullOrEmpty(text)) {
											titles.add(new Title(text, TitleType.original, origin));
											break;
										}
									}
								}
								if (titles.isEmpty()) {
									logger.warn("missing original title: {}", jsonObject);
									continue;
								}
								// alternative title(s)
								for (String alternativeTitleField : alternativeTitleFields) {
									jsonElement = jsonObject.get(alternativeTitleField);
									if (null != jsonElement) {
										final Set<String> texts = new HashSet<>();
										if (jsonElement.isJsonArray()) {
											for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
												texts.add(jsonArrayElement.getAsString());
											}												
										} else if (jsonElement.isJsonPrimitive()) {
											texts.add(jsonElement.getAsString());
										}
										for (String text : texts) {
											if (!Strings.isNullOrEmpty(text)) {
												titles.add(new Title(text, TitleType.alternative, origin));
											}
										}
									}
								}

								// artist(s)
								final HashSet<Artist> artists = new HashSet<>();
								// author(s)
								jsonElement = jsonObject.get(authorField);
								if (null != jsonElement) {
									final Set<String> texts = new HashSet<>();
									if (jsonElement.isJsonArray()) {
										for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
											texts.add(jsonArrayElement.getAsString());
										}												
									} else if (jsonElement.isJsonPrimitive()) {
										texts.add(jsonElement.getAsString());
									}
									for (String text : texts) {
										if (!Strings.isNullOrEmpty(text)) {
											artists.add(new Artist(text, ArtistRole.author, origin));
										}
									}
								}
								// composer(s)
								jsonElement = jsonObject.get(composerField);
								if (null != jsonElement) {
									final Set<String> texts = new HashSet<>();
									if (jsonElement.isJsonArray()) {
										for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
											texts.add(jsonArrayElement.getAsString());
										}												
									} else if (jsonElement.isJsonPrimitive()) {
										texts.add(jsonElement.getAsString());
									}
									for (String text : texts) {
										if (!Strings.isNullOrEmpty(text)) {
											artists.add(new Artist(text, ArtistRole.composer, origin));
										}
									}
								}
								// performer(s)
								jsonElement = jsonObject.get(performerField);
								if (null != jsonElement) {
									final Set<String> texts = new HashSet<>();
									if (jsonElement.isJsonArray()) {
										for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
											texts.add(jsonArrayElement.getAsString());
										}												
									} else if (jsonElement.isJsonPrimitive()) {
										texts.add(jsonElement.getAsString());
									}
									for (String text : texts) {
										if (!Strings.isNullOrEmpty(text)) {
											artists.add(new Artist(text, ArtistRole.performer, origin));
										}
									}
								}
								if (artists.isEmpty()) {
									logger.warn("missing artists: {}", jsonObject);
									continue;
								}
								
								// attribute(s)
								final HashSet<Attribute> attributes = new HashSet<>();
								// flag EL
								jsonElement = jsonObject.get(elField);
								if (null != jsonElement) {
									String text = jsonElement.getAsString();
									if (!Strings.isNullOrEmpty(text)) {
										attributes.add(new Attribute(AttributeKey.flag_el, text, origin));
									}
								}
								// flag PD
								jsonElement = jsonObject.get(pdField);
								if (null != jsonElement) {
									String text = jsonElement.getAsString();
									if (!Strings.isNullOrEmpty(text)) {
										attributes.add(new Attribute(AttributeKey.flag_pd, text, origin));
									}
								}
								// ambito
								jsonElement = jsonObject.get(scopeField);
								if (null != jsonElement) {
									String text = jsonElement.getAsString();
									if (!Strings.isNullOrEmpty(text)) {
										attributes.add(new Attribute(AttributeKey.ambito, text, origin));
									}
								}
								// rilevanza
								BigDecimal weight = BigDecimal.ZERO;
								jsonElement = jsonObject.get(weightField);
								if (null != jsonElement) {
									String text = jsonElement.getAsString();
									if (!Strings.isNullOrEmpty(text)) {
										attributes.add(new Attribute(AttributeKey.rilevanza, text, origin));
										weight = new BigDecimal(text);
									}
								}
								
								// find existing or generate new work id
								final long id;
								final CollectingCode collecting = collectingDatabase.getByCode(code);
								if (null == collecting) {
									id = workDatabase.getNextId();
									// insert collecting code
									collectingDatabase.upsert(new CollectingCode(code, id, weight));
								} else {
									id = collecting.workId;
									if (!BigDecimals.almostEquals(weight, collecting.weight)) {
										// update collecting code
										collecting.weight = weight;
										collectingDatabase.upsert(collecting);										
									}
								}
								
								// merge data from other origin(s)
								if (!overwriteExisting && null != collecting) {
									final Work work = workDatabase.getById(id);
									if (null != work) {
										// merge existing code(s)
										if (null != work.codes) {
											for (Code workCode : work.codes) {
												if (origin != workCode.origin) {
													codes.add(workCode);
												}
											}
										}
										// merge existing title(s)
										if (null != work.titles) {
											for (Title workTitle : work.titles) {
												if (origin != workTitle.origin) {
													titles.add(workTitle);
												}
											}
										}
										// merge existing artist(s)
										if (null != work.artists) {
											for (Artist workArtist : work.artists) {
												if (origin != workArtist.origin) {
													artists.add(workArtist);
												}
											}
										}
										// merge existing attribute(s)
										if (null != work.attributes) {
											for (Attribute workAttribute : work.attributes) {
												if (origin != workAttribute.origin) {
													attributes.add(workAttribute);
												}
											}
										}
									}
								}

								// insert or replace work
								final Work work = new Work(id, codes, titles, artists, attributes);
								workDatabase.upsert(work);
								
								heartbeat.pump();

							}
							jsonReader.endArray();
						}
						// save last processed
						writeResumeFile(resumeFile, s3FileUrl);
						break;
					} catch (IOException e) {
						logger.error("process", e);
					}
				}
			}
			logger.debug("{} records imported", heartbeat.getTotalPumps());
			logger.debug("import completed in {}",
					TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
		}

		// defrag work database
		long startTimeMillis = System.currentTimeMillis();
		logger.debug("work database defrag started");
		try {
			workDatabase.defrag();
		} catch (Exception e) {
			logger.error("process", e);
		}
		logger.debug("work database defrag completed in {}",
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
		
		// defrag collecting code database
		startTimeMillis = System.currentTimeMillis();
		logger.debug("collecting database defrag started");
		try {
			collectingDatabase.defrag();
		} catch (Exception e) {
			logger.error("process", e);
		}
		logger.debug("collecting database defrag completed in {}",
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
		
		return this;
	}

}