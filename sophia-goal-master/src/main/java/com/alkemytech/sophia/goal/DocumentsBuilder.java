package com.alkemytech.sophia.goal;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.text.TextNormalizer;
import com.alkemytech.sophia.commons.text.TextNormalizerProvider;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.goal.collecting.CollectingDatabase;
import com.alkemytech.sophia.goal.collecting.CollectingDatabaseProvider;
import com.alkemytech.sophia.goal.document.DocumentStore;
import com.alkemytech.sophia.goal.document.DocumentStoreProvider;
import com.alkemytech.sophia.goal.model.Artist;
import com.alkemytech.sophia.goal.model.Code;
import com.alkemytech.sophia.goal.model.CodeType;
import com.alkemytech.sophia.goal.model.CollectingCode;
import com.alkemytech.sophia.goal.model.Document;
import com.alkemytech.sophia.goal.model.Origin;
import com.alkemytech.sophia.goal.model.Text;
import com.alkemytech.sophia.goal.model.TextType;
import com.alkemytech.sophia.goal.model.Title;
import com.alkemytech.sophia.goal.model.Work;
import com.alkemytech.sophia.goal.work.WorkDatabase;
import com.alkemytech.sophia.goal.work.WorkDatabaseProvider;
import com.google.common.base.Strings;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class DocumentsBuilder {
	
	private static final Logger logger = LoggerFactory.getLogger(DocumentsBuilder.class);

	protected static class GuiceModuleExtension extends GuiceModule {

		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// normalizer(s)
			bind(TextNormalizer.class)
				.annotatedWith(Names.named("iswc_normalizer"))
				.toProvider(new TextNormalizerProvider(configuration, "iswc_normalizer"));
			bind(TextNormalizer.class)
				.annotatedWith(Names.named("isrc_normalizer"))
				.toProvider(new TextNormalizerProvider(configuration, "isrc_normalizer"));
			bind(TextNormalizer.class)
				.annotatedWith(Names.named("title_normalizer"))
				.toProvider(new TextNormalizerProvider(configuration, "title_normalizer"));
			bind(TextNormalizer.class)
				.annotatedWith(Names.named("artist_normalizer"))
				.toProvider(new TextNormalizerProvider(configuration, "artist_normalizer"));
			// collecting database
			bind(CollectingDatabase.class)
				.toProvider(new CollectingDatabaseProvider(configuration, "collecting_database"))
				.asEagerSingleton();			
			// work database
			bind(WorkDatabase.class)
				.toProvider(new WorkDatabaseProvider(configuration, "work_database"))
				.asEagerSingleton();			
			// document store
			bind(DocumentStore.class)
				.toProvider(new DocumentStoreProvider(configuration, "document_store"))
				.asEagerSingleton();
			// this
			bind(DocumentsBuilder.class)
				.asEagerSingleton();
		}

	}
	
	public static void main(String[] args) {
		try {
			final DocumentsBuilder instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/documents-builder.properties"))
					.getInstance(DocumentsBuilder.class)
					.startup();
			try {
				instance.process();
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	private final Properties configuration;
	private final WorkDatabase workDatabase;
	private final CollectingDatabase collectingDatabase;
	private final DocumentStore documentStore;
	private final TextNormalizer iswcNormalizer;
	private final TextNormalizer isrcNormalizer;
	private final TextNormalizer titleNormalizer;
	private final TextNormalizer artistNormalizer;

	@Inject
	protected DocumentsBuilder(@Named("configuration") Properties configuration,
			WorkDatabase workDatabase, CollectingDatabase collectingDatabase,
			DocumentStore documentStore,
			@Named("iswc_normalizer") TextNormalizer iswcNormalizer,
			@Named("isrc_normalizer") TextNormalizer isrcNormalizer,
			@Named("title_normalizer") TextNormalizer titleNormalizer,
			@Named("artist_normalizer") TextNormalizer artistNormalizer) {
		super();
		this.configuration = configuration;
		this.workDatabase = workDatabase;
		this.collectingDatabase = collectingDatabase;
		this.documentStore = documentStore;
		this.iswcNormalizer = iswcNormalizer;
		this.isrcNormalizer = isrcNormalizer;
		this.titleNormalizer = titleNormalizer;
		this.artistNormalizer = artistNormalizer;
	}

	public DocumentsBuilder startup() throws IOException {
		if ("true".equalsIgnoreCase(configuration.getProperty("documents_builder.locked", "true"))) {
			throw new IllegalStateException("application locked");
		}
		workDatabase.startup();
		collectingDatabase.startup();
		documentStore.startup();
		return this;
	}

	public DocumentsBuilder shutdown() {
		documentStore.shutdown();
		collectingDatabase.shutdown();
		workDatabase.shutdown();
		return this;
	}
	
	private HashSet<Text> normalizeWork(Work work, Set<Integer> includeOrigins) {
		final HashSet<Text> texts = new HashSet<Text>();
		// title(s)
		int titles = 0;
		if (null != work.titles) {
			for (Title title : work.titles) {
				if (includeOrigins.contains(title.origin)) {
					final String text = titleNormalizer
							.normalize(title.text);
					if (!Strings.isNullOrEmpty(text)) {
						texts.add(new Text(text, TextType.title));
						titles ++;
					}
				}
			}
		}
		// artist(s)
		int artists = 0;
		if (null != work.artists) {
			for (Artist artist : work.artists) {
				if (includeOrigins.contains(artist.origin)) {
					final String text = artistNormalizer
							.normalize(artist.text);
					if (!Strings.isNullOrEmpty(text)) {
						texts.add(new Text(text, TextType.artist));
						artists ++;
					}
				}
			}
		}
		// code(s)
		int codes = 0;
		if (null != work.codes) {
			for (Code code : work.codes) {
				if (includeOrigins.contains(code.origin)) {
					if (CodeType.iswc == code.type) { // iswc(s)
						final String text = iswcNormalizer
								.normalize(code.text);
						if (!Strings.isNullOrEmpty(text)) {
							texts.add(new Text(text, TextType.iswc));
							codes ++;
						}
					} else if (CodeType.isrc == code.type) { // isrc(s)
						final String text = isrcNormalizer
								.normalize(code.text);
						if (!Strings.isNullOrEmpty(text)) {
							texts.add(new Text(text, TextType.isrc));
							codes ++;
						}
					}
				}
			}
		}
		if (0 == titles || 0 == artists) {
			logger.debug("invalid work {} titles, {} artists, {} codes", titles, artists, codes);
			logger.debug("  {}", work);
			return null;
		}
		return texts;
	}
	
	public DocumentsBuilder process() throws Exception {
		final int bindPort = Integer.parseInt(configuration.getProperty("documents_builder.bind_port",
				configuration.getProperty("default.bind_port", "0")));
		// bind lock tcp port
		try (ServerSocket socket = new ServerSocket(bindPort)) {
			logger.debug("socket bound to {}", socket.getLocalSocketAddress());
			// create document(s)
			createDocuments();
		}
		return this;
	}

	private void createDocuments() throws Exception {
		
		final Set<Integer> includeOrigins = new HashSet<>();
		for (String origin : configuration
				.getProperty("documents_builder.include_origins").split(","))  {
			includeOrigins.add(Origin.valueOf(origin));
		}
		final HeartBeat heartbeat = HeartBeat.constant("documents", Integer.parseInt(configuration
				.getProperty("documents_builder.heartbeat", configuration.getProperty("default.heartbeat", "1000"))));

		logger.debug("creating document(s)");
		final AtomicLong skipped = new AtomicLong(0L);
		final long startTimeMillis = System.currentTimeMillis();

		documentStore.truncate();
		collectingDatabase.selectOrderByWeight(new CollectingCode.Selector() {

			private final AtomicLong documentId = new AtomicLong(0L);

			@Override
			public void select(CollectingCode collecting) throws Exception {
				final Work work = workDatabase.getById(collecting.workId);
				final HashSet<Text> texts = normalizeWork(work, includeOrigins);
				if (null != texts && !texts.isEmpty()) {
					final Document document = new Document(documentId
							.incrementAndGet(), work.id, texts);
					documentStore.upsert(document);
				} else {
					skipped.incrementAndGet();
				}
				heartbeat.pump();
			}

		}, false); // largest weight first
		
		logger.debug("{} processed collecting code(s)", heartbeat.getTotalPumps());
		logger.info("skipped {} works", skipped);
		logger.info("document(s) created in {}", 
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
	}

}