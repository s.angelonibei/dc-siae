package com.alkemytech.sophia.goal.pojo;

import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ExArt18Exclusion {
	
	public String type;				// AAD | REP-AA
	public String value;			// Sony

	public ExArt18Exclusion() {
		super();
	}
	
	public ExArt18Exclusion(String type, String value) {
		super();
		this.type = type;
		this.value = value;
	}
	
	public boolean isSociety() {
		return "AAD".equalsIgnoreCase(type);
	}

	public boolean isAngloAmerican() {
		return "REP-AA".equalsIgnoreCase(type);
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}

}
