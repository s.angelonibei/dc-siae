package com.alkemytech.sophia.goal.pojo;

import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class RoyaltyHolder {

	public final DemRoyalty dem;
	public final DrmRoyalty drm;

	public RoyaltyHolder(DemRoyalty dem, DrmRoyalty drm) {
		super();
		this.dem = dem;
		this.drm = drm;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
