package com.alkemytech.sophia.goal.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class UlisseParquetDAO {

	private final Logger logger = LoggerFactory.getLogger(UlisseParquetDAO.class);

	private final Properties configuration;
	private final DataSource dataSource;
	
	@Inject
	public UlisseParquetDAO(@Named("configuration") Properties configuration,
			@Named("MCMDB") DataSource dataSource) {
		super();
		this.configuration = configuration;
		this.dataSource = dataSource;
	}
	
	public DataSource getDataSource() {
		return dataSource;
	}
	
	public String selectAngloAmericanConfig(String year, String month) throws SQLException {
		final String sql = configuration
				.getProperty("ulisse_parquet_parser.sql.select_anglo_american_config")
				.replace("{year}", year)
				.replace("{month}", month);
		logger.debug("executing sql \"{}\"", sql);
		try (final Connection connection = dataSource.getConnection();
				final Statement statement = connection.createStatement();
				final ResultSet resultSet = statement.executeQuery(sql)) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			if (resultSet.next()) {
				return resultSet.getString("s3FileUrl");
			}
		}
		return null;
	}

	public String selectExArt18Config(String year, String month) throws SQLException {
		final String sql = configuration
				.getProperty("ulisse_parquet_parser.sql.select_ex_art_18_config")
				.replace("{year}", year)
				.replace("{month}", month);
		logger.debug("executing sql \"{}\"", sql);
		try (final Connection connection = dataSource.getConnection();
				final Statement statement = connection.createStatement();
				final ResultSet resultSet = statement.executeQuery(sql)) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			if (resultSet.next()) {
				return resultSet.getString("s3FileUrl");
			}
		}
		return null;
	}
	
}
