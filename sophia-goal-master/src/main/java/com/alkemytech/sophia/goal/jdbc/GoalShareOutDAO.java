package com.alkemytech.sophia.goal.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.goal.pojo.ToProcessBean;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class GoalShareOutDAO {

	private final Logger logger = LoggerFactory.getLogger(GoalShareOutDAO.class);

	private final Properties configuration;
	private final DataSource dataSource;

	@Inject
	public GoalShareOutDAO(@Named("configuration") Properties configuration,
			@Named("MCMDB") DataSource dataSource) {
		super();
		this.configuration = configuration;
		this.dataSource = dataSource;
	}

	public DataSource getDataSource() {
		return dataSource;
	}
	
	public List<ToProcessBean> selectToProcess() throws SQLException {
		final List<ToProcessBean> resultList = new ArrayList<>();
		final String sql = configuration
				.getProperty("goal_share_out.sql.select_to_process");
		logger.debug("executing sql \"{}\"", sql);
		try (final Connection connection = dataSource.getConnection();
				final Statement statement = connection.createStatement();
				final ResultSet resultSet = statement.executeQuery(sql)) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			while (resultSet.next()) {
				final ToProcessBean bean = new ToProcessBean();
				bean.id = resultSet.getLong("id");
				bean.licenseeCode = resultSet.getString("licenseeCode");
				bean.period = resultSet.getString("period");
				bean.periodDateFrom = resultSet.getString("periodDateFrom");
				bean.periodDateTo = resultSet.getString("periodDateTo");
				bean.reportId = resultSet.getString("reportId");
				bean.inputFileUrl = resultSet.getString("inputFileUrl");
				bean.invoiceId = resultSet.getString("invoiceId");
				bean.shareOutId = resultSet.getString("shareOutId");
				bean.totalPoints = resultSet.getBigDecimal("totalPoints");
				bean.exArt18Points = resultSet.getBigDecimal("exArt18Points");
				bean.amount = resultSet.getBigDecimal("amount");
				resultList.add(bean);
			}
		}
		return resultList;
	}

	public synchronized boolean updateStarted(ToProcessBean request, String additionalInfo) throws SQLException {
		final String sql = configuration
				.getProperty("goal_share_out.sql.update_started")
				.replace("{id}", Long.toString(request.id))
				.replace("{additionalInfo}", additionalInfo.replace("'", "''"));
		logger.debug("executing sql \"{}\"", sql);
		try (final Connection connection = dataSource.getConnection();
				final Statement statement = connection.createStatement()) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			return 1 == statement.executeUpdate(sql);
		}
	}
	
	public synchronized boolean updateCompleted(ToProcessBean request, String outputFileUrl, String exArt18Amount, String additionalInfo) throws SQLException {
		final String sql = configuration
				.getProperty("goal_share_out.sql.update_completed")
				.replace("{id}", Long.toString(request.id))
				.replace("{outputFileUrl}", outputFileUrl.replace("'", "''"))
				.replace("{exArt18Amount}", exArt18Amount)
				.replace("{additionalInfo}", additionalInfo.replace("'", "''"));
		logger.debug("executing sql \"{}\"", sql);
		try (final Connection connection = dataSource.getConnection();
				final Statement statement = connection.createStatement()) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			return 1 == statement.executeUpdate(sql);
		}
	}

	public synchronized boolean updateFailed(ToProcessBean request, String additionalInfo) throws SQLException {
		final String sql = configuration
				.getProperty("goal_share_out.sql.update_failed")
				.replace("{id}", Long.toString(request.id))
				.replace("{additionalInfo}", additionalInfo.replace("'", "''"));
		logger.debug("executing sql \"{}\"", sql);
		try (final Connection connection = dataSource.getConnection();
				final Statement statement = connection.createStatement()) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			return 1 == statement.executeUpdate(sql);
		}
	}
	
}
