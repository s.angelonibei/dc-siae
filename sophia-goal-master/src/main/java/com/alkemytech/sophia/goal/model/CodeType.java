package com.alkemytech.sophia.goal.model;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class CodeType {

	public static final int undefined = 0;
	public static final int iswc = 1;
	public static final int isrc = 2;
	public static final int siae = 3;

	public static int valueOf(String text) {
		if ("iswc".equals(text)) {
			return iswc;
		} else if ("isrc".equals(text)) {
			return isrc;
		} else if ("siae".equals(text)) {
			return siae;
		}
		return undefined;
	}
	
	public static String toString(int codeType) {
		switch (codeType) {
		case iswc: return "iswc";
		case isrc: return "isrc";
		case siae: return "siae";
		default: return "undefined";
		}
	}

}
