package com.alkemytech.sophia.goal.pojo;

import java.math.BigDecimal;

import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class RightSplit {

	public final BigDecimal dem;	// .75
	public final BigDecimal drm;	// .25

	public RightSplit(BigDecimal dem, BigDecimal drm) {
		super();
		this.dem = dem;
		this.drm = drm;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
