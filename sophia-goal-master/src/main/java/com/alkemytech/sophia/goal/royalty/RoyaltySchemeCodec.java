package com.alkemytech.sophia.goal.royalty;

import java.math.BigDecimal;
import java.nio.charset.Charset;

import com.alkemytech.sophia.commons.mmap.ObjectCodec;
import com.alkemytech.sophia.goal.pojo.DemRoyalty;
import com.alkemytech.sophia.goal.pojo.DrmRoyalty;
import com.alkemytech.sophia.goal.pojo.Royalty;
import com.alkemytech.sophia.goal.pojo.RoyaltyScheme;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class RoyaltySchemeCodec extends ObjectCodec<RoyaltyScheme> {
		
	private static final int DEM					= 0x0001;
	private static final int DEM_PUBLIC_DOMAIN		= 0x0002;
	private static final int DEM_SIAE				= 0x0004;
	private static final int DEM_NOT_ASSOCIATED		= 0x0008;
	private static final int DEM_SPECIAL_ACCOUNT	= 0x0010;
	private static final int DEM_EX_ART18_EXCLUDED	= 0x0020;

	private static final int DRM					= 0x0100;
	private static final int DRM_PUBLIC_DOMAIN		= 0x0200;
	private static final int DRM_SIAE				= 0x0400;
	private static final int DRM_NOT_ASSOCIATED		= 0x0800;
	private static final int DRM_SPECIAL_ACCOUNT	= 0x1000;
	
	private final Charset charset;
	
	public RoyaltySchemeCodec(Charset charset) {
		super();
		this.charset = charset;
	}

	@Override
	public RoyaltyScheme bytesToObject(byte[] bytes) {
		beginDecoding(bytes);
		final String code = getString(charset);
		final RoyaltyScheme scheme = new RoyaltyScheme(code);
		for (int size = getShort(); size > 0; size --) {
			final String ipiCode = getString(charset);
			final String siaePosition = getString(charset);
			final String role = getString(charset);
			final String accountCode = getString(charset);
			final String accountId = getString(charset);
			final String society = getString(charset);	
			final int irregularityType = getInt();
			final String angloAmericanRepertoire = getString(charset);
			final int mask = getShort();
			if (DEM == (mask & DEM)) {
				final BigDecimal denominator = getBigDecimal();
				final BigDecimal numerator = getBigDecimal();
				final boolean publicDomain = 0 != (mask & DEM_PUBLIC_DOMAIN);
				final boolean siae = 0 != (mask & DEM_SIAE);
				final boolean notAssociated = 0 != (mask & DEM_NOT_ASSOCIATED);
				final boolean specialAccount = 0 != (mask & DEM_SPECIAL_ACCOUNT);
				final boolean exArt18Excluded = 0 != (mask & DEM_EX_ART18_EXCLUDED);
				scheme.add(new DemRoyalty(ipiCode, siaePosition, role, accountCode, accountId, society,
						-1 == irregularityType ? null : irregularityType, notAssociated, specialAccount,
						publicDomain, siae, angloAmericanRepertoire, denominator, numerator, exArt18Excluded));
			} else if (DRM == (mask & DRM)) {
				final BigDecimal percentage = getBigDecimal();
				final boolean publicDomain = 0 != (mask & DRM_PUBLIC_DOMAIN);
				final boolean siae = 0 != (mask & DRM_SIAE);
				final boolean notAssociated = 0 != (mask & DRM_NOT_ASSOCIATED);
				final boolean specialAccount = 0 != (mask & DRM_SPECIAL_ACCOUNT);
				scheme.add(new DrmRoyalty(ipiCode, siaePosition, role, accountCode, accountId, society,
						-1 == irregularityType ? null : irregularityType, notAssociated, specialAccount,
						publicDomain, siae, angloAmericanRepertoire, percentage));
			}
		}
		scheme.publicDomain = (1 == get());
		scheme.demSum = getBigDecimal();
		scheme.drmSum = getBigDecimal();
		return scheme;
	}

	@Override
	public byte[] objectToBytes(RoyaltyScheme scheme) {
		beginEncoding();
		putString(scheme.code, charset);
		putShort((short) scheme.size());
		for (Royalty royalty : scheme) {
			putString(royalty.ipiCode, charset);
			putString(royalty.siaePosition, charset);
			putString(royalty.role, charset);
			putString(royalty.accountCode, charset);
			putString(royalty.accountId, charset);
			putString(royalty.society, charset);
			putInt(null == royalty.irregularityType ? -1 : royalty.irregularityType);
			putString(royalty.angloAmericanRepertoire, charset);
			if (royalty.isDem()) {
				final DemRoyalty dem = (DemRoyalty) royalty;
				int mask = DEM;
				if (royalty.publicDomain)
					mask |= DEM_PUBLIC_DOMAIN;
				if (royalty.siae)
					mask |= DEM_SIAE;
				if (royalty.notAssociated)
					mask |= DEM_NOT_ASSOCIATED;
				if (royalty.specialAccount)
					mask |= DEM_SPECIAL_ACCOUNT;
				if (dem.exArt18Excluded)
					mask |= DEM_EX_ART18_EXCLUDED;
				putShort((short) mask);
				putBigDecimal(dem.denominator);
				putBigDecimal(dem.numerator);
			} else if (royalty.isDrm()) {
				final DrmRoyalty drm = (DrmRoyalty) royalty;
				int mask = DRM;
				if (royalty.publicDomain)
					mask |= DRM_PUBLIC_DOMAIN;
				if (royalty.siae)
					mask |= DRM_SIAE;
				if (royalty.notAssociated)
					mask |= DRM_NOT_ASSOCIATED;
				if (royalty.specialAccount)
					mask |= DRM_SPECIAL_ACCOUNT;
				putShort((short) mask);
				putBigDecimal(drm.percentage);
			}
		}
		put((byte) (scheme.publicDomain ? 1 : 0));
		putBigDecimal(scheme.demSum);
		putBigDecimal(scheme.drmSum);
		return commitEncoding();
	}
	
}
