package com.alkemytech.sophia.goal.collecting;

import java.io.File;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.goal.model.CollectingCode;
import com.google.common.base.Strings;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityCursor;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.SecondaryIndex;
import com.sleepycat.persist.StoreConfig;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class BdbCollectingDatabase implements CollectingDatabase {

	private static final Logger logger = LoggerFactory.getLogger(BdbCollectingDatabase.class);

	private final File environmentHome;
	private final boolean readOnly;
	private final long cacheSize;
	private final int cachePercent;
	private final boolean deferredWrite;
	private final AtomicInteger startupCalls;

	private Environment environment;
	private EntityStore store;

	public BdbCollectingDatabase(Properties configuration, String propertyPrefix) {
		super();
		readOnly = "true".equalsIgnoreCase(configuration
				.getProperty(propertyPrefix + ".read_only", "true"));
		environmentHome = new File(configuration
				.getProperty(propertyPrefix + ".home_folder"));
		cacheSize = TextUtils.parseLongSize(configuration
				.getProperty(propertyPrefix + ".cache_size", "0"));
		cachePercent = Integer.parseInt(configuration
				.getProperty(propertyPrefix + ".cache_percent", "60"));
		deferredWrite = "true".equals(configuration
				.getProperty(propertyPrefix + ".deferred_write", "true"));
		this.startupCalls = new AtomicInteger(0);
	}

	@Override
	public BdbCollectingDatabase startup() {
		if (0 == startupCalls.getAndIncrement()) {
			try {
				environmentHome.mkdirs();
				environment = new Environment(environmentHome,
						(EnvironmentConfig) EnvironmentConfig.DEFAULT
							.setAllowCreate(!readOnly)
							.setReadOnly(readOnly)
							.setCacheSize(cacheSize)
							.setCachePercent(cachePercent));
				store = new EntityStore(environment, "collecting",
						StoreConfig.DEFAULT
							.setAllowCreate(!readOnly)
							.setReadOnly(readOnly)
							.setDeferredWrite(deferredWrite)); 
			} catch (DatabaseException e) {
				logger.error("startup", e);
			}
		}
		return this;
	}

	@Override
	public BdbCollectingDatabase shutdown() {
		if (0 == startupCalls.decrementAndGet()) {
			try {
				if (null != store) {
					if (!readOnly) {
						store.sync();
					}
					store.close();
				}
			} catch (DatabaseException e) {
				logger.error("shutdown", e);
			}
			try {
				if (null != environment) {
					if (!readOnly) {
						environment.sync();
					}
					environment.close();
				}
			} catch (DatabaseException e) {
				logger.error("shutdown", e);
			}
		}
		return this;
	}
	
	@Override
	public boolean isReadOnly() {
		return readOnly;
	}
	
	@Override
	public File getHomeFolder() {
		return environmentHome;
	}

	@Override
	public CollectingCode getByCode(String code) {
		if (Strings.isNullOrEmpty(code)) {
			throw new IllegalArgumentException("code");
		}
		PrimaryIndex<String, CollectingCode> primaryIndex = store
				.getPrimaryIndex(String.class, CollectingCode.class);
		return primaryIndex.get(code);
	}
	
	@Override
	public CollectingCode getByWorkId(long workId) {
		PrimaryIndex<String, CollectingCode> primaryIndex = store
				.getPrimaryIndex(String.class, CollectingCode.class);
		SecondaryIndex<Long, String, CollectingCode> secondaryIndex = store
				.getSecondaryIndex(primaryIndex, Long.class, "workId");
		return secondaryIndex.get(workId);
	}

	@Override
	public CollectingCode upsert(CollectingCode collectingCode) {
		if (!CollectingCode.isValid(collectingCode)) {
			throw new IllegalArgumentException("collectingCode");
		}
		PrimaryIndex<String, CollectingCode> primaryIndex = store
				.getPrimaryIndex(String.class, CollectingCode.class);
		return primaryIndex.put(collectingCode);
	}

	@Override
	public CollectingDatabase truncate() {
		store.truncateClass(CollectingCode.class);
		return this;
	}
	
	@Override
	public BdbCollectingDatabase selectOrderByWeight(CollectingCode.Selector selector, boolean ascending) throws Exception {
		PrimaryIndex<String, CollectingCode> primaryIndex = store
				.getPrimaryIndex(String.class, CollectingCode.class);
		SecondaryIndex<Double, String, CollectingCode> secondaryIndex = store
				.getSecondaryIndex(primaryIndex, Double.class, "weight");
		try (EntityCursor<CollectingCode> collectings = secondaryIndex.entities()) {
			if (ascending) {
				for (CollectingCode collecting = collectings.first();
						null != collecting;
						collecting = collectings.next()) {
					selector.select(collecting);
				}
			} else {
				for (CollectingCode collecting = collectings.last();
						null != collecting;
						collecting = collectings.prev()) {
					selector.select(collecting);
				}
			}
		}
		return this;
	}

	@Override
	public CollectingDatabase defrag() {
		return this;
	}

}
