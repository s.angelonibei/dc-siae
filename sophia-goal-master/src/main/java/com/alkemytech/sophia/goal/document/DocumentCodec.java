package com.alkemytech.sophia.goal.document;

import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Set;

import com.alkemytech.sophia.commons.mmap.ObjectCodec;
import com.alkemytech.sophia.goal.model.Document;
import com.alkemytech.sophia.goal.model.Text;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class DocumentCodec extends ObjectCodec<Document> {
		
	private final Charset charset;

	public DocumentCodec(Charset charset) {
		super();
		this.charset = charset;
	}

	@Override
	public Document bytesToObject(byte[] bytes) {
		beginDecoding(bytes);
		final long id = getLong();
		final long workId = getLong();
		int size = getPackedInt();
		if (-1 == size)
			return new Document(id, workId, null);
		final Set<Text> texts = new HashSet<>(size);
		for (; size > 0; size --) {
			final String text = getString(charset);
			final int type = getPackedInt();
			texts.add(new Text(text, type));
		}
		return new Document(id, workId, texts);
	}

	@Override
	public byte[] objectToBytes(Document document) {
		beginEncoding();
		putLong(document.id);
		putLong(document.workId);
		if (null == document.texts) {
			putPackedInt(-1);
		} else {
			putPackedInt(document.texts.size());
			for (Text text : document.texts) {
				putString(text.text, charset);
				putPackedInt(text.type);
			}
		}
		return commitEncoding();
	}

}
