package com.alkemytech.sophia.goal.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.goal.pojo.RightSplit;
import com.alkemytech.sophia.goal.pojo.ToProcessBean;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class GoalScoringDAO {

	private final Logger logger = LoggerFactory.getLogger(GoalScoringDAO.class);

	private final Properties configuration;
	private final DataSource dataSource;

	@Inject
	public GoalScoringDAO(@Named("configuration") Properties configuration,
			@Named("MCMDB") DataSource dataSource) {
		super();
		this.configuration = configuration;
		this.dataSource = dataSource;
	}

	public DataSource getDataSource() {
		return dataSource;
	}
	
	public RightSplit selectRightSplit(String service, String year, String month) throws SQLException {
		final String sql = configuration
				.getProperty("goal_scoring.sql.select_right_split")
				.replace("{service}", service)
				.replace("{year}", year)
				.replace("{month}", month);
		logger.debug("executing sql \"{}\"", sql);
		try (final Connection connection = dataSource.getConnection();
				final Statement statement = connection.createStatement();
				final ResultSet resultSet = statement.executeQuery(sql)) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			if (resultSet.next()) {
				return new RightSplit(resultSet.getBigDecimal("dem"),
						resultSet.getBigDecimal("drm"));
			}
		}
		return null;
	}
	
	public String selectAngloAmericanConfig(String year, String month) throws SQLException {
		final String sql = configuration
				.getProperty("goal_scoring.sql.select_anglo_american_config")
				.replace("{year}", year)
				.replace("{month}", month);
		logger.debug("executing sql \"{}\"", sql);
		try (final Connection connection = dataSource.getConnection();
				final Statement statement = connection.createStatement();
				final ResultSet resultSet = statement.executeQuery(sql)) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			if (resultSet.next()) {
				return resultSet.getString("s3FileUrl");
			}
		}
		return null;
	}
	
	public List<ToProcessBean> selectToProcess() throws SQLException {
		final List<ToProcessBean> resultList = new ArrayList<>();
		final String sql = configuration
				.getProperty("goal_scoring.sql.select_to_process");
		logger.debug("executing sql \"{}\"", sql);
		try (final Connection connection = dataSource.getConnection();
				final Statement statement = connection.createStatement();
				final ResultSet resultSet = statement.executeQuery(sql)) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			while (resultSet.next()) {
				final ToProcessBean bean = new ToProcessBean();
				bean.id = resultSet.getLong("id");
				bean.license = resultSet.getString("license");
				bean.licenseeCode = resultSet.getString("licenseeCode");
				bean.licenseeName = resultSet.getString("licenseeName");
				bean.service = resultSet.getString("service");
				bean.period = resultSet.getString("period");
				bean.periodDateFrom = resultSet.getString("periodDateFrom");
				bean.periodDateTo = resultSet.getString("periodDateTo");
				bean.utilizationMode = resultSet.getLong("utilizationMode");
				bean.fruitionMode = resultSet.getString("fruitionMode");
				bean.subscription = resultSet.getBoolean("subscription");
				bean.reportId = resultSet.getString("reportId");
				bean.inputFileUrl = resultSet.getString("inputFileUrl");
				bean.invoiceId = resultSet.getString("invoiceId");
				bean.commercialOffer = resultSet.getString("commercialOffer");
				bean.useType = resultSet.getString("useType");
				resultList.add(bean);
			}
		}
		return resultList;
	}

	public synchronized boolean updateStarted(ToProcessBean request, String additionalInfo) throws SQLException {
		final String sql = configuration
				.getProperty("goal_scoring.sql.update_started")
				.replace("{id}", Long.toString(request.id))
				.replace("{additionalInfo}", additionalInfo.replace("'", "''"));
		logger.debug("executing sql \"{}\"", sql);
		try (final Connection connection = dataSource.getConnection();
				final Statement statement = connection.createStatement()) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			return 1 == statement.executeUpdate(sql);
		}
	}
	
	public synchronized boolean updateCompleted(ToProcessBean request, String outputFileUrl, String additionalInfo) throws SQLException {
		final String sql = configuration
				.getProperty("goal_scoring.sql.update_completed")
				.replace("{id}", Long.toString(request.id))
				.replace("{outputFileUrl}", outputFileUrl.replace("'", "''"))
				.replace("{additionalInfo}", additionalInfo.replace("'", "''"));
		logger.debug("executing sql \"{}\"", sql);
		try (final Connection connection = dataSource.getConnection();
				final Statement statement = connection.createStatement()) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			return 1 == statement.executeUpdate(sql);
		}
	}

	public synchronized boolean updateFailed(ToProcessBean request, String additionalInfo) throws SQLException {
		final String sql = configuration
				.getProperty("goal_scoring.sql.update_failed")
				.replace("{id}", Long.toString(request.id))
				.replace("{additionalInfo}", additionalInfo.replace("'", "''"));
		logger.debug("executing sql \"{}\"", sql);
		try (final Connection connection = dataSource.getConnection();
				final Statement statement = connection.createStatement()) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			return 1 == statement.executeUpdate(sql);
		}
	}
	
}
