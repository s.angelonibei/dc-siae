package com.alkemytech.sophia.goal.royalty;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@SuppressWarnings("serial")
public class RoyaltyDatabaseException extends RuntimeException {

	public RoyaltyDatabaseException(String message, Throwable cause) {
		super(message, cause);
	}

	public RoyaltyDatabaseException(String message) {
		super(message);
	}

	public RoyaltyDatabaseException(Throwable cause) {
		super(cause);
	}

}
