package com.alkemytech.sophia.valorizzatore.dto;

import java.util.List;


public class ConfigurationDTO {
	private String voceIncasso;
	private String periodo;
	private String tipoAlgoritmo;
	private EsclusioneDTO esclusioni;
	private TipologiaPM tipologiaPM;

	public final static String PRO_QUOTA = "pro_quota";
	public final static String PRO_DURATA = "pro_durata";
	public final static String ESC_BIS = "escluso per bis";
	public final static String ESC_DURATA = "escluso per durata";

	public ConfigurationDTO() {
		super();
	}

	public ConfigurationDTO(String voceIncasso, String periodo, String tipoAlgoritmo, EsclusioneDTO esclusioni,
			TipologiaPM tipologiaPM) {
		super();
		this.voceIncasso = voceIncasso;
		this.periodo = periodo;
		this.tipoAlgoritmo = tipoAlgoritmo;
		this.esclusioni = esclusioni;
		this.tipologiaPM = tipologiaPM;
	}

	public String getVoceIncasso() {
		return voceIncasso;
	}

	public void setVoceIncasso(String voceIncasso) {
		this.voceIncasso = voceIncasso;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public String getTipoAlgoritmo() {
		return tipoAlgoritmo;
	}

	public void setTipoAlgoritmo(String tipoAlgoritmo) {
		this.tipoAlgoritmo = tipoAlgoritmo;
	}

	public EsclusioneDTO getEsclusioni() {
		return esclusioni;
	}

	public void setEsclusioni(EsclusioneDTO esclusioni) {
		this.esclusioni = esclusioni;
	}

	public TipologiaPM getTipologiaPM() {
		return tipologiaPM;
	}

	public void setTipologiaPM(TipologiaPM tipologiaPM) {
		this.tipologiaPM = tipologiaPM;
	}

	public static ConfigurationDTO getConfigurazione(List<ConfigurationDTO> configurazioni, String voceIncasso,
			String periodo, TipologiaPM tipologiaPM) {
		//Viene presa la prima configurazione in cui i 3 parametri (periodo,TipologiaPm e voceIncasso) coincidono 
		//con quello dell'eventopreso i considerazione...
		//... Assicurarsi che ogni configurazione restituita sia univocamente determinata
		for (ConfigurationDTO configurationDTO : configurazioni) {
			if (configurationDTO.getPeriodo().equals(periodo) && configurationDTO.getTipologiaPM().equals(tipologiaPM)
					&& configurationDTO.getVoceIncasso().equals(voceIncasso)) {
				return configurationDTO;
			}

		}
		return null;

	}

}
