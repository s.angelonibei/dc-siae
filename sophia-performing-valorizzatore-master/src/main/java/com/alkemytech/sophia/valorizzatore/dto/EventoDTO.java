package com.alkemytech.sophia.valorizzatore.dto;

import java.util.List;

public class EventoDTO {
	private Long idEvento;
	private String periodo;
	private String voceIncasso;
	private List<ProgrammaMusicaleDTO> programmiMusicali;
	private TipologiaPM tipologiaPM;
	private Double totaleValoreEconomico;
	private Long puntiTotali;
	private Long puntiPMNonRientrati;
	private Integer pmAttesi;
	private Integer pmRientrati;
	
	public EventoDTO() {
		super();
	}


	public EventoDTO(Long idEvento, String periodo, String voceIncasso, List<ProgrammaMusicaleDTO> programmiMusicali,
			Integer pmAttesi, Integer pmRientrati, TipologiaPM tipologiaPM, Double totaleValoreEconomico,
			Long puntiTotali) {
		super();
		this.idEvento = idEvento;
		this.periodo = periodo;
		this.voceIncasso = voceIncasso;
		this.programmiMusicali = programmiMusicali;
		this.pmAttesi = pmAttesi;
		this.pmRientrati = pmRientrati;
		this.tipologiaPM = tipologiaPM;
		this.totaleValoreEconomico = totaleValoreEconomico;
		this.puntiTotali = puntiTotali;
	}


	public String getPeriodo() {
		return periodo;
	}


	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}


	public Long getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(Long idEvento) {
		this.idEvento = idEvento;
	}

	public String getVoceIncasso() {
		return voceIncasso;
	}

	public void setVoceIncasso(String voceIncasso) {
		this.voceIncasso = voceIncasso;
	}

	public List<ProgrammaMusicaleDTO> getProgrammiMusicali() {
		return programmiMusicali;
	}

	public void setProgrammiMusicali(List<ProgrammaMusicaleDTO> programmiMusicali) {
		this.programmiMusicali = programmiMusicali;
	}

	public Integer getPmAttesi() {
		return pmAttesi;
	}

	public void setPmAttesi(Integer pmAttesi) {
		this.pmAttesi = pmAttesi;
	}

	public Integer getPmRientrati() {
		return pmRientrati;
	}

	public void setPmRientrati(Integer pmRientrati) {
		this.pmRientrati = pmRientrati;
	}

	public TipologiaPM getTipologiaPM() {
		return tipologiaPM;
	}

	public void setTipologiaPM(TipologiaPM tipologiaPM) {
		this.tipologiaPM = tipologiaPM;
	}

	public Double getTotaleValoreEconomico() {
		return totaleValoreEconomico;
	}

	public void setTotaleValoreEconomico(Double totaleValoreEconomico) {
		this.totaleValoreEconomico = totaleValoreEconomico;
	}

	public Long getPuntiTotali() {
		return puntiTotali;
	}

	public void setPuntiTotali(Long puntiTotali) {
		this.puntiTotali = puntiTotali;
	}


	public Long getPuntiPMNonRientrati() {
		return puntiPMNonRientrati;
	}


	public void setPuntiPMNonRientrati(Long puntiPMNonRientrati) {
		this.puntiPMNonRientrati = puntiPMNonRientrati;
	}

}
