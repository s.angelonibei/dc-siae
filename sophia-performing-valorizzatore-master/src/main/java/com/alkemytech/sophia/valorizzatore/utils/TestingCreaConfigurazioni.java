package com.alkemytech.sophia.valorizzatore.utils;

import java.util.ArrayList;
import java.util.List;

import com.alkemytech.sophia.valorizzatore.dto.ConfigurationDTO;
import com.alkemytech.sophia.valorizzatore.dto.EsclusioneDTO;
import com.alkemytech.sophia.valorizzatore.dto.TipologiaPM;

public class TestingCreaConfigurazioni {
	
	/*
	 * GetConfiguration crea una lista di configurazioni che prevedono vari tipi di esclusione inerenti
	 * al bis e alla durata di esecuzione.... se si vuole aggiungere una configurazione di testing particolare
	 * modificare l'oggetto "esclusioneTest" e "confTest"
	 * */

public static List<ConfigurationDTO> getConfiguration(){
		
		TipologiaPM tipologiaPMdigitale=TipologiaPM.CARTACEO;
        TipologiaPM tipologiaPMcartacea=TipologiaPM.DIGITALE;
        EsclusioneDTO esclusione1=new EsclusioneDTO(false,0);   // Esclusione_bis = false esclusione_30sec=false
        EsclusioneDTO esclusione2=new EsclusioneDTO(false,30);	// Esclusione_bis = false esclusione_30sec=true
        EsclusioneDTO esclusione3=new EsclusioneDTO(true,0);	// Esclusione_bis = true esclusione_30sec=false
        EsclusioneDTO esclusione4=new EsclusioneDTO(true,30);	// Esclusione_bis = true esclusione_30sec=true
		
        EsclusioneDTO esclusioneTest=new EsclusioneDTO(true,30,false,false);
        
		List<ConfigurationDTO> listConfigurazioniDTO= new ArrayList<>() ;
		
		// Creo diversi casi di ConfigurazioneDTO possibili:  i casi non sono tutti pertanto bisogna settare il confTest per il caso particolare
		ConfigurationDTO conf1=new ConfigurationDTO("2223","171","pro_quota",esclusione4,tipologiaPMdigitale);
//		ConfigurationDTO conf2=new ConfigurationDTO("2223","171","pro_durata",esclusione4,tipologiaPMdigitale);
		ConfigurationDTO conf3=new ConfigurationDTO("2224","171","pro_durata",esclusione4,tipologiaPMdigitale);
//		ConfigurationDTO conf4=new ConfigurationDTO("2224","171","pro_quota",esclusione4,tipologiaPMdigitale);
		
		
		ConfigurationDTO conf5=new ConfigurationDTO("2223","171","pro_quota",esclusione4,tipologiaPMcartacea);
//		ConfigurationDTO conf6=new ConfigurationDTO("2223","171","pro_durata",esclusione4,tipologiaPMcartacea);
//		ConfigurationDTO conf7=new ConfigurationDTO("2224","171","pro_quota",esclusione4,tipologiaPMcartacea);
		ConfigurationDTO conf8=new ConfigurationDTO("2224","171","pro_durata",esclusione4,tipologiaPMcartacea);
		
		//Settare i parametri di ConfigurationDTO per un caso particolare
		ConfigurationDTO confTest=new ConfigurationDTO("2224","171","pro_durata",esclusioneTest,tipologiaPMdigitale);
		
		
		listConfigurazioniDTO.add(conf1);
//		listConfigurazioniDTO.add(conf2);
		listConfigurazioniDTO.add(conf3);
//		listConfigurazioniDTO.add(conf4);
		listConfigurazioniDTO.add(conf5);
//		listConfigurazioniDTO.add(conf6);
//		listConfigurazioniDTO.add(conf7);
		listConfigurazioniDTO.add(conf8);
		listConfigurazioniDTO.add(confTest);
		
		return listConfigurazioniDTO;
	}

	
}
