package com.alkemy.sophia.api.sophiamsingestionsap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.*;

import java.util.logging.Level;
import java.util.logging.Logger;

@SpringBootApplication
@EnableEurekaClient
@RestController
public class SophiaMsTestApplication {

	@RequestMapping("/")
	public String ciao(){
		Logger.getLogger("logger").log(Level.INFO,"vado in /");
		return "ciao";
	}

	@RequestMapping("/contacts")
	public String ciao2(){
		Logger.getLogger("logger").log(Level.INFO,"vado in /contacts");
		return "ciao2";
	}



	public static void main(String[] args) {
		SpringApplication.run(SophiaMsTestApplication.class, args);
	}

}

