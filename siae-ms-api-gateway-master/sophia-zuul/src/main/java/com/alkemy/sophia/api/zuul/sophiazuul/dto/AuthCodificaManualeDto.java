package com.alkemy.sophia.api.zuul.sophiazuul.dto;

import java.io.Serializable;
import java.util.List;

public class AuthCodificaManualeDto implements Serializable {
    private boolean authentication;
    private List<String> permissions;

    private String user;


    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public boolean isAuthentication() {
        return authentication;
    }

    public void setAuthentication(boolean authentication) {
        this.authentication = authentication;
    }

    public List<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<String> permissions) {
        this.permissions = permissions;
    }
}
