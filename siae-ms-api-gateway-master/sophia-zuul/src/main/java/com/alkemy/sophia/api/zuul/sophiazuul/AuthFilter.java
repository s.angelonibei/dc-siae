package com.alkemy.sophia.api.zuul.sophiazuul;

import com.alkemy.sophia.api.zuul.sophiazuul.dto.AuthCodificaManualeDto;
import com.alkemy.sophia.api.zuul.sophiazuul.util.Utils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_DECORATION_FILTER_ORDER;
import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_TYPE;

public class AuthFilter extends ZuulFilter {

    private static Logger log = LoggerFactory.getLogger(AuthFilter.class);

    @Override
    public String filterType() {
        return PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return PRE_DECORATION_FILTER_ORDER -1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Value("${auth.server.url:'http://localhost:80/cruscottoUtilizzazioni/rest/user/roles'}")
    private String authServer;

    @Value("${auth.disable}")
    private boolean disableAuth;

//    @Value("${aws.checkfile}")
//    private String checFileUrl;

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        log.info(String.format("%s request to %s from address %s", request.getMethod(), request.getRequestURL().toString(),request.getHeader("X-Forwarded-For")));

        if(disableAuth || Utils.isDefaultIP(request)){
            return null;
        }

//        if(request.getRequestURI().equals(checFileUrl)){
//            ctx.setSendZuulResponse(false); //This makes request not forwarding to micro services
//            ctx.setResponseStatusCode(HttpStatus.OK.value());
//            log.info("checkFileAws");
//            return null;
//        }

        String sessionId = null;

        if(request.getCookies() != null) {
            for (Cookie cookie : request.getCookies()) {
                if ("JSESSIONID".equalsIgnoreCase(cookie.getName())) {
                    sessionId = cookie.getValue();
                    break;
                }
            }
        }

        if(StringUtils.isEmpty(sessionId)){
            ctx.setSendZuulResponse(false); //This makes request not forwarding to micro services
            ctx.setResponseStatusCode(HttpStatus.UNAUTHORIZED.value());
            log.warn(String.format("401 - NO TOKEN - response at %s", request.getRequestURL().toString()));
            return null;
        }



        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Cookie", "JSESSIONID="+sessionId);

		try {
			ResponseEntity<AuthCodificaManualeDto> response = restTemplate.exchange(authServer, HttpMethod.GET,
					new HttpEntity<String>(headers), AuthCodificaManualeDto.class);
			ctx.addZuulRequestHeader("user", response.getBody().getUser());
			log.info(String.format("user added to headers %s", response.getBody().getUser()));

		} catch (Exception e) {
			ctx.setSendZuulResponse(false); // This makes request not forwarding to micro services
			ctx.setResponseStatusCode(HttpStatus.UNAUTHORIZED.value());
			log.warn(String.format("401 - INVALID TOKEN - response at %s", request.getRequestURL().toString()));
		}


        return null;
    }
}
