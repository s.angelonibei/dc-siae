package com.alkemy.sophia.api.zuul.sophiazuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.netflix.zuul.EnableZuulServer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@EnableZuulProxy

public class SophiaZuulApplication {

	public static void main(String[] args) {
		SpringApplication.run(SophiaZuulApplication.class, args);
	}

	@Bean
	public AuthFilter authFilter() {
		return new AuthFilter();
	}

//	@Bean
//	public MyFallbackProvider myFallbackProvider (){ return new MyFallbackProvider();}

}

