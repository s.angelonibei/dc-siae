package com.alkemy.sophia.api.zuul.sophiazuul.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Component
public class Utils {


    public static String defaultIps;


    @Value("${auth.defaultIp}")
    public void setDefaultIps(String defaultIps) {
        Utils.defaultIps = defaultIps;
    }


    private static boolean isDefaultIP(String remoteAddr) {

        if(!StringUtils.hasLength(defaultIps))
            return false;

        String[] ips = defaultIps.split(",");

        for (String ip : ips) {
            if(ip.trim().equals(remoteAddr))
                return true;
        }

        return false;
    }


    public static boolean isDefaultIP(HttpServletRequest httpRequest)  {

        return isDefaultIP(httpRequest.getRemoteAddr()) || isDefaultIP(httpRequest.getHeader("X-Forwarded-For"));
    }
}
