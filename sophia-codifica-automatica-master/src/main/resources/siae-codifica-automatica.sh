#!/bin/sh

ENVNAME=dev
APPNAME=siae-codifica-automatica
VERSION=5.3

JARNAME=$APPNAME-$VERSION-jar-with-dependencies.jar
CFGNAME=$APPNAME.properties
HOME=/var/local/sophia/$ENVNAME
#HOME=/home/orchestrator/sophia/fuzzy
#OUTPUT=/dev/null
OUTPUT=$HOME/logs/$APPNAME.out

echo "$(date +%Y%m%d%H%M%S) $ENVNAME $APPNAME" >> $OUTPUT

nohup java -jar $HOME/$JARNAME $HOME/$CFGNAME 2>&1 >> $OUTPUT &

echo "$(date +%Y%m%d%H%M%S) $ENVNAME $APPNAME" >> $HOME/logs/crontab.log
