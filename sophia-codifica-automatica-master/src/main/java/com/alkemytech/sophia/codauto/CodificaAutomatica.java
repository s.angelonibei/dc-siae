package com.alkemytech.sophia.codauto;

import java.io.IOException;
import java.net.ServerSocket;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import javax.sql.DataSource;

import org.apache.commons.pool.impl.GenericObjectPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.common.rest.RestClient;
import com.alkemytech.sophia.solr.SearchResult;
import com.alkemytech.sophia.solr.SolrClient;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class CodificaAutomatica {
	
	private static final Logger logger = LoggerFactory.getLogger(CodificaAutomatica.class);

	private final Properties configuration;
	private final DataSource unidentifiedDS;
 
	@Inject
	protected CodificaAutomatica(@Named("configuration") Properties configuration,
			@Named("unidentified") DataSource unidentifiedDS) {
		super();
		this.configuration = configuration;
		this.unidentifiedDS = unidentifiedDS;
	}
	
	public void lookupSolr() throws Exception {

		// connect to unidentified
		try (final ServerSocket socket = new ServerSocket(Integer.parseInt(configuration.getProperty("listenPort", "0")));
				final Connection unidentified = unidentifiedDS.getConnection()) {
			logger.info("lookupSolr: listening on {}", socket.getLocalSocketAddress());				
			logger.info("lookupSolr: connected to: {} {}", unidentified.getMetaData()
					.getDatabaseProductName(), unidentified.getMetaData().getURL());

			// unidentified DAO
			final UnidentifiedDAO unidentifiedDAO = new UnidentifiedDAO(unidentified);

			// general configuration
			final int threadsCount = Integer.parseInt(configuration.getProperty("threadsCount", "1"));
			final int maxUnidentifiedPerRun = Integer.parseInt(configuration.getProperty("maxUnidentifiedPerRun", "1000"));
			final int nextUnidentifiedLimit = Integer.parseInt(configuration.getProperty("nextUnidentifiedLimit", "100"));
			final double sogliaRiconoscimentoAutomatico = Double.parseDouble(configuration.getProperty("sogliaRiconoscimentoAutomatico", "99.0"));
			final long longDelayDays = Integer.parseInt(configuration.getProperty("longDelayDays", "30"));
			final long shortDelayMinutes = Integer.parseInt(configuration.getProperty("shortDelayMinutes", "5"));
			final boolean logIdentificationResults = "true".equalsIgnoreCase(configuration.getProperty("logIdentificationResults", "false"));
			final long runTimeoutSeconds = Long.parseLong(configuration.getProperty("runTimeoutSeconds", "3600")); // 1 hour default

			// SOLR clients pool
			final GenericObjectPool<SolrClient> solrClientPool =
					new GenericObjectPool<SolrClient>(new SolrClientObjectFactory(configuration));
			solrClientPool.setMaxActive(threadsCount);
			solrClientPool.setMaxIdle(threadsCount);
			solrClientPool.setWhenExhaustedAction(GenericObjectPool.WHEN_EXHAUSTED_BLOCK);
			solrClientPool.setMaxWait(-1L);
			solrClientPool.setTestOnBorrow(false);
			solrClientPool.setTestOnReturn(false);
			solrClientPool.setTestWhileIdle(false);

			final AtomicInteger adjustedMaxPerRun = new AtomicInteger(maxUnidentifiedPerRun
					- (maxUnidentifiedPerRun % nextUnidentifiedLimit));
			logger.info("lookupSolr: adjusted max per run {}", adjustedMaxPerRun.get());
			final ConcurrentLinkedQueue<UnidentifiedSong> unidentifiedSongs = new ConcurrentLinkedQueue<UnidentifiedSong>();
			final Semaphore nextRowSemaphore = new Semaphore(threadsCount);
			final ExecutorService executor = Executors.newCachedThreadPool();
			final AtomicInteger runningTasks = new AtomicInteger(0);
			final long startTimeMillis = System.currentTimeMillis();
			while (adjustedMaxPerRun.get() > 0) {
				
				// load next chunk
				if (unidentifiedSongs.isEmpty()) {
					if (System.currentTimeMillis() > startTimeMillis + TimeUnit.SECONDS.toMillis(runTimeoutSeconds)) {
						logger.info("lookupSolr: process running since too long");
						break;
					}
					if (runningTasks.get() > 0) {
						logger.debug("lookupSolr: waiting to load next chunk (still running {})", runningTasks.get());
						Thread.sleep(1000L);
						continue;
					}
					final List<UnidentifiedSong> nextChunk = unidentifiedDAO
							.nextUnidentifiedSongs(nextUnidentifiedLimit, shortDelayMinutes);
					if (null == nextChunk || nextChunk.isEmpty()) {
						logger.info("lookupSolr: no more unidentified songs available");				
						break;
					}
					logger.info("lookupSolr: loaded {} unidentified songs", nextChunk.size());				
					unidentifiedSongs.addAll(nextChunk);
				}
				
				// process song(s)
				nextRowSemaphore.acquire();
				runningTasks.incrementAndGet();
				executor.execute(new Runnable() {

					@Override
					public void run() {
						try {
							final UnidentifiedSong unidentifiedSong = unidentifiedSongs.poll();
							if (null != unidentifiedSong) {
			            		SolrClient solrClient = null;
								try {
									solrClient = solrClientPool.borrowObject();

									// TODO scarta line se titolo contiene parola in black list titoli
									
									// TODO scarta line se autore contiene parola in black list autori
									
			            			logger.debug("run: looking up " + unidentifiedSong);
			            			
				    				// perform search
			            			final List<SearchResult> results = new ArrayList<SearchResult>();
			            			final List<SearchResult> nofuzzy = solrClient.searchOpereCodifica(unidentifiedSong.getTitle(), unidentifiedSong.getArtists(), unidentifiedSong.getRoles());
			            			if (null != nofuzzy && !nofuzzy.isEmpty()) {
			            				results.addAll(nofuzzy);
			            			}
			            			if (null == nofuzzy || nofuzzy.isEmpty() || !solrClient.checkGradoConfidenzaMinimo(nofuzzy)) {
			            				final List<SearchResult> fuzzy = solrClient.searchOpereCodificaFuzzy(unidentifiedSong.getTitle(), unidentifiedSong.getArtists(), unidentifiedSong.getRoles());
			            				if (null != fuzzy && !fuzzy.isEmpty()) {
			            					fuzzy.removeAll(nofuzzy);
			            					results.addAll(fuzzy);
			            				}
			            			}
			            			logger.debug("run: {} results found", results.size());
			            			if (!results.isEmpty()) {
										Collections.sort(results, new Comparator<SearchResult>() {
											@Override
											public int compare(SearchResult o1, SearchResult o2) {
												return - Double.compare(o1.getConfidenza(), o2.getConfidenza()); // descending order
											}
										});
										final SearchResult first = results.get(0);
										if (first.getConfidenza() >= sogliaRiconoscimentoAutomatico) {
											// identify unidentified song
											final IdentifiedSong identifiedSong = new IdentifiedSong()
												.setSiaeWorkCode(first.getOpera().getCodiceOpera())
												.setUnidentifiedSong(unidentifiedSong);
											if (!handleIdentifiedSong(identifiedSong)) {
												try (final Connection unidentified = unidentifiedDS.getConnection()) {
													new UnidentifiedDAO(unidentified)
															.insertIdentified(identifiedSong);
												}
											}
											if (logIdentificationResults) {
												logger.debug("run: identified {} - {} ({})", first.getTitolo(), first.getArtista(),first.getConfidenza());
											}
											return;
										}
			            			}
			            			// delay unidentified song
									try (final Connection unidentified = unidentifiedDS.getConnection()) {
										new UnidentifiedDAO(unidentified)
												.delayUnidentifiedSong(unidentifiedSong.getHashId(), longDelayDays);
									}
									if (logIdentificationResults) {
										logger.debug("run: delayed {} - {}", unidentifiedSong.getTitle(), unidentifiedSong.getArtists());
									}
																	
								} catch (Exception e) {
			            			logger.error("run", e);
			            			if (null != solrClient) {
				            			solrClientPool.invalidateObject(solrClient); // invalidate the object
				            			solrClient = null; // do not return the object to the pool twice
			            			}
			            		} finally {
									adjustedMaxPerRun.decrementAndGet();
			            			if (null != solrClient) {
			            	             solrClientPool.returnObject(solrClient); // make sure the object is returned to the pool
			            			}
			            		}								
							}
						} catch (Exception e) {
	            			logger.error("run", e);
	            		} finally {
							runningTasks.decrementAndGet();
							nextRowSemaphore.release();
						}
					}
					
				});
			}
			
			// join threads
			while (runningTasks.get() > 0) {
				logger.info("lookupSolr: waiting executor shutdown (still running {})", runningTasks.get());
				Thread.sleep(1000L);
			}
			
			// shutdown executor
			executor.shutdown();
			executor.awaitTermination(5, TimeUnit.MINUTES);
	
			logger.info("lookupSolr: elapsed {}ms", System.currentTimeMillis() - startTimeMillis);

		}
		
	}
	
	private boolean handleIdentifiedSong(IdentifiedSong identifiedSong) {
	
		try {
			
			final JsonObject requestJson = new JsonObject();
			requestJson.addProperty("hashId", identifiedSong.getUnidentifiedSong().getHashId());
			requestJson.addProperty("title", identifiedSong.getUnidentifiedSong().getTitle());
			requestJson.addProperty("artists", identifiedSong.getUnidentifiedSong().getArtists());
			requestJson.addProperty("siadaTitle", identifiedSong.getUnidentifiedSong().getSiadaTitle());
			requestJson.addProperty("siadaArtists", identifiedSong.getUnidentifiedSong().getSiadaArtists());
			requestJson.addProperty("roles", identifiedSong.getUnidentifiedSong().getRoles());
			requestJson.addProperty("identificationType", "automatic");
			requestJson.addProperty("siaeWorkCode", identifiedSong.getSiaeWorkCode());
			
			final RestClient.Result result = RestClient
					.put(configuration.getProperty("identifiedSongRestUrl"), 
							requestJson);
			logger.debug(identifiedSong.getUnidentifiedSong().getHashId() + " " + result);
			if (200 == result.getStatusCode()) {
				if ("OK".equals(result.getJsonElement().getAsJsonObject().get("status").getAsString())) {
					return true;
				}
			}

		} catch (IOException e) {
			logger.error("handleIdentifiedSong", e);
		}
		
		return false;
	}
	
	public static void main(String[] args) {
		try {
			final Injector injector = Guice.createInjector(new GuiceModule(args));
//			injector.getInstance(SQSService.class).startup();
//			injector.getInstance(S3Service.class).startup();
//			try {
				injector.getInstance(CodificaAutomatica.class).lookupSolr();
//			} finally {
//				injector.getInstance(SQSService.class).shutdown();
//				injector.getInstance(S3Service.class).shutdown();
//			}
		} catch (Exception e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
}
