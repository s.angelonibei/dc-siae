package com.alkemytech.sophia.codauto;

import java.util.Properties;

import org.apache.commons.pool.PoolableObjectFactory;

import com.alkemytech.sophia.solr.SolrClient;
import com.alkemytech.sophia.solr.SolrClientFactory;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class SolrClientObjectFactory implements PoolableObjectFactory<SolrClient> {

	private final Properties properties;
	
	public SolrClientObjectFactory(Properties properties) {
		super();
		this.properties = properties;
	}
	
	@Override
	public void activateObject(SolrClient arg0) throws Exception {
		
	}

	@Override
	public void destroyObject(SolrClient arg0) throws Exception {

	}

	@Override
	public SolrClient makeObject() throws Exception {
		return SolrClientFactory.getInstance(properties);
	}

	@Override
	public void passivateObject(SolrClient arg0) throws Exception {

	}

	@Override
	public boolean validateObject(SolrClient arg0) {
		return true;
	}

}
