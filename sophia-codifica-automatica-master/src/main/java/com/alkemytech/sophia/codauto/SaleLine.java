package com.alkemytech.sophia.codauto;

import java.io.IOException;

import org.apache.commons.csv.CSVPrinter;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public interface SaleLine {

	public String getProprietaryId();
	public String getTitle();
	public String getArtists();
	public String getRoles();
	public void print(CSVPrinter printer) throws IOException ;
	
}
