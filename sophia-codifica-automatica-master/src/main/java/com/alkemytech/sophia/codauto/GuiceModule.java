package com.alkemytech.sophia.codauto;

import java.util.Properties;
import java.util.TimeZone;

import javax.sql.DataSource;

import com.alkemytech.sophia.common.config.ConfigurationLoader;
import com.alkemytech.sophia.common.dbcp.DBCPDataSourceProvider;
import com.alkemytech.sophia.common.tools.Log4j2Tools;
import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.google.inject.name.Names;

public class GuiceModule extends AbstractModule {

	private final String[] args;
	
	public GuiceModule(String[] args) {
		super();
		this.args = args;
	}

	@Override
	protected void configure() {
		
		// properties
		final Properties properties = new ConfigurationLoader()
				.withCommandLineArgs(args).load();
		Names.bindProperties(binder(), properties);
		bind(Properties.class)
			.annotatedWith(Names.named("configuration"))
			.toInstance(properties);

		// default time zone
		TimeZone.setDefault(TimeZone.getTimeZone(properties.getProperty("defaultTimezone", "UTC")));

		// initialize log4j2
		Log4j2Tools.initialize(properties);

		// mysql datasource(s)
//		bind(DataSource.class)
//			.annotatedWith(Names.named("MCMDB"))
//			.toProvider(new DBCPDataSourceProvider(properties, "MCMDB"))
//			.in(Scopes.SINGLETON);
//		bind(DataSource.class)
//			.annotatedWith(Names.named("sophia_kb"))
//			.toProvider(new DBCPDataSourceProvider(properties, "sophia_kb"))
//			.in(Scopes.SINGLETON);
		bind(DataSource.class)
			.annotatedWith(Names.named("unidentified"))
			.toProvider(new DBCPDataSourceProvider(properties, "unidentified"))
			.in(Scopes.SINGLETON);
		
		// codifica automatica
		bind(CodificaAutomatica.class).asEagerSingleton();
		
	}
	
}
