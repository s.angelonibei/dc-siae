package com.alkemytech.sophia.codauto;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class IdentifiedSong {

	private String siaeWorkCode;
	private UnidentifiedSong unidentifiedSong;
	
	public IdentifiedSong() {
		super();
	}

	public String getSiaeWorkCode() {
		return siaeWorkCode;
	}

	public IdentifiedSong setSiaeWorkCode(String siaeWorkCode) {
		this.siaeWorkCode = siaeWorkCode;
		return this;
	}

	public UnidentifiedSong getUnidentifiedSong() {
		return unidentifiedSong;
	}

	public IdentifiedSong setUnidentifiedSong(UnidentifiedSong unidentifiedSong) {
		this.unidentifiedSong = unidentifiedSong;
		return this;
	}
		

	
}
