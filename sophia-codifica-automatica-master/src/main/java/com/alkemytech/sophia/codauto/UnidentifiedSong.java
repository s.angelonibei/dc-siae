package com.alkemytech.sophia.codauto;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class UnidentifiedSong {

	private String hashId;
	private String title;
	private String artists;
	private String siadaTitle;
	private String siadaArtists;
	private String roles;
	private Long priority;
	private Long insertTime;
	private Long lastAutoTime;
	private Long lastManualTime;
	private Long lastLockTime;
	
	public UnidentifiedSong() {
		super();
		lastAutoTime = 0L;
		lastManualTime = 0L;
		lastLockTime = 0L;
	}
		
	public String getHashId() {
		return hashId;
	}
	public UnidentifiedSong setHashId(String hashId) {
		this.hashId = hashId;
		return this;
	}
	public String getTitle() {
		return title;
	}
	public UnidentifiedSong setTitle(String title) {
		this.title = title;
		return this;
	}
	public String getArtists() {
		return artists;
	}
	public UnidentifiedSong setArtists(String artists) {
		this.artists = artists;
		return this;
	}

	public String getSiadaTitle() {
		return siadaTitle;
	}
	public UnidentifiedSong setSiadaTitle(String siadaTitle) {
		this.siadaTitle = siadaTitle;
		return this;
	}
	public String getSiadaArtists() {
		return siadaArtists;
	}
	public UnidentifiedSong setSiadaArtists(String siadaArtists) {
		this.siadaArtists = siadaArtists;
		return this;
	}

	public String getRoles() {
		return roles;
	}
	public UnidentifiedSong setRoles(String roles) {
		this.roles = roles;
		return this;
	}
	public Long getPriority() {
		return priority;
	}
	public UnidentifiedSong setPriority(Long priority) {
		this.priority = priority;
		return this;
	}
	public Long getInsertTime() {
		return insertTime;
	}
	public UnidentifiedSong setInsertTime(Long insertTime) {
		this.insertTime = insertTime;
		return this;
	}
	public Long getLastAutoTime() {
		return lastAutoTime;
	}
	public UnidentifiedSong setLastAutoTime(Long lastAutoTime) {
		this.lastAutoTime = lastAutoTime;
		return this;
	}
	public Long getLastManualTime() {
		return lastManualTime;
	}
	public UnidentifiedSong setLastManualTime(Long lastManualTime) {
		this.lastManualTime = lastManualTime;
		return this;
	}
	public Long getLastLockTime() {
		return lastLockTime;
	}
	public UnidentifiedSong setLastLockTime(Long lastLockTime) {
		this.lastLockTime = lastLockTime;
		return this;
	}

	@Override
	public String toString() {
		return "UnidentifiedSong [hashId=" + hashId + ", title=" + title
				+ ", artists=" + artists + ", roles=" + roles + ", priority="
				+ priority + ", insertTime=" + insertTime + ", lastAutoTime="
				+ lastAutoTime + ", lastManualTime=" + lastManualTime
				+ ", lastLockTime=" + lastLockTime + "]";
	}
	
}
