package com.alkemytech.sophia.codauto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class UnidentifiedDAO {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Connection jdbc;
	
	public UnidentifiedDAO(Connection jdbc) {
		super();
		this.jdbc = jdbc;
	}
	
	public List<UnidentifiedSong> nextUnidentifiedSongs(int count, long shortDelayMinutes) throws SQLException {
		try {
			jdbc.setAutoCommit(false);
			final List<UnidentifiedSong> unidentifiedSongs = new ArrayList<UnidentifiedSong>(count);
			PreparedStatement stmt = jdbc.prepareStatement(new StringBuffer()
				.append("select hash_id, title, artists, siada_title, siada_artists, roles from unidentified_song")
				.append(" where last_auto_time < ?")
				.append(" and last_manual_time < ?")
				.append(" order by priority desc limit " + count)
				.toString());
			stmt.setLong(1, System.currentTimeMillis());
			stmt.setLong(2, Long.MAX_VALUE); // not yet identified manually
			final ResultSet rset = stmt.executeQuery();
			while (rset.next()) {
				int i = 1;
				unidentifiedSongs.add(new UnidentifiedSong()
					.setHashId(rset.getString(i ++))
					.setTitle(rset.getString(i ++))
					.setArtists(rset.getString(i ++))
					.setSiadaTitle(rset.getString(i ++))
					.setSiadaArtists(rset.getString(i ++))
					.setRoles(rset.getString(i ++)));
			}
			rset.close();
			stmt.close();
			stmt = jdbc.prepareStatement(new StringBuffer()
				.append("update unidentified_song")
				.append(" set last_auto_time = ?")
				.append(" where hash_id = ?")
				.toString());
			final long lastAutoTime = System.currentTimeMillis() +
					TimeUnit.MINUTES.toMillis(shortDelayMinutes);
			try {
				for (UnidentifiedSong unidentifiedSong : unidentifiedSongs) {
					stmt.setLong(1, lastAutoTime);
					stmt.setString(2, unidentifiedSong.getHashId());
					stmt.executeUpdate();
				}
			} finally {
				stmt.close();
			}
			jdbc.commit();
			return unidentifiedSongs;
		} catch (SQLException e) {
			jdbc.rollback();
			throw e;
		}
	}

	public boolean delayUnidentifiedSong(String hashId, long longDelayDays) throws SQLException {

		jdbc.setAutoCommit(true);
		final PreparedStatement stmt = jdbc.prepareStatement(new StringBuffer()
			.append("update unidentified_song")
			.append(" set last_auto_time = ?")
			.append(" where hash_id = ?")
			.toString());
		stmt.setLong(1, System.currentTimeMillis() + 
				TimeUnit.DAYS.toMillis(longDelayDays));
		stmt.setString(2, hashId);
		int count = 0;
		try {
			count = stmt.executeUpdate();
		} finally {
			stmt.close();
		}
		return 1 == count;
		
	}
	
	public boolean insertIdentified(IdentifiedSong identifiedSong) throws SQLException {

		jdbc.setAutoCommit(true);
		PreparedStatement stmt = jdbc.prepareStatement(new StringBuffer()
			.append("insert into identified_song")
			.append("( hash_id")
			.append(", title")
			.append(", artists")
			.append(", siada_title")
			.append(", siada_artists")
			.append(", roles")
			.append(", siae_work_code")
			.append(", identification_type")
			.append(", insert_time")
			.append(") values (?, ?, ?, ?, ?, ?, ?, ?, ?)")
			.toString());
		int i = 1;
		stmt.setString(i ++, identifiedSong.getUnidentifiedSong().getHashId());
		stmt.setString(i ++, identifiedSong.getUnidentifiedSong().getTitle());
		stmt.setString(i ++, identifiedSong.getUnidentifiedSong().getArtists());
		stmt.setString(i ++, identifiedSong.getUnidentifiedSong().getSiadaTitle());
		stmt.setString(i ++, identifiedSong.getUnidentifiedSong().getSiadaArtists());
		stmt.setString(i ++, identifiedSong.getUnidentifiedSong().getRoles());
		stmt.setString(i ++, identifiedSong.getSiaeWorkCode());
		stmt.setString(i ++, "automatic");
		stmt.setLong(i ++, System.currentTimeMillis());
		int count = 0;
		try {
			count = stmt.executeUpdate();
		} catch (SQLException e) {
			if (1062 == e.getErrorCode()) {
				logger.debug("insertIdentified: duplicate entry {}", identifiedSong.getUnidentifiedSong().getHashId());				
			} else {
				logger.error("insertIdentified", e);
			}
		} finally {
			stmt.close();
		}
		stmt = jdbc.prepareStatement(new StringBuffer()
			.append("update unidentified_song")
			.append(" set last_auto_time = ?")
			.append(" where hash_id = ?")
			.toString());
		i = 1;
		stmt.setLong(i ++, Long.MAX_VALUE); // identified automatically
		stmt.setString(i ++, identifiedSong.getUnidentifiedSong().getHashId());
		try {
			count = stmt.executeUpdate();
		} finally {
			stmt.close();
		}
		return 1 == count;

	}
	
}
