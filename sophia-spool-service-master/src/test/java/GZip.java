

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;

@Service
public class GZip {
	protected static Logger logger = LoggerFactory.getLogger(GZip.class.getName());

	@Value("${inbound.out.path}")
	private String outPath;

	public File compressGZIP(File input) {
		String fileName = input.getName().replaceAll(" ", "-");
    	fileName = fileName.concat(".gz");
		
		File output = new File(outPath + "/" + fileName);
		// , File output
		logger.info("inizio compressione file "+output.getName());
		try (GzipCompressorOutputStream out = new GzipCompressorOutputStream(new FileOutputStream(output))) {
			IOUtils.copy(new FileInputStream(input), out);
			logger.info("compressione del "+output.getName()+" file completata");
			return output;
		} catch (Exception ex) {
			throw new MessagingException("failed to transform File Message");
		}
	}

	public void decompressGZIP(File input, File output) throws IOException {
		try (GzipCompressorInputStream in = new GzipCompressorInputStream(new FileInputStream(input))) {
			IOUtils.copy(in, new FileOutputStream(output));
		}
	}
}
