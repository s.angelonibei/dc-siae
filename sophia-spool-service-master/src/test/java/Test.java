import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(Test.extractYear("UCMR-ADA-Repertoire-20190130.csv.gz"));
	}
	private static String extractYear(String fileName) {
        String year = "";
		Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(fileName);
        while(m.find()) {
        	year = m.group();            
        }
        return year.length()==0?null:year;

	}

}
