package com.alkemy.sophia.spool.utils;

import java.io.File;
import java.io.FileInputStream;

import com.alkemy.sophia.spool.service.ccdi.SqsCCDIReceiveService;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SFtpClient {
	protected static Logger logger = LoggerFactory.getLogger(SFtpClient.class.getName());
	private String server;
	private int port;
	private String user;
	private String password;
	private String sftpPrivateKey;
	
	private JSch 	jsch;
	private Session session;
	private Channel     channel;
	public SFtpClient() {
		jsch = new JSch();
		//ftp = new FTPClient();
	}
	
	public void init(String server, int port, String user, String password,String sftpPrivateKey) {
		this.server 		= server;
		this.port 			= port;
		this.user 			= user;
		this.password 		= password;
		this.sftpPrivateKey = sftpPrivateKey;
	}

	public void open() throws Throwable {
		if(this.sftpPrivateKey != null && !this.sftpPrivateKey.isEmpty()) {
	        File privateKey = new File(this.sftpPrivateKey);
	        if(privateKey.exists() && privateKey.isFile())
	            this.jsch.addIdentity(this.sftpPrivateKey);
		}
        this.session = this.jsch.getSession(this.user,this.server,this.port);
        if(this.password != null)
        	this.session.setPassword(this.password);
        this.session.setConfig("StrictHostKeyChecking", "no");
        this.session.connect(60000);
        this.channel = session.openChannel("sftp");
        this.channel.connect();
	}
	public void close() {
		if(this.channel != null)
			this.channel.disconnect();
		
		if(this.session != null)
			this.session.disconnect();
	}

	public void putFileToPath(File file, String workingDirectory) throws Throwable {
		logger.debug("I'm in putFileToPath method");
		ChannelSftp channelSftp = (ChannelSftp)this.channel;
		this.changeDirectory(workingDirectory);
		channelSftp.put( new FileInputStream(file), file.getName());
	}
	
	private void changeDirectory(String path) throws Throwable {
		logger.debug("I'm in changeDir method");
		ChannelSftp channelSftp = (ChannelSftp)this.channel;
		String[] folders = path.split("/");
		for (String folder : folders) {
			logger.debug("I'm in changeDir method FOR");
			if (folder.length() > 0 && !folder.contains(".")) {
				// This is a valid folder:
				try {
					channelSftp.cd(folder);
				} catch (SftpException e) {
					// No such folder yet:
					channelSftp.mkdir(folder);
					channelSftp.cd(folder);
				}
			}
		}
	}
//	public void getFileToPath(String fileName, String workingDirectory) throws Throwable {
//		ChannelSftp channelSftp = (ChannelSftp)this.channel;
//		this.changeDirectory(workingDirectory);
//		channelSftp.get( new FileInputStream(file), file.getName());
//	}
}
