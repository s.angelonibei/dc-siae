package com.alkemy.sophia.spool.setup;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "sophiaEntityManagerFactory",
        transactionManagerRef = "sophiaTransactionManager",
        basePackages = { "com.alkemy.sophia.spool.persistence.sophia" })
public class SophiaPersistenceConfiguration {
	@Bean(name = "sophiaDataSource")
    @ConfigurationProperties(prefix="spring.sophia.datasource")
    @Primary
    public DataSource sophiaDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "sophiaEntityManagerFactory")
    @Primary
    public LocalContainerEntityManagerFactoryBean sophiaEntityManagerFactory(
            EntityManagerFactoryBuilder builder,
            @Qualifier("sophiaDataSource") DataSource sunDataSource) {
        return builder
                .dataSource(sunDataSource)
                .packages("com.alkemy.sophia.spool.persistence")
                .persistenceUnit("sophia")
                .build();
    }

    @Bean(name = "sophiaTransactionManager")
    @Primary
    public PlatformTransactionManager sophiaTransactionManager(
            @Qualifier("sophiaEntityManagerFactory") EntityManagerFactory sunEntityManagerFactory) {
        return new JpaTransactionManager(sunEntityManagerFactory);
    }
}
