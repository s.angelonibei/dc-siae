package com.alkemy.sophia.spool.persistence.sophia;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

//
@Repository("sophiaDao")
@Transactional
public class DaoRepositoryImpl implements DaoRepository {

    protected static Logger logger = LoggerFactory.getLogger(DaoRepositoryImpl.class.getName());

    @PersistenceContext(unitName = "sophia")
    private EntityManager entityManager;

    @Override
    public boolean checkCCID(Long id) {
        String sqlString = "select ID from MM_INVIO_CCID WHERE ID=?1";
        Object results = this.entityManager.createNativeQuery(sqlString).setParameter(1, id).getSingleResult();
        return true;
    }

    public boolean consegnatoCCID(Long id) {
        // CONSEGNATO
        // DATA_CONSEGNA
        String sqlString = "UPDATE MM_INVIO_CCID SET CONSEGNATO=1, DATA_CONSEGNA=now() WHERE ID=?1";
        int results = this.entityManager.createNativeQuery(sqlString).setParameter(1, id).executeUpdate();
        return results == 0 ? false : true;
    }

    @Override
    public boolean consegnatoCCIDs(List<Long> ids,String zipName) {
        // CONSEGNATO
        // DATA_CONSEGNA
        int i = 0;
        String sqlString = "UPDATE MM_INVIO_CCID SET CONSEGNATO=1, DATA_CONSEGNA=now(), ERRORE_CONSEGNA ='',ZIPFILE='"+zipName+"' WHERE ID IN(";
        for (Long id : ids) {
            sqlString = sqlString + id + ",";
            i++;
        }
        sqlString = sqlString.substring(0,sqlString.length()-1) + ")";
        logger.info("Query update : "+sqlString);
        return this.entityManager.createNativeQuery(sqlString).executeUpdate() == 0 ? false : true;
    }

    public boolean scaricatoCCID(Long id) {
        // CONSEGNATO
        // DATA_CONSEGNA
        String sqlString = "UPDATE MM_INVIO_CCID SET SCARICATO=1, DATA_SCARICO=now() WHERE ID=?1";
        int results = this.entityManager.createNativeQuery(sqlString).setParameter(1, id).executeUpdate();
        return results == 0 ? false : true;
    }

    public boolean erroreCCID(Long id, String errore) {
        String sqlString = "UPDATE MM_INVIO_CCID SET ERRORE_CONSEGNA=?1 WHERE ID=?2";
        int results = this.entityManager.createNativeQuery(sqlString).setParameter(1, errore).setParameter(2, id).executeUpdate();
        return results == 0 ? false : true;
    }

    @Override
    public boolean erroreCCIDs(List<Long> ids,String errore) {
        int i = 0;
        String sqlString = "UPDATE MM_INVIO_CCID SET ERRORE_CONSEGNA="+errore+" WHERE ID IN(";
        for (Long id : ids) {
            sqlString = sqlString + id + ",";
            i++;
        }
        sqlString = sqlString.substring(0,sqlString.length()-1) + ")";
        logger.info("Query update : "+sqlString);
        return false;
    }

    @Override
    public boolean scaricatoCCIDs(List<Long> ids) {
        // CONSEGNATO
        // DATA_CONSEGNA
        int i = 0;
        String sqlString = "UPDATE MM_INVIO_CCID SET SCARICATO=1, DATA_SCARICO=now() WHERE ID IN(";
        for (Long id : ids) {
            sqlString = sqlString + id + ",";
            i++;
        }
        sqlString = sqlString.substring(0,sqlString.length()-1) + ")";
        return this.entityManager.createNativeQuery(sqlString).executeUpdate() == 0 ? false : true;
    }
}