package com.alkemy.sophia.spool.setup;

import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.ftp.session.DefaultFtpSessionFactory;

@Configuration
public class AppConfiguration {
    @Bean
	public DefaultFtpSessionFactory sessionFactory(
			@Value("${ada.inbound.ftp.host}") String host,
			@Value("${ada.inbound.ftp.port}") int port,
			@Value("${ada.inbound.ftp.username}") String username,
			@Value("${ada.inbound.ftp.password}") String password
			) {
		DefaultFtpSessionFactory sessionFactory = new DefaultFtpSessionFactory();
		sessionFactory.setHost(host);
		sessionFactory.setPort(port);
		sessionFactory.setUsername(username);
		sessionFactory.setPassword(password);
		sessionFactory.setClientMode(FTPClient.PASSIVE_LOCAL_DATA_CONNECTION_MODE);;
		sessionFactory.setBufferSize(1024 * 1024 * 1024);
		return sessionFactory;
	}
}
