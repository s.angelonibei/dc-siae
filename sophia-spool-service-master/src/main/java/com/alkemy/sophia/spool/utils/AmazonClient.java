package com.alkemy.sophia.spool.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;

@Service
public class AmazonClient {
	protected static Logger logger = LoggerFactory.getLogger(AmazonClient.class.getName());
	@Autowired
	private AmazonS3 s3Client;

    @Value("${ada.bucketUrl}")
    private String bucketUrl;

	@Value("${ada.inbound.out.path}")
	private String outPath;

    
    class S3Repository{
    	String bucketName;
    	String key;
    }
    
    
    
	public List<S3ObjectSummary> listObjects(String filename) {
		S3Repository s3Repository = convertUrlS3(bucketUrl+"/"+filename);
		final ListObjectsV2Request request = new ListObjectsV2Request().withBucketName(s3Repository.bucketName).withPrefix(s3Repository.key);
		ListObjectsV2Result result = this.s3Client.listObjectsV2(request);
		final List<S3ObjectSummary> summaries = result.getObjectSummaries();
		while (result.isTruncated()) {
			request.setContinuationToken(result.getNextContinuationToken());
			summaries.addAll (result.getObjectSummaries());
		}
		return summaries;
	}
	public boolean doesObjectExist(String url) {
    	String fileName = this.convertName(url);
		S3Repository s3Repository = convertUrlS3(bucketUrl+"/"+fileName);
		try {
			return this.s3Client.doesObjectExist(s3Repository.bucketName, s3Repository.key);
		} catch (Exception e) {
			return false;
		}
	}
    
    
    public String uploadFileAda(File file) {
        String fileUrl = "";
        try {
        	String lastVersion = this.getLatestVersionAda(StandardCharsets.UTF_8);
        	String currentVersion = this.extractYear(file.getName());
        	
        	File fileGz = this.compressGZIP(file);
    		S3Repository s3Repository = convertUrlS3(bucketUrl+"/"+fileGz.getName());
            logger.info("inizio upload del file \""+fileGz.getName()+ "\" su S3");
    		this.s3Client.putObject(new PutObjectRequest(s3Repository.bucketName, s3Repository.key, fileGz).withCannedAcl(CannedAccessControlList.PublicRead));
    		
    		
        	if(  currentVersion != null && (lastVersion == null || Integer.parseInt(lastVersion)< Integer.parseInt(currentVersion)) ) {
    			// create & upload index latest file
        		final File latest = new File(outPath+"/_LATEST");
    			try (final FileWriter writer = new FileWriter(latest)) {
    				writer.write("# latest version\n" + 
    						"location=s3://"+s3Repository.bucketName+"/"+s3Repository.key);
    				writer.close();
    				S3Repository s3RepositoryLatest = convertUrlS3(bucketUrl+"/_LATEST");
    	    		this.s3Client.putObject(new PutObjectRequest(s3RepositoryLatest.bucketName, s3RepositoryLatest.key, latest).withCannedAcl(CannedAccessControlList.PublicRead));
    	    		latest.delete();
    			}

        	}
        	fileGz.delete();
        } catch (Exception e) {
           e.printStackTrace();
        }
        return fileUrl;
    }

	public File compressGZIP(File input) {
    	String fileName = this.convertName(input.getName());
		
		File output = new File(outPath + "/" + fileName);
		// , File output
		logger.info("inizio compressione file "+output.getName());
		try (GzipCompressorOutputStream out = new GzipCompressorOutputStream(new FileOutputStream(output))) {
			IOUtils.copy(new FileInputStream(input), out);
			logger.info("compressione del "+output.getName()+" file completata");
			return output;
		} catch (Exception ex) {
			throw new MessagingException("failed to transform File Message");
		}
	}

	
	private String getLatestVersionAda(Charset charset) {
		S3Repository s3Repository = convertUrlS3(bucketUrl+"/_LATEST");
		if (this.s3Client.doesObjectExist(s3Repository.bucketName, s3Repository.key)) {
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			try (final S3Object s3object =  this.s3Client.getObject(new GetObjectRequest(s3Repository.bucketName, s3Repository.key));
					final InputStream in = s3object.getObjectContent()) {
				final byte[] bytes = new byte[256];
				for (int length = 0; -1 != length; ) {
					length = in.read(bytes);
					if (length > 0)
						out.write(bytes, 0, length);
				}
			} catch (IOException e) {
				logger.error("getObjectContents", e);
			}
			String multiLine = new String(out.toByteArray(), charset);
			String lines[] = multiLine.split("\\r?\\n");			
			
			return this.extractYear(lines.length > 0 ? lines[1]:lines[0]);
		}
		return null;
	}

	
	
    private S3Repository convertUrlS3(String url) {
    	S3Repository s3Repository = new S3Repository();
    	s3Repository.bucketName = url.substring(5, url.indexOf('/', 5));
    	s3Repository.key = url.substring(1 + url.indexOf('/', 5));
    	return s3Repository;
    }

    private String convertName(String file) {
    	String fileName = file.replaceAll(" ", "-");
    	fileName = fileName.concat(".gz");
    	return fileName;
    }
    
	private String extractYear(String fileName) {
        String year = "";
		Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(fileName);
        while(m.find()) {
        	year = m.group();            
        }
        return year.length()==0?null:year;

	}
}
