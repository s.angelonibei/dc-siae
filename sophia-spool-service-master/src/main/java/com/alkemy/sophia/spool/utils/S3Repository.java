package com.alkemy.sophia.spool.utils;

public class S3Repository {
	public String bucketName;
	public String key;
	public S3Repository(String url) {
		if (!url.startsWith("s3://")) {
			throw new IllegalArgumentException(String
					.format("invalid url \"%s\"", url));
		}
		this.bucketName = url.substring(5, url.indexOf('/', 5));
		this.key = url.substring(1 + url.indexOf('/', 5));
	}    	
}
