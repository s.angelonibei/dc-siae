package com.alkemy.sophia.spool.api;

import java.io.IOException;

import com.alkemy.sophia.spool.exception.ParserJmsException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CCID {
	private String idDsr;
	private String path_s3;
	private String id;

	
	public String getIdDsr() {
		return idDsr;
	}


	public void setIdDsr(String idDsr) {
		this.idDsr = idDsr;
	}


	public String getPath_s3() {
		return path_s3;
	}


	public void setPath_s3(String path_s3) {
		this.path_s3 = path_s3;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public static CCID fromJSON(String json) throws ParserJmsException {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.readValue(json, CCID.class);
		} catch (IOException e) {
			throw new ParserJmsException(e);
		}
	}

}
