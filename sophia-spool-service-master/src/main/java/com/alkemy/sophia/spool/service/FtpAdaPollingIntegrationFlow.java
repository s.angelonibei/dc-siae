package com.alkemy.sophia.spool.service;

import java.io.File;

import org.apache.commons.net.ftp.FTPFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.Pollers;
import org.springframework.integration.file.filters.AbstractFileListFilter;
import org.springframework.integration.file.filters.FileListFilter;
import org.springframework.integration.file.remote.session.SessionFactory;
import org.springframework.integration.ftp.dsl.Ftp;
import org.springframework.integration.transaction.PseudoTransactionManager;
import org.springframework.integration.transaction.TransactionSynchronizationFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.support.MessageBuilder;

import com.alkemy.sophia.spool.setup.ada.AdaFtpConfiguration;
import com.alkemy.sophia.spool.utils.AmazonClient;

@Configuration
@ConditionalOnProperty(prefix = "module.ada", name = "enabled", havingValue = "true")
public class FtpAdaPollingIntegrationFlow {
	protected static Logger logger = LoggerFactory.getLogger(FtpAdaPollingIntegrationFlow.class.getName());
    @Autowired
    private AmazonClient aws;
    @Bean
	public IntegrationFlow inboundFlow(
			@Value("${ada.inbound.ftp.poller.cron}") String period,
			@Value("${ada.inbound.ftp.poller.maxFile}") int maxFile,
			@Value("${ada.inbound.temp.path}") String temp,
			@Value("${ada.inbound.remote.path}") String remote,
          TaskExecutor taskExecutor,
          SessionFactory<FTPFile> sessionFactory,
          FileListFilter<FTPFile> s3Filter,
          TransactionSynchronizationFactory transactionSynchronizationFactory,
          PseudoTransactionManager transactionManager,
          @Qualifier(AdaFtpConfiguration.LOGGING_MESSAGE_HANDLER) MessageHandler loggingHandler
			) {
			return IntegrationFlows.from(Ftp.inboundAdapter(sessionFactory)
											.autoCreateLocalDirectory(true)
											.deleteRemoteFiles(false)
											.remoteDirectory(remote)
											.localDirectory(new File(temp))
											.filter(s3Filter)										
											,
							spec -> spec.poller(Pollers.cron(period)
									.maxMessagesPerPoll(maxFile)
									.transactionSynchronizationFactory(transactionSynchronizationFactory)
									.transactional(transactionManager)
									.errorChannel(AdaFtpConfiguration.ERROR_CHANNEL)
									)
					)
					.transform( x->{
						this.aws.uploadFileAda((File)x);
						return "Trasferimento avvenuto con successo";
						
					})   
					.handle(loggingHandler)
					.get();
	}
    @Bean
    public IntegrationFlow errorHandlingFlow(@Qualifier(AdaFtpConfiguration.LOGGING_MESSAGE_HANDLER) MessageHandler loggingHandler) {
        return IntegrationFlows.from(AdaFtpConfiguration.ERROR_CHANNEL)
        		.transform(m -> {
	        			logger.error("Errore acquisizione file",m);
	        			MessagingException m1 = (MessagingException)m;
	        			Message<String> message1 = MessageBuilder.withPayload(m1.getCause().getCause().getMessage()+"\r\n")
	        			        .copyHeaders(m1.getFailedMessage().getHeaders())
	        			        .build();
	        			return message1;
	        		}
				)
        		.handle(loggingHandler)
               .get();
    }  
    @Bean
    public FileListFilter<FTPFile> s3Filter(){
    	boolean ret = true;
    	ret = !ret;
    	return new AbstractFileListFilter<FTPFile>() {
			@Override
			public boolean accept(FTPFile file) {
				boolean ret = aws.doesObjectExist(file.getName());
				if(ret) {
					logger.info("il file \""+file.getName()+ "\" è gia presente ");
				}else {
					logger.info("il file \""+file.getName()+ "\" NON è presente ");
				}
				return !ret ;
			}
    		
    	};
    }
    
    
}



