package com.alkemy.sophia.spool.api;

import java.io.IOException;

import com.alkemy.sophia.spool.exception.ParserJmsException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Header {
	private String uuid;
	private String timestamp;
	private String queue;
	private String sender;
	private String hostname;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getQueue() {
		return queue;
	}

	public void setQueue(String queue) {
		this.queue = queue;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public static Header fromJSON(String json) throws ParserJmsException {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.readValue(json, Header.class);
		} catch (IOException e) {
			throw new ParserJmsException(e);
		}
	}
}
