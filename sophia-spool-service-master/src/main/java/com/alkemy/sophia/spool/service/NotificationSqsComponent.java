package com.alkemy.sophia.spool.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.alkemy.sophia.spool.api.SophiaResponse;
import com.alkemy.sophia.spool.exception.ParserJmsException;

@Component
public class NotificationSqsComponent {
	@Autowired
    protected JmsTemplate defaultJmsTemplate;


    public void sendMessage(String serviceBusQueue, SophiaResponse<?> response) {
        try {
			this.defaultJmsTemplate.convertAndSend(serviceBusQueue, response.toJSON());
		} catch (JmsException | ParserJmsException e) {
			e.printStackTrace();
		}
    }}
