package com.alkemy.sophia.spool;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
/**
 * The Class	 Application.
 * 
 * @author Antonio
 */
@SpringBootApplication
@EnableConfigurationProperties
public class SpoolBootstrap {
	
	public static void main(String[] args) throws Exception {
//		String tmp = "UCMR-ADA Repertoire 20190131.csv";
//		
//		tmp = tmp.replaceAll(" ", "-");
//		
//		int index = tmp.lastIndexOf(".");
//		tmp = tmp.substring(0,index)+".latest";
//		System.out.println(tmp);
//		Pattern p = Pattern.compile("\\d+");
//        Matcher m = p.matcher(tmp);
//        String year = "";
//        while(m.find()) {
//        	year = year.concat(m.group());            
//        }
//		System.out.println(year);
		
		SpringApplication app = new SpringApplication(SpoolBootstrap.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
	}
}
