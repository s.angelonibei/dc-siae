package com.alkemy.sophia.spool.exception;

public class ParserJmsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ParserJmsException(final Throwable cause) {
		super(cause);
	}
	public ParserJmsException(final String message) {
		super(message);
	}

	public ParserJmsException(final String message, final Throwable cause) {
		super(message,cause);
	}


}
