package com.alkemy.sophia.spool.service.ccdi;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.ftp.session.DefaultFtpSessionFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import com.alkemy.sophia.spool.api.Body;
import com.alkemy.sophia.spool.api.Error;
import com.alkemy.sophia.spool.api.SophiaRequest;
import com.alkemy.sophia.spool.api.SophiaResponse;
import com.alkemy.sophia.spool.common.SophiaMessageSqsUtils;
import com.alkemy.sophia.spool.exception.ParserJmsException;
import com.alkemy.sophia.spool.service.NotificationSqsComponent;
import com.alkemy.sophia.spool.utils.AmazonClient;

@Service
@ConditionalOnProperty(prefix = "module.ccdi", name = "enabled", havingValue = "true")
public class SqsAdaReceiveService {
	protected static Logger logger = LoggerFactory.getLogger(SqsAdaReceiveService.class.getName());
	@Autowired
	private SophiaMessageSqsUtils sophiaUtils;
	@Autowired
	private NotificationSqsComponent notification;

	@Value("${ada.sqs.sender_name}")
	private String senderName;
	@Value("${ada.sqs.failed_queue}")
	private String failedQueue;
	@Value("${ada.sqs.service_bus_queue}")
	private String serviceBusQueue;
	@Value("${ada.sqs.completed_queue}")
	private String completedQueue;
	@Value("${ada.sqs.started_queue}")
	private String startedQueue;

	@Value("${ada.inbound.remote.path}") 
	private String remote;
	@Value("${ada.inbound.temp.path}") 
	private String temp;

	@Autowired
	private AmazonClient aws;

	@Autowired
	private DefaultFtpSessionFactory sessionFactory;

	
	private void buildResponseError(SophiaRequest request, Error error) {
		SophiaResponse<SophiaRequest> response = new SophiaResponse<SophiaRequest>();
		response.setHeader(this.sophiaUtils.buildHeader(this.senderName, this.failedQueue));
		response.setInput(request);
		response.setError(error);
		this.notification.sendMessage(serviceBusQueue, response);
	}
	private void buildResponseComplete(SophiaRequest request) {
		SophiaResponse<SophiaRequest> response = new SophiaResponse<SophiaRequest>();
		response.setHeader(this.sophiaUtils.buildHeader(this.senderName, this.completedQueue));
		response.setInput(request);
		this.notification.sendMessage(serviceBusQueue, response);
	}
	private void buildResponseStart(SophiaRequest request) {
		SophiaResponse<Body> response = new SophiaResponse<Body>();
		response.setHeader(this.sophiaUtils.buildHeader(this.senderName, this.startedQueue));
		response.setContext(this.sophiaUtils.buildContext());
		response.setInput(request.getBody());
		this.notification.sendMessage(serviceBusQueue, response);
	}
	public void downloadFile() throws Exception {
			Arrays.stream(this.sessionFactory.getSession().list(remote)).forEach(file ->
			{
				if(file.isFile() && !aws.doesObjectExist(file.getName())) {
					logger.info("il file \""+file.getName()+ "\" NON è presente ");
					try {
						File f = new File(temp+"/" + file.getName());
				        OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(f));
						this.sessionFactory.getSession().getClientInstance().retrieveFile(file.getName(), outputStream);
						outputStream.close();
						this.aws.uploadFileAda(f);
					} catch (IOException e) {
						logger.error("errore inaspettato",e);
					}
					logger.info("il file \""+file.getName()+ "\" Nè stato caricato su S3 ");
				}else {
					logger.info("il file \""+file.getName()+ "\" è gia presente o è una directory");
				}
			});
	}
	
	@JmsListener(destination = "${ada.sqs.to_process_queue}")
	public void sqsToProcesAda(String requestJSON) {
		logger.info("Received messaggio "+ requestJSON);
		SophiaRequest request = null;
		try {
			request = SophiaRequest.fromJSON(requestJSON);
		} catch (ParserJmsException ex) {
			// non sono riuscito a decodilogger.info("il file \""+file.getName()+ "\" NON è presente ");ficare il messaggio
			SophiaResponse<?> response = new SophiaResponse<>();
			response.setHeader(this.sophiaUtils.buildHeader(this.senderName, this.failedQueue));
			response.setError(this.sophiaUtils.builError("1", ex));
			this.notification.sendMessage(serviceBusQueue, response);
			return;
		}
		try {
    		this.buildResponseStart(request);

    		this.downloadFile();
    		
			this.buildResponseComplete(request);
		} catch (Exception ex) {
			// ho ricevuto un errore inatteso
			logger.error("errore interno",ex);
			 this.buildResponseError(request,this.sophiaUtils.builError("2", ex));
		}
	}

}
