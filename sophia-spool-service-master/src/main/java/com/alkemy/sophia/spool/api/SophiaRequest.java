package com.alkemy.sophia.spool.api;

import java.io.IOException;

import com.alkemy.sophia.spool.exception.ParserJmsException;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonInclude(Include.NON_NULL)
public class SophiaRequest {
	private Header header;
	private Body body;
	
	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	public static SophiaRequest fromJSON(String json) throws ParserJmsException {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.readValue(json, SophiaRequest.class);
		} catch (IOException e) {
			throw new ParserJmsException(e);
		}
	}
}
