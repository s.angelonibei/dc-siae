package com.alkemy.sophia.spool.common;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.alkemy.sophia.spool.utils.S3Repository;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.transfer.Download;
import com.amazonaws.services.s3.transfer.Transfer.TransferState;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;

@Component
public class AwsS3 {
	protected static Logger logger = LoggerFactory.getLogger(AmazonS3.class.getName());
	@Autowired
	private AmazonS3 s3Client;

	private TransferManager transferManager;
	
    @Value("${amazonProperties.max-retry:1}")
    private Integer maxRetry;
	
	@PostConstruct
	private void init() {
		transferManager = TransferManagerBuilder.standard()
				.withS3Client(s3Client)
				.build();
	}

	
	public boolean download(S3Repository url, File file) {
		for (int retries = maxRetry; retries > 0; retries--) {
			try {
				file.delete();
				final Download download = transferManager
						.download(url.bucketName, url.key, file);
				download.waitForCompletion();
				if (TransferState.Completed == download.getState()
						&& file.exists()) {
					return true;
				}
			} catch (Exception e) {
				logger.error("download", e);
			}
		}
		return false;
	}
	
	public boolean download(S3Repository url, OutputStream out) {
		try {
			final S3Object object = s3Client.getObject(new GetObjectRequest(url.bucketName, url.key));
			try (final InputStream in = object.getObjectContent()) {
				final byte[] bytes = new byte[1024];
				for (int count = 0; -1 != (count = in.read(bytes)); ) {
					out.write(bytes, 0, count);
				}
			}
			return true;
		} catch (Exception e) {
			logger.error("download", e);
		}
		return false;
	}
	
}
