package com.alkemy.sophia.spool.api;

import java.io.IOException;
import java.util.List;

import com.alkemy.sophia.spool.exception.ParserJmsException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Body {
	 private Boolean force;
	 private String dsp;
	 private String host;
	 private String port;
	 private String protocol;
	 private String user;
	 private String pwd;
	 private String cert_path;
	 private String path;
	 private String tipoTrasferimento;
	 private boolean zip;
	 private String zipName;
	 private List<CCID> ccid;

	public String getDsp() {
		return dsp;
	}

	public void setDsp(String dsp) {
		this.dsp = dsp;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getCert_path() {
		return cert_path;
	}

	public void setCert_path(String cert_path) {
		this.cert_path = cert_path;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public List<CCID> getCcid() {
		return ccid;
	}

	public void setCcid(List<CCID> ccid) {
		this.ccid = ccid;
	}

	public String getTipoTrasferimento() {
		return tipoTrasferimento;
	}

	public void setTipoTrasferimento(String tipoTrasferimento) {
		this.tipoTrasferimento = tipoTrasferimento;
	}

	public String getZipName() {
		return zipName;
	}

	public void setZipName(String zipName) {
		this.zipName = zipName;
	}

	public boolean isZip() {
		return zip;
	}

	public void setZip(boolean zip) {
		this.zip = zip;
	}

	public Boolean getForce() {
		return force;
	}

	public void setForce(Boolean force) {
		this.force = force;
	}

	public static Body fromJSON(String json) throws ParserJmsException {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.readValue(json, Body.class);
		} catch (IOException e) {
			throw new ParserJmsException(e);
		}
	}
}
