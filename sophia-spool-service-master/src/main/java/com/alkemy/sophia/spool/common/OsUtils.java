package com.alkemy.sophia.spool.common;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class OsUtils {
	protected static Logger logger = LoggerFactory.getLogger(OsUtils.class.getName());
	private String ip = "127.0.0.1";
	private String hostname = "localhost";
	
	@PostConstruct
	private void init() {
		try {
			for (Enumeration<NetworkInterface> networks = NetworkInterface.getNetworkInterfaces(); networks.hasMoreElements();) {
				for (Enumeration<InetAddress> addresses = networks.nextElement().getInetAddresses(); addresses.hasMoreElements();) {
					if (!addresses.nextElement().isLoopbackAddress()) {
						InetAddress address = addresses.nextElement();
						this.ip = address.getHostAddress();
						this.hostname = address.getHostName();
					}
				}
			}
		} catch (Throwable e) {
		}
	}

	public String getIp() {
		return ip;
	}

	public String getHostname() {
		return hostname;
	}
	
}
