package com.alkemy.sophia.spool.api;

import java.io.IOException;

import com.alkemy.sophia.spool.exception.ParserJmsException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.alkemy.sophia.spool.api.Error;

public class SophiaResponse<T>{
	private Header header;
	private Context context;
	private T input;
	private Error error;
	
	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	public T getInput() {
		return input;
	}

	public void setInput(T input) {
		this.input = input;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public static SophiaResponse fromJSON(String json) throws ParserJmsException {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.readValue(json, SophiaResponse.class);
		} catch (IOException e) {
			throw new ParserJmsException(e);
		}
	}
	public String toJSON() throws ParserJmsException {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.writeValueAsString(this);
		} catch (IOException e) {
			throw new ParserJmsException(e);
		}
	}
}
