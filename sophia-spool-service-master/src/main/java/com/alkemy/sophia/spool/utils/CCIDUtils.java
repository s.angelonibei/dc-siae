package com.alkemy.sophia.spool.utils;

import com.alkemy.sophia.spool.api.CCID;
import com.alkemy.sophia.spool.api.SophiaRequest;
import com.alkemy.sophia.spool.common.AwsS3;
import com.alkemy.sophia.spool.persistence.sophia.DaoRepository;
import com.jcraft.jsch.JSchException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
public class CCIDUtils {

    protected static Logger logger = LoggerFactory.getLogger(CCIDUtils.class.getName());

    @Autowired
    private DaoRepository dao;

    public File s3Download(String ccidPath, String pathTemp, String filename, AwsS3 s3) {
        // scarico il file CCDI da S3 nella dirrectory temporanea
        final File archiveFile = new File(pathTemp + "/" + filename);
        logger.info("inizio scaricamento ccid (" + pathTemp + "/" + filename);
        S3Repository repository = new S3Repository(ccidPath);
        return s3.download(repository, archiveFile) == true ? archiveFile : null;
    }

    public File zipFiles(List<CCID> ccidList, String zipName, String pathTemp, AwsS3 s3) throws IOException {
        File file = new File(zipName+".zip");
        FileOutputStream fos = new FileOutputStream(file);
        ZipOutputStream zipOut = new ZipOutputStream(fos);
        for (CCID ccid : ccidList) {
            String filename = ccid.getPath_s3().substring(ccid.getPath_s3().lastIndexOf('/'));
            File fileToZip = s3Download(ccid.getPath_s3(), pathTemp, filename, s3);
            FileInputStream fis = new FileInputStream(fileToZip);
            ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
            zipOut.putNextEntry(zipEntry);
            byte[] bytes = new byte[1024];
            int length;
            while ((length = fis.read(bytes)) >= 0) {
                zipOut.write(bytes, 0, length);
            }
            fis.close();
        }
        zipOut.close();
        fos.close();
        return file;
    }

    public void consegna(SFtpClient sftp, File archiveFile, SophiaRequest request, String ccidID) {
        try {
            // carico il file sul server FTP
            logger.info("inizio upload del file CCID");
            sftp.open();
            sftp.putFileToPath(archiveFile, request.getBody().getPath());
            if ("CONSEGNA".equals(request.getBody().getTipoTrasferimento())) {
                this.dao.consegnatoCCID(Long.parseLong(ccidID));
                logger.info("file CCID consegnato");
            } else {
                this.dao.scaricatoCCID(Long.parseLong(ccidID));
                logger.info("file CCID scaricato");
            }
        } catch (Throwable ex) {
            this.dao.erroreCCID(Long.parseLong(ccidID), "impossibile caricare il file sul server FTP");
            logger.info("impossibile caricare il file sul server FTP");
        } finally {
            sftp.close();
        }
    }

    public void consegnaList(SFtpClient sftp, File archiveFile, SophiaRequest request, List<Long> ccidIDs) {
        try {
            // carico il file sul server FTP
            logger.info("inizio upload dei file CCID");
            sftp.open();
            sftp.putFileToPath(archiveFile, request.getBody().getPath());
            if ("CONSEGNA".equals(request.getBody().getTipoTrasferimento())) {
                this.dao.consegnatoCCIDs(ccidIDs,archiveFile.getName());
                logger.info("file CCID consegnato");
            } else {
                this.dao.scaricatoCCIDs(ccidIDs);
                logger.info("file CCID scaricato");
            }
        } catch (JSchException ex) {
            this.dao.erroreCCIDs(ccidIDs, "impossibile caricare i file sul server FTP");
            logger.info("impossibile caricare il file sul server FTP dettaglio : "+ex);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            sftp.close();
        }
    }
}
