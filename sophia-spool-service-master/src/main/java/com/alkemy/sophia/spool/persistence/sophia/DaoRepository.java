package com.alkemy.sophia.spool.persistence.sophia;

import java.util.List;

public interface DaoRepository{
	boolean checkCCID(Long id);
	boolean consegnatoCCID(Long id);
	boolean consegnatoCCIDs(List<Long> ids, String name);
	boolean scaricatoCCID(Long id);
	boolean erroreCCID(Long id, String errore);
	boolean erroreCCIDs(List<Long> ids,String errore);
	boolean scaricatoCCIDs(List<Long> ccidIDs);
}
