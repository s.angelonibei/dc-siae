package com.alkemy.sophia.spool.api;

import java.io.IOException;

import com.alkemy.sophia.spool.exception.ParserJmsException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Context {
	private String applicationId;
	private String ipAddress;
	private String emrClusterId;

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getEmrClusterId() {
		return emrClusterId;
	}

	public void setEmrClusterId(String emrClusterId) {
		this.emrClusterId = emrClusterId;
	}

	public static Context fromJSON(String json) throws ParserJmsException {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.readValue(json, Context.class);
		} catch (IOException e) {
			throw new ParserJmsException(e);
		}
	}
}
