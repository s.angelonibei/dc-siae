package com.alkemy.sophia.spool.setup.ada;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.integration.handler.LoggingHandler;
import org.springframework.integration.transaction.DefaultTransactionSynchronizationFactory;
import org.springframework.integration.transaction.ExpressionEvaluatingTransactionSynchronizationProcessor;
import org.springframework.integration.transaction.PseudoTransactionManager;
import org.springframework.integration.transaction.TransactionSynchronizationFactory;
import org.springframework.messaging.MessageHandler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@ConditionalOnProperty(prefix = "module.ada", name = "enabled", havingValue = "true")
public class AdaFtpConfiguration {
    public static final String INBOUND_CHANNEL = "inbound-channel";
    public static final String ERROR_CHANNEL = "error-channel";
	public static final String LOGGING_MESSAGE_HANDLER = "loggingMessageHandler";
    @Autowired
    private ApplicationContext applicationContext;
    
    @Bean
    public PseudoTransactionManager transactionManager() {
        return new PseudoTransactionManager();
    }
  
    @Bean
    public TransactionSynchronizationFactory transactionSynchronizationFactory() {
        ExpressionParser parser = new SpelExpressionParser();
        ExpressionEvaluatingTransactionSynchronizationProcessor syncProcessor =
                new ExpressionEvaluatingTransactionSynchronizationProcessor();
        syncProcessor.setBeanFactory(applicationContext.getAutowireCapableBeanFactory());
        syncProcessor.setAfterCommitExpression(parser.parseExpression("@MessageProcessingIntegrationFlow.processSuccess(payload)"));
        syncProcessor.setAfterRollbackExpression(parser.parseExpression("@MessageProcessingIntegrationFlow.processFailed(payload)"));
        return new DefaultTransactionSynchronizationFactory(syncProcessor);
    }

    @Bean
    public TaskExecutor taskExecutor(@Value("${inbound.ftp.poller.thread.pool.size}") int poolSize) {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(poolSize);
        return taskExecutor;
    }
  
    
    @Bean(name = LOGGING_MESSAGE_HANDLER)
    public MessageHandler loggingHandler() {
    	LoggingHandler logger = new LoggingHandler("INFO");
        logger.setShouldLogFullMessage(false);
        return logger;
    }
    
    @Bean(name="inboundOutDirectory")
    public File inboundOutDirectory(@Value("${ada.inbound.out.path}") String path) {
        File file = new File(path);
        file.mkdirs();
        return file;
    }
}
