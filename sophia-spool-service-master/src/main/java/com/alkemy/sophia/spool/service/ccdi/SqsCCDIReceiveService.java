package com.alkemy.sophia.spool.service.ccdi;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipOutputStream;

import com.alkemy.sophia.spool.utils.CCIDUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import com.alkemy.sophia.spool.api.Body;
import com.alkemy.sophia.spool.api.CCID;
import com.alkemy.sophia.spool.api.Error;
import com.alkemy.sophia.spool.api.SophiaRequest;
import com.alkemy.sophia.spool.api.SophiaResponse;
import com.alkemy.sophia.spool.common.AwsS3;
import com.alkemy.sophia.spool.common.SophiaMessageSqsUtils;
import com.alkemy.sophia.spool.exception.ParserJmsException;
import com.alkemy.sophia.spool.persistence.sophia.DaoRepository;
import com.alkemy.sophia.spool.service.NotificationSqsComponent;
import com.alkemy.sophia.spool.utils.S3Repository;
import com.alkemy.sophia.spool.utils.SFtpClient;

@Service
@ConditionalOnProperty(prefix = "module.ccdi", name = "enabled", havingValue = "true")
public class SqsCCDIReceiveService {
	protected static Logger logger = LoggerFactory.getLogger(SqsCCDIReceiveService.class.getName());

	@Autowired
	private NotificationSqsComponent notification;

	@Autowired
	private AwsS3 s3;

	@Autowired
	private SophiaMessageSqsUtils sophiaUtils;

	@Autowired
	private DaoRepository dao;

	@Autowired
	private CCIDUtils ccidUtils;

	@Value("${ccdi.sqs.sender_name}")
	private String senderName;

	@Value("${ccdi.sqs.started_queue}")
	private String startedQueue;

	@Value("${ccdi.sqs.completed_queue}")
	private String completedQueue;

	@Value("${ccdi.sqs.failed_queue}")
	private String failedQueue;

	@Value("${ccdi.sqs.service_bus_queue}")
	private String serviceBusQueue;

	@Value("${ccdi.folder.temp}")
	private String pathTemp;


	private void buildResponseStart(SophiaRequest request) {
		SophiaResponse<Body> response = new SophiaResponse<Body>();
		response.setHeader(this.sophiaUtils.buildHeader(this.senderName, this.startedQueue));
		response.setContext(this.sophiaUtils.buildContext());
		response.setInput(request.getBody());
		this.notification.sendMessage(serviceBusQueue, response);
	}

	private void buildResponseError(SophiaRequest request, Error error) {
		SophiaResponse<SophiaRequest> response = new SophiaResponse<SophiaRequest>();
		response.setHeader(this.sophiaUtils.buildHeader(this.senderName, this.failedQueue));
		response.setInput(request);
		response.setError(error);
		this.notification.sendMessage(serviceBusQueue, response);
	}

	private void buildResponseComplete(SophiaRequest request) {
		SophiaResponse<SophiaRequest> response = new SophiaResponse<SophiaRequest>();
		response.setHeader(this.sophiaUtils.buildHeader(this.senderName, this.completedQueue));
		response.setInput(request);
		this.notification.sendMessage(serviceBusQueue, response);
	}

	private String downloadCertificated(String certPath) {
		String filename = null;
		if(certPath != null && !certPath.isEmpty()) {
			filename = certPath.substring(certPath.lastIndexOf('/'));
			final File archiveFile = new File(pathTemp + "/" + filename);
			S3Repository repository = new S3Repository(certPath);
			boolean download = s3.download(repository, archiveFile);
			if(!download) {
				filename = null;
				logger.error("Impossibile scaricare il certificato ftp");
			}
		}
		return filename;
	}

	@JmsListener(destination = "${ccdi.sqs.to_process_queue}")
	public void sqsToProcesCCID(String requestJSON) {
		logger.info("Received messaggio "+ requestJSON);
		SophiaRequest request = null;
		try {
			request = SophiaRequest.fromJSON(requestJSON);
		} catch (ParserJmsException ex) {
			// non sono riuscito a decodificare il messaggio
			SophiaResponse<?> response = new SophiaResponse<>();
			response.setHeader(this.sophiaUtils.buildHeader(this.senderName, this.failedQueue));
			response.setError(this.sophiaUtils.builError("1", ex));
			this.notification.sendMessage(serviceBusQueue, response);
			return;
		}
		try {
    		this.buildResponseStart(request);
    		logger.info("Inviato messaggio di start");
    		SFtpClient sftp = new SFtpClient();
			String sftpPrivateKey = downloadCertificated(request.getBody().getCert_path());
			sftp.init(request.getBody().getHost(), Integer.parseInt(request.getBody().getPort()),request.getBody().getUser(), request.getBody().getPwd(), sftpPrivateKey);
			logger.info("scaricata configurazione ftp");
			if(request.getBody().isZip()){
				//Effettuo la compressione dei CCID
				File ccidsZipped = ccidUtils.zipFiles(request.getBody().getCcid(),request.getBody().getZipName(),pathTemp,s3);
				if(ccidsZipped != null){
					// carico il file zippato sul server ftp
					List<Long> ccidds = request.getBody().getCcid().stream().map(entity -> Long.parseLong(entity.getId())) // <<< this
							.collect(Collectors.toList());
					ccidUtils.consegnaList(sftp,ccidsZipped,request,ccidds);
					ccidsZipped.delete();
				}
			}else {
				for (CCID ccid : request.getBody().getCcid()) {
					String filename = ccid.getPath_s3().substring(ccid.getPath_s3().lastIndexOf('/'));
					File archiveFile = ccidUtils.s3Download(ccid.getPath_s3(), pathTemp, filename, s3);
					// carico il file sul server ftp
					if (archiveFile != null) {
						ccidUtils.consegna(sftp, archiveFile, request, ccid.getId());
					} else {
						this.dao.erroreCCID(Long.parseLong(ccid.getId()), "impossibile scaricare il file da S3");
						logger.info("impossibile scaricare il file da S3");
					}
				}
			}
			this.buildResponseComplete(request);
		} catch (Exception ex) {
			// ho ricevuto un errore inatteso
			logger.error("errore interno",ex);
			 this.buildResponseError(request,this.sophiaUtils.builError("2", ex));
		}
	}

}
