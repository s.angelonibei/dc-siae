package com.alkemy.sophia.spool.setup;

import javax.jms.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.destination.DynamicDestinationResolver;

import com.amazon.sqs.javamessaging.ProviderConfiguration;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;

@Configuration
@EnableJms
public class AmazonConfiguration {
	protected static Logger logger = LoggerFactory.getLogger(AmazonConfiguration.class.getName());

    @Value("${amazonProperties.accessKey}")
    private String accessKey;
    @Value("${amazonProperties.secretKey}")
    private String secretKey;
    @Value("${amazonProperties.region}")
    private String region;
    @Value("${amazonProperties.proxyHost:}")
    private String proxyHost;
    @Value("${amazonProperties.proxyPort:}")
    private String proxyPort;

    @Bean
    public AmazonSQS sqsClient() {
        AWSCredentials awsCreds = new BasicAWSCredentials(this.accessKey, this.secretKey);
    	final ClientConfiguration clientConfiguration = new ClientConfiguration();
    	if((proxyHost!= null && proxyHost.length()>0)  && (proxyPort != null  && proxyPort.length()>0 )) {
			clientConfiguration.setProxyHost(proxyHost);
			clientConfiguration.setProxyPort(Integer.parseInt(proxyPort));
    	}
    	AmazonSQS sqs = AmazonSQSClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(awsCreds))
				.withRegion(region)
				.withClientConfiguration(clientConfiguration)
				.build();
    	return sqs;
    }
    
    @Bean
    public AmazonS3 s3Client() {
        AWSCredentials awsCreds = new BasicAWSCredentials(this.accessKey, this.secretKey);
    	final ClientConfiguration clientConfiguration = new ClientConfiguration();
    	if((proxyHost!= null && proxyHost.length()>0)  && (proxyPort != null  && proxyPort.length()>0 )) {
			clientConfiguration.setProxyHost(proxyHost);
			clientConfiguration.setProxyPort(Integer.parseInt(proxyPort));
    	}
    	AmazonS3 s3Clientt = AmazonS3ClientBuilder.standard()
        		.withCredentials(new AWSStaticCredentialsProvider(awsCreds))
        		.withClientConfiguration(clientConfiguration)
        		.withRegion(region)
        		.build();
    	return s3Clientt;
    }
    
    
	@Bean
	public SQSConnectionFactory createSqs(AmazonSQS sqs) {
    	SQSConnectionFactory connectionFactory = new SQSConnectionFactory( new ProviderConfiguration(), sqs);    	
    	return connectionFactory;
    }
	@Bean
	public DefaultJmsListenerContainerFactory jmsListenerContainerFactory(SQSConnectionFactory connectionFactory) {
		DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
		factory.setConnectionFactory(connectionFactory);
		factory.setDestinationResolver(new DynamicDestinationResolver());
		factory.setConcurrency("1");
		factory.setSessionAcknowledgeMode(Session.CLIENT_ACKNOWLEDGE);
		return factory;
	}
	@Bean
	public JmsTemplate defaultJmsTemplate(SQSConnectionFactory connectionFactory) {
		return new JmsTemplate(connectionFactory);
	}

}
