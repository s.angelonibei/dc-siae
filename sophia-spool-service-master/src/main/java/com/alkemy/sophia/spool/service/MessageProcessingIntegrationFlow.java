package com.alkemy.sophia.spool.service;


import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component("MessageProcessingIntegrationFlow")
public class MessageProcessingIntegrationFlow {
    protected static Logger logger = LoggerFactory.getLogger(MessageProcessingIntegrationFlow.class.getName());
	public void processSuccess(File message){
		logger.info("trasferimento avvenuto con SUCCESSO per il file "+message.getName());
		message.delete();
	}
	public void processFailed(File message){
		logger.info("errore nel processo di trasferimento del file "+message.getName());
	}
	
}