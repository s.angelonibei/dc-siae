package com.alkemy.sophia.spool.api;

import java.io.IOException;

import com.alkemy.sophia.spool.exception.ParserJmsException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Error {
	private String code;
	private String description;
	private String stackTrace;

	
	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getStackTrace() {
		return stackTrace;
	}


	public void setStackTrace(String stackTrace) {
		this.stackTrace = stackTrace;
	}


	public static Error fromJSON(String json)  throws ParserJmsException {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.readValue(json, Error.class);
		} catch (IOException e) {
			throw new ParserJmsException(e);
		}
	}
}
