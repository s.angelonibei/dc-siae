package com.alkemy.sophia.spool.common;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alkemy.sophia.spool.api.Context;
import com.alkemy.sophia.spool.api.Error;
import com.alkemy.sophia.spool.api.Header;
import com.alkemy.sophia.spool.utils.Iso8601Helper;

@Component
public class SophiaMessageSqsUtils {
	protected static Logger logger = LoggerFactory.getLogger(SophiaMessageSqsUtils.class.getName());
    @Autowired
    private OsUtils osUtil;

    public Header buildHeader(String senderName,String startedQueue) {
    	Header header = new Header();
    	header.setHostname(this.osUtil.getIp());
    	header.setQueue(startedQueue);
    	header.setSender(senderName);
    	header.setTimestamp(Iso8601Helper.format(new Date()));
    	header.setUuid(UUID.randomUUID().toString());
    	return header;
    }
    public Error builError(String code,Throwable stackTrace) {
    	Error error = new Error();
    	error.setCode(code);
    	error.setDescription(stackTrace.getMessage());
    	error.setStackTrace(this.printStackTrace(stackTrace));
    	return error;
    }
    public Error builError(String code,String description) {
    	Error error = new Error();
    	error.setCode(code);
    	error.setDescription(description);
    	return error;
    }
    public Context buildContext() {
    	Context context = new Context();
    //	context.setApplicationId(applicationId);
    	context.setEmrClusterId(this.osUtil.getHostname());
    	context.setIpAddress(this.osUtil.getIp());
    	return context;
    }
    
    public String printStackTrace(Throwable e) {
		final StringWriter writer = new StringWriter();
		e.printStackTrace(new PrintWriter(writer));
		return writer.toString();
	}
}
