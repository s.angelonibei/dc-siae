package com.alkemytech.sophia.harmonizer.dto;

import java.util.Date;

/**
 * Created by Alessandro Russo on 04/12/2017.
 */
public class RaiMusicDTO extends BaseRaiDTO {

    public String progressivoOpTrasmissione;
    public String tipoOpera;
    public String durata;
    public String titoloOpera;
    public String cognomeAutore1;
    public String nomeAutore1;
    public String tutelaSiae1;
    public String cognomeAutore2;
    public String nomeAutore2;
    public String tutelaSiae2;
    public String cognomeAutore3;
    public String nomeAutore3;
    public String tutelaSiae3;
    public String cognomeAutore4;
    public String nomeAutore4;
    public String tutelaSiae4;
    public String cognomeAutore5;
    public String nomeAutore5;
    public String tutelaSiae5;
    public String cognomeAutore6;
    public String nomeAutore6;
    public String tutelaSiae6;
    public String cognomeAutore7;
    public String nomeAutore7;
    public String tutelaSiae7;
    public String cognomeAutore8;
    public String nomeAutore8;
    public String tutelaSiae8;
    public String cognomeAutore9;
    public String nomeAutore9;
    public String tutelaSiae9;
    public String cognomeAutore10;
    public String nomeAutore10;
    public String tutelaSiae10;
    public String genere;
    public String generazione;
    public String dataPrimaTrasmissione;
    public String codiceCasa;
    public String codiceMarca;
    public String descMarcaDiscografica;
    public String siglaNumero;
    public String nu1;
    public String edizione;
    public String nu2;
    public String descEdizione;
    public String codProprieta;
    public String descProprieta;
    public String note;
    public String dataVariazione;
    public String key;

    public String getProgressivoOpTrasmissione() {
        return progressivoOpTrasmissione;
    }

    public void setProgressivoOpTrasmissione(String progressivoOpTrasmissione) {
        this.progressivoOpTrasmissione = progressivoOpTrasmissione;
    }

    public String getTipoOpera() {
        return tipoOpera;
    }

    public void setTipoOpera(String tipoOpera) {
        this.tipoOpera = tipoOpera;
    }

    public String getDurata() {
        return durata;
    }

    public void setDurata(String durata) {
        this.durata = durata;
    }

    public String getTitoloOpera() {
        return titoloOpera;
    }

    public void setTitoloOpera(String titoloOpera) {
        this.titoloOpera = titoloOpera;
    }

    public String getCognomeAutore1() {
        return cognomeAutore1;
    }

    public void setCognomeAutore1(String cognomeAutore1) {
        this.cognomeAutore1 = cognomeAutore1;
    }

    public String getNomeAutore1() {
        return nomeAutore1;
    }

    public void setNomeAutore1(String nomeAutore1) {
        this.nomeAutore1 = nomeAutore1;
    }

    public String getTutelaSiae1() {
        return tutelaSiae1;
    }

    public void setTutelaSiae1(String tutelaSiae1) {
        this.tutelaSiae1 = tutelaSiae1;
    }

    public String getCognomeAutore2() {
        return cognomeAutore2;
    }

    public void setCognomeAutore2(String cognomeAutore2) {
        this.cognomeAutore2 = cognomeAutore2;
    }

    public String getNomeAutore2() {
        return nomeAutore2;
    }

    public void setNomeAutore2(String nomeAutore2) {
        this.nomeAutore2 = nomeAutore2;
    }

    public String getTutelaSiae2() {
        return tutelaSiae2;
    }

    public void setTutelaSiae2(String tutelaSiae2) {
        this.tutelaSiae2 = tutelaSiae2;
    }

    public String getCognomeAutore3() {
        return cognomeAutore3;
    }

    public void setCognomeAutore3(String cognomeAutore3) {
        this.cognomeAutore3 = cognomeAutore3;
    }

    public String getNomeAutore3() {
        return nomeAutore3;
    }

    public void setNomeAutore3(String nomeAutore3) {
        this.nomeAutore3 = nomeAutore3;
    }

    public String getTutelaSiae3() {
        return tutelaSiae3;
    }

    public void setTutelaSiae3(String tutelaSiae3) {
        this.tutelaSiae3 = tutelaSiae3;
    }

    public String getCognomeAutore4() {
        return cognomeAutore4;
    }

    public void setCognomeAutore4(String cognomeAutore4) {
        this.cognomeAutore4 = cognomeAutore4;
    }

    public String getNomeAutore4() {
        return nomeAutore4;
    }

    public void setNomeAutore4(String nomeAutore4) {
        this.nomeAutore4 = nomeAutore4;
    }

    public String getTutelaSiae4() {
        return tutelaSiae4;
    }

    public void setTutelaSiae4(String tutelaSiae4) {
        this.tutelaSiae4 = tutelaSiae4;
    }

    public String getCognomeAutore5() {
        return cognomeAutore5;
    }

    public void setCognomeAutore5(String cognomeAutore5) {
        this.cognomeAutore5 = cognomeAutore5;
    }

    public String getNomeAutore5() {
        return nomeAutore5;
    }

    public void setNomeAutore5(String nomeAutore5) {
        this.nomeAutore5 = nomeAutore5;
    }

    public String getTutelaSiae5() {
        return tutelaSiae5;
    }

    public void setTutelaSiae5(String tutelaSiae5) {
        this.tutelaSiae5 = tutelaSiae5;
    }

    public String getCognomeAutore6() {
        return cognomeAutore6;
    }

    public void setCognomeAutore6(String cognomeAutore6) {
        this.cognomeAutore6 = cognomeAutore6;
    }

    public String getNomeAutore6() {
        return nomeAutore6;
    }

    public void setNomeAutore6(String nomeAutore6) {
        this.nomeAutore6 = nomeAutore6;
    }

    public String getTutelaSiae6() {
        return tutelaSiae6;
    }

    public void setTutelaSiae6(String tutelaSiae6) {
        this.tutelaSiae6 = tutelaSiae6;
    }

    public String getCognomeAutore7() {
        return cognomeAutore7;
    }

    public void setCognomeAutore7(String cognomeAutore7) {
        this.cognomeAutore7 = cognomeAutore7;
    }

    public String getNomeAutore7() {
        return nomeAutore7;
    }

    public void setNomeAutore7(String nomeAutore7) {
        this.nomeAutore7 = nomeAutore7;
    }

    public String getTutelaSiae7() {
        return tutelaSiae7;
    }

    public void setTutelaSiae7(String tutelaSiae7) {
        this.tutelaSiae7 = tutelaSiae7;
    }

    public String getCognomeAutore8() {
        return cognomeAutore8;
    }

    public void setCognomeAutore8(String cognomeAutore8) {
        this.cognomeAutore8 = cognomeAutore8;
    }

    public String getNomeAutore8() {
        return nomeAutore8;
    }

    public void setNomeAutore8(String nomeAutore8) {
        this.nomeAutore8 = nomeAutore8;
    }

    public String getTutelaSiae8() {
        return tutelaSiae8;
    }

    public void setTutelaSiae8(String tutelaSiae8) {
        this.tutelaSiae8 = tutelaSiae8;
    }

    public String getCognomeAutore9() {
        return cognomeAutore9;
    }

    public void setCognomeAutore9(String cognomeAutore9) {
        this.cognomeAutore9 = cognomeAutore9;
    }

    public String getNomeAutore9() {
        return nomeAutore9;
    }

    public void setNomeAutore9(String nomeAutore9) {
        this.nomeAutore9 = nomeAutore9;
    }

    public String getTutelaSiae9() {
        return tutelaSiae9;
    }

    public void setTutelaSiae9(String tutelaSiae9) {
        this.tutelaSiae9 = tutelaSiae9;
    }

    public String getCognomeAutore10() {
        return cognomeAutore10;
    }

    public void setCognomeAutore10(String cognomeAutore10) {
        this.cognomeAutore10 = cognomeAutore10;
    }

    public String getNomeAutore10() {
        return nomeAutore10;
    }

    public void setNomeAutore10(String nomeAutore10) {
        this.nomeAutore10 = nomeAutore10;
    }

    public String getTutelaSiae10() {
        return tutelaSiae10;
    }

    public void setTutelaSiae10(String tutelaSiae10) {
        this.tutelaSiae10 = tutelaSiae10;
    }

    public String getGenere() {
        return genere;
    }

    public void setGenere(String genere) {
        this.genere = genere;
    }

    public String getGenerazione() {
        return generazione;
    }

    public void setGenerazione(String generazione) {
        this.generazione = generazione;
    }

    public String getDataPrimaTrasmissione() {
        return dataPrimaTrasmissione;
    }

    public void setDataPrimaTrasmissione(String dataPrimaTrasmissione) {
        this.dataPrimaTrasmissione = dataPrimaTrasmissione;
    }

    public String getCodiceCasa() {
        return codiceCasa;
    }

    public void setCodiceCasa(String codiceCasa) {
        this.codiceCasa = codiceCasa;
    }

    public String getCodiceMarca() {
        return codiceMarca;
    }

    public void setCodiceMarca(String codiceMarca) {
        this.codiceMarca = codiceMarca;
    }

    public String getDescMarcaDiscografica() {
        return descMarcaDiscografica;
    }

    public void setDescMarcaDiscografica(String descMarcaDiscografica) {
        this.descMarcaDiscografica = descMarcaDiscografica;
    }

    public String getSiglaNumero() {
        return siglaNumero;
    }

    public void setSiglaNumero(String siglaNumero) {
        this.siglaNumero = siglaNumero;
    }

    public String getNu1() {
        return nu1;
    }

    public void setNu1(String nu1) {
        this.nu1 = nu1;
    }

    public String getEdizione() {
        return edizione;
    }

    public void setEdizione(String edizione) {
        this.edizione = edizione;
    }

    public String getNu2() {
        return nu2;
    }

    public void setNu2(String nu2) {
        this.nu2 = nu2;
    }

    public String getDescEdizione() {
        return descEdizione;
    }

    public void setDescEdizione(String descEdizione) {
        this.descEdizione = descEdizione;
    }

    public String getCodProprieta() {
        return codProprieta;
    }

    public void setCodProprieta(String codProprieta) {
        this.codProprieta = codProprieta;
    }

    public String getDescProprieta() {
        return descProprieta;
    }

    public void setDescProprieta(String descProprieta) {
        this.descProprieta = descProprieta;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getDataVariazione() {
        return dataVariazione;
    }

    public void setDataVariazione(String dataVariazione) {
        this.dataVariazione = dataVariazione;
    }

    public String getKey() {
        return super.toString();
    }

    public void setKey(String key) {
        this.key = key;
    }
}


