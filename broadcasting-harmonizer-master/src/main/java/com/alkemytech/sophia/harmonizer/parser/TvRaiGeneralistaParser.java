package com.alkemytech.sophia.harmonizer.parser;

import com.alkemytech.sophia.broadcasting.dto.BdcConfigDTO;
import com.alkemytech.sophia.broadcasting.model.BdcInformationFileEntity;
import com.alkemytech.sophia.broadcasting.model.MusicType;
import com.alkemytech.sophia.broadcasting.service.interfaces.DecodeService;
import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.mapper.TemplateMapper;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.harmonizer.dto.BaseRaiDTO;
import com.alkemytech.sophia.harmonizer.utils.ExceptionHandler;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.util.CollectionUtils;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Singleton;
import org.slf4j.Logger;

import java.io.ByteArrayOutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Alessandro Russo on 07/12/2017.
 */
@Singleton
public class TvRaiGeneralistaParser extends GenericParser {

    private final static String TX_SUFFIX       = "_TX";
    private final static String OP_SUFFIX       = "_OP";

    private final static String AMBITO_LABEL    = "ambito";
    private final static String AMBITO_TV       = "1";

    public TvRaiGeneralistaParser(Logger logger, S3Service s3Service, TemplateMapper mapper, DecodeService decodeService) {
        super(logger, s3Service, mapper, decodeService);
    }

    @Override
    public Map<String, Object> parse(Map<String, Object> parameters) {
        //Result map (it may contain RECORDS if OK or ERRORS if KO)
        Map<String, Object> ret = new HashMap<>();
        try{
            if(parameters.containsKey(ParserParameters.CONFIGURATION) && parameters.containsKey(ParserParameters.FILES)){
                BdcConfigDTO configuration = (BdcConfigDTO) parameters.get(ParserParameters.CONFIGURATION);
                Map<BdcInformationFileEntity,AmazonS3URI> fileMap = (Map<BdcInformationFileEntity,AmazonS3URI>) parameters.get(ParserParameters.FILES);
                if( configuration!= null && fileMap != null ){
                    if( fileMap.size() == 2 ){
                        List<Object> showList = new ArrayList<>(); //TV SHOW RECORDS
                        List<Object> musicList = new ArrayList<>(); //TV MUSIC RECORDS
                        Integer idBroadcaster = null;
                        for(BdcInformationFileEntity currentFile : fileMap.keySet()){
                            ByteArrayOutputStream bos = new ByteArrayOutputStream();
                            try{
                                if( idBroadcaster == null){
                                    idBroadcaster = currentFile.getEmittente().getId();
                                }
                                AmazonS3URI currentUri = fileMap.get(currentFile);
                                //download file from Amanzon S3
                                logger.debug("report to parse -> {}", currentFile.getPercorso() );
                                s3Service.download(currentUri.getBucket(), currentUri.getKey(), bos);
                                if(currentFile.getNomeFile().contains(TX_SUFFIX)){
                                    Charset charset = super.prepareFile(currentFile, currentUri, bos);
                                    showList = mapper.parseFile(getConfigProperty(configuration.getParseConfigPath(), TX_SUFFIX),getConfigProperty(configuration.getParseStreamName(), TX_SUFFIX), bos, s3Service,charset);
                                }
                                else if(currentFile.getNomeFile().contains(OP_SUFFIX)){
                                    Charset charset = super.prepareFile(currentFile, currentUri, bos);
                                    musicList = mapper.parseFile(getConfigProperty(configuration.getParseConfigPath(), OP_SUFFIX),getConfigProperty(configuration.getParseStreamName(), OP_SUFFIX), bos, s3Service,charset);
                                }
                            }catch (Exception e){
                                currentFile.setStackTrace(getWrappedErrorMessage(e.getMessage()));
                                currentFile.setDecodificaErrore("Parsing error");
                                ret.put(ERRORS, ExceptionHandler.logStackTrace(e));
                            }

                            finally {
                                bos.flush();
                                bos.close();
                            }
                        }
                        if(!CollectionUtils.isNullOrEmpty(showList) && !CollectionUtils.isNullOrEmpty(musicList)){
                            List<Object> joinedRecorrds = joinResult(showList, musicList);
                            List<Map<String,Object>> normalizedRecords = normalize(idBroadcaster, configuration.getNormalizeRulePath(), joinedRecorrds);
                            cleanNormalizedRecords(normalizedRecords);
                            ret.put(PARSED_RECORDS, normalizedRecords);
                            return ret;
                        }
                        else{
                            ret.put(ERRORS, "Parsing result is empty!");
                        }
                    }
                    else{
                        ret.put(ERRORS, String.format("Unexpected files number: Expected %s -> Found %s",configuration.getExpectedFiles(), fileMap.size() ) );
                    }
                }
                else{
                    ret.put(ERRORS, "Parser parameters empty!");
                }
            }
            else{
                ret.put(ERRORS, "Expected parser parameters not found!");
            }
        }
        catch (Exception e){
            ret.put(ERRORS, ExceptionHandler.logStackTrace(e));
        }
        return ret;
    }

    private void cleanNormalizedRecords(List<Map<String,Object>> normalizedRecords) {
        for(Map<String, Object> current : normalizedRecords){
            if(current.containsKey(Constants.DECODE_ID_CATEGORIA_USO_OP) && current.get(Constants.DECODE_ID_CATEGORIA_USO_OP) != null ){
                MusicType musicType = decodeService.findMusicTypeById((Long) current.get(Constants.DECODE_ID_CATEGORIA_USO_OP));
                if (!"COMMENTO".equalsIgnoreCase(musicType.name)){ //si tratta di uno SHOW TV
                    current.put(Constants.REGISTA_TX, null);
                    current.put(Constants.TITOLO_ORIGINALE_EPISODIO_TX, null);
                }
            }
        }
    }

    private List<Object> joinResult(List<Object> showDTOList, List<Object> musicDTOList){
        List<Object> joinedRecords = new ArrayList<>();
        Map<BaseRaiDTO, List<Map<String, Object>>> filteredShow = filterRecord(showDTOList);
        Map<BaseRaiDTO, List<Map<String, Object>>> filteredMusic = filterRecord(musicDTOList);
        for(BaseRaiDTO currentBase : filteredShow.keySet()){
            if(filteredMusic.containsKey(currentBase)){
                for(Map<String,Object> currentShow : filteredShow.get(currentBase)){
                    for (Map<String,Object> currentMusic : filteredMusic.get(currentBase)){
                        Map<String,Object> currentRecord = createRecord(currentShow, currentMusic );
                        if(currentRecord != null){
                            joinedRecords.add(currentRecord);
                        }
                    }
                }
            }
        }
        return joinedRecords;
    }

    private Map<BaseRaiDTO, List<Map<String, Object>>> filterRecord(List<Object> recordList) {
        Map<BaseRaiDTO, List<Map<String, Object>>> ret = new HashMap<>();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        for(Object obj : recordList){
            Map<String, Object> currentRecord = (Map<String, Object>) obj;
            if(currentRecord.get(AMBITO_LABEL)!= null && currentRecord.get(AMBITO_LABEL).equals(AMBITO_TV)){
                BaseRaiDTO currentKey = objectMapper.convertValue(currentRecord, BaseRaiDTO.class);
                if(!ret.containsKey(currentKey)){
                    ret.put(currentKey, new ArrayList<Map<String, Object>>());
                }
                ret.get(currentKey).add(currentRecord);
            }
        }
        return ret;
    }

    private Map<String, Object> createRecord(Map<String, Object> currentShow, Map<String, Object> currentMusic) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        BaseRaiDTO baseShow = new ObjectMapper().convertValue(currentShow, BaseRaiDTO.class);
        BaseRaiDTO baseMusic = new ObjectMapper().convertValue(currentMusic, BaseRaiDTO.class);
        if(baseShow.equals(baseMusic)){
            Map<String, Object> ret = new HashMap<>();
            ret.putAll(currentShow);
            ret.putAll(currentMusic);
            logger.debug(" joined record -> {}", ret);
            return ret;
        }
        return null;
    }
}