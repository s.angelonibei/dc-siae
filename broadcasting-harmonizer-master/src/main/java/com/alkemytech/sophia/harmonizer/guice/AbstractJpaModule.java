package com.alkemytech.sophia.harmonizer.guice;

import com.alkemytech.sophia.common.config.ConfigurationLoader;
import com.google.inject.PrivateModule;
import com.google.inject.name.Names;
import com.google.inject.persist.jpa.JpaPersistModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public abstract class AbstractJpaModule extends PrivateModule {
    public final String[] args;
    private String unitName;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public AbstractJpaModule(String[] args, String unitName) {
        super();
        this.args = args;
        this.unitName = unitName;
    }

    protected Properties loadConfig() {
        final Properties properties = new ConfigurationLoader().withCommandLineArgs(args).load();
        Names.bindProperties(binder(), properties);
        bind(Properties.class)
                .annotatedWith(Names.named("configuration"))
                .toInstance(properties);
//        final InputStream input = classLoader.getResourceAsStream(configuration.getProperty("jpa." + unitName + ".config"));
        final File file = new File(properties.getProperty("jpa." + unitName + ".config"));
        final InputStream input;
        try  {
            if (file.exists()){
                input = new FileInputStream(file);
            } else {
                input = this.getClass().getResourceAsStream("/" + properties.getProperty("jpa." + unitName + ".config"));
            }
            properties.load(input);
            logger.debug("loadConfig: properties {}", properties);
            Names.bindProperties(binder(), properties);
        } catch (IOException e) {
            logger.error("loadConfig", e);
        }
        return properties;
    }

    protected void configure() {
        final JpaPersistModule jpaPersistModule = new JpaPersistModule(unitName);
        jpaPersistModule.properties(loadConfig());
        install(jpaPersistModule);
    }
}
