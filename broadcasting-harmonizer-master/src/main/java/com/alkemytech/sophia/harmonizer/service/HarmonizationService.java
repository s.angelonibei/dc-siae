package com.alkemytech.sophia.harmonizer.service;

import com.alkemytech.sophia.broadcasting.dto.BdcConfigDTO;
import com.alkemytech.sophia.broadcasting.dto.SearchBroadcasterConfigDTO;
import com.alkemytech.sophia.broadcasting.dto.harmonization.FieldErrorDTO;
import com.alkemytech.sophia.broadcasting.enums.Stato;
import com.alkemytech.sophia.broadcasting.model.*;
import com.alkemytech.sophia.broadcasting.service.interfaces.BroadcasterConfigService;
import com.alkemytech.sophia.broadcasting.service.interfaces.DecodeService;
import com.alkemytech.sophia.broadcasting.service.interfaces.NormalizedFileService;
import com.alkemytech.sophia.broadcasting.utils.Utilities;
import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.mapper.TemplateMapper;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.common.tools.StringTools;
import com.alkemytech.sophia.harmonizer.parser.GenericParser;
import com.alkemytech.sophia.harmonizer.parser.ParserParameters;
import com.alkemytech.sophia.harmonizer.utils.DirityChars;
import com.alkemytech.sophia.harmonizer.validator.GenericValidator;
import com.amazonaws.services.s3.AmazonS3URI;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BOMInputStream;
import org.apache.commons.lang.StringUtils;
import org.drools.runtime.StatefulKnowledgeSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;

/**
 * Created by Alessandro Russo on 01/12/2017.
 */
@Singleton
public class HarmonizationService {

    private static final String ENVIRONMENT = "ENVNAME";
    private static final String NORMALIZED_PATH = "%s/%s/NORMALIZED/%s/%s";
    private static final String SELECTED_FILE = "selectedFile";
    private static final String FILE_LIST = "fileList";

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final BroadcasterConfigService broadcasterConfigService;
    private final S3Service s3Service;
    private final DecodeService decodeService;
    private final NormalizedFileService normalizedFileService;
    private final Properties configuration;

    @Inject
    public HarmonizationService(@Named("configuration") Properties configuration,
                                BroadcasterConfigService broadcasterConfigService, S3Service s3Service,
                                DecodeService decodeService,
                                NormalizedFileService normalizedFileService) {
        this.configuration = configuration;
        this.broadcasterConfigService = broadcasterConfigService;
        this.s3Service = s3Service;
        this.decodeService = decodeService;
        this.normalizedFileService = normalizedFileService;
    }

    /**
     * Retrieve Broadcaster configuration
     *
     * @param uploadedFile report info stored on DB (bradcaster, channel, s3 path, etc.)
     * @return a Broadcaster configuration
     */
    public BdcConfigDTO retrieveConfig(BdcInformationFileEntity uploadedFile) {
        Integer idBroadcaster = (uploadedFile.getEmittente() != null && uploadedFile.getEmittente().getId() != null) ? uploadedFile.getEmittente().getId() : null;
        Integer idCanale = (uploadedFile.getCanale() != null && uploadedFile.getCanale().getId() != null) ? uploadedFile.getCanale().getId() : null;
        if (idBroadcaster == null && idCanale == null) {
            return null;
        }
        SearchBroadcasterConfigDTO searchConfigDto = new SearchBroadcasterConfigDTO(idBroadcaster, idCanale);
        return broadcasterConfigService.findActive(searchConfigDto);
    }

    public AmazonS3URI reserveSingleFile(String filePath, String suffix) {
        if (StringUtils.isNotEmpty(filePath)) {
            AmazonS3URI amazonS3URI = new AmazonS3URI(filePath);
            if (!s3Service.lockFile(amazonS3URI.getBucket(), amazonS3URI.getKey(), suffix)) {
                logger.debug("File {} preaviously reserved by other process", amazonS3URI.getKey());
                return null;
            }
            logger.debug("Reserved file at {}", filePath);
            return amazonS3URI;
        }
        return null;
    }

    public void releaseSingleFile(String bucket, String key, String suffix) {
        if (s3Service.doesObjectExist(bucket, key + suffix)) {
            s3Service.delete(bucket, key + suffix);
            logger.debug("Released file at {}", bucket + key);
        } else {
            logger.debug("Doesn't exist file at {}", bucket + key + suffix);
        }
    }

    public Map<BdcInformationFileEntity, AmazonS3URI> reserveMultipleFile(List<BdcInformationFileEntity> fileList, String suffix) {
        Map<BdcInformationFileEntity, AmazonS3URI> ret = new HashMap<>();
        for (BdcInformationFileEntity currentFile : fileList) {
            AmazonS3URI currentUri = reserveSingleFile(currentFile.getPercorso(), suffix);
            if (currentUri != null) {
                ret.put(currentFile, currentUri);
            } else {
                releaseMultipleFile(ret.values(), suffix);
            }
        }
        return ret;
    }

    /**
     * Delete from Amazon S3 a locked files list with the input suffix
     *
     * @param fileEntityList locked files list
     * @param suffix         lock suffix (eg. .lock, .prelock, etc.)
     */
    public void releaseMultipleFile(List<BdcInformationFileEntity> fileEntityList, String suffix) {
        if (fileEntityList != null) {
            for (BdcInformationFileEntity currentFile : fileEntityList) {
                AmazonS3URI amazonS3URI = new AmazonS3URI(currentFile.getPercorso());
                releaseSingleFile(amazonS3URI.getBucket(), amazonS3URI.getKey(), suffix);
            }
        }
    }

    public void releaseMultipleFile(Collection<AmazonS3URI> amazonS3URIs, String suffix) {
        if (amazonS3URIs != null) {
            for (AmazonS3URI uri : amazonS3URIs) {
                releaseSingleFile(uri.getBucket(), uri.getKey(), suffix);
            }
        }
    }

    public Map<String, Object> evaluateFile(BdcConfigDTO broadcasterConfig, BdcInformationFileEntity selectedFile, List<BdcInformationFileEntity> fileList) {
        Map<String, Object> ret = new HashMap<>();
        List<BdcInformationFileEntity> fileToParseList = new ArrayList<>();
        BdcConfigDTO bdcConfigDTO = null;
        if (StringUtils.isNotEmpty(broadcasterConfig.getFileValidator())) {
            try {
                GenericValidator validator = (GenericValidator) Class.forName(broadcasterConfig.getFileValidator()).newInstance();
                Map<String, Object> parameters = new HashMap<>();
                parameters.put(SELECTED_FILE, selectedFile);
                parameters.put(FILE_LIST, fileList);
                fileToParseList = validator.validateFile(parameters);
                bdcConfigDTO = validator.extractConfiguration(broadcasterConfig, selectedFile);
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        } else {
            bdcConfigDTO = broadcasterConfig;
            fileToParseList.add(selectedFile);
        }

        List<String> configMissingFields = validateFullConfiguration(bdcConfigDTO);
        if (!configMissingFields.isEmpty()) {
            logger.error("Broadcaster configuration missing following mandatory fields: {}", StringUtils.join(configMissingFields, ","));
        } else {
            ret.put("fileToParseList", fileToParseList);
            ret.put("configuration", bdcConfigDTO);
        }
        return ret;
    }

    private List<String> validateFullConfiguration(BdcConfigDTO broadcasterConfig) {
        List<String> ret = new ArrayList<>();
        if (broadcasterConfig == null) {
            ret.add("Empty Configuration");
        } else {
            if (StringUtils.isEmpty(broadcasterConfig.getParseConfigPath())) {
                ret.add("parseConfigPath");
            }
            if (StringUtils.isEmpty(broadcasterConfig.getParseStreamName())) {
                ret.add("parseStreamName");
            }
            if (StringUtils.isEmpty(broadcasterConfig.getFileParser())) {
                ret.add("fileParser");
            }
            if (StringUtils.isEmpty(broadcasterConfig.getNormalizeRulePath())) {
                ret.add("normalizeRulePath");
            }
            if (StringUtils.isEmpty(broadcasterConfig.getValidateRulePath())) {
                ret.add("validateRulePath");
            }
            if (StringUtils.isEmpty(broadcasterConfig.getWriteConfigPath())) {
                ret.add("writeConfigPath");
            }
            if (StringUtils.isEmpty(broadcasterConfig.getWriteConfigStream())) {
                ret.add("writeConfigStream");
            }
        }
        return ret;
    }

    @SuppressWarnings(value = "unchecked")
    public List<Map<String, Object>> parseReport(BdcConfigDTO broadcasterConfig, Map<BdcInformationFileEntity, AmazonS3URI> mapFile) {
        if (StringUtils.isNotEmpty(broadcasterConfig.getFileParser())) {
            try {
                GenericParser parser = (GenericParser) Class.forName(broadcasterConfig.getFileParser())
                        .getConstructor(Logger.class, S3Service.class, TemplateMapper.class, DecodeService.class)
                        .newInstance(logger, s3Service, new TemplateMapper(), decodeService);
//                parser.setS3Service(s3Service);
//                parser.setMapper(new TemplateMapper());
//                parser.setDecodeService(decodeService);
                Map<String, Object> parameters = new HashMap<>();
                parameters.put(ParserParameters.CONFIGURATION, broadcasterConfig);
                parameters.put(ParserParameters.FILES, mapFile);
                Map<String, ?> ret = parser.parse(parameters);
                if (ret.containsKey(GenericParser.ERRORS)) {
                    logger.error("parsing error : {}", ret.get(GenericParser.ERRORS));
                    return new ArrayList<>();
                }
                logger.info("file parsed successfully!");
                return (List<Map<String, Object>>) ret.get(GenericParser.PARSED_RECORDS);
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        } else {
            logger.debug("Parser assente");
            return new ArrayList<>();
        }
        return new ArrayList<>();
    }

    public List<Map<String, Object>> validateNormalizedRecords(BdcInformationFileEntity currentFile, BdcConfigDTO configDto, List<Map<String, Object>> listNormalizedRecords) throws Exception {
        if (StringUtils.isNotEmpty(configDto.getValidateRulePath())) {
            StatefulKnowledgeSession session = new RulesEngineService().createKnowledgeBaseFromSpreadsheet(configDto.getValidateRulePath(), s3Service).newStatefulKnowledgeSession();
//            session.addEventListener(new DebugAgendaEventListener());
//            session.addEventListener( new DefaultProcessEventListener());
            for (Map<String, Object> normalizedRecord : listNormalizedRecords) {
                setupDroolsSession(session, currentFile, normalizedRecord);
                session.insert(normalizedRecord);
                session.fireAllRules();
                Gson gson = new Gson();
                String errorString = gson.toJson(session.getGlobal(Constants.ERRORS));
                normalizedRecord.put(Constants.ERRORS, errorString);
//                logger.debug("- VALIDATION ERRORS -> {}", errorString.toString());
            }
            logger.info("normalized file content validated successfully!");
            return listNormalizedRecords;
        } else {
            logger.error("Validation rules file not found in config!");
            return null;
        }
    }

    private void setupDroolsSession(StatefulKnowledgeSession session, BdcInformationFileEntity currentFile, Map<String, Object> normalizedRecord) {
        boolean isTv = TipoBroadcaster.TELEVISIONE.equals(currentFile.getEmittente().getTipo_broadcaster());
        if (isTv) {
            if (normalizedRecord.containsKey(Constants.DECODE_ID_GENERE_TX) && normalizedRecord.get(Constants.DECODE_ID_GENERE_TX) != null) {
                ShowType showType = decodeService.findShowTypeById((Long) normalizedRecord.get(Constants.DECODE_ID_GENERE_TX));
                normalizedRecord.put(Constants.DECODE_DESC_GENERE_TX, showType != null ? showType.getCategory() : null);
            }
            if (normalizedRecord.containsKey(Constants.DECODE_ID_CATEGORIA_USO_OP) && normalizedRecord.get(Constants.DECODE_ID_CATEGORIA_USO_OP) != null) {
                MusicType musicType = decodeService.findMusicTypeById((Long) normalizedRecord.get(Constants.DECODE_ID_CATEGORIA_USO_OP));
                normalizedRecord.put(Constants.DECODE_DESC_CATEGORIA_USO_OP, musicType != null ? musicType.getName() : null);
            }
        }

        //Setup error fields
        Map<String, List<FieldErrorDTO>> errors = new HashMap<>();
        errors.put(Constants.REGOLA1, new ArrayList<FieldErrorDTO>());
        errors.put(Constants.REGOLA2, new ArrayList<FieldErrorDTO>());
        errors.put(Constants.REGOLA3, new ArrayList<FieldErrorDTO>());
        session.setGlobal(Constants.ERRORS, errors);

        //Insert compare fields
        session.setGlobal(Constants.COMPARE_CHANNEL, currentFile.getCanale() != null ? currentFile.getCanale().getId().toString() : null);
        session.setGlobal(Constants.COMPARE_DATE, currentFile.getDataUpload());
        session.setGlobal(Constants.COMPARE_YEAR, currentFile.getAnno());
        session.setGlobal(Constants.CURRENT_DATE, new Date());
        session.setGlobal(Constants.CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));
        session.setGlobal(Constants.DECODE_SERVICE, decodeService);
        Date date = null;
        if (currentFile.getAnno() != null && currentFile.getMese() != null) {
            date = Utilities.getDateFromMonthAndYear(currentFile.getMese(), currentFile.getAnno());
        }
        session.setGlobal(Constants.BEGIN_MONTH, date != null ? Utilities.getMonthStart(date) : null);
        session.setGlobal(Constants.END_MONTH, date != null ? Utilities.getMonthEnd(date) : null);
    }

    public BdcNormalizedFile createNormalizedFile(BdcConfigDTO broadcasterConfig, String bucketFile,
                                                  List<Map<String, Object>> validatedContent,
                                                  List<BdcInformationFileEntity> reportToParseList) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        File fileToWrite = null;
        try {
            String writeUploadBucket = StringUtils.isNotEmpty(broadcasterConfig.getWriteUploadBucket()) ? broadcasterConfig.getWriteUploadBucket() : Constants.DEFAULT_WRITE_BUCKET;
            String writeUploadExt = StringUtils.isNotEmpty(broadcasterConfig.getWriteFileExtension()) ? broadcasterConfig.getWriteFileExtension() : Constants.DEFAULT_WRITE_EXT;
            String emptyTemplateBucket = StringUtils.isNotEmpty(broadcasterConfig.getWriteTemplateBucket()) ? broadcasterConfig.getWriteTemplateBucket() : Constants.DEFAULT_WRITE_BUCKET;
            String fileName = bucketFile.substring(bucketFile.lastIndexOf("/") + 1, bucketFile.lastIndexOf(".")) + writeUploadExt;
            String broadcasterName = reportToParseList.get(0).getEmittente().getNome();
            logger.debug("Current Environment {}", System.getenv(ENVIRONMENT));
            String bucketPath = StringTools.buildBucketfromBroadcaster(NORMALIZED_PATH, writeUploadBucket, broadcasterName, fileName);
            AmazonS3URI amazonS3URI = new AmazonS3URI(emptyTemplateBucket);
            s3Service.download(amazonS3URI.getBucket(), amazonS3URI.getKey(), bos);
            //retrieve header from empty template
            TemplateMapper templateMapper = new TemplateMapper();
            Map<String, Object> header = templateMapper.parseEmpyTemplate(broadcasterConfig.getWriteConfigPath(), broadcasterConfig.getWriteConfigStream(), bos, s3Service);
            logger.debug("result " + header);
            fileToWrite = new File(fileName);
            logger.debug("create temp file {}", fileToWrite.getAbsolutePath());
            templateMapper.writeFile(header, validatedContent, broadcasterConfig.getWriteConfigPath(), broadcasterConfig.getWriteConfigStream(), fileToWrite, s3Service);
            amazonS3URI = new AmazonS3URI(bucketPath);
            logger.info("Load normalized file on Amazon S3 at {}", bucketPath);
            if (s3Service.upload(amazonS3URI.getBucket(), amazonS3URI.getKey(), fileToWrite)) {
                BdcNormalizedFile normalizedFile = new BdcNormalizedFile();
                normalizedFile.setFileBucket(bucketPath);
                normalizedFile.setFileName(fileName);
                normalizedFile.setCreationDate(new Date());
                for (BdcInformationFileEntity fileToParse : reportToParseList) {
                    fileToParse.setStato(Stato.VALIDATO);
                    fileToParse.setDataProcessamento(new Date());
                }
                normalizedFile.setOriginalReport(reportToParseList);
                return normalizedFileService.save(normalizedFile);
            }
        } catch (Exception e) {
            logger.error("", e);
        } finally {
            bos.flush();
            bos.close();
            assert fileToWrite != null;
            fileToWrite.deleteOnExit();
        }
        return null;
    }

    public void utf8BomAndDirtyCharRemove(AmazonS3URI uri, BdcInformationFileEntity fileName) {

        InputStream is = s3Service.download(uri.getBucket(), uri.getKey());
        try (BOMInputStream bomIn = new BOMInputStream(is,true)) {
            byte[] bytes;
            if (bomIn.hasBOM()) {
                bytes = IOUtils.toByteArray(is);
                // override the same path
//                bytes = replaceDirtyCharacters(bytes, fileName);
                s3Service.upload(uri.getBucket(), uri.getKey(), new ByteArrayInputStream(bytes), fileName.getNomeFile());
                logger.info("bom removed and overwrite file to s3");
            }
        } catch (Exception e) {
            logger.error("HarmonizationService error, {}", e.getMessage());
            e.printStackTrace();
        }
    }

    public void replaceDirtyCharacters(AmazonS3URI uri, BdcInformationFileEntity fileName) throws Exception {
        InputStream dirtyCharFile;
        AmazonS3URI dirtyCharPath = new AmazonS3URI(configuration.getProperty("dirtyCharPathEmittente"));
        AmazonS3URI dirtyCharPathDefault = new AmazonS3URI(configuration.getProperty("dirtyCharPathDefault"));
        if (s3Service.doesObjectExist(dirtyCharPath.getBucket(), dirtyCharPath.getKey().replace("{emittente}", fileName.getEmittente().getNome()))) {
            dirtyCharFile = s3Service.download(dirtyCharPath.getBucket(), dirtyCharPath.getKey().replace("{emittente}", fileName.getEmittente().getNome()));
            logger.debug("Dirty char list for emittente");
            logger.info("retrieved charlist for emittente");
            } else {
            dirtyCharFile = s3Service.download(dirtyCharPathDefault.getBucket(), dirtyCharPathDefault.getKey());
            logger.debug("Dirty char list default");
            logger.info("retrieved default charlist");
        }
        Map<String, String> dirtyCharList = DirityChars.getCharsList(dirtyCharFile);
        byte[] file = IOUtils.toByteArray(s3Service.download(uri.getBucket(), uri.getKey()));
        file = fixWrongEndOfLine(file);
        s3Service.upload(uri.getBucket(), uri.getKey(), new ByteArrayInputStream(DirityChars.replaceInFile(file, dirtyCharList)), fileName.getNomeFile());
        logger.info("replaced dirty char and overwrite to s3");
    }

    //R6-22 Fix Wrong End of Line [Problema doppio apice e a capo]
    public byte[] fixWrongEndOfLine(byte[] file) throws IOException {
        File tempFile = File.createTempFile("buffer", ".tmp");
        tempFile.deleteOnExit();

        try(FileWriter fw = new FileWriter(tempFile);
            Reader fr = new InputStreamReader(new ByteArrayInputStream(file));
            BufferedReader br = new BufferedReader(fr)) {
            boolean toMerge = false;
            String lineToMerge = null;
            String line;
            while (br.ready() && (line = br.readLine()) != null) {

                if(toMerge){
                    line = lineToMerge.concat(line);
                    toMerge = false;
                }
                if(line.endsWith(";\"")) {
                    toMerge = true;
                    lineToMerge = line;
                    continue;
                }
                fw.write(line + "\n");
            }
        } catch (Exception e) {
        }
        return FileUtils.readFileToByteArray(tempFile);
    }
}