package com.alkemytech.sophia.harmonizer.recursive;

import com.alkemytech.sophia.broadcasting.dto.BdcConfigDTO;
import com.alkemytech.sophia.broadcasting.enums.Stato;
import com.alkemytech.sophia.broadcasting.model.BdcInformationFileEntity;
import com.alkemytech.sophia.broadcasting.model.BdcNormalizedFile;
import com.alkemytech.sophia.broadcasting.service.interfaces.InformationFileEntityService;
import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.harmonizer.service.HarmonizationService;
import com.alkemytech.sophia.harmonizer.utils.ExceptionHandler;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.util.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.RecursiveAction;

public class ProcessReports extends RecursiveAction {
    private static final Logger logger = LoggerFactory.getLogger(ProcessReports.class);
    final static int THRESHOLD = 2;

    private List<BdcInformationFileEntity> reportToProcessList;
    private HarmonizationService harmonizationService;
    private InformationFileEntityService informationFileEntityService;


    public ProcessReports(List<BdcInformationFileEntity> reportToProcessList,
                          HarmonizationService harmonizationService,
                          InformationFileEntityService informationFileEntityService) {
        this.reportToProcessList = reportToProcessList;
        this.harmonizationService = harmonizationService;
        this.informationFileEntityService = informationFileEntityService;
    }

    @Override
    protected void compute() {
        if (reportToProcessList.size() < THRESHOLD) {
            try {
                computeDirectly();
            } catch (ParseException e) {
                logger.error(e.getMessage(), e);
            }
        } else {
            int middle = reportToProcessList.size() / 2;

            ProcessReports subTask1 = new ProcessReports(reportToProcessList.subList(0, middle), harmonizationService, informationFileEntityService);
            ProcessReports subTask2 = new ProcessReports(reportToProcessList.subList(middle, reportToProcessList.size()), harmonizationService, informationFileEntityService);

            invokeAll(subTask1, subTask2);
        }
    }

    private void computeDirectly() throws ParseException {
        for (BdcInformationFileEntity currentFile : reportToProcessList) {
            List<BdcInformationFileEntity> reportToParseList = null;
            try {
                //Reserve file on Amazon S3 in .prelock MODE
                logger.info("----------------------------------------");
                logger.info("START working {}", currentFile.getPercorso());
                logger.info("----------------------------------------");
                AmazonS3URI currentFileUri = harmonizationService.reserveSingleFile(currentFile.getPercorso(), Constants.PRELOCK_SUFFIX);
                if (currentFileUri != null) {
                    try {
                        //Load valid Broadcaster configuration
                        BdcConfigDTO broadcasterConfig = harmonizationService.retrieveConfig(currentFile);
                        if (broadcasterConfig == null) {
                            logger.error("ERROR - No valid configuration for file with id {} and name {}", currentFile.getId(), currentFile.getNomeFile());
                            harmonizationService.releaseSingleFile(currentFileUri.getBucket(), currentFileUri.getKey(), Constants.PRELOCK_SUFFIX);
                            continue;
                        }
                        //Validate configuration and check files list to parse
                        Map<String, Object> evaluationMap = harmonizationService.evaluateFile(broadcasterConfig, currentFile, reportToProcessList);
                        broadcasterConfig = evaluationMap.containsKey("configuration") ? (BdcConfigDTO) evaluationMap.get("configuration") : null;
                        reportToParseList = evaluationMap.containsKey("fileToParseList") ? (List<BdcInformationFileEntity>) evaluationMap.get("fileToParseList") : null;
                        if (!CollectionUtils.isNullOrEmpty(reportToParseList) && broadcasterConfig != null) {
                            Map<BdcInformationFileEntity, AmazonS3URI> mapFile = harmonizationService.reserveMultipleFile(reportToParseList, Constants.LOCK_SUFFIX);
                            if (!CollectionUtils.isNullOrEmpty(mapFile.keySet())) {
                                //Parsing original report via BEANIO and mapping to normalized template via DROOLS
                                List<Map<String, Object>> normalizedContent = harmonizationService.parseReport(broadcasterConfig, mapFile);
                                if (normalizedContent != null) {
                                    //Validating normalized field via DROOLS
                                    List<Map<String, Object>> validatedContent = harmonizationService.validateNormalizedRecords(currentFile, broadcasterConfig, normalizedContent);
                                    //Upload on S3 bucket and save on DB
                                    BdcNormalizedFile normalizedFile = harmonizationService.createNormalizedFile(broadcasterConfig, currentFile.getPercorso(), validatedContent, reportToParseList);
                                    if (normalizedFile == null) {
                                        logger.error("ERROR - Peristing normalized file error");
                                        harmonizationService.releaseMultipleFile(reportToParseList, Constants.LOCK_SUFFIX);
                                        harmonizationService.releaseSingleFile(currentFileUri.getBucket(), currentFileUri.getKey(), Constants.PRELOCK_SUFFIX);
                                        continue;
                                    }
                                    logger.info("----------------------------------------");
                                    logger.info("SUCCESS - {}", currentFile.getPercorso());
                                    logger.info("----------------------------------------");
                                } else {
                                    logger.error("ERROR - Change file status to {}", Stato.ERRORE);
                                    for (BdcInformationFileEntity report : reportToParseList) {
                                        report.setStato(Stato.ERRORE);
                                        report.setDataProcessamento(new Date());

                                      /*  if(!report.getStackTrace().isEmpty()) {
                                            String appoggio = report.getStackTrace().replace("'", "''");
                                            report.setStackTrace(appoggio);
                                        }*/

                                        informationFileEntityService.save(report);
                                    }
                                }
                            } else {
                                logger.error("ERROR - Locking file error");
                                harmonizationService.releaseSingleFile(currentFileUri.getBucket(), currentFileUri.getKey(), Constants.PRELOCK_SUFFIX);
                            }
                        } else {
                            logger.error("ERROR - Validation Error for file {} - {}", currentFile.getId(), currentFile.getNomeFile());
                            harmonizationService.releaseSingleFile(currentFileUri.getBucket(), currentFileUri.getKey(), Constants.PRELOCK_SUFFIX);
                        }
                    } finally {
                        harmonizationService.releaseMultipleFile(reportToParseList, Constants.LOCK_SUFFIX);
                        harmonizationService.releaseSingleFile(currentFileUri.getBucket(), currentFileUri.getKey(), Constants.PRELOCK_SUFFIX);

                    }
                } else {
                    logger.info("File {} already locked by other process!", currentFile.getNomeFile());
                }
            } catch (Exception e) {
                for (BdcInformationFileEntity report : reportToParseList) {
                    report.setStato(Stato.ERRORE);
                    report.setDataProcessamento(new Date());

                    informationFileEntityService.save(report);
                }
                logger.error("-- HARMONIZATION TASK ERROR {}", ExceptionHandler.logStackTrace(e));
            }
        }
    }
}
