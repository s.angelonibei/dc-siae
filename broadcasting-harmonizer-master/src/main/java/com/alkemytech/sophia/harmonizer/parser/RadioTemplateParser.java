package com.alkemytech.sophia.harmonizer.parser;

import com.alkemytech.sophia.broadcasting.dto.BdcConfigDTO;
import com.alkemytech.sophia.broadcasting.model.BdcInformationFileEntity;
import com.alkemytech.sophia.broadcasting.service.interfaces.DecodeService;
import com.alkemytech.sophia.common.mapper.TemplateMapper;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.harmonizer.utils.ExceptionHandler;
import com.amazonaws.services.s3.AmazonS3URI;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Alessandro Russo on 07/12/2017.
 */
@Singleton
public class RadioTemplateParser extends GenericParser {

    Logger logger = LoggerFactory.getLogger(getClass());

    public RadioTemplateParser(Logger logger, S3Service s3Service, TemplateMapper mapper, DecodeService decodeService) {
        super(logger, s3Service, mapper, decodeService);
    }


    @Override
    public Map<String, Object> parse(Map<String, Object> parameters) {
        //Result map (it may contain RECORDS if OK or ERRORS if KO)
        Map<String, Object> ret = new HashMap<>();
        try{
            if(parameters.containsKey(ParserParameters.CONFIGURATION) && parameters.containsKey(ParserParameters.FILES)){
                BdcConfigDTO configuration = (BdcConfigDTO) parameters.get(ParserParameters.CONFIGURATION);
                Map<BdcInformationFileEntity,AmazonS3URI> fileMap = (Map<BdcInformationFileEntity, AmazonS3URI>) parameters.get(ParserParameters.FILES);
                if( configuration!= null && fileMap != null ){
                    if( configuration.getExpectedFiles() == null || fileMap.size() == configuration.getExpectedFiles()){
                        for(BdcInformationFileEntity currentFile : fileMap.keySet()){
                            AmazonS3URI currentUri = fileMap.get(currentFile);
                            ByteArrayOutputStream bos = new ByteArrayOutputStream();
                            try{
                                //download file from Amanzon S3
                                logger.debug("report to parse -> {}", currentFile.getPercorso() );
                                s3Service.download(currentUri.getBucket(), currentUri.getKey(), bos);
                                Charset charset = super.prepareFile(currentFile, currentUri, bos);
                                List<Object> records =  mapper.parseFile(configuration.getParseConfigPath(), configuration.getParseStreamName(), bos, s3Service,charset);
                                if(records != null && records.size() > 0){
                                    ret.put(PARSED_RECORDS, normalize(currentFile.getEmittente().getId(), configuration.getNormalizeRulePath(), records));
                                    return ret;
                                }
                                else {
                                    ret.put(ERRORS, "Parsing result is empty!");
                                }
                            }catch (Exception e){
                                currentFile.setStackTrace(getWrappedErrorMessage(e.getMessage()));
                                currentFile.setDecodificaErrore("Parsing error");
                                ret.put(ERRORS, ExceptionHandler.logStackTrace(e));
                            }
                            finally {
                                bos.flush();
                                bos.close();
                            }
                        }
                    }
                    else{
                        ret.put(ERRORS, String.format("Unexpected files number: Expected %s -> Found %s", configuration.getExpectedFiles(), fileMap.size() ) );
                    }
                }
                else{
                    ret.put(ERRORS, "Parser parameters empty!");
                }
            }
            else{
                ret.put(ERRORS, "Expected parser parameters not found!");
            }
        }
        catch (Exception e){
            ret.put(ERRORS, ExceptionHandler.logStackTrace(e));
        }
        return ret;
    }
}
