package com.alkemytech.sophia.harmonizer.guice;

import com.alkemytech.sophia.broadcasting.service.*;
import com.alkemytech.sophia.broadcasting.service.interfaces.*;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.harmonizer.Harmonizer;
import com.google.inject.Provider;
import com.google.inject.persist.PersistService;
import com.google.inject.persist.jpa.JpaPersistModule;

import javax.persistence.EntityManager;
import java.util.Properties;

/**
 * Created by Alessandro Russo on 01/12/2017.
 */
public class McmdbJpaModule extends AbstractJpaModule {
    public static final String UNIT_NAME = "mcmdb";

    public McmdbJpaModule(String[] args) {
        super(args, UNIT_NAME);
    }

    @Override
    protected void configure() {
        final JpaPersistModule jpaPersistModule = new JpaPersistModule(UNIT_NAME);
        Properties moduleProperties = loadConfig();
        jpaPersistModule.properties(moduleProperties);
        install(jpaPersistModule);
        final Provider<EntityManager> entityManagerProvider = binder().getProvider(EntityManager.class);
        final DecodeService decodeService = new DecodeServiceImpl(entityManagerProvider);
        final BdcCanaliService canaleService = new BdcCanaliServiceImpl(entityManagerProvider);

        //BIND
        bind(BroadcasterConfigService.class).toInstance(new BroadcasterConfigServiceImpl(entityManagerProvider));
        bind(InformationFileEntityService.class).toInstance(new InformationFileEntityServiceImpl(entityManagerProvider));
        bind(DecodeService.class).toInstance(decodeService);
        bind(BdcCanaliService.class).toInstance(canaleService);
        bind(NormalizedFileService.class).toInstance(new NormalizedServiceImpl(entityManagerProvider,decodeService, canaleService));
        bind(S3Service.class).asEagerSingleton();
        bind(Harmonizer.class).asEagerSingleton();

        //EXPOSE
        expose(InformationFileEntityService.class);
        expose(BroadcasterConfigService.class);
        expose(DecodeService.class);
        expose(NormalizedFileService.class);
        expose(PersistService.class);
        expose(S3Service.class);
        expose(Harmonizer.class);
    }
}
