package com.alkemytech.sophia.harmonizer.validator;

import com.alkemytech.sophia.broadcasting.dto.BdcConfigDTO;
import com.alkemytech.sophia.broadcasting.model.BdcInformationFileEntity;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Alessandro Russo on 07/12/2017.
 */
public abstract class GenericValidator {

    public abstract List<BdcInformationFileEntity> validateFile(Map<String, Object> parameters);

    public abstract BdcConfigDTO extractConfiguration(BdcConfigDTO configuration, BdcInformationFileEntity selectedFile);

    public List<String> validateFullConfiguration( BdcConfigDTO broadcasterConfig ){
        List<String> ret = new ArrayList<>();
        if(broadcasterConfig == null){
            ret.add("Empty Configuration");
        }
        else{
            if( StringUtils.isEmpty( broadcasterConfig.getParseConfigPath()) ){
                ret.add("parseConfigPath");
            }
            if( StringUtils.isEmpty( broadcasterConfig.getParseStreamName()) ){
                ret.add("parseStreamName");
            }
            if( StringUtils.isEmpty( broadcasterConfig.getFileParser()) ){
                ret.add("fileParser");
            }
            if( StringUtils.isEmpty( broadcasterConfig.getNormalizeRulePath()) ){
                ret.add("normalizeRulePath");
            }
            if( StringUtils.isEmpty( broadcasterConfig.getValidateRulePath()) ){
                ret.add("validateRulePath");
            }
            if( StringUtils.isEmpty( broadcasterConfig.getWriteConfigPath()) ){
                ret.add("writeConfigPath");
            }
            if( StringUtils.isEmpty( broadcasterConfig.getWriteConfigStream()) ){
                ret.add("writeConfigStream");
            }
        }
        return ret;
    }

    protected BdcConfigDTO fillConfiguration( BdcConfigDTO fullConfiguration,
                                              String parserPart,
                                              String parseConfigPart,
                                              String parseStreamPart,
                                              String normalizeRulePart){
        BdcConfigDTO ret = new BdcConfigDTO();
        ret.setFileParser( getCorrectValue(fullConfiguration.getFileParser(), parserPart) );
        ret.setParseConfigPath( getCorrectValue(fullConfiguration.getParseConfigPath(), parseConfigPart) );
        ret.setParseStreamName( getCorrectValue(fullConfiguration.getParseStreamName(), parseStreamPart) );
        ret.setNormalizeRulePath( getCorrectValue(fullConfiguration.getNormalizeRulePath(), normalizeRulePart) );
        ret.setValidateRulePath( fullConfiguration.getValidateRulePath() );
        ret.setWriteConfigPath( fullConfiguration.getWriteConfigPath() );
        ret.setWriteConfigStream( fullConfiguration.getWriteConfigStream() );
        ret.setWriteTemplateBucket(fullConfiguration.getWriteTemplateBucket());
        return  ret;
    }

    protected String getCorrectValue(String values, String partName){
        String[] parts = values.split("\\|");
        if(parts != null && parts.length > 0){
            for(String current : parts){
                if(current.contains(partName)){
                    return current;
                }
            }
        }
        return null;
    }

}
