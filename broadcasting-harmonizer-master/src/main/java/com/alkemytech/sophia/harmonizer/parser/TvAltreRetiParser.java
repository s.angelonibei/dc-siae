package com.alkemytech.sophia.harmonizer.parser;

import com.alkemytech.sophia.broadcasting.dto.BdcConfigDTO;
import com.alkemytech.sophia.broadcasting.model.BdcInformationFileEntity;
import com.alkemytech.sophia.broadcasting.service.interfaces.DecodeService;
import com.alkemytech.sophia.common.mapper.TemplateMapper;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.harmonizer.utils.ExceptionHandler;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.util.CollectionUtils;
import com.google.inject.Singleton;
import org.slf4j.Logger;

import java.io.ByteArrayOutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Alessandro Russo on 07/12/2017.
 */
@Singleton
public class TvAltreRetiParser extends GenericParser {

    private final static String PROMO_PREFIX = "Promo";
    private final static String GEN_PREFIX = "Gen";

    public TvAltreRetiParser(Logger logger, S3Service s3Service, TemplateMapper mapper, DecodeService decodeService) {
        super(logger, s3Service, mapper, decodeService);
    }

    @Override
    public Map<String, Object> parse(Map<String, Object> parameters) {
        //Result map (it may contain RECORDS if OK or ERRORS if KO)
        Map<String, Object> ret = new HashMap<>();
        try{
            if(parameters.containsKey(ParserParameters.CONFIGURATION) && parameters.containsKey(ParserParameters.FILES)){
                BdcConfigDTO configuration = (BdcConfigDTO) parameters.get(ParserParameters.CONFIGURATION);
                Map<BdcInformationFileEntity,AmazonS3URI> fileMap = (Map<BdcInformationFileEntity, AmazonS3URI>) parameters.get(ParserParameters.FILES);
                if( configuration!= null && fileMap != null ){
                    if( configuration.getExpectedFiles() == null || fileMap.size() == 1){
                        for(BdcInformationFileEntity currentFile : fileMap.keySet()){
                            AmazonS3URI currentUri = fileMap.get(currentFile);
                            ByteArrayOutputStream bos = new ByteArrayOutputStream();
                            try{
                                //download file from Amanzon S3
                                logger.debug("report to parse -> {}", currentFile.getPercorso() );
                                s3Service.download(currentUri.getBucket(), currentUri.getKey(), bos);
                                String parseConfigPath = configuration.getParseConfigPath();
                                Charset charset = super.prepareFile(currentFile, currentUri, bos);
                                List<Object> records = mapper.parseFile(parseConfigPath, configuration.getParseStreamName(), bos, s3Service,charset);

                                if( !CollectionUtils.isNullOrEmpty(records) ){
                                    Integer idBroadcaster = currentFile.getEmittente().getId();
                                    String normalizedRulePath = configuration.getNormalizeRulePath();

                                    List<Object> mergedRecords = new ArrayList();
                                    HashMap<String, String> currentTx = null;
                                    for(Object current : records){
                                        HashMap<String, String> currentCast = (HashMap<String, String>) current;
                                        if (currentCast.get("recordType").equals("RPC")){
                                            currentTx = currentCast;
                                            continue;
                                        }
                                        else if (currentCast.get("recordType").equals("ROM")){
                                            HashMap<String, String> musicRpc = (HashMap<String, String>) currentTx.clone();
                                            musicRpc.put("titoloOpera", currentCast.get("titoloOpera"));
                                            musicRpc.put("cognomeCompositore", currentCast.get("cognomeCompositore"));
                                            musicRpc.put("nomeCompositore", currentCast.get("nomeCompositore"));
                                            musicRpc.put("cognomeCompositore2", currentCast.get("cognomeCompositore2"));
                                            musicRpc.put("nomeCompositore2", currentCast.get("nomeCompositore2"));
                                            musicRpc.put("durata", currentCast.get("durata"));
                                            musicRpc.put("categoriaUso", currentCast.get("categoriaUso"));
                                            mergedRecords.add(musicRpc);
                                        }
                                        else if (currentCast.get("recordType").equals("ROF")){
                                            HashMap<String, String> showRpc = (HashMap<String, String>) currentTx.clone();
                                            showRpc.put("durataTx", currentCast.get("durataEffettivaTx"));
                                            showRpc.put("titoloTx", currentCast.get("titoloTx"));
                                            showRpc.put("titoloOriginaleTx", currentCast.get("titoloOriginaleTx"));
                                            showRpc.put("codNazionalita", currentCast.get("codNazionalita"));
                                            showRpc.put("annoProduzione", currentCast.get("annoProduzione"));
                                            showRpc.put("produttore", currentCast.get("produttore"));
                                            showRpc.put("regista", currentCast.get("regista"));
                                            showRpc.put("titoloEpisodioTx", currentCast.get("titoloEpisodioTx"));
                                            showRpc.put("titoloOriginaleEpisodioTx", currentCast.get("titoloOriginaleEpisodioTx"));
                                            showRpc.put("numEpisodio", currentCast.get("numEpisodio"));
                                            showRpc.put("categoriaUso", null);
                                            mergedRecords.add(showRpc);
                                        }

                                    }
                                    ret.put(PARSED_RECORDS, normalize(idBroadcaster, normalizedRulePath, mergedRecords));
                                    return ret;
                                }
                                else{
                                    ret.put(ERRORS, "Parsing result is empty!");
                                }
                            }catch (Exception e){
                                currentFile.setStackTrace(getWrappedErrorMessage(e.getMessage()));
                                currentFile.setDecodificaErrore("Parsing error");
                                ret.put(ERRORS, ExceptionHandler.logStackTrace(e));
                            }
                            finally {
                                bos.flush();
                                bos.close();
                            }
                        }
                    }
                    else{
                        ret.put(ERRORS, String.format("Unexpected files number: Expected %s -> Found %s", configuration.getExpectedFiles(), fileMap.size() ) );
                    }
                }
                else{
                    ret.put(ERRORS, "Parser parameters empty!");
                }
            }
            else{
                ret.put(ERRORS, "Expected parser parameters not found!");
            }
        }
        catch (Exception e){
            ret.put(ERRORS, ExceptionHandler.logStackTrace(e));
        }
        return ret;
    }
}
