package com.alkemytech.sophia.harmonizer.utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class DirityChars {

    //Effettua la sostituzione dei caratteri sporchi
    public static byte[] replaceInFile(byte[] file, Map<String, String> mapCharsList) throws Exception {

        final Logger logger = LoggerFactory.getLogger(DirityChars.class);

        File tempFile = File.createTempFile("buffer", ".tmp");
        tempFile.deleteOnExit();

        try(FileWriter fw = new FileWriter(tempFile);
            Reader fr = new InputStreamReader(new ByteArrayInputStream(file));
            BufferedReader br = new BufferedReader(fr)) {
            int lineCounter = 0;
            String line = null;
            while (br.ready() && (line = br.readLine()) != null) {

                if (lineCounter > 0) {
                    for (String c : mapCharsList.keySet().stream().collect(Collectors.toList()))
                        line = line.replaceAll(Pattern.quote(c), Matcher.quoteReplacement(mapCharsList.get(c)));
                } else {
                    logger.debug("LINE " + line);
                }
                lineCounter++;
                fw.write(line + "\n");
            }
        } catch (Exception e) {
        }

        return FileUtils.readFileToByteArray(tempFile);

        // Finally replace the original file.
        //tempFile.renameTo(file);
        //copyContent(tempFile, file);
    }

    //Recupera i caratteri sporchi con i relativi sostituti
    public static Map<String, String> getCharsList(InputStream charslist) {

        Map<String, String> mapCharsList = new HashMap<>();

        //Recupero i caratteri dalla lista
        try (final InputStreamReader reader = new InputStreamReader(charslist, StandardCharsets.UTF_8);
             final BufferedReader br = new BufferedReader(reader)) {

            //FIX problema dubplicate key
            /*mapCharsList = br.lines()
                    .filter(a -> a.indexOf("#") != 0)
                    .map(str -> str.split("->"))
                    .collect(Collectors.toMap(str -> str[0], str -> str.length == 1 ? "" : str[1])); */
            for(String line : br.lines().collect(Collectors.toList())) {
                String[] chars = line.split("->");
                mapCharsList.put(chars[0],chars.length == 1 ? "" : chars[1]);
            }

        } catch (Exception e) {
        }
        return mapCharsList;
    }

    /*public static void copyContent(File a, File b) throws Exception {
        FileInputStream in = new FileInputStream(a);
        FileOutputStream out = new FileOutputStream(b);

        try {
            int n;
            // read() function to read the
            // byte of data
            while ((n = in.read()) != -1) {
                // write() function to write
                // the byte of data
                out.write(n);
            }
        } finally {
            if (in != null) {
                // close() function to close the
                // stream
                in.close();
            }
            // close() function to close
            // the stream
            if (out != null) {
                out.close();
            }
        }
        System.out.println("File Copied");
    }

     */
}
