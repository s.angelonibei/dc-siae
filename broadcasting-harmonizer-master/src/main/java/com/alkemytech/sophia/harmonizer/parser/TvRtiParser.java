package com.alkemytech.sophia.harmonizer.parser;

import com.alkemytech.sophia.broadcasting.dto.BdcConfigDTO;
import com.alkemytech.sophia.broadcasting.model.BdcInformationFileEntity;
import com.alkemytech.sophia.broadcasting.service.interfaces.DecodeService;
import com.alkemytech.sophia.common.mapper.TemplateMapper;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.harmonizer.utils.ExceptionHandler;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.util.CollectionUtils;
import com.google.inject.Singleton;
import org.slf4j.Logger;

import java.io.ByteArrayOutputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Alessandro Russo on 07/12/2017.
 */
@Singleton
public class TvRtiParser extends GenericParser {

    private final static String PROMO_PREFIX = "Promo";
    private final static String GEN_PREFIX = "Gen";

    public TvRtiParser(Logger logger, S3Service s3Service, TemplateMapper mapper, DecodeService decodeService) {
        super(logger, s3Service, mapper, decodeService);
    }

    @Override
    public Map<String, Object> parse(Map<String, Object> parameters) {
        //Result map (it may contain RECORDS if OK or ERRORS if KO)
        Map<String, Object> ret = new HashMap<>();
        try{
            if(parameters.containsKey(ParserParameters.CONFIGURATION) && parameters.containsKey(ParserParameters.FILES)){
                BdcConfigDTO configuration = (BdcConfigDTO) parameters.get(ParserParameters.CONFIGURATION);
                Map<BdcInformationFileEntity,AmazonS3URI> fileMap = (Map<BdcInformationFileEntity, AmazonS3URI>) parameters.get(ParserParameters.FILES);
                if( configuration!= null && fileMap != null ){
                    if( configuration.getExpectedFiles() == null || fileMap.size() == 1){
                        for(BdcInformationFileEntity currentFile : fileMap.keySet()){
                            AmazonS3URI currentUri = fileMap.get(currentFile);
                            ByteArrayOutputStream bos = new ByteArrayOutputStream();
                            try{
                                //download file from Amanzon S3
                                logger.debug("report to parse -> {}", currentFile.getPercorso() );
                                s3Service.download(currentUri.getBucket(), currentUri.getKey(), bos);
                                String parseConfigPath = currentFile.getNomeFile().contains(PROMO_PREFIX) ? getConfigProperty(configuration.getParseConfigPath(), PROMO_PREFIX) : getConfigProperty(configuration.getParseConfigPath(), GEN_PREFIX);
                                String parseStreamName = currentFile.getNomeFile().contains(PROMO_PREFIX) ? getConfigProperty(configuration.getParseStreamName(), PROMO_PREFIX) : getConfigProperty(configuration.getParseStreamName(), GEN_PREFIX);
                                Charset charset = super.prepareFile(currentFile, currentUri, bos);
                                List<Object> records = mapper.parseFile(parseConfigPath, parseStreamName, bos, s3Service,charset);

                                if( !CollectionUtils.isNullOrEmpty(records) ){
                                    Integer idBroadcaster = currentFile.getEmittente().getId();
                                    String normalizedRulePath = currentFile.getNomeFile().contains(PROMO_PREFIX) ? getConfigProperty(configuration.getNormalizeRulePath(), PROMO_PREFIX) : getConfigProperty(configuration.getNormalizeRulePath(), GEN_PREFIX);
                                    ret.put(PARSED_RECORDS, normalize(idBroadcaster, normalizedRulePath, records));
                                    return ret;
                                }else {
                                    ret.put(ERRORS, "Parsing result is empty!");
                                    currentFile.setDecodificaErrore("Parsing result is empty!");
                                }
                            }catch (Exception e){
                                currentFile.setStackTrace(getWrappedErrorMessage(e.getMessage()));
                                currentFile.setDecodificaErrore("Parsing Error");
                                ret.put(ERRORS, ExceptionHandler.logStackTrace(e));
                            }
                            finally {
                                bos.flush();
                                bos.close();
                            }
                        }
                    }
                    else{
                        ret.put(ERRORS, String.format("Unexpected files number: Expected %s -> Found %s", configuration.getExpectedFiles(), fileMap.size() ) );
                    }
                }
                else{
                    ret.put(ERRORS, "Parser parameters empty!");
                }
            }
            else{
                ret.put(ERRORS, "Expected parser parameters not found!");
            }
        }
        catch (Exception e){
            ret.put(ERRORS, ExceptionHandler.logStackTrace(e));
        }
        return ret;
    }
}
