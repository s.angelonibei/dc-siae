package com.alkemytech.sophia.harmonizer.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Alessandro Russo on 04/12/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseRaiDTO {

    private String ambito;
    private String sede;
    private String rete;
    private String data;
    private String progressivo;

    public String getAmbito() {
        return ambito;
    }

    public void setAmbito(String ambito) {
        this.ambito = ambito;
    }

    public String getSede() {
        return sede;
    }

    public void setSede(String sede) {
        this.sede = sede;
    }

    public String getRete() {
        return rete;
    }

    public void setRete(String rete) {
        this.rete = rete;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getProgressivo() {
        return progressivo;
    }

    public void setProgressivo(String progressivo) {
        this.progressivo = progressivo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseRaiDTO that = (BaseRaiDTO) o;

        if (ambito != null ? !ambito.equals(that.ambito) : that.ambito != null) return false;
        if (sede != null ? !sede.equals(that.sede) : that.sede != null) return false;
        if (rete != null ? !rete.equals(that.rete) : that.rete != null) return false;
        if (data != null ? !data.equals(that.data) : that.data != null) return false;
        return progressivo != null ? progressivo.equals(that.progressivo) : that.progressivo == null;

    }

    @Override
    public int hashCode() {
        int result = ambito != null ? ambito.hashCode() : 0;
        result = 31 * result + (sede != null ? sede.hashCode() : 0);
        result = 31 * result + (rete != null ? rete.hashCode() : 0);
        result = 31 * result + (data != null ? data.hashCode() : 0);
        result = 31 * result + (progressivo != null ? progressivo.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "BaseRaiDTO{" +
                "ambito='" + ambito + '\'' +
                ", sede='" + sede + '\'' +
                ", rete='" + rete + '\'' +
                ", data='" + data + '\'' +
                ", progressivo='" + progressivo + '\'' +
                '}';
    }
}
