package com.alkemytech.sophia.harmonizer;

import com.alkemytech.sophia.broadcasting.dto.BdcConfigDTO;
import com.alkemytech.sophia.broadcasting.enums.Stato;
import com.alkemytech.sophia.broadcasting.model.BdcInformationFileEntity;
import com.alkemytech.sophia.broadcasting.model.BdcNormalizedFile;
import com.alkemytech.sophia.broadcasting.service.interfaces.DecodeService;
import com.alkemytech.sophia.broadcasting.service.interfaces.InformationFileEntityService;
import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.harmonizer.guice.McmdbJpaModule;
import com.alkemytech.sophia.harmonizer.service.HarmonizationService;
import com.alkemytech.sophia.harmonizer.utils.ExceptionHandler;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.util.CollectionUtils;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;
import com.google.inject.persist.PersistService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.ServerSocket;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.alkemytech.sophia.harmonizer.utils.DirityChars.getCharsList;
import static com.alkemytech.sophia.harmonizer.utils.DirityChars.replaceInFile;

/**
 * Created by Alessandro Russo on 01/12/2017.
 */
public class Harmonizer {

    private static final Logger logger = LoggerFactory.getLogger(Harmonizer.class);

    private final Integer listenPort;
    private HarmonizationService harmonizationService;
    private InformationFileEntityService informationFileEntityService;

    @Inject
    public Harmonizer( @Named("listenPort") Integer listenPort,
            InformationFileEntityService informationFileEntityService,
                      HarmonizationService harmonizationService) {
        this.listenPort = listenPort;
        this.informationFileEntityService = informationFileEntityService;
        this.harmonizationService = harmonizationService;


    }

    public static void main(String[] args) {
        long start = 0;
        try {
            start = System.currentTimeMillis();
            logger.info("HARMONIZER STARTED at " + new Date(start));
            final Injector injector = Guice.createInjector( new McmdbJpaModule(args) );
            try{
                //Setup services
                injector.getInstance(PersistService.class).start();
                injector.getInstance(S3Service.class).startup();
                injector.getInstance(DecodeService.class).start();
                Harmonizer harmonizer = injector.getInstance(Harmonizer.class);

                //Run business logic
                harmonizer.process();
            }
            finally {
                //Stop services
                injector.getInstance(PersistService.class).stop();
                injector.getInstance(S3Service.class).shutdown();
            }
        } catch (Throwable e) {
            logger.error(e.getMessage(), e);
        } finally {
            long finish = System.currentTimeMillis();
            logger.info("HARMONIZER FINISHED AT " + new Date(finish) + ".");
            logger.info("THE EXECUTION TOOK " + Harmonizer.getDifference(new Date(start), new Date(finish)));
            System.exit(0);
        }
    }

    /**
     * Process Original Report in state "DA_ELABORARE"
     * @throws Exception
     */
    private void process() throws Exception {

        Locale.setDefault(Locale.ITALY);

        try(final ServerSocket socket = new ServerSocket( listenPort )){
            logger.info("HARMONIZATION TASK START: listening on {}", socket.getLocalSocketAddress());
            //Retrieve files to process
            List<BdcInformationFileEntity> reportToProcessList = informationFileEntityService.findByStato(Stato.DA_ELABORARE);
            if( !CollectionUtils.isNullOrEmpty(reportToProcessList) ) {
                logger.debug("Retrieved {} files to process ", reportToProcessList.size());
                for( BdcInformationFileEntity currentFile : reportToProcessList ){
                    List<BdcInformationFileEntity> reportToParseList = null;
                    try{
                        //Reserve file on Amazon S3 in .prelock MODE
                        logger.info("----------------------------------------");
                        logger.info("START working {}", currentFile.getPercorso());
                        logger.info("----------------------------------------");
                        AmazonS3URI currentFileUri =  harmonizationService.reserveSingleFile(currentFile.getPercorso(), Constants.PRELOCK_SUFFIX);
                        if( currentFileUri != null ){
                            try{
                                //Load valid Broadcaster configuration
                                logger.debug("Load valid Broadcaster configuration");
                                BdcConfigDTO broadcasterConfig = harmonizationService.retrieveConfig(currentFile);
                                if(broadcasterConfig == null){
                                    logger.error("ERROR - No valid configuration for file with id {} and name {}", currentFile.getId(), currentFile.getNomeFile() );
                                    harmonizationService.releaseSingleFile(currentFileUri.getBucket(), currentFileUri.getKey(), Constants.PRELOCK_SUFFIX);
                                    continue;
                                }
                                //R6-22 UTF8-BOM Remove
                                harmonizationService.utf8BomAndDirtyCharRemove(currentFileUri, currentFile);
                                //FIX in int-test errore LA7 file corrotto
                                if(!currentFile.getNomeFile().contains("xls") && !currentFile.getNomeFile().contains("xlsx")){
                                    harmonizationService.replaceDirtyCharacters(currentFileUri,currentFile);
                                }
//                                harmonizationService.replaceDirtyCharacters(currentFileUri, currentFile);
                                //Validate configuration and check files list to parse
                                Map<String, Object> evaluationMap = harmonizationService.evaluateFile(broadcasterConfig, currentFile, reportToProcessList);
                                broadcasterConfig = evaluationMap.containsKey("configuration") ? (BdcConfigDTO) evaluationMap.get("configuration") : null;
                                reportToParseList = evaluationMap.containsKey("fileToParseList") ? (List<BdcInformationFileEntity>) evaluationMap.get("fileToParseList") : null;
                                if( !CollectionUtils.isNullOrEmpty(reportToParseList) && broadcasterConfig != null ){
                                    logger.debug("ReportParseList is not Empty"+reportToParseList.toString());
                                    Map<BdcInformationFileEntity, AmazonS3URI> mapFile = harmonizationService.reserveMultipleFile(reportToParseList, Constants.LOCK_SUFFIX);
                                    if( !CollectionUtils.isNullOrEmpty(mapFile.keySet()) ){
                                        logger.debug("Parsing original report via BEANIO and mapping to normalized template via DROOLS");
                                        //Parsing original report via BEANIO and mapping to normalized template via DROOLS
                                        List<Map<String, Object> > normalizedContent  = harmonizationService.parseReport(broadcasterConfig, mapFile);
                                        if( normalizedContent != null ){
                                            //Validating normalized field via DROOLS
                                            logger.debug("Validating normalized field via DROOLS");
                                            List<Map<String, Object> > validatedContent = harmonizationService.validateNormalizedRecords(currentFile, broadcasterConfig, normalizedContent);
                                            //Upload on S3 bucket and save on DB
                                            BdcNormalizedFile normalizedFile = harmonizationService.createNormalizedFile(broadcasterConfig, currentFile.getPercorso(), validatedContent, reportToParseList);
                                            logger.debug("Uploaded on S3 bucket and save on DB");
                                            if( normalizedFile == null){
                                                logger.error("ERROR - Peristing normalized file error" );
                                                harmonizationService.releaseMultipleFile(reportToParseList, Constants.LOCK_SUFFIX);
                                                harmonizationService.releaseSingleFile(currentFileUri.getBucket(), currentFileUri.getKey(), Constants.PRELOCK_SUFFIX);
                                                continue;
                                            }
                                            logger.info("----------------------------------------");
                                            logger.info("SUCCESS - {}", currentFile.getPercorso());
                                            logger.info("----------------------------------------");
                                        }
                                        else{
                                            logger.error("ERROR - Change file status to {}", Stato.ERRORE);
                                            for(BdcInformationFileEntity report : reportToParseList){
                                                report.setStato(Stato.ERRORE);
                                                report.setDataProcessamento(new Date());
                                                informationFileEntityService.save(report);
                                            }
                                        }
                                    }
                                    else{
                                        logger.error("ERROR - Locking file error" );
                                        harmonizationService.releaseSingleFile(currentFileUri.getBucket(), currentFileUri.getKey(), Constants.PRELOCK_SUFFIX);
                                    }
                                }
                                else{
                                    logger.error("ERROR - Validation Error for file {} - {}", currentFile.getId(), currentFile.getNomeFile() );
                                    harmonizationService.releaseSingleFile(currentFileUri.getBucket(), currentFileUri.getKey(), Constants.PRELOCK_SUFFIX);
                                }
                            }
                            finally {
                                harmonizationService.releaseMultipleFile(reportToParseList, Constants.LOCK_SUFFIX);
                                harmonizationService.releaseSingleFile(currentFileUri.getBucket(), currentFileUri.getKey(), Constants.PRELOCK_SUFFIX);
                            }
                        }
                        else{
                            logger.info("File {} already locked by other process!", currentFile.getNomeFile());
                        }
                    }
                    catch (Exception e){
                        logger.error("-- HARMONIZATION TASK ERROR {}", ExceptionHandler.logStackTrace(e));
                        e.printStackTrace();
                        for(BdcInformationFileEntity report : reportToParseList){
                            logger.debug("Sono nel for reportToParseList");
                            report.setStato(Stato.ERRORE);
                            report.setDataProcessamento(new Date());
                            informationFileEntityService.save(report);
                        }
                    }
                }
            }
            else {
                logger.info("No Report with status {} found!", Stato.DA_ELABORARE);
            }
            logger.info("-- HARMONIZATION TASK END --");
        }
    }

    //1 minute = 60 seconds
    //1 hour = 60 x 60 = 3600
    //1 day = 3600 x 24 = 86400
    public static String getDifference(Date startDate, Date endDate){

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        /*logger.info("startDate : " + startDate);
        logger.info("endDate : "+ endDate);
        logger.info("different : " + different);*/

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        return  String.format("%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays,
                elapsedHours, elapsedMinutes, elapsedSeconds);

    }
}