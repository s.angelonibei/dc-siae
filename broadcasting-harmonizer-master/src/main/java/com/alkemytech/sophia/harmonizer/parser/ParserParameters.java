package com.alkemytech.sophia.harmonizer.parser;

/**
 * Created by Alessandro Russo on 08/12/2017.
 */
public interface ParserParameters {
    String CONFIGURATION = "configuration";
    String FILES         = "fileList";
}
