package com.alkemytech.sophia.harmonizer.validator;

import com.alkemytech.sophia.broadcasting.dto.BdcConfigDTO;
import com.alkemytech.sophia.broadcasting.model.BdcInformationFileEntity;
import com.amazonaws.util.CollectionUtils;
import com.google.inject.Singleton;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.alkemytech.sophia.harmonizer.utils.HarmonizerConstants.*;

/**
 * Created by Alessandro Russo on 07/12/2017.
 */
@Singleton
public class RaiValidator extends GenericValidator {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public List<BdcInformationFileEntity> validateFile(Map<String, Object> parameters) {
        List<BdcInformationFileEntity> ret = new ArrayList<>();
        if( parameters.containsKey("selectedFile") && parameters.containsKey("fileList") ){
            BdcInformationFileEntity selectedFile = (BdcInformationFileEntity) parameters.get("selectedFile");
            List<BdcInformationFileEntity> fileList = (List<BdcInformationFileEntity>) parameters.get("fileList");
            if( selectedFile != null && !CollectionUtils.isNullOrEmpty(fileList) ){
                logger.debug("VALIDATION STARTED");
                if( StringUtils.isNotEmpty(selectedFile.getNomeFile()) && selectedFile.getNomeFile().contains(RAI_TX_SUFFIX) ){
                    ret = checkGeneralista(selectedFile, fileList);
                }
                else if( StringUtils.isNotEmpty(selectedFile.getNomeFile()) && selectedFile.getNomeFile().contains(RAI_CINEMA_PART) ){
                    ret.add(selectedFile);
                }
                else if( StringUtils.isNotEmpty(selectedFile.getNomeFile()) && selectedFile.getNomeFile().contains(RAI_MUSICA_PART) ){
                    ret.add(selectedFile);
                }
            }
            else{
                logger.error("Validation parameters empty!");
            }
        }
        else{
            logger.error("Expected validators parameters not found!");
        }
        return ret;
    }

    @Override
    public BdcConfigDTO extractConfiguration(BdcConfigDTO configuration, BdcInformationFileEntity selectedFile) {
        if( selectedFile != null ){
            if( StringUtils.isNotEmpty(selectedFile.getNomeFile()) && selectedFile.getNomeFile().contains(RAI_TX_SUFFIX) ){
                return configGeneralista(configuration);
            }
            else if( StringUtils.isNotEmpty(selectedFile.getNomeFile()) && selectedFile.getNomeFile().contains(RAI_CINEMA_PART) ){
                return fillConfiguration(configuration, RAI_TEMATICI_PART, RAI_CINEMA_PART, RAI_CINEMA_PART, RAI_CINEMA_PART );
            }
            else if( StringUtils.isNotEmpty(selectedFile.getNomeFile()) && selectedFile.getNomeFile().contains(RAI_MUSICA_PART) ){
                return fillConfiguration(configuration, RAI_TEMATICI_PART, RAI_MUSICA_PART, RAI_MUSICA_PART, RAI_MUSICA_PART);
            }
        }
        else{
            logger.error("Validation parameters empty!");
        }
        return null;
    }

    private BdcConfigDTO configGeneralista(BdcConfigDTO configuration) {
        BdcConfigDTO txConfig = fillConfiguration(configuration, RAI_GENERALISTA_PART, RAI_TX_PART, RAI_TX_PART, RAI_TX_PART);
        BdcConfigDTO opConfig = fillConfiguration(configuration, RAI_GENERALISTA_PART, RAI_OP_PART, RAI_OP_PART, RAI_OP_PART);
        txConfig.setParseConfigPath( txConfig.getParseConfigPath()+"|"+opConfig.getParseConfigPath() );
        txConfig.setParseStreamName( txConfig.getParseStreamName()+"|"+opConfig.getParseStreamName() );
        return txConfig;
    }

    private List<BdcInformationFileEntity> checkGeneralista(BdcInformationFileEntity selectedFile, List<BdcInformationFileEntity> fileList) {
        List<BdcInformationFileEntity> ret = new ArrayList<>();
        String musicReportName = null;
        logger.debug("Selected file with suffix {}", RAI_TX_SUFFIX );
        musicReportName = selectedFile.getNomeFile().replace(RAI_TX_SUFFIX, RAI_OP_SUFFIX);
        if( StringUtils.isNotEmpty(musicReportName) ){
            logger.debug("Searching for file with name{}", musicReportName );
            for( BdcInformationFileEntity currentFile : fileList){
                if(musicReportName.equals(currentFile.getNomeFile())){
                    logger.debug("Found file with id {} and name {}", currentFile.getId(), currentFile.getNomeFile());
                    ret.add(currentFile);
                    ret.add(selectedFile);
                    break;
                }
            }
        }
        return ret;
    }
}