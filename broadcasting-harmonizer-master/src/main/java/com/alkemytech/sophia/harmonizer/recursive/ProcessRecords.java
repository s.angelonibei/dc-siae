package com.alkemytech.sophia.harmonizer.recursive;

import com.alkemytech.sophia.broadcasting.service.interfaces.DecodeService;
import com.alkemytech.sophia.broadcasting.utils.Utilities;
import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.mapper.TemplateMapper;
import com.alkemytech.sophia.common.s3.S3Service;
import org.drools.runtime.StatefulKnowledgeSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.RecursiveTask;

public class ProcessRecords extends RecursiveTask<List<Map<String, Object>>> {
    Logger logger = LoggerFactory.getLogger(getClass());
    final static int THRESHOLD = 2;

    protected S3Service s3Service;
    protected TemplateMapper mapper;
    protected DecodeService decodeService;

    private StatefulKnowledgeSession session;
    private List<Object> records;
    private Integer idBroadcaster;

    public ProcessRecords(List<Object> records,
                          S3Service s3Service,
                          TemplateMapper mapper,
                          DecodeService decodeService,
                          StatefulKnowledgeSession session,
                          Integer idBroadcaster) {
        this.s3Service = s3Service;
        this.mapper = mapper;
        this.decodeService = decodeService;
        this.session = session;
        this.records = records;
        this.idBroadcaster = idBroadcaster;
    }

    @Override
    protected List<Map<String, Object>> compute() {
        if (records.size() < THRESHOLD) {
            return computeDirectly();
        } else {
            int middle = records.size() / 2;
            List<Object> newRecords = records.subList(middle, records.size());
            records = records.subList(0, middle);
            ProcessRecords subTask1 = new ProcessRecords(newRecords,
                    s3Service,
                    mapper,
                    decodeService,
                    session,
                    idBroadcaster);
            subTask1.fork();

            List<Map<String, Object>> thisNormalizedRecords = this.compute();
            thisNormalizedRecords.addAll(subTask1.join());
            return thisNormalizedRecords;
        }
    }

    private List<Map<String, Object>> computeDirectly() {
        List<Map<String, Object>> normalizedRecords = new ArrayList<>();
        for (Object record : records) {
            Map<String, Object> castRecord = (Map<String, Object>) record;
            if (Utilities.isFilleMap(castRecord)) {
//                logger.debug("- NORMALIZED RECORD -> {}", castRecord.toString());
                Map<String, Object> normalizedRecord = new HashMap<>();
                List<String> errors = new ArrayList<>();
                session.setGlobal(Constants.ID_BROADCASTER, idBroadcaster);
                session.setGlobal(Constants.DECODE_SERVICE, decodeService);
                session.setGlobal(Constants.GLOBAL_RECORD, normalizedRecord);
                session.setGlobal(Constants.GLOBAL_ERROR, errors);
                session.insert(castRecord);
                session.fireAllRules();
//                logger.debug("- NORMALIZED RECORD -> {}", normalizedRecord.toString());
                normalizedRecords.add(normalizedRecord);
            }
        }
        return normalizedRecords;
    }
}
