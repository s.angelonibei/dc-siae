package com.alkemytech.sophia.harmonizer.utils;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by Alessandro Russo on 09/12/2017.
 */
public class ExceptionHandler {

    public static String logStackTrace(Exception e){
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        return sw.toString();
    }
}
