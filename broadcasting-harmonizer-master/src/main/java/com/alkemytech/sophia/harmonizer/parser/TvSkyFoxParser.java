package com.alkemytech.sophia.harmonizer.parser;

import com.alkemytech.sophia.broadcasting.service.interfaces.DecodeService;
import com.alkemytech.sophia.common.mapper.TemplateMapper;
import com.alkemytech.sophia.common.s3.S3Service;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Alessandro Russo on 07/12/2017.
 */
@Singleton
public class TvSkyFoxParser extends GenericParser {

    Logger logger = LoggerFactory.getLogger(getClass());

    public TvSkyFoxParser(Logger logger, S3Service s3Service, TemplateMapper mapper, DecodeService decodeService) {
        super(logger, s3Service, mapper, decodeService);
    }

//    public static void main(String[] args) throws FileNotFoundException {
//
//        System.out.println("default charset: " + Charset.defaultCharset());
//        String csvFile = "C:\\Users\\15P258\\Downloads\\OpereISO-8859-15-new.csv";
//
//        try {
//            InputStream is = new FileInputStream(csvFile);
//            byte[] data = IOUtils.toByteArray(is);
//            CharsetMatch match = new CharsetDetector().setText(data).detect();
//            System.out.println(match.getConfidence());
//
//            String charsetName = match.getName();
//            System.out.println("detected charset: " + charsetName);
//            byte[] encoded = new String(data, charsetName).getBytes("UTF-8");
//            System.out.println(new String(encoded));
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
}
