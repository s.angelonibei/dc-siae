package com.alkemytech.sophia.harmonizer.parser;

import com.alkemytech.sophia.broadcasting.dto.BdcConfigDTO;
import com.alkemytech.sophia.broadcasting.model.BdcCanali;
import com.alkemytech.sophia.broadcasting.model.BdcInformationFileEntity;
import com.alkemytech.sophia.broadcasting.service.interfaces.DecodeService;
import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.mapper.TemplateMapper;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.harmonizer.utils.ExceptionHandler;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.util.CollectionUtils;
import com.google.inject.Singleton;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Alessandro Russo on 07/12/2017.
 */
@Singleton
public class RadioIsoradioParser extends GenericParser {

    public RadioIsoradioParser(Logger logger, S3Service s3Service, TemplateMapper mapper, DecodeService decodeService) {
        super(logger, s3Service, mapper, decodeService);
    }

    @Override
    public Map<String, Object> parse(Map<String, Object> parameters) {
        Map<String, Object> ret = new HashMap<>();
        try {
            if (parameters.containsKey(ParserParameters.CONFIGURATION) && parameters.containsKey(ParserParameters.FILES)) {
                BdcConfigDTO configuration = (BdcConfigDTO) parameters.get(ParserParameters.CONFIGURATION);
                Map<BdcInformationFileEntity, AmazonS3URI> fileMap = (Map<BdcInformationFileEntity, AmazonS3URI>) parameters.get(ParserParameters.FILES);
                if (configuration != null && fileMap != null) {
                    if (configuration.getExpectedFiles() == null || fileMap.size() == 1) {
                        for (BdcInformationFileEntity currentFile : fileMap.keySet()) {
                            AmazonS3URI currentUri = fileMap.get(currentFile);
                            ByteArrayOutputStream bos = new ByteArrayOutputStream();
                            try {
                                //download file from Amanzon S3
                                logger.debug("report to parse -> {}", currentFile.getPercorso());
                                s3Service.download(currentUri.getBucket(), currentUri.getKey(), bos);
                                List<Object> records = mapper.parseXlsFile(configuration.getParseConfigPath(), bos, s3Service);
                                if (!CollectionUtils.isNullOrEmpty(records)) {
                                    Integer idBroadcaster = currentFile.getEmittente().getId();
                                    BdcCanali canale = decodeService.findCanaliByIdBroadcaster(idBroadcaster) != null ? decodeService.findCanaliByIdBroadcaster(idBroadcaster).get(0) : null;
                                    Integer idCanale = canale != null ? canale.getId() : null;
                                    String nomeCanale = canale != null ? canale.getNome() : null;
                                    configuration.getNormalizeRulePath();
                                    Map<String, List<Object>> mergedRecords = new HashMap<>();
                                    String candidateKey = null;
                                    Map<String, Object> currentRecord = null;
                                    for (Object current : records) {
                                        Map<String, Object> currentCast = (Map<String, Object>) current;
                                        String key = (StringUtils.isNotEmpty((String) currentCast.get("rete")) && StringUtils.isNotEmpty((String) currentCast.get("data"))) ? String.format("%s|%s", currentCast.get("rete"), currentCast.get("data")) : null;
                                        if (StringUtils.isEmpty(candidateKey) && StringUtils.isNotEmpty(key)) {
                                            candidateKey = key;
                                            currentRecord = currentCast;
                                            continue;
                                        }
                                        if (StringUtils.isEmpty(key)) {
                                            currentCast.put(Constants.CANALE, nomeCanale);
                                            currentCast.put(Constants.DECODE_ID_CANALE, idCanale);
                                            currentCast.put("data", currentRecord.get("data"));
                                            if (!mergedRecords.containsKey(candidateKey)) {
                                                mergedRecords.put(candidateKey, new ArrayList<Object>());
                                            }
                                            mergedRecords.get(candidateKey).add(currentCast);
                                            logger.debug("utilizzazione musicale --> " + currentCast);
                                        } else {
//                                             logger.debug("chiave aggiornata --> "+key);
                                            candidateKey = key;
                                            currentRecord = currentCast;
                                        }
                                    }

                                    List<Object> totalRecords = new ArrayList<>();
                                    for (String key : mergedRecords.keySet()) {
                                        totalRecords.addAll(mergedRecords.get(key));
                                    }
                                    ret.put(PARSED_RECORDS, normalize(idBroadcaster, configuration.getNormalizeRulePath(), totalRecords));
                                    return ret;
                                } else {
                                    ret.put(ERRORS, "Parsing result is empty!");
                                    currentFile.setDecodificaErrore("Parsing result is empty!");
                                }
                            }catch (Exception e){
                                currentFile.setStackTrace(getWrappedErrorMessage(e.getMessage()));
                                currentFile.setDecodificaErrore("Parsing Error");
                                ret.put(ERRORS, ExceptionHandler.logStackTrace(e));
                            }  finally {
                                bos.flush();
                                bos.close();
                            }
                        }
                    } else {
                        ret.put(ERRORS, String.format("Unexpected files number: Expected %s -> Found %s", configuration.getExpectedFiles(), fileMap.size()));
                    }
                } else {
                    ret.put(ERRORS, "Parser parameters empty!");
                }
            } else {
                ret.put(ERRORS, "Expected parser parameters not found!");
            }
        } catch (Exception e) {
            ret.put(ERRORS, ExceptionHandler.logStackTrace(e));
        }
        return ret;
    }
}
