package com.alkemytech.sophia.harmonizer.parser;

import com.alkemytech.sophia.broadcasting.dto.BdcConfigDTO;
import com.alkemytech.sophia.broadcasting.model.BdcInformationFileEntity;
import com.alkemytech.sophia.broadcasting.service.interfaces.DecodeService;
import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.mapper.TemplateMapper;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.harmonizer.utils.ExceptionHandler;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.util.CollectionUtils;
import com.google.inject.Singleton;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Alessandro Russo on 07/12/2017.
 */
@Singleton
public class TvLa7Parser extends GenericParser {
    public TvLa7Parser(Logger logger, S3Service s3Service, TemplateMapper mapper, DecodeService decodeService) {
        super(logger, s3Service, mapper, decodeService);
    }

    @Override
    public Map<String, Object> parse(Map<String, Object> parameters) {
        Map<String, Object> ret = new HashMap<>();
        try{
            if(parameters.containsKey(ParserParameters.CONFIGURATION) && parameters.containsKey(ParserParameters.FILES)){
                BdcConfigDTO configuration = (BdcConfigDTO) parameters.get(ParserParameters.CONFIGURATION);
                Map<BdcInformationFileEntity,AmazonS3URI> fileMap = (Map<BdcInformationFileEntity, AmazonS3URI>) parameters.get(ParserParameters.FILES);
                if( configuration!= null && fileMap != null ){
                    if( configuration.getExpectedFiles() == null || fileMap.size() == 1){
                        for(BdcInformationFileEntity currentFile : fileMap.keySet()){
                            AmazonS3URI currentUri = fileMap.get(currentFile);
                            ByteArrayOutputStream bos = new ByteArrayOutputStream();
                            try{
                                //download file from Amanzon S3
                                logger.debug("report to parse -> {}", currentFile.getPercorso() );
                                s3Service.download(currentUri.getBucket(), currentUri.getKey(), bos);
                                String parseConfigPath = configuration.getParseConfigPath();
                                List<Object> records = mapper.parseXlsFile(parseConfigPath, bos, s3Service);

                                if( !CollectionUtils.isNullOrEmpty(records) ){
                                        Integer idBroadcaster = currentFile.getEmittente().getId();
                                        Integer idCanale = currentFile.getCanale() != null ? currentFile.getCanale().getId():null;
                                        String nomeCanale = currentFile.getCanale() != null ? currentFile.getCanale().getNome():null;
                                        String normalizedRulePath = configuration.getNormalizeRulePath();

                                        Map<String, List<Object> > mergedRecords = new HashMap<>();
                                        String candidateKey = null;
                                        Map<String, Object> currentRecord = null;
                                        boolean hasParent = false;
                                        for(Object current : records){
                                            Map<String, Object> currentCast = (Map<String, Object>) current;
                                            String key = String.format("%s|%s", currentCast.get("data"), currentCast.get("oraInizio"));
                                            if(StringUtils.isEmpty(candidateKey)){
                                                candidateKey = key;
                                                currentRecord = currentCast;
//                                                logger.debug("chiave iniziale --> "+candidateKey);
                                                continue;
                                            }
                                            currentRecord.put(Constants.CANALE, nomeCanale);
                                            currentRecord.put(Constants.DECODE_ID_CANALE, idCanale);
                                            if(key.equals(candidateKey)){
//                                                logger.debug("chiave esistente --> "+key);
                                                hasParent = true;
                                                Map<String, Object> music = new HashMap<>();
                                                music.putAll(currentRecord);

                                                music.put("titoloOpera", currentCast.get("titoloOriginaleEpisodio"));
                                                music.put("autore1", currentCast.get("autore1"));
                                                music.put("autore2", currentCast.get("autore2"));
                                                music.put("autore3", currentCast.get("autore3"));
                                                music.put("categoriaUso", currentCast.get("categoriaUso"));
                                                music.put("tipoGenerazione", currentCast.get("tipoGenerazione"));
                                                music.put("interprete", currentCast.get("interprete"));
                                                music.put("album", currentCast.get("album"));
                                                music.put("etichetta", currentCast.get("etichetta"));
                                                music.put("durataOpera", currentCast.get("durataNetta"));
                                                if( !mergedRecords.containsKey(candidateKey) ){
                                                    mergedRecords.put(candidateKey, new ArrayList<Object>());
                                                }
                                                mergedRecords.get(candidateKey).add(music);
                                                logger.debug("utilizzazione musicale --> "+ music);
                                            }
                                            else{
//                                                logger.debug("chiave aggiornata --> "+key);
                                                if(!hasParent){
                                                    if( !mergedRecords.containsKey(candidateKey) ){
                                                        mergedRecords.put(candidateKey, new ArrayList<Object>());
                                                    }
                                                    logger.debug("utilizzazione tv --> "+ currentRecord);
                                                    mergedRecords.get(candidateKey).add(currentRecord);
                                                }
                                                candidateKey = key;
                                                currentRecord = currentCast;
                                                hasParent = false;
                                            }
                                        }
                                        if(!hasParent){
                                            if( !mergedRecords.containsKey(candidateKey) ){
                                                mergedRecords.put(candidateKey, new ArrayList<Object>());
                                            }
                                            logger.debug("utilizzazione tv --> "+ currentRecord);
                                            mergedRecords.get(candidateKey).add(currentRecord);
                                        }
                                        List<Object> totalRecords = new ArrayList<>();
                                        for(String key : mergedRecords.keySet()){
                                            totalRecords.addAll(mergedRecords.get(key));
                                        }
                                        ret.put(PARSED_RECORDS, normalize(idBroadcaster, normalizedRulePath, totalRecords));
                                        return ret;
                                }
                                else{
                                    ret.put(ERRORS, "Parsing result is empty!");
                                }
                            }catch (Exception e){
                                currentFile.setStackTrace(getWrappedErrorMessage(e.getMessage()));
                                currentFile.setDecodificaErrore("Parsing error");
                                ret.put(ERRORS, ExceptionHandler.logStackTrace(e));
                            }
                            finally {
                                bos.flush();
                                bos.close();
                            }
                        }
                    }
                    else{
                        ret.put(ERRORS, String.format("Unexpected files number: Expected %s -> Found %s", configuration.getExpectedFiles(), fileMap.size() ) );
                    }
                }
                else{
                    ret.put(ERRORS, "Parser parameters empty!");
                }
            }
            else{
                ret.put(ERRORS, "Expected parser parameters not found!");
            }
        }
        catch (Exception e){
            ret.put(ERRORS, ExceptionHandler.logStackTrace(e));
        }
        return ret;
    }
}
