package com.alkemytech.sophia.harmonizer.parser;

import com.alkemytech.sophia.broadcasting.dto.BdcConfigDTO;
import com.alkemytech.sophia.broadcasting.model.BdcInformationFileEntity;
import com.alkemytech.sophia.broadcasting.service.interfaces.DecodeService;
import com.alkemytech.sophia.broadcasting.utils.Utilities;
import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.mapper.TemplateMapper;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.harmonizer.service.RulesEngineService;
import com.alkemytech.sophia.harmonizer.utils.ExceptionHandler;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.util.IOUtils;
import com.amazonaws.util.StringUtils;
import org.drools.runtime.StatefulKnowledgeSession;
import org.mozilla.universalchardet.UniversalDetector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Alessandro Russo on 07/12/2017.
 */
public abstract class GenericParser {
    Logger logger = LoggerFactory.getLogger(getClass());

    public static final String PARSED_RECORDS = "records";
    public static final String ERRORS = "errors";


    protected S3Service s3Service;
    protected TemplateMapper mapper;
    protected DecodeService decodeService;

    public GenericParser(Logger logger, S3Service s3Service, TemplateMapper mapper, DecodeService decodeService) {
        this.logger = logger;
        this.s3Service = s3Service;
        this.mapper = mapper;
        this.decodeService = decodeService;
    }

    public Map<String, Object> parse(Map<String, Object> parameters) {
        //Result map (it may contain RECORDS if OK or ERRORS if KO)
        Map<String, Object> ret = new HashMap<>();
        try {
            if (parameters.containsKey(ParserParameters.CONFIGURATION) && parameters.containsKey(ParserParameters.FILES)) {
                BdcConfigDTO configuration = (BdcConfigDTO) parameters.get(ParserParameters.CONFIGURATION);
                Map<BdcInformationFileEntity, AmazonS3URI> fileMap = (Map<BdcInformationFileEntity, AmazonS3URI>) parameters.get(ParserParameters.FILES);
                if (configuration != null && fileMap != null) {
                    if (configuration.getExpectedFiles() == null || fileMap.size() == 1) {
                        for (BdcInformationFileEntity currentFile : fileMap.keySet()) {
                            AmazonS3URI currentUri = fileMap.get(currentFile);
                            ByteArrayOutputStream bos = new ByteArrayOutputStream();
                            try {
                                Charset charset = prepareFile(currentFile, currentUri, bos);

                                List<Object> records = mapper.parseFile(configuration.getParseConfigPath(), configuration.getParseStreamName(), bos, s3Service, charset);
                                if (parsedRecordsNotEmpty(ret, configuration, currentFile, records))
                                    return ret;
                            }catch (Exception e){
                                currentFile.setStackTrace(getWrappedErrorMessage(e.getMessage()));
                                currentFile.setDecodificaErrore("Parsing error");
                                ret.put(ERRORS, ExceptionHandler.logStackTrace(e));
                            }
                            finally {
                                bos.flush();
                                bos.close();
                            }
                        }
                    } else {
                        ret.put(ERRORS, String.format("Unexpected files number: Expected %s -> Found %s", configuration.getExpectedFiles(), fileMap.size()));
                    }
                } else {
                    ret.put(ERRORS, "Parser parameters empty!");
                }
            } else {
                ret.put(ERRORS, "Expected parser parameters not found!");
            }
        } catch (Exception e) {
            ret.put(ERRORS, ExceptionHandler.logStackTrace(e));
        }
        return ret;
    }

    public Charset prepareFile(BdcInformationFileEntity currentFile, AmazonS3URI currentUri, ByteArrayOutputStream bos) throws IOException {
        byte[] buf = new byte[4096];
        logger.info("default charset: {}", Charset.defaultCharset());
        //download file from Amanzon S3
        logger.info("report to parse -> {}", currentFile.getPercorso());
        InputStream is = s3Service.download(currentUri.getBucket(), currentUri.getKey());
        UniversalDetector detector = new UniversalDetector(null);

        // (2)
        int nread;
        while ((nread = is.read(buf)) > 0 && !detector.isDone()) {
            detector.handleData(buf, 0, nread);
            //ai pesci
//            bos.write(buf, 0, nread);
        }
        // (3)
        detector.dataEnd();

        // (4)
        String encoding = detector.getDetectedCharset();
        Charset charset;
        if (encoding != null) {
            logger.info("detected charset: {}", encoding);
            charset = Charset.forName(encoding);
        } else {
            logger.info("No encoding detected.");
            charset = Charset.defaultCharset();
        }

        // (5)
        detector.reset();

        //ai pesci
//        IOUtils.copy(is,bos);

        return charset;
    }

    protected boolean parsedRecordsNotEmpty(Map<String, Object> ret, BdcConfigDTO configuration, BdcInformationFileEntity currentFile, List<Object> records) throws Exception {
        if (records != null && records.size() > 0) {
            Integer idBroadcaster = currentFile.getEmittente().getId();
            ret.put(PARSED_RECORDS, normalize(idBroadcaster, configuration.getNormalizeRulePath(), records));
            return true;
        } else {
            currentFile.setStackTrace(getWrappedErrorMessage("Invalid 'header' record at line 1"));
            currentFile.setDecodificaErrore("Parsing Error");
            ret.put(ERRORS, "Parsing result is empty!");
        }
        return false;
    }

    protected List<Map<String, Object>> normalize(Integer idBroadcaster, String normalizedRule, List<Object> records) throws Exception {
        List<Map<String, Object>> normalizedRecords = new ArrayList<>();
        logger.debug("rule file is -> {}", normalizedRule);
        StatefulKnowledgeSession session = new RulesEngineService().createKnowledgeBaseFromSpreadsheet(normalizedRule, s3Service).newStatefulKnowledgeSession();
        for (Object record : records) {
            Map<String, Object> castRecord = (Map<String, Object>) record;
            if (Utilities.isFilleMap(castRecord)) {
//                logger.debug("- NORMALIZED RECORD -> {}", castRecord.toString());
                Map<String, Object> normalizedRecord = new HashMap<>();
                List<String> errors = new ArrayList<>();
                session.setGlobal(Constants.ID_BROADCASTER, idBroadcaster);
                session.setGlobal(Constants.DECODE_SERVICE, decodeService);
                session.setGlobal(Constants.GLOBAL_RECORD, normalizedRecord);
                session.setGlobal(Constants.GLOBAL_ERROR, errors);
                session.insert(castRecord);
                session.fireAllRules();
//                logger.debug("- NORMALIZED RECORD -> {}", normalizedRecord.toString());
                normalizedRecords.add(normalizedRecord);
            }
        }
        return normalizedRecords;
    }

    public void setMapper(TemplateMapper mapper) {
        this.mapper = mapper;
    }

    public void setS3Service(S3Service s3Service) {
        this.s3Service = s3Service;
    }

    public void setDecodeService(DecodeService decodeService) {
        this.decodeService = decodeService;
    }

    protected String getConfigProperty(String configProperty, String type) {
        String[] configPropertyArray = getConfigList(configProperty);
        if (configPropertyArray != null) {
            for (String currentValue : configPropertyArray) {
                if (currentValue.contains(type)) {
                    return currentValue;
                }
            }
        }
        return null;
    }

    private String[] getConfigList(String configProperty) {
        if (!StringUtils.isNullOrEmpty(configProperty)) {
            return configProperty.split("\\|");
        }
        return null;
    }

    public String getWrappedErrorMessage(String e){
        String result = e;
        if(e.contains("Invalid 'header' record at line 1")){
            result="Il file non rispetta il formato atteso ("+e+")";
        }
        if(e.contains("Invalid 'record' record at line ")){
            String recordErrato = e.substring(32,e.length());
            result="Il file non rispetta il formato atteso Record " + recordErrato +" errato";
        }
        if(e.contains("Malformed record at line ")){
            result="Presenza di caratteri speciali (" + e +")";
        }
        if(e.contains("Your InputStream was")){
            result="Il file non rispetta il formato atteso ("+e+")";
        }
        if(e.contains("Index:")){
            result="Il file non rispetta il formato atteso ("+e+")";
        }

        return result;
    }

}
