package com.alkemytech.sophia.harmonizer.utils;

/**
 * Created by Alessandro Russo on 07/03/2018.
 */
public class HarmonizerConstants {

    public final static String RAI_TX_SUFFIX            = "_TX_SIAE";
    public final static String RAI_OP_SUFFIX            = "_OP_SIAE";
    public final static String RAI_TX_PART              ="_TX";
    public final static String RAI_OP_PART              ="_OP";
    public final static String RAI_GENERALISTA_PART     = "Generalista";
    public final static String RAI_TEMATICI_PART        = "Tematici";
    public final static String RAI_CINEMA_PART          ="Cinema";
    public final static String RAI_MUSICA_PART          ="Musica";

    public final static String RTI_GEN_PART     ="Gen";
    public final static String RTI_PROMO_PART   ="Promo";

}
