package com.alkemytech.sophia.harmonizer.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Alessandro Russo on 04/12/2017.
 */

public class RaiShowDTO extends BaseRaiDTO implements Serializable{

    private String uorg;
    private String matricola;
    private String serie;
    private String puntata;
    private String replica;
    private String oraInizio;
    private String oraFine;
    private String durata;
    private String provenienza;
    private String tipo;
    private String titolo;
    private String genere;
    private String diffusione;
    private String generazione;
    private String colore;
    private String unitaProduzione;
    private String rapportoArtistico;
    private String numeroOpere;
    private String durataTotale;
    private String durataOpere;
    private String durataSottofondo;
    private String durataInserto;
    private String oraFunaleContenitore;
    private String spazio1;
    private String note;
    private String sottotitolo;
    private String funzionario;
    private String dataElaborazione;
    private String dataCompetenza;
    private String dataAggiornamento;
    private String spazio2;
    private String titoloAlternativo1;
    private String titoloAlternativo2;
    private String titoloAlternativo3;

    public String getUorg() {
        return uorg;
    }

    public void setUorg(String uorg) {
        this.uorg = uorg;
    }

    public String getMatricola() {
        return matricola;
    }

    public void setMatricola(String matricola) {
        this.matricola = matricola;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getPuntata() {
        return puntata;
    }

    public void setPuntata(String puntata) {
        this.puntata = puntata;
    }

    public String getReplica() {
        return replica;
    }

    public void setReplica(String replica) {
        this.replica = replica;
    }

    public String getOraInizio() {
        return oraInizio;
    }

    public void setOraInizio(String oraInizio) {
        this.oraInizio = oraInizio;
    }

    public String getOraFine() {
        return oraFine;
    }

    public void setOraFine(String oraFine) {
        this.oraFine = oraFine;
    }

    public String getDurata() {
        return durata;
    }

    public void setDurata(String durata) {
        this.durata = durata;
    }

    public String getProvenienza() {
        return provenienza;
    }

    public void setProvenienza(String provenienza) {
        this.provenienza = provenienza;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTitolo() {
        return titolo;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public String getGenere() {
        return genere;
    }

    public void setGenere(String genere) {
        this.genere = genere;
    }

    public String getDiffusione() {
        return diffusione;
    }

    public void setDiffusione(String diffusione) {
        this.diffusione = diffusione;
    }

    public String getGenerazione() {
        return generazione;
    }

    public void setGenerazione(String generazione) {
        this.generazione = generazione;
    }

    public String getColore() {
        return colore;
    }

    public void setColore(String colore) {
        this.colore = colore;
    }

    public String getUnitaProduzione() {
        return unitaProduzione;
    }

    public void setUnitaProduzione(String unitaProduzione) {
        this.unitaProduzione = unitaProduzione;
    }

    public String getRapportoArtistico() {
        return rapportoArtistico;
    }

    public void setRapportoArtistico(String rapportoArtistico) {
        this.rapportoArtistico = rapportoArtistico;
    }

    public String getNumeroOpere() {
        return numeroOpere;
    }

    public void setNumeroOpere(String numeroOpere) {
        this.numeroOpere = numeroOpere;
    }

    public String getDurataTotale() {
        return durataTotale;
    }

    public void setDurataTotale(String durataTotale) {
        this.durataTotale = durataTotale;
    }

    public String getDurataOpere() {
        return durataOpere;
    }

    public void setDurataOpere(String durataOpere) {
        this.durataOpere = durataOpere;
    }

    public String getDurataSottofondo() {
        return durataSottofondo;
    }

    public void setDurataSottofondo(String durataSottofondo) {
        this.durataSottofondo = durataSottofondo;
    }

    public String getDurataInserto() {
        return durataInserto;
    }

    public void setDurataInserto(String durataInserto) {
        this.durataInserto = durataInserto;
    }

    public String getOraFunaleContenitore() {
        return oraFunaleContenitore;
    }

    public void setOraFunaleContenitore(String oraFunaleContenitore) {
        this.oraFunaleContenitore = oraFunaleContenitore;
    }

    public String getSpazio1() {
        return spazio1;
    }

    public void setSpazio1(String spazio1) {
        this.spazio1 = spazio1;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getSottotitolo() {
        return sottotitolo;
    }

    public void setSottotitolo(String sottotitolo) {
        this.sottotitolo = sottotitolo;
    }

    public String getFunzionario() {
        return funzionario;
    }

    public void setFunzionario(String funzionario) {
        this.funzionario = funzionario;
    }

    public String getDataElaborazione() {
        return dataElaborazione;
    }

    public void setDataElaborazione(String dataElaborazione) {
        this.dataElaborazione = dataElaborazione;
    }

    public String getDataCompetenza() {
        return dataCompetenza;
    }

    public void setDataCompetenza(String dataCompetenza) {
        this.dataCompetenza = dataCompetenza;
    }

    public String getDataAggiornamento() {
        return dataAggiornamento;
    }

    public void setDataAggiornamento(String dataAggiornamento) {
        this.dataAggiornamento = dataAggiornamento;
    }

    public String getSpazio2() {
        return spazio2;
    }

    public void setSpazio2(String spazio2) {
        this.spazio2 = spazio2;
    }

    public String getTitoloAlternativo1() {
        return titoloAlternativo1;
    }

    public void setTitoloAlternativo1(String titoloAlternativo1) {
        this.titoloAlternativo1 = titoloAlternativo1;
    }

    public String getTitoloAlternativo2() {
        return titoloAlternativo2;
    }

    public void setTitoloAlternativo2(String titoloAlternativo2) {
        this.titoloAlternativo2 = titoloAlternativo2;
    }

    public String getTitoloAlternativo3() {
        return titoloAlternativo3;
    }

    public void setTitoloAlternativo3(String titoloAlternativo3) {
        this.titoloAlternativo3 = titoloAlternativo3;
    }
}
