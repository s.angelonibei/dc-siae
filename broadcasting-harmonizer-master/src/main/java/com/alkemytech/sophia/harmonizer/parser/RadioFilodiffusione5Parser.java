package com.alkemytech.sophia.harmonizer.parser;

import com.alkemytech.sophia.broadcasting.dto.BdcConfigDTO;
import com.alkemytech.sophia.broadcasting.model.BdcInformationFileEntity;
import com.alkemytech.sophia.broadcasting.service.interfaces.DecodeService;
import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.mapper.TemplateMapper;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.common.tools.DateTools;
import com.alkemytech.sophia.common.tools.StringTools;
import com.alkemytech.sophia.harmonizer.utils.ExceptionHandler;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.util.CollectionUtils;
import com.google.inject.Singleton;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Alessandro Russo on 07/12/2017.
 */
@Singleton
public class RadioFilodiffusione5Parser extends GenericParser {
    Logger logger = LoggerFactory.getLogger(getClass());

    public RadioFilodiffusione5Parser(Logger logger, S3Service s3Service, TemplateMapper mapper, DecodeService decodeService) {
        super(logger, s3Service, mapper, decodeService);
    }

    @Override
    public Map<String, Object> parse(Map<String, Object> parameters) {
        //Result map (it may contain RECORDS if OK or ERRORS if KO)
        Map<String, Object> ret = new HashMap<>();
        try {
            if (parameters.containsKey(ParserParameters.CONFIGURATION) && parameters.containsKey(ParserParameters.FILES)) {
                BdcConfigDTO configuration = (BdcConfigDTO) parameters.get(ParserParameters.CONFIGURATION);
                Map<BdcInformationFileEntity, AmazonS3URI> fileMap = (Map<BdcInformationFileEntity, AmazonS3URI>) parameters.get(ParserParameters.FILES);
                if (configuration != null && fileMap != null) {
                    if (configuration.getExpectedFiles() == null || fileMap.size() == 1) {
                        for (BdcInformationFileEntity currentFile : fileMap.keySet()) {
                            AmazonS3URI currentUri = fileMap.get(currentFile);
                            ByteArrayOutputStream bos = new ByteArrayOutputStream();
                            try {
                                //download file from Amanzon S3
                                logger.debug("report to parse -> {}", currentFile.getPercorso());
                                s3Service.download(currentUri.getBucket(), currentUri.getKey(), bos);
                                List<Object> records = mapper.parseXlsFile(configuration.getParseConfigPath(), bos, s3Service);
                                if (!CollectionUtils.isNullOrEmpty(records)) {
                                    Integer idBroadcaster = currentFile.getEmittente().getId();
                                    Map<String, List<Object>> mergedRecords = new HashMap<>();
                                    String penultimate = null;
                                    Integer durata = 0;
                                    Integer durataCast = 0;
                                    Map<String, Object> currentRecord = null;
                                    for (Object current : records) {
                                        Map<String, Object> currentCast = (Map<String, Object>) current;
                                        String key;
                                        SimpleDateFormat stf = new SimpleDateFormat(DateTools.TIME_SIMPLE_FORMAT);
                                        Date hourFrom = null;
                                        try {
                                            hourFrom = stf.parse(((String) currentCast.get("from")));
                                        } catch (ParseException e) {
                                        }
                                        key = (hourFrom != null) ? String.format("%s", currentCast.get("from")) : null;
                                        if (StringUtils.isEmpty(key) || !key.equals(penultimate)) {
                                            currentRecord = currentCast;
                                            penultimate = key;
                                            if (currentRecord.get("from") == null) {
                                                durata = 0;
                                                currentCast.put(Constants.ORARIO_INIZIO_OP, StringTools.parseSecondsToTime(durata.toString()));
                                            } else if (StringTools.parseTimeToSeconds((String) currentRecord.get("from")) == null) {
                                                currentCast.put(Constants.ORARIO_INIZIO_OP, currentRecord.get("from").toString());
                                            } else {
                                                durata = Integer.parseInt(StringTools.parseTimeToSeconds(DateTools.dateToString(DateTools.stringToDate((String) currentRecord.get("from"), "HH:mm"), DateTools.TIME_COMPLETE_FORMAT)));
                                                currentCast.put(Constants.ORARIO_INIZIO_OP, StringTools.parseSecondsToTime(durata.toString()));
                                            }
                                        } else {
                                            if (StringTools.isNullOrEmpty((String) currentRecord.get("duration")) || StringTools.parseTimeToSeconds(((String) currentRecord.get("duration")).replaceAll(",|\\.", ":")) == null) {
                                                durata += durataCast;
                                                currentRecord = currentCast;
                                                currentCast.put(Constants.ORARIO_INIZIO_OP, currentRecord.get("duration").toString());
                                            } else {
                                                String timeParsed = StringTools.parseTimeToSeconds(((String) currentRecord.get("duration")).replaceAll(",|\\.", ":"));
                                                if (!StringUtils.isEmpty(timeParsed)) {
                                                    durataCast = Integer.valueOf(timeParsed);
                                                }
                                                durata += durataCast;
                                                currentRecord = currentCast;
                                                currentCast.put(Constants.ORARIO_INIZIO_OP, StringTools.parseSecondsToTime(durata.toString()));
                                            }
                                        }
                                    }
                                    List<Object> totalRecords = new ArrayList<>();
                                    for (String key : mergedRecords.keySet()) {
                                        totalRecords.addAll(mergedRecords.get(key));
                                    }
                                    ret.put(PARSED_RECORDS, normalize(idBroadcaster, configuration.getNormalizeRulePath(), records));
                                    return ret;
                                } else {
                                    ret.put(ERRORS, "Parsing result is empty!");
                                }
                            }catch (Exception e){
                                currentFile.setStackTrace(getWrappedErrorMessage(e.getMessage()));
                                currentFile.setDecodificaErrore("Parsing error");
                                ret.put(ERRORS, ExceptionHandler.logStackTrace(e));
                            }
                            finally {
                                bos.flush();
                                bos.close();
                            }
                        }
                    } else {
                        ret.put(ERRORS, String.format("Unexpected files number: Expected %s -> Found %s", configuration.getExpectedFiles(), fileMap.size()));
                    }
                } else {
                    ret.put(ERRORS, "Parser parameters empty!");
                }
            } else {
                ret.put(ERRORS, "Expected parser parameters not found!");
            }
        } catch (Exception e) {
            ret.put(ERRORS, ExceptionHandler.logStackTrace(e));
        }
        return ret;
    }
}
