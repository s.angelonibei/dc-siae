package com.alkemytech.sophia.commons.query;

import com.alkemytech.sophia.commons.index.LevenshteinIndex;
import com.alkemytech.sophia.commons.util.LongArray;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class AbstractQueryChain implements QueryChain {

	protected abstract LongArray searchTerm(LevenshteinIndex index, String text);

	protected LongArray exactSearch(LevenshteinIndex index, String text) {
		final LevenshteinIndex.TermSet termSet = index.search(text);
		if (termSet.next()) {
			return termSet.getPostings();
		}
		return null;
	}
	
	protected LongArray fuzzySearch(LevenshteinIndex index, String text, int[] fuzzyCosts) {
		if (text.length() >= fuzzyCosts[0]) {
			for (int maxCost = fuzzyCosts.length; maxCost > 0; maxCost --) {
				if (text.length() >= fuzzyCosts[maxCost-1]) {
					final LevenshteinIndex.TermSet termSet = index.search(text, maxCost);
					if (termSet.next()) {
						LongArray postings = termSet.getPostings();
						while (termSet.next()) {
							postings = LongArray.union(postings, termSet.getPostings());
						}
						return postings;
					}
					return null;
				}
			}
		}
		return exactSearch(index, text);
	}
	
}
