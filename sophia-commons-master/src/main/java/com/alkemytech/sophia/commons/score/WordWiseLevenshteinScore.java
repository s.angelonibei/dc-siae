package com.alkemytech.sophia.commons.score;

import com.alkemytech.sophia.commons.distance.LevenshteinEditDistance;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class WordWiseLevenshteinScore extends WordWiseScore {

	public WordWiseLevenshteinScore() {
		super(new LevenshteinEditDistance());
	}
	
}
