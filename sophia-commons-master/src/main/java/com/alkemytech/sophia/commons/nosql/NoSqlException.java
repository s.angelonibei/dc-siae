package com.alkemytech.sophia.commons.nosql;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@SuppressWarnings("serial")
public class NoSqlException extends RuntimeException {

	public NoSqlException(String message, Throwable cause) {
		super(message, cause);
	}

	public NoSqlException(String message) {
		super(message);
	}

	public NoSqlException(Throwable cause) {
		super(cause);
	}

}
