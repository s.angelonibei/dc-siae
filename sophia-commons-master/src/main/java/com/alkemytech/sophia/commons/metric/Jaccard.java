package com.alkemytech.sophia.commons.metric;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * The similarity between the two strings is the size of the intersection divided by the size
 * of the union of their vector representations. Each input string is converted into a set of
 * n-grams, the Jaccard index is then computed as |V1 intersection V2| / |V1 union V2|.
 * 
 * @see https://blog.nishtahir.com/2015/09/19/fuzzy-string-matching-using-cosine-similarity/
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class Jaccard extends NGrams {
	
	public static double jaccard(String a, String b, int n) {
		if (a.equals(b)) {
            return 1.0;
        } else if (a.length() < n || b.length() < n) {
            return 0.0;
        }
		Map<String, Integer> aa = ngrams(a, n);
        Map<String, Integer> bb = ngrams(b, n);
        Set<String> union = new HashSet<>();
        union.addAll(aa.keySet());
        union.addAll(bb.keySet());
        return (double) (aa.size() + bb.size() -
        		union.size()) / (double) union.size();
	}

}
