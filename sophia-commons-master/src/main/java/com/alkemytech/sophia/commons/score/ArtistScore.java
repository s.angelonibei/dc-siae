package com.alkemytech.sophia.commons.score;

import com.alkemytech.sophia.commons.util.TokenizableText;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ArtistScore implements TextScore {

	private final TextScore plainTextScore = new LevenshteinScore();
	private final TextScore wordWiseScore = new WordWiseLevenshteinScore();
	
	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.commons.score.TextScore#getScore(com.alkemytech.sophia.commons.util.TokenizableText, com.alkemytech.sophia.commons.util.TokenizableText)
	 */
	@Override
	public double getScore(TokenizableText pattern, TokenizableText target) {
		return Math.max(plainTextScore.getScore(pattern, target),
						wordWiseScore.getScore(pattern, target));
	}

}
