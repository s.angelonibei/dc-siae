package com.alkemytech.sophia.commons.text;

import java.util.Properties;

import com.google.inject.Provider;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class TextTokenizerProvider implements Provider<TextTokenizer> {
	
	private final Provider<TextTokenizer> provider;
	
	public TextTokenizerProvider(final Properties configuration, final String propertyPrefix) {
		super();
		final String className = configuration
				.getProperty(propertyPrefix + ".class_name");
		if (RegExTextTokenizer.class.getName().equals(className)) {
			provider = new Provider<TextTokenizer>() {
				@Override
				public TextTokenizer get() {
					return new RegExTextTokenizer(configuration, propertyPrefix);
				}
			}; 
		} else {
			throw new IllegalArgumentException(String
					.format("unknown class name \"%s\"", className));
		}
	}
	
	@Override
	public TextTokenizer get() {
		return provider.get();
	}
	
}
