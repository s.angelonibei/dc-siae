package com.alkemytech.sophia.commons.query;

import com.alkemytech.sophia.commons.index.LevenshteinIndex;
import com.alkemytech.sophia.commons.util.LongArray;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ExactAndQueryChain extends AbstractAndQueryChain {

	@Override
	protected LongArray searchTerm(LevenshteinIndex index, String text) {
		return exactSearch(index, text);
	}

}
