package com.alkemytech.sophia.commons.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class HeartBeat {
	
	public static HeartBeat constant(String label, long step) {
		final HeartBeat heartBeat = new HeartBeat(label);
		heartBeat.step = step;
		return heartBeat;
	}

	public static HeartBeat percentage(String label, long maximum, long times) {
		final HeartBeat heartBeat = new HeartBeat(label);
		heartBeat.maximum = maximum;
		heartBeat.step = -1 + maximum / times;
		return heartBeat;
	}
	
	private final Logger logger = LoggerFactory.getLogger(HeartBeat.class);

	private final String label;
	
	private long step;
	private long maximum;
	private long count;
	private long progress;
	
	private HeartBeat(String label) {
		super();
		this.label = label;
	}
	
	public void pump() {
		count ++;
		if (maximum > 0) {
			if (progress ++ >= step) {
				logger.debug("{} {}%", label,
						Math.round(100.0 * count / (double) maximum));
				progress = 0;
			}
		} else if (0 == (count % step)) {
			if (step >= 1000) {
				logger.debug("{} {}k", label, count / 1000);
			} else {
				logger.debug("{} {}", label, count);
			}
		}
	}
	
	public long getTotalPumps() {
		return count;
	}
	
//	public static void main(String[] args) {
//		HeartBeat heartBeat = HeartBeat.percentage(12345, 20);
//		for (int i = 0; i < 12345; i ++) {
//			heartBeat.pump();
//		}
//		heartBeat = HeartBeat.constant(1000);
//		for (int i = 0; i < 12345; i ++) {
//			heartBeat.pump();
//		}
//	}
	
}
