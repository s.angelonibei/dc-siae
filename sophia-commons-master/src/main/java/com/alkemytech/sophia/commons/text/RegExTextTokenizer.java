package com.alkemytech.sophia.commons.text;

import java.util.Collection;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

import com.alkemytech.sophia.commons.regex.RegExTransformer;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.google.common.base.Strings;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class RegExTextTokenizer implements TextTokenizer {
		
	private final LetterCase letterCase;
	private final RegExTransformer transformer;
	private final Pattern splitPattern;
	private final Set<String> tokens;
	
	public RegExTextTokenizer(Properties configuration, String propertyPrefix) {
		super();
		this.letterCase = LetterCase.parse(configuration
				.getProperty(propertyPrefix + ".letter_case"));
		this.transformer = new RegExTransformer(TextUtils.split(configuration
				.getProperty(propertyPrefix + ".transformer"), ","));
		this.splitPattern = Pattern.compile(configuration
				.getProperty(propertyPrefix + ".split_regex"));
		this.tokens = new HashSet<>();
	}
	
	@Override
	public RegExTextTokenizer clear() {
		tokens.clear();
		return this;
	}

	@Override
	public RegExTextTokenizer tokenize(String text) {
		if (!Strings.isNullOrEmpty(text)) {
			text = letterCase.enforce(text);
			text = transformer.transform(text);
			if (!Strings.isNullOrEmpty(text)) {
				for (String token : splitPattern.split(text, -1)) {
					token = token.trim();
					if (!Strings.isNullOrEmpty(token)) {
						tokens.add(token);
					}
				}
			}
		}
		return this;
	}

	@Override
	public Collection<String> getTokens() {
		return tokens;
	}
	
}
