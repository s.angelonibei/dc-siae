package com.alkemytech.sophia.commons.collecting;

import java.util.regex.Pattern;

import com.alkemytech.sophia.commons.text.TextNormalizer;
import com.google.common.base.Strings;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class SiaeCode {

	private static final Pattern IS_VALID_REGEX = Pattern
			.compile("^\\d{11}$");
	
	public static final TextNormalizer NORMALIZER = new TextNormalizer() {
		
		@Override
		public String normalize(String text) {
			return SiaeCode.normalize(text);
		}
		
	};
	
	public static String normalize(String code) {
		if (Strings.isNullOrEmpty(code))
			return null;
		final int length = code.length();
		if (length > 11)
			throw new IllegalArgumentException(String
					.format("code too long %d \"%s\"", length, code));
		return "00000000000".substring(code.length()) + code;
	}
	
	public static boolean isValid(String code) {
		if (Strings.isNullOrEmpty(code)) {
			return false;
		}
		return IS_VALID_REGEX.matcher(code).matches();
	}
	
}
