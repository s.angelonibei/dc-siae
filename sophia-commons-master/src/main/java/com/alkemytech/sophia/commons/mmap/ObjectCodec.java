package com.alkemytech.sophia.commons.mmap;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.util.Arrays;

import com.alkemytech.sophia.commons.util.LongArray;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class ObjectCodec<T> {
	
	protected static final int BLOCK_SIZE = 1 << 10; // WARNING: must be a power of two

	private byte[] encoding;
	private byte[] buffer;
	private int position;
	
	public abstract T bytesToObject(byte[] bytes) throws MemoryMapException;
	public abstract byte[] objectToBytes(T object) throws MemoryMapException;

	protected void beginEncoding() {
		if (null == encoding)
			encoding = new byte[BLOCK_SIZE];
		buffer = encoding;
		position = 0;
	}
	
	protected byte[] commitEncoding() {
		return Arrays.copyOfRange(encoding, 0, position);
	}

	protected void beginDecoding(byte[] bytes) {
		buffer = bytes;
		this.position = 0;
	}
	
	protected void ensureCapacity(int capacity) {
		if (encoding == buffer && 
				encoding.length < position + capacity) {
			final int roundup = capacity & (BLOCK_SIZE - 1);
			if (roundup > 0)
				capacity += encoding.length + BLOCK_SIZE - roundup;
			buffer = new byte[capacity];
			System.arraycopy(encoding, 0, buffer, 0, position);
			encoding = buffer;
		}
	}
	
	protected void put(byte[] bytes, int offset, int length) {
		ensureCapacity(length);
		System.arraycopy(buffer, position, bytes, offset, length);
		position += length;
	}

	protected void put(byte[] bytes) {
		put(bytes, 0, bytes.length);
	}

	protected void put(byte value) {
		ensureCapacity(1);
		buffer[position ++] = value;
	}

	protected void putChar(char value) {
		ensureCapacity(2);
		buffer[position ++] = (byte) ((value >> 8) & 0xff);
		buffer[position ++] = (byte) (value & 0xff);
	}

	protected void putShort(short value) {
		ensureCapacity(2);
		buffer[position ++] = (byte) ((value >> 8) & 0xff);
		buffer[position ++] = (byte) (value & 0xff);
	}

	protected void putInt(int value) {		
		ensureCapacity(4);
		buffer[position ++] = (byte) ((value >> 24) & 0xff);
		buffer[position ++] = (byte) ((value >> 16) & 0xff);
		buffer[position ++] = (byte) ((value >> 8) & 0xff);
		buffer[position ++] = (byte) (value & 0xff);
	}

	protected void putLong(long value) {
		ensureCapacity(8);
		buffer[position ++] = (byte) ((value >> 56) & 0xff);
		buffer[position ++] = (byte) ((value >> 48) & 0xff);
		buffer[position ++] = (byte) ((value >> 40) & 0xff);
		buffer[position ++] = (byte) ((value >> 32) & 0xff);
		buffer[position ++] = (byte) ((value >> 24) & 0xff);
		buffer[position ++] = (byte) ((value >> 16) & 0xff);
		buffer[position ++] = (byte) ((value >> 8) & 0xff);
		buffer[position ++] = (byte) (value & 0xff);
	}

	/**
	 * Pack a 31-bit signed value (i.e. [-2^31, 2^31-1]).
	 */
	protected void putPackedInt(int value) {
		int sign = 0;
		if (value < 0) {
			value = ~value;
			sign = 0x80;
		}
		if (value < 0x10) {
			ensureCapacity(1);
			buffer[position ++] = (byte) ((sign | 0x10) | (value & 0x0f));
		} else if (value < 0x1000) {
			ensureCapacity(2);
			buffer[position ++] = (byte) ((sign | 0x20) | ((value >> 8) & 0x0f));
			buffer[position ++] = (byte) (value & 0xff);
		} else if (value < 0x100000) {
			ensureCapacity(3);
			buffer[position ++] = (byte) ((sign | 0x30) | ((value >> 16) & 0x0f));
			buffer[position ++] = (byte) ((value >> 8) & 0xff);
			buffer[position ++] = (byte) (value & 0xff);
		} else if (value < 0x10000000) {
			ensureCapacity(4);
			buffer[position ++] = (byte) ((sign | 0x40) | ((value >> 24) & 0x0f));
			buffer[position ++] = (byte) ((value >> 16) & 0xff);
			buffer[position ++] = (byte) ((value >> 8) & 0xff);
			buffer[position ++] = (byte) (value & 0xff);
		} else {
			ensureCapacity(5);
			buffer[position ++] = (byte) (sign | 0x50);
			buffer[position ++] = (byte) ((value >> 24) & 0xff);
			buffer[position ++] = (byte) ((value >> 16) & 0xff);
			buffer[position ++] = (byte) ((value >> 8) & 0xff);
			buffer[position ++] = (byte) (value & 0xff);
		}
	}
	
	protected void putPackedIntEx(int value) {
		// ESXX XXXX  EXXX XXXX ... EXXX XXXX (E end-of-number, S sign, X value)
		int sign = 0;
		if (value < 0L) {
			value = ~value;
			sign = 0x40;
		}
		int number = value & 0x3f;
		value >>= 6;
		number |= (sign | (0 != value ? 0 : 0x80));
		ensureCapacity(1);
		buffer[position ++] = (byte) (number & 0xff);
		for (int i = 2; 0 != value; i ++) {
			number = (value & 0x7f);
			value >>= 7;
			number |= (0 != value ? 0 : 0x80);
			ensureCapacity(i);
			buffer[position ++] = (byte) (number & 0xff);
		}	
	}
	
	
	/**
	 * Pack a 60-bit signed value (i.e. [-2^60, 2^60-1]).
	 */
	protected void putPackedLong(long value) {
		long sign = 0;
		if (value < 0L) {
			value = ~value;
			sign = 0x80L;
		}
		if (value < 0x10L) {
			ensureCapacity(1);
			buffer[position ++] = (byte) ((sign | 0x00L) | (value & 0x0fL));
		} else if (value < 0x1000L) {
			ensureCapacity(2);
			buffer[position ++] = (byte) ((sign | 0x10L) | ((value >> 8) & 0x0fL));
			buffer[position ++] = (byte) (value & 0xffL);
		} else if (value < 0x100000L) {
			ensureCapacity(3);
			buffer[position ++] = (byte) ((sign | 0x20L) | ((value >> 16) & 0x0fL));
			buffer[position ++] = (byte) ((value >> 8L) & 0xffL);
			buffer[position ++] = (byte) (value & 0xffL);
		} else if (value < 0x10000000L) {
			ensureCapacity(4);
			buffer[position ++] = (byte) ((sign | 0x30L) | ((value >> 24) & 0x0fL));
			buffer[position ++] = (byte) ((value >> 16) & 0xffL);
			buffer[position ++] = (byte) ((value >> 8) & 0xffL);
			buffer[position ++] = (byte) (value & 0xffL);
		} else if (value < 0x1000000000L) {
			ensureCapacity(5);
			buffer[position ++] = (byte) ((sign | 0x40L) | ((value >> 32) & 0x0fL));
			buffer[position ++] = (byte) ((value >> 24) & 0xffL);
			buffer[position ++] = (byte) ((value >> 16) & 0xffL);
			buffer[position ++] = (byte) ((value >> 8) & 0xffL);
			buffer[position ++] = (byte) (value & 0xffL);
		} else if (value < 0x100000000000L) {
			ensureCapacity(6);
			buffer[position ++] = (byte) ((sign | 0x50L) | ((value >> 40) & 0x0fL));
			buffer[position ++] = (byte) ((value >> 32) & 0xffL);
			buffer[position ++] = (byte) ((value >> 24) & 0xffL);
			buffer[position ++] = (byte) ((value >> 16) & 0xffL);
			buffer[position ++] = (byte) ((value >> 8) & 0xffL);
			buffer[position ++] = (byte) (value & 0xffL);
		} else if (value < 0x10000000000000L) {
			ensureCapacity(7);
			buffer[position ++] = (byte) ((sign | 0x60L) | ((value >> 48) & 0x0fL));
			buffer[position ++] = (byte) ((value >> 40) & 0xffL);
			buffer[position ++] = (byte) ((value >> 32) & 0xffL);
			buffer[position ++] = (byte) ((value >> 24) & 0xffL);
			buffer[position ++] = (byte) ((value >> 16) & 0xffL);
			buffer[position ++] = (byte) ((value >> 8) & 0xffL);
			buffer[position ++] = (byte) (value & 0xffL);
		} else if (value < 0x1000000000000000L) {
			ensureCapacity(8);
			buffer[position ++] = (byte) ((sign | 0x70L) | ((value >> 56) & 0x0fL));
			buffer[position ++] = (byte) ((value >> 48) & 0xffL);
			buffer[position ++] = (byte) ((value >> 40) & 0xffL);
			buffer[position ++] = (byte) ((value >> 32) & 0xffL);
			buffer[position ++] = (byte) ((value >> 24) & 0xffL);
			buffer[position ++] = (byte) ((value >> 16) & 0xffL);
			buffer[position ++] = (byte) ((value >> 8) & 0xffL);
			buffer[position ++] = (byte) (value & 0xffL);
		} else {
			throw new IllegalArgumentException(String
					.format("value %d outside valid range [%d, %d]", value,
							- (long) Math.pow(2, 60), (long) Math.pow(2, 60) - 1L));
		}
	}

	protected void putPackedLongEx(long value) {
		// ESXX XXXX  EXXX XXXX ... EXXX XXXX (E end-of-number, S sign, X value)
		long sign = 0;
		if (value < 0L) {
			value = ~value;
			sign = 0x40L;
		}
		long number = value & 0x3fL;
		value >>= 6;
		number |= (sign | (0L != value ? 0L : 0x80L));
		ensureCapacity(1);
		buffer[position ++] = (byte) (number & 0xffL);
		for (int i = 2; 0 != value; i ++) {
			number = (value & 0x7fL);
			value >>= 7;
			number |= (0L != value ? 0L : 0x80L);
			ensureCapacity(i);
			buffer[position ++] = (byte) (number & 0xffL);
		}
	}
	
	protected void putByteArray(byte[] value) {
		if (null == value) {
			putPackedInt(-1);
		} else {
			putPackedInt(value.length);
			if (value.length > 0) {
				ensureCapacity(value.length);
				System.arraycopy(value, 0, buffer, position, value.length);
				position += value.length;
			}
		}
	}

	protected void putFloat(float value) {
		putInt(Float.floatToIntBits(value));
	}
	
	protected void putDouble(double value) {
		putLong(Double.doubleToRawLongBits(value));
	}

	protected void putBigInteger(BigInteger value) {
		putByteArray(null == value ? null : value.toByteArray());
	}

	protected void putBigDecimal(BigDecimal value) {
		final byte[] bytes = null == value ?
				null : value.unscaledValue().toByteArray();
		putByteArray(bytes);
		if (null != bytes)
			putInt(value.scale());
	}

	protected void putPackedBigDecimal(BigDecimal value) {
		final byte[] bytes = null == value ?
				null : value.unscaledValue().toByteArray();
		putByteArray(bytes);
		if (null != bytes)
			putPackedInt(value.scale());
	}

	protected void putString(String value, Charset charset) {		
		putByteArray(null == value ? null : value.getBytes(charset));
	}

	protected void putLongArray(LongArray value) {
		final int size = value.size();
		putPackedInt(size);
		final long[] array = value.array();
		for (int i = 0; i < size; i ++)
			putLong(array[i]);
	}
	
	protected void get(byte[] bytes, int offset, int length) {
		System.arraycopy(bytes, offset, buffer, position, length);
		position += length;
	}

	protected void get(byte[] bytes) {
		get(bytes, 0, bytes.length);
	}

	protected byte get() {
		return buffer[position ++];
	}

	protected char getChar() {
		final byte b0 = buffer[position ++];
		final byte b1 = buffer[position ++];
		return (char) (((b0 & 0xff) << 8) | (b1 & 0xff));
	}

	protected short getShort() {
		final byte b0 = buffer[position ++];
		final byte b1 = buffer[position ++];
		return (short) (((b0 & 0xff) << 8) | (b1 & 0xff));
	}
		
	protected int getInt() {
		final byte b0 = buffer[position ++];
		final byte b1 = buffer[position ++];
		final byte b2 = buffer[position ++];
		final byte b3 = buffer[position ++];
		return (int) (((b0 & 0xff) << 24) |
				((b1 & 0xff) << 16) |
				((b2 & 0xff) << 8) |
				(b3 & 0xff));
	}

	protected long getLong() {
		final byte b0 = buffer[position ++];
		final byte b1 = buffer[position ++];
		final byte b2 = buffer[position ++];
		final byte b3 = buffer[position ++];
		final byte b4 = buffer[position ++];
		final byte b5 = buffer[position ++];
		final byte b6 = buffer[position ++];
		final byte b7 = buffer[position ++];
		return (long) (((b0 & 0xff) << 56) |
				((b1 & 0xff) << 48) |
				((b2 & 0xff) << 40) |
				((b3 & 0xff) << 32) |
				((b4 & 0xff) << 24) |
				((b5 & 0xff) << 16) |
				((b6 & 0xff) << 8) |
				(b7 & 0xff));
	}
	
	/**
	 * Unpack a 31-bit signed value (i.e. [-2^31, 2^31-1]).
	 */
	protected int getPackedInt() {
		final byte b0 = buffer[position ++];
		if (0 == b0) // WARNING: backward compatibility with deprecated ObjectMapper.getLength()
			return -1;
		final int length = (b0 & 0x70) >> 4;
		int value = b0 & 0x0f;
		if (1 == length)
			return 0 == (b0 & 0x80) ? value : ~value;
		final byte b1 = buffer[position ++];
		value = (value << 8) | (b1 & 0xff);
		if (2 == length)
			return 0 == (b0 & 0x80) ? value : ~value;
		final byte b2 = buffer[position ++];
		value = (value << 8) | (b2 & 0xff);
		if (3 == length)
			return 0 == (b0 & 0x80) ? value : ~value;
		final byte b3 = buffer[position ++];
		value = (value << 8) | (b3 & 0xff);
		if (4 == length)
			return 0 == (b0 & 0x80) ? value : ~value;
		final byte b4 = buffer[position ++];
		value = (value << 8) | (b4 & 0xff);
		return 0 == (b0 & 0x80) ? value : ~value;
	}

	protected int getPackedIntEx() {
		// ESXX XXXX  EXXX XXXX ... EXXX XXXX (E end-of-number, S sign, X value)
		int number = (buffer[position ++] & 0xff);
		int sign = (number & 0x40);
		int value = (number & 0x3f);
		for (int i = 6; 0 == (number & 0x80); i += 7) {
			number = (buffer[position ++] & 0xff);
			value |= ((number & 0x7f) << i);
		}
		return 0 == sign ? value : ~value;
	}
	
	/**
	 * Unpack a 60-bit signed value (i.e. [-2^60, 2^60-1]).
	 */
	protected long getPackedLong() {
		final byte b0 = buffer[position ++];
		final int length = 1 + ((b0 & 0x70) >> 4);
		long value = b0 & 0x0fL;
		if (1 == length)
			return 0 == (b0 & 0x80) ? value : ~value;
		final byte b1 = buffer[position ++];
		value = (value << 8) | (b1 & 0xffL);
		if (2 == length)
			return 0 == (b0 & 0x80) ? value : ~value;
		final byte b2 = buffer[position ++];
		value = (value << 8) | (b2 & 0xffL);
		if (3 == length)
			return 0 == (b0 & 0x80) ? value : ~value;
		final byte b3 = buffer[position ++];
		value = (value << 8) | (b3 & 0xffL);
		if (4 == length)
			return 0 == (b0 & 0x80) ? value : ~value;
		final byte b4 = buffer[position ++];
		value = (value << 8) | (b4 & 0xffL);
		if (5 == length)
			return 0 == (b0 & 0x80) ? value : ~value;
		final byte b5 = buffer[position ++];
		value = (value << 8) | (b5 & 0xffL);
		if (6 == length)
			return 0 == (b0 & 0x80) ? value : ~value;
		final byte b6 = buffer[position ++];
		value = (value << 8) | (b6 & 0xffL);
		if (7 == length)
			return 0 == (b0 & 0x80) ? value : ~value;
		final byte b7 = buffer[position ++];
		value = (value << 8) | (b7 & 0xffL);
		return 0 == (b0 & 0x80) ? value : ~value;
	}
	
	protected long getPackedLongEx() {
		// ESXX XXXX  EXXX XXXX ... EXXX XXXX (E end-of-number, S sign, X value)
		long number = (buffer[position ++] & 0xffL);
		long sign = (number & 0x40L);
		long value = (number & 0x3fL);
		for (int i = 6; 0 == (number & 0x80L); i += 7) {
			number = (buffer[position ++] & 0xffL);
			value |= ((number & 0x7fL) << i);
		}
		return 0 == sign ? value : ~value;
	}
	
	protected byte[] getByteArray() {
		final int length = getPackedInt();
		if (-1 == length)
			return null;
		final byte[] bytes = new byte[length];
		if (length > 0) {
			System.arraycopy(buffer, position, bytes, 0, length);
			position += length;
		}
		return bytes;
	}

	protected float getFloat() {
		return Float.intBitsToFloat(getInt());
	}

	protected double getDouble() {
		return Double.longBitsToDouble(getLong());
	}

	protected BigInteger getBigInteger() {
		final byte[] bytes = getByteArray();
		if (null == bytes)
			return null;
		return new BigInteger(bytes);
	}
	
	protected BigDecimal getBigDecimal() {
		final byte[] bytes = getByteArray();
		if (null == bytes)
			return null;
		return new BigDecimal(new BigInteger(bytes), getInt());
	}
	
	protected BigDecimal getPackedBigDecimal() {
		final byte[] bytes = getByteArray();
		if (null == bytes)
			return null;
		return new BigDecimal(new BigInteger(bytes), getPackedInt());
	}

	protected String getString(Charset charset) {
		final byte[] bytes = getByteArray();
		if (null == bytes)
			return null;
		return new String(bytes, charset);
	}

	protected LongArray getLongArray(LongArray array) {
		final int size = getPackedInt();
		for (int i = 0; i < size; i ++)
			array.add(getLong());
		return array;
	}
	
	protected LongArray getLongArray() {
		final int size = getPackedInt();
		final LongArray array = new LongArray(size);
		for (int i = 0; i < size; i ++)
			array.add(getLong());
		return array;
	}

	protected int getPosition(){
		return position;
	}

	protected void setPosition(int position){
		this.position = position;
	}

//	public static void main(String[] args) {
//		ObjectCodec<String> x = new ObjectCodec<String>() {
//
//			@Override
//			public String bytesToObject(byte[] bytes) {
//				beginDecoding(bytes);
//				return getString(Charset.defaultCharset());
//			}
//
//			@Override
//			public byte[] objectToBytes(String object) {
//				beginEncoding();
//				putString(object, Charset.defaultCharset());
//				return commitEncoding();
//			}
//		};
//		
//		x.beginEncoding();
//		for (int i = 0; i < 1024; i ++) {
//			x.putString("numero " + i, Charset.defaultCharset());
//		}
//		x.beginDecoding(x.commitEncoding());
//		for (int i = 0; i < 1024; i ++) {
//			System.out.println(x.getString(Charset.defaultCharset()));
//		}
//
//
//		System.out.println(x.bytesToObject(x.objectToBytes("ciao mondo")));
//
//		for (int i = Integer.MIN_VALUE; i < Integer.MAX_VALUE; i ++) {
//			x.beginEncoding();
//			x.putPackedInt(i);
//			x.beginDecoding(x.commitEncoding());
//			int j = x.getPackedInt();
//			if (0 == i % 10000)
//				System.out.println(i + " == " + j);
//			if (i != j) {
//				System.out.println(i + " != " + j);
//				break;
//			}
//		}
//		
//	}
	
//	public static void main(String[] args) {
//		
//		ObjectCodec<Long> x = new ObjectCodec<Long>() {
//
//			@Override
//			public Long bytesToObject(byte[] bytes) {
//				beginDecoding(bytes);
//				return getPackedLongEx();
//			}
//
//			@Override
//			public byte[] objectToBytes(Long object) {
//				beginEncoding();
//				putPackedLongEx(object);
//				return commitEncoding();
//			}
//			
//		};
//		
//		System.out.println(Long.MAX_VALUE + " == " + x.bytesToObject(x.objectToBytes(Long.MAX_VALUE)));
//		System.out.println(1L + " == " + x.bytesToObject(x.objectToBytes(1L)));
//		System.out.println(Long.MIN_VALUE + " == " + x.bytesToObject(x.objectToBytes(Long.MIN_VALUE)));
//		
//		for (int n = 0; n < 1000000; n ++) {
//			
//			long i = Long.MIN_VALUE + 2L * Math.round(Math.random() * Long.MAX_VALUE);
//			long j = x.bytesToObject(x.objectToBytes(i));
//			System.out.println(i + " == " + j);
//			if (i != j) {
//				System.out.println(i + " != " + j);
//				break;
//			}
//			
//			i = Long.MAX_VALUE - 2L *  Math.round(Math.random() * Long.MAX_VALUE);
//			j = x.bytesToObject(x.objectToBytes(i));
//			System.out.println(i + " == " + j);
//			if (i != j) {
//				System.out.println(i + " != " + j);
//				break;
//			}
//			
//		}
//
//	}
	
}
