package com.alkemytech.sophia.commons.util;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class MySqlUtils {

	public static String escapeJson(String json) {
		if (null == json)
			return null;
		return json.replace("\\", "\\\\");
	}
	
}
