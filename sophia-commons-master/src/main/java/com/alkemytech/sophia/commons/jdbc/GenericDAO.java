package com.alkemytech.sophia.commons.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class GenericDAO {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	protected final Properties configuration;
	protected final DataSource dataSource;
	private final Pattern parameterPattern;
	private final Pattern numberPattern;
	
	private Long lastGeneratedKey;
	protected Connection connection;
	
	public GenericDAO(Properties configuration, DataSource dataSource) {
		super();
		this.configuration = configuration;
		this.dataSource = dataSource;
		this.parameterPattern = Pattern.compile(".*(\\{[A-Za-z0-9\\_\\-]+\\}).*");
		this.numberPattern = Pattern.compile("^[-+]?\\d+(\\.\\d+)?$");
	}
	
	public DataSource getDataSource() {
		return dataSource;
	}
	
	public Long getLastGeneratedKey() {
		return lastGeneratedKey;
	}
	
	protected String replaceParameters(String sql, Map<String, String> parameters) {
		if (null != sql && null != parameters && !parameters.isEmpty()) {
			Matcher matcher = parameterPattern.matcher(sql);
			while (matcher.matches()) {
				final String key = matcher.group(1);
				final String name = key.substring(1, key.length() - 1);
				String value = parameters.get(name);
				if (null == value) {
					value = "NULL";
				} else if (!numberPattern.matcher(value).matches()) {
					value = '\'' + value.replace("'", "''")
										.replace("\\", "\\\\") + '\'';
				}
				logger.debug("replacing {} with {}", key, value);
				sql = sql.replace(key, value);
				matcher = parameterPattern.matcher(sql);
			}
		}
		return sql;
	}
	
//	private String replaceParameters(String sql, Map<String, String> parameters) {
//		if (null != parameters)
//			for (Map.Entry<String, String> parameter : parameters.entrySet())
//				sql = sql.replace('{' + parameter.getKey() + '}',
//						parameter.getValue().replace("'", "''"));
//		return sql;
//	}

	protected boolean isInsideTransaction() throws SQLException {
		return null != connection &&
				!connection.isClosed() &&
				!connection.getAutoCommit();
	}
	
	public void beginTransaction() throws SQLException {
		logger.debug("begin transaction");
		if (null == connection || connection.isClosed()) {
			connection = dataSource.getConnection();
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
		}
		connection.setAutoCommit(false);
	}
	
	public void commitTransaction() throws SQLException {
		if (null != connection) {
			logger.debug("commit transaction");
			connection.commit();
			connection.setAutoCommit(true);
			connection.close();
			connection = null;
		}
	}

	public void rollbackTransaction() throws SQLException {
		if (null != connection) {
			logger.debug("rollback transaction");
			connection.rollback();
			connection.setAutoCommit(true);
			connection.close();
			connection = null;
		}
	}
	
	public List<Map<String, String>> executeQuery(String propertyName) throws SQLException {
		return executeQuery(propertyName, null);
	}
	
	public List<Map<String, String>> executeQuery(String propertyName, Map<String, String> parameters) throws SQLException {
		final String sql = replaceParameters(configuration
				.getProperty(propertyName, null), parameters);
		if (null == sql)
			throw new IllegalArgumentException("unknown property name " + propertyName);
		logger.debug("executing sql {}", sql);
		final boolean insideTransaction = isInsideTransaction();
		if (!insideTransaction) {
			connection = dataSource.getConnection();
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			connection.setAutoCommit(true);
		}
		try (final Statement statement = connection.createStatement();
				final ResultSet resultSet = statement.executeQuery(sql)) {
			final ResultSetMetaData metadata = resultSet.getMetaData();
			final int columnCount = metadata.getColumnCount();
			final List<Map<String, String>> rows = new ArrayList<>();
			while (resultSet.next()) {
				final Map<String, String> row = new HashMap<>();
				for (int i = 1; i <= columnCount; i ++) {
					final String column = resultSet.getString(i);
					if (null != column)
						row.put(metadata.getColumnLabel(i), column);
				}
				rows.add(row);
			}
			return rows;
		} finally {
			if (!insideTransaction) {
				connection.close();
				connection = null;
			}
		}
	}
	
	public Map<String, String> executeSingleRowQuery(String propertyName) throws SQLException {
		return executeSingleRowQuery(propertyName, null);
	}
	
	public Map<String, String> executeSingleRowQuery(String propertyName, Map<String, String> parameters) throws SQLException {
		final String sql = replaceParameters(configuration
				.getProperty(propertyName, null), parameters);
		if (null == sql)
			throw new IllegalArgumentException("unknown property name " + propertyName);
		logger.debug("executing sql {}", sql);
		final boolean insideTransaction = isInsideTransaction();
		if (!insideTransaction) {
			connection = dataSource.getConnection();
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			connection.setAutoCommit(true);
		}
		try (final Statement statement = connection.createStatement();
				final ResultSet resultSet = statement.executeQuery(sql)) {
			final ResultSetMetaData metadata = resultSet.getMetaData();
			final int columnCount = metadata.getColumnCount();
			if (resultSet.next()) {
				final Map<String, String> row = new HashMap<>();
				for (int i = 1; i <= columnCount; i ++) {
					final String column = resultSet.getString(i);
					if (null != column)
						row.put(metadata.getColumnLabel(i), column);
				}
				return row;
			}
		} finally {
			if (!insideTransaction) {
				connection.close();
				connection = null;
			}
		}
		return null;
	}

	public int executeUpdate(String propertyName) throws SQLException {
		return executeUpdate(propertyName, null, false);
	}

	public int executeUpdate(String propertyName, boolean returnGeneratedKeys) throws SQLException {
		return executeUpdate(propertyName, null, returnGeneratedKeys);
	}

	public int executeUpdate(String propertyName, Map<String, String> parameters) throws SQLException {
		return executeUpdate(propertyName, parameters, false);
	}

	public int executeUpdate(String propertyName, Map<String, String> parameters, boolean returnGeneratedKeys) throws SQLException {
		final String sql = replaceParameters(configuration
				.getProperty(propertyName, null), parameters);
		if (null == sql)
			throw new IllegalArgumentException("unknown property name " + propertyName);
		logger.debug("executing sql {}", sql);
		final boolean insideTransaction = isInsideTransaction();
		if (!insideTransaction) {
			connection = dataSource.getConnection();
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			connection.setAutoCommit(true);
		}
		try (final Statement statement = connection.createStatement()) {
			if (returnGeneratedKeys) {					
				lastGeneratedKey = null;
				final int count = statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
				try (final ResultSet resultSet = statement.getGeneratedKeys()) {
					if (resultSet.next())
						lastGeneratedKey = resultSet.getLong(1);
				}
				return count;
			}
			return statement.executeUpdate(sql, Statement.NO_GENERATED_KEYS);
		} finally {
			if (!insideTransaction) {
				connection.close();
				connection = null;
			}
		}
	}

}
