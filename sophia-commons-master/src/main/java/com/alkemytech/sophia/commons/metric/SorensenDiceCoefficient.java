package com.alkemytech.sophia.commons.metric;

import java.util.HashSet;

import com.alkemytech.sophia.commons.util.TextUtils;
import com.google.common.collect.Sets;

/**
 * Sørensen–Dice Coefficient is similar to Jaccard index, but the
 * similarity is computed as 2 * |V1 intersection V2| / (|V1| + |V2|).
 * 
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class SorensenDiceCoefficient {

	/**
	 * Build the set of all unique two-grams found in string.
	 * 
	 * @param s a string
	 * @return the unique two-grams set
	 */
	private static HashSet<String> digrams(String s) {
		final HashSet<String> dgs = new HashSet<String>();
		char[] chs = s.toCharArray();
		for (int i = 0; i < chs.length - 1; i ++) {
			dgs.add(new String(chs, i, 2));
		}
		return dgs;
	}
	
	/**
	 * Sørensen–Dice Coefficient is a value between 0.0 and 1.0 measuring the number of overlapping two-grams.
	 * 
	 * @param a a string
	 * @param b another string
	 * @return the Sørensen–Dice Coefficient value
	 */
	public static double sorensenDiceCoefficient(String a, String b) {
		a = TextUtils.lpad(a, 2, '\n');
		b = TextUtils.lpad(b, 2, '\n');
		final HashSet<String> na = digrams(a);
		final HashSet<String> nb = digrams(b);
	    return (2.0 * Sets.intersection(na, nb).size()) / (na.size() + nb.size());
	}

}
