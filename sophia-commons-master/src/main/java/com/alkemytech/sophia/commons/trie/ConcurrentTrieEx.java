package com.alkemytech.sophia.commons.trie;

import com.alkemytech.sophia.commons.mmap.MemoryMap;
import com.alkemytech.sophia.commons.mmap.MemoryMapException;

/**
 * Thread safe version of class TrieEx&lt;T&gt;.
 * 
 * @see {@link TrieEx}
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ConcurrentTrieEx<T> extends TrieEx<T> {

	public static <X> ConcurrentTrieEx<X> wrap(MemoryMap memoryMap, LeafBinding<X> leafBinding) throws MemoryMapException {
		return new ConcurrentTrieEx<X>(memoryMap, leafBinding);
	}
	
	public ConcurrentTrieEx(MemoryMap memoryMap, LeafBinding<T> leafBinding) throws MemoryMapException {
		super(memoryMap, leafBinding);
	}

	@Override
	public synchronized T upsert(String text, T leaf) throws MemoryMapException {
		return super.upsert(text, leaf);
	}

	@Override
	public synchronized ConcurrentTrieEx<T> clear() {
		super.clear();
		return this;
	}

	@Override
	public synchronized  ConcurrentTrieEx<T> garbage() {
		super.garbage();
		return this;
	}
	
}
