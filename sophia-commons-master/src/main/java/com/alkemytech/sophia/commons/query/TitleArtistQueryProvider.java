package com.alkemytech.sophia.commons.query;

import java.util.Properties;

import com.google.inject.Provider;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class TitleArtistQueryProvider implements Provider<TitleArtistQuery> {

	private final Properties configuration;
	private final String propertyPrefix;
	
	public TitleArtistQueryProvider(Properties configuration, String propertyPrefix) {
		super();
		this.configuration = configuration;
		this.propertyPrefix = propertyPrefix;
	}

	@Override
	public TitleArtistQuery get() {
		final String className = configuration
				.getProperty(propertyPrefix + ".class_name");
		if (ExactTitleArtistQuery.class.getName().equals(className)) {
			return new ExactTitleArtistQuery(configuration, propertyPrefix);
		} else if (SmartTitleArtistQuery.class.getName().equals(className)) {
			return new SmartTitleArtistQuery(configuration, propertyPrefix);
		} else if (FuzzyTitleArtistQuery.class.getName().equals(className)) {
			return new FuzzyTitleArtistQuery(configuration, propertyPrefix);
		}
		throw new IllegalArgumentException(String
				.format("unknown class name \"%s\"", className));
	}

}
