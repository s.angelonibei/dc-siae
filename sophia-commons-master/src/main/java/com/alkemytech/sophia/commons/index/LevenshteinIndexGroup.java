package com.alkemytech.sophia.commons.index;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.alkemytech.sophia.commons.util.LongArray;

/**
 * A collection of reverse index(es) supporting exact and approximate string matching.
 * 
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class LevenshteinIndexGroup implements LevenshteinIndex {

	/**
	 * Exact match.
	 */
	private static class SingleTermSet implements TermSet {

		private final String term;
		private final LongArray postings;
		
		private int remaining;
		
		protected SingleTermSet(String term, LongArray postings) {
			super();
			this.term = term;
			this.postings = postings;
			this.remaining = 1;
		}

		@Override
		public int size() {
			return 1;			
		}

		@Override
		public boolean next() {
			remaining --;
			return 0 == remaining;
		}

		@Override
		public String getTerm() {
			if (0 == remaining)
				return term;
			throw new IllegalStateException(remaining > 0 ?
					"before first" : "after last");
		}

		@Override
		public LongArray getPostings() {
			if (0 == remaining)
				return postings;
			throw new IllegalStateException(remaining > 0 ?
					"before first" : "after last");
		}
		
	}
	
	/**
	 * Fuzzy match.
	 */
	private static class MultipleTermSet implements TermSet {
		
		private final Iterator<Map.Entry<String, LongArray>> iterator;
		private final int size;
		
		private Map.Entry<String, LongArray> term;
		
		protected MultipleTermSet(Map<String, LongArray> postingsMap) {
			super();
			this.size = postingsMap.size();
			this.iterator = postingsMap.entrySet().iterator();
		}

		@Override
		public final int size() {
			return size;			
		}

		@Override
		public final boolean next() {
			if (iterator.hasNext()) {
				term = iterator.next();
				return true;
			}
			term = null;
			return false;
		}

		@Override
		public final String getTerm() {
			if (null != term)
				return term.getKey();
			throw new IllegalStateException(iterator.hasNext() ?
					"before first" : "after last");
		}

		@Override
		public final LongArray getPostings() {
			if (null != term)
				return term.getValue();
			throw new IllegalStateException(iterator.hasNext() ?
					"before first" : "after last");
		}
		
	}
	
	protected final String name;
	protected final LevenshteinIndex[] indexes;
	
	public LevenshteinIndexGroup(String name, LevenshteinIndex ... indexes) {
		super();
		this.name = name;
		this.indexes = indexes;
		if (indexes.length < 2)
			throw new IllegalArgumentException(String
					.format("bad number of index(es): %d", indexes.length));
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public boolean isReadOnly() {
		for (LevenshteinIndex index : indexes) {
			if (!index.isReadOnly())
				return false;
		}
		return true;
	}

	@Override
	public TermSet search(String text) {
		LongArray postings = null;
		for (LevenshteinIndex index : indexes) {
			final TermSet termSet = index.search(text);
			if (null == termSet)
				continue;
			if (termSet.next()) {
				postings = null == postings ? termSet.getPostings() :
					LongArray.union(postings, termSet.getPostings());
			}
		}
		return null == postings ? TermSet.EMPTY :
			new SingleTermSet(text, postings);
	}

	@Override
	public TermSet search(String text, int maxDistance) {
		Map<String, LongArray> postingsMap = null;
		for (LevenshteinIndex index : indexes) {
			final TermSet termSet = index.search(text, maxDistance);
			if (null == termSet)
				continue;
			if (null == postingsMap)
				postingsMap = new HashMap<>();
			while (termSet.next()) {
				final String term = termSet.getTerm();
				final LongArray postings = postingsMap.get(term);
				postingsMap.put(term, null == postings ?
						termSet.getPostings() : LongArray.union(postings,
								termSet.getPostings()));
			}
		}
		return null == postingsMap ? TermSet.EMPTY :
			new MultipleTermSet(postingsMap);
	}
	
}
