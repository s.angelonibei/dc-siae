package com.alkemytech.sophia.commons.query;

import com.alkemytech.sophia.commons.index.LevenshteinIndex;
import com.alkemytech.sophia.commons.util.LongArray;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class FuzzyOrQueryChain extends AbstractOrQueryChain {

	private final int[] fuzzyCosts;

	public FuzzyOrQueryChain(int[] fuzzyCosts) {
		super();
		this.fuzzyCosts = fuzzyCosts;
	}

	@Override
	protected LongArray searchTerm(LevenshteinIndex index, String text) {
		return fuzzySearch(index, text, fuzzyCosts);
	}
	
}
