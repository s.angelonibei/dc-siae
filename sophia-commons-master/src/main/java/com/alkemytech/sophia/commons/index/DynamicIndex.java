package com.alkemytech.sophia.commons.index;

import java.util.Collection;

/**
 * A reverse index that can be dynamically modified.
 * 
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface DynamicIndex extends LevenshteinIndex {

	/**
	 * Startup instance.
	 */
	public void startup() throws LevenshteinIndexException;
	
	/**
	 * Add a <code>term</code> from a document identified by <code>docId</code>.
	 */
	public void put(String term, long docId) throws LevenshteinIndexException;

	/**
	 * Add a collection of <code>term</code>(s) from a document identified by <code>docId</code>.
	 */
	public void putAll(Collection<String> terms, long docId) throws LevenshteinIndexException;

	/**
	 * Remove a <code>docId</code> from a given <code>term</code>.
	 */
	public boolean remove(String term, long docId) throws LevenshteinIndexException;

	/**
	 * Remove a <code>docId</code> from a given collection of <code>term</code>(s).
	 */
	public boolean removeAll(Collection<String> terms, long docId) throws LevenshteinIndexException;

	/**
	 * Shutdown instance.
	 */
	public void shutdown() throws LevenshteinIndexException;
	
}
