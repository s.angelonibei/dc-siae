package com.alkemytech.sophia.commons.jdbc;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class DBCP2DataSource implements DataSource {

	private final BasicDataSource dataSource;

	protected DBCP2DataSource(Properties configuration, String propertyPrefix) {
		super();
		dataSource = new BasicDataSource();
		dataSource.setDriverClassName(configuration.getProperty(propertyPrefix + ".jdbc.driver_class"));
		dataSource.setUrl(configuration.getProperty(propertyPrefix + ".jdbc.url"));
		dataSource.setUsername(configuration.getProperty(propertyPrefix + ".jdbc.user"));
		dataSource.setPassword(configuration.getProperty(propertyPrefix + ".jdbc.password"));
		dataSource.setInitialSize(Integer.parseInt(configuration.getProperty(propertyPrefix + ".dbcp.initial_size", "0")));
		dataSource.setMaxTotal(Integer.parseInt(configuration.getProperty(propertyPrefix + ".dbcp.max_total", "8")));
		dataSource.setMaxIdle(Integer.parseInt(configuration.getProperty(propertyPrefix + ".dbcp.max_idle", "8")));
		dataSource.setMinIdle(Integer.parseInt(configuration.getProperty(propertyPrefix + ".dbcp.min_idle", "0")));
		dataSource.setMaxWaitMillis(Long.parseLong(configuration.getProperty(propertyPrefix + ".dbcp.max_wait_millis", "-1")));
	}

	public void addConnectionProperty(String name, String value) {
		dataSource.addConnectionProperty(name, value);
	}

	public void close() throws SQLException {
		dataSource.close();
	}

	public boolean equals(Object obj) {
		return dataSource.equals(obj);
	}

	public PrintWriter getAbandonedLogWriter() {
		return dataSource.getAbandonedLogWriter();
	}

	public boolean getAbandonedUsageTracking() {
		return dataSource.getAbandonedUsageTracking();
	}

	public boolean getCacheState() {
		return dataSource.getCacheState();
	}

	public Connection getConnection() throws SQLException {
		return dataSource.getConnection();
	}

	public Connection getConnection(String user, String pass) throws SQLException {
		return dataSource.getConnection(user, pass);
	}

	public List<String> getConnectionInitSqls() {
		return dataSource.getConnectionInitSqls();
	}

	public String[] getConnectionInitSqlsAsArray() {
		return dataSource.getConnectionInitSqlsAsArray();
	}

	public Boolean getDefaultAutoCommit() {
		return dataSource.getDefaultAutoCommit();
	}

	public String getDefaultCatalog() {
		return dataSource.getDefaultCatalog();
	}

	public Integer getDefaultQueryTimeout() {
		return dataSource.getDefaultQueryTimeout();
	}

	public Boolean getDefaultReadOnly() {
		return dataSource.getDefaultReadOnly();
	}

	public int getDefaultTransactionIsolation() {
		return dataSource.getDefaultTransactionIsolation();
	}

	public Set<String> getDisconnectionSqlCodes() {
		return dataSource.getDisconnectionSqlCodes();
	}

	public String[] getDisconnectionSqlCodesAsArray() {
		return dataSource.getDisconnectionSqlCodesAsArray();
	}

	public Driver getDriver() {
		return dataSource.getDriver();
	}

	public ClassLoader getDriverClassLoader() {
		return dataSource.getDriverClassLoader();
	}

	public String getDriverClassName() {
		return dataSource.getDriverClassName();
	}

	public boolean getEnableAutoCommitOnReturn() {
		return dataSource.getEnableAutoCommitOnReturn();
	}

	public String getEvictionPolicyClassName() {
		return dataSource.getEvictionPolicyClassName();
	}

	public boolean getFastFailValidation() {
		return dataSource.getFastFailValidation();
	}

	public int getInitialSize() {
		return dataSource.getInitialSize();
	}

	public String getJmxName() {
		return dataSource.getJmxName();
	}

	public boolean getLifo() {
		return dataSource.getLifo();
	}

	public boolean getLogAbandoned() {
		return dataSource.getLogAbandoned();
	}

	public boolean getLogExpiredConnections() {
		return dataSource.getLogExpiredConnections();
	}

	public PrintWriter getLogWriter() throws SQLException {
		return dataSource.getLogWriter();
	}

	public int getLoginTimeout() throws SQLException {
		return dataSource.getLoginTimeout();
	}

	public long getMaxConnLifetimeMillis() {
		return dataSource.getMaxConnLifetimeMillis();
	}

	public int getMaxIdle() {
		return dataSource.getMaxIdle();
	}

	public int getMaxOpenPreparedStatements() {
		return dataSource.getMaxOpenPreparedStatements();
	}

	public int getMaxTotal() {
		return dataSource.getMaxTotal();
	}

	public long getMaxWaitMillis() {
		return dataSource.getMaxWaitMillis();
	}

	public long getMinEvictableIdleTimeMillis() {
		return dataSource.getMinEvictableIdleTimeMillis();
	}

	public int getMinIdle() {
		return dataSource.getMinIdle();
	}

	public int getNumActive() {
		return dataSource.getNumActive();
	}

	public int getNumIdle() {
		return dataSource.getNumIdle();
	}

	public int getNumTestsPerEvictionRun() {
		return dataSource.getNumTestsPerEvictionRun();
	}

	public Logger getParentLogger() throws SQLFeatureNotSupportedException {
		return dataSource.getParentLogger();
	}

	public String getPassword() {
		return dataSource.getPassword();
	}

	public boolean getRemoveAbandonedOnBorrow() {
		return dataSource.getRemoveAbandonedOnBorrow();
	}

	public boolean getRemoveAbandonedOnMaintenance() {
		return dataSource.getRemoveAbandonedOnMaintenance();
	}

	public int getRemoveAbandonedTimeout() {
		return dataSource.getRemoveAbandonedTimeout();
	}

	public boolean getRollbackOnReturn() {
		return dataSource.getRollbackOnReturn();
	}

	public long getSoftMinEvictableIdleTimeMillis() {
		return dataSource.getSoftMinEvictableIdleTimeMillis();
	}

	public boolean getTestOnBorrow() {
		return dataSource.getTestOnBorrow();
	}

	public boolean getTestOnCreate() {
		return dataSource.getTestOnCreate();
	}

	public boolean getTestOnReturn() {
		return dataSource.getTestOnReturn();
	}

	public boolean getTestWhileIdle() {
		return dataSource.getTestWhileIdle();
	}

	public long getTimeBetweenEvictionRunsMillis() {
		return dataSource.getTimeBetweenEvictionRunsMillis();
	}

	public String getUrl() {
		return dataSource.getUrl();
	}

	public String getUsername() {
		return dataSource.getUsername();
	}

	public String getValidationQuery() {
		return dataSource.getValidationQuery();
	}

	public int getValidationQueryTimeout() {
		return dataSource.getValidationQueryTimeout();
	}

	public int hashCode() {
		return dataSource.hashCode();
	}

	public void invalidateConnection(Connection arg0) throws IllegalStateException {
		dataSource.invalidateConnection(arg0);
	}

	public boolean isAccessToUnderlyingConnectionAllowed() {
		return dataSource.isAccessToUnderlyingConnectionAllowed();
	}

	public boolean isClosed() {
		return dataSource.isClosed();
	}

	public boolean isPoolPreparedStatements() {
		return dataSource.isPoolPreparedStatements();
	}

	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return dataSource.isWrapperFor(iface);
	}

	public void postDeregister() {
		dataSource.postDeregister();
	}

	public void postRegister(Boolean registrationDone) {
		dataSource.postRegister(registrationDone);
	}

	public void preDeregister() throws Exception {
		dataSource.preDeregister();
	}

	public ObjectName preRegister(MBeanServer arg0, ObjectName arg1) {
		return dataSource.preRegister(arg0, arg1);
	}

	public void removeConnectionProperty(String name) {
		dataSource.removeConnectionProperty(name);
	}

	public void setAbandonedLogWriter(PrintWriter logWriter) {
		dataSource.setAbandonedLogWriter(logWriter);
	}

	public void setAbandonedUsageTracking(boolean usageTracking) {
		dataSource.setAbandonedUsageTracking(usageTracking);
	}

	public void setAccessToUnderlyingConnectionAllowed(boolean allow) {
		dataSource.setAccessToUnderlyingConnectionAllowed(allow);
	}

	public void setCacheState(boolean cacheState) {
		dataSource.setCacheState(cacheState);
	}

	public void setConnectionInitSqls(Collection<String> arg0) {
		dataSource.setConnectionInitSqls(arg0);
	}

	public void setConnectionProperties(String arg0) {
		dataSource.setConnectionProperties(arg0);
	}

	public void setDefaultAutoCommit(Boolean defaultAutoCommit) {
		dataSource.setDefaultAutoCommit(defaultAutoCommit);
	}

	public void setDefaultCatalog(String defaultCatalog) {
		dataSource.setDefaultCatalog(defaultCatalog);
	}

	public void setDefaultQueryTimeout(Integer defaultQueryTimeout) {
		dataSource.setDefaultQueryTimeout(defaultQueryTimeout);
	}

	public void setDefaultReadOnly(Boolean defaultReadOnly) {
		dataSource.setDefaultReadOnly(defaultReadOnly);
	}

	public void setDefaultTransactionIsolation(int defaultTransactionIsolation) {
		dataSource.setDefaultTransactionIsolation(defaultTransactionIsolation);
	}

	public void setDisconnectionSqlCodes(Collection<String> arg0) {
		dataSource.setDisconnectionSqlCodes(arg0);
	}

	public void setDriver(Driver driver) {
		dataSource.setDriver(driver);
	}

	public void setDriverClassLoader(ClassLoader driverClassLoader) {
		dataSource.setDriverClassLoader(driverClassLoader);
	}

	public void setDriverClassName(String driverClassName) {
		dataSource.setDriverClassName(driverClassName);
	}

	public void setEnableAutoCommitOnReturn(boolean enableAutoCommitOnReturn) {
		dataSource.setEnableAutoCommitOnReturn(enableAutoCommitOnReturn);
	}

	public void setEvictionPolicyClassName(String evictionPolicyClassName) {
		dataSource.setEvictionPolicyClassName(evictionPolicyClassName);
	}

	public void setFastFailValidation(boolean fastFailValidation) {
		dataSource.setFastFailValidation(fastFailValidation);
	}

	public void setInitialSize(int initialSize) {
		dataSource.setInitialSize(initialSize);
	}

	public void setJmxName(String jmxName) {
		dataSource.setJmxName(jmxName);
	}

	public void setLifo(boolean lifo) {
		dataSource.setLifo(lifo);
	}

	public void setLogAbandoned(boolean logAbandoned) {
		dataSource.setLogAbandoned(logAbandoned);
	}

	public void setLogExpiredConnections(boolean logExpiredConnections) {
		dataSource.setLogExpiredConnections(logExpiredConnections);
	}

	public void setLogWriter(PrintWriter logWriter) throws SQLException {
		dataSource.setLogWriter(logWriter);
	}

	public void setLoginTimeout(int loginTimeout) throws SQLException {
		dataSource.setLoginTimeout(loginTimeout);
	}

	public void setMaxConnLifetimeMillis(long maxConnLifetimeMillis) {
		dataSource.setMaxConnLifetimeMillis(maxConnLifetimeMillis);
	}

	public void setMaxIdle(int maxIdle) {
		dataSource.setMaxIdle(maxIdle);
	}

	public void setMaxOpenPreparedStatements(int maxOpenStatements) {
		dataSource.setMaxOpenPreparedStatements(maxOpenStatements);
	}

	public void setMaxTotal(int maxTotal) {
		dataSource.setMaxTotal(maxTotal);
	}

	public void setMaxWaitMillis(long maxWaitMillis) {
		dataSource.setMaxWaitMillis(maxWaitMillis);
	}

	public void setMinEvictableIdleTimeMillis(long minEvictableIdleTimeMillis) {
		dataSource.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
	}

	public void setMinIdle(int minIdle) {
		dataSource.setMinIdle(minIdle);
	}

	public void setNumTestsPerEvictionRun(int numTestsPerEvictionRun) {
		dataSource.setNumTestsPerEvictionRun(numTestsPerEvictionRun);
	}

	public void setPassword(String password) {
		dataSource.setPassword(password);
	}

	public void setPoolPreparedStatements(boolean poolingStatements) {
		dataSource.setPoolPreparedStatements(poolingStatements);
	}

	public void setRemoveAbandonedOnBorrow(boolean removeAbandonedOnBorrow) {
		dataSource.setRemoveAbandonedOnBorrow(removeAbandonedOnBorrow);
	}

	public void setRemoveAbandonedOnMaintenance(boolean removeAbandonedOnMaintenance) {
		dataSource.setRemoveAbandonedOnMaintenance(removeAbandonedOnMaintenance);
	}

	public void setRemoveAbandonedTimeout(int removeAbandonedTimeout) {
		dataSource.setRemoveAbandonedTimeout(removeAbandonedTimeout);
	}

	public void setRollbackOnReturn(boolean rollbackOnReturn) {
		dataSource.setRollbackOnReturn(rollbackOnReturn);
	}

	public void setSoftMinEvictableIdleTimeMillis(long softMinEvictableIdleTimeMillis) {
		dataSource.setSoftMinEvictableIdleTimeMillis(softMinEvictableIdleTimeMillis);
	}

	public void setTestOnBorrow(boolean testOnBorrow) {
		dataSource.setTestOnBorrow(testOnBorrow);
	}

	public void setTestOnCreate(boolean testOnCreate) {
		dataSource.setTestOnCreate(testOnCreate);
	}

	public void setTestOnReturn(boolean testOnReturn) {
		dataSource.setTestOnReturn(testOnReturn);
	}

	public void setTestWhileIdle(boolean testWhileIdle) {
		dataSource.setTestWhileIdle(testWhileIdle);
	}

	public void setTimeBetweenEvictionRunsMillis(long timeBetweenEvictionRunsMillis) {
		dataSource.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
	}

	public void setUrl(String url) {
		dataSource.setUrl(url);
	}

	public void setUsername(String username) {
		dataSource.setUsername(username);
	}

	public void setValidationQuery(String validationQuery) {
		dataSource.setValidationQuery(validationQuery);
	}

	public void setValidationQueryTimeout(int timeout) {
		dataSource.setValidationQueryTimeout(timeout);
	}

	public String toString() {
		return dataSource.toString();
	}

	public <T> T unwrap(Class<T> iface) throws SQLException {
		return dataSource.unwrap(iface);
	}

}