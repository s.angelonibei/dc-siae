package com.alkemytech.sophia.commons.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.index.LevenshteinIndex;
import com.alkemytech.sophia.commons.index.LevenshteinIndex.TermSet;
import com.alkemytech.sophia.commons.module.DynamicModuleException;
import com.alkemytech.sophia.commons.util.ListUtils;
import com.alkemytech.sophia.commons.util.LongArray;
import com.google.inject.Inject;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class SmartTitleArtistQuery implements TitleArtistQuery {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final String propertyPrefix;

	private int maxTerms;
	private int minTerms;

	@Inject
	protected SmartTitleArtistQuery() {
		super();
		this.propertyPrefix = "smart_title_artist_query";
	}

	public SmartTitleArtistQuery(Properties configuration, String propertyPrefix) {
		super();
		this.propertyPrefix = propertyPrefix;
		startup(configuration);
	}
	
	private LongArray exactSearch(LevenshteinIndex reverseIndex, String text) {
		final TermSet termSet = reverseIndex.search(text);
		if (termSet.next())
			return termSet.getPostings();
		return null;
	}
	
	@Override
	public void startup(Properties configuration) throws DynamicModuleException {
		maxTerms = Integer.parseInt(configuration
				.getProperty(propertyPrefix + ".max_terms"));
		minTerms = Integer.parseInt(configuration
				.getProperty(propertyPrefix + ".min_terms"));
		logger.debug("maxTerms {}", maxTerms);
		logger.debug("minTerms {}", minTerms);
	}
	
	@Override
	public void shutdown() {
		
	}
	
	@Override
	public LongArray search(LevenshteinIndex titleIndex, LevenshteinIndex artistIndex, Collection<String> titleTerms, Collection<String> artistTerms) {
		
		// sort title terms list (descending length)
		List<String> sortedTitleTerms = new ArrayList<>(titleTerms);
		Collections.sort(sortedTitleTerms, new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				return Integer.compare(o2.length(), o1.length());
			}
			
		});
		
		// keep longest terms only
		if (sortedTitleTerms.size() > maxTerms) {
			sortedTitleTerms = sortedTitleTerms.subList(0, maxTerms);
		}
		
		// title search
		final List<LongArray> titleTermsPostings = new ArrayList<>(sortedTitleTerms.size());
		for (String term : sortedTitleTerms) {
			final LongArray postings = exactSearch(titleIndex, term);
			if (null != postings && !postings.isEmpty()) {
				titleTermsPostings.add(postings);
			}
		}
		if (titleTermsPostings.isEmpty()) {
			return null;
		}
		
		// sort postings list (ascending length) to speed up intersections
		Collections.sort(titleTermsPostings, new Comparator<LongArray>() {

			@Override
			public int compare(LongArray o1, LongArray o2) {
				return Integer.compare(o1.size(), o2.size());
			}
			
		});
		
		// compute title terms piecewise intersection
		LongArray titlePostings = null;
		if (titleTermsPostings.size() > 2) {
			if (titleTermsPostings.size() > minTerms) {
				final List<List<LongArray>> postingsSublists = ListUtils
						.getSortedSublists(titleTermsPostings, minTerms);
				for (List<LongArray> postingsSublist : postingsSublists) {
					LongArray sublistIntersection = postingsSublist.get(0);
					for (int i = 1; i < postingsSublist.size(); i ++) {
						sublistIntersection = LongArray
								.intersection(sublistIntersection, postingsSublist.get(i));
					}
					titlePostings = null == titlePostings ? sublistIntersection :
						LongArray.union(titlePostings, sublistIntersection);
				}
			} else {
				titlePostings = titleTermsPostings.get(0);
				for (int i = 1; i < titleTermsPostings.size(); i ++) {
					titlePostings = LongArray
							.intersection(titlePostings, titleTermsPostings.get(i));
					if (null == titlePostings || titlePostings.isEmpty()) {
						return null;
					}
				}
			}
		} else if (titleTermsPostings.size() > 1) {
			titlePostings = LongArray
					.intersection(titleTermsPostings.get(0), titleTermsPostings.get(1));
		} else if (titleTermsPostings.size() > 0) {
			titlePostings = titleTermsPostings.get(0);
		}
		if (null == titlePostings || titlePostings.isEmpty()) {
			return null;
		}

		// artist search
		LongArray resultPostings = null;
		for (String term : artistTerms) {
			LongArray postings = exactSearch(artistIndex, term);
			// compute artist terms union of intersections with title postings
			if (null != postings && !postings.isEmpty()) {
				if (null == resultPostings) {
					resultPostings = LongArray.intersection(titlePostings, postings);
				} else {
					postings = LongArray.intersection(titlePostings, postings);
					resultPostings = LongArray.union(postings, resultPostings);
				}
			}
		}

		return resultPostings;
	}
	
}
