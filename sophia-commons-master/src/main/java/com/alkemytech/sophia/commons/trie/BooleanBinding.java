package com.alkemytech.sophia.commons.trie;

import com.alkemytech.sophia.commons.mmap.MemoryMap;
import com.alkemytech.sophia.commons.mmap.MemoryMapException;
import com.alkemytech.sophia.commons.util.SizeInBytes;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class BooleanBinding implements LeafBinding<Boolean> {
	
	@Override
	public int sizeOfLeaves() {
		return SizeInBytes.BYTE;
	}

	@Override
	public Boolean read(MemoryMap buffer) throws MemoryMapException {
		return (byte) 1 == buffer.get() ? true : null;
	}

	@Override
	public void write(MemoryMap buffer, Boolean leaf) throws MemoryMapException {
		buffer.put(null == leaf || !leaf ? (byte) 0 : (byte) 1);
	}

}
