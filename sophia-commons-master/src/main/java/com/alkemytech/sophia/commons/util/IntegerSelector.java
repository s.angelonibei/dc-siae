package com.alkemytech.sophia.commons.util;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface IntegerSelector {

	public boolean select(int value);
	
}
