package com.alkemytech.sophia.commons.score;

import com.alkemytech.sophia.commons.util.TokenizableText;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class TitleScore implements TextScore {

	private final TextScore plainTextScore = new LevenshteinScore();
	private final TextScore wordWiseScore = new WordWiseLevenshteinScore();
	
	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.commons.score.TextScore#getScore(com.alkemytech.sophia.commons.util.TokenizableText, com.alkemytech.sophia.commons.util.TokenizableText)
	 */
	@Override
	public double getScore(TokenizableText pattern, TokenizableText target) {
		return 0.75 * plainTextScore.getScore(pattern, target) +
				0.25 * wordWiseScore.getScore(pattern, target);
	}

}
