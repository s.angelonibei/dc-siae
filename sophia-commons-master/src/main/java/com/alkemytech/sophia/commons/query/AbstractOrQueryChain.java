package com.alkemytech.sophia.commons.query;

import java.util.Collection;

import com.alkemytech.sophia.commons.index.LevenshteinIndex;
import com.alkemytech.sophia.commons.util.LongArray;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class AbstractOrQueryChain extends AbstractQueryChain {

	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.commons.query.IndexQueryChain#search(com.alkemytech.sophia.commons.util.LongArray, com.alkemytech.sophia.commons.index.LevenshteinIndex, java.util.Collection)
	 */
	@Override
	public LongArray search(LongArray postingsChain, LevenshteinIndex index, Collection<String> terms) {

		// nothing to search
		if (null == terms || terms.isEmpty()) {
			return null;
		}

		// search(es)
		LongArray resultPostings = null;
		for (String term : terms) {
			LongArray termPostings = searchTerm(index, term);
			// union of intersections with postings chain
			if (!LongArray.isNullOrEmpty(termPostings)) {
				if (null != postingsChain) {
					termPostings = LongArray.intersection(termPostings, postingsChain);
				}
				if (null == resultPostings) {
					resultPostings = termPostings;
				} else {
					resultPostings = LongArray.union(resultPostings, termPostings);
				}
			}
		}
		
		// return result(s)
		return LongArray.isNullOrEmpty(resultPostings) ? null : resultPostings;
	}
	
}
