package com.alkemytech.sophia.commons.trie;

import java.util.ArrayList;
import java.util.List;

import com.alkemytech.sophia.commons.mmap.MemoryMap;
import com.alkemytech.sophia.commons.mmap.MemoryMapException;
import com.alkemytech.sophia.commons.util.SizeInBytes;

/**
 * Fixed-size generic leaf value trie supporting approximate string matching on keys.
 * 
 * @deprecated use TrieEx&lt;T&gt; class instead
 * @see http://stevehanov.ca/blog/index.php?id=114
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@Deprecated
public class Trie<T> implements ApproximateTrie<T> {

	public static <X> Trie<X> wrap(MemoryMap memoryMap, LeafBinding<X> leafBinding) throws MemoryMapException {
		return new Trie<X>(memoryMap, leafBinding);
	}
	
	private static final long SIZE_OFFSET = 0L;
	private static final long DATA_OFFSET = SizeInBytes.LONG;

	private static class Node<T> {

		public static final long BYTES = SizeInBytes.CHAR + SizeInBytes.LONG;

		public final long offset;

		public final char ch;
		public T leaf;
		public long children; // offset of next child ref (0 end-of-list)

		public Node(long offset, char ch, T leaf, long children) {
			super();
			this.offset = offset;
			this.ch = ch; 
			this.leaf = leaf;
			this.children = children;
		}
		
	}

	private static class NodeRef {
		
		public static final long BYTES = SizeInBytes.LONG + SizeInBytes.LONG;
		
		public final long offset;
		
		public final long child; // offset of child
		public final long next; // offset of next child ref (0 end-of-list)
		
		public NodeRef(long offset, long child, long next) {
			super();
			this.offset = offset;
			this.child = child;
			this.next = next;
		}

	}

	private final MemoryMap memoryMap;
	private final LeafBinding<T> leafBinding;
		
	public Trie(MemoryMap memoryMap, LeafBinding<T> leafBinding) throws MemoryMapException {
		super();
		this.memoryMap = memoryMap;
		this.leafBinding = leafBinding;
		// initialize root node
		if (0L == memoryMap.getSizeOnDisk() &&
				!memoryMap.isReadOnly()) {
			writeNode(new Node<T>(DATA_OFFSET, '\0', null, 0L));
			writeSize(DATA_OFFSET + Node.BYTES + leafBinding.sizeOfLeaves());
		}
	}
	
	private void writeSize(long size) throws MemoryMapException {
		memoryMap.position(SIZE_OFFSET)
			.putLong(size);
	}

	private long getSizeAndAdd(long delta) throws MemoryMapException {
		final long size = memoryMap.position(SIZE_OFFSET).getLong();
		memoryMap.position(SIZE_OFFSET).putLong(size + delta);
		return size;
	}
	
	private Node<T> readNode(long offset) throws MemoryMapException {
		memoryMap.position(offset);
		return new Node<T>(offset,
				memoryMap.getChar(),
				leafBinding.read(memoryMap),
				memoryMap.getLong());
	}
	
	private Node<T> writeNode(Node<T> node) throws MemoryMapException {
		memoryMap.position(node.offset);
		memoryMap.putChar(node.ch);
		leafBinding.write(memoryMap, node.leaf);
		memoryMap.putLong(node.children);
		return node;
	}

	private NodeRef readNodeRef(long offset) throws MemoryMapException {
		memoryMap.position(offset);
		return new NodeRef(offset,
				memoryMap.getLong(),
				memoryMap.getLong());
	}
	
	private NodeRef writeNodeRef(NodeRef nodeRef) throws MemoryMapException {
		memoryMap.position(nodeRef.offset);
		memoryMap.putLong(nodeRef.child);
		memoryMap.putLong(nodeRef.next);
		return nodeRef;
	}
	
	@Override
	public T upsert(String text, T leaf) throws MemoryMapException {
		return upsert(text.toCharArray(), leaf);
	}

	private T upsert(char[] text, T leaf) throws MemoryMapException {
		Node<T> node = readNode(DATA_OFFSET);
		for (char ch : text) {
			boolean found = false;
			for (long offset = node.children; offset > 0L; ) {
				final NodeRef childRef = readNodeRef(offset);
				final Node<T> child = readNode(childRef.child);
				if (child.ch == ch) {
					node = child;
					found = true;
					break;
				}
				offset = childRef.next;
			}
			if (!found) {
				long size = getSizeAndAdd(Node.BYTES + leafBinding.sizeOfLeaves() + NodeRef.BYTES);
				final Node<T> child = new Node<T>(size, ch, null, 0L);
				writeNode(child);
				size += Node.BYTES + leafBinding.sizeOfLeaves();
				final NodeRef childRef = new NodeRef(size, child.offset, node.children);
				writeNodeRef(childRef);
				node.children = childRef.offset;
				writeNode(node);
				node = child;
			}
		}
		final T existing = node.leaf;
		if (node.leaf != leaf) {
			node.leaf = leaf;
			writeNode(node);
		}
		return existing;
	}
	
	@Override
	public T delete(String text) throws MemoryMapException {
		return delete(text.toCharArray());
	}
	
	private T delete(char[] text) throws MemoryMapException {
		Node<T> node = readNode(DATA_OFFSET);
		for (char ch : text) {
			boolean found = false;
			for (long offset = node.children; offset > 0L; ) {
				final NodeRef childRef = readNodeRef(offset);
				final Node<T> child = readNode(childRef.child);
				if (child.ch == ch) {
					node = child;
					found = true;
					break;
				}
				offset = childRef.next;
			}
			if (!found) {
				return null;
			}
		}
		final T existing = node.leaf;
		if (null != node.leaf) {
			node.leaf = null;
			writeNode(node);
		}
		return existing;
	}
	
	@Override
	public T find(String text) throws MemoryMapException {
		Node<T> node = readNode(DATA_OFFSET);
		for (char ch : text.toCharArray()) {
			boolean found = false;
			for (long offset = node.children; offset > 0L; ) {
				final NodeRef childRef = readNodeRef(offset);
				final Node<T> child = readNode(childRef.child);
				if (child.ch == ch) {
					node = child;
					found = true;
					break;
				}
				offset = childRef.next;
			}
			if (!found) {
				return null;
			}
		}
		return node.leaf;
	}
	
	@Override
	public List<ScoredPath<T>> search(String text, int maxDistance) throws MemoryMapException {
		return search(text.toCharArray(), maxDistance);
	}

	private List<ScoredPath<T>> search(char[] text, int maxDistance) throws MemoryMapException {
		final List<ScoredPath<T>> results = new ArrayList<>();
		Node<T> node = readNode(DATA_OFFSET);
		if (0L == node.children) {
			return results;
		}
		final int columns = 1 + text.length;
		final int[] currentRow = new int[columns];
		for (int i = 0; i < columns; i++) {
			currentRow[i] = i;
		}
		for (long offset = node.children; offset > 0L; ) {
			final NodeRef childRef = readNodeRef(offset);
			final Node<T> child = readNode(childRef.child);
			search(text, maxDistance, child, currentRow, "", results);
			offset = childRef.next;
		}
		return results;
	}

	private void search(char[] text, int maxDistance, Node<T> node, int[] previousRow, String path, List<ScoredPath<T>> results) throws MemoryMapException {
		path += node.ch;
		final int columns = 1 + text.length;
		final int[] currentRow = new int[columns];
		currentRow[0] = previousRow[0] + 1;
		int minDistance = currentRow[0];
		for (int i = 1; i < columns; i++) {
			final int insertDistance = currentRow[i-1] + 1;
			final int deleteDistance = previousRow[i] + 1;
			final int replaceDistance = (node.ch == text[i-1] ? previousRow[i-1] : previousRow[i-1] + 1);
			currentRow[i] = Math.min(Math.min(insertDistance, deleteDistance), replaceDistance);
			minDistance = (minDistance > currentRow[i] ? currentRow[i] : minDistance);
		}
		if (null != node.leaf && currentRow[columns-1] <= maxDistance) {
			results.add(new ScoredPath<T>(currentRow[columns-1], path, node.leaf));
		}
		if (0L == node.children) {
			return;
		} else if (minDistance <= maxDistance) {
			for (long offset = node.children; offset > 0L; ) {
				final NodeRef childRef = readNodeRef(offset);
				final Node<T> child = readNode(childRef.child);
				search(text, maxDistance, child, currentRow, path, results);
				offset = childRef.next;
			}
		}
	}
	
	@Override
	public Trie<T> clear() {
		memoryMap.clear();
		writeNode(new Node<T>(DATA_OFFSET, '\0', null, 0L));
		writeSize(DATA_OFFSET + Node.BYTES + leafBinding.sizeOfLeaves());
		return this;
	}

	@Override
	public Trie<T> garbage() {
		memoryMap.garbage();
		return this;
	}
	
}
