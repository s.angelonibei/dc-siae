package com.alkemytech.sophia.commons.util;

import java.util.ArrayList;
import java.util.List;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class ListUtils {

	public static <T> List<List<T>> getSortedSublists(List<T> list, int size) {
		if (size < 1) {
			throw new IllegalArgumentException(String
					.format("sublist size too small: %d", size));
		}
		if (list.size() < size) {
			throw new IllegalArgumentException(String
					.format("list size smaller than sublist size: %d < %d", list.size(), size));
		}		
		final List<List<T>> results = new ArrayList<List<T>>();
		if (list.size() == size) {
			results.add(new ArrayList<T>(list));
		} else if (1 == size) {
			for (T element : list) {
				final List<T> result = new ArrayList<T>(1);
				result.add(element);
				results.add(result);
			}
		} else {
			final int delta = list.size() - size;
			for (int i = 0; i <= delta; i ++) {
				for (List<T> sublist : getSortedSublists(list.subList(i + 1, list.size()), size - 1)) {
					final List<T> result = new ArrayList<T>(size);
					result.add(list.get(i));
					result.addAll(sublist);
					results.add(result);
				}
			}
		}
		return results;
	}
	
//	public static void main(String[] args) {
//		
//		List<String> list = new ArrayList<>();
//		list.add("la");
//		list.add("vita");
//		list.add("e");
//		list.add("adesso");
//		
//		List<List<String>> sublists = getSortedSublists(list, 3);
//		for (List<String> sublist : sublists)
//			System.out.println(sublist.toString());
//		
//	}
		
}
