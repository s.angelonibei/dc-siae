package com.alkemytech.sophia.commons.trie;

import com.alkemytech.sophia.commons.mmap.MemoryMap;
import com.alkemytech.sophia.commons.mmap.MemoryMapException;
import com.alkemytech.sophia.commons.util.SizeInBytes;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class LongBinding implements LeafBinding<Long> {

	private final long nullValue;
	
	public LongBinding(long nullValue) {
		super();
		this.nullValue = nullValue;
	}
	
	@Override
	public int sizeOfLeaves() {
		return SizeInBytes.LONG;
	}

	@Override
	public Long read(MemoryMap buffer) throws MemoryMapException {
		final long leaf = buffer.getLong();
		return nullValue == leaf ? null : leaf;
	}

	@Override
	public void write(MemoryMap buffer, Long leaf) throws MemoryMapException {
		buffer.putLong(null == leaf ? nullValue : leaf);
	}

}
