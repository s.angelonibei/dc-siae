package com.alkemytech.sophia.commons.trie;

import java.nio.charset.Charset;
import java.util.Arrays;

import com.alkemytech.sophia.commons.mmap.MemoryMap;
import com.alkemytech.sophia.commons.mmap.MemoryMapException;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class FixedLengthStringBinding implements LeafBinding<String> {

	private final int length;
	private final Charset charset;
	private final byte[] emptyLeaf;
	
	public FixedLengthStringBinding(int length, Charset charset) {
		super();
		this.length = length;
		this.charset = charset;
		emptyLeaf = new byte[length];
		Arrays.fill(emptyLeaf, (byte) 0);
	}
	
	@Override
	public int sizeOfLeaves() {
		return length;
	}

	@Override
	public String read(MemoryMap buffer) throws MemoryMapException {
		final byte[] bytes = new byte[length];
		buffer.get(bytes, 0, length);
		return (byte) 0 == bytes[0] ?
				null : new String(bytes, charset);
	}

	@Override
	public void write(MemoryMap buffer, String leaf) throws MemoryMapException {
		if (null == leaf) {
			buffer.put(emptyLeaf, 0, length);			
		} else {
			final byte[] bytes = leaf.getBytes(charset);
			if (length != bytes.length) {
				throw new IllegalArgumentException("invalid leaf length " + bytes.length);
			}
			buffer.put(bytes, 0, length);
		}
	}

}
