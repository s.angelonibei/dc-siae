package com.alkemytech.sophia.commons.sqs;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface MessageDeduplicator {

	public boolean deduplicate(String uuid, String queueName);
	
}
