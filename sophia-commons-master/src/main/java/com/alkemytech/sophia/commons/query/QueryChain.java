package com.alkemytech.sophia.commons.query;

import java.util.Collection;

import com.alkemytech.sophia.commons.index.LevenshteinIndex;
import com.alkemytech.sophia.commons.util.LongArray;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface QueryChain {

	/**
	 * Performs a query based on a collection of terms.<br/>
	 * <br/>
	 * First query in chain will be invoked with a null <code>postingsChain</code> argument.
	 * 
	 * @param postingsChain array of document id(s) to be intersected with query results, or
	 *        null for no intersection
	 * @param index the index to be queried
	 * @param titleTerms the normalized terms collection to be used to query index
	 * @return array of matching document id(s)
	 */
	public LongArray search(LongArray postingsChain, LevenshteinIndex index, Collection<String> terms);

}
