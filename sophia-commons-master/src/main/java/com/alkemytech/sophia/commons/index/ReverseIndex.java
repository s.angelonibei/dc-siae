package com.alkemytech.sophia.commons.index;

import java.util.Collection;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface ReverseIndex extends LevenshteinIndex {

	/**
	 * Used to update index contents.
	 */
	public interface Editor {
		
		/**
		 * Discard all previously added terms.
		 */
		public void rewind() throws LevenshteinIndexException;
		
		/**
		 * Add a <code>term</code> coming from document identified by <code>id</code>.
		 */
		public void add(String term, long id) throws LevenshteinIndexException;

		/**
		 * Add a collection of <code>term</code>(s) from document identified by <code>id</code>.
		 */
		public void addAll(Collection<String> terms, long id) throws LevenshteinIndexException;
		
		/**
		 * Write changes to index.
		 */
		public void commit() throws LevenshteinIndexException;

	}

	/**
	 * Edit index.
	 */
	public Editor edit() throws LevenshteinIndexException;

}
