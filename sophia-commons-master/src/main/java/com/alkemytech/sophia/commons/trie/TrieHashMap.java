package com.alkemytech.sophia.commons.trie;

import java.nio.charset.StandardCharsets;

import com.alkemytech.sophia.commons.mmap.MemoryMap;
import com.alkemytech.sophia.commons.mmap.MemoryMapException;
import com.alkemytech.sophia.commons.util.SizeInBytes;

/**
 * Fixed-size generic leaf value trie implementation using a 1M buckets hash map.
 * 
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class TrieHashMap<T> implements ExactTrie<T> {

	public static <X> TrieHashMap<X> wrap(MemoryMap memoryMap, LeafBinding<X> leafBinding) throws MemoryMapException {
		return new TrieHashMap<X>(memoryMap, leafBinding);
	}
	
	private static final long SIZE_OFFSET = 0L;
	private static final long DATA_OFFSET = SizeInBytes.LONG;
	private static final char[] HEX_DIGITS = "0123456789abcdef".toCharArray();
	
	private static class Node {

		public static final long BYTES = SizeInBytes.CHAR + SizeInBytes.LONG + SizeInBytes.LONG + SizeInBytes.LONG;

		public final long offset;
		public final char ch;
		
		public long bucket; // offset of first bucket entry (0 end-of-list)
		public long children; // offset of first child (0 end-of-list)
		public long next; // offset of next brother (0 end-of-list)

		public Node(long offset, char ch, long bucket, long children, long next) {
			super();
			this.offset = offset;
			this.ch = ch; 
			this.bucket = bucket;
			this.children = children;
			this.next = next;
		}
		
	}
	
	private static class Bucket<T> {

		public final long offset;
		public final byte[] text;
		
		public T leaf;
		public long next; // offset of next bucket element (0 end-of-list)

		public Bucket(long offset, byte[] text, T leaf, long next) {
			super();
			this.offset = offset;
			this.text = text; 
			this.leaf = leaf;
			this.next = next;
		}
		
		public Bucket(long offset, String text, T leaf, long next) {
			this(offset, text.getBytes(StandardCharsets.UTF_8), leaf, next);
		}
		
		public static int sizeOf(String text) {
			return SizeInBytes.SHORT + text.length() + SizeInBytes.LONG;
		}
		
		public String text() {
			return new String(text, StandardCharsets.UTF_8);
		}
		
	}

	private final MemoryMap memoryMap;
	private final LeafBinding<T> leafBinding;
		
	public TrieHashMap(MemoryMap memoryMap, LeafBinding<T> leafBinding) throws MemoryMapException {
		super();
		this.memoryMap = memoryMap;
		this.leafBinding = leafBinding;
		// initialize root node
		if (0L == memoryMap.getSizeOnDisk() &&
				!memoryMap.isReadOnly()) {
			writeNode(new Node(DATA_OFFSET, '\0', 0L, 0L, 0L));
			writeSize(DATA_OFFSET + Node.BYTES);
		}
	}
	
	private void writeSize(long size) throws MemoryMapException {
		memoryMap.position(SIZE_OFFSET)
			.putLong(size);
	}

	private long getSizeAndAdd(long delta) throws MemoryMapException {
		final long size = memoryMap.position(SIZE_OFFSET).getLong();
		memoryMap.position(SIZE_OFFSET).putLong(size + delta);
		return size;
	}
	
	private Node readNode(long offset) throws MemoryMapException {
		memoryMap.position(offset);
		return new Node(offset,
				memoryMap.getChar(),
				memoryMap.getLong(),
				memoryMap.getLong(),
				memoryMap.getLong());
	}
	
	private Node writeNode(Node node) throws MemoryMapException {
		memoryMap.position(node.offset);
		memoryMap.putChar(node.ch);
		memoryMap.putLong(node.bucket);
		memoryMap.putLong(node.children);
		memoryMap.putLong(node.next);
		return node;
	}

	private Bucket<T> readBucket(long offset) throws MemoryMapException {
		memoryMap.position(offset);
		final short length = memoryMap.getShort();
		final byte[] text = new byte[length];
		memoryMap.get(text);
		return new Bucket<T>(offset, text,
				leafBinding.read(memoryMap),
				memoryMap.getLong());
	}
	
	private Bucket<T> writeBucket(Bucket<T> bucket) throws MemoryMapException {
		memoryMap.position(bucket.offset);
		memoryMap.putShort((short) bucket.text.length);
		memoryMap.put(bucket.text);
		leafBinding.write(memoryMap, bucket.leaf);
		memoryMap.putLong(bucket.next);
		return bucket;
	}

	private char[] toHash(String text) {
		final int code = text.hashCode();
		final char[] hash = new char[5];
		hash[0] = HEX_DIGITS[code & 0x0f];
		hash[1] = HEX_DIGITS[(code >> 4) & 0x0f];
		hash[2] = HEX_DIGITS[(code >> 8) & 0x0f];
		hash[3] = HEX_DIGITS[(code >> 12) & 0x0f];
		hash[4] = HEX_DIGITS[(code >> 16) & 0x0f];
		return hash;
	}
	
	@Override
	public T upsert(String text, T leaf) throws MemoryMapException {
		Node node = readNode(DATA_OFFSET);
		for (char ch : toHash(text)) {
			boolean found = false;
			for (long offset = node.children; offset > 0L; ) {
				final Node child = readNode(offset);
				if (child.ch == ch) {
					node = child;
					found = true;
					break;
				}
				offset = child.next;
			}
			if (!found) {
				final long size = getSizeAndAdd(Node.BYTES);
				final Node child = new Node(size, ch, 0L, 0L, node.children);
				writeNode(child);
				node.children = child.offset;
				writeNode(node);
				node = child;
			}
		}
		for (long offset = node.bucket; offset > 0L; ) {
			final Bucket<T> bucket = readBucket(offset);
			if (text.equals(bucket.text())) {
				final T existing = bucket.leaf;
				if (bucket.leaf != leaf) {
					bucket.leaf = leaf;
					writeBucket(bucket);
				}
				return existing;
			}
			offset = bucket.next;
		}
		final long size = getSizeAndAdd(Bucket.sizeOf(text) + leafBinding.sizeOfLeaves());
		final Bucket<T> bucket = new Bucket<T>(size, text, leaf, node.bucket);
		writeBucket(bucket);
		node.bucket = bucket.offset;
		writeNode(node);
		return null;
	}
	
	@Override
	public T delete(String text) throws MemoryMapException {
		Node node = readNode(DATA_OFFSET);
		for (char ch : toHash(text)) {
			boolean found = false;
			for (long offset = node.children; offset > 0L; ) {
				final Node child = readNode(offset);
				if (child.ch == ch) {
					node = child;
					found = true;
					break;
				}
				offset = child.next;
			}
			if (!found) {
				return null;
			}
		}
		for (long offset = node.bucket; offset > 0L; ) {
			final Bucket<T> bucket = readBucket(offset);
			if (text.equals(bucket.text())) {
				final T existing = bucket.leaf;
				if (null != bucket.leaf) {
					bucket.leaf = null;
					writeBucket(bucket);
				}
				return existing;
			}
			offset = bucket.next;
		}
		return null;
	}
	
	@Override
	public T find(String text) throws MemoryMapException {
		Node node = readNode(DATA_OFFSET);
		for (char ch : toHash(text)) {
			boolean found = false;
			for (long offset = node.children; offset > 0L; ) {
				final Node child = readNode(offset);
				if (child.ch == ch) {
					node = child;
					found = true;
					break;
				}
				offset = child.next;
			}
			if (!found) {
				return null;
			}
		}
		for (long offset = node.bucket; offset > 0L; ) {
			final Bucket<T> bucket = readBucket(offset);
			if (text.equals(bucket.text())) {
				return bucket.leaf;
			}
			offset = bucket.next;
		}
		return null;
	}
	
	@Override
	public TrieHashMap<T> clear() {
		memoryMap.clear();
		writeNode(new Node(DATA_OFFSET, '\0', 0L, 0L, 0L));
		writeSize(DATA_OFFSET + Node.BYTES);
		return this;
	}

	@Override
	public TrieHashMap<T> garbage() {
		memoryMap.garbage();
		return this;
	}
	
}
