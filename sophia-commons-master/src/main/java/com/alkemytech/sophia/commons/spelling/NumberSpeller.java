package com.alkemytech.sophia.commons.spelling;

import java.util.HashSet;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.regex.RegExTransformer;
import com.alkemytech.sophia.commons.text.LetterCase;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.google.common.base.Strings;
import com.ibm.icu.text.RuleBasedNumberFormat;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class NumberSpeller {
	
	public static String spell(long number, String language) {
		return spell(number, Locale.forLanguageTag(language));
	}
		
	public static String spell(long number, Locale locale) {
		return new RuleBasedNumberFormat(locale,
				RuleBasedNumberFormat.SPELLOUT).format(number);
	}

	private final Logger logger = LoggerFactory.getLogger(NumberSpeller.class);

	private final LetterCase letterCase;
	private final Pattern pattern;
	private final Locale[] locales;
	private final RegExTransformer transformer;

	public NumberSpeller(Properties configuration, String propertyPrefix) {
		super();
		this.pattern = Pattern.compile(configuration
				.getProperty(propertyPrefix + ".regex"));
		final String[] languages = TextUtils.split(configuration
				.getProperty(propertyPrefix + ".languages"), ",");
		this.locales = new Locale[languages.length];
		for (int i = 0; i < languages.length; i ++) {
			this.locales[i] = Locale.forLanguageTag(languages[i]);
		}
		this.letterCase = LetterCase.parse(configuration
				.getProperty(propertyPrefix + ".letter_case"));
		this.transformer = new RegExTransformer(TextUtils.split(configuration
				.getProperty(propertyPrefix + ".transformer"), ","));
	}

	public Set<String> spell(String string) {
		return spell(string, locales);
	}

	public Set<String> spell(String text, Locale...locales) {
		final HashSet<String> results = new HashSet<>();
		final HashSet<String> numbers = new HashSet<>();
		final Matcher matcher = pattern.matcher(text);
		while (matcher.find()) {
			numbers.add(matcher.group());
		}
		if (!numbers.isEmpty()) {
			for (Locale locale : locales) {
				String result = text;
				for (String number : numbers) {
					try {
						final long value = Long.parseLong(number);
						if (value >= 1700L && value < 2000L &&
								(locale == Locale.US || locale == Locale.UK)) { // english date
							final String hi = number.substring(0, 2);
							result = result.replace(hi, transformer.transform(letterCase
									.enforce(spell(Integer.parseInt(hi), locale))));
							final String lo = number.substring(2);
							result = result.replace(lo, transformer.transform(letterCase
									.enforce(spell(Integer.parseInt(lo), locale))));
						}
						result = result.replace(number, transformer.transform(letterCase
								.enforce(spell(value, locale))));
					} catch (NumberFormatException e) {
						logger.error("spell", e);
					}
				}
				if (!Strings.isNullOrEmpty(result)) {
					results.add(result);
				}
			}
		}
		return results;
	}
	
//	public static void main(String[] args) throws Exception {
//		System.out.println(spell(12345678, "it-IT"));
//		System.out.println(spell(12345678, "en-US"));
//	}
	
}
