package com.alkemytech.sophia.commons.nosql;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface NoSqlEntity {
	
	public String getPrimaryKey() throws NoSqlException;
	
	public String getSecondaryKey() throws NoSqlException;

}
