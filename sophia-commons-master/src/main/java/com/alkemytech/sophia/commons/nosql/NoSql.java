package com.alkemytech.sophia.commons.nosql;

import java.io.Closeable;
import java.io.File;
import java.util.Comparator;

/**
 * A No-SQL embedded database.
 * 
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface NoSql<T extends NoSqlEntity> extends Closeable {
	
	/**
	 * Callback for iteration on record(s).
	 */
	public static interface Selector<V> {
		public void select(V value) throws NoSqlException;
	}

	/**
	 * Record converter.
	 */
	public static interface Converter<T extends NoSqlEntity, V> {
		public V convert(T entity) throws NoSqlException;
	}

	/**
	 * No-SQL database home folder.
	 */
	public File getHomeFolder() throws NoSqlException;
	
	/**
	 * Read only mode.
	 */
	public boolean isReadOnly() throws NoSqlException;
	
	/**
	 * Increment and get primary key sequence.
	 */
	public long nextSequenceId() throws NoSqlException;

	/**
	 * Number of record(s).
	 */
	public long count() throws NoSqlException;
	
	/**
	 * Get record by primary key.
	 */
	public T getByPrimaryKey(String key) throws NoSqlException;

	/**
	 * Get record by secondary key (optional operation).
	 */
	public T getBySecondaryKey(String key) throws NoSqlException;

	/**
	 * Insert or update a record.
	 */
	public T upsert(T value) throws NoSqlException;

	/**
	 * Delete a record by value.
	 */
	public boolean delete(T value) throws NoSqlException;

	/**
	 * Delete a record by primary key.
	 */
	public boolean deleteByPrimaryKey(String key) throws NoSqlException;

	/**
	 * Iterate over record(s).
	 */
	public void select(Selector<T> selector) throws NoSqlException;

	/**
	 * Iterate over record(s) sorted in <code>comparator</code> order.
	 */
	public void select(Selector<T> selector, Comparator<T> comparator) throws NoSqlException;

	/**
	 * Iterate over record(s) converted by <code>converter</code> and sorted in <code>comparator</code> order.
	 */
	public <V> void select(Selector<V> selector, Converter<T, V> converter, Comparator<V> comparator) throws NoSqlException;
	
	/**
	 * Truncate No-SQL database.
	 */
	public void truncate() throws NoSqlException;	

	/**
	 * Optimize size of underlying storage.
	 */
	public void defrag() throws NoSqlException;
	
}
