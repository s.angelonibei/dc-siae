package com.alkemytech.sophia.commons.io;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

import com.google.common.base.Strings;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class CompressionAwareFileInputStream extends InputStream {

	private final InputStream stream;
	
	public CompressionAwareFileInputStream(File file) throws IOException {
		this(file, null);
	}
	
	public CompressionAwareFileInputStream(File file, String compression) throws IOException {
		super();
		if (Strings.isNullOrEmpty(compression) ||
				"guess".equalsIgnoreCase(compression)) {
			final String filename = file.getName().toLowerCase();
			final int dot = filename.lastIndexOf('.');
			if (-1 == dot) {
				compression = "none";
			} else {
				compression = filename.substring(1 + dot);
				if (!"gz".equals(compression) &&
						!"gzip".equals(compression)) {
					compression = "none";
				}
			}
		}
		if ("gz".equalsIgnoreCase(compression) ||
				"gzip".equalsIgnoreCase(compression)) {
			this.stream = new GZIPInputStream(new FileInputStream(file), 65536);
		} else if ("none".equalsIgnoreCase(compression)) {
			this.stream = new BufferedInputStream(new FileInputStream(file), 65536);
		} else {
			throw new IllegalArgumentException(String.format("unsupported compression: %s", compression));
		}
	}

	public int read() throws IOException {
		return stream.read();
	}

	public int read(byte[] b) throws IOException {
		return stream.read(b);
	}

	public int read(byte[] b, int off, int len) throws IOException {
		return stream.read(b, off, len);
	}

	public long skip(long n) throws IOException {
		return stream.skip(n);
	}

	public int available() throws IOException {
		return stream.available();
	}

	public void close() throws IOException {
		stream.close();
	}

	public void mark(int readlimit) {
		stream.mark(readlimit);
	}

	public void reset() throws IOException {
		stream.reset();
	}

	public boolean markSupported() {
		return stream.markSupported();
	}
	
}
