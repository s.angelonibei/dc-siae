package com.alkemytech.sophia.commons.text;

import com.google.common.base.Strings;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class LetterCase {

	public static final LetterCase parse(String text) {
		if (Strings.isNullOrEmpty(text) ||
				"mixed".equalsIgnoreCase(text)) {
			return LetterCase.mixed();
		} else if ("lower".equalsIgnoreCase(text)) {
			return LetterCase.lower();
		} else if ("upper".equalsIgnoreCase(text)) {
			return LetterCase.upper();
		}
		throw new IllegalArgumentException(String
				.format("unknown case \"%s\"", text));
	}
	
	public static final LetterCase mixed() {
		return  new LetterCase() {
			@Override
			public String enforce(String text) {
				return text;
			}
		};
	}

	public static final LetterCase lower() {
		return new LetterCase() {
			@Override
			public String enforce(String text) {
				return text.toLowerCase();
			}
			
		};
	}
		
	public static final LetterCase upper() {
		return new LetterCase() {
			
			@Override
			public String enforce(String text) {
				return text.toUpperCase();
			}
			
		};		
	}
	
	private LetterCase() {
		super();
	}
	
	public abstract String enforce(String text);

}
