package com.alkemytech.sophia.commons.mmap;

import java.io.File;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.nio.charset.Charset;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.util.LongArray;
import com.alkemytech.sophia.commons.util.TextUtils;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public final class ConcurrentMemoryMap implements MemoryMap {
	
	public static ConcurrentMemoryMap create(File folder, int pageSize) throws MemoryMapException {
		return new ConcurrentMemoryMap(folder, pageSize, false).deleteAllFiles(0);
	}
	
	public static ConcurrentMemoryMap open(File folder, int pageSize, boolean readOnly) throws MemoryMapException {
		return new ConcurrentMemoryMap(folder, pageSize, readOnly).scanFolder();
	}

	/**
	 * Thread safe duplicate of memory mapped byte buffer.
	 */
	private static class ThreadLocalBuffer extends ThreadLocal<MappedByteBuffer> {
		
	    private final MappedByteBuffer buffer;
	    
	    private ThreadLocalBuffer(MappedByteBuffer buffer) {
	        this.buffer = buffer;
	    }
	 
	    @Override
	    protected synchronized MappedByteBuffer initialValue() {
	        return (MappedByteBuffer) buffer.duplicate();
	    }
	    
	}
	
	/**
	 * Internal state needed for get/put operations.
	 */
	private class Context {

		private final byte[] codecBuffer;
		
		private long position;

		private Context() {
			super();
			codecBuffer = new byte[8];
			position = 0;
		}
		
		private void put(byte[] src, int offset, int length) throws MemoryMapException {
			final int index = (int) (position % (long) pageSize);
			final int remaining = pageSize - index;
			if (remaining >= length) {
				final MappedByteBuffer buffer = getBuffer(position).get();
				buffer.position(index);
				buffer.put(src, offset, length);
				position += length;
			} else {
				put(src, offset, remaining);
				put(src, offset + remaining, length - remaining);
			}
		}

		private void put(byte value) throws MemoryMapException {
			codecBuffer[0] = value;
			put(codecBuffer, 0, 1);
		}

		private void putChar(char value) throws MemoryMapException {
			codecBuffer[0] = (byte) ((value >> 8) & 0xff);
			codecBuffer[1] = (byte) (value & 0xff);
			put(codecBuffer, 0, 2);
		}
		
		private void putShort(short value) throws MemoryMapException {
			codecBuffer[0] = (byte) ((value >> 8) & 0xff);
			codecBuffer[1] = (byte) (value & 0xff);
			put(codecBuffer, 0, 2);
		}

		private void putInt(int value) throws MemoryMapException {
			codecBuffer[0] = (byte) ((value >> 24) & 0xff);
			codecBuffer[1] = (byte) ((value >> 16) & 0xff);
			codecBuffer[2] = (byte) ((value >> 8) & 0xff);
			codecBuffer[3] = (byte) (value & 0xff);
			put(codecBuffer, 0, 4);
		}

		private void putLong(long value) throws MemoryMapException {
			codecBuffer[0] = (byte) ((value >> 56) & 0xffL);
			codecBuffer[1] = (byte) ((value >> 48) & 0xffL);
			codecBuffer[2] = (byte) ((value >> 40) & 0xffL);
			codecBuffer[3] = (byte) ((value >> 32) & 0xffL);
			codecBuffer[4] = (byte) ((value >> 24) & 0xffL);
			codecBuffer[5] = (byte) ((value >> 16) & 0xffL);
			codecBuffer[6] = (byte) ((value >> 8) & 0xffL);
			codecBuffer[7] = (byte) (value & 0xffL);
			put(codecBuffer, 0, 8);
		}

		private void putByteArray(byte[] value) throws MemoryMapException {
			if (null == value) {
				putInt(-1);
			} else {
				putInt(value.length);
				if (value.length > 0)
					put(value, 0, value.length);
			}
		}

		private void putString(String value, String charsetName) throws MemoryMapException, UnsupportedEncodingException {
			putByteArray(null == value ? null : value.getBytes(charsetName));
		}

		private void putString(String value, Charset charset) throws MemoryMapException {
			putByteArray(null == value ? null : value.getBytes(charset));
		}

		private void putLongArray(LongArray value) throws MemoryMapException {
			final int size = value.size();
			putInt(size);
			final long[] array = value.array();
			for (int i = 0; i < size; i ++)
				putLong(array[i]);
		}
		
		private void get(byte[] dst, int offset, int length) throws MemoryMapException {
			final int index = (int) (position % (long) pageSize);
			final int remaining = pageSize - index;
			if (remaining >= length) {
				final MappedByteBuffer buffer = getBuffer(position).get();
				buffer.position(index);
				buffer.get(dst, offset, length);
				position += length;
			} else {
				get(dst, offset, remaining);
				get(dst, offset + remaining, length - remaining);
			}
		}

		private byte get() throws MemoryMapException {
			get(codecBuffer, 0, 1);
			return codecBuffer[0];
		}
		
		private char getChar() throws MemoryMapException {
			get(codecBuffer, 0, 2);
			final byte b0 = codecBuffer[0];
			final byte b1 = codecBuffer[1];
			return (char) (((b0 & 0xff) << 8) | (b1 & 0xff));
		}
		
		private short getShort() throws MemoryMapException {
			get(codecBuffer, 0, 2);
			final byte b0 = codecBuffer[0];
			final byte b1 = codecBuffer[1];
			return (short) (((b0 & 0xff) << 8) | (b1 & 0xff));
		}

		private int getInt() throws MemoryMapException {
			get(codecBuffer, 0, 4);
			final byte b0 = codecBuffer[0];
			final byte b1 = codecBuffer[1];
			final byte b2 = codecBuffer[2];
			final byte b3 = codecBuffer[3];
			return (int) (((b0 & 0xff) << 24) |
					((b1 & 0xff) << 16) |
					((b2 & 0xff) << 8) |
					(b3 & 0xff));
		}

		private long getLong() throws MemoryMapException {
			get(codecBuffer, 0, 8);
			final byte b0 = codecBuffer[0];
			final byte b1 = codecBuffer[1];
			final byte b2 = codecBuffer[2];
			final byte b3 = codecBuffer[3];
			final byte b4 = codecBuffer[4];
			final byte b5 = codecBuffer[5];
			final byte b6 = codecBuffer[6];
			final byte b7 = codecBuffer[7];
			return (long) (((b0 & 0xffL) << 56) |
					((b1 & 0xffL) << 48) |
					((b2 & 0xffL) << 40) |
					((b3 & 0xffL) << 32) |
					((b4 & 0xffL) << 24) |
					((b5 & 0xffL) << 16) |
					((b6 & 0xffL) << 8) |
					(b7 & 0xffL));
		}

		private byte[] getByteArray() throws MemoryMapException {
			final int length = getInt();
			if (-1 == length)
				return null;
			final byte[] bytes = new byte[length];
			if (length > 0)
				get(bytes, 0, length);
			return bytes;
		}

		private String getString(String charsetName) throws MemoryMapException, UnsupportedEncodingException {
			final byte[] bytes = getByteArray();
			if (null == bytes)
				return null;
			return new String(bytes, charsetName);
		}
		
		private String getString(Charset charset) throws MemoryMapException {
			final byte[] bytes = getByteArray();
			if (null == bytes)
				return null;
			return new String(bytes, charset);
		}

		private LongArray getLongArray(LongArray array) throws MemoryMapException {
			final int size = getInt();
			for (int i = 0; i < size; i ++)
				array.add(getLong());
			return array;
		}
		
		private LongArray getLongArray() throws MemoryMapException {
			final int size = getInt();
			final LongArray array = new LongArray(size);
			for (int i = 0; i < size; i ++)
				array.add(getLong());
			return array;
		}
		
	}

	/**
	 * Thread safe internal state needed for get/put operations.
	 */
	private class ThreadLocalContext extends ThreadLocal<Context> {
	 
	    @Override
	    protected synchronized Context initialValue() {
	        return new Context();
	    }
	    
	}
	
	private final Logger logger = LoggerFactory.getLogger(ConcurrentMemoryMap.class);
	
	private final File folder;
	private final boolean readOnly;
	private final FileChannel.MapMode mapMode;
	private final String fileMode;
	private final ThreadLocalContext threadLocalContext;

	private int pageSize;
	private ThreadLocalBuffer[] threadLocalBuffers;
		
	private ConcurrentMemoryMap(File folder, int pageSize, boolean readOnly) {
		super();
		this.folder = folder; 
		this.pageSize = pageSize;
		this.readOnly = readOnly;
		if (readOnly) {
			mapMode = MapMode.READ_ONLY;
			fileMode = "r";
		} else {
			mapMode = MapMode.READ_WRITE;
			fileMode = "rw";
			folder.mkdirs();
		}
		threadLocalBuffers = new ThreadLocalBuffer[] { null };
		threadLocalContext = new ThreadLocalContext();
	}
	
	private ConcurrentMemoryMap deleteAllFiles(int index) {
		for (;; index ++) {
			final File file = fileForBuffer(index);
			if (file.isFile() && file.length() > 0L) {
				logger.debug("deleting file {} ({})",
						file.getName(), TextUtils.formatSize(file.length()));
				file.delete();
				continue;
			}
			return this;
		}
	}
	
	private ConcurrentMemoryMap scanFolder() throws MemoryMapException {
		File file = fileForBuffer(0);
		if (file.isFile() && file.length() > 0L) {
			if (-1 == pageSize) {
				pageSize = (int) file.length();
			} else if (pageSize != (int) file.length()) {
				throw new MemoryMapException(String
						.format("bad file size \"%s\"", file.getAbsolutePath()));
			}
//		} else if (readOnly) {
//			throw new MemoryMapException(String
//					.format("file not found \"%s\"", file.getAbsolutePath()));
		} else {
			return this;
		}
		for (int index = 1; ; index ++) {
			file = fileForBuffer(index);
			if (file.isFile() && file.length() > 0L) {
				if (pageSize != (int) file.length()) {
					throw new MemoryMapException(String
							.format("bad file size \"%s\"", file.getAbsolutePath()));
				}
				continue;
			}
			break;
		}
		return this;
	}
	
	private File fileForBuffer(int index) {
		return new File(folder, String.format("%04x", index));
	}
	
	private synchronized void initializeBufferIfNull(int index) throws MemoryMapException {
		if (index >= threadLocalBuffers.length) {
			ThreadLocalBuffer[] extended = new ThreadLocalBuffer[1 + index];
			System.arraycopy(threadLocalBuffers, 0, extended, 0, threadLocalBuffers.length);
			Arrays.fill(extended, threadLocalBuffers.length, extended.length, null);
			threadLocalBuffers = extended;
		}
		if (null == threadLocalBuffers[index]) {
			try (RandomAccessFile file = new RandomAccessFile(fileForBuffer(index), fileMode)) {
				final MappedByteBuffer buffer = file.getChannel()
						.map(mapMode, 0L, (long) pageSize);
				threadLocalBuffers[index] = new ThreadLocalBuffer(buffer);
			} catch (Exception e) {
				throw new MemoryMapException(e);
			}
		}
	}
	
	private ThreadLocalBuffer getBuffer(long position) throws MemoryMapException {
		final int index = (int) (position / (long) pageSize);
		if (index >= threadLocalBuffers.length || null == threadLocalBuffers[index]) {
			initializeBufferIfNull(index);
		}
		return threadLocalBuffers[index];			
	}
	
	private void cleanDirectBuffer(MappedByteBuffer buffer) {
	    if (null == buffer || !buffer.isDirect()) {
	    	return;
	    }
	    // we could use this type cast and call functions without reflection code,
	    // but static import from sun.* package is risky for non-SUN virtual machine.
	    try {
	        final Method cleaner = buffer
	        		.getClass().getMethod("cleaner");
	        cleaner.setAccessible(true);
	        final Method clean = Class
	        		.forName("sun.misc.Cleaner").getMethod("clean");
	        clean.setAccessible(true);
	        clean.invoke(cleaner.invoke(buffer));
	    } catch (Throwable e) {
	    	logger.error("cleanDirectBuffer", e);
	    }
	}
	
	@Override
	public File getFolder() {
		return folder;
	}

	@Override
	public boolean isReadOnly() {
		return readOnly;
	}
	
	@Override
	public int getPageSize() {
		return pageSize;
	}
	
	@Override
	public long getSizeOnDisk() {
		long sizeOnDisk = 0L;
		for (int index = 0; ; index ++) {
			final File file = fileForBuffer(index);
			if (file.isFile() && file.length() > 0L) {
				sizeOnDisk += file.length();
				continue;
			}
			break;
		}
		return sizeOnDisk;
	}

	private void garbage(int index) {
		synchronized (this) {
			for (; index < threadLocalBuffers.length; index ++) {
				final ThreadLocalBuffer buffer = threadLocalBuffers[index];
				if (null != buffer) {
					cleanDirectBuffer(buffer.buffer);
					threadLocalBuffers[index] = null;
				}
			}
		}		
	}
	
	@Override
	public ConcurrentMemoryMap garbage() {
		garbage(0);
		return this;
	}

	@Override
	public ConcurrentMemoryMap shrink(long size) {
		final int index = 1 + (int) (size / (long) pageSize);
		garbage(index);
		if (!readOnly) {
			deleteAllFiles(index);
		}
		return this;
	}
	
	@Override
	public ConcurrentMemoryMap clear() {
		garbage(0);
		if (!readOnly) {
			deleteAllFiles(0);
		}
		return this;
	}
	
	@Override
	public long position() {
		return threadLocalContext.get().position;
	}

	@Override
	public ConcurrentMemoryMap position(long position) {
		if (position < 0L)
			throw new ArrayIndexOutOfBoundsException(Long.toString(position));
		threadLocalContext.get().position = position;
		return this;
	}
	
	@Override
	public ConcurrentMemoryMap put(byte[] src) throws MemoryMapException {
		threadLocalContext.get().put(src, 0, src.length);
		return this;
	}
	
	@Override
	public ConcurrentMemoryMap put(byte[] src, int offset, int length) throws MemoryMapException {
		threadLocalContext.get().put(src, offset, length);
		return this;
	}

	@Override
	public ConcurrentMemoryMap put(byte value) throws MemoryMapException {
		threadLocalContext.get().put(value);
		return this;
	}

	@Override
	public ConcurrentMemoryMap putChar(char value) throws MemoryMapException {
		threadLocalContext.get().putChar(value);
		return this;
	}
	
	@Override
	public ConcurrentMemoryMap putShort(short value) throws MemoryMapException {
		threadLocalContext.get().putShort(value);
		return this;
	}

	@Override
	public ConcurrentMemoryMap putInt(int value) throws MemoryMapException {
		threadLocalContext.get().putInt(value);
		return this;
	}

	@Override
	public ConcurrentMemoryMap putLong(long value) throws MemoryMapException {
		threadLocalContext.get().putLong(value);
		return this;
	}

	@Override
	public ConcurrentMemoryMap putString(String value, String charsetName) throws MemoryMapException, UnsupportedEncodingException {
		threadLocalContext.get().putString(value, charsetName);
		return this;
	}

	@Override
	public MemoryMap putString(String value, Charset charset) throws MemoryMapException {
		threadLocalContext.get().putString(value, charset);
		return this;
	}
	
	@Override
	public ConcurrentMemoryMap putLongArray(LongArray value) throws MemoryMapException {
		threadLocalContext.get().putLongArray(value);
		return this;
	}
	
	@Override
	public void get(byte[] dst) throws MemoryMapException {
		threadLocalContext.get().get(dst, 0, dst.length);
	}

	@Override
	public void get(byte[] dst, int offset, int length) throws MemoryMapException {
		threadLocalContext.get().get(dst, offset, length);
	}

	@Override
	public byte get() throws MemoryMapException {
		return threadLocalContext.get().get();
	}

	@Override
	public char getChar() throws MemoryMapException {
		return threadLocalContext.get().getChar();
	}
	
	@Override
	public short getShort() throws MemoryMapException {
		return threadLocalContext.get().getShort();
	}

	@Override
	public int getInt() throws MemoryMapException {
		return threadLocalContext.get().getInt();
	}

	@Override
	public long getLong() throws MemoryMapException {
		return threadLocalContext.get().getLong();
	}
	
	@Override
	public String getString(String charsetName) throws MemoryMapException, UnsupportedEncodingException {
		return threadLocalContext.get().getString(charsetName);
	}
	
	@Override
	public String getString(Charset charset) throws MemoryMapException {
		return threadLocalContext.get().getString(charset);
	}
	
	@Override
	public LongArray getLongArray(LongArray array) throws MemoryMapException {
		return threadLocalContext.get().getLongArray(array);
	}
	
	@Override
	public LongArray getLongArray() throws MemoryMapException {
		return threadLocalContext.get().getLongArray();
	}
	
}
