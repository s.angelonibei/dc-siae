package com.alkemytech.sophia.commons.guice;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.google.common.base.Strings;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ConfigurationLoader {

	private String filePath;
	private String[] args;
	private String resourceName;

	public ConfigurationLoader withFilePath(String filePath) {
		this.filePath = filePath;
		return this;
	}
	
	public ConfigurationLoader withCommandLineArgs(String[] args) {
		this.args = args;
		return this;
	}
	
	public ConfigurationLoader withResourceName(String resourceName) {
		this.resourceName = resourceName;
		return this;
	}

	public Properties load() {
		return load(new Properties());
	}
	
	public Properties load(Properties defaults) {

		final Properties properties = new Properties(defaults);
		properties.putAll(System.getenv()); // os environment
		properties.putAll(System.getProperties()); // java system properties
		boolean loaded = false;
		String message = "no configuration";
		if (null != args && args.length > 0) {
			try (final InputStream in = new FileInputStream(args[0])) {
				properties.load(in);
				loaded = true;
				message = "configuration from command line " + args[0];
			} catch (IOException | NullPointerException e) {
				// swallow
			}
		}
		if (!loaded && !Strings.isNullOrEmpty(filePath)) {
			try {
				final File file = new File(filePath);
				try (final InputStream in = new FileInputStream(file)) {
					properties.load(in);
					loaded = true;
					message = "configuration from file path " + file.getAbsolutePath();
				}
			} catch (IOException | NullPointerException e) {
				// swallow
			}
		}
		if (!loaded && !Strings.isNullOrEmpty(resourceName)) {
			try (final InputStream in = ConfigurationLoader.class.getResourceAsStream(resourceName)) {
				properties.load(in);
				loaded = true;
				message = "configuration from resource name";
			} catch (IOException | NullPointerException e) {
				// swallow
			}
		}
		if (!loaded && !Strings.isNullOrEmpty(properties.getProperty("configuration"))) {
			try (final InputStream in = new FileInputStream(properties.getProperty("configuration"))) {
				properties.load(in);
				loaded = true;
				message = "configuration from java system property " +
						properties.getProperty("configuration");
			} catch (IOException | NullPointerException e) {
				// swallow
			}
		}
		if (!loaded && !Strings.isNullOrEmpty(System.getenv("CONFIGURATION"))) {
			try (final InputStream in = new FileInputStream(System.getenv("CONFIGURATION"))) {
				properties.load(in);
				loaded = true;
				message = "configuration from os environment " + System.getenv("CONFIGURATION");
			} catch (IOException | NullPointerException e) {
				// swallow
			}
		}
		if (!loaded) {
			try (final InputStream in = ConfigurationLoader.class.getResourceAsStream("/configuration.properties")) {
				properties.load(in);
				loaded = true;
				message = "configuration from classpath";
			} catch (IOException | NullPointerException e) {
				// swallow
			}
		}
		if ("true".equals(properties.getProperty("default.debug"))) {
			System.out.println(message);
		}
		return properties;
		
	}
	
	public static Properties expandVariables(Properties properties) {
		return expandVariables(properties, "${", "}");
	}
	
	public static Properties expandVariables(Properties properties, String prefix, String suffix) {
		final int prefixLength = prefix.length();
		final int suffixLength = suffix.length();
		for (String propertyName : properties.stringPropertyNames()) {
			String propertyValue = properties.getProperty(propertyName);
			for (int beginIndex = propertyValue.indexOf(prefix), count = 0;
					-1 != beginIndex; beginIndex = propertyValue.indexOf(prefix), count ++) {
				int endIndex = propertyValue.indexOf(suffix, prefixLength + beginIndex);
				if (-1 != endIndex) {
					String replacementName = propertyValue
							.substring(prefixLength + beginIndex, endIndex).trim();
					String replacementValue = properties.getProperty(replacementName);
					if (null == replacementValue) {
						throw new IllegalArgumentException(String
								.format("missing property \"%s\" resolving \"%s\"", replacementName, propertyName));
					}
					propertyValue = propertyValue.substring(0, beginIndex) +
							replacementValue + propertyValue.substring(suffixLength + endIndex);
					properties.setProperty(propertyName, propertyValue);
				}
				if (count > 1024) {
					throw new IllegalArgumentException(String
							.format("loop detected while expanding property \"%s\"", propertyName));
				}
			}
		}
		return properties;
	}
	
}
