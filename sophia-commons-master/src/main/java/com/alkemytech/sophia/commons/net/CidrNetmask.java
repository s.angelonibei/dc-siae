
/*
 * The MIT License
 *
 * Copyright (c) 2013 Edin Dazdarevic (edin.dazdarevic@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */

package com.alkemytech.sophia.commons.net;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import com.alkemytech.sophia.commons.util.GsonUtils;

/**
 * An IP netmask from CIDR specification. Supports both IPv4 and IPv6.
 */
public class CidrNetmask {
	
    private final int prefixLength;
    private final InetAddress inetAddress;
    private final InetAddress startAddress;
    private final InetAddress endAddress;

    public CidrNetmask(String cidr) throws UnknownHostException {

        if (!cidr.contains("/"))
            throw new IllegalArgumentException(String
            		.format("invalid CIDR format: %s", cidr));

    	
        final int index = cidr.indexOf("/");
        final String addressPart = cidr.substring(0, index);
        final String networkPart = cidr.substring(index + 1);

        this.inetAddress = InetAddress.getByName(addressPart);
        this.prefixLength = Integer.parseInt(networkPart);

        final ByteBuffer maskBuffer;
        final int targetSize;
        final byte[] address = inetAddress.getAddress();
        if (4 == address.length) {
            maskBuffer = ByteBuffer.allocate(4)
            		.putInt(-1);
            targetSize = 4;
        } else {
            maskBuffer = ByteBuffer.allocate(16)
                    .putLong(-1L).putLong(-1L);
            targetSize = 16;
        }

        final BigInteger mask = (new BigInteger(1, maskBuffer.array())).not().shiftRight(prefixLength);
        final BigInteger startIp = (new BigInteger(1, address)).and(mask);
        final BigInteger endIp = startIp.add(mask.not());

        final byte[] startIpArray = toBytes(startIp.toByteArray(), targetSize);
        final byte[] endIpArray = toBytes(endIp.toByteArray(), targetSize);

        this.startAddress = InetAddress.getByAddress(startIpArray);
        this.endAddress = InetAddress.getByAddress(endIpArray);
        
    }

    private byte[] toBytes(byte[] array, int targetSize) {
        int counter = 0;
        final List<Byte> arrayList = new ArrayList<Byte>();
        while (counter < targetSize && (array.length - 1 - counter >= 0)) {
            arrayList.add(0, array[array.length - 1 - counter]);
            counter++;
        }
        final int size = arrayList.size();
        for (int i = 0; i < (targetSize - size); i++) {
            arrayList.add(0, (byte) 0);
        }
        final byte[] bytes = new byte[arrayList.size()];
        for (int i = arrayList.size() - 1; i > 0; i--) {
            bytes[i] = arrayList.get(i);
        }
        return bytes;
    }

    public String getNetworkAddress() {
        return startAddress.getHostAddress();
    }

    public String getBroadcastAddress() {
        return endAddress.getHostAddress();
    }

    public boolean isInRange(String ipAddress) throws UnknownHostException {
    	final InetAddress address = InetAddress.getByName(ipAddress);
        final BigInteger start = new BigInteger(1, startAddress.getAddress());
        final BigInteger end = new BigInteger(1, endAddress.getAddress());
        final BigInteger target = new BigInteger(1, address.getAddress());
        final int st = start.compareTo(target);
        final int te = target.compareTo(end);
        return (st == -1 || st == 0) && (te == -1 || te == 0);
    }
    
    @Override
    public String toString() {
    	return GsonUtils.toJson(this);
    }
    
}
