package com.alkemytech.sophia.commons.metric;

/**
 * Longest Common Substring distance.
 * <p>
 * <b>Warning:</b> this class is not thread safe, use a different instace in
 * each thread.
 * 
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class LongestCommonSubstring {
	
	private final int[] p = new int[256];
	private final int[] d = new int[256];

	public int distance(char[] x, char[] y) {
		return distance(x, 0, x.length, y, 0, y.length);
	}
	
	public int distance(char[] x, int xfst, int xlst, char[] y, int yfst, int ylst) {
		final int m = xlst - xfst;
		final int n = ylst - yfst;
		int lcs = 0;
		int[] p = this.p;
		int[] d = this.d;
		for (int i = 0; i < m; ++i) {
			for (int j = 0; j < n; ++j) {
				int cost = 0;
				if (x[xfst + i] == y[xfst + j]) {
					if (0 == i || 0 == j) {
						cost = 1;
					} else {
						cost = p[j-1] + 1;
					}
				}
				d[j] = cost;
				if (cost > lcs) {
					lcs = cost;
				}
			}
			int[] swap = p; p = d; d = swap;
		}
		return lcs;
	}

	public int distance(String a, String b) {
		return distance(a.toCharArray(), b.toCharArray());
	}

//	public static void main(String[] args) {
//		System.out.println(new LongestCommonSubstring().distance("ciao", "ciappo"));
//		System.out.println(new LongestCommonSubstring().distance("ciao", "ao ao"));
//	}
	
}
