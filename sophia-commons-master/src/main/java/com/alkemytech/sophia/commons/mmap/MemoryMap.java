package com.alkemytech.sophia.commons.mmap;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import com.alkemytech.sophia.commons.util.LongArray;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface MemoryMap {

	public File getFolder();
	public boolean isReadOnly();
	public int getPageSize();
	public long getSizeOnDisk();
	public MemoryMap garbage();
	public MemoryMap clear();
	public MemoryMap shrink(long size);
	public long position();
	public MemoryMap position(long position);	
	public MemoryMap put(byte[] src) throws MemoryMapException;
	public MemoryMap put(byte[] src, int offset, int length) throws MemoryMapException;
	public MemoryMap put(byte value) throws MemoryMapException;
	public MemoryMap putChar(char value) throws MemoryMapException;
	public MemoryMap putShort(short value) throws MemoryMapException;
	public MemoryMap putInt(int value) throws MemoryMapException;
	public MemoryMap putLong(long value) throws MemoryMapException;
	public MemoryMap putString(String value, String charsetName) throws MemoryMapException, UnsupportedEncodingException;
	public MemoryMap putString(String value, Charset charset) throws MemoryMapException;
	public MemoryMap putLongArray(LongArray value) throws MemoryMapException;
	public void get(byte[] dst) throws MemoryMapException;
	public void get(byte[] dst, int offset, int length) throws MemoryMapException;
	public byte get() throws MemoryMapException;
	public char getChar() throws MemoryMapException;
	public short getShort() throws MemoryMapException;
	public int getInt() throws MemoryMapException;
	public long getLong() throws MemoryMapException;
	public String getString(String charsetName) throws MemoryMapException, UnsupportedEncodingException;	
	public String getString(Charset charset) throws MemoryMapException;	
	public LongArray getLongArray(LongArray array) throws MemoryMapException;
	public LongArray getLongArray() throws MemoryMapException;

}
