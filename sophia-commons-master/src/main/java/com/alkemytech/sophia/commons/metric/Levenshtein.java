package com.alkemytech.sophia.commons.metric;

/**
 * The Levenshtein distance between two strings is the minimum number of
 * single-character edits (insertions, deletions or substitutions) required to
 * change one string into the other.
 * <p>
 * <b>Warning:</b> this class is not thread safe, use a different instace in
 * each thread.
 * 
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class Levenshtein {
	
	private final int[] prev = new int[256];
	private final int[] curr = new int[256];
	
	private int min(int a, int b, int c) {
		if (a < b) {
			if (a < c) {
				return a;
			}
			return c;
		} else if (b < c) {
			return b;
		}
		return c;
	}
	
	public int distance(char[] x, char[] y) {
		return distance(x, 0, x.length, y, 0, y.length);
	}
	
	public int distance(char[] x, int xfst, int xlst, char[] y, int yfst, int ylst) {
		final int xlen = xlst - xfst;
		final int ylen = ylst - yfst;
		int[] prev = this.prev;
		int[] curr = this.curr;
		for (int i = 0; i <= ylen; i++) {
	        prev[i] = i;
		}
		for (int i = 1; i <= xlen; i++) {
	        curr[0] = i;
	        for (int j = 1; j <= ylen; j++) {
	            if (x[xfst + i - 1] != y[yfst + j - 1]) {
	                int k = min(curr[j - 1], prev[j - 1], prev[j]);
	                curr[j] = k + 1;
	            } else {
	                curr[j] = prev[j - 1];
	            }
	        }
	        int[] swap = prev; prev = curr; curr = swap;
	    }
	    return prev[ylen];
	}
	
	public int distance(String a, String b) {
		return distance(a.toCharArray(), b.toCharArray());
	}
	
//	public static void main(String[] args) {
//		System.out.println(new Levenshtein().distance("ciao", "ciappo"));
//		System.out.println(new Levenshtein().distance("ciao", "ao ao"));
//	}
	
}
