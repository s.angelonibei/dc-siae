package com.alkemytech.sophia.commons.index;

import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.alkemytech.sophia.commons.mmap.ConcurrentMemoryMap;
import com.alkemytech.sophia.commons.mmap.MemoryMap;
import com.alkemytech.sophia.commons.trie.ConcurrentTrie;
import com.alkemytech.sophia.commons.trie.IntegerBinding;
import com.alkemytech.sophia.commons.trie.ScoredPath;
import com.alkemytech.sophia.commons.trie.Trie;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.commons.util.LongArray;
import com.alkemytech.sophia.commons.util.SizeInBytes;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.google.common.base.Strings;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ReverseIndexImpl implements ReverseIndex {

	private static class Posting {
		
		public static final long BYTES = 32L;

		public final long offset;
		public final long id; // document id
		public final long term; // offset of term string
		
		public long prev; // offset of prev posting (0 beginning-of-list)
		public long next; // offset of next posting (0 end-of-list)
		
		public Posting(long offset, long id, long term, long prev, long next) {
			super();
			this.offset = offset;
			this.id = id;
			this.term = term;
			this.prev = prev;
			this.next = next;
		}

	}

	/**
	 * Exact match
	 */
	public class SingleTermSet implements TermSet {

		private final String term;
		private final int index;

		private int remaining;
		
		protected SingleTermSet(String term, int index) {
			super();
			this.term = term;
			this.index = index;
			remaining = 1;
		}

		@Override
		public int size() {
			return 1;			
		}

		@Override
		public boolean next() {
			remaining --;
			return 0 == remaining;
		}

		@Override
		public String getTerm() {
			if (0 == remaining) {
				return term;
			}
			throw new IllegalStateException(remaining > 0 ?
					"next not yet called" : "term set exhausted");
		}

		@Override
		public LongArray getPostings() {
			if (0 == remaining) {
				return getPostingsByIndex(index);
			}
			throw new IllegalStateException(remaining > 0 ?
					"next not yet called" : "term set exhausted");
		}
		
	}
	
	/**
	 * Fuzzy  match
	 */
	public class MultipleTermSet implements TermSet {
		
		private final Iterator<ScoredPath<Integer>> iterator;
		private final int size;
		
		private ScoredPath<Integer> scoredPath;
		
		protected MultipleTermSet(Collection<ScoredPath<Integer>> scoredPaths) {
			super();
			size = scoredPaths.size();
			iterator = scoredPaths.iterator();
		}

		@Override
		public final int size() {
			return size;			
		}

		@Override
		public final boolean next() {
			if (iterator.hasNext()) {
				scoredPath = iterator.next();
				return true;
			}
			scoredPath = null;
			return false;
		}

		@Override
		public final String getTerm() {
			if (null != scoredPath) {
				return scoredPath.path;
			}
			throw new IllegalStateException(iterator.hasNext() ?
					"next not yet called" : "term set exhausted");
		}

		@Override
		public final LongArray getPostings() {
			if (null != scoredPath) {
				return getPostingsByIndex(scoredPath.leaf);
			}
			throw new IllegalStateException(iterator.hasNext() ?
					"next not yet called" : "term set exhausted");
		}
		
	}
	
	private final class ReverseIndexEditor implements Editor {

		private final Map<String, Long> termsMap;
		
		private ReverseIndexEditor() {
			super();
			if (0L == termsRaw.getSizeOnDisk()) {
				termsRaw.position(0L).putLong(SizeInBytes.LONG); // initial size in bytes
			}
			if (0L == postingsRaw.getSizeOnDisk()) {
				postingsRaw.position(0L).putLong(SizeInBytes.LONG); // initial size in bytes
			}
			termsMap = readTermsMap();
		}
		
		private Map<String, Long> readTermsMap() {
			final Map<String, Long> termsMap = new HashMap<>();
			final long size = postingsRaw.position(0L).getLong();
			final HeartBeat heartbeat = HeartBeat
					.percentage("loading raw index", size / Posting.BYTES, 100);
			for (long offset = SizeInBytes.LONG; offset < size; offset += Posting.BYTES) {
				final Posting posting = readPosting(offset);
				if (0L == posting.prev) { // beginning-of-list
					final String term = termsRaw
						.position(posting.term)
						.getString(Charset.forName("UTF-8"));
					termsMap.put(term, offset);
				}
				heartbeat.pump();
	        }
			return termsMap;
		}

		private long addTerm(String term) {
			final long offset = termsRaw.position(0).getLong();
			termsRaw.position(offset)
				.putString(term, Charset.forName("UTF-8"));
			final long size = termsRaw.position();
			termsRaw.position(0).putLong(size);
			return offset;
		}

		private Posting readPosting(long offset) {
			postingsRaw.position(offset);
			return new Posting(offset,
					postingsRaw.getLong(),
					postingsRaw.getLong(),
					postingsRaw.getLong(),
					postingsRaw.getLong());
		}

		private void writePosting(Posting posting) {
			postingsRaw.position(posting.offset);
			postingsRaw.putLong(posting.id);
			postingsRaw.putLong(posting.term);
			postingsRaw.putLong(posting.prev);
			postingsRaw.putLong(posting.next);
		}
		
		private LongArray sortPostings(long offset, LongArray postings) {
			while (offset > 0L) {
				final Posting posting = readPosting(offset);
				final int position = postings.indexOf(posting.id);
				if (position < 0) {
					postings.add(-position - 1, posting.id);
				}
				offset = posting.next;
			}
			return postings;
		}
		
		/*
		 * (non-Javadoc)
		 * @see com.alkemytech.sophia.commons.index.ReverseIndex.Editor#rewind()
		 */
		@Override
		public void rewind() {
			termsRaw.clear()
				.position(0L).putLong(SizeInBytes.LONG); // initial size in bytes
			postingsRaw.clear()
				.position(0L).putLong(SizeInBytes.LONG); // initial size in bytes
			termsMap.clear();
		}

		/*
		 * (non-Javadoc)
		 * @see com.alkemytech.sophia.commons.index.ReverseIndex.Editor#add(java.lang.String, long)
		 */
		@Override
		public void add(String term, long id) {
			if (!Strings.isNullOrEmpty(term)) {
				final Long offset = termsMap.get(term);
				final Posting existing = null == offset ? null : readPosting(offset);
				final long size = postingsRaw.position(0L).getLong();
				final Posting posting;
				if (null == existing) {
					posting = new Posting(size, id, addTerm(term), 0L, 0L);
				} else {
					posting = new Posting(size, id, existing.term, 0L, existing.offset);
					existing.prev = posting.offset;
					writePosting(existing);
				}
				writePosting(posting);
				postingsRaw.position(0L).putLong(size + Posting.BYTES);
				termsMap.put(term, posting.offset);
			}			
		}
		
		/*
		 * (non-Javadoc)
		 * @see com.alkemytech.sophia.commons.index.ReverseIndex.Editor#addAll(java.util.Collection, long)
		 */
		@Override
		public void addAll(Collection<String> terms, long id) {
			for (String term : terms) {
				add(term, id);
			}
		}

		/*
		 * (non-Javadoc)
		 * @see com.alkemytech.sophia.commons.index.ReverseIndex.Editor#commit()
		 */
		@Override
		public void commit() {
			// sort terms lexicographically
			final List<String> keys = new ArrayList<>(termsMap.keySet());
			Collections.sort(keys, new Comparator<String>() {

				@Override
				public int compare(String o1, String o2) {
					return o1.compareTo(o2);
				}
				
			});
			// save memory buffers
			final HeartBeat heartbeat = HeartBeat.percentage(String
					.format("writing %s index", name), keys.size(), 100);
			final LongArray postings = new LongArray();
			int index = 0;
			if (null != termsTrie)
				termsTrie.clear();
			termsData.clear()
				.position(0L).putInt(keys.size()); // number of terms
			termsIndex.clear()
				.position(0L);
			postingsData.clear()
				.position(0L);
			postingsIndex.clear()
				.position(0L);
			for (String key : keys) {
				postings.clear();
				if (null != termsTrie)
					termsTrie.upsert(key, index ++);
				termsIndex.putLong(termsData.position());
				termsData.putString(key, Charset.forName("UTF-8"));
				postingsIndex.putLong(postingsData.position());
				postingsData.putLongArray(sortPostings(termsMap.get(key), postings));
				heartbeat.pump();
			}
		}

	}

	private final String name;
	private final boolean readOnly;
	private final Trie<Integer> termsTrie;
	private final MemoryMap termsData;
	private final MemoryMap termsIndex;
	private final MemoryMap termsRaw;
	private final MemoryMap postingsData;
	private final MemoryMap postingsIndex;
	private final MemoryMap postingsRaw;
	private final Cache<Object, Object> postingsCache;
		
	public ReverseIndexImpl(Properties configuration, String propertyPrefix) {
		super();
		final File homeFolder = new File(configuration
				.getProperty(propertyPrefix + ".home_folder"));
		final int pageSize = TextUtils.parseIntSize(configuration
				.getProperty(propertyPrefix + ".page_size", "-1"));
		final int cacheSize = TextUtils.parseIntSize(configuration
				.getProperty(propertyPrefix + ".cache_size", "0"));
		final boolean approximateMatch = "true".equalsIgnoreCase(configuration
				.getProperty(propertyPrefix + ".approximate_match", "true"));
		name = configuration
				.getProperty(propertyPrefix + ".index_name");
		readOnly = "true".equalsIgnoreCase(configuration
				.getProperty(propertyPrefix + ".read_only", "true"));
		termsTrie = !approximateMatch ? null : ConcurrentTrie.wrap(ConcurrentMemoryMap
				.open(new File(homeFolder, "terms.trie"), pageSize, readOnly), new IntegerBinding(-1));
		termsData = ConcurrentMemoryMap
				.open(new File(homeFolder, "terms.data"), pageSize, readOnly);
		termsIndex = ConcurrentMemoryMap
				.open(new File(homeFolder, "terms.index"), pageSize, readOnly);
		termsRaw = readOnly ? null : ConcurrentMemoryMap
				.open(new File(homeFolder, "terms.raw"), pageSize, false);
		postingsData = ConcurrentMemoryMap
				.open(new File(homeFolder, "postings.data"), pageSize, readOnly);
		postingsIndex = ConcurrentMemoryMap
				.open(new File(homeFolder, "postings.index"), pageSize, readOnly);
		postingsRaw = readOnly ? null : ConcurrentMemoryMap
				.open(new File(homeFolder, "postings.raw"), pageSize, false);
		postingsCache = cacheSize < 1 ? null : CacheBuilder
				.newBuilder().maximumSize(cacheSize).build();
	}

	private LongArray getPostingsByIndex(int index) {
		if (null == postingsCache) {
			final long position = postingsIndex
	    			.position(index * SizeInBytes.LONG)
	    			.getLong();
			return postingsData.position(position)
					.getLongArray();
		}
		LongArray postings = (LongArray) postingsCache
				.getIfPresent(index);
		if (null == postings) {
			final long position = postingsIndex
	    			.position(index * SizeInBytes.LONG)
	    			.getLong();
			postings = postingsData
					.position(position)
					.getLongArray();
			postingsCache.put(index, postings);
		}
		return new LongArray(postings);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.commons.index.LevenshteinIndex#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	@Override
	public boolean isReadOnly() {
		return readOnly;
	}

	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.commons.index.LevenshteinIndex#search(java.lang.String)
	 */
	@Override
	public TermSet search(String text) {
		// performs a binary search on terms index
		int lo = 0;
        int hi = termsData
        		.position(0L).getInt() - 1;
        while (lo <= hi) {
            final int index = lo + (hi - lo) / 2;
        	long position = termsIndex
    			.position(index * SizeInBytes.LONG)
    			.getLong();
            final String term = termsData
        		.position(position)
        		.getString(Charset.forName("UTF-8"));
            final int cmp = text.compareTo(term);
            if (cmp < 0) {
            	hi = index - 1;
            } else if (cmp > 0) {
            	lo = index + 1;
            } else {
            	return new SingleTermSet(term, index);
    		}
        }
        return TermSet.EMPTY;
	}

	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.commons.index.LevenshteinIndex#search(java.lang.String, int)
	 */
	@Override
	public TermSet search(String text, int maxCost) {
		final List<ScoredPath<Integer>> scoredPaths = termsTrie.search(text, maxCost);
		if (null == scoredPaths || scoredPaths.isEmpty()) {
			return TermSet.EMPTY;
		}
		return new MultipleTermSet(scoredPaths);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.commons.index.ReverseIndex#edit()
	 */
	@Override
	public Editor edit() {
		if (null == postingsRaw) {
			throw new IllegalStateException("read only");
		}
		return new ReverseIndexEditor();
	}

}
