package com.alkemytech.sophia.commons.collecting;

import java.util.regex.Pattern;

import com.alkemytech.sophia.commons.text.TextNormalizer;
import com.google.common.base.Strings;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class UUIDCode {

	private static final Pattern IS_VALID_REGEX = Pattern
			.compile("[0-9a-fA-F]{8}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{12}");
	
	public static final TextNormalizer NORMALIZER = new TextNormalizer() {
		
		@Override
		public String normalize(String text) {
			return UUIDCode.normalize(text);
		}
		
	};
	
	public static String normalize(String code) {
		if (Strings.isNullOrEmpty(code))
			return null;
		final int length = code.length();
		if (length > 40)
			throw new IllegalArgumentException(String
					.format("code too long %d \"%s\"", length, code));
		return code;
	}
	
	public static boolean isValid(String code) {
		if (Strings.isNullOrEmpty(code)) {
			return false;
		}
		return IS_VALID_REGEX.matcher(code).matches();
	}
	
}
