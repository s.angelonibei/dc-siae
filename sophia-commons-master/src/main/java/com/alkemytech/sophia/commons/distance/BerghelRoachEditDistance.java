package com.alkemytech.sophia.commons.distance;

/**
 * The Berghel & Roach distance between two strings is the minimum number of
 * single-character edits (insertions, deletions or substitutions) required to
 * change one string into the other.
 */
public class BerghelRoachEditDistance implements EditDistance {
		
	private final int[][] fkp = new int[2*(MAX_LENGTH+1)+1][(MAX_LENGTH+1)+2];

	private final int max(int a, int b, int c) {
		if (a > b) {
			if (a > c) {
				return a;
			}
			return c;
		} else if (b > c) {
			return b;
		}
		return c;
	}

	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.commons.distance.EditDistance#getDistance(java.lang.CharSequence, java.lang.CharSequence)
	 */
	@Override
	public int getDistance(CharSequence pattern, CharSequence target) {

		if (pattern.length() > target.length()) {
			CharSequence swap = pattern; pattern = target; target = swap;
		}
		
		int zerok = Math.min(MAX_LENGTH, target.length());
		final int patternLength = Math.min(MAX_LENGTH, pattern.length());
		if (1 == zerok && 1 == patternLength)
			return pattern.charAt(0) == target.charAt(0) ? 0 : 1;
		final int lengthDifference = zerok - patternLength;
		
		for (int k = -zerok; k < 0; k++) {
			int p = -k - 1;
			fkp[k + zerok][p + 1] = Math.abs(k) - 1;
			fkp[k + zerok][p] = -Integer.MAX_VALUE;
		}
		
		fkp[zerok][0] = -1;
		
		for (int k = 1; k <= zerok; k++) {
			int p = k - 1;
			fkp[k + zerok][p + 1] = -1;
			fkp[k + zerok][p] = -Integer.MAX_VALUE;
		}
		
		int p = lengthDifference - 1;

		do {
			
			p ++;

			for (int i = (p - lengthDifference) / 2; i >= 1; i--)
				brf(pattern, target, zerok, lengthDifference + i, p - i, fkp);

			for (int i = (p + lengthDifference) / 2; i >= 1; i--)
				brf(pattern, target, zerok, lengthDifference - i, p - i, fkp);

			brf(pattern, target, zerok, lengthDifference, p, fkp);
			
		} while (fkp[lengthDifference + zerok][p] != patternLength);

		return p - 1;
		
	}
	
	/**
	 * Calculate FKP arrays in Berghel & Roach algorithm without support of transposition operation.
	 */
	private void brf(CharSequence pattern, CharSequence target, int zerok, int k, int p, int[][] fkp) {
		final int tmax = Math.min(pattern.length(), target.length() - k);
		zerok += k;
		int t = max(fkp[zerok][p] + 1, fkp[zerok - 1][p], fkp[zerok + 1][p] + 1);
		while (t < tmax && pattern.charAt(t) == target.charAt(t + k)) t ++;
		fkp[zerok][p + 1] = t;
	}
	
	/**
	 * Calculate FKP arrays in Berghel & Roach algorithm with support of transposition operation.
	 */
	@SuppressWarnings("unused")
	private void _brf(CharSequence pattern, CharSequence target, int zerok, int k, int p, int[][] fkp) {
		final int minLength = Math.min(pattern.length(), target.length() - k);
		int t = fkp[k + zerok][p] + 1;
		if (t > 1 && k + t > 1 && t < minLength) {
			if (pattern.charAt(t - 1) == target.charAt(k + t) && pattern.charAt(t) == target.charAt(k + t - 1))
				t ++;
		}
		t = max(fkp[k - 1 + zerok][p], fkp[k + 1 + zerok][p] + 1, t);
		for (; t < minLength && pattern.charAt(t) == target.charAt(t + k); t ++);
		fkp[k + zerok][p + 1] = t;
	}
	
}
