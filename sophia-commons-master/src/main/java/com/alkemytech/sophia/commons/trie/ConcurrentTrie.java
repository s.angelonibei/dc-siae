package com.alkemytech.sophia.commons.trie;

import com.alkemytech.sophia.commons.mmap.MemoryMap;
import com.alkemytech.sophia.commons.mmap.MemoryMapException;

/**
 * Thread safe version of class Trie&lt;T&gt;.
 * 
 * @deprecated use ConcurrentTrieEx&lt;T&gt; class instead
 * @see {@link Trie}
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@Deprecated
public class ConcurrentTrie<T> extends Trie<T> {

	public static <X> ConcurrentTrie<X> wrap(MemoryMap memoryMap, LeafBinding<X> leafBinding) throws MemoryMapException {
		return new ConcurrentTrie<X>(memoryMap, leafBinding);
	}
	
	public ConcurrentTrie(MemoryMap memoryMap, LeafBinding<T> leafBinding) throws MemoryMapException {
		super(memoryMap, leafBinding);
	}

	@Override
	public synchronized T upsert(String text, T leaf) throws MemoryMapException {
		return super.upsert(text, leaf);
	}

	@Override
	public synchronized ConcurrentTrie<T> clear() {
		super.clear();
		return this;
	}

	@Override
	public synchronized  ConcurrentTrie<T> garbage() {
		super.garbage();
		return this;
	}
	
}
