package com.alkemytech.sophia.commons.io;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.GZIPOutputStream;

import com.google.common.base.Strings;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class CompressionAwareFileOutputStream extends OutputStream {

	private final OutputStream stream;
	
	public CompressionAwareFileOutputStream(File file) throws IOException {
		this(file, null);
	}
	
	public CompressionAwareFileOutputStream(File file, String compression) throws IOException {
		super();
		if (Strings.isNullOrEmpty(compression) ||
				"guess".equalsIgnoreCase(compression)) {
			final String filename = file.getName().toLowerCase();
			final int dot = filename.lastIndexOf('.');
			if (-1 == dot) {
				compression = "none";
			} else {
				compression = filename.substring(1 + dot);
				if (!"gz".equals(compression) &&
						!"gzip".equals(compression)) {
					compression = "none";
				}
			}
		}
		if ("gz".equalsIgnoreCase(compression) ||
				"gzip".equalsIgnoreCase(compression)) {
			this.stream = new GZIPOutputStream(new FileOutputStream(file), 65536);
		} else if ("none".equalsIgnoreCase(compression)) {
			this.stream = new BufferedOutputStream(new FileOutputStream(file), 65536);
		} else {
			throw new IllegalArgumentException(String.format("unsupported compression: %s", compression));
		}
	}

	public void write(int b) throws IOException {
		stream.write(b);
	}

	public void write(byte[] b) throws IOException {
		stream.write(b);
	}

	public void write(byte[] b, int off, int len) throws IOException {
		stream.write(b, off, len);
	}

	public void flush() throws IOException {
		stream.flush();
	}

	public void close() throws IOException {
		stream.close();
	}

}
