package com.alkemytech.sophia.commons.query;

import com.alkemytech.sophia.commons.index.LevenshteinIndex;
import com.alkemytech.sophia.commons.util.LongArray;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class FuzzyAndQueryChain extends AbstractAndQueryChain {

	private final int[] fuzzyCosts;

	public FuzzyAndQueryChain(int[] fuzzyCosts) {
		super();
		this.fuzzyCosts = fuzzyCosts;
	}

	@Override
	protected LongArray searchTerm(LevenshteinIndex index, String text) {
		return fuzzySearch(index, text, fuzzyCosts);
	}
	
}
