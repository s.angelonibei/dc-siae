package com.alkemytech.sophia.commons.regex;

import java.util.regex.Pattern;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class RegExReplaceAll {

	private final String replacement;
	private final Pattern pattern;
	
	public RegExReplaceAll(String regex, String replacement) {
		this.replacement = replacement;
		this.pattern = Pattern.compile(regex);
	}
	
	public String replaceAll(String target) {
		return pattern.matcher(target).replaceAll(replacement);
	}
	
}
