package com.alkemytech.sophia.commons.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class FileUtils {

	private static final Logger logger = LoggerFactory.getLogger(FileUtils.class);

	public static void deleteFolder(File folder, boolean deleteSubdirs) {
		if (!folder.exists() || !folder.isDirectory())
			return;
		for (File file : folder.listFiles()) {
			if (file.isDirectory()) {
				if (deleteSubdirs)
					deleteFolder(file, true);
			} else {
				logger.debug("deleting file {} ({})", file.getName(),
						TextUtils.formatSize(file.length()));
				file.delete();
			}
		}
		folder.delete();
	}
	
	public static byte[] computeHash(File file, String algorithm) throws IOException, NoSuchAlgorithmException {
		final MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
		updateHash(file, messageDigest);
		return messageDigest.digest();
	}
	
	public static void updateHash(File file, MessageDigest messageDigest) throws IOException {
		try (final FileInputStream fileInputStream = new FileInputStream(file);
				final BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream)) {
			final byte[] buffer = new byte[4096];
			int length = 0;
			while (-1 != (length = bufferedInputStream.read(buffer))) {
				if (length > 0) {
					messageDigest.update(buffer, 0, length);
				}
			}
		}
	}
	
	public static byte[] toByteArray(File file) throws IOException {
		try (final FileInputStream in = new FileInputStream(file)) {
			if (file.length() > Integer.MAX_VALUE) {
				throw new IllegalArgumentException(String
						.format("file too large %s", TextUtils.formatSize(file.length())));
			}
			final int size = (int) file.length();
			final byte[] bytes = new byte[size];
			for (int length = 0, offset = 0;
					-1 != length && offset < size;
					offset += length) {
				length = in.read(bytes, offset, size - offset);
			}
			return bytes;
		}
	}
	
	public static long copy(File srcFile, File dstFile) throws IOException {
		long copied = -1L;
		try (final InputStream in = new FileInputStream(srcFile);
				final OutputStream out = new FileOutputStream(dstFile)) {
			copied = 0L;
			final byte[] buffer = new byte[1024];
			int length;
			while (-1 != (length = in.read(buffer))) {
				if (length > 0) {
					out.write(buffer, 0, length);
					copied += (long) length;
				}
			}
		}
		return copied;
	}
	
}
