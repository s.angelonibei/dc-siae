package com.alkemytech.sophia.commons.util;

import java.io.Reader;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class GsonUtils {

	private static final Logger logger = LoggerFactory.getLogger(GsonUtils.class);

	private static Gson gson = new GsonBuilder()
			.disableHtmlEscaping().create();
	
	public static JsonObject getAsJsonObject(JsonElement element) {
		return null == element ? null : element.getAsJsonObject();
	}

	public static JsonObject getAsJsonObject(JsonObject json, String key) {
		final JsonElement element = (null == json ? null : json.get(key));
		return null == element ? null : element.getAsJsonObject();
	}

	public static JsonArray getAsJsonArray(JsonElement element) {
		return null == element ? null : element.getAsJsonArray();
	}

	public static JsonArray getAsJsonArray(JsonObject json, String key) {
		final JsonElement element = (null == json ? null : json.get(key));
		return null == element ? null : element.getAsJsonArray();
	}

	public static JsonObject getAsJsonObject(JsonArray jsonArray, int position) {
		final JsonElement element = (null == jsonArray ? null : 
			(position < 0 || position >= jsonArray.size() ? null :
				jsonArray.get(position)));
		return null == element ? null : element.getAsJsonObject();
	}

	public static boolean getAsBoolean(JsonObject json, String key, boolean ifNull) {
		final JsonElement element = (null == json ? null : json.get(key));
		return null == element ? ifNull : element.getAsBoolean();
	}

	public static int getAsInt(JsonObject json, String key, int ifNull) {
		final JsonElement element = (null == json ? null : json.get(key));
		return null == element ? ifNull : element.getAsInt();
	}
	
	public static long getAsLong(JsonObject json, String key, long ifNull) {
		final JsonElement element = (null == json ? null : json.get(key));
		return null == element ? ifNull : element.getAsLong();
	}

	public static double getAsDouble(JsonObject json, String key, double ifNull) {
		final JsonElement element = (null == json ? null : json.get(key));
		return null == element ? ifNull : element.getAsDouble();
	}

	public static BigDecimal getAsBigDecimal(JsonObject json, String key, BigDecimal ifNull) {
		final JsonElement element = (null == json ? null : json.get(key));
		return null == element ? ifNull : element.getAsBigDecimal();
	}

	public static String getAsString(JsonObject json, String key) {
		return getAsString(json, key, null);
	}
	
	public static String getAsString(JsonObject json, String key, String ifNull) {
		final JsonElement element = (null == json ? null : json.get(key));
		final String value = (null == element ? null : element.getAsString());
		return null == value ? ifNull : value;
	}
	
	@SuppressWarnings("serial")
	public static Map<String,String> decodeJsonMap(String json) {
		if (null == json)
			return null;
		return gson.fromJson(json,
				new TypeToken<Map<String, String>>(){}.getType());
	}
	
	@SuppressWarnings("serial")
	public static Map<String,String> getAsHashMap(JsonElement jsonElement) {
		if (null == jsonElement)
			return null;
		return gson.fromJson(jsonElement,
				new TypeToken<Map<String, String>>(){}.getType());
	}

	public static JsonArray newJsonArray(JsonElement ...elements) {
		final JsonArray result = new JsonArray();
		for (JsonElement element : elements)
			result.add(element);
		return result;
	}

	public static JsonArray newJsonArray(Collection<String> texts) {
		final JsonArray result = new JsonArray();
		for (String text : texts)
			result.add(text);
		return result;
	}

	public static JsonElement deepCopy(JsonElement jsonElement) {
		if (jsonElement.isJsonPrimitive() || jsonElement.isJsonNull())
			return jsonElement;       // these are immutables anyway
		if (jsonElement.isJsonObject())
			return deepCopy(jsonElement.getAsJsonObject());
		if (jsonElement.isJsonArray())
			return deepCopy(jsonElement.getAsJsonArray());
	    throw new UnsupportedOperationException("Unsupported element: " + jsonElement);
	}

	public static JsonObject deepCopy(JsonObject jsonObject) {
		final JsonObject result = new JsonObject();
		for (Map.Entry<String, JsonElement> entry : jsonObject.entrySet())
			result.add(entry.getKey(), deepCopy(entry.getValue()));
		return result;
	}

	public static JsonArray deepCopy(JsonArray jsonArray) {
		final JsonArray result = new JsonArray();
		for (JsonElement e : jsonArray)
			result.add(deepCopy(e));
		return result;
	}

	public static <T> T fromJson(JsonElement json, Class<T> classOfT) throws JsonSyntaxException {
		return gson.fromJson(json, classOfT);
	}

	public static <T> T fromJson(JsonReader json, Class<T> classOfT) throws JsonSyntaxException {
		return gson.fromJson(json, classOfT);
	}

	public static <T> T fromJson(String json, Class<T> classOfT) throws JsonSyntaxException {
		return gson.fromJson(json, classOfT);
	}

	public static <T> T fromJson(Reader json, Class<T> classOfT) throws JsonSyntaxException {
		return gson.fromJson(json, classOfT);
	}

	public static String toJson(JsonElement jsonElement) {
		return gson.toJson(jsonElement);
	}

	public static void toJson(JsonElement jsonElement, JsonWriter jsonWriter) {
		gson.toJson(jsonElement, jsonWriter);
	}

	public static String toJson(Object src) {
		return gson.toJson(src);
	}

	@Inject
	protected GsonUtils(@Named("configuration") Properties configuration) {
		super();
		final boolean htmlEscaping = "true".equalsIgnoreCase(configuration
				.getProperty("gson.html_escaping", "false"));
		final boolean prettyPrinting = "true".equalsIgnoreCase(configuration
				.getProperty("gson.pretty_printing", "false"));
		final GsonBuilder builder = new GsonBuilder();
		if (!htmlEscaping)
			builder.disableHtmlEscaping();
		if (prettyPrinting)
			builder.setPrettyPrinting();
		GsonUtils.gson = builder.create();;
		logger.info("htmlEscaping {}", htmlEscaping);
		logger.info("prettyPrinting {}", prettyPrinting);
	}
	
}
