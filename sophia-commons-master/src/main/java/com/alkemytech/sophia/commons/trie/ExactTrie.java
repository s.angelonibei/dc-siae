package com.alkemytech.sophia.commons.trie;

import com.alkemytech.sophia.commons.mmap.MemoryMapException;

/**
 * Fixed-size generic leaf value trie.
 * 
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface ExactTrie<T> {

	public T upsert(String text, T leaf) throws MemoryMapException;	
	
	public T delete(String key) throws MemoryMapException;	

	public T find(String text) throws MemoryMapException;
		
	public ExactTrie<T> clear();
	
	public ExactTrie<T> garbage();

}
