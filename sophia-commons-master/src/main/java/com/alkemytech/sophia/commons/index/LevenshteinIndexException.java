package com.alkemytech.sophia.commons.index;

/**
 * A reverse index supporting exact and approximate string matching.
 * 
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@SuppressWarnings("serial")
public class LevenshteinIndexException extends RuntimeException {

	public LevenshteinIndexException(String message, Throwable cause) {
		super(message, cause);
	}

	public LevenshteinIndexException(String message) {
		super(message);
	}

	public LevenshteinIndexException(Throwable cause) {
		super(cause);
	}

}
