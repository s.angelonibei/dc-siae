package com.alkemytech.sophia.commons.trie;

import com.alkemytech.sophia.commons.mmap.MemoryMap;
import com.alkemytech.sophia.commons.mmap.MemoryMapException;

/**
 * Thread safe version of class TrieHashMap&lt;T&gt;.
 * 
 * @see {@link TrieHashMap}
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ConcurrentTrieHashMap<T> extends TrieHashMap<T> {

	public static <X> ConcurrentTrieHashMap<X> wrap(MemoryMap memoryMap, LeafBinding<X> leafBinding) throws MemoryMapException {
		return new ConcurrentTrieHashMap<X>(memoryMap, leafBinding);
	}
	
	public ConcurrentTrieHashMap(MemoryMap memoryMap, LeafBinding<T> leafBinding) throws MemoryMapException {
		super(memoryMap, leafBinding);
	}

	@Override
	public synchronized T upsert(String text, T leaf) throws MemoryMapException {
		return super.upsert(text, leaf);
	}

	@Override
	public synchronized ConcurrentTrieHashMap<T> clear() {
		super.clear();
		return this;
	}

	@Override
	public synchronized  ConcurrentTrieHashMap<T> garbage() {
		super.garbage();
		return this;
	}
	
}
