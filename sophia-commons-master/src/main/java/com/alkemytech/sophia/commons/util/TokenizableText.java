package com.alkemytech.sophia.commons.util;

import java.util.Iterator;

public interface TokenizableText extends CharSequence, Iterator<CharSequence>, Iterable<CharSequence> {

}
