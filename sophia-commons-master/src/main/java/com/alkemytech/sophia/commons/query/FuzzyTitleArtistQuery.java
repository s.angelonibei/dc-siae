package com.alkemytech.sophia.commons.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.index.LevenshteinIndex;
import com.alkemytech.sophia.commons.module.DynamicModuleException;
import com.alkemytech.sophia.commons.util.ListUtils;
import com.alkemytech.sophia.commons.util.LongArray;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.google.inject.Inject;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class FuzzyTitleArtistQuery implements TitleArtistQuery {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final String propertyPrefix;
	
	private int maxTerms;
	private int minTerms;
	private int[] fuzzyCosts;

	@Inject
	protected FuzzyTitleArtistQuery() {
		super();
		this.propertyPrefix = "fuzzy_title_artist_query";
	}

	public FuzzyTitleArtistQuery(Properties configuration, String propertyPrefix) {
		super();
		this.propertyPrefix = propertyPrefix;
		startup(configuration);
	}
	
	private LongArray exactSearch(LevenshteinIndex index, String text) {
		final LevenshteinIndex.TermSet termSet = index.search(text);
		if (termSet.next())
			return termSet.getPostings();
		return null;
	}

	private LongArray fuzzySearch(LevenshteinIndex reverseIndex, String text) {
		final int length = text.length();
		for (int maxCost = fuzzyCosts.length; maxCost > 0; maxCost --) {
			if (length >= fuzzyCosts[maxCost-1]) {
				final LevenshteinIndex.TermSet termSet = reverseIndex.search(text, maxCost);
				if (termSet.next()) {
					LongArray postings = termSet.getPostings();
					while (termSet.next()) {
						postings = LongArray.union(postings, termSet.getPostings());
					}
					return postings;
				}
				return null;
			}
		}
		return exactSearch(reverseIndex, text);
	}
	
	@Override
	public void startup(Properties configuration) throws DynamicModuleException {
		maxTerms = Integer.parseInt(configuration
				.getProperty(propertyPrefix + ".max_terms"));
		minTerms = Integer.parseInt(configuration
				.getProperty(propertyPrefix + ".min_terms"));
		final String[] fuzzyCostsCsv = TextUtils.split(configuration
				.getProperty(propertyPrefix + ".fuzzy_costs", "-1"), ",");
		fuzzyCosts = new int[fuzzyCostsCsv.length];
		for (int i = 0; i < fuzzyCosts.length; i ++) {
			fuzzyCosts[i] = Integer.parseInt(fuzzyCostsCsv[i]);
		}
		logger.debug("maxTerms {}", maxTerms);
		logger.debug("minTerms {}", minTerms);
		logger.debug("fuzzyCosts {}", fuzzyCosts);
	}

	@Override
	public void shutdown() {
		
	}
	
	@Override
	public LongArray search(LevenshteinIndex titleIndex, LevenshteinIndex artistIndex, Collection<String> titleTerms, Collection<String> artistTerms) {
		
		// sort title terms list (descending length)
		List<String> sortedTitleTerms = new ArrayList<>(titleTerms);
		Collections.sort(sortedTitleTerms, new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				return Integer.compare(o2.length(), o1.length());
			}
			
		});
		
		// keep longest terms only
		if (sortedTitleTerms.size() > maxTerms) {
			sortedTitleTerms = sortedTitleTerms.subList(0, maxTerms);
		}
		
		// skip useless title fuzzy searches
		final boolean fuzzyTitle = sortedTitleTerms
				.get(0).length() >= fuzzyCosts[0];
				
		// fuzzy vs. regular title search
		final List<LongArray> titleTermsPostings = new ArrayList<>(sortedTitleTerms.size());
		for (String term : sortedTitleTerms) {
			final LongArray postings = fuzzyTitle ?
					fuzzySearch(titleIndex, term) :
						exactSearch(titleIndex, term);
			if (null != postings && !postings.isEmpty()) {
				titleTermsPostings.add(postings);
			}
		}
		if (titleTermsPostings.isEmpty()) {
			return null;
		}
		
		// sort postings list (ascending length) to speed up intersections
		Collections.sort(titleTermsPostings, new Comparator<LongArray>() {

			@Override
			public int compare(LongArray o1, LongArray o2) {
				return Integer.compare(o1.size(), o2.size());
			}
			
		});
		
		// compute title terms piecewise intersection
		LongArray titlePostings = null;
		if (titleTermsPostings.size() > 2) {
			if (titleTermsPostings.size() > minTerms) {
				final List<List<LongArray>> postingsSublists = ListUtils
						.getSortedSublists(titleTermsPostings, minTerms);
				for (List<LongArray> postingsSublist : postingsSublists) {
					LongArray sublistIntersection = postingsSublist.get(0);
					for (int i = 1; i < postingsSublist.size(); i ++) {
						sublistIntersection = LongArray
								.intersection(sublistIntersection, postingsSublist.get(i));
					}
					titlePostings = null == titlePostings ? sublistIntersection :
						LongArray.union(titlePostings, sublistIntersection);
				}
			} else {
				titlePostings = titleTermsPostings.get(0);
				for (int i = 1; i < titleTermsPostings.size(); i ++) {
					titlePostings = LongArray
							.intersection(titlePostings, titleTermsPostings.get(i));
					if (null == titlePostings || titlePostings.isEmpty()) {
						return null;
					}
				}
			}
		} else if (titleTermsPostings.size() > 1) {
			final LongArray leftArray = titleTermsPostings.get(0);
			final LongArray rightArray = titleTermsPostings.get(1);
			titlePostings = LongArray.intersection(leftArray, rightArray);
		} else if (titleTermsPostings.size() > 0) {
			titlePostings = titleTermsPostings.get(0);
		}
		if (null == titlePostings || titlePostings.isEmpty()) {
			return null;
		}

		// skip useless artist fuzzy searches
		boolean fuzzyArtist = false;
		for (String artistTerm : artistTerms) {
			if (artistTerm.length() >= fuzzyCosts[0]) {
				fuzzyArtist = true;
				break;
			}
		}
		// fuzzy vs. regular artist search
		LongArray resultPostings = null;
		for (String term : artistTerms) {
			LongArray postings = fuzzyArtist ?
					fuzzySearch(artistIndex, term) :
						exactSearch(artistIndex, term);
			// compute artist terms union of intersections with title postings
			if (null != postings && !postings.isEmpty()) {
				if (null == resultPostings) {
					resultPostings = LongArray.intersection(titlePostings, postings);
				} else {
					postings = LongArray.intersection(titlePostings, postings);
					resultPostings = LongArray.union(postings, resultPostings);
				}
			}
		}

		return resultPostings;
	}
	
}
