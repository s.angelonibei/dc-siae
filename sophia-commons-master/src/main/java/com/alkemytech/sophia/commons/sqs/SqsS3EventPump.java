package com.alkemytech.sophia.commons.sqs;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.amazonaws.services.s3.AmazonS3URI;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class SqsS3EventPump {
	
	public static interface Consumer {

		public void consume(String bucket, String key);

	}

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final SQS sqs;
	private final SqsS3Events s3Events;
	private final String queueUrl;
	private final long pollingDelayMilllis;
	private final long maxIdleMillis;
	private final int filesPerRun;
	private final Pattern keyRegex;
	private final boolean deleteNonWatched;

	public SqsS3EventPump(SQS sqs, SqsS3Events s3Events, Properties configuration, String propertyPrefix) {
		super();
		this.sqs = sqs;
		this.s3Events = s3Events;
		this.queueUrl = sqs.getOrCreateUrl(configuration
				.getProperty(propertyPrefix + ".queue_name"));
		this.pollingDelayMilllis = Long.parseLong(configuration
				.getProperty(propertyPrefix + ".polling_delay_ms", "5000")); // default 5 seconds
		this.maxIdleMillis = Long.parseLong(configuration
				.getProperty(propertyPrefix + ".max_idle_ms", "60000")); // default 1 minute
		this.filesPerRun = Math.max(1, Integer.parseInt(configuration
				.getProperty(propertyPrefix + ".files_per_run", "1"))); // default 1
		this.keyRegex = Pattern.compile(configuration
				.getProperty(propertyPrefix + ".key_regex"));
		this.deleteNonWatched = "true".equalsIgnoreCase(configuration
				.getProperty(propertyPrefix + ".delete_non_watched", "true")); // default true
	}

	public void pollingLoop(Consumer consumer) throws UnsupportedEncodingException, InterruptedException {
		
		final long startTimeMillis = System.currentTimeMillis();
		final AtomicInteger remainingFiles = new AtomicInteger(filesPerRun);
		while (remainingFiles.get() > 0) {

			// idle for too long
			if (System.currentTimeMillis() - startTimeMillis > maxIdleMillis) {
				logger.debug("pollingLoop: idle for too long {}", TextUtils
						.formatDuration(System.currentTimeMillis() - startTimeMillis));
				break;
			}

			// poll SQS queue
			logger.debug("pollingLoop: polling queue url {}", queueUrl);
			final List<SQS.Msg> messages = sqs.receiveMessages(queueUrl);
			if (!messages.isEmpty()) {
				logger.debug("pollingLoop: {} message(s) received {}", messages.size(), messages);

				// parse messag(es)
				for (SQS.Msg message : messages) {
					int nonWatchedObjects = 0;
					int totalObjects = 0;
					for (AmazonS3URI uri : s3Events.parseMessage(message)) {
						final String s3Bucket = URLDecoder.decode(uri.getBucket(), "UTF-8");
						final String s3Key = URLDecoder.decode(uri.getKey(), "UTF-8");
						if (keyRegex.matcher(s3Key).matches()) {
							remainingFiles.decrementAndGet();
							consumer.consume(s3Bucket, s3Key);
						} else {
							nonWatchedObjects ++;
						}
						totalObjects ++;
					}
					// remove message
					if (0 == nonWatchedObjects || deleteNonWatched) {
						if (sqs.deleteMessage(queueUrl, message.receiptHandle)) {
							logger.debug("pollingLoop: deleted message {}", message.receiptHandle);
						} else {
							logger.warn("pollingLoop: unable to delete message {}", message.receiptHandle);
						}
					}
					if (nonWatchedObjects > 0) {
						// TODO send message without watched object(s)
						logger.warn("pollingLoop: message in-flight with {}/{} non watched objects", nonWatchedObjects, totalObjects);
					}
				}

			} else {
				// slow down
				Thread.sleep(pollingDelayMilllis);
			}

		}
		logger.info("pollingLoop: exiting polling loop after {}", TextUtils
				.formatDuration(System.currentTimeMillis() - startTimeMillis));
		
	}

}
