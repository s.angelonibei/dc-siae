package com.alkemytech.sophia.commons.module;

import java.util.Properties;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface DynamicModule {

	/**
	 * Initialize this search engine instance.
	 * 
	 * @param configuration configuration properties
	 * @throws DynamicModuleException
	 */
	public void startup(Properties configuration) throws DynamicModuleException;
	
	/**
	 * Finalize this search engine instance.
	 */
	public void shutdown();
	
}
