package com.alkemytech.sophia.commons.util;

import java.nio.CharBuffer;
import java.util.Arrays;
import java.util.Iterator;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public final class SplitCharSequence implements TokenizableText {
	
	private final char delimiter;
	private final char[] array;
	private final CharBuffer buffer;

	private int position;
	
	public SplitCharSequence(String text) {
		this(text, ' ');
	}
	
	public SplitCharSequence(String text, char delimiter) {
		super();
		this.delimiter = delimiter;
		this.array = text.toCharArray();
		this.buffer = CharBuffer.wrap(array);
		for (position = 0; position < array.length &&
				delimiter == array[position]; position ++);
	}
	
	public char[] toCharArray() {
		return array;
	}
	
	@Override
	public char charAt(int index) {
		return array[index];
	}

	@Override
	public int length() {
		return array.length;
	}
	
	@Override
	public CharBuffer subSequence(int start, int end) {
		return buffer.subSequence(start, end);
	}

	@Override
	public boolean hasNext() {
		return position < array.length;
	}

	@Override
	public CharBuffer next() {
		int limit = position;
		for (; limit < array.length &&
				delimiter != array[limit]; limit ++);
		buffer.limit(limit).position(position);
		for (position = limit; position < array.length &&
				delimiter == array[position]; position ++);
		return buffer;
	}
	
	@Override
	public Iterator<CharSequence> iterator() {
		for (position = 0; position < array.length &&
				delimiter == array[position]; position ++);
		return this;
	}

	@Override
	public int hashCode() {		
		return Arrays.hashCode(array);
	}

	@Override
	public boolean equals(Object object) {
		if (this == object)
			return true;
		if (null == object)
			return false;
		if (getClass() != object.getClass())
			return false;
		final SplitCharSequence other = (SplitCharSequence) object;
		return Arrays.equals(array, other.array);
	}
	
	@Override
	public String toString() {
		return new String(array);
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
	
}
