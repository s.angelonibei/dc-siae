package com.alkemytech.sophia.commons.index;

import com.alkemytech.sophia.commons.util.LongArray;

/**
 * A reverse index supporting exact and approximate string matching.
 * 
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface LevenshteinIndex {

	/**
	 * Search result as a set of term(s).
	 */
	public interface TermSet {
		
		/**
		 * The empty term set.
		 */
		public static final TermSet EMPTY = new TermSet() {
			
			@Override
			public int size() throws LevenshteinIndexException {
				return 0;
			}
			
			@Override
			public boolean next() throws LevenshteinIndexException {
				return false;
			}
			
			@Override
			public String getTerm() throws LevenshteinIndexException {
				throw new IllegalStateException("after last");
			}
			
			@Override
			public LongArray getPostings() throws LevenshteinIndexException {
				throw new IllegalStateException("after last");
			}

		};

		/**
		 * Number of terms.
		 */
		public int size() throws LevenshteinIndexException;
		
		/**
		 * Move to next term.
		 */
		public boolean next() throws LevenshteinIndexException;

		/**
		 * Get current term string.
		 */
		public String getTerm() throws LevenshteinIndexException;

		/**
		 * Get postings associated with current term.
		 */
		public LongArray getPostings() throws LevenshteinIndexException;

	}

	/**
	 * Index name.
	 */
	public String getName() throws LevenshteinIndexException;
	
	/**
	 * Read only mode.
	 */
	public boolean isReadOnly() throws LevenshteinIndexException;

	/**
	 * Search a text (exact match).
	 */
	public TermSet search(String text) throws LevenshteinIndexException;

	/**
	 * Search a text up to a maximum edit distance (approximate match).
	 */
	public TermSet search(String text, int maxDistance) throws LevenshteinIndexException;
	
}
