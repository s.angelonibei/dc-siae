package com.alkemytech.sophia.commons.text;

import java.util.Collection;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface TextTokenizer {
	
	public TextTokenizer clear();
	public TextTokenizer tokenize(String text);
	public Collection<String> getTokens();
	
}
