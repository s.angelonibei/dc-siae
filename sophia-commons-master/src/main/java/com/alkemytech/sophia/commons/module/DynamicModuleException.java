package com.alkemytech.sophia.commons.module;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@SuppressWarnings("serial")
public class DynamicModuleException extends RuntimeException {

	public DynamicModuleException(String message, Throwable cause) {
		super(message, cause);
	}

	public DynamicModuleException(String message) {
		super(message);
	}

	public DynamicModuleException(Throwable cause) {
		super(cause);
	}

}
