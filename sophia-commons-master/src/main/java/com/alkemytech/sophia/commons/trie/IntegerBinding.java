package com.alkemytech.sophia.commons.trie;

import com.alkemytech.sophia.commons.mmap.MemoryMap;
import com.alkemytech.sophia.commons.mmap.MemoryMapException;
import com.alkemytech.sophia.commons.util.SizeInBytes;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class IntegerBinding implements LeafBinding<Integer> {

	private final int nullValue;
	
	public IntegerBinding(int nullValue) {
		super();
		this.nullValue = nullValue;
	}
	
	@Override
	public int sizeOfLeaves() {
		return SizeInBytes.INT;
	}

	@Override
	public Integer read(MemoryMap buffer) throws MemoryMapException {
		final int leaf = buffer.getInt();
		return nullValue == leaf ? null : leaf;
	}

	@Override
	public void write(MemoryMap buffer, Integer leaf) throws MemoryMapException {
		buffer.putInt(null == leaf ? nullValue : leaf);
	}

}
