package com.alkemytech.sophia.commons.metric;

import java.util.HashMap;
import java.util.Map;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class NGrams {
	
	protected static Map<String, Integer> ngrams(String string, int n) {
		final HashMap<String, Integer> ngrams = new HashMap<String, Integer>();
		for (int i = 0; i < string.length() - n + 1; i ++) {
            final String ngram = string.substring(i, i + n);
            final Integer count = ngrams.get(ngram);
            ngrams.put(ngram, null == count ? 1 : count + 1);
        }
		return ngrams;
	}

}
