package com.alkemytech.sophia.commons.util;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface SizeInBytes {

	public static final int BYTE = 1;
	public static final int CHAR = 2;
	public static final int SHORT = 2;
	public static final int INT = 4;
	public static final int FLOAT = 4;
	public static final int LONG = 8;
	public static final int DOUBLE = 8;
	
}
