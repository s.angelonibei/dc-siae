package com.alkemytech.sophia.commons.aws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazon.sqs.javamessaging.AmazonSQSExtendedClient;
import com.amazon.sqs.javamessaging.ExtendedClientConfiguration;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.auth.PropertiesFileCredentialsProvider;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.DeleteMessageResult;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.QueueDeletedRecentlyException;
import com.amazonaws.services.sqs.model.QueueDoesNotExistException;
import com.amazonaws.services.sqs.model.QueueNameExistsException;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class SQS {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	public static class Msg {
		
		public final String text;
		public final String receiptHandle;
		
		public Msg(String text, String receiptHandle) {
			super();
			this.text = text;
			this.receiptHandle = receiptHandle;
		}
		
		@Override
		public int hashCode() {
			int result = 31 + (null == receiptHandle ? 0 : receiptHandle.hashCode());
			return 31 * result + (null == text ? 0 : text.hashCode());
		}

		@Override
		public boolean equals(Object object) {
			if (this == object)
				return true;
			if (null == object)
				return false;
			if (getClass() != object.getClass())
				return false;
			final Msg other = (Msg) object;
			if (null == receiptHandle) {
				if (null != other.receiptHandle)
					return false;
			} else if (!receiptHandle.equals(other.receiptHandle))
				return false;
			if (null == text) {
				if (null != other.text)
					return false;
			} else if (!text.equals(other.text))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return text;
		}
	
	}
	
	private final Properties configuration;
	
	AmazonSQS sqsClient;
	@Inject
	public SQS(@Named("configuration") Properties configuration) {
		super();
		this.configuration = configuration;
	}
	
	public synchronized SQS startup() {
		if (null == sqsClient) {
			final String proxyHost = configuration.getProperty("http.proxy.host");
			final String proxyPort = configuration.getProperty("http.proxy.port", "8080");
		    final ClientConfiguration clientConfiguration = new ClientConfiguration();
		    if (!Strings.isNullOrEmpty(proxyHost)) {
		    	clientConfiguration.setProxyHost(proxyHost);
		    	clientConfiguration.setProxyPort(Integer.parseInt(proxyPort));
		    }
			final String region = configuration.getProperty("aws.region", "eu-west-1");
			final String endpoint = configuration
					.getProperty("sqs.endpoint", "https://sqs.eu-west-1.amazonaws.com");
		    final String credentialsFilePath = configuration.getProperty("aws.credentials");
		    final AWSCredentialsProvider credentials = Strings.isNullOrEmpty(credentialsFilePath) ?
		    		new ClasspathPropertiesFileCredentialsProvider() :
		    			new PropertiesFileCredentialsProvider(credentialsFilePath);
			sqsClient = AmazonSQSClientBuilder.standard()
				.withCredentials(credentials)
				.withClientConfiguration(clientConfiguration)
				.withEndpointConfiguration(new EndpointConfiguration(endpoint, region))
				.build();
		}
		return this;
	}
		
	public synchronized SQS shutdown() {
		if (null != sqsClient) {
			sqsClient.shutdown();
			sqsClient = null;
		}
		return this;
	}

	public List<String> getQueueUrls() {
		return sqsClient.listQueues()
				.getQueueUrls();
	}

	public String getUrl(String queueName) {
		
		// get existing queue url
		logger.debug("opening queue {}", queueName);
		try {
			return sqsClient
					.getQueueUrl(new GetQueueUrlRequest(queueName))
					.getQueueUrl();
		} catch (QueueDoesNotExistException e) {
			logger.error("getUrl", e);
		}
		
		// error
		return null;
		
	}
	
	public String getOrCreateUrl(String queueName) {
		
		// get existing queue url
		logger.debug("opening queue {}", queueName);
		try {
			return sqsClient
					.getQueueUrl(new GetQueueUrlRequest(queueName))
					.getQueueUrl();
		} catch (QueueDoesNotExistException e) {
			logger.error("getOrCreateUrl", e);
		}
		
		// create new queue
		logger.debug("getOrCreateUrl: creating queue {}", queueName);
		try {
			final Map<String, String> attributes = new HashMap<String, String>();
			attributes.put("VisibilityTimeout", configuration
					.getProperty("sqs.visibility_timeout", "300")); // default to 5 minutes
			return sqsClient.createQueue(new CreateQueueRequest()
				.withQueueName(queueName)
				.withAttributes(attributes))
					.getQueueUrl();
		} catch (QueueNameExistsException | QueueDeletedRecentlyException  e) {
			logger.error("getOrCreateUrl", e);
		}
		
		// error
		return null;
		
	}
	
	public boolean sendMessage(String queueUrl, String text) {
        final SendMessageRequest request = new SendMessageRequest()
        	.withMessageBody(text)
        	.withQueueUrl(queueUrl);
        
		final SendMessageResult result = sqsClient.sendMessage(request);
		return null != result && null != result.getSdkHttpMetadata() && 
				200 == result.getSdkHttpMetadata().getHttpStatusCode();
	}

	public List<Msg> receiveMessages(String queueUrl) {
		final Integer maxNumberOfMessages = Integer.valueOf(configuration
				.getProperty("sqs.max_number_of_messages", "1"));
		return receiveMessages(queueUrl, maxNumberOfMessages);
	}

	public List<Msg> receiveMessages(String queueUrl, Integer maxNumberOfMessages) {
        final ReceiveMessageRequest request = new ReceiveMessageRequest()
        	.withQueueUrl(queueUrl)
        	.withMaxNumberOfMessages(maxNumberOfMessages);
        final List<Msg> result = new ArrayList<Msg>();
        final List<Message> messages = sqsClient
        		.receiveMessage(request).getMessages();        
        if (null != messages) {
        	for (Message message : messages)
	        	result.add(new Msg(message.getBody(),
	        			message.getReceiptHandle()));
        }
        return result;
	}
	
	public boolean deleteMessage(String queueUrl, String receiptHandle) {
		final DeleteMessageRequest request = new DeleteMessageRequest()
			.withQueueUrl(queueUrl)
			.withReceiptHandle(receiptHandle);
        final DeleteMessageResult result = sqsClient.deleteMessage(request);
		return null != result && null != result.getSdkHttpMetadata() && 
				200 == result.getSdkHttpMetadata().getHttpStatusCode();
	}
	
	
	
	
	public boolean sendExtendedMessage(String queueUrl, String text,S3 s3,String S3_BUCKET_NAME) {
		ExtendedClientConfiguration extendedClientConfig =
        new ExtendedClientConfiguration()
                .withLargePayloadSupportEnabled(s3.getClient(), S3_BUCKET_NAME);

		AmazonSQS sqsExtended = new AmazonSQSExtendedClient(sqsClient,extendedClientConfig);
		final SendMessageRequest request = new SendMessageRequest().withMessageBody(text).withQueueUrl(queueUrl);
		final SendMessageResult result = sqsExtended.sendMessage(request);
		return null != result && null != result.getSdkHttpMetadata() && 
				200 == result.getSdkHttpMetadata().getHttpStatusCode();
	}
	public List<Msg> receiveExtendedMessages(String queueUrl,S3 s3,String S3_BUCKET_NAME) {
		final Integer maxNumberOfMessages = Integer.valueOf(configuration
				.getProperty("sqs.max_number_of_messages", "1"));
		return receiveExtendedMessages(queueUrl, maxNumberOfMessages,s3,S3_BUCKET_NAME);
	}

	public List<Msg> receiveExtendedMessages(String queueUrl, Integer maxNumberOfMessages,S3 s3,String S3_BUCKET_NAME) {
		logger.debug("opening queue  Extended mode");
		ExtendedClientConfiguration extendedClientConfig =
		        new ExtendedClientConfiguration()
		                .withLargePayloadSupportEnabled(s3.getClient(), S3_BUCKET_NAME);
		AmazonSQS sqsExtended = new AmazonSQSExtendedClient(sqsClient,extendedClientConfig);
        final ReceiveMessageRequest request = new ReceiveMessageRequest()
        	.withQueueUrl(queueUrl)
        	.withMaxNumberOfMessages(maxNumberOfMessages);
        final List<Msg> result = new ArrayList<Msg>();
        final List<Message> messages = sqsExtended.receiveMessage(request).getMessages();        
        if (null != messages) {
        	for (Message message : messages)
	        	result.add(new Msg(message.getBody(),
	        			message.getReceiptHandle()));
        }
        return result;
	}
	
	public boolean deleteExtendedMessage(String queueUrl, String receiptHandle,S3 s3,String S3_BUCKET_NAME) {
		final DeleteMessageRequest request = new DeleteMessageRequest()
			.withQueueUrl(queueUrl)
			.withReceiptHandle(receiptHandle);
		logger.debug("delete message:"+queueUrl + ":" + receiptHandle + ":" + S3_BUCKET_NAME);
		ExtendedClientConfiguration extendedClientConfig =
		        new ExtendedClientConfiguration()
		                .withLargePayloadSupportEnabled(s3.getClient(), S3_BUCKET_NAME);
		AmazonSQS sqsExtended = new AmazonSQSExtendedClient(sqsClient,extendedClientConfig);
        final DeleteMessageResult result = sqsExtended.deleteMessage(request);
		return null != result && null != result.getSdkHttpMetadata() && 
				200 == result.getSdkHttpMetadata().getHttpStatusCode();
		//bug della versione 1.0.0
		//sqsExtended.deleteMessage(queueUrl,receiptHandle);
//		return true;
	}
	

}
