package com.alkemytech.sophia.commons.util;

import java.math.BigDecimal;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class BigDecimals {
	
	private static final Logger logger = LoggerFactory.getLogger(BigDecimals.class);

	private static int scale = 20;
	private static BigDecimal tolerance = new BigDecimal(1e-10);
	
	public static boolean isAlmostZero(BigDecimal number) {
		return number.compareTo(tolerance) < 0;
	}
	
	public static boolean almostEquals(BigDecimal number, BigDecimal target) {
		return number.subtract(target).abs()
				.compareTo(tolerance) < 0;
	}

	public static boolean isReallyGreaterThan(BigDecimal number, BigDecimal target) {
		return number.compareTo(target.add(tolerance)) > 0;
	}

	public static boolean isReallyLessThan(BigDecimal number, BigDecimal target) {
		return number.compareTo(target.subtract(tolerance)) < 0;
	}

	public static BigDecimal setScale(BigDecimal number) {
		return number.setScale(scale, BigDecimal.ROUND_HALF_UP);
	}
	
	public static BigDecimal divide(BigDecimal number, BigDecimal divisor) {
		return number.divide(divisor, scale + 2, BigDecimal.ROUND_HALF_UP)
				.setScale(scale, BigDecimal.ROUND_HALF_UP);
	}
	
	public static String toPlainString(BigDecimal number) {
		if (isAlmostZero(number))
			return "0";
		return number.setScale(scale, BigDecimal.ROUND_HALF_UP)
				.stripTrailingZeros().toPlainString();
	}
	
	@Inject
	protected BigDecimals(@Named("configuration") Properties configuration) {
		super();
		BigDecimals.scale = Integer.parseInt(configuration
				.getProperty("big_decimal.scale",
						Integer.toString(scale)));
		BigDecimals.tolerance = new BigDecimal(configuration
				.getProperty("big_decimal.tolerance",
						tolerance.toPlainString()));
		logger.info("scale {}", scale);
		logger.info("tolerance {}", tolerance);
	}
	
}
