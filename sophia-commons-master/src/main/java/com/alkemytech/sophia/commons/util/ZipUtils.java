package com.alkemytech.sophia.commons.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class ZipUtils {

	private static final Logger logger = LoggerFactory.getLogger(ZipUtils.class);

	public static void archiveFromFolder(File folder, File archive) throws IOException {
		logger.debug("creating {} from folder {}",
				archive.getName(), folder.getAbsolutePath());
		try (final FileOutputStream fileOutputStream = new FileOutputStream(archive);
				final BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
				final ZipOutputStream zip = new ZipOutputStream(bufferedOutputStream)) {
			final byte[] buffer = new byte[4096];
			for (File file : folder.listFiles()) {
				logger.debug("adding file {} ({})", file.getName(),
						TextUtils.formatSize(file.length()));
				try (final FileInputStream fileInputStream = new FileInputStream(file);
						final BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream)) {
					zip.putNextEntry(new ZipEntry(file.getName()));
					for (int length; (length = fileInputStream.read(buffer)) > 0; )
						zip.write(buffer, 0, length);
					zip.closeEntry();
				}
			}
		}
		logger.debug("zip file size {}", TextUtils.formatSize(archive.length()));
	}

	public static void folderFromArchive(File archive, File folder) throws IOException {
		logger.debug("extracting {} to folder {}",
				archive.getName(), folder.getAbsolutePath());
		folder.mkdirs();
		long uncompressedSize = 0L;
		try (final FileInputStream fileInputStream = new FileInputStream(archive);
				final BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
				final ZipInputStream zip = new ZipInputStream(bufferedInputStream)) {
			final byte[] buffer = new byte[4096];
			for (ZipEntry zipEntry; null != (zipEntry = zip.getNextEntry()); ) {
				if (!zipEntry.isDirectory()) {
					final File file = new File(folder, zipEntry.getName());
					try (final FileOutputStream fileOutputStream = new FileOutputStream(file);
							final BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream)) {
						for (int length; (length = zip.read(buffer)) > 0; )
							bufferedOutputStream.write(buffer, 0, length);						
					}
					uncompressedSize += file.length();
					logger.debug("extracted file {} ({})", file.getName(),
							TextUtils.formatSize(file.length()));
				}
			}
		}
		logger.debug("uncompressed size {}", TextUtils.formatSize(uncompressedSize));
	}

//	public static void main(String[] args) throws Exception {		
//		zipFromFolder(new File("/Users/roberto/Temp/guzzutapp"), new File("/Users/roberto/Temp/uuuuu.zip"));
//		folderFromZip(new File("/Users/roberto/Temp/uuuuu.zip"), new File("/Users/roberto/Temp/guzzutapp2"));
//	}
	
}
