package com.alkemytech.sophia.commons.collecting;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.regex.Pattern;

import com.alkemytech.sophia.commons.text.TextNormalizer;
import com.google.common.base.Strings;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class AdaCode {

	private static final Pattern IS_VALID_REGEX = Pattern
			.compile("^(-?)(0|([1-9][0-9]*))(\\.[0-9]+)?$");
	
	public static final TextNormalizer NORMALIZER = new TextNormalizer() {
		
		@Override
		public String normalize(String text) {
			return AdaCode.normalize(text);
		}
		
	};
	
	public static String normalize(String code) {
		if (Strings.isNullOrEmpty(code))
			return null;
		final int length = code.length();
		if (length > 24)
			throw new IllegalArgumentException(String.format("code too long %d \"%s\"", length, code));
		
		if(IS_VALID_REGEX.matcher(code).matches()) {
	        DecimalFormat df = new DecimalFormat("000000000.000000000");

	        try {
				return df.format(df.parse(code).doubleValue());
			} catch (ParseException e) {
				throw new IllegalArgumentException(String.format("wrong formatting code %d \"%s\"", length, code));
			}
		}else {
			throw new IllegalArgumentException(String.format("wrong formatting code %d \"%s\"", length, code));
		}		
	}
	
	public static boolean isValid(String code) {
		if (Strings.isNullOrEmpty(code)) {
			return false;
		}
		return IS_VALID_REGEX.matcher(code).matches();
	}
	
}
