package com.alkemytech.sophia.commons.trie;

import com.alkemytech.sophia.commons.mmap.MemoryMap;
import com.alkemytech.sophia.commons.mmap.MemoryMapException;

/**
 * Constant size trie leaves binding.
 * 
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface LeafBinding<T> {

	/**
	 * (Constant) size in bytes of serialized leaves.
	 * 
	 * @return the (constant) size in bytes of serialized leaves.
	 */
	public int sizeOfLeaves();
	
	/**
	 * Reads a trie leaf from underlying memory map.
	 * 
	 * @param buffer the underlying memory map.
	 * @return the read leaf
	 * @throws MemoryMapException
	 */
	public T read(MemoryMap buffer) throws MemoryMapException;
	
	/**
	 * Writes a trie leaf to the underlying memory map.
	 * 
	 * @param buffer the underlying memory map.
	 * @param leaf the leaf to write.
	 * @throws MemoryMapException
	 */
	public void write(MemoryMap buffer, T leaf) throws MemoryMapException;

}
