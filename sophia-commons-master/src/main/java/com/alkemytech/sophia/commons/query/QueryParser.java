package com.alkemytech.sophia.commons.query;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.alkemytech.sophia.commons.index.LevenshteinIndex;
import com.alkemytech.sophia.commons.util.LongArray;
import com.fathzer.soft.javaluator.AbstractEvaluator;
import com.fathzer.soft.javaluator.BracketPair;
import com.fathzer.soft.javaluator.Operator;
import com.fathzer.soft.javaluator.Parameters;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class QueryParser extends AbstractEvaluator<LongArray> {

	private static final Operator AND = new Operator("&&", 2, Operator.Associativity.LEFT, 2);
	private static final Operator OR = new Operator("||", 2, Operator.Associativity.LEFT, 1);
	
	public static QueryParser create() {
		final Parameters parameters = new Parameters();
		parameters.add(AND);
		parameters.add(OR);
		parameters.addExpressionBracket(BracketPair.PARENTHESES);
		return new QueryParser(parameters);
	}
	
	private final Map<String, LevenshteinIndex> levenshteinIndexes;
	
	private QueryParser(Parameters parameters) {
		super(parameters);
		this.levenshteinIndexes = new HashMap<String, LevenshteinIndex>();
	}
	
	public final QueryParser addIndex(LevenshteinIndex index) {
		levenshteinIndexes.put(index.getName(), index);
		return this;
	}

	@Override
	protected LongArray toValue(String literal, Object evaluationContext) {
//		logger.debug("evaluating literal \"{}\"", literal);
		final int colon = literal.indexOf(':');
		if (-1 == colon) {
			throw new IllegalArgumentException(String
					.format("invalid literal \"%s\"", literal));
		}
		final String prefix = literal.substring(0, colon);
		final int tilde = literal.indexOf('~', colon);
		final String text;
		final int maxDistance;
		if (-1 == tilde) {
			text = literal.substring(colon + 1);
			maxDistance = 0;
		} else {
			text = literal.substring(colon + 1, tilde);
			maxDistance = Integer.parseInt(literal.substring(tilde + 1));
		}
		final LevenshteinIndex levenshteinIndex = levenshteinIndexes.get(prefix);
		if (null == levenshteinIndex) {
			throw new IllegalArgumentException(String
					.format("unknown prefix \"%s\"", prefix));
		}
//		logger.debug("prefix:\"{}\", text:\"{}\", maxDistance:{}", prefix, text, maxDistance);
		final LevenshteinIndex.TermSet termSet = maxDistance > 0 ? 
				levenshteinIndex.search(text, maxDistance) :
					levenshteinIndex.search(text);
		if (termSet.next()) {
			LongArray postings = termSet.getPostings();
			while (termSet.next()) {
				postings = LongArray.union(postings, termSet.getPostings());
			}
			return postings;
		}
		return new LongArray();
	}
		
	@Override
	protected LongArray evaluate(Operator operator, Iterator<LongArray> operands, Object evaluationContext) {
		final LongArray leftOperand = operands.next();
		final LongArray rightOperand = operands.next();
		if (AND == operator) {
//			logger.debug("intersection of {} && {}", leftOperand.size(), rightOperand.size());
			return LongArray.intersection(leftOperand, rightOperand);
		} else if (OR == operator) {
//			logger.debug("union of {} || {}", leftOperand.size(), rightOperand.size());
			return LongArray.union(leftOperand, rightOperand);
		}
		throw new IllegalArgumentException(String
				.format("unknown operator \"%s\"", operator.getSymbol()));
	}
	
}
