package com.alkemytech.sophia.commons.sqs;

import java.util.List;
import java.util.Properties;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.google.common.base.Strings;
import com.google.gson.JsonObject;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class SqsMessagePump {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	public static interface Consumer {
		
		/**
		 * Returns a context json object if started message should be sent, <code>null</code> otherwise.
		 * <br/>
		 * This method is always invoked before {@link consumeMessage()}.
		 * 
		 * @return a context json object if started message should be sent, <code>null</code> otherwise.
		 */
		public JsonObject getStartedMessagePayload(JsonObject message);

		/**
		 * Process a standard json message.
		 * 
		 * @param message the json message to process
		 * @return <code>true</code> if message was successfully processed, <code>false</code> otherwise.
		 */
		public boolean consumeMessage(JsonObject message);
		
		/**
		 * Returns a output json object if completed message should be sent, <code>null</code> otherwise.
		 * <br/>
		 * This method is invoked only if {@link consumeMessage()} returns <code>true</code>.
		 * 
		 * @return a output json object if completed message should be sent, <code>null</code> otherwise.
		 */
		public JsonObject getCompletedMessagePayload(JsonObject message);

		/**
		 * Returns a error json object if completed message should be sent, <code>null</code> otherwise.
		 * <br/>
		 * This method is invoked only if {@link consumeMessage()} returns <code>false</code>.
		 * 
		 * @return a error json object if completed message should be sent, <code>null</code> otherwise.
		 */
		public JsonObject getFailedMessagePayload(JsonObject message);

	}

	private final boolean sqsExtend;
	private final String sqsExtendBuketName;
	private final SQS sqs;
	private final S3 s3;
	private final int maxMessagesPerRun;
	private final long maxPollingDurationMillis;
	private final long pollingPeriod;
	private final String senderName;
	private final String serviceBusQueue;
	private final String toProcessQueue;
	private final String startedQueue;
	private final String completedQueue;
	private final String failedQueue;
	public SqsMessagePump(SQS sqs, Properties configuration, String propertyPrefix,S3 s3) {
		super();
		this.sqs = sqs;
		this.s3 = s3;
		this.sqsExtend =  Boolean.parseBoolean(configuration
				.getProperty(propertyPrefix + ".extends_messages", "false")); // default to false message;
		this.sqsExtendBuketName = configuration
				.getProperty(propertyPrefix + ".extends_buket_name");
		
		this.maxMessagesPerRun = Integer.parseInt(configuration
				.getProperty(propertyPrefix + ".max_messages_per_run", "1")); // default to 1 message
		this.maxPollingDurationMillis = TextUtils.parseLongDuration(configuration
				.getProperty(propertyPrefix + ".max_polling_duration", "1m")); // default to 1 minute
		this.pollingPeriod = TextUtils.parseLongDuration(configuration
				.getProperty(propertyPrefix + ".polling_period", "5s")); // default to 5 seconds
		this.senderName = configuration
				.getProperty(propertyPrefix + ".sender_name");
		this.serviceBusQueue = configuration
				.getProperty(propertyPrefix + ".service_bus_queue");
		this.toProcessQueue = configuration
				.getProperty(propertyPrefix + ".to_process_queue");
		this.startedQueue = configuration
				.getProperty(propertyPrefix + ".started_queue");
		this.completedQueue = configuration
				.getProperty(propertyPrefix + ".completed_queue");
		this.failedQueue = configuration
				.getProperty(propertyPrefix + ".failed_queue");	
	}	
	public SqsMessagePump(SQS sqs, Properties configuration, String propertyPrefix) {
		super();
		this.s3 = null;
		this.sqs = sqs;
		this.sqsExtend =  Boolean.parseBoolean(configuration
				.getProperty(propertyPrefix + ".extends_messages", "false")); // default to false message;
		this.sqsExtendBuketName = configuration
				.getProperty(propertyPrefix + ".extends_buket_name","");
		this.maxMessagesPerRun = Integer.parseInt(configuration
				.getProperty(propertyPrefix + ".max_messages_per_run", "1")); // default to 1 message
		this.maxPollingDurationMillis = TextUtils.parseLongDuration(configuration
				.getProperty(propertyPrefix + ".max_polling_duration", "1m")); // default to 1 minute
		this.pollingPeriod = TextUtils.parseLongDuration(configuration
				.getProperty(propertyPrefix + ".polling_period", "5s")); // default to 5 seconds
		this.senderName = configuration
				.getProperty(propertyPrefix + ".sender_name");
		this.serviceBusQueue = configuration
				.getProperty(propertyPrefix + ".service_bus_queue");
		this.toProcessQueue = configuration
				.getProperty(propertyPrefix + ".to_process_queue");
		this.startedQueue = configuration
				.getProperty(propertyPrefix + ".started_queue");
		this.completedQueue = configuration
				.getProperty(propertyPrefix + ".completed_queue");
		this.failedQueue = configuration
				.getProperty(propertyPrefix + ".failed_queue");	
	}

	public void sendToProcessMessage(JsonObject body, boolean skipServiceBus) {
		sendToProcessMessage(toProcessQueue, body, skipServiceBus);
	}

	public void sendToProcessMessage(String queueName, JsonObject body, boolean skipServiceBus) {
		final String queueUrl = sqs.getOrCreateUrl(skipServiceBus || Strings
				.isNullOrEmpty(serviceBusQueue) ? queueName : serviceBusQueue);
		final JsonObject message = SqsMessageHelper
				.formatToProcessJson(queueName, UUID.randomUUID().toString(), senderName, body);
		logger.debug("queueUrl {}", queueUrl);
		logger.debug("message {}", message);
		if(sqsExtend && this.s3 != null)
			sqs.sendExtendedMessage(queueUrl, GsonUtils.toJson(message),this.s3,this.sqsExtendBuketName);
		else
			sqs.sendMessage(queueUrl, GsonUtils.toJson(message));
	}
	
	public void sendStartedMessage(JsonObject input, JsonObject context, boolean skipServiceBus) {
		sendStartedMessage(startedQueue, input, context, skipServiceBus);
	}

	public void sendStartedMessage(String queueName, JsonObject input, JsonObject context, boolean skipServiceBus) {
		final String queueUrl = sqs.getOrCreateUrl(skipServiceBus || Strings
				.isNullOrEmpty(serviceBusQueue) ? queueName : serviceBusQueue);
		final JsonObject message = SqsMessageHelper
				.formatStartedJson(queueName, senderName, input, context);
		logger.debug("queueUrl {}", queueUrl);
		logger.debug("message {}", message);
		if(sqsExtend && this.s3 != null)
			sqs.sendExtendedMessage(queueUrl, GsonUtils.toJson(message),this.s3,this.sqsExtendBuketName);
		else
			sqs.sendMessage(queueUrl, GsonUtils.toJson(message));
	}
	
	public void sendCompletedMessage(JsonObject input, JsonObject output, boolean skipServiceBus) {
		sendCompletedMessage(completedQueue, input, output, skipServiceBus);
	}

	public void sendCompletedMessage(String queueName, JsonObject input, JsonObject output, boolean skipServiceBus) {
		final String queueUrl = sqs.getOrCreateUrl(skipServiceBus || Strings
				.isNullOrEmpty(serviceBusQueue) ? queueName : serviceBusQueue);
		final JsonObject message = SqsMessageHelper
				.formatCompletedJson(queueName, senderName, input, output);
		logger.debug("queueUrl {}", queueUrl);
		logger.debug("message {}", message);
		if(sqsExtend  && this.s3 != null)
			sqs.sendExtendedMessage(queueUrl, GsonUtils.toJson(message),this.s3,this.sqsExtendBuketName);
		else
			sqs.sendMessage(queueUrl, GsonUtils.toJson(message));
	}
	
	public void sendFailedMessage(JsonObject input, JsonObject error, boolean skipServiceBus) {
		sendFailedMessage(failedQueue, input, error, skipServiceBus);
	}
	
	public void sendFailedMessage(String queueName, JsonObject input, JsonObject error, boolean skipServiceBus) {
		final String queueUrl = sqs.getOrCreateUrl(skipServiceBus || Strings
				.isNullOrEmpty(serviceBusQueue) ? queueName : serviceBusQueue);
		final JsonObject message = SqsMessageHelper
				.formatFailedJson(queueName, senderName, input, error);
		logger.debug("queueUrl {}", queueUrl);
		logger.debug("message {}", message);
		if(sqsExtend && this.s3 != null)
			sqs.sendExtendedMessage(queueUrl, GsonUtils.toJson(message),this.s3,this.sqsExtendBuketName);
		else
			sqs.sendMessage(queueUrl, GsonUtils.toJson(message));
	}
	
	public void pollingLoop(MessageDeduplicator deduplicator, Consumer handler) throws Exception {
		
		final String toProcessQueueUrl = sqs.getOrCreateUrl(toProcessQueue);
		logger.debug("toProcessQueueUrl {}", toProcessQueueUrl);
		
		final long startTimeMillis = System.currentTimeMillis();
		for (int receivedMessages = 0; receivedMessages < maxMessagesPerRun &&
				System.currentTimeMillis() - startTimeMillis < maxPollingDurationMillis; ) {
			
			// read messages from to_process queue
			List<SQS.Msg> messages = null;
			if(sqsExtend  && this.s3 != null)
				messages = sqs.receiveExtendedMessages(toProcessQueueUrl,this.s3,this.sqsExtendBuketName);
			else
				messages = sqs.receiveMessages(toProcessQueueUrl);
			
			logger.debug("{} message(s) received", messages.size());
			if (null == messages || messages.isEmpty()) {
				Thread.sleep(pollingPeriod);
				continue;
			}
			
			// loop on received message(s)
			for (SQS.Msg message : messages) {
				receivedMessages ++;
				try {
					
					// parse message json
					logger.debug("processing message {}", message.text);
					final JsonObject messageJson = GsonUtils.
							fromJson(message.text, JsonObject.class);
					
					// deduplicate message
					if (null != deduplicator && !deduplicator
							.deduplicate(SqsMessageHelper.getMessageUuid(messageJson), toProcessQueue)) {
						logger.warn("duplicate message");
					}
					
					// process message
					else {
						
						// send started message
						final JsonObject context = handler.getStartedMessagePayload(messageJson);
						if (null != context && !Strings.isNullOrEmpty(startedQueue)) {
							sendStartedMessage(startedQueue, messageJson, context, false);
						}

						// consume message
						if (handler.consumeMessage(messageJson)) {
							
							// send completed message
							final JsonObject output = handler.getCompletedMessagePayload(messageJson);
							if (null != output && !Strings.isNullOrEmpty(completedQueue)) {
								sendCompletedMessage(completedQueue, messageJson, output, false);
							}

						} else {
							
							// send failed message
							final JsonObject error = handler.getFailedMessagePayload(messageJson);
							if (null != error && !Strings.isNullOrEmpty(failedQueue)) {
								sendFailedMessage(failedQueue, messageJson, error, false);
							}

						}
						
					}
					// delete from to_process queue
					if(sqsExtend && this.s3 != null)
						sqs.deleteExtendedMessage(toProcessQueueUrl, message.receiptHandle,this.s3,this.sqsExtendBuketName);
					else
						sqs.deleteMessage(toProcessQueueUrl, message.receiptHandle);
					
				} catch (Exception e) {
					logger.error("messageLoop", e);
				}
			}
		}
		logger.info("exiting polling loop after {}", TextUtils
				.formatDuration(System.currentTimeMillis() - startTimeMillis));
		
	}
	
	
}
