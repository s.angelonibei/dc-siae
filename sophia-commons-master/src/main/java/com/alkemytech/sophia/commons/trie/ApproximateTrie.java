package com.alkemytech.sophia.commons.trie;

import java.util.List;

import com.alkemytech.sophia.commons.mmap.MemoryMapException;

/**
 * Fixed-size generic leaf value trie supporting approximate string matching on keys.
 * 
 * @see http://stevehanov.ca/blog/index.php?id=114
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface ApproximateTrie<T> extends ExactTrie<T> {

	public List<ScoredPath<T>> search(String text, int maxDistance) throws MemoryMapException;

}
