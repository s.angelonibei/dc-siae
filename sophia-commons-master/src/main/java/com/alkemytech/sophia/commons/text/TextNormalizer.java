package com.alkemytech.sophia.commons.text;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface TextNormalizer {
	
	public String normalize(String text);
	
}
