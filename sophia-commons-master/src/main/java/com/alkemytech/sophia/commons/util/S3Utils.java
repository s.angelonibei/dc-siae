package com.alkemytech.sophia.commons.util;

import com.alkemytech.sophia.commons.aws.S3;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

public final class S3Utils {

    private S3Utils() {
    }

    public static List<S3.Url> getS3UrlFromS3Message(JsonObject message) {
        JsonArray records = GsonUtils.getAsJsonArray(message, "Records");

        List<S3.Url> result = new ArrayList<>();

        for (JsonElement record : records) {
            JsonObject s3 = GsonUtils.getAsJsonObject(record.getAsJsonObject(), "s3");
            JsonObject bucket = GsonUtils.getAsJsonObject(s3, "bucket");
            String bucketName = GsonUtils.getAsString(bucket, "name");

            JsonObject object = GsonUtils.getAsJsonObject(s3, "object");
            String key = GsonUtils.getAsString(object, "key");

            result.add(new S3.Url(bucketName, key));
        }

        return result;
    }

    private static String uncheckedUrlDecode(String url)  {
        try {
            return URLDecoder.decode(url, "UTF-8");
        }catch(UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }

    }
}
