package com.alkemytech.sophia.commons.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class IOUtils {

	public static byte[] toByteArray(InputStream in, int bufferSize) throws IOException {
		final ByteArrayOutputStream out = new ByteArrayOutputStream(bufferSize);
		final byte[] buffer = new byte[bufferSize];
		for (int length; -1 != (length = in.read(buffer)); ) {
			out.write(buffer, 0, length);
		}
		return out.toByteArray();
	}
	
	public static byte[] toByteArray(InputStream in) throws IOException {
		return toByteArray(in, 256);
	}
	
}
