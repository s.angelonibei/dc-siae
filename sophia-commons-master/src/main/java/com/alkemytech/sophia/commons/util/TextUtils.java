package com.alkemytech.sophia.commons.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public final class TextUtils {
	
	// split string using single separator char
	public static String[] split(String text, char separator) {
		if (null == text)
			return null;
		if (text.isEmpty())
			return new String[] { "" };
		final ArrayList<String> array = new ArrayList<>();
		final int length = text.length();
		while (true) {
			int index = text.indexOf(separator);
			if (-1 == index) {
				array.add(text);
				break;
			}
			array.add(index > 0 ? text.substring(0, index) : "");
			index ++;
			text = index < length ? text.substring(index) : "";
		}
		return array.toArray(new String[array.size()]);
	}
	
	// split string using regular expression
	public static String[] split(String text, String regex) {
		if (null == text)
			return null;
		if (text.isEmpty())
			return new String[] { "" };
		return text.split(regex, -1);
	}
	
	// strip left chars
	public static final String lstrip(String string, char ch) {
		if (null == string) {
			return null;
		}
		final char[] array = string.toCharArray();
		int offset = 0;
		for (; offset < array.length && ch == array[offset] ; offset ++);
		return 0 == offset ?
				string : new String(array, offset, array.length - offset);
	}
	
	// strip right chars
	public static final String rstrip(String string, char ch) {
		if (null == string) {
			return null;
		}
		final char[] array = string.toCharArray();
		int length = array.length;
		for (; length > 0 && ch == array[length - 1] ; length --);
		return length == array.length ?
				string : new String(array, 0, length);
	}

	// left pad string up to length
	public static final String lpad(String string, int length, char pad) {
		length -= string.length();
		if (length > 0) {
			final StringBuilder builder = new StringBuilder();
			for (; length > 0; length --) {
				builder.append(pad);
			}
			return builder.append(string)
					.toString();
		}
		return string;
	}

	// right pad string up to length
	public static final String rpad(String string, int length, char pad) {
		length -= string.length();
		if (length > 0) {
			final StringBuilder builder = new StringBuilder()
					.append(string);
			for (; length > 0; length --)
				builder.append(pad);
			return builder.toString();
		}
		return string;
	}
	
	public static final String remove(final String string, final char ch) {
		if (null == string) {
			return null;
		}
		final char[] array = string.toCharArray();
		int length = 0;
		for (int i = 0; i < array.length; i ++) {
			if (ch != array[i]) {
				array[length ++] = array[i];
			}
		}
		return new String(array, 0, length);
	}
	
	public static final String nullValue(String text, String ifNull) {
		return null == text ? ifNull : text;
	}
	
	public static final String nullOrEmptyValue(String text, String ifNullOrEmpty) {
		return null == text || "".equals(text) ? ifNullOrEmpty : text;
	}

	public static List<String> parseCsv(String text, String delimiterRegex) {
		if (null == text)
			return null;
		if (text.isEmpty())
			return new ArrayList<>(0);
		return Arrays.asList(text.split(delimiterRegex, -1));
	}

	public static String formatCsv(Collection<String> texts, String delimiter) {
		if (null == texts)
			return null;
		if (texts.isEmpty())
			return "";
		final StringBuilder text = new StringBuilder();
		final Iterator<String> iterator = texts.iterator();
		text.append(iterator.next());
		while (iterator.hasNext()) {
			text.append(delimiter)
				.append(iterator.next());
		}
		return text.toString();
	}
	
	// parse int array
	public static int[] parseIntArray(String text, String splitRegex) {
		final String[] texts = text.split(splitRegex, -1);
		final int[] integers = new int[texts.length];
		for (int i = 0; i < texts.length; i ++) {
			integers[i] = Integer.parseInt(texts[i]);
		}
		return integers;
	}

	// parse strings containing a size
	public static int parseIntSize(String text) {
		text = text.toLowerCase();
		Matcher matcher = Pattern.compile("([-+]?\\d+)").matcher(text);
		if (matcher.matches()) {
			return Integer.parseInt(matcher.group(1));
		}
		// kilo
		matcher = Pattern.compile("([-+]?\\d+)(k|kb)").matcher(text);
		if (matcher.matches()) {
			return 1024 * Integer.parseInt(matcher.group(1));
		}
		// mega
		matcher = Pattern.compile("([-+]?\\d+)(m|mb)").matcher(text);
		if (matcher.matches()) {
			return 1024 * 1024 * Integer.parseInt(matcher.group(1));
		}
		// giga
		matcher = Pattern.compile("([-+]?\\d+)(g|gb)").matcher(text);
		if (matcher.matches()) {
			return 1024 * 1024 * 1024 * Integer.parseInt(matcher.group(1));
		}
		throw new IllegalArgumentException(String
				.format("not a valid size string \"%s\"", text));
	}
	
	// parse strings containing a size
	public static long parseLongSize(String text) {
		text = text.toLowerCase();
		Matcher matcher = Pattern.compile("([-+]?\\d+)").matcher(text);
		if (matcher.matches()) {
			return Long.parseLong(matcher.group(1));
		}
		// kilo
		matcher = Pattern.compile("([-+]?\\d+)(k|kb)").matcher(text);
		if (matcher.matches()) {
			return 1024L * Long.parseLong(matcher.group(1));
		}
		// mega
		matcher = Pattern.compile("([-+]?\\d+)(m|mb)").matcher(text);
		if (matcher.matches()) {
			return 1024L * 1024L * Long.parseLong(matcher.group(1));
		}
		// giga
		matcher = Pattern.compile("([-+]?\\d+)(g|gb)").matcher(text);
		if (matcher.matches()) {
			return 1024L * 1024L * 1024L * Long.parseLong(matcher.group(1));
		}
		// tera
		matcher = Pattern.compile("([-+]?\\d+)(t|tb)").matcher(text);
		if (matcher.matches()) {
			return 1024L * 1024L * 1024L * 1024L * Long.parseLong(matcher.group(1));
		}
		// peta
		matcher = Pattern.compile("([-+]?\\d+)(p|pb)").matcher(text);
		if (matcher.matches()) {
			return 1024L * 1024L * 1024L * 1024L * 1024L * Long.parseLong(matcher.group(1));
		}
		// exa
		matcher = Pattern.compile("([-+]?\\d+)(p|pb)").matcher(text);
		if (matcher.matches()) {
			return 1024L * 1024L * 1024L * 1024L * 1024L * 1024L * Long.parseLong(matcher.group(1));
		}
		throw new IllegalArgumentException(String
				.format("not a valid size string \"%s\"", text));
	}
	
	public static String formatSize(long size) {
		if (size > 1024L * 1024L * 1024L)
			return String.format("%.02fGb", (double) size / (1024.0 * 1024.0 * 1024.0));
		if (size > 1024L * 1024L)
			return String.format("%.02fMb", (double) size / (1024.0 * 1024.0));
		if (size > 1024L)
			return String.format("%.02fKb", (double) size / 1024.0);
		return String.format("%db", size);
	}
	
	public static int parseIntDuration(String text) {
		Matcher matcher = Pattern.compile("(\\d+d)?(\\d+h)?(\\d+m)?(\\d+s)?(\\d+ms)?").matcher(text);
		if (matcher.matches()) {
			String group;
			int duration = 0;
			if (null != (group = matcher.group(1))) {
				// WARNING: Integer.MAX_VALUE is about 24 days in milliseconds
				duration += 24 * 60 * 60 * 1000 * Math.min(23,
						Integer.parseInt(group.substring(0, group.length() - 1)));
			}
			if (null != (group = matcher.group(2))) {
				duration += 60 * 60 * 1000 * Integer.parseInt(group.substring(0, group.length() - 1));
			}
			if (null != (group = matcher.group(3))) {
				duration += 60 * 1000 * Integer.parseInt(group.substring(0, group.length() - 1));
			}
			if (null != (group = matcher.group(4))) {
				duration += 1000 * Integer.parseInt(group.substring(0, group.length() - 1));
			}
			if (null != (group = matcher.group(5))) {
				duration += Integer.parseInt(group.substring(0, group.length() - 2));
			}
			return duration;
		}
		throw new IllegalArgumentException(String
				.format("not a valid duration string \"%s\"", text));
	}
	public static long parseLongDuration(String text) {
		Matcher matcher = Pattern.compile("(\\d+d)?(\\d+h)?(\\d+m)?(\\d+s)?(\\d+ms)?").matcher(text);
		if (matcher.matches()) {
			String group;
			long duration = 0L;
			if (null != (group = matcher.group(1))) {
				duration += 24L * 60L * 60L * 1000L * Long.parseLong(group.substring(0, group.length() - 1));
			}
			if (null != (group = matcher.group(2))) {
				duration += 60L * 60L * 1000L * Long.parseLong(group.substring(0, group.length() - 1));
			}
			if (null != (group = matcher.group(3))) {
				duration += 60L * 1000L * Long.parseLong(group.substring(0, group.length() - 1));
			}
			if (null != (group = matcher.group(4))) {
				duration += 1000L * Long.parseLong(group.substring(0, group.length() - 1));
			}
			if (null != (group = matcher.group(5))) {
				duration += Long.parseLong(group.substring(0, group.length() - 2));
			}
			return duration;
		}
		throw new IllegalArgumentException(String
				.format("not a valid duration string \"%s\"", text));
	}

	public static String formatDuration(long duration) {
		final StringBuilder text = new StringBuilder();
		long unit;
		if ((unit = duration / (24L * 60L * 60L * 1000L)) > 0L) {
			text.append(unit).append('d');
			duration %= 24L * 60L * 60L * 1000L;
		}
		if ((unit = duration / (60L * 60L * 1000L)) > 0L) {
			text.append(unit).append('h');
			duration %= 60L * 60L * 1000L;
		}
		if ((unit = duration / (60L * 1000L)) > 0L) {
			text.append(unit).append('m');
			duration %= 60L * 1000L;
		}
		if ((unit = duration / 1000L) > 0L) {
			text.append(unit).append('s');
			duration %= 1000L;
		}
		if (duration > 0 || 0 == text.length()) {
			text.append(duration).append("ms");
		}
		return text.toString();
	}
	
	public static String printStackTrace(Throwable e) {
		final StringWriter writer = new StringWriter();
		e.printStackTrace(new PrintWriter(writer));
		return writer.toString();
	}
		
//	public static void main(String[] args) {
//		System.out.println("" + Arrays.asList(split("|69817433|250506203|22486300||69817433|22486300|250506203", '|')));
//		System.out.println("" + Arrays.asList(split("|||||||", '|')));
//		System.out.println(lstrip("0000345", '0'));
//		System.out.println(lstrip("34500", '0'));
//	}
	
}
