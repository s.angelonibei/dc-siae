package com.alkemytech.sophia.commons.text;

import java.util.Properties;

import com.google.inject.Provider;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class TextNormalizerProvider implements Provider<TextNormalizer> {
	
	private final Provider<TextNormalizer> provider;
	
	public TextNormalizerProvider(final Properties configuration, final String propertyPrefix) {
		super();
		final String className = configuration
				.getProperty(propertyPrefix + ".class_name");
		if (RegExTextNormalizer.class.getName().equals(className)) {
			provider = new Provider<TextNormalizer>() {
				@Override
				public TextNormalizer get() {
					return new RegExTextNormalizer(configuration, propertyPrefix);
				}
			}; 
		} else {
			throw new IllegalArgumentException(String
					.format("unknown class name \"%s\"", className));
		}
	}
	
	@Override
	public TextNormalizer get() {
		return provider.get();
	}
	
}
