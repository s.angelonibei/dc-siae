package com.alkemytech.sophia.commons.nosql;

import java.io.File;

import com.alkemytech.sophia.commons.mmap.MemoryMap;
import com.alkemytech.sophia.commons.mmap.ObjectCodec;
import com.alkemytech.sophia.commons.trie.ConcurrentTrie;
import com.alkemytech.sophia.commons.trie.ConcurrentTrieEx;
import com.alkemytech.sophia.commons.trie.ConcurrentTrieHashMap;
import com.alkemytech.sophia.commons.trie.LeafBinding;
import com.alkemytech.sophia.commons.trie.Trie;
import com.alkemytech.sophia.commons.trie.TrieEx;
import com.alkemytech.sophia.commons.trie.TrieHashMap;
import com.alkemytech.sophia.commons.trie.ExactTrie;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class NoSqlConfig<T extends NoSqlEntity> {
	
	protected File homeFolder;
	protected boolean readOnly;
	protected int pageSize;
	protected int cacheSize;
	protected ThreadLocal<ObjectCodec<T>> codec;
	protected boolean secondaryIndex;
	private String trieClassName;
		
	public NoSqlConfig() {
		super();
		readOnly = true;
		pageSize = -1;
		cacheSize = 0;
		secondaryIndex = false;
		trieClassName = ConcurrentTrie.class.getName();
	}

	public NoSqlConfig<T> setHomeFolder(File homeFolder) {
		this.homeFolder = homeFolder;
		return this;
	}
	
	public NoSqlConfig<T> setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
		return this;
	}

	public NoSqlConfig<T> setPageSize(int pageSize) {
		this.pageSize = pageSize;
		return this;
	}

	public NoSqlConfig<T> setCacheSize(int cacheSize) {
		this.cacheSize = cacheSize;
		return this;
	}
	
	public NoSqlConfig<T> setCodec(ThreadLocal<ObjectCodec<T>> codec) {
		this.codec = codec;
		return this;
	}		
	
	public NoSqlConfig<T> setSecondaryIndex(boolean secondaryIndex) {
		this.secondaryIndex = secondaryIndex;
		return this;
	}
	
	public NoSqlConfig<T> setTrieClassName(String trieClassName) {
		if (!Trie.class.getName().equals(trieClassName) &&
				!ConcurrentTrie.class.getName().equals(trieClassName) &&
				TrieEx.class.getName().equals(trieClassName) &&
				!ConcurrentTrieEx.class.getName().equals(trieClassName) &&
				TrieHashMap.class.getName().equals(trieClassName) &&
				!ConcurrentTrieHashMap.class.getName().equals(trieClassName)) {
			throw new IllegalArgumentException("invalid trie class: " + trieClassName);
		}
		this.trieClassName = trieClassName;
		return this;
	}
	
	protected ExactTrie<Long> newTrieInstance(MemoryMap memoryMap, LeafBinding<Long> leafBinding) {
		if (Trie.class.getName().equals(trieClassName)) {
			return Trie.wrap(memoryMap, leafBinding);
		}
		if (ConcurrentTrie.class.getName().equals(trieClassName)) {
			return ConcurrentTrie.wrap(memoryMap, leafBinding);
		}
		if (TrieEx.class.getName().equals(trieClassName)) {
			return TrieEx.wrap(memoryMap, leafBinding);
		}
		if (ConcurrentTrieEx.class.getName().equals(trieClassName)) {
			return ConcurrentTrieEx.wrap(memoryMap, leafBinding);
		}
		if (TrieHashMap.class.getName().equals(trieClassName)) {
			return TrieHashMap.wrap(memoryMap, leafBinding);
		}
		if (ConcurrentTrieHashMap.class.getName().equals(trieClassName)) {
			return ConcurrentTrieHashMap.wrap(memoryMap, leafBinding);
		}
		throw new IllegalStateException("invalid trie class: " + trieClassName);
	}

}
