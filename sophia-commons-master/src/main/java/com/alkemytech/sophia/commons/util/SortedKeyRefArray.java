package com.alkemytech.sophia.commons.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public final class SortedKeyRefArray {

	public static boolean isNullOrEmpty(SortedKeyRefArray array) {
		return null == array || array.isEmpty();
	}

	private long[] keys;
	private long[] refs;
	private int size;

	public SortedKeyRefArray() {
		super();
		this.keys = new long[1];
		this.refs = new long[1];
		this.size = 0;
	}

	public SortedKeyRefArray(int capacity) {
		super();
		this.keys = new long[capacity];
		this.refs = new long[capacity];
		this.size = 0;
	}

	public SortedKeyRefArray(SortedKeyRefArray that) {
		super();
		this.keys = new long[that.size];
		this.refs = new long[that.size];
		this.size = that.size;
		System.arraycopy(that.keys, 0, this.keys, 0, that.size);
		System.arraycopy(that.refs, 0, this.refs, 0, that.size);
	}

	public final int size() {
		return size;
	}

	public final void clear() {
		size = 0;
	}
	
	public final boolean isEmpty() {
		return size < 1;
	}

	public final boolean add(long key, long ref) {
		return add(key, ref, false);
	}

	public final boolean add(long key, long ref, boolean overwrite) {
		int index = Arrays.binarySearch(keys, key);
		if (index >= 0) {
			if (overwrite)
				refs[index] = ref;
			return false;
		}
		index = (-index) - 1;
		final long[] keysEx = new long[1 + size];
		final long[] refsEx = new long[1 + size];
		if (index > 0) {
			System.arraycopy(keys, 0, keysEx, 0, index);
			System.arraycopy(refs, 0, refsEx, 0, index);
		}
		keysEx[index] = key;
		refsEx[index] = ref;
		if (index < size) {
			System.arraycopy(keys, index, keysEx, index + 1, size - index);
			System.arraycopy(refs, index, refsEx, index + 1, size - index);
		}
		keys = keysEx;
		refs = refsEx;
		size ++;
		return true;
	}	
	
	public void save(File file) throws IOException {
		try (FileOutputStream fout = new FileOutputStream(file);
				BufferedOutputStream out = new BufferedOutputStream(fout) ) {
			final byte[] arr = new byte[16];
			final ByteBuffer buf = ByteBuffer.wrap(arr);
			buf.putInt(size);
			out.write(arr, 0, 4);
			for (int i = 0; i < size; i ++) {
				buf.clear();
				buf.putLong(keys[i]);
				buf.putLong(refs[i]);
				out.write(arr, 0, 16);
			}
		}
	}
	
	public static SortedKeyRefArray load(File file) throws IOException {
		try (FileInputStream fin = new FileInputStream(file);
				BufferedInputStream in = new BufferedInputStream(fin)) {
			final byte[] arr = new byte[16];
			final ByteBuffer buf = ByteBuffer.wrap(arr);
			int count = in.read(arr, 0, 4);
			if (count < 4)
				throw new IOException("unespected end-of-file");
			final SortedKeyRefArray array = new SortedKeyRefArray(buf.getInt());
			for (int i = 0; i < array.size; i ++) {
				count = in.read(arr, 0, 16);
				if (count < 16)
					throw new IOException("unespected end-of-file");
				buf.clear();
				array.keys[i] = buf.getLong();
				array.refs[i] = buf.getLong();
			}
			return array;
		}
	}
	
	@Override
	public final String toString() {
		final StringBuilder builder = new StringBuilder()
				.append('[');
		if (size > 0) {
			builder.append(keys[0])
				.append(':')
				.append(refs[0]);
			for (int i = 1; i < size; i ++) {
				builder.append(',')
					.append(keys[i])
					.append(':')
					.append(refs[i]);
			}
		}
		return builder.append(']')
				.toString();
	}
	
}

