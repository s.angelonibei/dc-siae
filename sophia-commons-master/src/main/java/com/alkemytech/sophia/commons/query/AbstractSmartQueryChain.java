package com.alkemytech.sophia.commons.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.alkemytech.sophia.commons.index.LevenshteinIndex;
import com.alkemytech.sophia.commons.util.ListUtils;
import com.alkemytech.sophia.commons.util.LongArray;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class AbstractSmartQueryChain extends AbstractQueryChain {

	protected final int maxTerms;
	protected final int minTerms;

	protected AbstractSmartQueryChain(int maxTerms, int minTerms) {
		super();
		this.maxTerms = maxTerms;
		this.minTerms = minTerms;
	}

	@Override
	public LongArray search(LongArray postingsChain, LevenshteinIndex index, Collection<String> terms) {
		
		// nothing to search
		if (null == terms || terms.isEmpty()) {
			return null;
		}
		
		// sort terms list (descending length)
		List<String> sortedTerms = new ArrayList<>(terms);
		Collections.sort(sortedTerms, new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				return Integer.compare(o2.length(), o1.length());
			}

		});
		
		// keep longest terms only
		if (sortedTerms.size() > maxTerms) {
			sortedTerms = sortedTerms.subList(0, maxTerms);
		}

		// compute term(s) piecewise intersection
		LongArray resultPostings = null;
		if (sortedTerms.size() > 1) { // multiple term(s) query
			if (sortedTerms.size() > minTerms) { // possible union of sublist(s) postings intersection
				// perform all search(es) in advance
				final List<LongArray> termsPostings = new ArrayList<>(sortedTerms.size());
				for (String term : sortedTerms) {
					final LongArray termPostings = searchTerm(index, term);
					if (!LongArray.isNullOrEmpty(termPostings)) { // drop not found term(s)
						termsPostings.add(termPostings);
					}
				}
				if (termsPostings.isEmpty()) {
					return null;
				}
				if (termsPostings.size() > minTerms) { // union of sorted sublist(s) postings intersection
					// sort postings list (ascending length) to speed up intersections
					Collections.sort(termsPostings, new Comparator<LongArray>() {

						@Override
						public int compare(LongArray o1, LongArray o2) {
							return Integer.compare(o1.size(), o2.size());
						}
						
					});
					// build sorted postings sublist(s)
					final List<List<LongArray>> postingsSublists = ListUtils
							.getSortedSublists(termsPostings, minTerms);
					for (List<LongArray> postingsSublist : postingsSublists) {
						// sublist intersection
						LongArray sublistIntersection = postingsSublist.get(0);
						if (null != postingsChain) {
							sublistIntersection = LongArray
									.intersection(sublistIntersection, postingsChain);
						}
						for (int i = 1; i < postingsSublist.size(); i ++) {
							if (LongArray.isNullOrEmpty(sublistIntersection)) {
								break;
							}
							sublistIntersection = LongArray
									.intersection(sublistIntersection, postingsSublist.get(i));
						}
						// union of sublist intersection
						resultPostings = null == resultPostings ? sublistIntersection :
								LongArray.union(resultPostings, sublistIntersection);
					}
				} else { // intersection of sorted postings
					resultPostings = termsPostings.get(0);
					if (null != postingsChain) {
						resultPostings = LongArray.intersection(resultPostings, postingsChain);
					}
					for (int i = 1; i < termsPostings.size(); i ++) {
						if (LongArray.isNullOrEmpty(resultPostings)) {
							return null;
						}
						resultPostings = LongArray
								.intersection(resultPostings, termsPostings.get(i));
					}					
				}
			} else { // intersection of postings
				resultPostings = searchTerm(index, sortedTerms.get(0));
				if (LongArray.isNullOrEmpty(resultPostings)) {
					return null;
				}
				if (null != postingsChain) {
					resultPostings = LongArray.intersection(resultPostings, postingsChain);
				}
				for (int i = 1; i < sortedTerms.size(); i ++) {
					if (LongArray.isNullOrEmpty(resultPostings)) {
						return null;
					}
					final LongArray termPostings = searchTerm(index, sortedTerms.get(i));
					if (LongArray.isNullOrEmpty(termPostings)) {
						return null;
					}
					resultPostings = LongArray.intersection(resultPostings, termPostings);
				}
			}
		} else if (sortedTerms.size() > 0) { // single term query
			resultPostings = searchTerm(index, sortedTerms.get(0));
			if (LongArray.isNullOrEmpty(resultPostings)) {
				return null;
			}
			if (null != postingsChain) {
				resultPostings = LongArray.intersection(resultPostings, postingsChain);
			}
		}
		
		// return result(s)
		return LongArray.isNullOrEmpty(resultPostings) ? null : resultPostings;
	}
	
}
