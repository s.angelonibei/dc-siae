package com.alkemytech.sophia.commons.trie;

import java.util.UUID;

import com.alkemytech.sophia.commons.mmap.MemoryMap;
import com.alkemytech.sophia.commons.mmap.MemoryMapException;
import com.alkemytech.sophia.commons.util.SizeInBytes;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class UUIDBinding implements LeafBinding<UUID> {
	
	public UUIDBinding() {
		super();
	}
	
	@Override
	public int sizeOfLeaves() {
		return SizeInBytes.LONG + SizeInBytes.LONG;
	}

	@Override
	public UUID read(MemoryMap buffer) throws MemoryMapException {
		final long msb = buffer.getLong();
		final long lsb = buffer.getLong();
		return 0L == msb && 0L == lsb ? null : new UUID(msb, lsb);
	}

	@Override
	public void write(MemoryMap buffer, UUID leaf) throws MemoryMapException {
		if (null == leaf) {
			buffer.putLong(0L);
			buffer.putLong(0L);
		} else {
			buffer.putLong(leaf.getMostSignificantBits());
			buffer.putLong(leaf.getLeastSignificantBits());
		}
	}

}
