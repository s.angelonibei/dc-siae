package com.alkemytech.sophia.commons.jdbc;

import java.util.Properties;

import javax.sql.DataSource;

import com.google.inject.Provider;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class DBCP2DataSourceProvider implements Provider<DataSource> {

	private final Provider<DBCP2DataSource> provider;
	
	public DBCP2DataSourceProvider(final Properties configuration, final String propertyPrefix) {
		super();
		final String className = configuration.getProperty(propertyPrefix + ".class_name");
		if (DBCP2DataSource.class.getName().equals(className)) {
			provider = new Provider<DBCP2DataSource>() {
				@Override
				public DBCP2DataSource get() {
					return new DBCP2DataSource(configuration, propertyPrefix);
				}
			}; 
		} else {
			throw new IllegalArgumentException(String
					.format("unknown class name \"%s\"", className));
		}
	}

	@Override
	public DataSource get() {
		return provider.get();
	}

}
