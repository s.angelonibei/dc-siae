package com.alkemytech.sophia.commons.util;

import java.util.Arrays;
import java.util.Iterator;

/**
 * Memory efficient string manipulation.
 * 
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public final class CharArray implements CharSequence, Comparable<CharArray> {
	
	public final class Tokenizer implements Iterator<CharArray>, Iterable<CharArray> {
		
		private char delimiter;
		private boolean pack;
		private int position;
		private CharArray next;

		/**
		 *  Reset tokenizer to initial token. 
		 */
		private Tokenizer rewind(char delimiter, boolean pack) {
			this.delimiter = delimiter;
			this.pack = pack;
			for (position = offset; pack && position < limit &&
					delimiter == array[position]; position ++);
			return this;
		}
		
		/*
		 * (non-Javadoc)
		 * @see java.util.Iterator#hasNext()
		 */
		@Override
		public boolean hasNext() {
			return position < limit;
		}

		/*
		 * (non-Javadoc)
		 * @see java.util.Iterator#next()
		 */
		@Override
		public CharArray next() {
			int index = position;
			for (; index < limit &&
					delimiter != array[index]; index ++);
			if (null == next) {
				next = new CharArray(array, position, index);
			} else {
				next.offset = position;
				next.limit = index;
			}
			for (position = index + 1; pack && position < limit &&
				delimiter == array[position]; position ++);
			return next;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Iterable#iterator()
		 */
		@Override
		public Iterator<CharArray> iterator() {
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see java.util.Iterator#remove()
		 */
		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
		
	}
	
	private final char[] array;
	private int offset;
	private int limit;
	private Tokenizer tokenizer;
	
	/**
	 * Create a CharArray instance backed by given char array,
	 */
	public CharArray(char[] array) {
		this.array = array;
		offset = 0;
		limit = array.length;
	}
	
	/**
	 * Create a CharArray instance backed by given char array,
	 */
	public CharArray(char[] array, int from, int to) {
		this.array = array;
		offset = from;
		limit = to;
	}

	/**
	 * Create a CharArray instance backed by a new char array,
	 * initialized with characters from CharSequence object.
	 */
	public CharArray(CharSequence other) {
		this.array = other
				.toString().toCharArray();
		offset = 0;
		limit = array.length;
	}
	
	/**
	 * Create a copy of CharArray instance backed by a different char array.
	 */
	public CharArray copy() {
		return new CharArray(Arrays.copyOfRange(array, offset, limit));
	}

	/**
	 * Create a duplicate CharArray instance backed by the same char array.
	 */
	public CharArray duplicate() {
		return new CharArray(array, offset, limit);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.CharSequence#charAt(int)
	 */
	@Override
	public char charAt(int index) {
		return array[offset + index];
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.CharSequence#length()
	 */
	@Override
	public int length() {
		return limit - offset;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.CharSequence#subSequence(int, int)
	 */
	@Override
	public CharArray subSequence(int start, int end) {
		return new CharArray(Arrays.copyOfRange(array, offset + start, offset + end));
	}
	
	/**
	 * Count the number of token(s) separated by given delimiter char.
	 * 
	 * @param delimiter token(s) delimiter char
	 * @param pack skip empty tokens flag
	 * @return the number of token(s)
	 */
	public int tokens(char delimiter, boolean pack) {
		int count = 0;
		for (int index = offset; index < limit; ) {
			for (; index < limit &&
					delimiter != array[index]; index ++);
			count ++;
			for (index ++; pack && index < limit &&
					delimiter == array[index]; index ++);
		}
		return count;
	}
	
	/**
	 * Iterate over tokens separated by given delimiter char.
	 * 
	 * @param delimiter token(s) delimiter char
	 * @param pack skip empty tokens flag
	 * @return an iterator over token(s)
	 */
	public Tokenizer split(char delimiter, boolean pack) {
		if (null == tokenizer)
			tokenizer = new Tokenizer();
		return tokenizer.rewind(delimiter, pack);
	}

	/**
	 * Sort token(s) separated by given delimiter char.
	 * 
	 * @param delimiter token(s) delimiter char
	 * @param pack skip empty tokens flag
	 * @return a copy of this object with lexicographically sorted token(s)
	 */
	public CharArray sort(char delimiter, boolean pack) {
		int count = tokens(delimiter, pack);
		if (count > 1) {
			final CharArray[] tokens = new CharArray[count];
			count = 0;
			for (CharArray token : split(delimiter, pack))
				tokens[count ++] = token.duplicate();
			Arrays.sort(tokens);
			final char[] sorted = new char[limit - offset];
			CharArray token = tokens[0];
			int position = count = token.limit - token.offset;
			if (count > 0)
				System.arraycopy(array, token.offset, sorted, 0, count);
			for (int index = 1; index < tokens.length; index ++) {
				token = tokens[index];
				sorted[position ++] = delimiter;
				count = token.limit - token.offset;
				if (count > 0) {
					System.arraycopy(array, token.offset, sorted, position, count);
					position += count;
				}
			}
			System.arraycopy(sorted, 0, array, offset, sorted.length);
		}
		return this;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(CharArray other) {
		final int length = limit - offset;
        final int otherLength = other.limit - other.offset;
        int n = Math.min(length, otherLength);
        int i = offset;
        int j = other.offset;
        if (i == j) {
        	for (int k = i, lim = n + i; k < lim; k++) {
                char ch = array[k];
                char otherCh = other.array[k];
                if (ch != otherCh)
                    return ch - otherCh;
            }
        } else {
            for (; 0 != n; n--) {
                char ch = array[i++];
                char otherCh = other.array[j++];
                if (ch != otherCh)
                    return ch - otherCh;
            }
        }
        return length - otherLength;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int result = 1;
		for (int index = offset; index < limit; index ++)
			result = 31 * result + array[index];
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object object) {
		if (this == object)
			return true;
		if (null == object)
			return false;
		if (getClass() != object.getClass())
			return false;
		final CharArray other = (CharArray) object;
		if (limit - offset != other.limit - other.offset)
			return false;
		for (int index = offset, otherIndex = other.offset;
				index < limit; index ++, otherIndex ++) {
			if (array[index] != other.array[otherIndex])
				return false;
		}
		return true;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return new String(array, offset, limit - offset);
	}
	
}
