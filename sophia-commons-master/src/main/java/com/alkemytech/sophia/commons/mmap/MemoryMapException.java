package com.alkemytech.sophia.commons.mmap;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@SuppressWarnings("serial")
public class MemoryMapException extends RuntimeException {

	public MemoryMapException(String message, Throwable cause) {
		super(message, cause);
	}

	public MemoryMapException(String message) {
		super(message);
	}

	public MemoryMapException(Throwable cause) {
		super(cause);
	}

}
