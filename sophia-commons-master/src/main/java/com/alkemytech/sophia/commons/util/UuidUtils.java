package com.alkemytech.sophia.commons.util;

import org.apache.commons.codec.binary.Base64;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class UuidUtils {

	private static int charToNibble(char ch) {
		switch (ch) {
		case '0': return 0x0;
		case '1': return 0x1;
		case '2': return 0x2;
		case '3': return 0x3;
		case '4': return 0x4;
		case '5': return 0x5;
		case '6': return 0x6;
		case '7': return 0x7;
		case '8': return 0x8;
		case '9': return 0x9;
		case 'a': return 0xa;
		case 'b': return 0xb;
		case 'c': return 0xc;
		case 'd': return 0xd;
		case 'e': return 0xe;
		case 'f': return 0xf;
		}
		throw new IllegalArgumentException("invalid char '" + ch + "'");
	}
	
	private static char nibbleToChar(int nibble) {
		switch (nibble) {
		case 0x0: return '0';
		case 0x1: return '1';
		case 0x2: return '2';
		case 0x3: return '3';
		case 0x4: return '4';
		case 0x5: return '5';
		case 0x6: return '6';
		case 0x7: return '7';
		case 0x8: return '8';
		case 0x9: return '9';
		case 0xa: return 'a';
		case 0xb: return 'b';
		case 0xc: return 'c';
		case 0xd: return 'd';
		case 0xe: return 'e';
		case 0xf: return 'f';
		}
		throw new IllegalArgumentException("invalid nibble " + nibble);
	}
	
	public static byte[] uuidToBytes(String uuid) {
		final char[] array = uuid.toCharArray();
		final byte[] bytes = new byte[16];
		bytes[0x0] = (byte) ((charToNibble(array[ 0]) << 4) | charToNibble(array[ 1]));
		bytes[0x1] = (byte) ((charToNibble(array[ 2]) << 4) | charToNibble(array[ 3]));
		bytes[0x2] = (byte) ((charToNibble(array[ 4]) << 4) | charToNibble(array[ 5]));
		bytes[0x3] = (byte) ((charToNibble(array[ 6]) << 4) | charToNibble(array[ 7]));
		bytes[0x4] = (byte) ((charToNibble(array[ 9]) << 4) | charToNibble(array[10]));
		bytes[0x5] = (byte) ((charToNibble(array[11]) << 4) | charToNibble(array[12]));
		bytes[0x6] = (byte) ((charToNibble(array[14]) << 4) | charToNibble(array[15]));
		bytes[0x7] = (byte) ((charToNibble(array[16]) << 4) | charToNibble(array[17]));
		bytes[0x8] = (byte) ((charToNibble(array[19]) << 4) | charToNibble(array[20]));
		bytes[0x9] = (byte) ((charToNibble(array[21]) << 4) | charToNibble(array[22]));
		bytes[0xa] = (byte) ((charToNibble(array[24]) << 4) | charToNibble(array[25]));
		bytes[0xb] = (byte) ((charToNibble(array[26]) << 4) | charToNibble(array[27]));
		bytes[0xc] = (byte) ((charToNibble(array[28]) << 4) | charToNibble(array[29]));
		bytes[0xd] = (byte) ((charToNibble(array[30]) << 4) | charToNibble(array[31]));
		bytes[0xe] = (byte) ((charToNibble(array[32]) << 4) | charToNibble(array[33]));
		bytes[0xf] = (byte) ((charToNibble(array[34]) << 4) | charToNibble(array[35]));
		return bytes;
	}
	
	public static String bytesToUuid(byte[] bytes) {
		final char[] array = new char[36];
		array[ 0] = nibbleToChar((bytes[ 0] & 0xf0) >> 4);
		array[ 1] = nibbleToChar(bytes[ 0] & 0x0f);
		array[ 2] = nibbleToChar((bytes[ 1] & 0xf0) >> 4);
		array[ 3] = nibbleToChar(bytes[ 1] & 0x0f);
		array[ 4] = nibbleToChar((bytes[ 2] & 0xf0) >> 4);
		array[ 5] = nibbleToChar(bytes[ 2] & 0x0f);
		array[ 6] = nibbleToChar((bytes[ 3] & 0xf0) >> 4);
		array[ 7] = nibbleToChar(bytes[ 3] & 0x0f);
		array[ 8] = '-';
		array[ 9] = nibbleToChar((bytes[ 4] & 0xf0) >> 4);
		array[10] = nibbleToChar(bytes[ 4] & 0x0f);
		array[11] = nibbleToChar((bytes[ 5] & 0xf0) >> 4);
		array[12] = nibbleToChar(bytes[ 5] & 0x0f);
		array[13] = '-';
		array[14] = nibbleToChar((bytes[ 6] & 0xf0) >> 4);
		array[15] = nibbleToChar(bytes[ 6] & 0x0f);
		array[16] = nibbleToChar((bytes[ 7] & 0xf0) >> 4);
		array[17] = nibbleToChar(bytes[ 7] & 0x0f);
		array[18] = '-';
		array[19] = nibbleToChar((bytes[ 8] & 0xf0) >> 4);
		array[20] = nibbleToChar(bytes[ 8] & 0x0f);
		array[21] = nibbleToChar((bytes[ 9] & 0xf0) >> 4);
		array[22] = nibbleToChar(bytes[ 9] & 0x0f);
		array[23] = '-';
		array[24] = nibbleToChar((bytes[10] & 0xf0) >> 4);
		array[25] = nibbleToChar(bytes[10] & 0x0f);
		array[26] = nibbleToChar((bytes[11] & 0xf0) >> 4);
		array[27] = nibbleToChar(bytes[11] & 0x0f);
		array[28] = nibbleToChar((bytes[12] & 0xf0) >> 4);
		array[29] = nibbleToChar(bytes[12] & 0x0f);
		array[30] = nibbleToChar((bytes[13] & 0xf0) >> 4);
		array[31] = nibbleToChar(bytes[13] & 0x0f);
		array[32] = nibbleToChar((bytes[14] & 0xf0) >> 4);
		array[33] = nibbleToChar(bytes[14] & 0x0f);
		array[34] = nibbleToChar((bytes[15] & 0xf0) >> 4);
		array[35] = nibbleToChar(bytes[15] & 0x0f);
		return new String(array);
	}
	
	public static String uuidToBase64(String uuid) {
		return Base64.encodeBase64URLSafeString(uuidToBytes(uuid));
	}

	public static String base64ToUuid(String base64) {
		return bytesToUuid(Base64.decodeBase64(base64));
	}
	
}
