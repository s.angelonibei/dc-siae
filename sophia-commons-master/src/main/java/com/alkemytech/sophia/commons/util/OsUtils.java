package com.alkemytech.sophia.commons.util;

import java.io.File;
import java.lang.management.ManagementFactory;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class OsUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(OsUtils.class);

	private static String hostname = "localhost";
	private static String ipAddress = "127.0.0.1";
	private static int processId = -1;
	
	public static String getHostname() {
		return hostname;
	}
	
	public static String getIpAddress() {
		return ipAddress;
	}
	
	public static int getProcessId() {
		return processId;
	}
	
	@Inject
	protected OsUtils(@Named("configuration") Properties configuration) {
		super();
		// localhost
		InetAddress localhost = null;
		try {
			localhost = InetAddress.getLocalHost();
		} catch (Exception e) {
			logger.error("getting local host", e);
		}
		// hostname
		hostname = configuration
				.getProperty("default.hostname");
		if (Strings.isNullOrEmpty(hostname) || "localhost".equals(hostname)) {
			hostname = null == localhost ? "localhost" : localhost.getHostName();
		}
		logger.info("hostname {}", hostname);
		// ip address
		ipAddress = configuration
				.getProperty("default.ip_address");
		if (Strings.isNullOrEmpty(ipAddress) || "127.0.0.1".equals(ipAddress) || "0.0.0.0".equals(ipAddress)) {
			try (final DatagramSocket socket = new DatagramSocket()) {
				socket.connect(InetAddress.getByName("8.8.8.8"), 15572);
				ipAddress = socket.getLocalAddress().getHostAddress();
			} catch (Exception e) {
				try {
					for (Enumeration<NetworkInterface> networks = NetworkInterface.getNetworkInterfaces();
						networks.hasMoreElements(); ) {
						for (Enumeration<InetAddress> addresses = networks.nextElement().getInetAddresses();
							addresses.hasMoreElements(); ) {
							final String address = addresses.nextElement().getHostAddress();
							if (Strings.isNullOrEmpty(ipAddress) || "127.0.0.1".equals(ipAddress) || "0.0.0.0".equals(ipAddress)) {
								ipAddress = address;
							} else if (ipAddress.startsWith("127.")) {
								if (!address.startsWith("127.")) {
									ipAddress = address;
								}
							} else if (ipAddress.startsWith("192.168.")) {
								if (!address.startsWith("127.") && !address.startsWith("192.168.")) {
									ipAddress = address;
								}
							} else if (ipAddress.startsWith("192.254.")) {
								if (!address.startsWith("127.") && !address.startsWith("192.168.") && !address.startsWith("192.254.")) {
									ipAddress = address;
								}
							}							
						}
					}
				} catch (Exception ex) {
					ipAddress = null == localhost ? "127.0.0.1" : localhost.getHostAddress();
				}
			}
		}
		logger.info("ipAddress {}", ipAddress);
		// process id
		try {
			// WARNING: only works on Linux
			final String name = new File("/proc/self")
					.getCanonicalFile().getName();
			if (!Strings.isNullOrEmpty(name) &&
					!"self".equalsIgnoreCase(name)) {
				processId = Integer.parseInt(name);
			}
		} catch (Exception e) {
			logger.error("getting process id", e);
		}
		if (-1 == processId) {
			try {
				final String name = ManagementFactory
						.getRuntimeMXBean().getName();
				if (!Strings.isNullOrEmpty(name)) {
					final int at = name.indexOf('@');
					processId = Integer.parseInt(-1 == at ?
							name : name.substring(0, at));
				}			
			} catch (Exception e) {
				logger.error("getting process id", e);
			}
		}
		logger.info("processId {}", processId);
	}
	
}
