package com.alkemytech.sophia.commons.score;

import com.alkemytech.sophia.commons.util.TokenizableText;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface TextScore {

	public double getScore(TokenizableText pattern, TokenizableText target);

}
