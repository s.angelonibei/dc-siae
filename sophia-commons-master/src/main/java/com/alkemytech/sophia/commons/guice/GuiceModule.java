package com.alkemytech.sophia.commons.guice;

import java.nio.charset.Charset;
import java.util.Properties;
import java.util.TimeZone;

import com.alkemytech.sophia.commons.util.BigDecimals;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.OsUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class GuiceModule extends AbstractModule {

	private final String[] args;
	private final String resourceName;
	
	protected Properties configuration;
	
	protected GuiceModule(String[] args) {
		super();
		this.args = args;
		this.resourceName = "/configuration.properties";
	}

	protected GuiceModule(String[] args, String resourceName) {
		super();
		this.args = args;
		this.resourceName = resourceName;
	}

	@Override
	protected void configure() {
		
		// default properties
		final Properties defaults = new ConfigurationLoader()
				.withResourceName("/defaults.properties").load();
		// configuration properties
		configuration = null != args && args.length > 0 ?
				new ConfigurationLoader()
					.withCommandLineArgs(args)
						.load(defaults) :
				new ConfigurationLoader()
					.withResourceName(resourceName)
						.load(defaults);
		ConfigurationLoader.expandVariables(configuration);
		if ("true".equals(configuration.getProperty("default.debug"))) {
			configuration.list(System.out);
		}
		Names.bindProperties(binder(), configuration);
		bind(Properties.class)
			.annotatedWith(Names.named("configuration"))
			.toInstance(configuration);

		// charset(s)
		bind(Charset.class)
			.annotatedWith(Names.named("charset"))
			.toInstance(Charset.forName(configuration
					.getProperty("default.charset", "UTF-8")));
		
		// default time zone
		TimeZone.setDefault(TimeZone.getTimeZone(configuration
				.getProperty("default.timezone", "UTC")));

		// initialize log4j2
		Log4j2.initialize(configuration);
		
		// configurable singleton(s)
		bind(GsonUtils.class)
			.asEagerSingleton();
		bind(BigDecimals.class)
			.asEagerSingleton();
		bind(OsUtils.class)
			.asEagerSingleton();

		// gson
		bind(Gson.class)
			.toInstance(new GsonBuilder()
					.disableHtmlEscaping()
//					.setPrettyPrinting()
					.create());		

	}
	
	@SuppressWarnings("unchecked")
	protected <C extends B, B> C classForPrefix(B baseClass, String propertyPrefix) {
		try {
			return (C) Class.forName(configuration
					.getProperty(propertyPrefix + ".class_name"));
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
	
}
