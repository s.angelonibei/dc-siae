package com.alkemytech.sophia.commons.aws;

import com.amazon.sqs.javamessaging.AmazonSQSExtendedClient;
import com.amazon.sqs.javamessaging.ExtendedClientConfiguration;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.auth.PropertiesFileCredentialsProvider;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.name.Named;

import java.util.Properties;

public class SQSExtendedClient extends SQS {

    private final Properties configuration;

    @Inject
    public SQSExtendedClient(@Named("configuration") Properties configuration) {
        super(configuration);
        this.configuration = configuration;
    }

    public synchronized SQSExtendedClient startup() {
        if (null == sqsClient) {
            final String proxyHost = configuration.getProperty("http.proxy.host");
            final String proxyPort = configuration.getProperty("http.proxy.port", "8080");
            final ClientConfiguration clientConfiguration = new ClientConfiguration();
            if (!Strings.isNullOrEmpty(proxyHost)) {
                clientConfiguration.setProxyHost(proxyHost);
                clientConfiguration.setProxyPort(Integer.parseInt(proxyPort));
            }
            final String region = configuration.getProperty("aws.region", "eu-west-1");
            final String endpoint = configuration
                    .getProperty("sqs.endpoint", "https://sqs.eu-west-1.amazonaws.com");
            final String credentialsFilePath = configuration.getProperty("aws.credentials");
            final AWSCredentialsProvider credentials = Strings.isNullOrEmpty(credentialsFilePath) ?
                    new ClasspathPropertiesFileCredentialsProvider() :
                    new PropertiesFileCredentialsProvider(credentialsFilePath);
            final AmazonSQS amazonSQS = AmazonSQSClientBuilder.standard()
                    .withCredentials(credentials)
                    .withClientConfiguration(clientConfiguration)
                    .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endpoint, region))
                    .build();
            AmazonS3 s3 = AmazonS3ClientBuilder.standard()
                    .withCredentials(credentials)
                    .withClientConfiguration(clientConfiguration)
                    .withRegion(region)
                    .build();
            sqsClient = new AmazonSQSExtendedClient(amazonSQS, new ExtendedClientConfiguration()
                    .withLargePayloadSupportEnabled(s3, configuration.getProperty("sqs.s3_bucket", "siae-sophia-sqs-messages")));
        }
        return this;
    }

}
