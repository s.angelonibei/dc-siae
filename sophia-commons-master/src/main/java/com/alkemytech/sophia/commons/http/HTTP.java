package com.alkemytech.sophia.commons.http;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.PrivateKeyDetails;
import org.apache.http.ssl.PrivateKeyStrategy;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.util.TextUtils;
import com.google.common.base.Strings;

public class HTTP {
	
	private static final Logger logger = LoggerFactory.getLogger(HTTP.class);

	public static class Response {
		
		public final int status;
		public final byte[] body;
		
		private Response(int status, byte[] body) {
			super();
			this.status = status;
			this.body = body;
		}
		
		public boolean isOK() {
			return HttpStatus.SC_OK == status;
		}

		public String getBodyAsString(Charset charset) {
			return null == body ? null : new String(body, charset);
		}
		
	}
	
	private final Properties configuration;

	private CloseableHttpClient httpClient;
	
	public HTTP(Properties configuration) {
		super();
		this.configuration = configuration;
	}

	private void initializeRequest(HttpClientBuilder httpClientBuilder) {
		final RequestConfig.Builder requestConfigBuilder = RequestConfig.custom();
		final String connectTimeout = configuration.getProperty("http.connect_timeout");
		if (!Strings.isNullOrEmpty(connectTimeout))
			requestConfigBuilder.setConnectTimeout(TextUtils.parseIntDuration(connectTimeout));
		final String socketTimeout = configuration.getProperty("http.socket_timeout");
		if (!Strings.isNullOrEmpty(socketTimeout))
			requestConfigBuilder.setSocketTimeout(TextUtils.parseIntDuration(socketTimeout));
		final String maxRedirects = configuration.getProperty("http.max_redirects");
		if (!Strings.isNullOrEmpty(maxRedirects))
			requestConfigBuilder.setMaxRedirects(Integer.parseInt(maxRedirects))
				.setRedirectsEnabled(Integer.parseInt(maxRedirects) > 0);
		httpClientBuilder.setDefaultRequestConfig(requestConfigBuilder.build());
	}

	private void initializeProxy(HttpClientBuilder httpClientBuilder) {
		final String proxyHost = configuration.getProperty("http.proxy.host");
		if (Strings.isNullOrEmpty(proxyHost))
			return;
		final String proxyPort = configuration.getProperty("http.proxy.port", "8080");
		final HttpHost httpProxy = new HttpHost(proxyHost, Integer.parseInt(proxyPort));
		httpClientBuilder.setRoutePlanner(new DefaultProxyRoutePlanner(httpProxy));
	}

	private void initializeSSL(HttpClientBuilder httpClientBuilder) {
		final String sslConfigPrefix = configuration.getProperty("http.ssl_config_prefix");
		// ssl context
		final String keyStoreFile = configuration.getProperty(sslConfigPrefix + ".key_store.file");
		if (Strings.isNullOrEmpty(keyStoreFile)) {
			httpClientBuilder.setSSLContext(SSLContexts.createDefault());
		} else {
			final String keyStoreType = configuration.getProperty(sslConfigPrefix + ".key_store.type");
			final String keyStoreAlias = configuration.getProperty(sslConfigPrefix + ".key_store.alias");
			final String keyStorePassword = configuration.getProperty(sslConfigPrefix + ".key_store.password");
			final boolean keyStoreSelfSigned = "true".equalsIgnoreCase(configuration
					.getProperty(sslConfigPrefix + ".key_store.self_signed"));
			final String keyPassword = configuration.getProperty(sslConfigPrefix + ".key.password");
			try {
				final SSLContextBuilder sslContextBuilder = SSLContexts.custom();
				sslContextBuilder.loadKeyMaterial(new File(keyStoreFile), 
							Strings.isNullOrEmpty(keyStorePassword) ? null : keyStorePassword.toCharArray(), 
							Strings.isNullOrEmpty(keyPassword) ? null : keyPassword.toCharArray(),
							Strings.isNullOrEmpty(keyStoreAlias) ? null : new PrivateKeyStrategy() {
	
								@Override
								public String chooseAlias(Map<String, PrivateKeyDetails> aliases, Socket socket) {
									for (String alias : aliases.keySet()) { 
								        if (alias.equalsIgnoreCase(keyStoreAlias))
								        	return alias; 
						            } 
						            return null; 
								}
	
							});
				sslContextBuilder.loadTrustMaterial(new File(keyStoreFile),
							Strings.isNullOrEmpty(keyStorePassword) ? null : keyStorePassword.toCharArray(), 
							keyStoreSelfSigned ? new TrustSelfSignedStrategy() : null);
				if (!Strings.isNullOrEmpty(keyStoreType))
					sslContextBuilder.setKeyStoreType(keyStoreType);
				httpClientBuilder.setSSLContext(sslContextBuilder.build());
			} catch (UnrecoverableKeyException | NoSuchAlgorithmException |
					KeyStoreException | CertificateException | IOException | KeyManagementException e) {
				logger.error("initializeSSL", e);
			}
		}
		// ssl hostname verifier
		final boolean verifyHostnames = "true".equalsIgnoreCase(configuration
				.getProperty(sslConfigPrefix + ".verify_hostnames", "true"));
		if (!verifyHostnames)
			httpClientBuilder.setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE);
	}
	
	public synchronized HTTP startup()  {
		if (null == httpClient) {
			final String userAgent = configuration.getProperty("http.user_agent",
					"Mozilla/4.0 (compatible; Apache HttpComponents Client 4.5.5)");
			final HttpClientBuilder httpClientBuilder = HttpClients.custom()
			        .setUserAgent(userAgent)
					.disableCookieManagement();
			initializeRequest(httpClientBuilder);
			initializeProxy(httpClientBuilder);
			initializeSSL(httpClientBuilder);
			httpClient = httpClientBuilder.build();
		}
		return this;
	}
		
	public synchronized HTTP shutdown() throws IOException {
		if (null != httpClient) {
			httpClient.close();
			httpClient = null;
		}
		return this;
	}
	
	private Response parseResponse(CloseableHttpResponse response) throws IOException {
		final HttpEntity entity = response.getEntity();
		if (null == entity) {
			return new Response(response.getStatusLine()
					.getStatusCode(), null);
		}
		final long length = entity.getContentLength();
		final ByteArrayOutputStream out = length > 0L ?
				new ByteArrayOutputStream((int) length) : new ByteArrayOutputStream();
		entity.writeTo(out);
		return new Response(response.getStatusLine()
				.getStatusCode(), out.toByteArray());
	}
	
	public Response get(String url) throws ClientProtocolException, IOException {
		final HttpGet httpGet = new HttpGet(url);
		try (final CloseableHttpResponse response = httpClient.execute(httpGet)) {
			return parseResponse(response);
		}
	}
	
	public Response post(String url, Map<String, String> params) throws ClientProtocolException, IOException {
		final HttpPost httpPost = new HttpPost(url);
		List<NameValuePair> formParams = new ArrayList<NameValuePair>();
		for (Map.Entry<String, String> entry : params.entrySet()) {
			formParams.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
		}
		httpPost.setEntity(new UrlEncodedFormEntity(formParams, Consts.UTF_8));
		try (final CloseableHttpResponse response = httpClient.execute(httpPost)) {
			return parseResponse(response);
		}
	}
	
	public Response post(String url, String content, String contentType) throws ClientProtocolException, IOException {
		final HttpPost httpPost = new HttpPost(url);
		httpPost.setEntity(new StringEntity(content,
				ContentType.create(contentType, Consts.UTF_8)));
		try (final CloseableHttpResponse response = httpClient.execute(httpPost)) {
			return parseResponse(response);
		}
	}

	public Response post(String url, File file, String contentType) throws ClientProtocolException, IOException {
		final HttpPost httpPost = new HttpPost(url);
		httpPost.setEntity(new FileEntity(file,
				ContentType.create(contentType, Consts.UTF_8)));
		try (final CloseableHttpResponse response = httpClient.execute(httpPost)) {
			return parseResponse(response);
		}
	}

}
