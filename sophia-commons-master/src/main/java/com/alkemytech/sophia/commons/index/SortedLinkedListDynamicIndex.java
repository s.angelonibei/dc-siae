package com.alkemytech.sophia.commons.index;

import java.io.File;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.mmap.ConcurrentMemoryMap;
import com.alkemytech.sophia.commons.mmap.MemoryMap;
import com.alkemytech.sophia.commons.mmap.MemoryMapException;
import com.alkemytech.sophia.commons.trie.ConcurrentTrie;
import com.alkemytech.sophia.commons.trie.LeafBinding;
import com.alkemytech.sophia.commons.trie.ScoredPath;
import com.alkemytech.sophia.commons.trie.Trie;
import com.alkemytech.sophia.commons.util.LongArray;
import com.alkemytech.sophia.commons.util.SizeInBytes;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class SortedLinkedListDynamicIndex implements DynamicIndex {

	private static class Posting {
		
		private static final long BYTES = 16L;
		
		private transient long offset;
		
		private long docId;	// document id		
		private long next;	// offset of next posting (0L end-of-list)
		
	}
	
//	private static class PostingCodec extends ObjectCodec<Posting> {
//
//		@Override
//		public Posting bytesToObject(byte[] bytes) {
//			beginDecoding(bytes);
//			final Posting object = new Posting();
//			object.docId = getPackedLong();
//			object.next = getPackedLong();
//			return object;
//		}
//
//		@Override
//		public byte[] objectToBytes(Posting object) {
//			beginEncoding();
//			putPackedLong(object.docId);
//			putPackedLong(object.next);
//			return commitEncoding();
//		}
//		
//	}

//	private static class ThreadLocalPostingCodec extends ThreadLocal<ObjectCodec<Posting>> {
//
//		@Override
//	    protected synchronized ObjectCodec<Posting> initialValue() {
//	        return new PostingObjectCodec();
//	    }
//	    
//	}

	private static class Node {
		
		private static final int BYTES = 20;
		
		private int length;	// number of posting(s)
		private long first;	// offset of first posting
		private long last;	// offset of last posting
		
		// WARNING: offset of last posting is used to speed up insertion of already sorted docId(s)
		//          useful when inserting all document(s) at once during initial index creation
		
		private Node(int length, long first, long last) {
			super();
			this.length = length;
			this.first = first;
			this.last = last;
		}

	}

	private static class NodeBinding implements LeafBinding<Node> {

		@Override
		public int sizeOfLeaves() {
			return Node.BYTES;
		}

		@Override
		public Node read(MemoryMap buffer) throws MemoryMapException {
			final int length = buffer.getInt();
			if (0 == length) {
				return null;
			}
			final long first = buffer.getLong();
			final long last = buffer.getLong();
			return new Node(length, first, last);
		}

		@Override
		public void write(MemoryMap buffer, Node leaf) throws MemoryMapException {
			if (null == leaf) {
				buffer.putInt(0);
			} else {
				buffer.putInt(leaf.length);
				buffer.putLong(leaf.first);
				buffer.putLong(leaf.last);
			}
		}

	}

	/**
	 * Exact match.
	 */
	public class SingleTermSet implements TermSet {

		private final String term;
		private final Node node;
		private int remaining;
		
		protected SingleTermSet(String term, Node node) {
			super();
			this.term = term;
			this.node = node;
			remaining = 1;
		}

		@Override
		public int size() {
			return 1;			
		}

		@Override
		public boolean next() {
			remaining --;
			return 0 == remaining;
		}

		@Override
		public String getTerm() {
			if (0 == remaining)
				return term;
			throw new IllegalStateException(remaining > 0 ?
					"before first" : "after last");
		}

		@Override
		public LongArray getPostings() {
			if (0 == remaining)
				return getPostingsByNode(node);
			throw new IllegalStateException(remaining > 0 ?
					"before first" : "after last");
		}
		
	}
	
	/**
	 * Fuzzy match.
	 */
	public class MultipleTermSet implements TermSet {
		
		private final Iterator<ScoredPath<Node>> iterator;
		private final int size;
		private ScoredPath<Node> scoredPath;
		
		protected MultipleTermSet(Collection<ScoredPath<Node>> scoredPaths) {
			super();
			size = scoredPaths.size();
			iterator = scoredPaths.iterator();
		}

		@Override
		public final int size() {
			return size;			
		}

		@Override
		public final boolean next() {
			if (iterator.hasNext()) {
				scoredPath = iterator.next();
				return true;
			}
			scoredPath = null;
			return false;
		}

		@Override
		public final String getTerm() {
			if (null != scoredPath)
				return scoredPath.path;
			throw new IllegalStateException(iterator.hasNext() ?
					"before first" : "after last");
		}

		@Override
		public final LongArray getPostings() {
			if (null != scoredPath)
				return getPostingsByNode(scoredPath.leaf);
			throw new IllegalStateException(iterator.hasNext() ?
					"before first" : "after last");
		}
		
	}
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private String name;
	private boolean readOnly;
	private Trie<Node> terms;
	private MemoryMap postings;
	private Cache<Long, LongArray> cache;

	private final Properties configuration;
	private final String propertyPrefix;
	
	public SortedLinkedListDynamicIndex(Properties configuration, String propertyPrefix) {
		super();
		this.configuration = configuration;
		this.propertyPrefix = propertyPrefix;
	}

	private boolean getPosting(long offset, Posting posting) {
		posting.offset = offset;
		if (0L == offset) {
			posting.docId = 0L;
			posting.next = 0L;
			return false;
		}
		postings.position(offset);
		posting.docId = postings.getLong();
		posting.next = postings.getLong();
		return true;
	}

	private void putPosting(long offset, long docId, long next) {
		postings.position(offset);
		postings.putLong(docId);
		postings.putLong(next);
	}
	
	private LongArray getPostingsByNode(Node node) {
		if (null != cache) {
			final LongArray postings = cache.getIfPresent(node.first);
			if (null != postings) {
				return new LongArray(postings);
			}
		}
		final LongArray postings = new LongArray(node.length);
		final Posting posting = new Posting();
		for (long offset = node.first;
				getPosting(offset, posting); 
					offset = posting.next) {
			postings.add(posting.docId);
		}
		if (null != cache) {
			cache.put(node.first, new LongArray(postings));
		}
		return postings;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.commons.index.DynamicIndex#startup()
	 */
	@Override
	public synchronized void startup() {
		if (null == terms) {
			final File homeFolder = new File(configuration
					.getProperty(propertyPrefix + ".home_folder"));
			final int pageSize = TextUtils.parseIntSize(configuration
					.getProperty(propertyPrefix + ".page_size", "-1"));
			final int cacheSize = TextUtils.parseIntSize(configuration
					.getProperty(propertyPrefix + ".cache_size", "0"));
			name = configuration
					.getProperty(propertyPrefix + ".index_name");
			readOnly = "true".equalsIgnoreCase(configuration
					.getProperty(propertyPrefix + ".read_only", "true"));
			terms = new ConcurrentTrie<Node>(ConcurrentMemoryMap
					.open(new File(homeFolder, "terms"),
							pageSize, readOnly), new NodeBinding());
			postings = ConcurrentMemoryMap
					.open(new File(homeFolder, "postings"), pageSize, readOnly);
			logger.debug("name {}", name);
			logger.debug("readOnly {}", readOnly);
			if (!readOnly && 0L == postings.getSizeOnDisk()) {
				postings.position(0L)
					.putLong(SizeInBytes.LONG); // initialize index size
			}
			if (cacheSize < 1) {
				cache = null;
			} else {
				cache = CacheBuilder.newBuilder()
					.maximumSize(cacheSize).build();
			}
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.commons.index.DynamicIndex#shutdown()
	 */
	@Override
	public synchronized void shutdown() {
		if (null != terms) {
			logger.debug("shutdown");
			terms.garbage();
			postings.garbage();
			if (null != cache) {
				cache.invalidateAll();
				cache.cleanUp();
			}
			terms = null;
			postings = null;
			cache = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.commons.index.LevenshteinIndex#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.commons.index.LevenshteinIndex#isReadOnly()
	 */
	@Override
	public boolean isReadOnly() {
		return readOnly;
	}

	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.commons.index.LevenshteinIndex#search(java.lang.String)
	 */
	@Override
	public TermSet search(String text) {
		final Node node = terms.find(text);
		// WARNING: must handle empty node(s)
		return null == node || 0 == node.length ?
				TermSet.EMPTY : new SingleTermSet(text, node);
	}

	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.commons.index.LevenshteinIndex#search(java.lang.String, int)
	 */
	@Override
	public TermSet search(String text, int maxDistance) {
		final List<ScoredPath<Node>> scoredPaths = terms.search(text, maxDistance);
		if (null == scoredPaths || scoredPaths.isEmpty()) {
			return TermSet.EMPTY;
		}
		// WARNING: must handle empty node(s)
		for (Iterator<ScoredPath<Node>> iterator = scoredPaths.iterator();
				iterator.hasNext(); ) {
			final ScoredPath<Node> scoredPath = iterator.next();
			if (0 == scoredPath.leaf.length)
				iterator.remove();
		}
		return new MultipleTermSet(scoredPaths);
	}

	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.commons.index.DynamicIndex#put(java.lang.String, long)
	 */
	@Override
	public synchronized void put(String term, long docId) {
		final Node node = terms.find(term);
		if (null == node || 0 == node.length) {
			// insert new term with docId as the only posting
			final long size = postings.position(0L).getLong();
			putPosting(size, docId, 0L);
			postings.position(0L).putLong(size + Posting.BYTES);
			// insert new trie node
			terms.upsert(term, new Node(1, size, size));
//			logger.debug("new term {} {} ...", docId, term);
			return;
		}
		if (null != cache) {
			final LongArray postings = cache.getIfPresent(node.first);
			if (null != postings && postings.indexOf(docId) >= 0) {
				// existing term already contains docId
				return;
			}
			// we are going to modify term's postings
			cache.invalidate(node.first);
		}
		final Posting curr = new Posting();
		getPosting(node.last, curr);
		if (docId > curr.docId) {
			// add docId to existing term as last posting
			final long size = postings.position(0L).getLong();
			putPosting(size, docId, 0L); // insert new posting
			postings.position(0L).putLong(size + Posting.BYTES);
			putPosting(curr.offset, curr.docId, size); // update existing posting
			// update trie node
			node.length ++;
			node.last = size;
			terms.upsert(term, node);
//			logger.debug("term appended {} {} ...", docId, term);
			return;
		}
//		logger.debug("search insertion point of {} {} ...", docId, term);
		final Posting prev = new Posting();
		for (long offset = node.first;
				getPosting(offset, curr);
					offset = curr.next) {
			if (docId == curr.docId) {
				// term already contains docId
				return;
			} else if (docId < curr.docId) {
				if (0L == prev.offset) {
					// add docId to existing term as first posting
					final long size = postings.position(0L).getLong();
					putPosting(size, docId, node.first);
					postings.position(0L).putLong(size + Posting.BYTES);
					node.length ++;
					node.first = size;
					terms.upsert(term, node); // update trie node
					return;
				}
				// add docId to existing term
				final long size = postings.position(0L).getLong();
				putPosting(size, docId, prev.next);
				postings.position(0L).putLong(size + Posting.BYTES);
				putPosting(prev.offset, prev.docId, size);
				node.length ++;
				terms.upsert(term, node); // update trie node
				return;
			}
			prev.offset = curr.offset;
			prev.docId = curr.docId;
			prev.next = curr.next;	
		}
		throw new IllegalArgumentException(String
				.format("invalid arguments: %s %d", term, docId));
	}

	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.commons.index.DynamicIndex#putAll(java.util.Collection, long)
	 */
	@Override
	public void putAll(Collection<String> terms, long docId) {
		for (String term : terms) {
			put(term, docId);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.commons.index.DynamicIndex#remove(java.lang.String, long)
	 */
	@Override
	public synchronized boolean remove(String term, long docId) throws LevenshteinIndexException {
		final Node node = terms.find(term);
		if (null == node || 0 == node.length)
			return false;
		// we are going to modify term's postings
		if (null != cache) {
			cache.invalidate(node.first);
		}
		boolean removed = false;
		final Posting curr = new Posting();
		final Posting prev = new Posting();
		for (long offset = node.first;
				getPosting(offset, curr);
					offset = curr.next) {
			if (docId == curr.docId) {
				// term's postings contain docId
				if (0L == prev.offset) {
					// removing first docId from this term's postings
					node.first = curr.next;
				} else {
					putPosting(prev.offset, prev.docId, curr.next);
				}
				if (node.last == curr.offset) {
					// removing last docId from this term's postings
					node.last = prev.offset;
				}
				node.length --;
				terms.upsert(term, node); // update trie node
				removed = true;
				break;
			}
			prev.offset = curr.offset;
			prev.docId = curr.docId;
			prev.next = curr.next;	
		}
		return removed;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.commons.index.DynamicIndex#removeAll(java.util.Collection, long)
	 */
	@Override
	public boolean removeAll(Collection<String> terms, long docId) throws LevenshteinIndexException {
		boolean removed = false;
		for (String term : terms) {
			removed = removed || remove(term, docId);
		}
		return removed;
	}

}
