package com.alkemytech.sophia.commons.query;

import java.util.Collection;

import com.alkemytech.sophia.commons.index.LevenshteinIndex;
import com.alkemytech.sophia.commons.module.DynamicModule;
import com.alkemytech.sophia.commons.util.LongArray;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@Deprecated
public interface TitleArtistQuery extends DynamicModule {

	/**
	 * Performs a query based on title and artist terms.
	 * 
	 * @param titleIndex title index
	 * @param artistIndex artist index
	 * @param titleTerms title terms
	 * @param artistTerms artist terms
	 * @return array of matching document id(s)
	 */
	public LongArray search(LevenshteinIndex titleIndex, LevenshteinIndex artistIndex, Collection<String> titleTerms, Collection<String> artistTerms);

}
