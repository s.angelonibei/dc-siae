package com.alkemytech.sophia.commons.nosql;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.mmap.ConcurrentMemoryMap;
import com.alkemytech.sophia.commons.mmap.MemoryMap;
import com.alkemytech.sophia.commons.mmap.MemoryMapException;
import com.alkemytech.sophia.commons.trie.LongBinding;
import com.alkemytech.sophia.commons.trie.ExactTrie;
import com.alkemytech.sophia.commons.util.SizeInBytes;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.google.common.base.Strings;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ConcurrentNoSql<T extends NoSqlEntity> implements NoSql<T> {

	private static final long SIZE_OFFSET = 0L;
	private static final long COUNT_OFFSET = 8L;
	private static final long SEQUENCE_OFFSET = 16L;
		
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	private final NoSqlConfig<T> config;
	private final Object monitor;
	private final MemoryMap catalog;
	private final MemoryMap records;
	private final ExactTrie<Long> primaryIndex;
	private final ExactTrie<Long> secondaryIndex;
	private final Cache<String, T> primaryCache;
	private final Cache<String, T> secondaryCache;
	private final Converter<T, T> identityConverter;
	
	public ConcurrentNoSql(NoSqlConfig<T> config) throws NoSqlException {
		super();
		this.config = config;
		logger.debug("readOnly {}", config.readOnly);
		logger.debug("homeFolder {}", config.homeFolder);
		logger.debug("pageSize {}", config.pageSize);
		logger.debug("cacheSize {}", config.cacheSize);
		logger.debug("secondaryIndex {}", config.secondaryIndex);
		this.monitor = new Object();
		try {
			catalog = ConcurrentMemoryMap
					.open(new File(config.homeFolder, "catalog"), 1024, config.readOnly);
			if (!config.readOnly && 0L == catalog.getSizeOnDisk()) {
				catalog.position(SIZE_OFFSET).putLong(0L);
				catalog.position(COUNT_OFFSET).putLong(0L);
				catalog.position(SEQUENCE_OFFSET).putLong(0L);				
			}
			records = ConcurrentMemoryMap
					.open(new File(config.homeFolder, "records"), config.pageSize, config.readOnly);
			primaryIndex = config.newTrieInstance(ConcurrentMemoryMap.open(new File(config.homeFolder, "primary"),
					config.pageSize, config.readOnly), new LongBinding(-1L));
			if (config.secondaryIndex) {
				secondaryIndex = config.newTrieInstance(ConcurrentMemoryMap.open(new File(config.homeFolder, "secondary"),
						config.pageSize, config.readOnly), new LongBinding(-1L));
			} else {
				secondaryIndex = null;
			}
		} catch (MemoryMapException e) {
			throw new NoSqlException(e);
		}
		if (config.cacheSize > 0) {
			this.primaryCache = CacheBuilder.newBuilder()
					.maximumSize(config.cacheSize).build();
		} else {
			this.primaryCache = null;
		}
		if (config.secondaryIndex && config.cacheSize > 0) {
			this.secondaryCache = CacheBuilder.newBuilder()
				.maximumSize(config.cacheSize).build();
		} else {
			this.secondaryCache = null;
		}
		this.identityConverter = new Converter<T, T>() {
			@Override
			public T convert(T entity) {
				return entity;
			}
		};
	}

	@Override
	public void close() {
		synchronized (monitor) {
			catalog.garbage();
			records.garbage();
			primaryIndex.garbage();
			if (null != secondaryIndex)
				secondaryIndex.garbage();
		}
		if (null != primaryCache) {
			primaryCache.invalidateAll();
			primaryCache.cleanUp();
		}
		if (null != secondaryCache) {
			secondaryCache.invalidateAll();
			secondaryCache.cleanUp();
		}
	}
	
	@Override
	public File getHomeFolder() {
		return config.homeFolder;
	}
	
	@Override
	public boolean isReadOnly() {
		return config.readOnly;
	}
	
	@Override
	public long nextSequenceId() throws NoSqlException {
		try {
			// atomically get and increment sequence value
			synchronized (monitor) {
				final long id = 1L + catalog.position(SEQUENCE_OFFSET).getLong();
				catalog.position(SEQUENCE_OFFSET).putLong(id);
				return id;
			}
		} catch (MemoryMapException e) {
			throw new NoSqlException(e);
		}
	}
	
	@Override
	public long count() throws NoSqlException {
		try {
			synchronized (monitor) {
				return catalog.position(COUNT_OFFSET).getLong();
			}
		} catch (MemoryMapException e) {
			throw new NoSqlException(e);
		}
	}
	
	private T get(String key, Cache<String, T> cache, ExactTrie<Long> index) throws NoSqlException {
		// lookup cache
		if (null != cache) {
			final T value = cache.getIfPresent(key);
			if (null != value)
				return value;
		}
		try {	
			// search record position
			final Long position = index.find(key);
			if (null == position)
				return null;
			// read database record
			final int length = records
					.position(position).getInt();
			if (length <= 0)
				return null;
			final byte[] bytes = new byte[length];
			records.get(bytes, 0, length);
			// decode value
			final T value = config.codec.get()
					.bytesToObject(bytes);
			// cache value
			if (null != cache)
				cache.put(key, value);
			return value;
		} catch (MemoryMapException e) {
			throw new NoSqlException(e);
		}
	}
	
	@Override
	public T getByPrimaryKey(String key) throws NoSqlException {
		return get(key, primaryCache, primaryIndex);
	}
	
	@Override
	public T getBySecondaryKey(String key) throws NoSqlException {
		if (null == secondaryIndex)
			throw new NoSqlException("no secondary index");
		return get(key, secondaryCache, secondaryIndex);
	}

	@Override
	public T upsert(T value) throws NoSqlException {
		// primary key
		final String primaryKey = value.getPrimaryKey();
		if (Strings.isNullOrEmpty(primaryKey))
			throw new NoSqlException("missing primary key");
		if (null != primaryCache && !Strings.isNullOrEmpty(primaryKey))
			primaryCache.invalidate(primaryKey);
		// secondary key
		final String secondaryKey = value.getSecondaryKey();
		if (null != secondaryIndex && Strings.isNullOrEmpty(secondaryKey))
			throw new NoSqlException("missing secondary key");
		if (null != secondaryCache && !Strings.isNullOrEmpty(secondaryKey))
			secondaryCache.invalidate(secondaryKey);
		// encode value
		final byte[] bytes = config.codec.get()
				.objectToBytes(value);
		try {
			// lookup primary key
			Long position = primaryIndex.find(primaryKey);
			byte[] previous = null;
			// primary key found
			if (null != position) {
				final int length = records
						.position(position).getInt();
				if (length > 0) {
					// read previous record
					previous = new byte[length];
					records.get(previous, 0, length);
					// recycle or discard memory slot
					if (bytes.length + SizeInBytes.INT <= length) {
						// write record && mark remaining bytes as free
						records.position(position)
							.putInt(bytes.length)
							.put(bytes, 0, bytes.length)
							.putInt(bytes.length + SizeInBytes.INT - length);
						// decode previous value
						return config.codec.get()
								.bytesToObject(previous);
					}
					// mark entire memory slot as free
					records.position(position)
						.putInt(-length);
				}
			}
			// atomically read and update size in bytes and record(s) count
			synchronized (monitor) {
				position = catalog.position(SIZE_OFFSET).getLong();
				catalog.position(SIZE_OFFSET)
					.putLong(position + bytes.length + SizeInBytes.INT);	
				if (null == previous) {
					final long count = 1L + catalog.position(COUNT_OFFSET).getLong();
					catalog.position(COUNT_OFFSET).putLong(count);
				}				
			}
			// write record
			records.position(position)
				.putInt(bytes.length)
				.put(bytes, 0, bytes.length);
			// update record position inside index(ex)
			primaryIndex.upsert(primaryKey, position);
			if (null != secondaryIndex)
				secondaryIndex.upsert(secondaryKey, position);		
			// decode previous value
			return null == previous ? null : config.codec.get()
					.bytesToObject(previous);
		} catch (MemoryMapException e) {
			throw new NoSqlException(e);
		}
	}

	@Override
	public boolean delete(T value) throws NoSqlException {
		return deleteByPrimaryKey(value.getPrimaryKey());
	}

	@Override
	public boolean deleteByPrimaryKey(String key) throws NoSqlException {
		if (Strings.isNullOrEmpty(key))
			throw new NoSqlException("missing primary key");
		// remove item from primary cache
		if (null != primaryCache && !Strings.isNullOrEmpty(key))
			primaryCache.invalidate(key);
		try {
			// search record position using primary index
			final Long position = primaryIndex.find(key);
			if (null == position)
				return false;
			// read record length
			final int length = records.position(position).getInt();
			if (length <= 0)
				return false;
			// retrieve secondary key
			final String secondaryKey;
			if (null == secondaryIndex) {
				secondaryKey = null;
			} else {
				// read database record
				final byte[] bytes = new byte[length];
				records.get(bytes, 0, length);
				// decode value & get secondary key
				secondaryKey = config.codec.get()
						.bytesToObject(bytes).getSecondaryKey();
			}
			// mark memory slot as free
			records.position(position).putInt(-length);
			// update record position inside index(es)
			primaryIndex.upsert(key, -1L);
			if (null != secondaryIndex)
				secondaryIndex.upsert(secondaryKey, -1L);
			// remove item from secondary cache
			if (null != secondaryCache)
				secondaryCache.invalidate(secondaryKey);
			// atomically read and decrement record(s) count
			synchronized (monitor) {
				final long count = catalog.position(COUNT_OFFSET).getLong() ;
				catalog.position(COUNT_OFFSET).putLong(count - 1L);
			}
			return true;
		} catch (MemoryMapException e) {
			throw new NoSqlException(e);
		}
	}
	
	@Override
	public void select(Selector<T> selector) throws NoSqlException {
		try {
			// read database size in bytes
			final long size;
			synchronized (monitor) {
				size = catalog.position(SIZE_OFFSET).getLong();
			}
			// scan all database record(s)
			byte[] bytes = new byte[1024];
			for (long position = 0L; position < size; ) {
				int length = records.position(position).getInt();
				if (length > 0) {
					// read record
					if (length > bytes.length)
						bytes = new byte[length];
					records.get(bytes, 0, length);
					// decode work
					selector.select(config.codec.get()
							.bytesToObject(bytes));				
					position += length + SizeInBytes.INT;					
				} else {
					position += SizeInBytes.INT - length;
				}
			}
		} catch (MemoryMapException e) {
			throw new NoSqlException(e);
		}
	}
	
	@Override
	public void select(Selector<T> selector, Comparator<T> comparator) throws NoSqlException {
		select(selector, identityConverter, comparator);
	}
	
	@Override
	public <V> void select(Selector<V> selector, Converter<T, V> converter, Comparator<V> comparator) throws NoSqlException {
		try {
			// read database size in bytes and record(s) count		
			final long size;
			final long count;
			synchronized (monitor) {
				size = catalog.position(SIZE_OFFSET).getLong();
				count = catalog.position(COUNT_OFFSET).getLong();
			}
			if (count > Integer.MAX_VALUE)
				throw new NoSqlException("too many items " + count);
			// array of value(s)
			final ArrayList<V> values = new ArrayList<>((int) count);
			// scan all database record(s)
			byte[] bytes = new byte[1024];
			for (long position = 0L; position < size; ) {
				int length = records.position(position).getInt();
				if (length > 0) {
					// read record
					if (length > bytes.length)
						bytes = new byte[length];
					records.get(bytes, 0, length);
					// decode value & add to array
					final V value = converter
							.convert(config.codec.get()
									.bytesToObject(bytes));
					values.add(value);				
					position += length + SizeInBytes.INT;					
				} else {
					position += SizeInBytes.INT - length;
				}
			}
			// sort array of value(s)
			Collections.sort(values, comparator);
			// loop on sorted array
			for (V value : values)
				selector.select(value);
		} catch (MemoryMapException e) {
			throw new NoSqlException(e);
		}
	}
	
	@Override
	public void truncate() throws NoSqlException {
		try {
			catalog.clear();
			catalog.position(SIZE_OFFSET).putLong(0L);
			catalog.position(COUNT_OFFSET).putLong(0L);
			catalog.position(SEQUENCE_OFFSET).putLong(0L);
			records.clear();
			primaryIndex.clear();
			if (null != secondaryIndex)
				secondaryIndex.clear();
			if (null != primaryCache) {
				primaryCache.invalidateAll();
				primaryCache.cleanUp();
			}
			if (null != secondaryCache) {
				secondaryCache.invalidateAll();
				secondaryCache.cleanUp();
			}
		} catch (MemoryMapException e) {
			throw new NoSqlException(e);
		}
	}
	
	@Override
	public void defrag() throws NoSqlException {
		try {
			// read database size in bytes
			final long size;
			final long count;
			synchronized (monitor) {
				size = catalog.position(SIZE_OFFSET).getLong();
				count = catalog.position(COUNT_OFFSET).getLong();
			}
			logger.debug("initial size {}", size);
			logger.debug("records count {}", count);
			// scan all database record(s)
			byte[] bytes = new byte[1024];
			long offset = 0L;
			for (long position = 0L; position < size; ) {
				int length = records.position(position).getInt();
				if (length > 0) {
					if (position > offset) {
						// move non empty record
						if (length > bytes.length)
							bytes = new byte[length];
						records.get(bytes, 0, length);
						records.position(offset)
							.putInt(length)
							.put(bytes, 0, length);
						// decode value
						final T value = config.codec.get()
								.bytesToObject(bytes);
						// update position inside index(es)
						primaryIndex.upsert(value.getPrimaryKey(), offset);
						if (null != secondaryIndex)
							secondaryIndex.upsert(value.getSecondaryKey(), offset);
					}
					offset += length + SizeInBytes.INT;
					position += length + SizeInBytes.INT;
				} else {
					position += SizeInBytes.INT - length;
				}
			}
			// update database size in bytes
			synchronized (monitor) {
				catalog.position(SIZE_OFFSET).putLong(offset);
			}
			logger.debug("defragmented size {}", offset);
			logger.debug("database size reduced by {}",
					TextUtils.formatSize(size - offset));
			// shrink database
			records.shrink(offset);
		} catch (MemoryMapException e) {
			throw new NoSqlException(e);
		}

	}
	
}
