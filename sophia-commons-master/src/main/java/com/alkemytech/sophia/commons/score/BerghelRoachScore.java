package com.alkemytech.sophia.commons.score;

import com.alkemytech.sophia.commons.distance.BerghelRoachEditDistance;
import com.alkemytech.sophia.commons.distance.EditDistance;
import com.alkemytech.sophia.commons.util.TokenizableText;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class BerghelRoachScore implements TextScore {

	private final EditDistance editDistance = new BerghelRoachEditDistance();
	
	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.commons.score.TextScore#getScore(com.alkemytech.sophia.commons.util.TokenizableText, com.alkemytech.sophia.commons.util.TokenizableText)
	 */
	@Override
	public double getScore(TokenizableText pattern, TokenizableText target) {
		if (pattern.length() > target.length()) {
			return (double) (pattern.length() - editDistance
					.getDistance(pattern, target)) / (double) pattern.length();				
		} else {
			return (double) (target.length() - editDistance
					.getDistance(pattern, target)) / (double) target.length();
		}
	}

}
