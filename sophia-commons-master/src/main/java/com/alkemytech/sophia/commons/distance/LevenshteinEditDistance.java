package com.alkemytech.sophia.commons.distance;

/**
 * The Levenshtein distance between two strings is the minimum number of
 * single-character edits (insertions, deletions or substitutions) required to
 * change one string into the other.
 */
public class LevenshteinEditDistance implements EditDistance {

	private final int[] currentArray = new int[MAX_LENGTH + 1];
	private final int[] previousArray = new int[MAX_LENGTH + 1];

	private final int min(int a, int b) {
		return a < b ? a : b;
	}

	private final int min(int a, int b, int c) {
		if (a < b) {
			if (a < c) {
				return a;
			}
			return c;
		} else if (b < c) {
			return b;
		}
		return c;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.commons.distance.EditDistance#getDistance(java.lang.CharSequence, java.lang.CharSequence)
	 */
	@Override
	public int getDistance(CharSequence pattern, CharSequence target) {
		
		int patternLength = min(pattern.length(), MAX_LENGTH);
		int targetLength = min(target.length(), MAX_LENGTH);

		if (patternLength < targetLength) {
			final CharSequence swapSequence = pattern; pattern = target; target = swapSequence;
			final int swapLength = patternLength; patternLength = targetLength; targetLength = swapLength;
		}
		
		int[] curr = currentArray;
		int[] prev = previousArray;
		
		for (int i = 0; i <= targetLength; i++)
			prev[i] = i;

//		for (int i = 1; i <= patternLength; i++) {
//			curr[0] = i;
//			for (int j = 1; j <= targetLength; j++) {
//				if (pattern.charAt(i - 1) != target.charAt(j - 1)) {
//					int k = Math.min(Math.min(curr[j - 1], prev[j - 1]), prev[j]);
//					curr[j] = k + 1;
//				} else {
//					curr[j] = prev[j - 1];
//				}
//			}
//			int[] swap = prev; prev = curr; curr = swap;
//		}

		for (int i = 0; i < patternLength; i++) {
			curr[0] = i + 1;
			for (int j = 0; j < targetLength; j++) {
				if (pattern.charAt(i) == target.charAt(j)) {
					curr[j+1] = prev[j];
				} else {
					curr[j+1] = 1 + min(curr[j], prev[j], prev[j+1]);
				}
			}
			int[] swap = prev; prev = curr; curr = swap;
		}

		return prev[targetLength];
		
	}
	
}
