	package com.alkemytech.sophia.commons.score;

import com.alkemytech.sophia.commons.distance.EditDistance;
import com.alkemytech.sophia.commons.util.TokenizableText;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class WordWiseScore implements TextScore {

	private final EditDistance editDistance;
	
	protected WordWiseScore(EditDistance editDistance) {
		super();
		this.editDistance = editDistance;
	}

	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.commons.score.TextScore#getScore(com.alkemytech.sophia.commons.util.TokenizableText, com.alkemytech.sophia.commons.util.TokenizableText)
	 */
	@Override
	public double getScore(TokenizableText pattern, TokenizableText target) {
		if (pattern.length() > target.length()) {
			final TokenizableText swap = target;
			target = pattern;
			pattern = swap;
		}
		int patternTokens = 0;
		double scoreSum = 0.0;
		for (CharSequence patternToken : pattern) {
			double bestScore = 0.0;
			for (CharSequence targetToken : target) {
				final double similarity;
				final int patternTokenLength = patternToken.length();
				final int targetTokenLength = targetToken.length();
				if (patternTokenLength > targetTokenLength) {
					similarity = (double) (patternTokenLength - editDistance
							.getDistance(patternToken, targetToken)) / (double) patternTokenLength;				
				} else {
					similarity = (double) (targetTokenLength - editDistance
							.getDistance(patternToken, targetToken)) / (double) targetTokenLength;
				}
				if (similarity > bestScore) {
					bestScore = similarity;
				}
			}
			scoreSum += bestScore;
			patternTokens ++;
		}
		return scoreSum / (double) patternTokens;
	}

}
