package com.alkemytech.sophia.commons.index;

import java.util.Properties;

import com.google.inject.Provider;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ReverseIndexProvider implements Provider<ReverseIndex> {

	private final Provider<ReverseIndex> provider;
	
	public ReverseIndexProvider(final Properties configuration, final String propertyPrefix) {
		super();
		final String className = configuration
				.getProperty(propertyPrefix + ".class_name");
		if (ReverseIndexImpl.class.getName().equals(className)) {
			provider = new Provider<ReverseIndex>() {
				@Override
				public ReverseIndex get() {
					return new ReverseIndexImpl(configuration, propertyPrefix);
				}
			};
		} else {
			throw new IllegalArgumentException(String
					.format("unknown class name \"%s\"", className));
		}
	}

	@Override
	public ReverseIndex get() {
		return provider.get();
	}

}
