package com.alkemytech.sophia.commons.text;

import java.util.Properties;

import com.alkemytech.sophia.commons.regex.RegExTransformer;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.google.common.base.Strings;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class RegExTextNormalizer implements TextNormalizer {

	private final LetterCase letterCase;
	private final RegExTransformer transformer;

	public RegExTextNormalizer(Properties configuration, String propertyPrefix) {
		super();
		this.letterCase = LetterCase.parse(configuration
				.getProperty(propertyPrefix + ".letter_case"));
		this.transformer = new RegExTransformer(TextUtils.split(configuration
				.getProperty(propertyPrefix + ".transformer"), ","));
	}
	
	@Override
	public String normalize(String text) {
		if (!Strings.isNullOrEmpty(text)) {
			text = letterCase.enforce(text);
			text = transformer.transform(text);
		}
		return text;
	}

}
