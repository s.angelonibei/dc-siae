package com.alkemytech.sophia.commons.distance;

/**
 * Edit distance is a way of quantifying how dissimilar two strings are to one another
 * by counting the minimum number of operations required to transform one string into
 * the other.
 * 
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface EditDistance {

	/**
	 * Maximum string length for edit distance calculations.
	 */
	public static final int MAX_LENGTH = 256;

	/**
	 * Compute edit distance from {@code pattern} to {@code target}.
	 * 
	 * @param pattern char sequence from which distances are measured
	 * @param target char sequence to which distances are measured
	 * @return the computed edit distance
	 */
	public int getDistance(CharSequence pattern, CharSequence target);
	
}
