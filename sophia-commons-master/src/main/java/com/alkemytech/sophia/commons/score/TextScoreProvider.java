package com.alkemytech.sophia.commons.score;

import java.util.Properties;

import com.google.inject.Provider;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class TextScoreProvider implements Provider<TextScore> {
	
	private final Provider<TextScore> provider;
	
	public TextScoreProvider(final Properties configuration, final String propertyPrefix) {
		super();
		final String className = configuration
				.getProperty(propertyPrefix + ".class_name");
		if (ArtistScore.class.getName().equals(className)) {
			provider = new Provider<TextScore>() {
				@Override
				public TextScore get() {
					return new ArtistScore();
				}
			}; 
		} else if (BerghelRoachScore.class.getName().equals(className)) {
			provider = new Provider<TextScore>() {
				@Override
				public TextScore get() {
					return new BerghelRoachScore();
				}
			}; 
		} else if (LevenshteinScore.class.getName().equals(className)) {
			provider = new Provider<TextScore>() {
				@Override
				public TextScore get() {
					return new LevenshteinScore();
				}
			}; 
		} else if (TitleScore.class.getName().equals(className)) {
			provider = new Provider<TextScore>() {
				@Override
				public TextScore get() {
					return new TitleScore();
				}
			}; 
		} else if (WordWiseBerghelRoachScore.class.getName().equals(className)) {
			provider = new Provider<TextScore>() {
				@Override
				public TextScore get() {
					return new WordWiseBerghelRoachScore();
				}
			}; 
		} else if (WordWiseLevenshteinScore.class.getName().equals(className)) {
			provider = new Provider<TextScore>() {
				@Override
				public TextScore get() {
					return new WordWiseLevenshteinScore();
				}
			}; 
		} else {
			throw new IllegalArgumentException(String
					.format("unknown class name \"%s\"", className));
		}
	}
	
	@Override
	public TextScore get() {
		return provider.get();
	}
	
}
