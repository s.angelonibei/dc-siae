package com.alkemytech.sophia.commons.score;

import com.alkemytech.sophia.commons.distance.EditDistance;
import com.alkemytech.sophia.commons.distance.LevenshteinEditDistance;
import com.alkemytech.sophia.commons.util.TokenizableText;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class LevenshteinScore implements TextScore {

	private final EditDistance editDistance = new LevenshteinEditDistance();
	
	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.commons.score.TextScore#getScore(com.alkemytech.sophia.commons.util.TokenizableText, com.alkemytech.sophia.commons.util.TokenizableText)
	 */
	@Override
	public double getScore(TokenizableText pattern, TokenizableText target) {
		final int patternLength = pattern.length();
		final int targetLength = target.length();
		if (patternLength > targetLength) {
			return (double) (patternLength - editDistance
					.getDistance(pattern, target)) / (double) patternLength;				
		} else {
			return (double) (targetLength - editDistance
					.getDistance(pattern, target)) / (double) targetLength;
		}
	}
	
}
