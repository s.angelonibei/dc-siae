package com.alkemytech.sophia.commons.score;

import com.alkemytech.sophia.commons.distance.BerghelRoachEditDistance;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class WordWiseBerghelRoachScore extends WordWiseScore {

	public WordWiseBerghelRoachScore() {
		super(new BerghelRoachEditDistance());
	}
	
}
