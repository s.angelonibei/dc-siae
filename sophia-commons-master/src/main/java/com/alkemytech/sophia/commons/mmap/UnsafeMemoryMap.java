package com.alkemytech.sophia.commons.mmap;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.nio.charset.Charset;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.util.LongArray;
import com.alkemytech.sophia.commons.util.TextUtils;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public final class UnsafeMemoryMap implements MemoryMap {
	
	public static UnsafeMemoryMap create(File folder, int pageSize) throws MemoryMapException {
		return new UnsafeMemoryMap(folder, pageSize, false).deleteAllFiles(0);
	}
	
	public static UnsafeMemoryMap open(File folder, int pageSize, boolean readOnly) throws MemoryMapException {
		return new UnsafeMemoryMap(folder, pageSize, readOnly).scanFolder();
	}

	private final Logger logger = LoggerFactory.getLogger(UnsafeMemoryMap.class);
	
	private final File folder;
	private final boolean readOnly;
	private final FileChannel.MapMode mapMode;
	private final String fileMode;
	private final byte[] codecBuffer;

	private int pageSize;
	private MappedByteBuffer[] buffers;
	private long position;
		
	private UnsafeMemoryMap(File folder, int pageSize, boolean readOnly) {
		super();
		this.folder = folder; 
		this.pageSize = pageSize;
		this.readOnly = readOnly;
		if (readOnly) {
			mapMode = MapMode.READ_ONLY;
			fileMode = "r";
		} else {
			mapMode = MapMode.READ_WRITE;
			fileMode = "rw";
			folder.mkdirs();
		}
		buffers = new MappedByteBuffer[] { null };
		codecBuffer = new byte[8];
		position = 0;
	}
	
	private UnsafeMemoryMap deleteAllFiles(int index) {
		for (;; index ++) {
			final File file = fileForBuffer(index);
			if (file.isFile() && file.length() > 0L) {
				logger.debug("deleting file {} ({})",
						file.getName(), TextUtils.formatSize(file.length()));
				file.delete();
				continue;
			}
			return this;
		}
	}
	
	private UnsafeMemoryMap scanFolder() throws MemoryMapException {
		File file = fileForBuffer(0);
		if (file.isFile() && file.length() > 0L) {
			if (-1 == pageSize) {
				pageSize = (int) file.length();
			} else if (pageSize != (int) file.length()) {
				throw new MemoryMapException(String
						.format("bad file size \"%s\"", file.getAbsolutePath()));
			}
//		} else if (readOnly) {
//			throw new MemoryMapException(String
//					.format("file not found \"%s\"", file.getAbsolutePath()));
		} else {
			return this;
		}
		for (int index = 1; ; index ++) {
			file = fileForBuffer(index);
			if (file.isFile() && file.length() > 0L) {
				if (pageSize != (int) file.length()) {
					throw new MemoryMapException(String
							.format("bad file size \"%s\"", file.getAbsolutePath()));
				}
				continue;
			}
			break;
		}
		return this;
	}
	
	private File fileForBuffer(int index) {
		return new File(folder, String.format("%04x", index));
	}
	
	private synchronized void initializeBufferIfNull(int index) throws MemoryMapException {
		if (index >= buffers.length) {
			final MappedByteBuffer[] extended = new MappedByteBuffer[1 + index];
			System.arraycopy(buffers, 0, extended, 0, buffers.length);
			Arrays.fill(extended, buffers.length, extended.length, null);
			buffers = extended;
		}
		if (null == buffers[index]) {
			try (RandomAccessFile file = new RandomAccessFile(fileForBuffer(index), fileMode)) {
				buffers[index] =  file.getChannel().map(mapMode, 0L, (long) pageSize);
			} catch (IOException e) {
				throw new MemoryMapException(e);
			}
		}
	}
	
	private MappedByteBuffer getBuffer(long position) throws MemoryMapException {
		final int index = (int) (position / (long) pageSize);
		if (index >= buffers.length || null == buffers[index]) {
			initializeBufferIfNull(index);
		}
		return buffers[index];			
	}
	
	private void cleanDirectBuffer(MappedByteBuffer buffer) {
	    if (null == buffer || !buffer.isDirect()) {
	    	return;
	    }
	    // we could use this type cast and call functions without reflection code,
	    // but static import from sun.* package is risky for non-SUN virtual machine.
	    try {
	        final Method cleaner = buffer
	        		.getClass().getMethod("cleaner");
	        cleaner.setAccessible(true);
	        final Method clean = Class
	        		.forName("sun.misc.Cleaner").getMethod("clean");
	        clean.setAccessible(true);
	        clean.invoke(cleaner.invoke(buffer));
	    } catch (Throwable e) {
	    	logger.error("cleanDirectBuffer", e);
	    }
	}
	
	@Override
	public File getFolder() {
		return folder;
	}

	@Override
	public boolean isReadOnly() {
		return readOnly;
	}
	
	@Override
	public int getPageSize() {
		return pageSize;
	}
	
	@Override
	public long getSizeOnDisk() {
		long sizeOnDisk = 0L;
		for (int index = 0; ; index ++) {
			final File file = fileForBuffer(index);
			if (file.isFile() && file.length() > 0L) {
				sizeOnDisk += file.length();
				continue;
			}
			break;
		}
		return sizeOnDisk;
	}
	
	private void garbage(int index) {
		synchronized (this) {
			for (; index < buffers.length; index ++) {
				final MappedByteBuffer buffer = buffers[index];
				if (null != buffer) {
					cleanDirectBuffer(buffer);
					buffers[index] = null;
				}
			}
		}		
	}
	
	@Override
	public UnsafeMemoryMap garbage() {
		garbage(0);
		return this;
	}

	@Override
	public UnsafeMemoryMap shrink(long size) {
		final int index = 1 + (int) (size / (long) pageSize);
		garbage(index);
		if (!readOnly) {
			deleteAllFiles(index);
		}
		return this;
	}
	
	@Override
	public UnsafeMemoryMap clear() {
		garbage(0);
		if (!readOnly) {
			deleteAllFiles(0);
		}
		return this;
	}
	
	@Override
	public long position() {
		return position;
	}

	@Override
	public UnsafeMemoryMap position(long position) {
		if (position < 0L)
			throw new ArrayIndexOutOfBoundsException(Long.toString(position));
		this.position = position;
		return this;
	}
	
	@Override
	public UnsafeMemoryMap put(byte[] src) throws MemoryMapException {
		put(src, 0, src.length);
		return this;
	}
	
	@Override
	public UnsafeMemoryMap put(byte[] src, int offset, int length) throws MemoryMapException {
		final int index = (int) (position % (long) pageSize);
		final int remaining = pageSize - index;
		if (remaining >= length) {
			final MappedByteBuffer buffer = getBuffer(position);
			buffer.position(index);
			buffer.put(src, offset, length);
			position += length;
		} else {
			put(src, offset, remaining);
			put(src, offset + remaining, length - remaining);
		}
		return this;
	}

	@Override
	public UnsafeMemoryMap put(byte value) throws MemoryMapException {
		codecBuffer[0] = value;
		put(codecBuffer, 0, 1);
		return this;
	}

	@Override
	public UnsafeMemoryMap putChar(char value) throws MemoryMapException {
		codecBuffer[0] = (byte) ((value >> 8) & 0xff);
		codecBuffer[1] = (byte) (value & 0xff);
		put(codecBuffer, 0, 2);
		return this;
	}
	
	@Override
	public UnsafeMemoryMap putShort(short value) throws MemoryMapException {
		codecBuffer[0] = (byte) ((value >> 8) & 0xff);
		codecBuffer[1] = (byte) (value & 0xff);
		put(codecBuffer, 0, 2);
		return this;
	}

	@Override
	public UnsafeMemoryMap putInt(int value) throws MemoryMapException {
		codecBuffer[0] = (byte) ((value >> 24) & 0xff);
		codecBuffer[1] = (byte) ((value >> 16) & 0xff);
		codecBuffer[2] = (byte) ((value >> 8) & 0xff);
		codecBuffer[3] = (byte) (value & 0xff);
		put(codecBuffer, 0, 4);
		return this;
	}

	@Override
	public UnsafeMemoryMap putLong(long value) throws MemoryMapException {
		codecBuffer[0] = (byte) ((value >> 56) & 0xffL);
		codecBuffer[1] = (byte) ((value >> 48) & 0xffL);
		codecBuffer[2] = (byte) ((value >> 40) & 0xffL);
		codecBuffer[3] = (byte) ((value >> 32) & 0xffL);
		codecBuffer[4] = (byte) ((value >> 24) & 0xffL);
		codecBuffer[5] = (byte) ((value >> 16) & 0xffL);
		codecBuffer[6] = (byte) ((value >> 8) & 0xffL);
		codecBuffer[7] = (byte) (value & 0xffL);
		put(codecBuffer, 0, 8);
		return this;
	}

	private void putByteArray(byte[] value) throws MemoryMapException {
		if (null == value) {
			putInt(-1);
		} else {
			putInt(value.length);
			if (value.length > 0)
				put(value, 0, value.length);
		}
	}
	
	@Override
	public UnsafeMemoryMap putString(String value, String charsetName) throws MemoryMapException, UnsupportedEncodingException {
		putByteArray(null == value ? null : value.getBytes(charsetName));
		return this;
	}
	
	@Override
	public MemoryMap putString(String value, Charset charset) throws MemoryMapException {
		putByteArray(null == value ? null : value.getBytes(charset));
		return this;
	}

	@Override
	public UnsafeMemoryMap putLongArray(LongArray value) throws MemoryMapException {
		final int size = value.size();
		putInt(size);
		final long[] array = value.array();
		for (int i = 0; i < size; i ++)
			putLong(array[i]);
		return this;
	}

	@Override
	public void get(byte[] dst) throws MemoryMapException {
		get(dst, 0, dst.length);
	}

	@Override
	public void get(byte[] dst, int offset, int length) throws MemoryMapException {
		final int index = (int) (position % (long) pageSize);
		final int remaining = pageSize - index;
		if (remaining >= length) {
			final MappedByteBuffer buffer = getBuffer(position);
			buffer.position(index);
			buffer.get(dst, offset, length);
			position += length;
		} else {
			get(dst, offset, remaining);
			get(dst, offset + remaining, length - remaining);
		}
	}

	@Override
	public byte get() throws MemoryMapException {
		get(codecBuffer, 0, 1);
		return codecBuffer[0];
	}

	@Override
	public char getChar() throws MemoryMapException {
		get(codecBuffer, 0, 2);
		final byte b0 = codecBuffer[0];
		final byte b1 = codecBuffer[1];
		return (char) (((b0 & 0xff) << 8) | (b1 & 0xff));
	}
	
	@Override
	public short getShort() throws MemoryMapException {
		get(codecBuffer, 0, 2);
		final byte b0 = codecBuffer[0];
		final byte b1 = codecBuffer[1];
		return (short) (((b0 & 0xff) << 8) | (b1 & 0xff));
	}

	@Override
	public int getInt() throws MemoryMapException {
		get(codecBuffer, 0, 4);
		final byte b0 = codecBuffer[0];
		final byte b1 = codecBuffer[1];
		final byte b2 = codecBuffer[2];
		final byte b3 = codecBuffer[3];
		return (int) (((b0 & 0xff) << 24) |
				((b1 & 0xff) << 16) |
				((b2 & 0xff) << 8) |
				(b3 & 0xff));
	}

	@Override
	public long getLong() throws MemoryMapException {
		get(codecBuffer, 0, 8);
		final byte b0 = codecBuffer[0];
		final byte b1 = codecBuffer[1];
		final byte b2 = codecBuffer[2];
		final byte b3 = codecBuffer[3];
		final byte b4 = codecBuffer[4];
		final byte b5 = codecBuffer[5];
		final byte b6 = codecBuffer[6];
		final byte b7 = codecBuffer[7];
		return (long) (((b0 & 0xffL) << 56) |
				((b1 & 0xffL) << 48) |
				((b2 & 0xffL) << 40) |
				((b3 & 0xffL) << 32) |
				((b4 & 0xffL) << 24) |
				((b5 & 0xffL) << 16) |
				((b6 & 0xffL) << 8) |
				(b7 & 0xffL));
	}
	
	private byte[] getByteArray() throws MemoryMapException {
		final int length = getInt();
		if (-1 == length)
			return null;
		final byte[] bytes = new byte[length];
		if (length > 0)
			get(bytes, 0, length);;
		return bytes;
	}

	@Override
	public String getString(String charsetName) throws MemoryMapException, UnsupportedEncodingException {
		final byte[] bytes = getByteArray();
		if (null == bytes)
			return null;
		return new String(bytes, charsetName);
	}
	
	@Override
	public String getString(Charset charset) throws MemoryMapException {
		final byte[] bytes = getByteArray();
		if (null == bytes)
			return null;
		return new String(bytes, charset);
	}
	
	@Override
	public LongArray getLongArray(LongArray array) throws MemoryMapException {
		final int size = getInt();
		for (int i = 0; i < size; i ++)
			array.add(getLong());
		return array;
	}
	
	@Override
	public LongArray getLongArray() throws MemoryMapException {
		final int size = getInt();
		final LongArray array = new LongArray(size);
		for (int i = 0; i < size; i ++)
			array.add(getLong());
		return array;
	}
	
}
