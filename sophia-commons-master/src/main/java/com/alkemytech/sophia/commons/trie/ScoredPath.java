package com.alkemytech.sophia.commons.trie;

import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ScoredPath<T> {

	public final int distance;
	public final String path;
	public final T leaf;
	
	public ScoredPath(int distance, String path, T leaf) {
		this.distance = distance;
		this.path = path;
		this.leaf = leaf;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.disableHtmlEscaping()
				.create()
				.toJson(this);
	}

}
