package com.alkemytech.sophia.commons.util;

import java.util.Arrays;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public final class LongArray {
	
	public static boolean isNullOrEmpty(LongArray array) {
		return null == array || 0 == array.size;
	}
	
	public static final LongArray intersection(LongArray leftArray, LongArray rightArray) {
//		if (null == leftArray) {
//			return null;
//		}
//		if (null == rightArray) {
//			return null;
//		}
		return intersection(leftArray, rightArray, 
				new LongArray(leftArray.size < rightArray.size ? leftArray.size : rightArray.size));
	}

	public static final LongArray intersection(LongArray leftArray, LongArray rightArray, LongArray resultArray) {
		if (0 == leftArray.size) {
			return resultArray;
		} else if (0 == rightArray.size) {
			return resultArray;
		} else if (leftArray.size > rightArray.size) {
			final LongArray swapArray = rightArray;
			rightArray = leftArray;
			leftArray = swapArray;
		}
		int leftIndex = 0, rightIndex = 0;
		while (leftIndex < leftArray.size && rightIndex < rightArray.size) {
			final long leftValue = leftArray.array[leftIndex];
			final long rightValue = rightArray.array[rightIndex];
			if (leftValue < rightValue) {
				leftIndex ++;
			} else if (leftValue > rightValue) {
				rightIndex ++;
			} else {
				resultArray.add(leftValue);
				leftIndex ++;
				rightIndex ++;				
			}
		}
		return resultArray;
	}

	public static final LongArray union(LongArray leftArray, LongArray rightArray) {
//		if (null == leftArray) {
//			if (null == rightArray) {
//				return null;
//			}
//			return new LongArray(rightArray);
//		}
//		if (null == rightArray) {
//			return new LongArray(leftArray);
//		}
		return union(leftArray, rightArray,
				new LongArray(leftArray.size > rightArray.size ? leftArray.size : rightArray.size));
	}

	public static final LongArray union(LongArray leftArray, LongArray rightArray, LongArray resultArray) {
		if (0 == leftArray.size) {
			return rightArray;
		} else if (0 == rightArray.size) {
			return leftArray;
		}
		int leftIndex = 0, rightIndex = 0;
		while (leftIndex < leftArray.size && rightIndex < rightArray.size) {
			final long leftValue = leftArray.array[leftIndex];
			final long rightValue = rightArray.array[rightIndex];
			if (leftValue < rightValue) {
				resultArray.add(leftValue);
				leftIndex ++;
			} else if (leftValue > rightValue) {
				resultArray.add(rightValue);
				rightIndex ++;
			} else {
				resultArray.add(leftValue);
				leftIndex ++;
				rightIndex ++;				
			}
		}
		for ( ; leftIndex < leftArray.size; leftIndex ++) {
			resultArray.add(leftArray.array[leftIndex]);
		}
		for ( ; rightIndex < rightArray.size; rightIndex ++) {
			resultArray.add(rightArray.array[rightIndex]);
		}
		return resultArray;
	}
		
	private long[] array;
	private int size;

	public LongArray() {
		super();
		this.array = new long[1];
		this.size = 0;
	}
	
	public LongArray(int capacity) {
		super();
		this.array = new long[capacity];
		this.size = 0;
	}

	public LongArray(long...values) {
		super();
		this.array = new long[values.length];
		this.size = values.length;
		System.arraycopy(values, 0, this.array, 0, this.size);
	}

	public LongArray(LongArray that) {
		super();
		this.array = new long[that.size];
		this.size = that.size;
		System.arraycopy(that.array, 0, this.array, 0, that.size);
	}

	private final void ensureCapacity(int capacity) {
		int length = array.length;
		if (capacity >= length) {
			length += length;
			capacity = capacity > length ? capacity : length;
			final long[] extended = new long[capacity];
			System.arraycopy(array, 0, extended, 0, size);
			array = extended;
		}
	}
	
	public final int capacity() {
		return array.length;
	}
	
	public final int size() {
		return size;
	}
	
	public final long[] array() {
		return array;
	}

	public final void clear() {
		size = 0;
	}
	
	public final boolean isEmpty() {
		return size < 1;
	}
	
	public final void add(long value) {
		ensureCapacity(1 + size);
		array[size ++] = value;
	}

	public final void add(int index, long value) {
		if (index < 0 || index > size) {
			throw new IndexOutOfBoundsException();
		}
		ensureCapacity(1 + size);
		if (index < size) {
			System.arraycopy(array, index, array, 1 + index, size - index);
		}
		array[index] = value;
		size ++;
	}

	public final void addAll(LongArray that) {
		ensureCapacity(size + that.size);
		System.arraycopy(that.array, 0, array, size, that.size);
		size += that.size;
	}

	public final long get(int index) {
		return array[index];
	}
	
	public final void set(int index, long value) {
		array[index] = value;
	}

	/**
	 * Searches the specified array of longs for the specified value using the binary search algorithm.
	 * The array must be sorted prior to making this call. If it is not sorted, the results are undefined.
	 * If the array contains multiple elements with the specified value, there is no guarantee which one
	 * will be found.
	 * 
	 * @param value the value to be searched for
	 * @return index of the search value, if it is contained in the array; otherwise, (-(insertion point) - 1).
	 * The insertion point is defined as the point at which the value would be inserted into the array: the
	 * index of the first element greater than the value, or size() if all elements in the array are less than
	 * the specified value. Note that this guarantees that the return value will be >= 0 if and only if the
	 * value is found.
	 */
	public final int indexOf(long value) {
		int lo = 0;
        int hi = size - 1;
        int index = lo + (hi - lo) / 2;
        while (lo <= hi) {
        	final long val = array[index];
            if (value < val) {
            	hi = index - 1;
            } else if (value > val) {
            	lo = index + 1;
            } else {
            	return index;
            }
        	index = lo + (hi - lo) / 2;
        }
        return -index - 1;
	}

	public final void sort() {
		Arrays.sort(array, 0, size);
	}
	

	@Override
	public final String toString() {
		final StringBuilder builder = new StringBuilder()
				.append('[');
		if (size > 0) {
			builder.append(array[0]);
			for (int i = 1; i < size; i ++) {
				builder.append(',')
					.append(array[i]);
			}
		}
		return builder.append(']')
				.toString();
	}
	
//	public static void main(String[] args) {
//		
//		LongList a = new LongArray(1, 3L, 4, 6, 7);
//		LongList b = new LongArray(2, 3, 6, 7L, 8);
//		LongList c = new LongArray(1, 3, 5, 7);
//		System.out.println("a = " + a);
//		System.out.println("b = " + b);
//		System.out.println("c = " + c);
//		System.out.println("b AND c = " + intersection(b, c));
//		System.out.println("a OR b AND c = " + union(a, intersection(b, c)));
//
//		LongList d = new LongArray(10);
//		for (int i = 10; i > 0; i --) {
//			int pos = d.indexOf(i);
//			if (pos < 0) {
//				d.add(-pos - 1, i);
//			}
//		}
//		System.out.println("d = " + d);
//
//		System.out.println("a.indexOf(0) = " + a.indexOf(0L));
//		System.out.println("a.indexOf(2) = " + a.indexOf(2L));
//		System.out.println("a.indexOf(4) = " + a.indexOf(4L));
//		System.out.println("a.indexOf(5) = " + a.indexOf(5L));
//		System.out.println("a.indexOf(7) = " + a.indexOf(7L));
//
//	}
	
}

