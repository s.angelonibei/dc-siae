package com.alkemytech.sophia.commons.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;

import com.alkemytech.sophia.commons.index.LevenshteinIndex;
import com.alkemytech.sophia.commons.index.LevenshteinIndex.TermSet;
import com.alkemytech.sophia.commons.util.LongArray;
import com.google.inject.Inject;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ExactTitleArtistQuery implements TitleArtistQuery {

	@Inject
	protected ExactTitleArtistQuery() {
		super();
	}

	public ExactTitleArtistQuery(Properties configuration, String propertyPrefix) {
		super();
	}
	
	private LongArray exactSearch(LevenshteinIndex reverseIndex, String text) {
		final TermSet termSet = reverseIndex.search(text);
		if (termSet.next())
			return termSet.getPostings();
		return null;
	}
	
	@Override
	public void startup(Properties configuration) {

	}
	
	@Override
	public void shutdown() {
		
	}
	
	@Override
	public LongArray search(LevenshteinIndex titleIndex, LevenshteinIndex artistIndex, Collection<String> titleTerms, Collection<String> artistTerms) {
		
		// title search
		final List<LongArray> titleTermsPostings = new ArrayList<>(titleTerms.size());
		for (String term : titleTerms) {
			final LongArray postings = exactSearch(titleIndex, term);
			if (null != postings && !postings.isEmpty()) {
				titleTermsPostings.add(postings);
			}
		}
		if (titleTermsPostings.isEmpty()) {
			return null;
		}
		
		// sort postings list (ascending length) to speed up intersections
		Collections.sort(titleTermsPostings, new Comparator<LongArray>() {

			@Override
			public int compare(LongArray o1, LongArray o2) {
				return Integer.compare(o1.size(), o2.size());
			}
			
		});
		
		// compute title terms piecewise intersection
		LongArray resultPostings = null;
		if (titleTermsPostings.size() > 2) {
			resultPostings = titleTermsPostings.get(0);
			for (int i = 1; i < titleTermsPostings.size(); i ++) {
				resultPostings = LongArray
						.intersection(resultPostings, titleTermsPostings.get(i));
				if (null == resultPostings || resultPostings.isEmpty()) {
					return null;
				}
			}
		} else if (titleTermsPostings.size() > 1) {
			resultPostings = LongArray
					.intersection(titleTermsPostings.get(0), titleTermsPostings.get(1));
		} else if (titleTermsPostings.size() > 0) {
			resultPostings = titleTermsPostings.get(0);
		}
		if (null == resultPostings || resultPostings.isEmpty()) {
			return null;
		}

		// artist search
		for (String term : artistTerms) {
			final LongArray postings = exactSearch(artistIndex, term);
			if (null == postings || postings.isEmpty()) {
				return null;
			}
			// compute artist terms intersections with title postings
			resultPostings = LongArray.intersection(postings, resultPostings);
			if (null == resultPostings || resultPostings.isEmpty()) {
				return null;
			}
		}

		return resultPostings;
	}
	
}
