package com.alkemytech.sophia.commons.query;

import java.util.Collection;

import com.alkemytech.sophia.commons.index.LevenshteinIndex;
import com.alkemytech.sophia.commons.util.LongArray;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class AbstractAndQueryChain extends AbstractQueryChain {

	/*
	 * (non-Javadoc)
	 * @see com.alkemytech.sophia.commons.query.IndexQueryChain#search(com.alkemytech.sophia.commons.util.LongArray, com.alkemytech.sophia.commons.index.LevenshteinIndex, java.util.Collection)
	 */
	@Override
	public LongArray search(LongArray postingsChain, LevenshteinIndex index, Collection<String> terms) {
		
		// nothing to search
		if (null == terms || terms.isEmpty()) {
			return null;
		}

		// search(es)
		LongArray resultPostings = postingsChain;
		for (String term : terms) {
			LongArray termPostings = searchTerm(index, term);
			if (LongArray.isNullOrEmpty(termPostings)) {
				return null;
			}
			// intersection with postings chain
			if (null == resultPostings) {
				resultPostings = termPostings;
			} else {
				resultPostings = LongArray.intersection(resultPostings, termPostings);
				if (LongArray.isNullOrEmpty(resultPostings)) {
					return null;
				}
			}
		}
		
		// return result(s)
		return LongArray.isNullOrEmpty(resultPostings) ? null : resultPostings;
	}

}
