package com.alkemytech.sophia.commons.query;

import com.alkemytech.sophia.commons.index.LevenshteinIndex;
import com.alkemytech.sophia.commons.util.LongArray;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class FuzzySmartQueryChain extends AbstractSmartQueryChain {

	private final int[] fuzzyCosts;

	public FuzzySmartQueryChain(int maxTerms, int minTerms, int[] fuzzyCosts) {
		super(maxTerms, minTerms);
		this.fuzzyCosts = fuzzyCosts;
	}

	@Override
	protected LongArray searchTerm(LevenshteinIndex index, String text) {
		return fuzzySearch(index, text, fuzzyCosts);
	}
	
}
