package it.siae.msgestioneconflitti.report.csv;

import lombok.Data;
import net.sf.jsefa.csv.annotation.CsvDataType;

import java.io.Serializable;

@Data
public class CsvInput implements Serializable {
    String sr_approx_daily_views;
    String sr_asset_id;
    String sr_isrc;
    String sr_title;
    String sr_artist;
    String embedded_composition_asset_id;
    String composition_share_custom_id;
    String composition_share_asset_id;
    String composition_share_iswc;
    String composition_share_hfaSong_code;
    String composition_share_title;
    String composition_share_writers;
    String composition_share_asset_labels;
    String conflicting_owner;
    String conflicting_right_type;
    String conflicting_country_code;
    String my_ownership_last_updated;
    String other_ownership_last_updated;
    String other_ownership_origination;
    String my_ownership_origination;
    String views_in_conflict;
    String conflicting_country_percentage;

}
