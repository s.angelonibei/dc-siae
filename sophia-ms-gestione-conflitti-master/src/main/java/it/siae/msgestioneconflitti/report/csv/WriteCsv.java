/*
 * @Autor A.D.C.
 */

package it.siae.msgestioneconflitti.report.csv;

import com.alkemytech.sophia.royalties.claim.ClaimResult;
import it.siae.msgestioneconflitti.entity.MmRichiesteGestioneConflitti;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;

public interface WriteCsv {
    boolean createYtFiles(List<CsvInput> csvConflitti, File ytAudioFile, File ytStagingFile, MmRichiesteGestioneConflitti report) throws Exception;

    boolean createYtStagingFile(List<CsvInput> in, String path, File ytStaginFile, MmRichiesteGestioneConflitti report, List<YtAudioRow> ytAudioRows, HashMap<CsvInput, String> rows, HashMap<String, String> claimMap) throws Exception;

    boolean uploadCsvFile(MmRichiesteGestioneConflitti report, File ytAudio, File ytStaging) throws Exception;

    ClaimResult doClaim(String uuid, String territory, String dspCode, String schemiRiparto) throws IOException, InterruptedException;

    List<CsvInput> downloadToS3(MmRichiesteGestioneConflitti report) throws FileNotFoundException, UnsupportedEncodingException;
}
