/*
 * @Autor A.D.C.
 */

package it.siae.msgestioneconflitti.report.csv;

import com.alkemy.siae.sophia.pricing.DsrLine;
import com.alkemy.siae.sophia.pricing.DsrMetadata;
import com.alkemytech.sophia.common.config.ConfigurationLoader;
import com.alkemytech.sophia.commons.aws.S3;
import it.siae.msgestioneconflitti.client.RestClient;
import com.alkemytech.sophia.commons.util.FileUtils;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.royalties.Configuration;
import com.alkemytech.sophia.royalties.LatestVersion;
import com.alkemytech.sophia.royalties.blacklist.BlackList;
import com.alkemytech.sophia.royalties.claim.ClaimHelper;
import com.alkemytech.sophia.royalties.claim.ClaimResult;
import com.alkemytech.sophia.royalties.claim.SocietyInfo;
import com.alkemytech.sophia.royalties.nosql.IpiSocNoSqlDb;
import com.alkemytech.sophia.royalties.nosql.OperaNoSqlDb;
import com.alkemytech.sophia.royalties.nosql.UuidCodiciNoSqlDb;
import com.alkemytech.sophia.royalties.role.RoleCatalog;
import com.alkemytech.sophia.royalties.scheme.IrregularityCatalog;
import com.alkemytech.sophia.royalties.scheme.RoyaltySchemeService;
import com.alkemytech.sophia.royalties.scheme.TemporaryWorkCatalog;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressEventType;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.google.common.collect.Lists;
import com.google.gson.JsonObject;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import it.siae.msgestioneconflitti.dto.RicercaOpereKBDTO;
import it.siae.msgestioneconflitti.entity.MmRichiesteGestioneConflitti;
import it.siae.msgestioneconflitti.entity.enumeration.StatoGestioneConflittiEnum;
import it.siae.msgestioneconflitti.service.CommonsService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.QuoteMode;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.math.RoundingMode;
import java.nio.charset.Charset;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Service
@Transactional
@EnableConfigurationProperties
public class WriteCsvImpl implements WriteCsv {

    private static final Logger logger = LoggerFactory.getLogger(WriteCsvImpl.class);

    private final String DSP = "youtube (trimestrali)";

    private final static String SOCIETY_NOSQL = "pricing_and_claim.society_nosql";

    private final static String IPI_NOSQL = "pricing_and_claim.ipi_nosql";

    private final static String CODICI_NOSQL = "pricing_and_claim.codici_nosql";

    private Properties properties = new ConfigurationLoader()
            .withFilePath("configuration.properties")
            .load();

    private IrregularityCatalog irregularityCatalog;

    private TemporaryWorkCatalog temporaryWorkCatalog;

    private RoyaltySchemeService royaltySchemeService;

    private RoleCatalog roleCatalog;

    private BlackList blackList;

    @Autowired
    private Environment env;

    @Autowired
    private CommonsService commonsService;

    @Value("${spring.s3uri.path}")
    public String path;

    @Value("${spring.s3uri.fileInputLocation}")
    public String fileInputLocation;

    @Value("${s3.retry.put_object}")
    private String put_object;

    @Value("${spring.cloud.aws.credentials.accessKey}")
    private String accessKey;

    @Value("${spring.cloud.aws.credentials.secretKey}")
    private String secretKey;

    @Value("${spring.cloud.aws.region.static}")
    private String region;

    @Value("${thread.pool.size}")
    private String trheadMax;

    @Value("${executor_shutdown}")
    private String executorShutdown;

    @Value("${ricerca.opere}")
    private String ricercaOpere;

    @Value("${pricing_and_claim.data_folder}")
    private String pricingAndClaimDataFolder;

    //IPI_NOSQL
    @Value("${ipi_nosql.latest_url}")
    private String ipiLatestUrl;

    @Value("${ipi_nosql.latest_file}")
    private String ipiLatestFile;

    @Value("${ipi_nosql.home_folder}")
    private String ipiHf;

    @Value("${ipi_nosql.cache_size}")
    private String ipiCache;

    @Value("${ipi_nosql.create_always}")
    private String ipiCrAlways;

    @Value("${ipi_nosql.read_only}")
    private String ipiRo;

    //CODICI_NOSQL
    @Value("${codici_nosql.latest_url}")
    private String codiciLatestUrl;

    @Value("${codici_nosql.latest_file}")
    private String codiciLatestFile;

    @Value("${codici_nosql.home_folder}")
    private String codiciHf;

    @Value("${codici_nosql.cache_size}")
    private String codiciCache;

    @Value("${codici_nosql.trie_class_name}")
    private String codiciTrie;

    @Value("${codici_nosql.create_always}")
    private String codiciCrAlways;

    @Value("${codici_nosql.read_only}")
    private String codiciRo;

    //SOCIETY_NOSQL
    @Value("${society_nosql.latest_url}")
    private String socLatestUrl;

    @Value("${society_nosql.latest_file}")
    private String socLatestFile;

    @Value("${society_nosql.home_folder}")
    private String socHf;

    @Value("${society_nosql.cache_size}")
    private String socCache;

    @Value("${society_nosql.create_always}")
    private String socCrAlways;

    @Value("${society_nosql.read_only}")
    private String socRo;

    @Value("${irregularity_catalog.home_url}")
    private String irrcatHomeUrl;

    @Value("${irregularity_catalog.csv_url}")
    private String irrcatCsvUrl;

    @Value("${temporary_work_catalog.home_url}")
    private String tempWorkHomeUrl;

    @Value("${temporary_work_catalog.csv_url}")
    private String tempWorkCsvUrl;

    @Value("${role_catalog.home_url}")
    private String roleHomeUrl;

    @Value("${role_catalog.csv_url}")
    private String rolekCsvUrl;

    @Value("${charset}")
    Charset charset;


    public boolean createYtFiles(List<CsvInput> csvConflitti, File ytAudioFile, File ytStagingFile, MmRichiesteGestioneConflitti report) throws Exception {
        List<YtAudioRow> ytAudioRows = new ArrayList<>();
        HashMap<CsvInput, String> uuidsMap = new HashMap<>();
        HashMap<String, String> claimMap = new HashMap<>(); //{Riga}{Nuovo Claim}
        final Integer maxThread = Integer.parseInt(trheadMax);
        final Integer executorTimeout = Integer.parseInt(executorShutdown);

        int partitionSize = csvConflitti.size() / maxThread;

        if (csvConflitti.size() <= maxThread) {
            partitionSize = csvConflitti.size();
        }
        logger.debug("records " + csvConflitti);
        logger.debug("partitionSize " + partitionSize);


        //Threads
        ExecutorService es = Executors.newCachedThreadPool();
        try {
            for (final List<CsvInput> partitions : Lists.partition(csvConflitti, partitionSize)) {
                es.execute(new Runnable() {
                    @Override
                    public void run() {
                        logger.info("Starting elaborateRecords on new Thread " + Thread.currentThread() + " " + partitions.size() + " files");
                        System.out.println("Starting on new Thread " + Thread.currentThread() + " " + partitions.size() + " files");
                        //call service search
                        //In questa mappa di stringhe  salverò tutte le triple [composition_share_asset_id, conflicting_country_code,UUID] per non elaborare più volte il calim per la stessa opera
                        HashMap<String, String> exists = new HashMap<>();
                        for (CsvInput row : partitions) {
                            try {
                                String uuid = null;
                                if (row.getComposition_share_custom_id() != null && !row.getComposition_share_custom_id().isEmpty()) {
                                    //da colonna G sempre prendere uuid
                                    String codiceOpera = row.getComposition_share_custom_id();
                                    int lenght = codiceOpera.length();
                                    //considerare le righe che rispettano le regole per la colonna G nel SRS
                                    if (!codiceOpera.startsWith("S") && lenght > 0) {

                                        if (lenght < 11) {
                                            String zerosToAdd = "";
                                            for (int j = lenght; j < 11; j++) {
                                                zerosToAdd += "0";
                                            }
                                            codiceOpera = zerosToAdd + codiceOpera;
                                            lenght = codiceOpera.length();
                                        }

                                        if (lenght == 11) {
                                            RicercaOpereKBDTO ro = new RicercaOpereKBDTO();
                                            ro.setCodice(codiceOpera);
                                            uuid = ricercaOpereMassive(ro, ricercaOpere);
                                        } else if (lenght == 36) {
                                            uuid = codiceOpera;
                                        }
                                    } else if (codiceOpera.startsWith("S")) {
                                        //Sarà gestito nel metodo toAudioFile settando automaticamente il claim a 0 in quanto codice provvisiorio
                                        uuid = codiceOpera;
                                    }
                                    logger.info("UUID -->" + uuid);
                                    if (uuid != null) {
                                        //considerare le righe hanno il conflicting_country_code uguale al territorio scelto in fase di creazione richiesta utilizzando lo schema riparto Italia per italia o Francia per il resto
                                        List<YtAudioRow> ytAudioRow = toAudioFile(row, report, uuid, claimMap, exists);
                                        uuidsMap.put(row, uuid);
                                        ytAudioRows.addAll(ytAudioRow);
                                    }
                                }
                            } catch (IOException | InterruptedException e) {
                                e.printStackTrace();
                                logger.debug("1 catch " + e);
                            }
                        }
                        System.out.println("Terminated Thread " + Thread.currentThread() + " " + partitions.size() + " files");
                    }
                });
            }
            es.shutdown();
            es.awaitTermination(executorTimeout, TimeUnit.MINUTES);

            logger.debug("YtAudioFile completed ... ");
            System.out.println("Elaborazione terminata");

        } catch (Exception e) {
            report.setStato(StatoGestioneConflittiEnum.ERRORE.getName());
            report.setYtAudio("");
            report.setYtStaging("");
            report.setErrore(e.getMessage());
            commonsService.updateReportStatus(report);
            System.out.println("Elaborazione terminata con errore " + e.getMessage());
            return false;
        }
        saveYtAudioFile(ytAudioFile, report, ytAudioRows);
        return createYtStagingFile(csvConflitti, path, ytStagingFile, report, ytAudioRows, uuidsMap, claimMap);
    }

    public boolean createYtStagingFile(List<CsvInput> in, String path, File ytStaginFile, MmRichiesteGestioneConflitti report, List<YtAudioRow> ytAudioRows, HashMap<CsvInput, String> uuidsMap, HashMap<String, String> claimMap) throws Exception {
        logger.info("Creating YtStaginFile ...");
        List<YtStagingFile> ytList = new ArrayList<>();
        String territorio = report.getTerritorio().equalsIgnoreCase("IT")
                ? "IT" : report.getTerritorio().equalsIgnoreCase("R")
                ? "FR" : "E";
        for (CsvInput i : in) {
            //Come concordato con Athan il giorno 18/03 rimuovo dal file di staging le righe aventi i territori non presenti nella richiesta
            if (((territorio.equalsIgnoreCase("IT") && i.getConflicting_country_code().contains(territorio))
                    || (territorio.equalsIgnoreCase("FR") && !i.getConflicting_country_code().replace("IT", "").replace("||", "|").contains("IT") && (!i.getConflicting_country_code().replace("IT","").replace("||","|").equalsIgnoreCase("") && i.getConflicting_country_code().replace("IT", "").replace("||", "|").split("\\|").length>0))
                    || territorio.equalsIgnoreCase("E"))
                    && StringUtils.isNotEmpty(i.getComposition_share_custom_id())
            ) {
                YtStagingFile y = new YtStagingFile();
                y.setSrApproxDailyViews(i.getSr_approx_daily_views());
                y.setSrAssetId(i.getSr_asset_id());
                y.setSrIsrc(i.getSr_isrc());
                y.setSrTitle(i.getSr_title());
                y.setSrArtist(i.getSr_artist());
                y.setEmbeddedCompositionAssetId(i.getEmbedded_composition_asset_id());
                y.setCompositionShareCustomId(i.getComposition_share_custom_id());
                y.setCompositionShareAssetId(i.getComposition_share_asset_id());
                y.setCompositionShareIswc(i.getComposition_share_iswc());
                y.setCompositionShareHfaSongCode(i.getComposition_share_hfaSong_code());
                y.setCompositionShareTitle(i.getComposition_share_title());
                y.setCompositionShareWriters(i.getComposition_share_writers());
                y.setCompositionShareAssetLabels(i.getComposition_share_asset_labels());
                y.setConflictingOwner(i.getConflicting_owner());
                y.setConflictingRightType(i.getConflicting_right_type());
                y.setConflictingCountryCode(i.getConflicting_country_code());
                y.setMyOwnershipLastUpdated(i.getMy_ownership_last_updated());
                y.setOtherOwnershipLastUpdated(i.getOther_ownership_last_updated());
                y.setOtherOwnershipOrigination(i.getOther_ownership_origination());
                y.setMyOwnershipOrigination(i.getMy_ownership_origination());
                y.setViewsInConflict(i.getViews_in_conflict());
                y.setConflictingCountryPercentage(i.getConflicting_country_percentage());
                y.setUuid(uuidsMap.get(i));
                y.setNuovoValoreClaim(claimMap.get(i.getConflicting_country_code() + "@" + i.getComposition_share_asset_id() + "@" + i.getConflicting_right_type())); //Map MechanicalOwnershipShare
                logger.debug("YtStagin NUOVO VALORE CLAIM " + y.getNuovoValoreClaim() + " TIPO " + i.getConflicting_right_type());
                ytList.add(y);
            }
        }
        saveYtStagingFile(path, ytStaginFile, report, ytList);

        return true;
    }

    public ClaimResult doClaim(String uuid, String territory, String dspCode, String schemiRiparto) throws IOException, InterruptedException {

        DsrMetadata dsrMetadata = new DsrMetadata();
        dsrMetadata.setDspCode(dspCode);
        String[] yearMonth = schemiRiparto.split("-");
        dsrMetadata.setYear(yearMonth[0]);   //DA SCHEMA RIPARTO
        dsrMetadata.setMonth(yearMonth[1]); //DA SCHEMA RIPARTO
        SocietyInfo societyInfo = new SocietyInfo();
        societyInfo.setBlackList(blackList);
        societyInfo.setSociety("SIAE");
        societyInfo.setTerritory(territory);
        dsrMetadata.setTenant(societyInfo);

        DsrLine dsrLine = new DsrLine(null);
        dsrLine.setUuidSophia(uuid);
        dsrLine.setTerritory(territory);

        final Map<String, OperaNoSqlDb> operaNoSqlDbs = new HashMap<>();

        properties = new ConfigurationLoader()
                .withFilePath("configuration.properties")
                .load();
        Configuration configuration = new Configuration(properties);
        configuration.setProperty(SOCIETY_NOSQL + ".latest_url", socLatestUrl.replace("{year}{month}", dsrMetadata.getYear() + dsrMetadata.getMonth()));
        configuration.setProperty(SOCIETY_NOSQL + ".latest_file", socLatestFile.replace("{year}{month}", dsrMetadata.getYear() + dsrMetadata.getMonth()));
        configuration.setProperty(SOCIETY_NOSQL + ".home_folder", socHf.replace("{year}{month}", dsrMetadata.getYear() + dsrMetadata.getMonth()));
        configuration.setProperty(SOCIETY_NOSQL + ".cache_size", socCache);
        configuration.setProperty(SOCIETY_NOSQL + ".create_always", socCrAlways);
        configuration.setProperty(SOCIETY_NOSQL + ".read_only", socRo);

        configuration.setProperty(IPI_NOSQL + ".latest_url", ipiLatestUrl.replace("{year}{month}", dsrMetadata.getYear() + dsrMetadata.getMonth()));
        configuration.setProperty(IPI_NOSQL + ".latest_file", ipiLatestFile.replace("{year}{month}", dsrMetadata.getYear() + dsrMetadata.getMonth()));
        configuration.setProperty(IPI_NOSQL + ".home_folder", ipiHf.replace("{year}{month}", dsrMetadata.getYear() + dsrMetadata.getMonth()));
        configuration.setProperty(IPI_NOSQL + ".cache_size", ipiCache);
        configuration.setProperty(IPI_NOSQL + ".create_always", ipiCrAlways);
        configuration.setProperty(IPI_NOSQL + ".read_only", ipiRo);

        configuration.setProperty(CODICI_NOSQL + ".latest_url", codiciLatestUrl.replace("{year}{month}", dsrMetadata.getYear() + dsrMetadata.getMonth()));
        configuration.setProperty(CODICI_NOSQL + ".latest_file", codiciLatestFile.replace("{year}{month}", dsrMetadata.getYear() + dsrMetadata.getMonth()));
        configuration.setProperty(CODICI_NOSQL + ".home_folder", codiciHf.replace("{year}{month}", dsrMetadata.getYear() + dsrMetadata.getMonth()));
        configuration.setProperty(CODICI_NOSQL + ".cache_size", codiciCache);
        configuration.setProperty(CODICI_NOSQL + ".create_always", codiciCrAlways);
        configuration.setProperty(CODICI_NOSQL + ".read_only", codiciRo);

        configuration.setProperty("irregularity_catalog.home_url", irrcatHomeUrl);
        configuration.setProperty("irregularity_catalog.csv_url", irrcatCsvUrl);

        configuration.setProperty("temporary_work_catalog.home_url", tempWorkHomeUrl);
        configuration.setProperty("temporary_work_catalog.csv_url", tempWorkCsvUrl);

        configuration.setProperty("role_catalog.home_url", roleHomeUrl);
        configuration.setProperty("role_catalog.csv_url", rolekCsvUrl);

        logger.debug("CHARSET " + charset);

        RoleCatalog roleCatalog = new RoleCatalog(charset);

        TemporaryWorkCatalog temporaryWorkCatalog = new TemporaryWorkCatalog(charset);

        IrregularityCatalog irregularityCatalog = new IrregularityCatalog(charset);

        S3 s3 = new S3(properties);
        s3.startup();

        // initialize role catalog
        logger.debug("Role catalog csv url " + configuration.getProperty("role_catalog.csv_url"));
        roleCatalog.load(s3, configuration.getProperty("role_catalog.csv_url"));

        // initialize temporary work catalog
        logger.debug("temporary work catalog csv url " + configuration.getProperty("temporary_work_catalog.csv_url"));
        temporaryWorkCatalog.load(s3, configuration.getProperty("temporary_work_catalog.csv_url"));

        // initialize irregularity catalog
        logger.debug("irregularityCatalog csv url " + configuration.getProperty("irregularity_catalog.csv_url"));
        irregularityCatalog.load(s3, configuration.getProperty("irregularity_catalog.csv_url"));


        // files and folders to keep
        final Set<String> dataFilesToKeep = new HashSet<>();

        // initialize codici nosql to latest version
        dataFilesToKeep.add(configuration.getProperty(CODICI_NOSQL + ".latest_file"));
        dataFilesToKeep.add(configuration.getProperty(CODICI_NOSQL + ".home_folder"));
        logger.debug("call downloadAndExtractLatest CODICI_NOSQL");
        downloadAndExtractLatest("pricing_and_claim", CODICI_NOSQL, configuration);
        UuidCodiciNoSqlDb codiciNoSqlDb = new UuidCodiciNoSqlDb(configuration
                .getProperties(), "pricing_and_claim.codici_nosql");

        // initialize ipi nosql to latest version
        dataFilesToKeep.add(configuration.getProperty(IPI_NOSQL + ".latest_file"));
        dataFilesToKeep.add(configuration.getProperty(IPI_NOSQL + ".home_folder"));
        logger.debug("call downloadAndExtractLatest IPI_NOSQL");
        downloadAndExtractLatest("pricing_and_claim", IPI_NOSQL, configuration);
        IpiSocNoSqlDb ipiNoSqlDb = new IpiSocNoSqlDb(configuration
                .getProperties(), "pricing_and_claim.ipi_nosql");

        // initialize tenant nosql to latest version
        configuration.setTag("{society}", dsrMetadata.getTenant().getSociety());
        dataFilesToKeep.add(configuration.getProperty(SOCIETY_NOSQL + ".latest_file"));
        dataFilesToKeep.add(configuration.getProperty(SOCIETY_NOSQL + ".home_folder"));
        logger.debug("call downloadAndExtractLatest SOCIETY_NOSQL");
        downloadAndExtractLatest("pricing_and_claim", SOCIETY_NOSQL, configuration);
        operaNoSqlDbs.put(
                "SIAE",
                new OperaNoSqlDb(configuration.getProperties(), SOCIETY_NOSQL)
        );

        // cleanup data folder (if exceeding max size)
        final long dataFolderMaxSize = TextUtils
                .parseLongSize(configuration.getProperty("pricing_and_claim.data_limit", "20Gb"));
        final File dataFolder = new File(configuration
                .getProperty("pricing_and_claim.data_folder"));
        logger.debug("dataFolder {}", dataFolder);
        logger.debug("dataFolderMaxSize {}", dataFolderMaxSize);
        logger.debug("dataFilesToKeep {}", dataFilesToKeep);
        cleanupFolder(dataFolder, dataFolderMaxSize, dataFilesToKeep);

        royaltySchemeService = new RoyaltySchemeService(
                configuration,
                roleCatalog,
                temporaryWorkCatalog,
                irregularityCatalog);

        ClaimHelper claimHelper = new ClaimHelper(configuration,
                ipiNoSqlDb,
                codiciNoSqlDb,
                operaNoSqlDbs,
                royaltySchemeService);
        logger.debug("Sto per fare il claim");
        ClaimResult claimResult;
        claimResult = claimHelper.claim(dsrMetadata, dsrLine);

        return claimResult;
    }

    protected void cleanupFolder(File folder, long maxSize, Set<String> pathsToSkip) {
        if (maxSize <= 0L) {
            return;
        }
        final long size = computeSize(folder, pathsToSkip);
        logger.warn("cleanupFolder: size {} ({})", size, TextUtils.formatSize(size));
        if (size <= maxSize) {
            return;
        }
        for (File file : folder.listFiles()) {
            boolean delete = true;
            if (null != pathsToSkip) {
                for (String pathToSkip : pathsToSkip) {
                    if (file.getAbsolutePath()
                            .startsWith(pathToSkip)) {
                        delete = false;
                        break;
                    }
                }
            }
            if (delete) {
                if (file.isDirectory()) {
                    logger.debug("deleting folder {}", file.getName());
                    FileUtils.deleteFolder(file, true);
                } else {
                    logger.debug("deleting file {} ({})", file.getName(),
                            TextUtils.formatSize(file.length()));
                    file.delete();
                }
            }
        }
    }

    protected long computeSize(File fileOrFolder, Set<String> pathsToSkip) {
        if (null != pathsToSkip) {
            for (String pathToSkip : pathsToSkip) {
                if (fileOrFolder.getAbsolutePath()
                        .startsWith(pathToSkip)) {
                    return 0L;
                }
            }
        }
        if (fileOrFolder.isDirectory()) {
            long size = 0L;
            for (File file : fileOrFolder.listFiles()) {
                size += computeSize(file, pathsToSkip);
            }
            return size;
        }
        return fileOrFolder.length();
    }

    public boolean uploadCsvFile(MmRichiesteGestioneConflitti report, File ytAudio, File ytStaging) throws Exception {
        final S3.Url s3Uri = new S3.Url(path);
        logger.info("uploadCsvFile for report Id " + report.getIdRichiesta() + " s3Uri: {}", s3Uri);
        boolean ytAudioUpload = false;
        boolean ytStagingUpload = false;

        if (ytAudio.length() > 0) {
            if (this.upload(s3Uri, ytAudio)) {
                logger.info("uploadCsvFile ytAudio for report Id " + report.getIdRichiesta() + " s3Uri {}", s3Uri);
                System.out.println(s3Uri);
                ytAudioUpload = true;
            }
        }
        if (ytStaging.length() > 0) {
            if (this.upload(s3Uri, ytStaging)) {
                logger.info("uploadCsvFile ytStaging for report Id " + report.getIdRichiesta() + " s3Uri {}", s3Uri);
                System.out.println(s3Uri);
                ytStagingUpload = true;
            }
        }
        if ((ytAudio.length() > 0 && ytAudioUpload == true) && (ytStaging.length() > 0 && ytStagingUpload == true)) {
            logger.info("report Id  " + report.getIdRichiesta() + "COMPLETE");
            report.setStato(StatoGestioneConflittiEnum.COMPLETATA.getName());
            report.setYtAudio(path + ytAudio.getName());
            report.setYtStaging(path + ytStaging.getName());
            commonsService.updateReportStatus(report);
            return true;
        } else {
            report.setStato(StatoGestioneConflittiEnum.ERRORE.getName());
            report.setYtAudio("");
            report.setYtStaging("");
            report.setErrore("Upload failed!");
            commonsService.updateReportStatus(report);
            return false;
        }
    }


    public boolean upload(S3.Url url, final File file) {
        try {
            for (int retries = Integer.parseInt(put_object);
                 retries > 0; retries--) {
                try {
                    logger.info("uploading file {}" + file.getName(), url);
                    final PutObjectRequest putObjectRequest = new PutObjectRequest(url.bucket,
                            url.key.replace("//", "/"), file);
                    putObjectRequest.setGeneralProgressListener(new ProgressListener() {

                        private long accumulator = 0L;
                        private long total = 0L;
                        private long chunk = Math.max(256 * 1024, file.length() / 10L);

                        @Override
                        public void progressChanged(ProgressEvent event) {
                            if (ProgressEventType.REQUEST_BYTE_TRANSFER_EVENT
                                    .equals(event.getEventType())) {
                                accumulator += event.getBytes();
                                total += event.getBytes();
                                if (accumulator >= chunk) {
                                    accumulator -= chunk;
                                    logger.info("transferred {}Kb/{}Kb of file {}",
                                            total / 1024L, file.length() / 1024L, file.getName());
                                }
                            } else if (ProgressEventType.TRANSFER_COMPLETED_EVENT
                                    .equals(event.getEventType())) {
                                logger.info("transfer of file completed", file.getName());
                            }
                        }

                    });
                    AWSCredentials credentials = new BasicAWSCredentials(
                            accessKey,
                            secretKey
                    );
                    AmazonS3 s3client = AmazonS3ClientBuilder
                            .standard()
                            .withCredentials(new AWSStaticCredentialsProvider(credentials))
                            .withRegion(region)
                            .build();
                    if (!s3client.doesBucketExist(url.bucket)) {
                        s3client.createBucket(url.bucket);
                    }
                    s3client.putObject(url.bucket, url.key + file.getName(), file);
                    logger.info("file {} successfully uploaded {}", file.getName(), url);
                    return true;
                } catch (Exception e) {
                    logger.error("upload failed ", e);
                }
            }
        } catch (NumberFormatException | NullPointerException e) {
            logger.error("upload failed ", e);
        }
        logger.info("upload: upload failed ", file.getName());

        return false;
    }

    private void saveYtAudioFile(File file, MmRichiesteGestioneConflitti report, List<YtAudioRow> ytAudioRows) throws Exception {
        try (final FileWriter writer = new FileWriter(file, false)) {
            try (final CSVPrinter printer = new CSVPrinter(new BufferedWriter(writer),
                    CSVFormat.EXCEL.withDelimiter(';').withQuoteMode(QuoteMode.MINIMAL).withHeader(
                            //INSERIRE COLONNE CSV
                            "custom id",
                            "asset_id",
                            "related_isrc",
                            "related_asset_id",
                            "iswc",
                            "title",
                            "add_asset_labels",
                            "hfa_song_code",
                            "writers",
                            "writers_isni_ipi",
                            "match_policy",
                            "publisher_name",
                            "sync_ownership_share",
                            "sync_ownership_territory",
                            "sync_ownership_restriction",
                            "mechanical_ownership_share",
                            "mechanical_ownership_territory",
                            "mechanical_ownership_restriction",
                            "performance_ownership_share",
                            "performance_ownership_territory",
                            "performance_ownership_restriction",
                            "lyric_ownership_share",
                            "lyric_ownership_territory",
                            "lyric_ownership_restriction",
                            "apply_ownership_incrementally"
                    ))) {


                logger.info("saveYtAudioFile of " + "report " + report.getIdRichiesta());
                int rows = 0;

//              print columns
                for (YtAudioRow r : ytAudioRows) {
                    printer.print(r.getCustomId());
                    printer.print(r.getAssetId());
                    printer.print(r.getRelatedIsrc());
                    printer.print(r.getRelatedAssetId());
                    printer.print(r.getIswc());
                    printer.print(r.getTitle());
                    printer.print(r.getAddAssetLabels());
                    printer.print(r.getHfaSongCode());
                    printer.print(r.getWriters());
                    printer.print(r.getWritersIsniIpi());
                    printer.print(r.getMatchPolicy());
                    printer.print(r.getPublisherName());
                    printer.print(r.getSyncOwnershipShare());
                    printer.print(r.getSyncOwnershipTerritory());
                    printer.print(r.getSyncOwnershipRestriction());
                    printer.print(r.getMechanicalOwnershipShare());
                    printer.print(r.getMechanicalOwnershipTerritory());
                    printer.print(r.getMechanicalOwnershipRestriction());
                    printer.print(r.getPerformanceOwnershipShare());
                    printer.print(r.getPerformanceOwnershipTerritory());
                    printer.print(r.getPerformanceOwnershipRestriction());
                    printer.print(r.getLyricOwnershipShare());
                    printer.print(r.getLyricOwnershipTerritory());
                    printer.print(r.getLyricOwnershipRestriction());
                    printer.print(r.getApplyOwnershipIncrementally());
                    printer.println();
                    rows++;
                }
                if (0 == (rows % 1000)) {
                    logger.info("saveYtAudioFile of " + "report " + report.getIdRichiesta() + " : rows {}", rows);
                }
            }

        } catch (Exception e) {
            report.setStato(StatoGestioneConflittiEnum.ERRORE.getName());
            report.setYtAudio("");
            report.setErrore("Creation YtAudio failed!");
            commonsService.updateReportStatus(report);
            logger.error("createYtAudioFile of " + "report Id  " + report.getIdRichiesta() + "", e);
        }
    }

    private void saveYtStagingFile(String path, File file, MmRichiesteGestioneConflitti report, List<YtStagingFile> inputs) throws Exception {
        try (final FileWriter writer = new FileWriter(file, false)) {
            try (final CSVPrinter printer = new CSVPrinter(new BufferedWriter(writer),
                    CSVFormat.EXCEL.withDelimiter(';').withQuoteMode(QuoteMode.MINIMAL).withHeader(
                            //INSERIRE COLONNE CSV
                            "sr_approx_daily_views",
                            "sr_asset_id",
                            "sr_isrc",
                            "sr_title",
                            "sr_artist",
                            "embedded_composition_asset_id",
                            "composition_share_custom_id",
                            "composition_share_asset_id",
                            "composition_share_iswc",
                            "composition_share_hfa_song_code",
                            "composition_share_title",
                            "composition_share_writers",
                            "composition_share_asset_labels",
                            "conflicting_owner",
                            "conflicting_right_type",
                            "conflicting_country_code",
                            "my_ownership_last_updated",
                            "other_ownership_last_updated",
                            "other_ownership_origination",
                            "my_ownership_origination",
                            "views_in_conflict",
                            "conflicting_country_percentage",
                            "uuid",
                            "nuovo valore claim - territorio di conflitto"
                    ))) {

                logger.info("saveYtStagingFile of " + "report " + report.getIdRichiesta());
                int rows = 0;

                for (YtStagingFile r : inputs) {
                    printer.print(r.getSrApproxDailyViews());
                    printer.print(r.getSrAssetId());
                    printer.print(r.getSrIsrc());
                    printer.print(r.getSrTitle());
                    printer.print(r.getSrArtist());
                    printer.print(r.getEmbeddedCompositionAssetId());
                    printer.print(r.getCompositionShareCustomId());
                    printer.print(r.getCompositionShareAssetId());
                    printer.print(r.getCompositionShareIswc());
                    printer.print(r.getCompositionShareHfaSongCode());
                    printer.print(r.getCompositionShareTitle());
                    printer.print(r.getCompositionShareWriters());
                    printer.print(r.getCompositionShareAssetLabels());
                    printer.print(r.getConflictingOwner());
                    printer.print(r.getConflictingRightType());
                    printer.print(r.getConflictingCountryCode());
                    printer.print(r.getMyOwnershipLastUpdated());
                    printer.print(r.getOtherOwnershipLastUpdated());
                    printer.print(r.getOtherOwnershipOrigination());
                    printer.print(r.getMyOwnershipOrigination());
                    printer.print(r.getViewsInConflict());
                    printer.print(r.getConflictingCountryPercentage());
                    printer.print(r.getUuid());
                    printer.print(r.getNuovoValoreClaim()); //Nuovo Claim con Territorio di conflitto

                    printer.println();
                    rows++;
                }
                if (0 == (rows % 1000)) {
                    logger.info("saveYtStagingFile of " + "report " + report.getIdRichiesta() + " : rows {}", rows);
                }
            }

        } catch (Exception e) {
            report.setStato(StatoGestioneConflittiEnum.ERRORE.getName());
            report.setYtStaging("");
            report.setErrore("Creation YtStaging failed!");
            commonsService.updateReportStatus(report);
            logger.error("createYtStagingFile of " + "report Id  " + report.getIdRichiesta() + "", e);
        }
    }

    public List<CsvInput> downloadToS3(MmRichiesteGestioneConflitti report) throws FileNotFoundException {
        final S3.Url url = new S3.Url(fileInputLocation);
        List<CsvInput> conflitti = new ArrayList<>();
        AWSCredentials credentials = new BasicAWSCredentials(
                accessKey,
                secretKey
        );
        AmazonS3 s3client = AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(region)
                .build();

        InputStream in = s3client.getObject(url.bucket, url.key + report.getNomeFile()).getObjectContent();
        Reader reader = new BufferedReader(new InputStreamReader(in));

        CsvToBean<CsvInput> csvReader = new CsvToBeanBuilder(reader)
                .withType(CsvInput.class)
                .withSeparator(';')
                .withIgnoreLeadingWhiteSpace(true)
                .withIgnoreEmptyLine(true)
                .build();

        conflitti = csvReader.parse();

        return conflitti;
    }

    private List<YtAudioRow> toAudioFile(CsvInput i, MmRichiesteGestioneConflitti report, String uuid, HashMap<String, String> claimMap, HashMap<String, String> exists) throws IOException, InterruptedException {
        List<YtAudioRow> ytlist = new ArrayList<>();
        HashMap<String, ClaimResult> claimResultHashMap = new HashMap<>();
        ClaimResult claimResult = null;
        //Split per territorio
        String[] countriesCodes = null;
        String[] countriesPerc = null;


        //GESTISCO IL CASO IN CUI C'è UN SOLO CASO DI CONFLITTO
        if (StringUtils.isNotEmpty(i.getConflicting_country_code())) {
            if (i.getConflicting_country_code().contains("|")) {
                countriesCodes = i.getConflicting_country_code().replace("|", "-").split("-");
            } else {
                ArrayList<String> s = new ArrayList<>();
                s.add(i.getConflicting_country_code());
                countriesCodes = s.toArray(new String[0]);
            }
        }
        if (StringUtils.isNotEmpty(i.getConflicting_country_percentage())) {
            if (i.getConflicting_country_percentage().contains("|")) {
                countriesPerc = i.getConflicting_country_percentage().split("\\|");
            } else {
                ArrayList<String> s = new ArrayList<>();
                s.add(i.getConflicting_country_percentage());
                countriesPerc = s.toArray(new String[0]);
            }
        }
        //

        if (report.getTerritorio().equals("R")) {
            //CASO IN CUI LA RICHIESTA PREVEDE IL CLAIM PER TUTTE LE NAZIONI MENO L'ITALIA

            for (String c : countriesCodes) {
                if (c.equals("IT")) {
                    //Rimuovo eventualmente IT dai territori di conflitto
                    List<String> lscod = new ArrayList<>(Arrays.asList(countriesCodes));
                    lscod.remove(c);
                    countriesCodes = lscod.toArray(new String[0]);
                    for (String s : countriesPerc) {
                        if (s.contains(c)) {
                            //Rimuovo eventualmente IT dalle percentuali
                            List<String> lsperc = new ArrayList<>(Arrays.asList(countriesPerc));
                            lsperc.remove(s);
                            countriesPerc = lsperc.toArray(new String[0]);
                        }
                    }
                }
            }
        } else if (report.getTerritorio().equals("IT")) {
            //CASO IN CUI LA RICHIESTA PREVEDE IL CLAIM PER ITALIA
            for (String c : countriesCodes) {
                if (!c.equals("IT")) {
                    //Rimuovo eventualmente tutte le nazioni diverse da Italia dai territori di conflitto
                    List<String> lscod = new ArrayList<>(Arrays.asList(countriesCodes));
                    lscod.remove(c);
                    countriesCodes = lscod.toArray(new String[0]);
                }
            }
            for (String s : countriesPerc) {
                if (!s.contains("IT")) {
                    //Rimuovo eventualmente tutte le nazioni diverse da Italia dalle percentuali
                    List<String> lsperc = new ArrayList<>(Arrays.asList(countriesPerc));
                    lsperc.remove(s);
                    countriesPerc = lsperc.toArray(new String[0]);
                }
            }

        }

        String codiceOpera = uuid != null ? uuid : i.getComposition_share_custom_id();
        String claimPerf = "0.00";
        String claimMech = "0.00";

        boolean todoClaim = false;

        for (String territorio : countriesCodes) {
            String claimKey = codiceOpera + "@" + (territorio.equalsIgnoreCase("IT") ? territorio : "FR");
            logger.debug("CLAIM KEY " + claimKey);
            claimResult = claimResultHashMap.get(claimKey);
            logger.debug("claim from map " + claimResult);

            if (!codiceOpera.startsWith("S") && claimResult == null) {
                logger.debug("claimResult == null");
                claimResult = doClaim(codiceOpera, (territorio.equalsIgnoreCase("IT") ? territorio : "FR"), DSP, report.getSchemiRiparto());
                if (claimResult != null) {
                    logger.debug("CLAIM VALORIZZATO");
                }
                claimPerf = claimResult != null ? String.valueOf(claimResult.getClaimLicensorDem().setScale(2, RoundingMode.HALF_UP)) : "0.00";
                claimMech = claimResult != null ? String.valueOf(claimResult.getClaimLicensorDrm().setScale(2, RoundingMode.HALF_UP)) : "0.00";
                //                    todoClaim = claimResult != null ? true : false;
                claimResultHashMap.put(claimKey, claimResult);
            } else {
                claimPerf = claimResult != null ? String.valueOf(claimResult.getClaimLicensorDem().setScale(2, RoundingMode.HALF_UP)) : "0.00";
                claimMech = claimResult != null ? String.valueOf(claimResult.getClaimLicensorDrm().setScale(2, RoundingMode.HALF_UP)) : "0.00";
            }

            if (claimResult != null)
                logger.info("CLAIM RESULT " + claimResult.toString());

            logger.debug("CLAIM PERF " + claimPerf);
            logger.debug("CLAIM MECH " + claimMech);

            logger.debug("CHECK IF EXISTS " + i.getComposition_share_custom_id() + "@" + i.getComposition_share_asset_id() + "@" + territorio + "@" + codiceOpera + " :" + exists.get(i.getComposition_share_custom_id() + "@" + i.getComposition_share_asset_id() + "@" + i.getConflicting_country_code() + "@" + codiceOpera ));
            if (exists.get(i.getComposition_share_custom_id() + "@" + i.getComposition_share_asset_id() + "@" + territorio + "@" + codiceOpera ) == null) {

                YtAudioRow y = new YtAudioRow();
                y.setCustomId(i.getComposition_share_custom_id());
                y.setAssetId(i.getComposition_share_asset_id());
                y.setRelatedIsrc(""); //*
                y.setRelatedAssetId(""); //*
                y.setIswc(i.getComposition_share_iswc());
                y.setTitle(i.getComposition_share_title());
                y.setAddAssetLabels(i.getComposition_share_asset_labels());
                y.setHfaSongCode(i.getComposition_share_hfaSong_code());
                y.setWriters(i.getComposition_share_writers());
                y.setWritersIsniIpi("");//*
                y.setMatchPolicy("");//*
                y.setPublisherName("SIAE");
                y.setSyncOwnershipShare("");//*
                y.setSyncOwnershipTerritory("");//*
                y.setSyncOwnershipRestriction("");//*
                y.setMechanicalOwnershipShare(claimResult != null ? String.valueOf(claimResult.getClaimLicensorDrm().setScale(2, RoundingMode.HALF_UP)) : "0.00");//Claim DRM
                claimMech = y.getMechanicalOwnershipShare();
                y.setMechanicalOwnershipRestriction("include"); //include
                y.setMechanicalOwnershipTerritory(territorio);
                y.setPerformanceOwnershipShare(claimResult != null ? String.valueOf(claimResult.getClaimLicensorDem().setScale(2, RoundingMode.HALF_UP)) : "0.00"); //Claim DEM
                claimPerf = y.getPerformanceOwnershipShare();
                y.setPerformanceOwnershipRestriction("include");//include
                y.setPerformanceOwnershipTerritory(territorio);
                y.setLyricOwnershipShare("");//*
                y.setLyricOwnershipTerritory("");//*
                y.setLyricOwnershipRestriction("");//*
                y.setApplyOwnershipIncrementally("");//*
                exists.put(i.getComposition_share_custom_id() + "@" + i.getComposition_share_asset_id() + "@" + territorio + "@" + codiceOpera , "true");
                logger.debug("PUT IN EXISTS " + i.getComposition_share_custom_id() + "@" + i.getComposition_share_asset_id() + "@" + territorio + "@" + codiceOpera );
//                if (i.getConflicting_right_type().equalsIgnoreCase("Performance"))
                ytlist.add(y);
            }
            //* FACOLTATIVO : il giorno 17/03 abbiamo deciso in call con Athan di lasciare vuoto

            //mi salvo in HashMap i valori del claim da inserire nel file di staging
            String newClaim = new String();
            String prev = "";
            for (String perc : countriesPerc) {
                logger.debug("COUNTRY CODE " + perc);
                if (!prev.equalsIgnoreCase(perc)) {
                    //Nuovo Claim con Territorio di conflitto
                    if (StringUtils.isNotEmpty(newClaim)) {
                        newClaim += "|";
                    }
                    newClaim += perc.substring(0, 2)
                            + "=OLD[" + (perc.substring((perc.lastIndexOf("SIAE_CS:"))).contains(" ") ? perc.substring((perc.lastIndexOf("SIAE_CS:"))).substring(8, (perc.substring((perc.lastIndexOf("SIAE_CS:")))).indexOf(" ")) : perc.substring((perc.lastIndexOf("SIAE_CS:"))).substring(8, (perc.substring((perc.lastIndexOf("SIAE_CS:")))).indexOf("}")))
                            + "]:NEW[" + (i.getConflicting_right_type().equalsIgnoreCase("Performance") ? claimPerf : claimMech) + "]";
                }
                prev = perc;
            }
            logger.debug("Riga " + i.toString());
            logger.debug("NEWCLAIM " + newClaim);
            claimMap.put(i.getConflicting_country_code() + "@" + i.getComposition_share_asset_id() + "@" + i.getConflicting_right_type(), newClaim);
        }
        return ytlist;
    }

    public static String ricercaOpereMassive(RicercaOpereKBDTO opereKBDTO, String searchUrl) throws IOException {
        String uuid = null;
        try {
            logger.info(String.format("RicercaOpereMassive: Calling to %s", searchUrl));
            logger.info("Json opereKBDTO " + GsonUtils.toJson(opereKBDTO));
            final RestClient.Result response = RestClient.post(searchUrl, GsonUtils.toJson(opereKBDTO));
            logger.info("Response: " + response.toString());
            if (response.getStatusCode() == 200 && response.getJsonElement().getAsJsonArray().size() != 0) {
                final JsonObject rb = response.getJsonElement().getAsJsonArray().get(0).getAsJsonObject();

                logger.debug("RISPOSTA " + rb.toString());
                if (rb.has("codiceOpera")) {
                    uuid = rb.get("codiceOpera").getAsString();
                }
            }
        } catch (Exception e) {
            logger.error("RicercaOpereMassive: ", e);
        } finally {
            return uuid;
        }
    }

    protected void downloadAndExtractLatest(String propertyPrefix, String propertyPrefixArchive, Configuration
            configuration) throws IOException, InterruptedException {
        // data folder
        final File dataFolder = new File(configuration
                .getProperty(propertyPrefix + ".data_folder"));
        dataFolder.mkdirs();

        logger.debug("DATA FOLDER " + configuration
                .getProperty(propertyPrefix + ".data_folder"));

        // .latest file
        final File latestFile = new File(configuration
                .getProperty(propertyPrefixArchive + ".latest_file"));
        logger.debug("downloadAndExtractLatest: latestFile {}", latestFile);

        // load local .latest version
        final LatestVersion localLatestVersion = new LatestVersion()
                .load(latestFile);
        logger.debug("downloadAndExtractLatest: localLatestVersion {}", localLatestVersion);

        // .latest url
        final String latestUrl = configuration
                .getProperty(propertyPrefixArchive + ".latest_url");
        logger.debug("downloadAndExtractLatest: latestUrl {}", latestUrl);

        // temporary latest file
        final File temporaryLatestFile = File
                .createTempFile("__tmp__", "__.latest", dataFolder);
        temporaryLatestFile.deleteOnExit();
        logger.debug("downloadAndExtractLatest: temporaryLatestFile {}", temporaryLatestFile);

        logger.info("access " + properties.getProperty("aws.credentials"));

        S3 s3 = new S3(properties);
        s3.startup();
        // download .latest version
        if (!s3.download(new S3.Url(latestUrl), temporaryLatestFile)) {
            temporaryLatestFile.delete();
            throw new IOException("file download error: " + latestUrl);
        }

        // load downloaded .latest version
        final LatestVersion latestVersion = new LatestVersion()
                .load(temporaryLatestFile);
        logger.debug("downloadAndExtractLatest: latestVersion {}", latestVersion);

        // check version
        if (localLatestVersion.getVersion("")
                .equals(latestVersion.getVersion())) {
            logger.debug("downloadAndExtractLatest: version unchanged");
            return;
        }

        // archive url
        final String archiveUrl = latestVersion.getLocation();

        // archive file
        final File archiveFile = File.createTempFile("__tmp__",
                "__" + archiveUrl.substring(1 + archiveUrl.lastIndexOf('/')), dataFolder);
        archiveFile.deleteOnExit();
        logger.debug("downloadAndExtractLatest: archiveFile {}", archiveFile);

        // download archive
        if (!s3.download(new S3.Url(archiveUrl), archiveFile)) {
            throw new IOException("file download error: " + archiveUrl);
        }

        // destination home folder
        final File destinationFolder = new File(configuration
                .getProperty(propertyPrefixArchive + ".home_folder"));

        // delete existing destination folder
        FileUtils.deleteFolder(destinationFolder, true);
        destinationFolder.mkdirs();

        // untar archive file
        final String[] cmdline = new String[]{
                configuration.getProperty(propertyPrefix + ".tar_path",
                        configuration.getProperty("default.tar_path", "/usr/bin/tar")),
                "-C", destinationFolder.getAbsolutePath(),
                "-xzf", archiveFile.getAbsolutePath()
        };
        logger.debug("downloadAndExtractLatest: exec cmdline {}", Arrays.asList(cmdline));
        final int exitCode = Runtime
                .getRuntime().exec(cmdline).waitFor();
        logger.debug("downloadAndExtractLatest: exec exitCode {}", exitCode);

        // delete archive file
        archiveFile.delete();

        // overwrite local latest file
        if (FileUtils.copy(temporaryLatestFile, latestFile) < 0L) {
            throw new IOException("unable to update local latest file: " + latestFile);
        }

        // delete temporary latest file
        temporaryLatestFile.delete();
    }
}

