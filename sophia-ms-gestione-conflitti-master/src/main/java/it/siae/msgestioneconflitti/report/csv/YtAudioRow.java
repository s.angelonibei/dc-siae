package it.siae.msgestioneconflitti.report.csv;

import lombok.Data;
import net.sf.jsefa.csv.annotation.CsvDataType;
import net.sf.jsefa.csv.annotation.CsvField;

import java.io.Serializable;

@CsvDataType()
@Data
public class YtAudioRow implements Serializable {

    @CsvField(pos = 1)
    String customId;

    @CsvField(pos = 2)
    String assetId;

    @CsvField(pos = 3)
    String relatedIsrc;

    @CsvField(pos = 4)
    String relatedAssetId;

    @CsvField(pos = 5)
    String iswc;

    @CsvField(pos = 6)
    String title;

    @CsvField(pos = 7)
    String addAssetLabels;

    @CsvField(pos = 8)
    String hfaSongCode;

    @CsvField(pos = 9)
    String writers;

    @CsvField(pos = 10)
    String writersIsniIpi;

    @CsvField(pos = 11)
    String matchPolicy;

    @CsvField(pos = 12)
    String publisherName;

    @CsvField(pos = 13)
    String syncOwnershipShare;

    @CsvField(pos = 14)
    String syncOwnershipTerritory;

    @CsvField(pos = 15)
    String syncOwnershipRestriction;

    @CsvField(pos = 16)
    String mechanicalOwnershipShare;

    @CsvField(pos = 17)
    String mechanicalOwnershipTerritory;

    @CsvField(pos = 18)
    String mechanicalOwnershipRestriction;

    @CsvField(pos = 19)
    String performanceOwnershipShare;

    @CsvField(pos = 20)
    String performanceOwnershipTerritory;

    @CsvField(pos = 21)
    String performanceOwnershipRestriction;

    @CsvField(pos = 22)
    String lyricOwnershipShare;

    @CsvField(pos = 23)
    String lyricOwnershipTerritory;

    @CsvField(pos = 24)
    String lyricOwnershipRestriction;

    @CsvField(pos = 25)
    String applyOwnershipIncrementally;

}
