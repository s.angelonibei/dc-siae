package it.siae.msgestioneconflitti.report.csv;

import lombok.Data;
import net.sf.jsefa.csv.annotation.CsvDataType;
import net.sf.jsefa.csv.annotation.CsvField;

import java.io.Serializable;

@CsvDataType()
@Data
public class  YtStagingFile implements Serializable {
    @CsvField(pos = 1)
    String srApproxDailyViews;
    @CsvField(pos = 2)
    String srAssetId;
    @CsvField(pos = 3)
    String srIsrc;
    @CsvField(pos = 4)
    String srTitle;
    @CsvField(pos = 5)
    String srArtist;
    @CsvField(pos = 6)
    String embeddedCompositionAssetId;
    @CsvField(pos = 7)
    String compositionShareCustomId;
    @CsvField(pos = 8)
    String compositionShareAssetId;
    @CsvField(pos = 9)
    String compositionShareIswc;
    @CsvField(pos = 10)
    String compositionShareHfaSongCode;
    @CsvField(pos = 11)
    String compositionShareTitle;
    @CsvField(pos = 12)
    String compositionShareWriters;
    @CsvField(pos = 13)
    String compositionShareAssetLabels;
    @CsvField(pos = 14)
    String conflictingOwner;
    @CsvField(pos = 15)
    String conflictingRightType;
    @CsvField(pos = 16)
    String conflictingCountryCode;
    @CsvField(pos = 17)
    String myOwnershipLastUpdated;
    @CsvField(pos = 18)
    String otherOwnershipLastUpdated;
    @CsvField(pos = 19)
    String otherOwnershipOrigination;
    @CsvField(pos = 20)
    String myOwnershipOrigination;
    @CsvField(pos = 21)
    String viewsInConflict;
    @CsvField(pos = 22)
    String conflictingCountryPercentage;
    @CsvField(pos = 23)
    String uuid;
    @CsvField(pos = 24)
    String nuovoValoreClaim;
    @CsvField(pos = 25)
    String territorioDiConflitto;

}
