package it.siae.msgestioneconflitti.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class SchemiRipartoDTO implements Serializable {
    String year;
    String month;
}
