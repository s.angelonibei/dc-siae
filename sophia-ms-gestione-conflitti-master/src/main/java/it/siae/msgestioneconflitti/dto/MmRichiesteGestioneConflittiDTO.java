package it.siae.msgestioneconflitti.dto;

import lombok.Data;

import java.io.File;
import java.io.Serializable;
import java.util.Date;

@Data
public class MmRichiesteGestioneConflittiDTO implements Serializable {
    private Long idRichiesta;
    private String file;
    private String schemiRiparto;
    private String territorio;
    private String stato;//“caricato” (il file è stato caricato correttamente), “errore” (si è verificato un errore in fase di processamento del file), “in corso” (il file è in fase di processamento da parte del sistema), “completato” (sono stati creati dal sistema i tre file di output).
    private Date dataAcquisizione;
    private Date ultimoAggiornamento;
    private String creatoDa;
    private String errore;
    private String ytAudio;//path s3
    private String ytStaging;//path s3
}
