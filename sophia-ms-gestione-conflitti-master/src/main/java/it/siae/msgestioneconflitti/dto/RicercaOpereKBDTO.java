package it.siae.msgestioneconflitti.dto;


import it.siae.msgestioneconflitti.utils.SearchOpereField;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class RicercaOpereKBDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @SearchOpereField("title")
    private String title;
    @SearchOpereField("artist_composer")
    private String artists;
    @SearchOpereField("iswc")
    private String iswc;
    @SearchOpereField("uuid")
    private String uuid;
    @SearchOpereField("codice")
    private String codice;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtists() {
        return artists;
    }

    public void setArtists(String artists) {
        this.artists = artists;
    }

    public String getIswc() {
        return iswc;
    }

    public void setIswc(String iswc) {
        this.iswc = iswc;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCodice() {
        return codice;
    }

    public void setCodice(String codice) {
        this.codice = codice;
    }

}