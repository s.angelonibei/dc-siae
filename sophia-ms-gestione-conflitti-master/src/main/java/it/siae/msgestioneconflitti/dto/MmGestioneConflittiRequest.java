package it.siae.msgestioneconflitti.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.Date;

@Data
public class MmGestioneConflittiRequest implements Serializable {
    private MultipartFile file;
    private String anno;
    private String mese;
    private String territorio;
    private String stato;//“caricato” (il file è stato caricato correttamente), “errore” (si è verificato un errore in fase di processamento del file), “in corso” (il file è in fase di processamento da parte del sistema), “completato” (sono stati creati dal sistema i tre file di output).
    private Date dataAcquisizione;
    private Date ultimoAggiornamento;
    private String creatoDa;
}
