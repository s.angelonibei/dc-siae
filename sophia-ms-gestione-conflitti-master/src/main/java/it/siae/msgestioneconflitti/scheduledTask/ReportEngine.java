/*
 * @Autor A.D.C.
 */

package it.siae.msgestioneconflitti.scheduledTask;

import it.siae.msgestioneconflitti.entity.MmRichiesteGestioneConflitti;
import it.siae.msgestioneconflitti.entity.enumeration.StatoGestioneConflittiEnum;
import it.siae.msgestioneconflitti.report.csv.WriteCsv;
import it.siae.msgestioneconflitti.repository.MmRichiesteGestioneConflittiRepository;
import it.siae.msgestioneconflitti.service.CommonsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.RejectedExecutionException;

@Component
public class ReportEngine {

    private static final Logger logger = LoggerFactory.getLogger(ReportEngine.class);
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
    @Autowired
    MmRichiesteGestioneConflittiRepository gCRepository;

    @Autowired
    private CommonsService commonsService;

    @Autowired
    private Environment env;

    @Autowired
    private WriteCsv writeCsv;

    @Autowired
    @Qualifier("CustomTaskExecutor")
    private TaskExecutor taskExecutor;


    // add scheduled methods here
    @Scheduled(fixedRate = 30000)
    public void scheduleTaskWithCronExpression() {

        logger.info("Gestione Conflitti Service is running");
        logger.info("Cron Task: Current Time - {}", formatter.format(LocalDateTime.now()));
        List<MmRichiesteGestioneConflitti> reports = gCRepository.findByStatoIgnoreCaseOrderByDataAcquisizioneAsc(StatoGestioneConflittiEnum.DA_ELABORARE.getName());
        List<MmRichiesteGestioneConflitti> reportsrunning = gCRepository.findByStatoIgnoreCaseOrderByDataAcquisizioneAsc(StatoGestioneConflittiEnum.IN_CORSO.getName());
        logger.info("File conflitti da Elaborare: " + reports.size());
        logger.info("File conflitti in corso: " + reportsrunning.size());
        for (MmRichiesteGestioneConflitti report : reports) {
            GestioneConflittiJob job = new GestioneConflittiJob(report, commonsService, env, writeCsv);
            try {
                taskExecutor.execute(job);
            } catch (RejectedExecutionException r) {
                logger.info("Impossibile prendere in carico il file: tutti gli slot disponibili sono occupati");
            }
        }
        logger.info("Conflict Manager Engine is running");
    }

}