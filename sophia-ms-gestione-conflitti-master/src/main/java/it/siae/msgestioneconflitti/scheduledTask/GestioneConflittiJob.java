/*
 * @Autor A.D.C.
 */

package it.siae.msgestioneconflitti.scheduledTask;

import it.siae.msgestioneconflitti.entity.MmRichiesteGestioneConflitti;
import it.siae.msgestioneconflitti.entity.enumeration.StatoGestioneConflittiEnum;
import it.siae.msgestioneconflitti.report.csv.CsvInput;
import it.siae.msgestioneconflitti.report.csv.WriteCsv;
import it.siae.msgestioneconflitti.report.csv.YtAudioRow;
import it.siae.msgestioneconflitti.service.CommonsService;
import lombok.Data;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.List;


@Data
public class GestioneConflittiJob implements Runnable {

    private final Logger logger = LoggerFactory.getLogger(GestioneConflittiJob.class);

    private MmRichiesteGestioneConflitti report;

    private CommonsService commonsService;

    private WriteCsv writeCsv;

    private Environment env;

    @Value("${spring.s3uri.path}")
    public String path;


    public GestioneConflittiJob(MmRichiesteGestioneConflitti report, CommonsService commonsService, Environment env, WriteCsv writeCsv) {
        this.report = report;
        this.commonsService = commonsService;
        this.env = env;
        this.writeCsv = writeCsv;
    }

    @Override
    public void run() {

        try {
            logger.info("STARTING GestioneConflittiService for report {}", report.getIdRichiesta());

            report.setStato(StatoGestioneConflittiEnum.IN_CORSO.getName());
            report.setYtAudio(""); //percorso finale del file dem
            report.setYtStaging(""); //percorso finale del file drm
            report.setErrore("");
            commonsService.updateReportStatus(report);

            logger.info("Load data for report {}", +report.getIdRichiesta());

            String ytAduioType = new String("ytAudio");
            String ytStagingType = new String("ytStaging");

            // temporary output file
            final File fileYtAudio = new File("yt_audio_composizione_" + report.getIdRichiesta() + "_"
                    + new SimpleDateFormat("ddMMyyyyHHmmss").format(report.getDataAcquisizione()) + ".csv");
            logger.info("process report Id  " + report.getIdRichiesta() + " : created temporary file {}", fileYtAudio);

            final File fileYtStaging = new File("yt_staging_file_" + report.getIdRichiesta() + "_"
                    + new SimpleDateFormat("ddMMyyyyHHmmss").format(report.getDataAcquisizione()) + ".csv");
            logger.info("process report Id  " + report.getIdRichiesta() + " : created temporary file {}", fileYtStaging);

            logger.info("Recupero il file di input da s3");
            List<CsvInput> csvConflitti = writeCsv.downloadToS3(report);
            logger.info("Creating YtAudio file ...");

            // create csv file ytAudio and file ytStaging
            logger.info("process report Id Id  " + report.getIdRichiesta() + " : creating CSV file for ytAudio type");
            boolean created = false;
            boolean response = false;
            created = writeCsv.createYtFiles(csvConflitti, fileYtAudio, fileYtStaging, report);
            if (created) {

                // upload file to S3
                logger.info("process report Id " + report.getIdRichiesta() + " : YtAudio file size {}", fileYtAudio.length());
                logger.info("process report Id " + report.getIdRichiesta() + " : YtStaging file size {}", fileYtStaging.length());
                logger.info("process report Id Id  " + report.getIdRichiesta() + " : uploading to S3");
                writeCsv.uploadCsvFile(report, fileYtAudio,fileYtStaging);

            }

            // delete temporary output file
            fileYtAudio.delete();
            logger.info("process report Id Id  " + report.getIdRichiesta() + " : deleted temporary file {}", fileYtAudio);
            fileYtStaging.delete();
            logger.info("process report Id Id  " + report.getIdRichiesta() + " : deleted temporary file {}", fileYtStaging);
            logger.info("Update report {} status", report.getIdRichiesta());

        } catch (Exception e) {
            report.setStato(StatoGestioneConflittiEnum.ERRORE.getName());
            report.setErrore(ExceptionUtils.getStackTrace(e));
            report.setYtAudio("");
            report.setYtStaging("");
            logger.error("Error occurred!", e);
            commonsService.updateReportStatus(report);
        }

    }

}

