/*
 * @Autor A.D.C.
 */

/*
 * @Autor A.D.C.
 */

package it.siae.msgestioneconflitti.repository;

import it.siae.msgestioneconflitti.entity.AnagDsp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnagDspRepository extends JpaRepository<AnagDsp, String> {
        AnagDsp findByIdDsp(String iddsp);
}