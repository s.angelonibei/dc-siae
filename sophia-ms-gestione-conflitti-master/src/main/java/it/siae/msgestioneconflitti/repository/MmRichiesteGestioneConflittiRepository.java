/*
 * @Autor A.D.C.
 */

package it.siae.msgestioneconflitti.repository;

import it.siae.msgestioneconflitti.entity.MmRichiesteGestioneConflitti;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MmRichiesteGestioneConflittiRepository extends JpaRepository<MmRichiesteGestioneConflitti, Long> {
    List<MmRichiesteGestioneConflitti> findByIdRichiestaAndNomeFileAndStatoAndSchemiRipartoAndTerritorio(Long idRichiesta, String nomeFile,String stato,String schemiRiparto,String Territorio);

    List<MmRichiesteGestioneConflitti> findByStatoIgnoreCaseOrderByDataAcquisizioneAsc(String daElaborare);

    List<MmRichiesteGestioneConflitti> findByIdRichiestaAndNomeFileAndStatoAndSchemiRipartoAndTerritorioNot(Long idRichiesta, String nomeFile,String stato,String schemiRiparto,String Territorio);
}