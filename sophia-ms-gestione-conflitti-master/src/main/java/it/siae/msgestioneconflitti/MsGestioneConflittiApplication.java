/*
 * @Autor A.D.C.
 */

package it.siae.msgestioneconflitti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan
@SpringBootApplication
@EnableAutoConfiguration
@EnableEurekaClient
public class MsGestioneConflittiApplication {

    public static void main(String[] args)  {

        SpringApplication.run(MsGestioneConflittiApplication.class, args);
    }

}
