/*
 * @Autor A.D.C.
 */

package it.siae.msgestioneconflitti.rest;

import com.google.inject.Provider;
import it.siae.msgestioneconflitti.dto.MmGestioneConflittiRequest;
import it.siae.msgestioneconflitti.dto.MmRichiesteGestioneConflittiDTO;
import it.siae.msgestioneconflitti.entity.MmRichiesteGestioneConflitti;
import it.siae.msgestioneconflitti.repository.MmRichiesteGestioneConflittiRepository;
import it.siae.msgestioneconflitti.service.GestioneConflittiService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("gestioneConflitti")
public class GestioneConflittiResource {

    private final Logger logger = LoggerFactory.getLogger(GestioneConflittiResource.class);

    @Autowired
    GestioneConflittiService service;

    @Autowired
    MmRichiesteGestioneConflittiRepository gestioneConflittiRepo;


    @PostMapping("/addRequest")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Description("Api for create report csv request in MMRichiesteLookThrough")
    public List<MmRichiesteGestioneConflitti> createReportRequest(MmGestioneConflittiRequest dto) throws Exception {
        logger.info("REST request to create new request into MmRichiesteGestioneConflitti: {} ", dto);
        return this.service.createReportRequest(dto);
    }

    @GetMapping("/getRequestList")
    @Description("Api for get all request from MMRichiesteLookThrough orderBy DataCreation Desc")
    public Object getAllRequest(@DefaultValue("0") @QueryParam("first") int first,
                                @DefaultValue("50") @QueryParam("last") int last,
                                @QueryParam("name") String name,
                                @QueryParam("stato") String stato,
                                @QueryParam("territorio") String territorio,
                                @QueryParam("schemiRiparto") String schemiRiparto) {
        logger.info("REST request to find all request into MmRichiesteLookThrough: { first: " + first + ",last: " + last + ",name: " + name + ",stato: " + stato + ",territorio: " + territorio + ",schemi riparto: " + schemiRiparto + "}");
        final PagedResult pagedResult = new PagedResult();
        List<MmRichiesteGestioneConflittiDTO> resultList = new ArrayList<>();
        try {
            if (name == null && stato == null && territorio == null && schemiRiparto == null) {
                resultList = this.service.toDtoList(this.gestioneConflittiRepo.findAll()
                        .stream()
                        .sorted(Comparator.comparing(MmRichiesteGestioneConflitti::getDataAcquisizione)
                                .reversed())
                        .skip(first) //offset
                        .limit(last - first)
                        .collect(Collectors.toList()));
            } else {
                if (StringUtils.isNotEmpty(name) && StringUtils.isNotEmpty(territorio) && StringUtils.isNotEmpty(stato) && StringUtils.isNotEmpty(schemiRiparto)) {
                    resultList = this.service.toDtoList(this.gestioneConflittiRepo.findAll()
                            .stream()
                            .filter(r -> (r.getNomeFile() != null && r.getNomeFile().toUpperCase().matches(".*" + name.toUpperCase() + ".*"))
                                    && (r.getTerritorio() != null && r.getTerritorio().equalsIgnoreCase(territorio))
                                    && (r.getStato() != null && r.getStato().toUpperCase().matches(".*" + stato.toUpperCase() + ".*")))
                            .sorted(Comparator.comparing(MmRichiesteGestioneConflitti::getDataAcquisizione)
                                    .reversed())
                            .skip(first) //offset
                            .limit(last - first)
                            .collect(Collectors.toList()));
                } else if (StringUtils.isNotEmpty(name) && StringUtils.isNotEmpty(schemiRiparto)) {
                    resultList = this.service.toDtoList(this.gestioneConflittiRepo.findAll()
                            .stream()
                            .filter(r -> (r.getNomeFile() != null && r.getNomeFile().toUpperCase().matches(".*" + name.toUpperCase() + ".*"))
                                    && (r.getSchemiRiparto() != null && r.getSchemiRiparto().toUpperCase().matches(".*"+schemiRiparto+".*")))
                            .sorted(Comparator.comparing(MmRichiesteGestioneConflitti::getDataAcquisizione)
                                    .reversed())
                            .skip(first) //offset
                            .limit(last - first)
                            .collect(Collectors.toList()));
                } else if (StringUtils.isNotEmpty(name) && StringUtils.isNotEmpty(stato)) {
                    resultList = this.service.toDtoList(this.gestioneConflittiRepo.findAll()
                            .stream()
                            .filter(r -> (r.getNomeFile() != null && r.getNomeFile().toUpperCase().matches(".*" + name.toUpperCase() + ".*"))
                                    && (r.getStato() != null && r.getStato().toUpperCase().matches(".*" + stato.toUpperCase() + ".*")))
                            .sorted(Comparator.comparing(MmRichiesteGestioneConflitti::getDataAcquisizione)
                                    .reversed())
                            .skip(first) //offset
                            .limit(last - first)
                            .collect(Collectors.toList()));
                } else if (StringUtils.isNotEmpty(schemiRiparto) && StringUtils.isNotEmpty(stato)) {
                    resultList = this.service.toDtoList(this.gestioneConflittiRepo.findAll()
                            .stream()
                            .filter(r -> (r.getSchemiRiparto() != null && r.getSchemiRiparto().toUpperCase().matches(".*"+schemiRiparto+".*"))
                                    && (r.getStato() != null && r.getStato().toUpperCase().matches(".*" + stato.toUpperCase() + ".*")))
                            .sorted(Comparator.comparing(MmRichiesteGestioneConflitti::getDataAcquisizione)
                                    .reversed())
                            .skip(first) //offset
                            .limit(last - first)
                            .collect(Collectors.toList()));
                } else if (StringUtils.isNotEmpty(name)) {
                    resultList = this.service.toDtoList(this.gestioneConflittiRepo.findAll()
                            .stream()
                            .filter(r -> r.getNomeFile() != null && r.getNomeFile().toUpperCase().matches(".*" + name.toUpperCase() + ".*"))
                            .sorted(Comparator.comparing(MmRichiesteGestioneConflitti::getDataAcquisizione)
                                    .reversed())
                            .skip(first) //offset
                            .limit(last - first)
                            .collect(Collectors.toList()));

                } else if (StringUtils.isNotEmpty(schemiRiparto)) {
                    resultList = this.service.toDtoList(this.gestioneConflittiRepo.findAll()
                            .stream()
                            .filter(r -> r.getSchemiRiparto() != null && r.getSchemiRiparto().toUpperCase().matches(".*"+schemiRiparto+".*"))
                            .sorted(Comparator.comparing(MmRichiesteGestioneConflitti::getDataAcquisizione)
                                    .reversed())
                            .skip(first) //offset
                            .limit(last - first)
                            .collect(Collectors.toList()));

                } else if (StringUtils.isNotEmpty(stato)) {
                    resultList = this.service.toDtoList(this.gestioneConflittiRepo.findAll()
                            .stream()
                            .filter(r -> r.getStato() != null && r.getStato().toUpperCase().matches(".*" + stato.toUpperCase() + ".*"))
                            .sorted(Comparator.comparing(MmRichiesteGestioneConflitti::getDataAcquisizione)
                                    .reversed())
                            .skip(first) //offset
                            .limit(last - first)
                            .collect(Collectors.toList()));

                }
            }

            if (null != resultList) {
                final int maxrows = last - first;
                final boolean hasNext = resultList.size() >= maxrows;
                pagedResult.setRows(resultList)
                        .setMaxrows(maxrows)
                        .setFirst(first)
                        .setLast(hasNext ? last : first + resultList.size())
                        .setHasNext(hasNext)
                        .setHasPrev(first > 0);
            } else {
                pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
            }
        } catch (Exception e) {
            logger.error("all", e);
        }
        return pagedResult;
    }

}
