/*
 * @Autor A.D.C.
 */

package it.siae.msgestioneconflitti.service;


import it.siae.msgestioneconflitti.entity.MmRichiesteGestioneConflitti;

public interface CommonsService {
    void updateReportStatus(MmRichiesteGestioneConflitti report);
}
