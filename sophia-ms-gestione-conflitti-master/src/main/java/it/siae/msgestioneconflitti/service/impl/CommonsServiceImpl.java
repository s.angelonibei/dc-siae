/*
 * @Autor A.D.C.
 */

package it.siae.msgestioneconflitti.service.impl;


import it.siae.msgestioneconflitti.entity.MmRichiesteGestioneConflitti;
import it.siae.msgestioneconflitti.repository.MmRichiesteGestioneConflittiRepository;
import it.siae.msgestioneconflitti.service.CommonsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;


@Service
@Transactional
public class CommonsServiceImpl implements CommonsService {

    private final Logger logger = LoggerFactory.getLogger(CommonsServiceImpl.class);

    @Autowired
    private MmRichiesteGestioneConflittiRepository gestioneConflittiRepo;

    @Override
    public void updateReportStatus(MmRichiesteGestioneConflitti report) {
        report.setUltimoAggiornamento(new Date());
        gestioneConflittiRepo.save(report);
        gestioneConflittiRepo.flush();
    }

}
