/*
 * @Autor A.D.C.
 */

package it.siae.msgestioneconflitti.service;

import it.siae.msgestioneconflitti.dto.MmGestioneConflittiRequest;
import it.siae.msgestioneconflitti.dto.MmRichiesteGestioneConflittiDTO;
import it.siae.msgestioneconflitti.dto.SchemiRipartoDTO;
import it.siae.msgestioneconflitti.entity.MmRichiesteGestioneConflitti;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.List;

public interface GestioneConflittiService {
    List<MmRichiesteGestioneConflittiDTO> toDtoList(List<MmRichiesteGestioneConflitti> richiesteGestioneConflittis);

    List<MmRichiesteGestioneConflitti> createReportRequest(MmGestioneConflittiRequest dto) throws Exception;
}
