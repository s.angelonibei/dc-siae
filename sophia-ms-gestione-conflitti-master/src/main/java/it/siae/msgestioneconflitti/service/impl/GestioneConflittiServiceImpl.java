/*
 * @Autor A.D.C.
 */

package it.siae.msgestioneconflitti.service.impl;

import com.alkemytech.sophia.commons.aws.S3;
import it.siae.msgestioneconflitti.dto.MmGestioneConflittiRequest;
import it.siae.msgestioneconflitti.dto.MmRichiesteGestioneConflittiDTO;
import it.siae.msgestioneconflitti.dto.SchemiRipartoDTO;
import it.siae.msgestioneconflitti.entity.MmRichiesteGestioneConflitti;
import it.siae.msgestioneconflitti.entity.enumeration.StatoGestioneConflittiEnum;
import it.siae.msgestioneconflitti.report.csv.WriteCsvImpl;
import it.siae.msgestioneconflitti.repository.MmRichiesteGestioneConflittiRepository;
import it.siae.msgestioneconflitti.service.GestioneConflittiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@Service
@Transactional
public class GestioneConflittiServiceImpl implements GestioneConflittiService {

    @Autowired
    MmRichiesteGestioneConflittiRepository gestioneConflittiRepository;

    @Autowired
    WriteCsvImpl writeCsv;

    @Value("${spring.s3uri.fileInputLocation}")
    public String fileInputLocation;

    Logger logger = LoggerFactory.getLogger(GestioneConflittiServiceImpl.class);

    @Override
    public List<MmRichiesteGestioneConflitti> createReportRequest(MmGestioneConflittiRequest dto) throws Exception {
        MmRichiesteGestioneConflitti r = new MmRichiesteGestioneConflitti();

        r.setSchemiRiparto(dto.getAnno() + "-" + dto.getMese());
        r.setTerritorio(dto.getTerritorio());
        r.setStato(StatoGestioneConflittiEnum.DA_ELABORARE.getName());
        r.setDataAcquisizione(new Date());
        r.setUltimoAggiornamento(new Date());
        r.setCreataDa(dto.getCreatoDa());

        //UPLOAD FILE SU S3
        final S3.Url s3Uri = new S3.Url(fileInputLocation);
        File convFile = new File(dto.getFile().getOriginalFilename());
        convFile.createNewFile();
        try(InputStream is = dto.getFile().getInputStream()) {
            Files.copy(is, convFile.toPath(),REPLACE_EXISTING);
        }
        if(!writeCsv.upload(s3Uri,convFile)){
            r.setStato(StatoGestioneConflittiEnum.ERRORE.getName());
            r.setErrore("Errore durante il caricamento del file");
            gestioneConflittiRepository.save(r);
            throw new Exception("Upload File input failed");
        }else {
            r.setNomeFile(convFile.getName());
        }

        List<MmRichiesteGestioneConflitti> l = gestioneConflittiRepository.findByIdRichiestaAndNomeFileAndStatoAndSchemiRipartoAndTerritorioNot(r.getIdRichiesta(), r.getNomeFile(), StatoGestioneConflittiEnum.ERRORE.getName(), r.getSchemiRiparto(), r.getTerritorio());
        if (l != null && !(l.size() > 0)) {
            gestioneConflittiRepository.save(r);
        } else {
            logger.info("ERROR !! Duplicate key , insert into MM_RICHIESTE_GESTIONE_CONFLITTI FAILED");
            throw new Exception("E' stata già trovata una richiesta con i parametri inseriti");
        }
        return gestioneConflittiRepository.findByIdRichiestaAndNomeFileAndStatoAndSchemiRipartoAndTerritorio(r.getIdRichiesta(), r.getNomeFile(), r.getStato(), r.getSchemiRiparto(), r.getTerritorio());
    }

    public List<MmRichiesteGestioneConflittiDTO> toDtoList(List<MmRichiesteGestioneConflitti> richiesteGestioneConflittis) {
        List<MmRichiesteGestioneConflittiDTO> dtoList = new ArrayList<>();
        for (MmRichiesteGestioneConflitti m : richiesteGestioneConflittis) {
            MmRichiesteGestioneConflittiDTO dto = new MmRichiesteGestioneConflittiDTO();
            dto.setIdRichiesta(m.getIdRichiesta());
            dto.setFile(m.getNomeFile());
            dto.setTerritorio(m.getTerritorio());
            dto.setSchemiRiparto(m.getSchemiRiparto());
            dto.setStato(m.getStato());
            dto.setDataAcquisizione(m.getDataAcquisizione());
            dto.setUltimoAggiornamento(m.getUltimoAggiornamento());
            dto.setCreatoDa(m.getCreataDa());
            dto.setErrore(m.getErrore());
            dto.setYtAudio(m.getYtAudio());
            dto.setYtStaging(m.getYtStaging());
            dtoList.add(dto);
        }
        return dtoList;
    }


}
