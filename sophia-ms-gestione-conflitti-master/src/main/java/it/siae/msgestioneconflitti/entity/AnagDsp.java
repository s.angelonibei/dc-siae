/*
 * @Autor A.D.C.
 */

/*
 * @Autor A.D.C.
 */

package it.siae.msgestioneconflitti.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "ANAG_DSP")
@Entity
@Data
public class AnagDsp implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(insertable = false, name = "IDDSP", nullable = false)
    private String idDsp;

    @Column(name = "CODE", nullable = false)
    private String code;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "FTP_SOURCE_PATH")
    private String ftpSourcePath;

    
}