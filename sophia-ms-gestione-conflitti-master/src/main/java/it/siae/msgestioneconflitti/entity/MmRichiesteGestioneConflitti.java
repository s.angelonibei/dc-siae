package it.siae.msgestioneconflitti.entity;

import lombok.Data;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Data
@Table(name ="MM_RICHIESTE_GESTIONE_CONFLITTI",schema = "MCMDB_debug")
@NamedQueries({@NamedQuery(name = "MmRichiesteGestioneConflitti.all", query = "select x from MmRichiesteGestioneConflitti x order by x.dataAcquisizione desc")})
@Entity
@XmlRootElement
public class MmRichiesteGestioneConflitti {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, name = "ID_RICHIESTA", nullable = false)
    private Long idRichiesta;

    @Column(name = "NOME_FILE")
    private String nomeFile;

    @Column(name = "SCHEMI_RIPARTO")
    private String schemiRiparto;

    @Column(name = "TERRITORIO")
    private String territorio;

    @Column(name = "STATO")
    private String stato;

    @Column(name = "DATA_ACQUISIZIONE")
    private Date dataAcquisizione;

    @Column(name = "ULTIMO_AGGIORNAMENTO")
    private Date ultimoAggiornamento;

    @Column(name = "RICHIESTA_DA")
    private String creataDa;

    @Column(name = "DESCRIZIONE_ERRORE")
    private String errore;

    @Column(name = "PATH_S3_YT_AUDIO")
    private String ytAudio;

    @Column(name = "PATH_S3_YT_STAGING")
    private String ytStaging;

    public String getErrore() {
        return errore;
    }

    public void setErrore(String errore) {
        if(errore.length()>400){
            errore = errore.substring(0,400);
        }
        this.errore = errore;
    }
}
