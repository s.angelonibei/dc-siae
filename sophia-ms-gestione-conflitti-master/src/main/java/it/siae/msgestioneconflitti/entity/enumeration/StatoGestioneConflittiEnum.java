/*
 * @Autor A.D.C.
 */

package it.siae.msgestioneconflitti.entity.enumeration;

public enum StatoGestioneConflittiEnum {
    DA_ELABORARE("DA ELABORARE"),
    IN_CORSO("IN CORSO"),
    COMPLETATA("COMPLETATA"),
    ERRORE("ERRORE");

    private String name;

    StatoGestioneConflittiEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
