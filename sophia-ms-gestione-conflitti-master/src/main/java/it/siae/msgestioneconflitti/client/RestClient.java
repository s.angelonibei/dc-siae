/*
 * @Autor A.D.C.
 */

package it.siae.msgestioneconflitti.client;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import it.siae.msgestioneconflitti.entity.MmRichiesteGestioneConflitti;
import it.siae.msgestioneconflitti.entity.enumeration.StatoGestioneConflittiEnum;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class RestClient {
    public static Logger logger = LoggerFactory.getLogger(RestClient.class);
    public static class Result {

        private int statusCode;
        private JsonElement jsonElement;

        public int getStatusCode() {
            return statusCode;
        }

        public Result setStatusCode(int statusCode) {
            this.statusCode = statusCode;
            return this;
        }

        public JsonElement getJsonElement() {
            return jsonElement;
        }

        public Result setJsonElement(JsonElement jsonElement) {
            this.jsonElement = jsonElement;
            return this;
        }

        @Override
        public String toString() {
            return new GsonBuilder().setLenient()
                    .setPrettyPrinting().create().toJson(this);
        }
    }


    private static Result fromResponse(CloseableHttpResponse response,MmRichiesteGestioneConflitti report) throws IOException {
        Result result = new Result()
                .setStatusCode(response.getStatusLine().getStatusCode());
        HttpEntity responseEntity = response.getEntity();
        if (null != responseEntity) {
            try (InputStream in = responseEntity.getContent()) {
                byte[] buffer = new byte[1024];
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                for (int count; -1 != (count = in.read(buffer)); ) {
                    out.write(buffer, 0, count);
                }
                result.setJsonElement(new GsonBuilder().setLenient().create()
                        .fromJson(out.toString("UTF-8"), JsonElement.class));
            }catch (Exception e){
                if(report != null) {
                    report.setStato(StatoGestioneConflittiEnum.ERRORE.getName());
                    report.setErrore(e.getMessage());
                    logger.error("createCsvFile", e);
                }
            }
        }
        return result;
    }

    public static Result get(String url, MmRichiesteGestioneConflitti report) throws IOException {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpGet httpGet = new HttpGet(url);
            try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
                return fromResponse(response,report);
            }
        }
    }

    public static Result post(String url, JsonObject requestJson,MmRichiesteGestioneConflitti report) throws IOException {
        return post(url, requestJson.toString(),report);
    }

    public static RestClient.Result post(String url, String requestJson) throws IOException {
        StringEntity requestEntity = new StringEntity(requestJson, ContentType.create("application/json", "UTF-8"));
        return post(url, (HttpEntity)requestEntity, false);
    }

    private static CloseableHttpClient getCloseableHttpClient(boolean insecure) throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        if (insecure) {
            SSLContextBuilder builder = new SSLContextBuilder();
            builder.loadTrustMaterial((KeyStore)null, new TrustSelfSignedStrategy());
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.build(), NoopHostnameVerifier.INSTANCE);
            return HttpClients.custom().setSSLSocketFactory(sslsf).build();
        } else {
            return HttpClients.createDefault();
        }
    }

    private static Result fromResponse(CloseableHttpResponse response) throws IOException {
        Result result = (new Result()).setStatusCode(response.getStatusLine().getStatusCode());
        HttpEntity responseEntity = response.getEntity();
        if (null != responseEntity) {
            InputStream in = responseEntity.getContent();

            try {
                byte[] buffer = new byte[1024];
                ByteArrayOutputStream out = new ByteArrayOutputStream();

                while(true) {
                    int count;
                    if (-1 == (count = in.read(buffer))) {
                        result.setJsonElement((JsonElement)(new GsonBuilder()).create().fromJson(out.toString("UTF-8"), JsonElement.class));
                        break;
                    }

                    out.write(buffer, 0, count);
                }
            } catch (Throwable var8) {
                if (in != null) {
                    try {
                        in.close();
                    } catch (Throwable var7) {
                        var8.addSuppressed(var7);
                    }
                }

                throw var8;
            }

            if (in != null) {
                in.close();
            }
        }

        return result;
    }


    private static RestClient.Result post(String url, HttpEntity httpEntity, boolean insecure) throws IOException {
        try {
            CloseableHttpClient httpClient = getCloseableHttpClient(insecure);

            RestClient.Result var6;
            try {
                HttpPost httpPost = new HttpPost(url);
                httpPost.setEntity(httpEntity);
                CloseableHttpResponse response = httpClient.execute(httpPost);

                try {
                    var6 = fromResponse(response);
                } catch (Throwable var10) {
                    if (response != null) {
                        try {
                            response.close();
                        } catch (Throwable var9) {
                            var10.addSuppressed(var9);
                        }
                    }

                    throw var10;
                }

                if (response != null) {
                    response.close();
                }
            } catch (Throwable var11) {
                if (httpClient != null) {
                    try {
                        httpClient.close();
                    } catch (Throwable var8) {
                        var11.addSuppressed(var8);
                    }
                }

                throw var11;
            }

            if (httpClient != null) {
                httpClient.close();
            }

            return var6;
        } catch (KeyStoreException | KeyManagementException | NoSuchAlgorithmException var12) {
            throw new RuntimeException(var12);
        }
    }
    
    public static Result post(String url, String requestJson,MmRichiesteGestioneConflitti report) throws IOException {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpPost httpPost = new HttpPost(url);
            StringEntity requestEntity = new StringEntity(requestJson.toString(),
                    ContentType.create("application/json", "UTF-8"));
            httpPost.setEntity(requestEntity);
            try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
                return fromResponse(response,report);
            }
        }
    }

    public static Result put(String url, JsonObject requestJson,MmRichiesteGestioneConflitti report) throws IOException {
        return put(url, requestJson.toString(),report);
    }

    public static Result put(String url, String requestJson,MmRichiesteGestioneConflitti report) throws IOException {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpPut httpPut = new HttpPut(url);
            StringEntity requestEntity = new StringEntity(requestJson.toString(),
                    ContentType.create("application/json", "UTF-8"));
            httpPut.setEntity(requestEntity);
            try (CloseableHttpResponse response = httpClient.execute(httpPut)) {
                return fromResponse(response,report);
            }
        }
    }

}
