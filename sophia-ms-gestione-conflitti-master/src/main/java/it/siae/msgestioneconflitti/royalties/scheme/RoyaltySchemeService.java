package it.siae.msgestioneconflitti.royalties.scheme;

import com.alkemytech.sophia.royalties.Configuration;
import com.alkemytech.sophia.royalties.role.RoleCatalog;
import com.alkemytech.sophia.royalties.scheme.IrregularityCatalog;
import com.alkemytech.sophia.royalties.scheme.TemporaryWorkCatalog;

public class RoyaltySchemeService extends com.alkemytech.sophia.royalties.scheme.RoyaltySchemeService {
    public RoyaltySchemeService(Configuration configuration, RoleCatalog roleCatalog, TemporaryWorkCatalog temporaryWorkCatalog, IrregularityCatalog irregularityCatalog) {
        super(configuration, roleCatalog, temporaryWorkCatalog, irregularityCatalog);
    }
}
