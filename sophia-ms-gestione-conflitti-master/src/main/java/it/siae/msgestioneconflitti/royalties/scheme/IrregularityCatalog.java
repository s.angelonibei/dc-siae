package it.siae.msgestioneconflitti.royalties.scheme;

import java.nio.charset.Charset;

public class IrregularityCatalog extends com.alkemytech.sophia.royalties.scheme.IrregularityCatalog {
    public IrregularityCatalog(Charset charset) {
        super(charset);
    }
}
