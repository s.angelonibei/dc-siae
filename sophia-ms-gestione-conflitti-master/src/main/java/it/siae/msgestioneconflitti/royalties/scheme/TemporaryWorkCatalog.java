package it.siae.msgestioneconflitti.royalties.scheme;

import java.nio.charset.Charset;

public class TemporaryWorkCatalog extends com.alkemytech.sophia.royalties.scheme.TemporaryWorkCatalog{
    public TemporaryWorkCatalog(Charset charset) {
        super(charset);
    }
}
