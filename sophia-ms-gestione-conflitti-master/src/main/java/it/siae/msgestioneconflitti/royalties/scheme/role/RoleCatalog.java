package it.siae.msgestioneconflitti.royalties.scheme.role;

import java.nio.charset.Charset;

public class RoleCatalog extends com.alkemytech.sophia.royalties.role.RoleCatalog {
    public RoleCatalog(Charset charset) {
        super(charset);
    }
}
