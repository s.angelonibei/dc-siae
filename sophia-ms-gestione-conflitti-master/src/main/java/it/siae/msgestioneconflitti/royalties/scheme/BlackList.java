package it.siae.msgestioneconflitti.royalties.scheme;

import com.alkemytech.sophia.royalties.role.RoleCatalog;

import java.nio.charset.Charset;

public class BlackList extends com.alkemytech.sophia.royalties.blacklist.BlackList {
    public BlackList(Charset charset, RoleCatalog roleCatalog, String society) {
        super(charset, roleCatalog, society);
    }
}
